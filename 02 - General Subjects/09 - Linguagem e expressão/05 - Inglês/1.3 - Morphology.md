# Conceptualization

* Study of the internal structure of complex words
* Processes by which words are formed
* **Morphemes**
  * Parts of the word
  * Root = independent morpheme (almost the same as *stem*, but there is a little difference)
  * Prefixe = precede the root
  * Suffixes = follow the root
  * Infixes = in the middle of a root
  * Example: unexpected =  un- + expect + -ed (three morphemes)
* **Processes**
  * inflection = modifies a word
  * Derivation = creates a new word from an existing one

# Comparative study

* Languages can be classified by its morphology

* There aren’t particional classifications, but a *continuum* change of the language philosofy
* A language also change is characteristic over time (ex: english envolved from a more synthetic to a more analitical language)
* **Synthetic languages**
  * Combine (synthesize) multiple concepts into each word
  * Example of highly synthetic: finnish
  * *Agglutinating languages*
    * Put morphemes together, each with an individual meaning (easy to interpret separatly)
  * *Fusional languages*
    * Put morphemes together, each with several different lexical meanings (hard to interpret separatly)
  * *Polysynthetic*
    * combine multiple stems as well as other morphemes into a single continuous word
    * Exemple in Mohawk: Washakotya'tawitsherahetkvhta'se = "He ruined her dress"
* **Analytic languages**
  * Break up (analyze) concepts into separate words
  * Example of highly analytic: mandarin