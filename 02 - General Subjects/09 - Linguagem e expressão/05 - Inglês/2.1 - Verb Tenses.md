# Conceptualization

* When the action or state shown by the verb happen
* expresses time reference with reference to the moment of speaking
* English has only two morphological tenses: the present (e.g., he goes), and the past (e.g., he went). The future is indicated with auxiliaries (like *will* and *would*)
* [http://www.learnenglish.de/grammar/tensetext.html*](http://www.learnenglish.de/grammar/tensetext.html)

![Resultado de imagem para english
tenses](Images - 2.1 - Verb Tenses/image2-1567125210652.jpeg){width="5.151388888888889in"
height="4.626388888888889in"}

## Time aspects

* Each tense can be combined with the following aspects: simple, perfect, professive (or *continuos*) and perfect progresive 
* **Simple tenses**
  * permanent or regularly characteristics/activities
  * A moment in time (past, present or future) not related to any other time
* **Perfect tenses**
  * A moment in time linked to a moment before it
  * Have + verb in past participle
* **Continuous tenses**
  * particular point in the time, but still happening
  
  * To express continous activities (but *not* habitual activities)
  
  * Can be used to express an activity that was intersected by another event (something was happening when…)
  
  * can be used to express annoyance (something is aaaalways happening). Tip: the adverb *always* is generally just used with countinous tenses in this case.
  
  * <u>Sensory perseptions</u>
  
    * Some verb cannot be inflected in the present participle (so, in continuous tenses)
  
    * Sensory perceptions: those verbs are involuntary, so cant be inflected. But they usually have a similar verb that can be inflected, related with *paying attention* to that sensory perception 
  
      | **Involutary (cannot be progressive)** | **Volutary**    |
      | -------------------------------------- | --------------- |
      | Hear                                   | Listen          |
      | see                                    | look at / watch |
      | feel                                   | touch           |
  
    * Possetions/relation: to possess, own, belong, contain, have (careful: to have can be inflected if it means *to experience*)
  
    * Emotions and attitudes: to prefer, care, hope, want, love/hate (careful: love and hate can be inflected to mean an really strong emotion)
  
    * Mental activity (when they mean something instantaneous): remember, forget, recognize, believes, consider. Careful: sometimes they can be used in 
  
    * State verbs: to look, appear, seem, be (careful: the *to be* can be inflected if it means *to behave usually*).
* **Perfect continuous tenses**
  * similar to continuous, but happening during a period of time (defined or not)
  * To Previous continuous actions, repetitive actions or temporary effets (e.g.: I can smell that he has been eating onions)



## Verb forms

* Principal parts of the verb: those forms that a student must memorize in order to be able to conjugate the verb
* All the forms: root (base form, morphologically equal to present form), third-person singular (add -s), present participle, past, and past participle

* The difference between a regular and an irregular verb is the formation 
  of the simple past and past participle
* ==finite and non finite forms??==
* Ex: principal parts of a regular verb
* ![1567124859378](Images - 2.1 - Verb Tenses/1567124859378.png)
* 
* **Participles**
* verb that work as an adjective/adverb: modifing an noun or verb
  * Generally formed by the base form of the verb + sufix -*en* or *-ed*
  * Ex: a drunk*en* man
  * Ex: well-preserv*ed* lady
  * Tense inflection
    * Participle are also used to caracterize some tenses
    * The participles must be combined with helping verbs before they can be used as verbs in sentences
    * Past participle: usually the same as the past tense form. ==diference to gerund (morphologicaly equal, grammaticl different)?==
    * Present participle: always with sufix -ing
    * Both types of participles have several usages. See https://en.wikipedia.org/wiki/Participle#Modern_English


## Grammatical mood

* Or *modality*
* the use of verbal inflections that allow speakers to express their attitude toward what they are saying
* Non-finite form ( Infinitives, gerunds, and participles) will not be studied here
* **conditional**
* **imperative**
* **indicative**
* **interrogative**
* **subjunctive**.

# Present

### Simple

* Frequently in the present
* **Examples**
  * I eat
  * he works
  * "I always get up at 6.00."
  * "you never drink coffee before 12.00."
  * "she works on my website every day."
  * "Every Monday and Thursday I go to the gym."
  * Veb work:

  ![img](./Images - 2.1 - Verb Tenses - Present/media/image1.png)

* **Verb have, go and do**

  * Irregulars with he, she and it.

  * do → does

  * have → has

  * go → goes

  ![img](./Images - 2.1 - Verb Tenses - Present/media/image2.png)

* **Verb endings**

  * We change the end of verbs in third person singular (he, she and it)
    
  * General rule → add “s” → *ex:*
  
  ![img](./Images - 2.1 - Verb Tenses - Present/media/image3.jpeg)
  * End in **-O, -CH, -SH, -SS, -X,** or **-Z →** add **-ES →** *ex: catch – catches*
  
  * End in a **consonant + Y →** remove the **Y** and add **-IES** →
    *ex: marry – marries*
  
  * End in a **vowel + Y →** just add **-S** → *ex: play – plays*
  
  * The “s” disappears in the negative and interrogative sentences.
  
  * *Ex: he doesn’t speak English.*
  
  * *Ex: Does he speak French?*
* **Other annotations**
  
  * Dont use the simple presente with "this + time word". Ex: "I take 3 courses this semester" is wrong, you should use the present countinuous 

### Perfect

-   The action begin in the past and end in the present
-   There isn’t any relation with verb tenses in Portuguese. Maybe with the “pretérito perfeito composto do modo indicativo”
-   Present tense of *to have* + participle.
-   Past participle of verb to be: been
-   **Exemples**
  * I have eaten
  * I have been married for 6 years. (*Estou casado há seis anos.*)
  * She has been married for ten years. (*Ela está casada há 1 anos.*)
  * How long have you been together? (Há quanto tempo vocês estão juntos?)
  * I’ve known Patrick for years. (Eu conheço o Patrick há anos.)
  * We’ve known each other since college. (A gente se conhece desde a faculdade.)
  * Where is Maria? I haven’t seen her for weeks.
  * We have just got back from our holidays.
  * They haven’t written
  * I have traveled each and every highway
* **Often used with the words…**
  * before, in the past, ever, yet, already (undefined time in past)
  * for <period of time>, since <specific time>, until now, up to now so far, thus far (unfinished action)
  * just, recently, barely (recently completed)

### Continuous


-   Happening now
-   Termination -ing in the verb
-   is/are + present participle (-ing)
-   **Example**

    -   I am eating

![img](./Images - 2.1 - Verb Tenses - Present/media/image4.png){width="7.457638888888889in"
height="1.8020833333333333in"}

### Perfect continous

* has/have been + the present participle (root + -ing).

* I have been eating

# Past

### Simple

-   Finished actions in the past
-   **Examples**
    -   I ate
    -   He worked
-   **Affirmative**

    -   Regular verbs → add “ed”

    -   Irregular verbs → good luck!
-   **Negative**

    -   Personal pronoun + didn’t + verb in the base form of the
        infinitive
-   **Interrogative**
    -   Did + personal pronoun + verb in the base form of the infinitive
-   *Example 1: to play (regular verb)*

![Resultado de imagem para simples
past](./Images - 2.2 - Verb Tenses - Past/media/image1.png){width="7.268055555555556in"
height="3.0993055555555555in"}

-   *Example 2: to have (irregular verb)*

![Resultado de imagem para simples past
have](./Images - 2.2 - Verb Tenses - Past/media/image2.png){width="4.075694444444444in"
height="3.1902777777777778in"}

-   *Example 3: to be (an especial case of irregular)*

![img](./Images - 2.2 - Verb Tenses - Past/media/image3.png)

* key expressions
  * used to
  * would 

### Perfect

* Past tense of *to have* + past participle

* **Example**
  * I had eaten

### Continuous

* was/were + present participle (-ing)

* **Example**
  * I was eating

### Perfect Continuous

* Had been + the present participle (root + -ing).

* **Example**
  * I had been eating

# Future

* English does not have a future tense formed by verb inflection (changes in the verb, morphologial indication), so auxiliary verbs (like will or shall) are used

### Simple

* **Will + main verb**
  * “shall” can be used to replace “will”, in the case of an invitation 
  * *Ex: shall we dance?*
  * *Ex: I will eat*
* **Be going to + verb**
  * We use it to say something that is already planned/decided or is
    about to happen
  * express more sureness than ‘will”’
  * Informally spoken as /gonna/
  * *Ex: we’re going to buy a new car next year*
  * *Ex: look at that cloud: I think is going to rain*
  * *Ex: I’m going to go to Germany.*

### Perfect

* an action that is to be completed sometime prior to a future time
* auxiliary *will* (or sometimes *shall* in the first person) + have + verb in past participle
* **Example**
  * I will have eaten
  * I shall have finished my essay by Thursday.
  * When I finally search him he will have disposed of the evidence.
  * By next year we will have lived in this house for half a century.

### Future-in-the-past

* was/were + going to + VERB, or would + VERB

* **Example**
  * She knew that she would win the game

### Continuous

* will be + present participle (-ing)

* **Example**
  * I will be eating
  * He will be working

### Perfect Continuous

* ow long something will have been happening up to a point of time in the future
* the auxiliary *will* (or sometimes *shall*, as above), the bare infinitive *have*, the past participle *been*, and the present participle of the main verb.
* will have been + the present participle (root + -ing).
* **Example**
  * I will have been eating 
  * *They will have been studying for five hours by six o'clock.*
  * *Mary will have been playing golf for five hours by the time she finishes.*
  * will have been drinking for ten hours