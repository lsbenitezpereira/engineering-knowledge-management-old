# Sports

* NBA - National Basketball Association
  * 30 teams (29 in the United States and 1 in Canada)
* Super Bowl
  * Annual championship game of the National Football League (NFL)
  * played between mid-January and early February