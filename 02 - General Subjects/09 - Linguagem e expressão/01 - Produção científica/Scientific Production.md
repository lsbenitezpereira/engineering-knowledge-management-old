# Conceitos Básicos

* **Ensino**
  * Aprendizagem de conhecimentos já estabelecidos
  * Método: caminho a ser seguido para que os resultados estejam
    de acordo ao proposto inicialmente
* **Pesquisa**
  * Desenvolvimento e descoberta de novos conhecimentos*
  * Pode ser de caráter básico, aplicado (existe uma pergunta a ser
    respondida) ou de desenvolvimento tecnológico e inovação (não existe uma
    pergunta, é apenas o desenvolvimento puro)
  * Pode ser quantitativa ou qualitativa 
* **Extensão**
  * Diáogo/interação com agentes sociais externos

  * Socilização/troca de conhecimento

  * Esse diálogo deve ser bidirecional: a comunidade académica não é
  detentora absoluta do conhecimento (nem mesmo do conhecimento
  científico)

  * Pode sr um programa, projeto, curso/oficina, evento ou prestação de
  serviço (cobrado ou não)

## Tipos de pesquisa

-   **Natureza** → básica ou aplicada

-   **Abordagem**

    -   *Qualitativa*

        -   Interpretação dos fatos

        -   Caráter subjetivo

        -   Método indutivo

    -   *Quantitativa*

        -   Análise estatística/numérica

        -   caráter objetivo

        -   Método dedutivo

    -   *Qualiquantitativa* → mista

-   **Objetivos**

    -   *Exploratória*

        -   Constrói hipóteses.

        -   Geralmente é bibliográfica ou estudo de caso

    -   *Descritiva*

        -   Descreve uma população, fenômeno, etc

    -   *Explicativa*

        -   Identifica fatores

        -   Visa explicar as coisas

        -   Requer método experimental ou observacional

-   **Procedimentos**

    -   Bibliográfica → revisão sistemática sobre os conhecimentos
        disponíveis

    -   Documental → em documentos inéditos

    -   Estudo de caso → estudo profundo e focado

    -   Pesquisa-ação → resolução de um problema

## Etapas de pesquisa

-   **Escolha do tema**

    -   Defina claramente critérios de seleção

    -   Escolha um campo do conhecimento

    -   Procure “lacunas”: temas que ainda não foram explorados,
        evitando chover no molhado

    -   *Delimitação*

        -   Determine um assunto prioritário

        -   Leve em conta se existe material bibliográfico disponível e
            suficiente

        -   A limitação pode ser temporal, geográfica, tecnológica ou
            outros.

        -   Devemos determinar um enfoque sobre esse tema (histórico,
            estatístico, etc)

    -   *Definição dos objetivos*

        -   Gerais → propósito do artigo (dicas de objetivos “fáceis”:
            mapear, identificar, levantar, diagnosticar, traçar o perfil
            ou historiar)

        -   Específicos → claramente o que será feito

-   **Definição do problema**

    -   Averigue o tema escolhido

    -   Veja que tipo de respostas você precisar obter

    -   Formule hipóteses

-   **Desenvolvimento**

    -   Levantamento bibliográfico → veja o que existe disponível
-   Fichamento → processe o material
    -   Redação → juntar tudo o que foi fichado

## Metodologias

Exemplo de metodologia:

![image-20200601053816843](Images - Scientific Production/image-20200601053816843.png)

* reproducibility = computational reproducibility; Reproducibility comes  before replicability because it is a minimal requirement for any  reported results to be trusted
* replicability = scientific applicability; consistent results, even with different methods

## Linguistic resources

### Portuguese

-   “Este trabalho tem com como objetivo...”

-   “Este trabalho visa a apresentação...”

-   “Para o desenvolvimento desse trabalho, utilizou-se a metodologia
    de...”

-   “foram realizadas pesquisas bibliográficas de modo qualitativo, para
    as quais o referencial teórico utilizado foi obtido em bases de
    dados como a *Google Academics*, a CAPES (Coordenação de
    Aperfeiçoamento de Pessoal de Nível Superior) e a *SciELO*
    (Scientific Eletronic Library Online)”

-   Números/estátisticas: diga "mais de" ao invés de "quase"

### English

-   Do not use contractions

-   *E.g. → exempli grata*

-   **introduction**

    -   This essay discusses the importance of ...

-   **Development**

    -   The interviews were conducted

-   **Results**

    -   “Taken together, these results show that”

-   **Fancy adverbs/connectors/etcs**
    -   Oposition: Conversely, In contrast, However, although/though, nevertheless, depiste, in spite of, despite the fact that
    -   More information: In addition, Furthermore, additionally, in addition, moreover
    -   Correlative: not only <this>, but also <that>
    -   Cause: bacause of, due to, as a result of
    -   Consequence: therefore, as a result, consequently
    -   Cause/consequence: for <cause>, so <consequence>
    -   Comparison: just as, similarly, in comparison, similar to
    -   Contrast: whereas, while, in contrast, on the other hand
    -   Condition: only if, unless, <do something> otherwise <consequence>, 
    -   get = obtain
    -   a lot = many

-   **References**

    -   This research shows that ...

    -   It could be said that ...

    -   Smith's argument illustrates that ...

    -   Freud's theory supports the view that...

    -   Smith's research is significant because

-   Words that do not accept pluralization

    -   Research
-   literature

# Tipos de produção

* Communication is part of science's
* Write is not just to “put in the curriculum”

## Resenha

-   **Cabeçalho**

    -   IFSC (negrito, maiúscula e por escrito)

    -   Nome do curso

    -   Nome da disciplina

    -   Nome da equipe

-   **Título e identificação**

    -   \*dois espaços\*

    -   “Resenha” (sublinhado e centralizado)

    -   \*três espaços\*

    -   Identificação da obra: “SOBRENOME, nome. **Título**: subtítulo.
        Edição. Local: editora, data”.

    -   \*dois espaços\*

-   **Corpo do texto**

    -   Breve apresentação da obra

    -   Descrição da estrutura (quantos capítulos, páginas, etc)

    -   Descrição do conteúdo (resumo)

    -   Análise Crítica

    -   Recomendar ou não a obra

    -   Assine e identifique-se

    -   Referência

## Resumo

-   Síntese da introdução, objetivos, metodologia e
    resultados/conclusões

-   Caráter informativo

-   Parágrafo único sem recuo (é um monólito textual)

-   Geralmente é limitado a 250 palavras

-   Não pode ter citação

-   Evite dar muito spoiler sobre o artigo

-   **Palavras-chave** → separadas por ponto final

-   **Tradução** → geralmente para o inglês (abstract e Key Words)


## Artigos

* **Tempo verbal**
  * No titulo, sempre no presente
  * No desenvolvimento, tambem escrevermos no presente (não estamos contando historinha, e sim descrevendo um trabalho), ex: pesquisa-se, utiliza-se, etc
  * A exceção é quando falamos de trabalhos anteriores, ex: pesquisou-se, tal autor implementou, 
  * Caso for escrever no futuro (nas considerações finais, por exemplo), a forma correta é “pesquisar-se-á”
  * http://www.professorwellington.adm.br/tempo.htm
  * ==**Em relatórios (ou seja, explicitamente sobre coisas do passado), também escrevemos no presente?==**
* **Pessoa gramatical**
  * Deve-se escolher a terceira pessoa do singular (com o uso da partícula SE)
* **How to chose an relevant subject**
  * Do nos write just to describe something new: write to convey an useful and reusable idea
  * Each paper should cover one idea. If you have 10 ideas, write 10 papers
  * Do  not be over ambitious : the most must be hard enough to be interesting,  but easy enough to be solvable
* **The structure of a paper**
  * Introduction: give just the intuition, and put forward references to the section in which you talk more about each thing.
  * Methodology: it can be an section, or be embedded in the develoment
  * Development: be sure that your work is reprodusible 
  * Cite related works: briefly in introduction, then more in the end comparing with your implementation 
* **General Writing tips**
  * The first draft is the creative part of the writting, so do not lose time correcting little things
  * Write that in that sequence: methology, development, conclusion (first in topics), introduction, title
  * Don't be toooo formal: write as you was trying to explain If the review misunderstood, try to the more clear
  * Do not explain to the reader all your mistakes and dead ends, reconstructing the "maze" that was your research. Be direct and say only the right path
  * 'Title:  <solution> to <problem/area>

## **Apresentação oral**

* Um membro não deve brilhar mais que o outro
* Figuras e tabelas devem ser referenciadas da mesma forma que no
  artigo
* Aconselha-se que tudo o que for apresentado deva estar no
  trabalho, senão deve ser citado

* **Etapas**
  * Cumprimentar a plateia e a banca
  * Se apresentar. Se for uma equipe, recomenda-se que você se
    apresente por último.
  * Apresentar o trabalho
  * Conclua (dica: faça um resumo de 15 segundos da apresentação
    inteira e depois agradeça)

* **Slides**
  * Fundo claro e letra escura
  * Dá para intercalar letra preta com azul escura
  * Se for destacar, use bordô/vinho ao invés de vermelho vivo
  * Coloque numero nos slides
  * Tamanho mínimo: 24
  * Terminar com ponto final → começa com letra maiúscula
  * Terminar com ponto e vírgula → começa com letra minúscula
  * Referências → só uma página com as principais
  * Ferramentas para fazer slides
    * [*https://create.piktochart.com*](https://create.piktochart.com/)
    * [*https://slides.com/*](https://slides.com/)
    * [*https://prezi.com/*](https://prezi.com/)
    * [*https://business.tutsplus.com/articles/12-best-powerpoint-presentation-templates-with-great-infographic-slides--cms-25379*](https://business.tutsplus.com/articles/12-best-powerpoint-presentation-templates-with-great-infographic-slides--cms-25379)



# ABNT

-   NBR 6022:2003 → norma para apresentação de artigos científicos
    impressos
-   Escritos na 3º pessoa do singular + se (ex: acredita-se)
-   
-   **Links uteis**

    -   [*http://portal.bu.ufsc.br/normalizacao/*](http://portal.bu.ufsc.br/normalizacao/)

    -   [www.periodicos.capes.gov.br](http://www.periodicos.capes.gov.br/)

* Formatação usual
  * Margem superior e esquerda: 3cm
  * Margem inferior e direita: 2cm
  * Cabeçalho: Arial 12 centralizado, espaçamento simples
  * Título: Arial 14 centralizado, espaçamento simples
  * Corpo: Arial 12 justificado, espaçamento 1.5



## Estrutura

-   **Elementos pré-textuais**

    -   Título e subtítulo

    -   Autoria (acompanhado de cargo, instituição, e-mail e relação com
        a área)

    -   Resumo

    -   Palavras-chave

-   **Elementos textuais**

    -   Novo título, nova página

    -   *Introdução*

        -   o que foi pesquisado e o porquê da investigação

        -   Expõe os objetivos, bibliografia, justificativa, resultados
            significativos

    -   *Desenvolvimento*

        -   Fundamentação teórica/revisão de bibliografia

        -   Metodologia

        -   Subtítulos específicos do trabalho

        -   Resultado e discussões

    -   *Considerações finais*
-   Resultados obtidos
    
-   Criticas, recomendações e sugestões para pesquisas futuras
    
-   **Elementos pós textuais**

    -   Referências

    -   Glossário

    -   Anexos

    -   Apêndices

-   **Elementos de apoio ao texto**
    -   Citações
        
    -   Notas de rodapé (indicações bibliográficas, observações,
            aditamentos ao texto pelo autor, tradutor ou editor)
        
    -   Figuras e tabelas

## Citação

-   Menção no texto

-   Utilizaremos a norma da ABNT

### Citação direta

-   Transcrição textual pura

-   **Menos de 3 linhas (em fonte padrão)**

    -   Deve conter ano e página (página que consta na numeração no
        artigo, e não do pdf)

    -   Fica entre aspas duplas

    -   Podemos usar segundo, de acordo com, conforme, fulano indica
        que, etc..

    -   A citação pode vir no começo ou o final

![img](./Images - Scientific Production/media/image1.png){width="4.020833333333333in"
height="1.6041666666666667in"}

-   **Mais de 3 linhas (em fonte padrão)**

    -   Transcritas em bloco (novo parágrafo);

    -   Em espaço simples de entrelinhas;

    -   Recuo de 4 cm da margem esquerda (o dobro do normal)

    -   Letra menor que a do texto (fonte 10);

    -   Sem aspas e sem itálico;

![img](./Images - Scientific Production/media/image2.png){width="4.654141513560805in"
height="2.125in"}

-   **Direta com supressão**

    -   Utilizamos \[…\] para indicar descontinuidades no texto

![img](./Images - Scientific Production/media/image3.png){width="4.294642388451444in"
height="0.8125in"}

### Citação indireta

-   Transcrição livre

-   Só sobrenome e ano (sem aspas nem página)

-   Também pode ser feita no começo ou no final:

![img](./Images - Scientific Production/media/image4.png){width="4.25717738407699in"
height="1.8229166666666667in"}

### Citação da citação

-   Citamos uma citação (direta ou indireta) que pegamos de um artigo

-   O ideal seria ir na própria fonte, a menos que o documento original
    seja raro ou não esteja acessível

-   Apud = citado por (latim)

-   *Exemplo:* citação direta

![img](./Images - Scientific Production/media/image5.png){width="4.210416666666666in"
height="1.6929604111986in"}

### Outras expressões

-   \[sic\] = “assim mesmo” (quando o autor escreveu uma palavra
    errado/alterado)

-   “tradução nossa”. *Ex:*

![img](./Images - Scientific Production/media/image6.png){width="4.447719816272966in"
height="0.6458333333333334in"}

-   **Grifo**

    -   Podemos destacar em negrito algum ponto da citação

    -   Devemos indicar com “grifo nosso”.

    -   Se o próprio autor destacou o texto grifado, vai “grifo do
        autor”

![img](./Images - Scientific Production/media/image7.png){width="4.324219160104987in"
height="0.65625in"}

-   **Citação com mais autores**
-   et al = e outros
    
-   Autor por ordem de importância

![img](./Images - Scientific Production/media/image8.png){width="6.05542104111986in"
height="1.625in"}

## Referências

-   De onde você tirou as coisas
-   Não pode citação “falsa” segundo a ABNT (entrentando o IEEE aceita que referências utilizadas no trabalho sejam incluídas mesmo que não sejam citadas no texto)
-   É sempre o ultimo capitulo, à parte
-   Autor por ordem alfabética (ABNT) ou por ordem de citação no texto (IEEE)
-   **Como escrever o nome do autor**
    * DIAS, Paulo Fonseca
    * DIAS, Paulo F.
    * DIAS, P.F.
-   **Formatação ABNT** 
    * Título em negrito, subtítulo normal
    * Espaçamento simples entrelinhas
    * Uma linha branca entre citação
    * Alinhada à esquerda
-   **Autoplágio**
    * Pode se pode alterar ligueiramente um trabalho e publicar como um trabalho inédito
    * O mesmo trabalho pode ser apresentado em mais de um local, mas o editor deve saber que o trabalho não é inédito
    * Se for aproveitar (por exemplo) a fundamentação teorica de um antigo artigo anterior, referencie voce mesmo
    * Dilema ético: se por um lado auto plágio não é correto, por outro estamos deixando de divulgar trabalhos potencialmente úteis e de socializar esse conhecimento que talvez outros pesquisadores estejam precisando
-   **Softwares**
    * Zotero
    * Mendeley
    * Mendeley Web Import (extensão)

### Como fazer

-   Sem data: botamos s.d.p (sem data de publicação)
-   ==Sem local: (s.l.p.) ou [s.L]?==
-   **Autor repetido**

    -   Substitui-se o nome do autor das referências subsequentes por um
        traço equivalente a seis espaços.

![img](./Images - Scientific Production/media/image1.png){width="4.974837051618548in"
height="0.8541666666666666in"}

-   **Múltiplos autores**

![img](./Images - Scientific Production/media/image2.png){width="4.895779746281715in"
height="3.1979166666666665in"}

-   **Meio eletrônico**

    -   Evite fortemente páginas sem título e sem autor

    -   Endereço completo (não pode ser link reduzido)

![img](./Images - Scientific Production/media/image3.png){width="5.022957130358705in"
height="1.375in"}

### figuras e tabelas


-   Tem que ser “chamada” no texto (pelo número, não “abaixo”, “acima”,
    ect)
-   imagens e tabelas de autoria propria precisam ser referenciadas, falando que são suas

![img](./Images - Scientific Production/media/image4.jpeg){width="3.1145833333333335in"
height="1.9166666666666667in"}

![img](./Images - Scientific Production/media/image5.png){width="3.8541666666666665in"
height="1.9583333333333333in"}