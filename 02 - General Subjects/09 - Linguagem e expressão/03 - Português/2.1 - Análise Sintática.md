# Conceituação

-   **Frase** → enunciado com sentido completo

-   **Oração**

    -   Frase que se organiza ao redor de um verbo

    -   Toda oração possui verbo.

    -   Toda oração possui sentido completo.

    -   *Orações reduzidas*

        -   O verbo está no infinitivo, gerúndio ou particípio.

        -   Não são introduzidas por conjunção.

        -   Ex: Ao terminar a prova**,** *todo candidato deve aguardar*
            (infinitivo)

        -   Ex: *Ouvimos uma criança* chorando na praça (gerúndio)

        -   Ex: Comprada a casa**,** *a família mudou-se* (particípio)

-   **Período** → do começo até o ponto final

## Tipos de período

-   **Período simples** → 1 oração

-   **Período composto**

    -   Uma ou mais orações

    -   *Subordinadas*

        -   Quando uma completa o sentido da outra

        -   Ex: não fui para a aula porque estava doente.

        -   “Porque estava doente”, sozinho, não faz sentido, precisa de
            algo explicando.

    -   *Coordenadas*

        -   Quando as orações são independentes.

        - Ex: fomos a tarde e voltamos à noite.
        
          ![1566432604127](Images - 2.1 - Análise Sintática/1566432604127.png)

# Termos da oração

-   **Essenciais**
-   São os termos indispensáveis de uma oração
    
-   Sujeito e predicado
    
-   **Integrantes**

    -   Até podem faltar

    -   Completam o sentido dos termos essenciais.

    -   Complemento verbal, complemento nominal, agente da passiva

-   **Acessórios**

    -   Podem ser retirados da oração sem prejudicar o sentido geral.

    -   Geralmente indicam uma característica ou especificam algo

    -   Aposto, vocativo, adjunto adnominal, adjunto adverbial e
        predicativo

## Sujeito

* Elemento principal da oração (mas nem toda oração tem sujeito)
* O “assunto” da oração.
* É a parte da oração sobre a qual se declara alguma coisa.
* Dica: pode ser substituído por “ela” ou “ele”.
* Todo sujeito tem um **núcleo**
  * Palavra mais importante
  * Em geral, um substantivo ou pronome

![1566432644649](Images - 2.1 - Análise Sintática/1566432644649.png)

### Inexistente

* Ex: nevou forte (verbo: nevou. E o Sujeito?non exist)
* Verbo na 3º pessoas do singular.

### Existente

* **Indeterminado**
  * Sabe que existe, mas nao sabe quem é.
  * Ex: roubaram meu carro (quem roubou? Sei la)
  * 3º pessoa do plural (quase sempre).
* **Determinado**
  * *Implícito*
    * *Oculto ou elíptico.*
    * Existe, sabe quem é, mas nao aparece.
    * Ex: sou militar. (quem é militar? Eu)
    * Ex: estamos aqui há muito tempo (quem está? Nós)
  * *Explicito*;
    * Aparece na frase.
    * Simples
      * Só 1 nucleo.
      * ex: ele me deu um presente.
    * Composto
      * +de1 núcleo.
      * Ex: ele e ela me deram um presente.

## Predicado

-   O que se diz sobre o sujeito.

-   O verbo faz parte do predicado.

-   Núcleo → parte principal

### Predicação verbal

-   O núcleo é um **verbo. **

-   Indica ação

-   Ex: o avião caiu no mar (verbo intransitivo)

-   Ex: eu escuto *música* (verbo transitivo direto)

-   Ex: as crianças precisam **de carinho** (verbo transitivo indireto)

### Predicação nominal

-   Seu núcleo é um **substantivo** ou **adjetivo**.

-   Formado por verbo de ligação (ver em *Classes gramaticais)* +
    predicativo do sujeito.

-   Indica estado ou qualidade.

-   Ex: leonardo é infeliz.

-   Ex: nosso herói acabou derrotado.

### Predicação verbo-nominal

-   Possui dois núcleos: um verbo e um nome.
-   Indica ação e estado ao mesmo tempo
-   Ex: joana saiu contente.



## Termos integrantes

### Complemento verbal

* Sofrem a ação de um verbo

* Verbos transitivos → aqueles que precisam de complemento

* O sujeito da oração é o agente

* Ex: o agricultor *cultiva* *a terra* (a terra sofre a ação do verbo
  “cultivar”)

* **Objeto direto**

  * Sem preposição
  * Ex: Algumas pessoas tomam *vinho*.
  * \#\#\#Objeto Direto Preposicionado

* **Objeto indireto**

  * Com preposição

  * Ex: Amélia acredita *em discos voadores.*

  * Ex: A proposta *interessava-lhe* (objeto direto com pronome
    oblíquo)

    ![1566432796564](Images - 2.1 - Análise Sintática/1566432796564.png)

### Complemento nominal

* Completo o sentido de um substantivos, adjetivo e advérbio
* Sempre unidos por uma preposição.
* É passivo → sofre a ação deles
* O sujeito da oração é o agente
* Ex: Camila tem muito amor *à mãe* (a mãe complementa o substantivo
  “amor”)
* Ex: Eu tenho saudades *da Ingrid* (a Ingrid complementa o
  substantivo “saudade”)
* Ex: A mulher tinha necessidade *de medicamentos.*

### Agente da passiva

* Faz a ação sobre o sujeito passivo
* O sujeito da oração é o paciente
* Ex: A criança foi orientada *pelo professor* (quem praticou a ação?
  O professor)

![1566432870598](Images - 2.1 - Análise Sintática/1566432870598.png)

## Termos acessórios

### Aposto

* Esclarece algo
* Ex: Alexandre**,** **presidente do clube,** fez a premiação.
* Ex: Tocaram duas músicas: **um samba e um forró**.

### Vocativo

* Evocar ou chamar alguém
* Termo independente do resto da oração
* Geralmente separado por vírgula
* Ex: Não fale tão alto, *Rita!*
* Ex: A vida, *minha amada*, é feita de escolhas.

### Adjunto adverbial

* Complementa um verbo, advérbio ou adjetivo.
* Indicam intensidade, modo, tempo, dúvida, dúvida, etc.
* Ex: Os doces estavam *muito*** **saborosos (adjunto adverbial de
  intensidade).

### Adjunto adnominal

* Complementa um substantivo
* Determina uma característica permanente.
* *Exemplo*
  * O deputado *conservador* saiu da câmara.
  * Caracteriza diretamente o substantivo: o deputado é, sempre foi
    e sempre será conservador.
* *Exemplo*
  * A equipe *cansada* disputou o campeonato.
  * Caracteriza diretamente o substantivo: a equipe é, sempre foi e
    sempre será cansada.

### Predicativo

* Complementa um substantivo
* Determina uma característica relativa, dependente do verbo.
* **Predicativo do sujeito**
  * O verbo refere-se ao sujeito da oração
  * *Exemplo*
    * O deputado saiu da câmara *conservador*
    * Ele se tornou conservador quando saiu da câmara, ou seja, a
      característica depende do verbo.
  * *Exemplo*
    * A *equipe* disputou *cansada* o campeonato.
    * Já sabe ne?
* **Predicativo do objeto**
  * O verbo refere-se ao objeto (predicado) da oração.
  * Ex: a equipe deixou *cansados* seus *torcedores*.

![img](Images - 2.1 - Análise Sintática/image1.png)

![img](Images - 2.1 - Análise Sintática/image2.png)

# Locuções

## Locução verbal

* forma representada por mais de um verbo na frase, fazendo o papel de um único verbo
* um dos verbos será o principa e o outro o auxiliar
  * O primeiro faz as flexões
  * O segundo – dito principal – está em uma forma nominal (infinitivo, gerúndio ou particípio)
* Exemplos
  * João foi encontrado pelo cachorro. – Particípio
  * Marta veio correndo: o noivo acabara de chegar.
  * Pode ocorrer algo inesperado durante a festa.
  * Quero ver você hoje.

## Locução adjetiva

* Substituir uma palavra adjetiva por um conjunto de outras palavras
* Fluvial → do rio
* Viperino → de serpente
* Onírico → de sonho

## **Locução prepositiva**

* Conjunto de palavras atuando como preposição
* A última palavra é sempre uma preposição
* Ex: abaixo de, acerca de, a fim de, além de, ao invés de, apesar
  de, a respeito de.

## Locução adverbial

* Outras palavras exercendo função de advérbio
* *Lugar* → à esquerda, à direita, de longe, para dentro, por aqui
* *Afirmação* → por certo, sem dúvida
* *Modo* **→** passo a passo, em vão, em geral, frente a frente
* *Tempo* **→** de noite, de vez em quando, à tarde, hoje em dia

## Locução de interjeição

* nossa Senhora!
* Valha-me Deus!
* que pena!

# Sintaxe de concordancia

* Relaciona as flexões de gênero, número e pessoa

## Concordância verbal simples

* Sujeito simples
* Existem muitas exceções, que não serão registradas aqui
* [*http://www.soportugues.com.br/secoes/sint/sint50.php*](http://www.soportugues.com.br/secoes/sint/sint50.php)
* **Caso normal**
  * O verbo se flexiona para concordar com o sujeito
  * O verbo concorda em número e pessoa
  * Ex: *a orquestra tocou* vs *Os pares dançaram*

* **Sujeito com expressão partitiva**
  * Parte de, uma porção de, o grosso de, metade de, etc
  * Verbo pode ficar tanto no plural como no singular
  * *A maioria* dos jornalistas** ***aprovou / aprovaram* a ideia.
  * *Metade dos* candidatos não *apresentou /
    apresentaram*** **nenhuma proposta.
* **Sujeito com expressão de quantidade aproximada**
  * Cerca de, mais de, menos de, perto de, etc
  * O verbo concorda apenas com o substantivo
  * Ex: *Perto de* quinhentos alunos *compareceram* à solenidade.
  * Ex: *Mais de* um atleta *estabeleceu* novo recorde nas últimas
    Olimpíadas.
* **Sujeito é um Interrogativo ou indefinido no singular**
  * Verbo fica no singular
  * Ex: *Qual*** **de nós *é*** **capaz?
  * Ex: *Algum*** **de vós *fez*** **isso
* **Sujeito é um pronome interrogativo ou indefinido plural + “de nos”
  ou “de vos” **
  * Quais, quantos, alguns, poucos, muitos, quaisquer, vários
  * Pode concordar tanto com o primeiro quando com o segundo
  * Ex: Quais** **de nós são / somos** **capazes?
  * Ex: Alguns** **de vós sabiam / sabíeis** **do caso?
  * Ex: Vários** **de nós propuseram / propusemos** **sugestões
    inovadoras.

## Concordância verbal composta

* Sujeito composto
* [*http://www.soportugues.com.br/secoes/sint/sint54.php*](http://www.soportugues.com.br/secoes/sint/sint54.php)
* **Sujeito está antes do verbo**
  * Concorda no plural
  * Pai e filho *conversavam* longamente.
* **Sujeito com pessoas gramaticais diferentes**
  * A primeira pessoa do plural prevalece sobre a segunda pessoa
  * Segunda prevalece sobre a terceira
  * Ex: Teus irmãos, tu e eu** ***tomaremos* a decisão (nós)
  * Ex: Tu e teus irmãos** ***tomareis** ***a decisão (vós)
* **Núcleos sinônimos**
  * Pode ficar no plural ou no singular
  * Ex: Descaso e desprezo *marcam / marca*** **seu comportamento.
* **Núcleos dispostos em gradação**
  * Pode ficar no plural ou concordar com o último
  * Ex: Com você, uma hora, um minuto, um segundo me *satisfazem /
    satisfaz.*

## Concordância nominal

* Relação entre um substantivo e as palavras que o caracterizam
  (artigos, adjetivos, pronomes adjetivos, numerais adjetivos e
  particípios)
* **Caso normal**
  * Concorda em gênero e número
  * As *mãos trêmulas*** **denunciavam o que sentia.
* **Vários substantivos comuns DEPOIS**
  * Concorda com o mais próximo
  * Ex: Encontramos *caídas as roupas* e os prendedores.
  * Ex: Encontramos *caída a roupa* e os prendedores.
  * Ex: Encontramos *caído o prendedor* e a roupa.
* **Vários substantivos comuns ANTES**
  * Concorda com o mais próximo ou no plural
  * Ex: A indústria oferece localização
    e atendimento *perfeito/perfeitos*
* **Vários substantivos próprios ou de parentesco**
  * Fica no plural
  * Ex: As *adoráveis*** **Fernanda e Cláudia vieram me visitar.
  * Ex: Encontrei os *divertidos* primos e primas na festa.
* **É proibido -  É necessário - É bom - É preciso - É permitido**
  * Invariáveis se o substantivo for genérico
    * É *proibido* entrada de crianças.
    * Em certos momentos, é *necessário atenção*.
    * Não é *permitido* saída pelas portas laterais
  * Se estiverem determinadas por artigos, pronomes ou adjetivos,
    concordam com ele
    * São *precisas* várias medidas na educação.
    * É *proibida* a entrada de crianças.
    * A educação é *necessária*.

# Sintaxe de colocação

* Onde enfiar um pronome oblíquo átono

## Próclise

* Antes do verbo
* **Orações com valor negativo**
  * Ninguém *o* apoia.\
    Nunca *se* esqueça de mim.\
    Não *me* fale sobre este assunto
* **Orações com advérbios e pronomes indefinidos SEM pausa**
  * Aqui *se* vive (advérbio)
  * Tudo *me* incomoda nesse lugar (pronome indefinido)
* **Orações iniciadas por pronomes e advérbios interrogativos**
  * Quem *te* convidou para sair? (Pronome interrogativo)
  * Por que a maltrataram? (Advérbio interrogativo)
* **Orações iniciadas por palavras exclamativas e nas optativas (que
  exprimem desejo)**
  * Como *te* admiro! (Oração exclamativa)
    Deus *o* ilumine! (Oração optativa)
* **Conjunções subordinativas**
  * Ela não quis a blusa, *embora lhe* servisse.
  * É necessário *que o* traga de volta.
* **Gerúndio precedido de preposição "em"**
  * *Em se *tratando de negócios, você precisa falar com o gerente.
  * *Em se *pensando em descanso, pensa-se em férias
* **Com a palavra "só" (no sentido de "apenas", "somente") **
  * *Só se* lembram de estudar na véspera das provas
* **Conjunções coordenativas alternativas.**
  * *Ou se* diverte, ou fica em casa
* **Orações introduzidas por pronomes relativos. **
  * Foi aquele colega *quem me* ensinou a matéria.
  * Há pessoas *que nos* tratam com carinho.
  * Aqui é o lugar *onde te* conheci.
* **Proparoxítonas**
  * Nós o criticávamos

## Mesóclise

* Verbo no futuro do presente ou futuro do pretérito indicativo
* Falar-*lhe*-ei a teu respeito. (Falarei + lhe)
* Procurar-me-iam caso precisassem de ajuda. (Procurariam + me)

## Ênclise

* Pronome depois do verbo
* É a colocação “básica”
* **Período iniciado com verbo**
  * Ou seja, não se começa frase com pronome oblíquo
  * Ex: Diga-*me* apenas a verdade.
  * Ex: Importava-*se* com o sucesso do projeto.
* **Oração reduzida no infinitivo**
  * **<http://www.soportugues.com.br/secoes/sint/sint74.php> **

# Sintaxe de regência

* Subordinação entre um verbo e seus complemento

## Regência verbal

* Quem rege é o verbo
* Permitem expressar corretamente o sentido desejado
* Cada verbo tem suas regras
* [*https://www.youtube.com/watch?v=cH65dRyJ\_Ys*](https://www.youtube.com/watch?v=cH65dRyJ_Ys)
* **Exemplo 1**
  * A mãe agrada *o* filho (agradar significa acariciar)
  * A mãe agrada *ao* filho (agradar significa "causar agrado ou
    prazer", satisfazer)
* **Exemplo 2**
  * Cheguei *ao* metrô (destino final)
  * Cheguei *no* metrô (destino intermediário, meio de transporte)
* **Agradecer, perdoar e pagar**
  * Tem umas treta loca com objeto direto e indireto
  * Uso de pronomes oblíquos átonos:
  * Agradeci o presente (Agradeci-*o*)
  * Agradeço *a* você (Agradeço-*lhe*)
  * Perdoei a ofensa (Perdoei-*a*)
  * Perdoei *ao* agressor (Perdoei-*lhe*)
  * Paguei minhas contas (Paguei-*as*)
  * Paguei *aos* meus credores (Paguei-*lhes*)
* **Agradar**
  * <u>Sentido de carinho</u>
    * Verbo transitivo direto
    * Ex: Sempre agrada o filho quando o revê.
  * <u>Sentido de satisfazer</u>
    * Verbo transitivo indireto
    * Ex: O cantor não agradou aos presentes.
    * Ex: O cantor não lhes agradou.
* **Aspirar**
  * <u>Sentido de inalar</u>
    * Transitivo direto
    * Ex: Aspirava o suave aroma. (Aspirava-o.)
  * <u>Sentido de desejar</u>
    * Transitivo indireto
    * Ex: Aspirávamos a melhores condições de vida. (Aspirávamos a
      elas.)
* **Assistir**
  * Sentido de ajudar
    * Transitivo direto
    * Ex: o SUS assiste os cidadãos
  * Sentido de ver
    * Transitivo iniditero
    * Ex: eu assistia à televisão
    * Ex: Eu assisti ao jogo
* **Chamar**
  * <u>Sentido de convocar</u>
    * Transitivo direto
    * Ex: por favor, vá chamar sua prima
  * <u>Sentido de denominar</u>
    * Transitivo indireto
    * Ex: Darwin chamou ao animal de tartaruga-ninja-de-galápagos

## Regência nominal

* Relação entre um nome (substantivo, adjetivo ou advérbio) e os
  termos regidos por ele
* Sempre intermediada por preposição
* Cada um tem sua regra.
* O negócio é tacar o foda-se →
  [*https://www.youtube.com/watch?v=QLOY6s53-5k*](https://www.youtube.com/watch?v=QLOY6s53-5k)
* **Regra geral**
  * Segue a mesma lógica do verbo relacionado
  * Ex: Obedecer *a* algo (verbo) / Obediente *a* algo (adjetivo)
* **Substantivos**
  * ?

* **Adjetivos**
  * ?

* **Advérbios**
  * ?