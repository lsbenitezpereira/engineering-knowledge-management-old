# Palavras soltas
Iporã		→ bien
Ava y		→ agua indígena
Nde ra-kore	→ por tu concha
Uà hē		→ lhegar
Soho /So-ó/ → carne
Karu		→ comer
Kê		→ dormir 
Mburubicha roga	→ casa del jefe (palacio presidencial) 


# Expressões soltas
che ha utá y		→ voy a tomar agua
Ikatu pio há ù nde y	→ puedo pio tomar tu agua
Ya uã hē hína	→ estamos chegando já (neste momento)
Jaha ja caru		→ vamos comer

Che ( yo )
A ha. ( me voy )
Che a ha ( yo me voy )

Che roga 	→ mi casa
Che = eu/meu
Roga = casa

Locuções adverbiais
A karu cé soho	→ queiro comer 
A = conjugação do verbo para a pessoa ‘eu’
Karu cé = verbo comer + verbo querer 

O Guatá ce 	→ ela quer caminhar
Che a guatá cé	→ eu quero caminhar 
O = conjugação do verbo para a pessoa “ela”
A = conjugação do verbo para a pessoa “eu”
Che = eu 
Guatá cé = verbo caminhar + verbo querer 

Añe ñupã	→ bater em sí mesmo
Añe = reflexivo
Ñupã = bater (porrada) 

# anotações culturais
En guaraní no hay 's', entonces algunos tienem problemas en pronunciar-la en espanhol. "Yo subire lá essscalera, ecalon por ecalon"

