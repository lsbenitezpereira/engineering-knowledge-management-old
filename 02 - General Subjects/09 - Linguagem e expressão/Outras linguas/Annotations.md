# Learning tips

* **Spaced Repetition**
  * appropriate distribution of information retrivial 
  * Spaced Repetition System (SRS): Implemented programmatic
  * Leitner system: implemented with papercards
  * Variations: free recall, recognition, cued-recall, and others

![Image result for spaced repetition system](Images - Annotations/Screen-Shot-2013-01-29-at-5.12.38-PM.png)