I started learning in the begining of  2020/12



# Prnounce

* umlaut: two dots; 
* ä is like /ê/

# classes

## Pronoums

* The
  * der: masculine
  * die: feminine
  * das: neuter 
* personal
  * Ich: I
  * du: you singular
  * er: he
  * sie: she
  * es: it
  * wir: we
  * ihr: you plural
  * sie: they
* demonstrative
  * das: that

## Verbs

* To be
  * bin
  * bist
  * ist
  *  
  * sind
  * seid
  * sind

## Pronoums

* plurals
  * +er or +en? 
  * the pronoum is always die

# Greetings

* guten = good (gute, iffeminine)
* Guten morgen: good morning
* Guten tag: good day (10 am)
* Guten Abend: good evening
* Gute Nacht /guté narrt/: good night
* Schalf schön: sleep well
*  
* auf wiedersehen: bye (formal)
* Tschuss: bye (informal)
* bis später /bish spâtã/: see you later

