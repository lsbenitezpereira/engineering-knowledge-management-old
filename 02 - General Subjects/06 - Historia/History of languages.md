# Western?

## Uralic

* finnish belong to the finno-ugric branch, which also includes Estonian and Hungarian

  ![image-20191204104959176](Images - History of languages/image-20191204104959176.png)

## Indoeuropean

* Germanic: include english
* Roman: include portuguease and spanish
* English
  * arose in coastal northwestern Europe
  * was carried from there to England by invading Anglo-Saxons in the
    fifth and sixth centuries A.D.