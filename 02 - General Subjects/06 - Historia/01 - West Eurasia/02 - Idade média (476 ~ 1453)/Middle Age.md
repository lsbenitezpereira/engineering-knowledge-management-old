# Contextualização

### Expansão Islâmica

-   **Surgimento**

	  -   Durante o sec. VI, o oriente médio era povoado por grupos nômades (beduínos).
	  -   Península Arábica → entre a África, Ásia e Europa
	  -   Viviam principalmente de comércio e do pastoreio
	  -   Sec VII: Maomé começa a pregar o Islamismo (ver detalhes em *Religiões e mitologias*)
	  -   Após a morte de Maomé, a aristocracia mercantil começou a expandir o território.

-   Fatores que contribuíram para a expansão

	-   Jihad: guerra santa

	-   Simplicidade da doutrina

	-   Decadência dos impérios persa e bizantino

	-   Cultura de ambição material árabe.

-   **Califas**: chefes religiosos e políticos.

-   O império islâmico conquistou Síria, Palestina, mesopotâmia, Egito,
	Pérsia, Índia, e o caralho A4.

-   771 dC: invadiram a Espanha pelo estreito de Gibraltar.

-   Acabaram se dividindo em califados independentes.

-   ==Sarracenos e mouros==

-   ==Esse território foi recuerado pelos europeus quando?== 

### Queda do Império Romano

-   **Declínio do Imperío**
	  -   A partir do século III
	  -   Declínio da produção e comércio
	  -   Altos gastos públicos
	  -   Pouca mão de obra escrava e muito camponês livre
	  -   Governo corrupto pra caralho
	  -   Crescimento do cristianismo
-   **Intensificação das invasões “barbaras”**
	  -   Bárbaros: qualquer cultura diferente que não falava Latim ou grego. Organizavam-se inicialmente em tribos, vivendo de pesca, saque e um pouco de agricultura.
	  -   Por que invadir Roma?  Invasões Hunas criam um êxodo em direção a Roma, e a Europa proporcionava terras férteis, clima ameno e cidades desenvolvidas.
	  -   Inicialmente (sec. I e II) foi uma absorção pacífica: esses povos ajudavam a proteger o império de outras invasões. barbaros eram aceitos como soldados no exército romano
	  -   Depois foi mais violênto (sec. IV e V), pois esses povos começaram a querer controlar a região, o que gerou conflitos com Roma.
	  -   Os territórios romanos foram **gradualmente conquistados** por diversos povos, dando início a vários reinos independentes.Invadem e destroem Roma.

* 330dC: a capital é transferida para Constantinopla.
* 395dC: Império Romano é dividido em 2 (oriente e ocidente).
* 410dC: Roma é conquistada por tribos germânica. Xau ocidente.
* 476dC: queda do ultimo imperador
* A parte oriental se manteve unida até o 1453 (império Bizantino)

![img](temp1 - Images/image2.png)

### Império Bizantino (339 \~ 1453)

-   Roma se expandiu para o oriente, transferindo a capital para a
	antiga cidade de Bizâncio, que virou capital do império e mudou o
	nome para Constantinopla (atual Istambul).

-   Adoção do Cristianismo.

-   O imperador Teodósio dividiu o império romano em dois: oriental e
	ocidental.

-   Cesaropapismo: igreja era submetida ao estado.

-   Sedição de Nike (523)

	-   Revolta popular por causa dos altos impostos do imperador
	    Justino, para sustentar sua expansão militar.

	-   Geral Belisário suprime a rebelião.

	-   30.000 pessoas mortas no Hipódromo da cidade, onde o caos
	    começou.

-   Corpus Júris Civilis: direito romano, melhorado durante o império
	bizantino.

-   Constantinopla puta centro comercial.

-   Sociedade servil.

-   Em 1453, os ==**turcos otomanos**== conquistaram Constantinopla, pondo
	fim ao império e marcando o fim da Idade Média.
	-   ==Os turcos bla bla bla==

https://www.facebook.com/imperiumtour2015/videos/1180280958649689/?hc\_ref=PAGES\_TIMELINE



# Alta idade média (sec V a IX)
* Período compreendido entre a queda de Roma e a consolidação do Feudalismo

### Povos Europeus (“Bárbaros”)

-   **Germanos**
	-   Povo indo-europeu

	-   Cultura nórdica

	-   Divididos em Visigodos, ostrogodos, vândalos, anglo-saxões e
		francos.
-   **Hunos**

	-   ==Fixaram-se na atual Hungria?==

	-   Especialmente violentos

	-   O “êxodo” causado pela sua agressividade aumentou os ataques a
		Roma.
-   **Francos**

	-   O mais duradouro e estável dos reinos bárbaros

	-   Gália, atual França

	-   Cultura Celta

	-   Os primeiros reis eram de Meroveu → Reis merovíngios

	-   496 dC → um desses reis se converte ao **cristianismo**,
		espalhando essa religião.

	-   Pepino, o breve toma o poder → Reis Carolíngios → Expansionismo.

	-   O auge dessa expansão acontece durante o reinado de Carlos
		Magno, filho de Pepino, fase conhecida como Império Carolíngio.

### Império Carolíngio (768 \~ 814)

-   Auge do Império Franco
-   Também chamado de sacro império romano germânico?
-   Iniciado sob o comando de Carlos Magno
-   Puta expansão militar.
-   Criou-se uma unidade na Europa (o que não havia desde a queda de roma)
-   Não tinha capital fixa → a corte ficava “viajando”.
-   **Reforma cultural**
	-   faz-se seguindo a cultura greco-romana (esse movimento as vezes é até chamado de primeiro renascimento)

	-   Criou escolas (normalmente junto à monastérios), ensinando-se segundo a divisão grega: Aritmética, geometria, astronomia, música, gramática, retórica e dialética.
-   800 dC → Coroado Imperador do Ocidente pelo papa Leão III.
-   814 dC → após sua morte, o império enfraquece.
-   843 dC → território dividido pelo Tratado de Verdun. Fim do
	império.

### Invasões Vikings (793–1066 AD)

-   Or *Vikings age*
-   Exploration of Europe by its seas and rivers by the Norsemen people (Expansionismo natural)
* modern hitoriography also call  those norseman explorers *vikings*
* Between norseman only the traders or raiders where called Viking (it was Like an lifestyle)
* Mainly from where today is Denmark, Norway, and Sweden (Povos Escandinavos)
* 793: viking invaded the English island of Lindisfare
-   Fortaleceu-se com fim do Império Carolíngio, pois a Europa ocidental ficou
    frágil e tensa.
* Expandem a Cultura nórdica
* Ended with the stablichment of an royal critian authority in the Scandinavian countries

![http://4.bp.blogspot.com/-swSfbG2H5ao/VgKH659xoWI/AAAAAAABANU/pA8yWzmvITY/s1600/Invas%25C3%25B5es%2Bs%25C3%25A9cs.%2BVIII%252C%2BIX%2Be%2BX.png](temp1 - Images/image3.png){width="5.28in"
height="5.505384951881015in"}

* Nordic colonization of the extreme North
  * Norse from
    Norway colonized Iceland in A.D. 874, then Norse from Iceland colonized
    Greenland in A.D. 986, and finally Norse from Greenland repeatedly vis-
    ited the northeastern coast of North America between about A.D. 1000
    and 1350

  * The Norse expansion from Norway across the North Atlan-
    tic, with dates or approximate dates when each area was reached: 

    ![image-20191208165757153](Images - temp1/image-20191208165757153.png)

# Baixa idade média (sec IX a XV)

* Caracterizado pelo Feudalismo: Sistema de organização política, social e econômica

* o termo *feudal* deriva *fief* e  *feodum* (origem Celta ou Germânica), designando o direito de usufruir sobre a terra. Ou seja, é não ter a *propriedade* da terra, e sim o seu direito de uso.

* **Razões do seu desenvolvimento**
  * Nasceu na sopa de povos europeus + restos romanos + fragilidade e insegurança

  * A ocupação árabe no mediterrâneo (o que diminuiu o comércio) também facilitou o isolamento das vilas.

* **Características**
  * Poder político descentralizado
  * Existiam moedas, mas escambo era muito comum
  * ==Pequenas guerras entre feudos eram comuns==
  * Cria-se uma unidade cultural, política e religiosa na Europa

- **Direito**
  * O direito era consuetudinário (proveniente da consolidação informal dos costumes), e estava acima do Rei e abaixo de Deus

  * Sem leis escritas, pois os costumes eram orais

* **O papel social da mulher**
   * Com a libertação da influência romana, caem as ideias clássicas (afirmadas pelo direto romano) centradas no *pater familias* (patriarca, chefe da família)

  * Na idade média o pai era um gerente da família, e não o proprietário da vida dos filhos 

  * joana D’Arc: heroína que surge durante a guerra dos 100 anos. Executada em 1431, canonizada em 1920.

* **Tecnologia**

  * Arado puxado por bois.

  * Metalurgia e fabricação de armas

  ![img](temp1 - Images/image1.jpeg)

### Estrutura social

* Estática e hierarquizada
* **Rei**

  *  Um senhor entre outros senhores feudais, mas marcado pela unção santa 

  *  A ajuda militar ao rei é limitada a 40 dias por ano. Como ele não tinha nenhum exercito permanente ou poder económico maior que os outros senhores, o papel do rei uma muito mais o de um árbitro do que o de um fodão 

  *  Esse equilíbrio faz com que casamentos e alianças sejam fundamentais para manter a estabilidade

  *  Não possui nenhuma autoridade soberana: não pode ditar leis, receber impostos de todo o reino ou ter um exército permanente 

  *  In the Middle Ages the authority of the king existed in theory, but in fact it was weak. The greater feudal barons were practically independent

  *  <u>Realeza medieval/feudal vs monarquia</u>
  
    * Até existe uma certa continuidade hereditária, mas isso é apenas para facilitar a aceitação, sem representar uma característica importante
  
    * O termo *monarca* vêm de *monos* (grego): aquele que governa só
  
    * Poder absoluto e centralizado 

* **Nobreza**

  *  *Alto nobreza* → Senhores feudais (dono da terra, suserano), condes, duques, viscondes

  *  *Baixa nobreza* → cavaleiros e outros +ou- nobres

  *  Muitas vezes estava relacionado ao clero, porem estes não formaram uma classe separada.

  *  Se o território era grande, fazia concessões à baixa nobreza

  *  Some nobles possessed a few manors, others possessed several domains, and others possessed a number of fiefs scattered in different places.

* **Clero**

  * Bishops and abbots took their places in the feudal structure much as counts and dukes did

* **Vilões**

  * ex-proprietários romanos que agora eram trabalhadores livres, geralmente recebendo um tratamento mais brando que os outros servos.

*   **Servos**

  * Ou *vassalos*

  * As populações de organizavam em <u>vilas autossuficientes</u>

  * Trabalhavam sob a proteção de um senhor feudal (conveniente, numa época de insegurança).
  * não há nada de parecido entre um servo e um escravo: servo é homem, escravo é objeto

  * Um servo possui estabilidade (pois cultiva sempre aquela terra) e liberdade (dentro de suas obrigações como usufruidor da terra)


### Relações de trabalho

* O servo recebia um pedaço de terra do senhor feudal e trabalhava nela, em troca de proteção e uso das instalações comuns.
* ==Alguns senhores tinham escravos, geralmente para trabalho doméstico==


* **Obrigações dos servos**

  * ==Algumas dessas fontes estão meio duvidosas, as obrigações não eram tão rígidas, mas mantenho esse texto aqui==
  * <u>Corveia:</u> trabalho gratuito na propriedade do senhor feudal, 3 ou 4 vezes por semana
  * <u>Talha:</u> parte da produção que ia pro senhor (geralmente metade)
  * <u>Capitalização:</u> importo per capita
  * <u>Banalidade:</u> pagamento pelo uso de instalações comuns (celeiro, moinho, forno, etc)
  * <u>Taxa de casamento:</u> cobrada quando o servo casava com alguém de fora da vila.
  * <u>Taxa de recasamento:</u> If a widow wanted to remarry, a fine had to be paid to her overlord
  * <u>Prestações:</u> os servos deveriam hospedar o Senhor quando este viajasse.
  * <u>Dizimo:</u> 10% do que ganhava ia pra igreja
  * Just as the heir of
    serfs holding had to pay a tax to the lord of the manor on taking possession
    of his inheritance, so the lord’s heir had to pay an inheritance tax to his
      overlord


* **Propriedade**
* Possession of the land did- not mean that you could do with it
    what you pleased to the extent that you can to-day. Possession implied
    obligations to someone that had to be carried out.
  * The lord of the manor, like the serf, did not
    own the land, but was himself a tenant of another lord higher up in the scale.
    The serf, villein, or freeman “held” his land from the lord of the manor, who
    in turn “held” the land from a count, who in turn “held” the land from a
    duke, who in turn “held” the land from the king.



# Cristianismo durante a Idade Média

-   O império romano declina, mas a igreja Católica se expande pela Europa com
	ajuda dos francos.
-   Mesmo com o fim do Império Carolíngio, a igreja continua forte, graças ao <u>feudalismo</u>.
-   In the early period of feudalism the Church had been a progressive, live
	element. It had preserved a good deal of the culture of the Roman Empire. It
	encouraged learning and set up schools. It aided the poor, took care of
	homeless children in its orphanages, and established hospitals for the sick
-   **Estados Pontifícios**

	-   Criado em 752 dC pelo papa Estêvão III

	-   Era o território do “império papal”, na Península Itálica.

	-   Teve uma puta influencia, mas foi perdendo força com o
		Renascentismo.

	-   Durou até 1870, com a unificação da Itália.

	-   O Vaticano foi fundado em 1929, depois que praticamente todo o
		território papal foi conquistado. O território foi cedido à
		igreja por Mussolini. Hoje, sobra praticamente só ele.
-   **A igreja e a sociedade**
	-   Chegou a ser dono de 2/3 do território europeu.
	-   One reason that priests were forbidden to marry was simply that the
	    heads of the Church did not want to low any Church lands through
	    inheritances to the children of its officers.
	-   Controlava a educação.
	-   A “ética” condenava o livre comércio e a cobrança de juros.
	-   Como só o primogênito herdava a terra, normalmente os outros viravam
		altos membros do clero.
	-   Dogmas → preceitos indiscutíveis da religião.
	-   Heresias → coisas opostas a igreja
-   **Cisma do Oriente (1054)**
	-   O ocidente se “desvirtuava” pelo contato com povos germânico,
		enquanto o oriente permanecia “puro”.
	-   Cesaropapismo: o lado oriental se submetia aos poderes do
		imperador bizantino.
	-   Monofisismo →Jesus era unicamente divino, nada de humano.
	-   Iconoclastia → Oposição à presença de imagens nos rituais
		religiosos, produzia idolatria.
	-   Uma excomungou a outra e a religião dividiu-se em duas:
		-   Igreja Apostólica Romana (ocidente).
		-   Igreja Ortodoxa (oriente)

### Santa Inquisição

* o termo *inquisição* vêm de *inquérito*, pois a acusação era julgada por um tribunal religioso 
* 1215 → criada a Santa Inquisição, no Concílio de Verona.
* 1231 → inquisição pontificial (papa Gregório IX)
* Foi ganhando força e agressividade ao longo do tempo.
* Se exagera bastante: 1 cada 15 acusados eram condenados à fogueira, sendo que os outros sofriam penas mais leves ou eram até inocentados 
* **Sobre o inquérito**
  * O réu não tinha direito de saber *por que* nem *por quem* foi
  
  	acusado.
  * Duas testemunhas já constituíam prova suficiente para
  
  	condenação.
  * As penas iam de advertência à morte na fogueira, passando por
  
  	excomunhão.
* **Inquisição Espanhola (sec XV \~ sec XIX)**
  * Pior período da inquisição
  * Foi uma forma de Fernando de Aragão perseguir seus
  
  	opositores, além de expulsar judeus e muçulmanos.

### Cruzadas (1095 \~ 1270)

-   Pode-se resumir por “cruzadas” todas as expedições militares para
	retomar Jerusalém dos muçulmanos.
-   Fala-se em 8 cruzadas, mas na verdade foi um processo praticamente
	continuo ao longo de 200 anos de guerra.
-   While the
	Roman Church saw in the Crusades an opportunity to extend its power, the
Byzantine Church saw in the Crusades a means of checking the Moslem
	advance on to its own territory
-   Entre as **principais causas**, podemos citar:

	-   Árabes muçulmanos tomam Jerusalém em 638 dC.
-   Turcos, também muçulmanos, tomam Jerusalém e impedem a
		peregrinação dos cristãos à “Terra Santa”, em 1071.
	-   Turcos ameaçam invadir a Europa.
-   Controle do comercio com o oriente;  Italian trading cities looked upon the Crusades as a
	chance for them to gain commercial advantages
-   Baixa produção de alimentos nos feudos.
-   **Cruzada dos mendigos**
    -   ou *do povo.*
-   **1 º cruzada**

	-   Organizada pelo papa Urbano II

	-   Reúnem-se em Constantinopla

	-   Menos de 1/6 da tropa chega a Jerusalém, mas ainda assim
		conquistam a cidade.
-   **2º cruzada**
-   Turcos reconquistam a Terra Santa
	
-   Grupos isolados vão pra Jerusalém.
	
-   Fail.
-   **3º cruzada**
-   “Cruzada dos reis”
	
-   Um dos reis, Ricardo Coração de Leão, faz um tratado com
		Saladino, cedendo um território próximo para os cristãos.
-   **4º cruzada**

	-   Financiada por mercadores de Veneza.

	-   Atacam a saqueiam Constantinopla.

	-   Fodase a Terra Santa.
-   5º cruzada
-   Cruzada das crianças
-   6º cruzada
-   7º cruzada
-   8º cruzada
-   Nesse período, foram formadas várias ordens de cavaleiros, como os
	Templários e Hospitalares.
-   **Consequências**

	-   Proporcionou um **intercâmbio cultural**, mas também **aumentou
		a intolerância** religiosa.
-   Aumento do **comércio** (beneficiando principalmente a Itália)
	-   From
    the point of view of religion the results of the Crusades were short-lived,
	    since the Moslems eventually took back the kingdom of Jerusalem. From the
    point of view of trade, however, the results of the Crusades were
	    tremendously important
	-   Enfraquecimento da aristocracia feudal.
	-   Fortalecimento da burguesia

![http://1.bp.blogspot.com/-Rwj4HqJaXWA/UgjFhrv5M8I/AAAAAAAABVg/UcZEpgxruig/s1600/cruzadas+mapa.png](temp1 - Images/image1.png){width="7.268055555555556in"
height="4.647787620297462in"}



# Fim da idade média (sec XIV e XV)

* período de muitas transformações que marcam a passagem do feudalismo para o capitalismo
* Guerra, fome e epidemias
* A grande fome (1315~1317)
	* Causada principalmente por uma mudança climática: frio e chuva fudeu as colheitas por muitos anos 

### Guerra dos Cem Anos

* 1337, França

* O rei francês morre e começa uma disputa pelo trono.
* Ingleses reivindicam o trono
* Burgueses apoiam Inglaterra (por causa das relações comerciais, principalmente em Flandres).

* Nobreza francesa apoia um sucessor francês.
* Porrada de burgueses em Flandres, ingleses mandam tropas, França declara guerra.

* Revoltas rurais jacqueries.
* 1415 França se fode e desiste do trono (Tratado de Tryes)
* 1428 Surge a figura de Joana D´Arc, enviada por deus para libertar a França.

* Mesmo depois de morrer na fogueira (1431), inflama a nação e o pais consegue se libertar.

* A guerra fortaleceu a monarquia e o nacionalismo.
* Mas também trouxe fome, revoltas e pestes.

### Peste Negra (1347~1348)

* Também chamada de *peste bubônica*
* Fome e falta de higiene.
* Veio do oriente, provavelmente China, em navios mercantes.
* 25 milhões de mortos.

![img](images/media/image1.jpeg){width="6.4618022747156605in"
height="4.245283245844269in"}

### Aumento do comercio

* Após as Cruzadas, o comércio cresceu muito na Europa, principalmente na Itália

* Expansão da atividade bancaria.

* A terra deixa de ser a única fonte de riqueza → Nobreza enfraquece e burguesia cresce.

* **Very begining**

  * When that demand is absent there is no stimulus for the production of a surplus. Therefore trading at the weekly markets was never very large and always local
  *  The markets were small dealing with local goods, most of them
    agricultural. The fairs, on the other hand, were huge, dealing with wholesale
    goods that came from all over the known world.

* **Feiras comerciais**

  * Atraiam mercadores do toda Europa.

  * Feira de Champagne

  * Feira de Flandres
	
* **Hansas, guildas e Corporações de Ofícios**

  * Associações de comerciantes
  * Controle e monopólio
  * “Máfia”
  * faced with feudal restrictions that cramped
    their style, traders joined together into associations called “gilds,” or
    “hanses,” to win for their towns the freedom necessary to their continued
    expansion
	* In the struggle for town freedom the merchants took the leadership. They
	  were the most powerful group in the towns and they won for their gilds or
	  hanses all kinds of privileges. Merchant gilds often exercised a monopoly
	  over the wholesale Commerce of the towns
	* unemployment insurance and old-age pensions so much in the newt to-day were provided by some craft gilds
	* The gilds had a
	  thousand and one rules and regulations for the prevention of bad work and
	  for the maintenance of a high-quality standard, and violations of these rules
	  brought severe penalties
	*  For the further protection of the public some gilds
	  stamped their products with their “just price.” As trade extended, the conditions affecting the
	  market were much more variable and just price was no longer practical. It
	  gave way in the end to market price
	* <u>Liga Hanseática</u>
	  * Norte da atual Alemanha
	  * A reunião de várias hansas
	  *  over one hundred towns, that it
	    practically monopolised the trade of northern Europe with the rest of the
	    world
	
* **Crescimento das cidades**

   * Os burgos eram núcleos urbanos, formado principalmente por servos fugindo da exploração senhorial.
  * O termo Burgo vem do latim Burgus: Fortaleza, pois geralmente a cidade era muralhada.
  * down to the beginning-of the twelfth century the word “mercator,”
    meaning merchant, and “burgensis,” meaning one who lives in the town,
    were used interchangeably
  * Aumento do artesanato e manufatura.
  * The crafts were made by professional craftsmen who owned both
    the raw material and the tools with which they worked, and sold the finished
    product.
  * <u>aumento da pordução agricola</u>
    * at this time only about one-half
      the land of France, one-third the land of Germany, and one-fifth the land of
      England were under cultivation
    * the European pioneers drained the swamps, built
      dykes against the theft of Und by the sea, cleared the forests, and turned the
      lands thus reclaimed into fields of growing grain.
    * by 1350 in Silesia there were 1,500 new settlements farmed by 150,000 to 200,000 colonists
    * The market had grown so that any crops
      beyond what he needed and the lord took could be sold

* **People emancipation**

  * large numbers of serfs, in
    addition to buying the freedom of their land from the obligation of labour
    services, also bought their personal freedom
  * To the lord, was just as good to let the serf pay a rent instead of giving labour
    services as of old
  * The chief opponent of emancipation in both
    town and country was not the nobility, but the Church.

  * <u>Peasant Revolts</u>
    * about… ==1350?==
    * In a series of uprisings alt over Western Europe, the peasants used
      that power in an attempt to gain by force what concessions they could not
      win—or keep—any other way.
    * the landlords wanted to force the peasants to go back to the labour services of the past
    * the landlords refused to grant commutation in this period when the peasant sensed his power and struck out for it
  * <u>more and more freedom</u>
    * arose a new conception of
      landed property. Large numbers of peasants were free to move about, and to
      sell or bequeath their land, although they had to make a certain payment for
      doing so

* **Queda de Constantinopla (1453)**

  * Fechando a rota de comercio terrestre por Constantinopla, torna-se preciso buscar novas rotas comerciais.
  
  * Início das grandes navegações
  
* **Fortalecimento da burguesia**

  * One of the most important effects of the increase in trade was the
    growth of towns.
  * A burguesia surge ainda durante o período feudal, assim como as cidades, devido ao aumento do comércio
  * A expansão da burguesia e sua competitividade comercial enfraquece a autonomia da cidade, fortalecendo o poder central 
  * o sistema feudal já não é adequado para a realidade da burgesia, pois essa se beneficia de um sistema centralizado na medida em pequenos líderes locais eram entraves para o comércio( já que cada um colocava suas taxas e regulamentações)
  * Um sistema centralizado tem mais poder para investir em grandes empreendimentos (como as grandes navegações)
  * Dessa forma,  a expansão da burguesia foi uma das causas do fim do feudalismo e a ascensão da monarquia
  * The townsman might suddenly need some ready
    cash in a business enterprise, and he liked to think that he could mortgage or
    sell his property in order to get it, without asking permission from a series of
    overlords. That same charter of Lorris dealt with this in so many words:
    “Any burgher who wishes to sell his property shall have the privilege of
    doing so.”
  
* **Early industrialization**

  * Two fundamental characteristics of
    the gild system had been the equality between masters, and the ease with
    which workers could become masters. In general this prevailed until the
    thirteenth and fourteenth centuries
  *  “Greater” and “lesser” gilds made their appearance
  *  It became increasingly difficult to rise from worker to boss.
  * As trade extended, the conditions affecting the
    market were much more variable and just price was no longer practical. It
    gave way in the end to market price.
  * The guilds fought to retain their old monopolies. But the heyday of the gilds was
    over. They were fighting a losing battle. The expansion of the market had
    made their system antiquated, unable to cope with the increasing demand for
    goods
  * <u>more revoults</u>
    * The discontent of the poor allied with (he resentment and
      jealousy of the small crafts toward these powerful rulers gave rise to a series
      of uprisings in the latter half of the fourteenth century, which, like the
      Peasant Revolts, swept over western Europe. It was a class struggle—the
      poor versus the rich, the unprivileged versus the privileged
    * After this period of disorder the gilds entered their declining years. The
      power of the free towns was weakened. Once again they were controlled
      from without—this time by a stronger duke or prince or king than any they
      had known before—one who was welding together the unorganised sections
      into a national state.

# Formação dos Estados Nacionais

* Transição de reis feudais para monarcas absolutistas, de reinos para estados nascionais
* A “concha de retalhos” de feudos e pequenos reinos começa a se unir em torno de um poder centralizado mais forte (rei)
* Centralização das forças armadas, da justiça e a administração nacional.
* Fortalecimento das tradições e do nacionalismo.
* **rise of middle class**
  * The rise of the middle classes is the important development of this period
    from the tenth to the fifteenth century.
  * Confusion and insecurity are bad for business. The middle class wanted
    order and security.
  * It was the presence of different
    overlords in different places along the high ways of business that made trade
    so difficult. What was needed was a central authority, a national state
  * The national state came out on top because the advantages offered
    by a strong central government and by a wider field for economic activities
    were hi the interests of the middle classes as a whole.
* **war became more expensive**
  * Gunpowder and cannon were coming in and effective use
    of these arms required trained cooperation. And while a feudal warrior could
    bring his own armour, he couldn’t easily bring cannon and powder.
  * Formerly the income of the sovereign had consisted of the revenues from
    his own domains. There was no national system of taxation. In 1439, in
    France, the king was able to introduce the tailb, a regular money tax.

### Primeiras unificações (1385)

* But by the end of the Middle Ages, along about the fifteenth century,  Nations come into being; national divisions become marked;
  national literatures spring up; national roles for industry take the place of
  local regulations; national laws, national tongues, even national Churches
  come into existence.

* **Portugal**

  * Primeiro a unificar-se

  * Reconquista da península ibérica + Revolução de Avis (crise pela sucessão, 1385)

* **França**

  * Fim da guerra dos 100 anos (1337 ~ 1453)

  * O rei francês morreu e o trono era disputado

  * O rei inglês reivindicava o trono

  * Além disso, a região de Flandres tinha relações econômicas com a Inglaterra.

  * Começa a porrada.

  * 1428 → Joana D´Arc → “libertadora da França”

  * França vence a guerra

  * Monarquia sai fortalecida

  * Porém deixou a França fudida de fome, peste e revoltas.

  * Guilhotinados pela Revolução Francesa

* **Inglaterra**

  * Guerra das duas Rosas (1455)

  * Vigorou o sistema de monarquia parlamentarista, na qual o poder do soberano era limitado (ele não podia suspender leis, cobrar impostos sem autorização do parlamento, etc)

* **Espanha**

  * Guerras de reconquista

  * Expulsão dos judeus (árabes e mouros)

  * União de Isabel de Castela e Fernando de Aragão (1469)


### Grandes Navegações (1490)

*  this period of history called the “Commercial
  Revolution.”
* Burguesia com dinheiro + estado com poder
* At this time, when gold and silver were so necessary to the further
  expansion of trade, the expansion of trade itself led to the discovery of huge
  stores of these metals, which in turn led to a still further expansion of trade.
* **Portugal se lançou a mar primeiro**
  * Unificação política rápida
  * Burguesia com espírito “empreendedor” ==(influência árabe?)==
  * Já tinham experiência náutica (pesca)
  * Posição geográfica favorável
  * Sabendo que não conseguiria contornar a terra, Portugal decidiu colonizar a Costa da Africa para chegar às Indias
  * 1341:  as ilhas Canárias (já conhecidas dos genoveses) foram oficialmente descobertas por Portugal
  * 1415: Ceuta foi ocupada pelos portugueses 
  * 1455: Papa declara que as terras e mares descobertos além do Cabo Bojador pertencerão à Portugal

![Image result for Cabo Bojador](temp1 - Images/Cabo do Bojador.gif)

* **Depois Espanha**
  
  * Unificação dos reinos de Aragão e Castela
  * Fato que se deu após o casamento dos reis católicos Fernando e Isabel.
  * A descoberta da américa se deu no mesmo ano da expulsão dos mouros e judeus
* **O que buscavam no Oriente?**
  
  * Açúcar e condimentos (pimenta, cravo)
  * Ouro, porcelanas, pedras preciosas
  * Drogas medicinais (bálsamos, ungüentos)
  * Perfumes e óleos aromáticos.
* 1492: Cristóvão Colombo chega na América (sob a coroa espanhola); Columbus’s voyage west
was only one of a number of similar voyages
* 1494: Tratado de Tordesilhas (divide o mundo entre Portugal e Espanha)
*  1497, Vasco da Gama, on this south-east passage, rounded the continent
  of Africa
* 1600: Chegada no brasil da esquadra de Pedro Alvares Cabral
* 1606: alguem chega a australia
* As late as 1609 Henry Hudson was still searching
  for a way to the East.
* It no longer mattered that the old route to the East had been captured
  by the Turks; it no longer mattered that the Venetians charged exorbitant
  prices; the route to the East via the Cape of Good Hope made the merchants
  independent of Turkish goodwill and broke the Venetian monopoly.
* **Tipos de colonização**
  
  * Colônia de povoamento (ex: EUA) e colônia de exploração (ex: brasil)
  * Sobre a colonização Espanhola, ver artigo específico 
### A reforma protestante (1517)
* Enfraqueceu o poder da Igreja Catolica e fortaleceu os Estados
* ==Talvez seja melhor colocar isso junto com a parte religiosa, mas tem uma importância política e social que justifica ficar aqui==
* **social and political seeds**
  * The Church was a political rival of the sovereign: the chuch could interfere even in the internal national affairs of a country.
  * The earlier religious reformers, unlike Luther, Calvin, and Knox, made
    the mistake of trying to reform more than religion
  * This new group of people, the rising middle class, sensed that standing in
    the way of its further development was the out-of-date feudal system. The
    rising middle class realised that its own further progress was blocked by the
    Catholic Church, which was the stronghold of that system.
* **Críticas à Igreja Católica**
   * Durante a idade média, a igreja ganhou uma puta força, mas acabou se distanciando dos ensinamentos originais
  * A Igreja condenava a acumulação de capitais → contradição com o que ela mesma fazia
  * Condenava as práticas da burguesia, que precisava de uma nova religião  
  * Venda de indulgência → perdão dos pecados
  * Deixou de tratar apenas de questões espirituais para ser uma instituição política
* **Matinho Lutero**
  * Publicou em 1517, na Alemanha, suas 95 teses
  * Teve grande apoio dos reis e nobres
  * Diversos nobres aproveitaram o enfraquecimento da igreja e atacaram/roubaram seus bens e propriedades.
* **Contrarreforma**
  * Reação da igreja
  * Catequização dos índios → jesuítas
  * Retomada da inquisição
  * Criação do Index
  	* *Index Librorum Prohibitorum*
  	* Lista de livros proibidos.
* **Variações**
  * França → calvinismo
  * Inglaterra → reforma anglicana
* **Reforma protestante e o capitalismo**
  * Relação estudada por Max Weber
  * Os protestantes valorizavam a autodisciplina, deve para com o trabalho e acúmulo de riquezas.
  * Trabalhar duro e ter sucesso era uma forma de mostrar devoção à vontade divina.
  * Calvinismo era a vertente mais capitalista

### Unificações tardias
* ==é anacrônico registrar isso aqui, mas fica contextualizado==
* It was no small achievement to curb the monopolistic power of mighty
    cities. Where the cities had been strongest, in Germany, and Italy, it was not
    until centuries later that a central authority arose powerful enough to make
    them submit. This was one of the reasons why these mightiest and wealthiest
    communities of the Middle Ages were the last to attain that unification
    which was necessary to cope with changing economic conditions
* **Itália**
    * 1861
    * Conde de Cavour e Giuseppe Garibaldi
* **Alemanha**
  * 1871
  * Otto Von Bismarck

## Others

*  The figures for the
  number of beggars in the sixteenth and seventeenth centuries are astounding.
  One-fourth of the population of Paris in the 1630’s were beggars, and in the
  country districts their number was as great;
* One of the reasons was the higg price of things, related to the unprecedented influx of silver into Europe.  a debased coinage lowers the value of money, or,
  looking at it from another angle, raises prices. The increase in the amount of
  money in circulation has the same effect.