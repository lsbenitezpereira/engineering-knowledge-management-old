

# Nordic and east europe history

## Russia

* **Initial occupations**
  * the Slavic tribes		

  * establishment of the Rus' state in the north in 862 ruled by Vikin

  * name 
    * Rus - ia = land of Rus (Latin) 

    * Rus = Vikings who came here

    * curiosity: Baltic countries give different names to Russia, related to the Slavic tribes with witch they bordered 

* **Centralized state**
  * adopted Christianity from the Byzantine Empire in 988

  * disintegrated as a state due to the Mongol invasions in 1237–1240

  * returned as and great duchy centralized in Moscow
* **Tsardom** 
  * 1547
  * Benin with Ivan the Terrible
* **Russian empire**
  * 1721
  * Peter the great
  * the state is reformed to the Europeans model
  * founded St petersburg, to europenize the state

## Estonia

* **middle age**
  * The Hanseatic League controlled trade on the Baltic Sea, and Estonia was in 
* **Russia-Sweden-Russia**
  * 1558, Russia occupy Estonia (Tsar Ivan the Terrible)
  * Sweden react, and wins in 1583
  * 1600, Polish-Swedish War. Sweden gaining Livonia
  * 1700, Great Northern War. conquered by the Russian Empire
* **Independence**
  * 1919, Estonian Constituent Assembly 
  * 2 years of war to that
* **Russia occupation**
  * 1940, Estonia was annexed by the Soviet Union
  * Germany occupy , not gently
  * Germany loses. Soviet occupation
* **re independence**
  * 1991
  * political parties
    *  Popular Front of Estonia. moderate wing in the independence movement
    *  Estonian National Independence Party, more radical independence

## Norway

* 1914 (napoleotic war) til 1905 (independence): part of Swede
* **Independence**
  * Managed to get full independence in 1905. 
  * By referendum , choose monarchy and elect an danish prince as king

* **ww1**
  * Neutral in ww1

* **Ww2**
  * Invaded by Germany in April 1940
  * beyond the strategic location, the Nazi thought of Norwegians as "asleep aryan"
  * 62 days of campaing
  * Norwegian strategic position: cut off Russian-UK communications

## Denmark

* Copenhagen (/coupenheigen/) = Emergent harbor
* Was founded Just after Viking age
* Denmark and Norway were against hanseatic league 
* Sweden don't want alliance, so they had 300years of wars between them 
* Ww2: Germany just invaded, Denmark want able to do much

## Finland

* tai *Suomen historia*
* **Pre history**
  * unabitated until 10.000 BCE (end of the last ice age)
  * First ceramic potter: 5200 BCE
  * Start of agriculture: 3000 BCE
  * Broze age: 1500 BCE
  * Pre roman iron age: 500 BCE
  * During the Viking Age, those regions were inhabitated by Finnic Tribes
* **Part of the kingdom of Sweden**

  * Gradually became part of Sweden during the 13th century, through the forced expantion of the Catholic Church (series of wars)
  * arround 1240: Second Swedish Crusade, led by Birger Jarl
  * the term *Österland* (Eastland) was used to describe the area until the 15th, when it was replaced by Suomi (Finland)
  * Swedish was the nobility and administration language 
  * Main defesive fortress: Turku, Hämeenlinna, and Viipuri
  * 1590: helsinki is stabished, but it remained a mere fishing village for over two centuries
  * 1591: translation of the New Testament in Finnish
  * 1640: first university in Finland, Åbo (later moved to Helsinki, after and fire)
  * The Large Wrath (1713~1721): russian occupation of finland (during Great Northen War)
  * The Lesser Wrath (1741~1743): russian occupation again
* **The Finnish War (1808~1809)**
  * From February 1808 to September 1809
  * The nobility wanted to join Russia/leave Sweden 
  * 1806: incorporated to the Russian Empire, as an independent region 
* **19th century urbanization**

  * The population in Finland grew rapidly, and the country became urbanized
  * In 1841 the Finnish language became a subject in schools
  * The Finns culture and indentity become stronger, and wanted more independence

  * 1906: adopted the Universal Suffrage
* **Independence (1917)**

  * 1905: after revoutionary movement, Russia limits the Finnish autonomy
  * 1907: first parlamentary elections in finland (with universal sufrage)
  * 1908: Russia increase the repression
  * FInland can’t have an army, but hey begin to organize (the so called *jaegers*)
  * 1917: declared it’s independence (after the Russian Revolution)
  * A civil wars followed that
    * was formed the Civil Guard Organization (government, pro independence). They keep acting after the war as an independent mainteiner of the finnish soberanity (+-40k in 1918)
    * Red guard: pro russia (+-30k in 1918)
    * 
  * After a brief but bitter civil war between Social Democrats and the non-Socialist Parliament, won by the latter, Finland was announced a democratic republic
  * Becomes an Presidential Republic 
* **Second World War**
  * Winter war (1939)
    * russia invaded finland
    * 4 moths later, peace is signed and Finand losted some territories 
    * 25k finnish causalities, 165k russian causualities 
  * Continuation was (1941~1944)
    * again agaist Russia, “togeder” with Germany
    * After lose the war, had to pay a lot of reparations
    * lost some areas in the Finnish Karelia
    * Germans helped on the north (220k german soldiers in the north)
    * In the end of the war, Finns atacked the German that were in the north (Lappish war). Germans burnt everything while retreating (scorched earth technique), including Rovaniemi
  * Years after war
    * Called *year of danger*, because of fear of becoming like the easter europe
    * 1948: agreement of mutual assistence with URSS (but just in case that Finland was directly affected) 

## Sweden

* **Swedish state consolidation**
  * from the 6th to 16th century
  * Kalmar Union (1397 to 1523)
    * joined under a single monarch of the three kingdoms of Denmark, Sweden (then including most of Finland), and Norway,
    * there were several short interruptions
    * Each sub kingdom remained quite independent
    * 1523: Gustav Vasa was electing king in sweden
* **Colonies**
  * 1630s: New Sweden (america), Swedish Gold Coast (Ghana)
  * 1784-1878: Saint Barthélemy (caribe). Bought and then sold
  * Slaves were freed in 1847
* **Napoleonic war**
  * Denmark-Norway on France side
  * Sweden on Russian side
  * Swedish–Norwegian War: Battles of 1814
  * France (and norway) lose
  * Norway becomes part of sweden (denmark remain independent). Norway had a lot of freedom, with even an own constitution)
* **Great Northern War (1700–1721)**
  * Russian (alied with Denmark-Norway and Polish–Lithuanian) vs Sweden
  * Russia conquered Estonia, Livonia, Ingria, and Karelia
* **WW2**
  * Neutral, with slight cooperation with German (much like Norway, but without being invaded)
* **Cold war**
  * Neutral, with slight cooperation with USA
* **Swedish Social Democratic Party**
  * Built the welfare-state of modern Sweden
  * In power 1932-1976 and 1982-1986
  * left-wing
  * Olof Palme
    *  Fucking popular, even today
    *  1969-1986: led the Party
    *  Prime minister:  1969-1976, 1982-1986
    *  killed in 1986
