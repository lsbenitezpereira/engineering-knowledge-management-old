# Matrizes

-   Tabela de números organizados em linhas e colunas
-   Estrutura de dados (do ponto de vista da Ciência da Computação)
-   **Fila** → linha ou coluna
-   **Ordem** → tamanho → nº de linhas x nº de colunas

![](./Images/&01.0 - Classificações/media/image1.png){width="4.4527777777777775in"
height="2.377083333333333in"}

-   **Identificação da matriz**

    -   Toda matriz tem um nome

    -   Geralmente é uma letra maiúscula

$$
A_{2x2} = A_{2} = \left( a_{\text{ij}} \right);i,j \in N
$$

-   **Identificação das linhas**

    -   Seja $A_m$, $L_i$ é a i-ésima linha, sendo $1 \leq i \leq m$

-   **Representação da matriz**

    -   Colchetes, parentes ou barra-dupla

    -   Os elementos são identificados pela mesma letra minúscula da
        identificação

$A = \begin{bmatrix}
a_{11} & a_{1j} \\
a_{i1} & a_{\text{ij}} \\
\end{bmatrix} = \begin{pmatrix}
a_{11} & a_{1j} \\
a_{i1} & a_{\text{ij}} \\
\end{pmatrix} = \left\| \begin{matrix}
a_{11} & a_{1j} \\
a_{i1} & a_{\text{ij}} \\
\end{matrix} \right\|$

-   **Matriz identidade**

    -   Todos os elementos da diagonal principal são 1

    -   Elemento neutro da multiplicação

        ![Resultado de imagem para matriz identidade](./Images/&01.0 - Classificações/media/image2.jpeg)

## Classificação quanto ao formato

-   **Matriz quadrada**

    -   Número de linhas = número de colunas

    -   Ex: $A_{3 \times 3}$ → matriz de ordem 3

![Resultado de imagem para matrizquadrada](./Images/&01.0 - Classificações/media/image3.jpeg)

-   **Matriz linha**

    -   Só uma linha

![Resultado de imagem para matrizlinha](./Images/&01.0 - Classificações/media/image4.png)

-   **Matriz coluna**

    -   Só uma coluna

## Classificação quanto à diagonal principal

-   Classificações válidas somente para matrizes quadradas

-   **Triangular superior**

    -   Todos os elementos abaixo da diagonal principal são nulos.

    -   $a_{\text{ij}} = 0 \text{  if } i > j$

![Resultado deimagem](./Images/&01.0 - Classificações/media/image6.jpeg)

-   **Triangular inferior**

    -   Todos os elementos acima da diagonal principal são nulos.

    -   $a_{\text{ij}} = 0\text{  if } i < j$

![Resultado deimagem](./Images/&01.0 - Classificações/media/image7.jpeg)

-   **Matriz diagonal**

    -   Matriz quadrada em que todos os elementos que estão fora da
        diagonal principal são nulos.

    -   Triangular superior e inferior ao mesmo tempo

    -   Traço → soma dos elementos dessa diagonal

    -   Matriz identidade → tipo especial de matriz diagonal (detalhes
        mais à frente)

$\begin{bmatrix}
\sqrt{2} & 0 & 0 \\
0 & - 9 & 0 \\
0 & 0 & 1 \\
\end{bmatrix}\begin{bmatrix}
8 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 2 & 0 \\
0 & 0 & 0 & 1 \\
\end{bmatrix}$

## Classificação quanto à disposição dos elementos

-   **Matriz transposta**

    -   Inverte a coluna com a linha de uma matriz

    -   A nova matriz será representada por $M^t$

![Resultado de imagem para matriz
transposta](./Images/&01.0 - Classificações/media/image8.jpeg) 

-   **Matriz simétrica**

    -   Sua transposta será igual à matriz original

-   **Matriz oposta**

    -   Todos os elementos multiplicados, individualmente, por (-1)

    -   A nova matriz será representada por "-M"

![](./Images/&01.0 - Classificações/media/image10.png)

-   **Matriz antissimétrica**

    -   A oposta e a transposta são iguais

        ![](./Images/&01.0 - Classificações/media/image11.png)

## Outras matrizes especiais 

* **Idempotente**

  * O quadrado da matriz é igual a ela mesma

  * $M^{2} = M$

  * A identidade é a única matriz idempotente com determinante diferente de zero

* **Matriz nula**

  * Todos os elementos são 0

  * Elemento neutro da soma

  ![Resultado de imagem para matriznula](./Images/&01.0 - Classificações/media/image12.jpeg)

* **Matrizes semelhantes**

  * Sejam A e B matrizes $n \times n$

  * A matriz A é semelhante à B se, e somente se, existir uma matriz P inversível que:

  $A = P \ast B \ast P^{- 1}$

  * Para entender a interpretação disso, veja *Autovetores -- Álgebra Linear*

  * Propriedades válidas:

  ![](./Images/&01.0 - Classificações/media/image13.png)

> ==A reciproca da propriedade 3 é válida?==

* **banded matrix**
  * has all elements equal to zero, with the exception of a band centered
  on the main diagonal:
  
  * Bandwidth: size of the band
  
  * Tridiagonal matrix: banded matrix with banwidth 3
  
    ![image-20200318203512147](Images - 1.0 - Matrizes/image-20200318203512147.png)
> Bandwidth 3

* ?


## Operations
* **Adição e subtração**

  * As duas têm que ter a mesma ordem

  * Opera elemento com elemento

![Resultado de imagem para adição deatrizes](./Images/&01.1 - Operações/media/image1.png)

* **Multiplicação por escalar**

  * Pode ter qualquer ordem

  * Multiplica cada elemento pelo número

![](./Images/&01.1 - Operações/media/image2.png)

* **Multiplicação de matrizes**

  * Condição básica: número de colunas da primeira matriz TEM que se
    igual ao número de linhas da segunda.

  * A matriz C=A*B será dada por:
    

  ![image-20200315003057269](Images - 1.0 - Matrizes/image-20200315003057269.png)

  * Matriz resultando: nº de linhas da primeira e o nº de colunas da
    segunda

  ![image-20200318214140353](Images - 1.0 - Matrizes/image-20200318214140353.png)

  * A.B é, normalmente, diferente de B.A
  * Quando forem iguais, é uma matriz comutável
  * Multiplica linha por coluna, cada elemento individualmente e depois somando tudo

  ![](./Images/&01.1 - Operações/media/image4.png)

  ![](./Images/&01.1 - Operações/media/image5.png)

* **Divisão de matrizes**

  * INDETERMINADO

  * Porém, para problemas no tipo $A*X = B$, é possível fazer o
    seguinte:

  ![](./Images/&01.1 - Operações/media/image6.png)

  * Não sabe o que é matriz inversa? Ver artigo específico

* **Propriedades operatórias**

  * Se A, B e C são matrizes quadradas de mesma ordem

  * ${(A\  + \ B)}^{t}\  = \ A^{t}\  + \ B^{t}$

  * $\left( \text{A.B} \right)^{t} = B^{T}.A^{T}$

  * ${(A^{t})}^{t} = \ A$

  * $(A\ –\ B)C\  = \ AC\ –\ BC$
## Transformações elementares

-   Operações dentro da matriz

-   Alteram seu determinante de forma conhecida

-   Se realizadas em um sistema linear (nos coeficientes e nos termos
    independentes simultaneamente), não alteram a solução

-   Representadas por "e"

-   Toda matriz quadrada e invertível pode ser transformada na
    identidade através de transformações
    elementares$(A_{n}*e*e^{'}*e^{''} = I_{n})$

-   Cada tipo de transformação será especificado mais à frente

-   **Reversibilidade**

    -   Toda transformação elementar é reversível

    -   Sempre pode-se voltar à matriz original aplicando outra transformação

$$e`\left( e\left( A \right) \right) = A$$

-   Se "e\`" reverte "e", então "e" também reverte "e'"

$$e\left( e`\left( A \right) \right) = A$$

-   **Matriz Elementar**

    -   definem uma transformação como uma matrix (ou melhor, como uma pré multiplicação por essa matriz)

    -   Obtida a partir de transformações elementares na Identidade
    
-   Quando pre multiplicam uma matriz, realizam nela a mesma
        transformação elementar que as originou.
    
-   Toda matriz elementar é invertível (afinal, pode ser
        transformada na identidade quando multiplicada por "algo")
    
-   Toda matriz quadrada e invertível pode ser representada por um
        produto de matrizes elementares $(A_{n} = I_{n}*e*e^{'}*e^{''})$
    
    
    ​    
    -   *Exemplo: Matriz elementar relativa à transformação de "permutar
        L1 por L2":*

$$E_{3x3} = e*I_{3x3} = \begin{bmatrix}
0 & 1 & 0 \\
1 & 0 & 0 \\
0 & 0 & 1 \\
\end{bmatrix}$$

-   **Matrizes equivalentes**

    -   A e B são equivalentes se uma pode ser transformada na outra por
        meio de um número finito de transformações elementares.

    -   Se sabemos que duas matrizes são equivalentes, realizar
        operações elementares nas duas simultaneamente não altera a
        equivalência.

    -   P.S.: ambas devem ter (obviamente) a mesma ordem.

### Tipos de Transformações Elementares

-   **Permutar linhas**

    -   $Li \leftrightarrow Lj$

    -   Multiplica o determinante por (-1)

    -   *Matriz elementar relativa à transformação de*
        $L1\  \leftrightarrow L2$:

$$\left| \begin{matrix}
0 & 1 & 0 \\
1 & 0 & 0 \\
0 & 0 & 1 \\
\end{matrix} \right|$$

* **==Permutar colunas==**: pós multiplica pela matriz de transformação

-   **Substituir uma linha por sua soma com outra**
-   $Li \rightarrow Li + Lj$
    
-   Não altera o determinante
    
-   *Matriz elementar relativa à transformação de* $L1 = L1 + L2$:

$$\left| \begin{matrix}
1 & 1 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1 \\
\end{matrix} \right|$$

-   **Multiplicar linha por escalar real**

    -   $Li \rightarrow k.Li$

    -   Multiplica o determinante por k

    -   *Matriz elementar relativa à transformação de* $L2 = 2*L2$:

$$\left| \begin{matrix}
1 & 0 & 0 \\
0 & 2 & 0 \\
0 & 0 & 1 \\
\end{matrix} \right|$$

# Determinante

-   Número associado a toda matriz QUADRADA

-   Facilita muita coisa, mas na moral não tem nenhum significado
    especial

-   Existem diferentes métodos, cada um adequado para uma situação

-   **Representação**

    -   Ex: determinante da matriz A~2x2~

    -   Representado por $\text{Det }(A_{2\times 2})$

    -   Ou também por $\left| A \right| = \left| \begin{matrix}
        a_{1,1} & \cdots & a_{1,j} \\
         \vdots & \ddots & \vdots \\
        a_{1,i} & \cdots & a_{i,j} \\
        \end{matrix} \right|$

## Regra de Sarrus

-   Válido somente para 3x3 e 2x2

-   **Determinante de uma 2x2**

    -   "Produto dos elementos da DP" - "Produto dos elementos da DS"

![Resultado de imagem para determinante
2x2](./Images/&02.0 - Determinante/media/image1.png){width="4.21698053368329in"
height="0.8497692475940507in"}

-   **Determinante de uma 3x3**

    -   Repete as duas primeiras colunas coluna

    -   Produto das diagonais decrescentes -- produto das diagonais
        crescentes

    -   *Exemplo salvador:*

![](./Images/&02.0 - Determinante/media/image2.jpeg){width="2.442361111111111in"
height="2.7819706911636044in"}

## Teorema de LaPlace

-   Determinante de qualquer matriz quadrada

-   **Menor complementar de um elemento**

    -   Determinante da matriz eliminando sua linha e coluna

<img src="./Images/&amp;02.0 - Determinante/media/image3.png" style="zoom:67%;" />

-   **Cofator**

    -   Magia relacionada a um elemento especifico

<img src="./Images/&amp;02.0 - Determinante/media/image4.png" style="zoom:67%;" />

-   Determinante = soma de todos os "produtos dos cofatores pelos seus
    respectivos elementos" de uma linha/coluna qualquer

![](./Images/&02.0 - Determinante/media/image5.png){width="6.801388888888889in"
height="3.873611111111111in"}

## Regra de Chió

-   Precisa de pelo menos um elemento igual a 1

-   Mais fácil que Laplace, quando não têm zeros

-   Escolhe um dos elementos 1 e elimina sua linha coluna.

-   A matriz de ordem inferior gerada será cada "elemento restante --
    produto dos elementos eliminados dessa mesma linha e coluna"

-   A determinante da matriz superior será a determinante da
    inferior\*(-1)^i+j^ do elemento escolhido

![](./Images/&02.0 - Determinante/media/image6.jpeg)
![](./Images/&02.0 - Determinante/media/image7.jpeg)
![](./Images/&02.0 - Determinante/media/image5.png)

<https://www.youtube.com/watch?v=fSalJmaYlUU&list=PL1E3A80A44F4F1669&index=5>

## Triangularização

-   Transformar, através de operações elementares, uma matriz quadrada
    normal em uma matriz triangular superior

-   Determinante será o produto da diagonal principal / alterações na
    determinante causadas pelas transformações elementares

-   Bizú: triangularize metodicamente de cima pra baixo e da esquerda
    pra direita

-   *Exemplo:*

$$
A = \begin{bmatrix}
1 & 4 & 2 \\
 - 3 & 1 & 3 \\
5 & 1 & 2 \\
\end{bmatrix}
$$
$$
\begin{bmatrix}
1 & 4 & 2 \\
0 & 13 & 9 \\
5 & 1 & 2 \\
\end{bmatrix}
$$
$$
\begin{bmatrix}
1 & 4 & 2 \\
0 & 13 & 9 \\
0 & - 19 & - 8 \\
\end{bmatrix}
$$
$$
\begin{bmatrix}
1 & 4 & 2 \\
0 & 1 & \frac{9}{13} \\
0 & - 19 & - 8 \\
\end{bmatrix}
$$
$$
\begin{bmatrix}
1 & 4 & 2 \\
0 & 1 & \frac{9}{13} \\
0 & 0 & \frac{67}{13} \\
\end{bmatrix}
$$

$$
\frac{1}{13}*Det\ \left( A \right) = \frac{67}{13}
$$

$$
\text{Det\ }\left( A \right) = 67
$$
## Propriedades

* **Determinante zero**

  * Uma fila inteira é igual a zero.

  ![](./Images/&02.1 - Determinante - propriedades /media/image1.png)

  * Duas filas paralelas são igual/proporcionais

  ![](./Images/&02.1 - Determinante - propriedades /media/image2.jpeg)

  * Quando uma fila é a combinação linear das outras paralelas a essa

  ![](./Images/&02.1 - Determinante - propriedades /media/image3.jpeg)

* **Determinante \*(-1)**

  * Determinante troca de sinal quando trocarmos duas filas paralelas de
    posição

  ![](./Images/&02.1 - Determinante - propriedades /media/image4.jpeg)

  * Det A = Det $A^t$

  ![](./Images/&02.1 - Determinante - propriedades /media/image5.png)
  
* **Multiplicação por K**

  * Sendo $K\mathbb{\in R}$

  * Fila multiplicada: Multiplicando toda uma fila por um número, o determinante da matriz fica multiplicado por esse número também

  ![](./Images/&02.1 - Determinante - propriedades /media/image6.jpeg)

  * Matriz multiplicada: Quando uma matriz QUADRADA de ordem n é multiplicada por um número K, seu determinante fica multiplicado por K^n^

  ![](./Images/&02.1 - Determinante - propriedades /media/image7.jpeg)

* **Teorema de Binet**
  
  * Se A e B são matrizes da mesma ordem, então:
$$
\det\left( A*B \right) = detA*detB
$$

* **Matriz diagonal, triangular e identidade**

  * O determinante dessas matrizes é sempre igual ao produto dos elementos da diagonal principal.

  * Determinante da matriz identidade é sempre 1.

* **Potência da determinante**

  * $Det(A^{m})\  = \ {(det\ A)}^{m}$

  ![](./Images/&02.1 - Determinante - propriedades /media/image8.jpeg)

* **Inverso da determinante**

  * $\text{Det\ }A^{- 1}\  = \frac{1}{\det A}$

  * Se determinante for 0, a matriz não é invertível (matriz singular)

* **Teorema de Jacobi**

  * O determinante de uma matriz não se altera quando adicionamos a uma fila outra fila paralela multiplicada por uma constante.

  ![](./Images/&02.1 - Determinante - propriedades /media/image9.jpeg)

# Matriz inversa

-   Uma matriz quadrada multiplicada pela sua inversa é igual à matriz
    identidade.

$$AB\  = \ BA\  = \ I$$

-   Condição para que a matriz tenha uma inversa → determinante ≠ 0

-   É possível resolver um sistema linear através da inversa (ver em
    *S.L. -- resolução)*

-   **Propriedades**

    -   ${{(A}^{- 1})}^{- 1} = A$

    -   ${(A*B)\ }^{- 1} = \ B^{- 1}*A^{- 1}\ $

    -   $\left( A^{t} \right)^{- 1} = \left( A^{- 1} \right)^{t}$

    -   Se A e B são invertíveis, então AB também é

## Através de um sistema

-   Matemática reversa

-   Resulta em um sistema linear

-   *Ex: inversa da matriz A*

![](./Images/&03.0 - Matriz inversa/media/image1.jpeg){width="2.5944444444444446in"
height="5.113338801399825in"}

## 3 passos consecutivos

-   **Matriz dos cofatores**
    -   Substituir todos os termos pelos seus respectivos cofatores

    -   Cofator → ver em *Determinante -- LaPlace*


![](./Images/&03.0 - Matriz inversa/media/image2.jpeg){width="2.3759612860892387in"
height="1.1320745844269466in"}

-   **Matriz adjunta**

    -   Transposta da matriz dos cofatores

![](./Images/&03.0 - Matriz inversa/media/image3.png){width="2.6414227909011374in"
height="0.7166797900262467in"}

-   **Matriz inversa**

    -   Adjunta dividida pela determinante

![](./Images/&03.0 - Matriz inversa/media/image4.jpeg){width="3.037735126859143in"
height="1.1879451006124235in"}

-   2x2 é facil

-   3x3 é chatinha, por que vais ter que fazer a determinante de 9
    matrizes 2x2 (cofator de cada uma)

-   4x4 é hard, são 16 matrizes 3x3

## Inversão de Gauss Jordan

-   "As mesmas operações elementares que transformam A em I transformam
    I em A"

-   Portanto, se juntarmos A e I em uma matriz aumentada, realizaremos
    as duas transformações ao mesmo tempo

$$\begin{bmatrix}
A & \vdots & I \\
\end{bmatrix}\begin{bmatrix}
I & \vdots & A^{- 1} \\
\end{bmatrix}$$

-   Se A não for invertível, nunca chegaremos na Identidade

-   *Exemplo: inverter A*

$$
A\ aumentada = \begin{bmatrix}
 - 2 & 3 & 1 & \vdots & 1 & 0 & 0 \\
1 & - 3 & 1 & \vdots & 0 & 1 & 0 \\
 - 1 & 2 & - 1 & \vdots & 0 & 0 & 1 \\
\end{bmatrix}
$$
$$
\begin{bmatrix}
1 & 0 & 0 & \vdots & - \frac{1}{3} & - \frac{5}{3} & - 2 \\
0 & 1 & 0 & \vdots & 0 & - 1 & - 1 \\
0 & 0 & 1 & \vdots & \frac{1}{3} & - \frac{1}{3} & - 1 \\
\end{bmatrix}
$$

$$
A^{- 1} = \ \begin{bmatrix}
 - \frac{1}{3} & - \frac{5}{3} & - 2 \\
0 & - 1 & - 1 \\
\frac{1}{3} & - \frac{1}{3} & - 1 \\
\end{bmatrix}
$$

## Através da fatoração LU

* LU fatorization is good to evaluate differente right vetors (b)
* if b have one 1 and all rest 0, them the ressult x will be a column of the inverse matrix
* To a $n \times n$ matrix, we solve $n$ systems like that to find the inverse
* Is similar to the first method, solving one big system, but more efficient