# Sets

* **Cartesian product**

  * set operation

  * For two sets A and B:
    $$
    A\times B=\{\,(a,b)\mid a\in A\ {\mbox{ and }}\ b\in B\,\}.
    $$
    
  * Can be expanded for $n$ sets taking $n$ elements
  
  * Given a family (set) of sets $A={A_1, A_2,...,A_n}$, the cartesian product between its elements is simply denotated $\times_{i=1}^n A_i$
  
* Cardinality

  * The number of elements in a finite set
  * $|A|$ = cardinality of set A
  * Properties
    * $|A \times B| = |A|*|B|$

# Relations

* Given a set A, a *relation R on A* is a set of pairs with elements taken from A
* $R \subseteq A \times A$
* $(x,y) \in R$ is also notated $xRy$
* This definition (binary) can be expanded to more term, taking from $A\times A\times A$ (ternary), etc
* Power set
  * The set of all relations that can be defined on a given Cartesian product of finite
    sets is the power set (the set of all subsets) of the Cartesian product.

## Properties

* Reflexivity
  * A relation is reflexive if $\forall x \in A, (x,x) \in R$
  * Irreflexive: no element satisfies that
* Transitivity
  *  if (x, y ) ∈ R and ( y, z ) ∈ R imply ( x, z ) ∈ R, for all x, y, z ∈ X.
* Simetricity
  * Simetric if x is related to y, then y is related to x
  * $\forall x,y \in A, if (x,y) \in R, then (y,x) \in R$
  * Assimetric: if x is related to y, then y is *not* related to x (for all  x!=y)

## Types of relations

Equivalence relations: reflective, symmetric, and transitive.
Compatibility relations: reflective and symmetric.
Partial orderings: reflexive, anti symmetric, and transitive.
Strict orderings: antireflexive, anti symmetric, and transitive.

## Partial orders

* **weak partial order**
  * or *non strict*
  * Reflexive, transitive and antisymmetric
* **strong partial order**
  * Or **strict**
  * irreflexive, transitive and antisymmetric
* **Total Order**

  * a partial order that has one additional property - any two elements in the set should be related

# Partially ordered sets

* **Hasse diagram**
  * Diagram to represent finite partially ordered set







# Algebra abstrata

* Homomorfismo

  * Dois grupos são isomorfos se existe uma aplicação que se conserva de um pra outro

  * 

    

* Isomorfismo

  *  homomorfismo bijetivo
  * V e W são isomorfos se existe T:V->W bijetor 
  * toda propriedade valida em um é válida no outro





























