# Bibliografia
* Sinais e sistemas lineares, Lathi, 2ºed (didático)
* Sinais e sistemas, Haykin e Veen (médio)
* Signal and Systems, Oppenhein e Willsky (top)

# Avaliação
* Duas provas, pesos iguais
* Geralmente ele dá um formulário

# Outro

* pulamos os tópicos discreto do Lathi (serão visto em DSP)
* não estudamos tópicos não-lineares, nem sinais variantes no tempo



# Lista P1

1.1-1	easy

1.1-2	easy

1.1-4	easy

1.1-5	media

1.2-1	média

1.2-2	média

1.2-3	media

1.3-1	média

1.3-2	media

1.3-5	media

1.4-1	easy

1.4-2	easy

1.4-3	easy

1.4-4	easy

1.4-6	easy

1.5-2	média

 

2.2.1	easy (estado nulo)

2.4.5	easy (convoluções básicas)

2.4.13	easy (convolução gráfica)

2.4.17	media

2.4.18	não deu

2.4.24	media (serie e paralelo de sistemas)

2.4.31	teorica

2.4.32	hard (quebrar convuloção em muuuuitas partes)



# lista 2

4.21 B) Por que o resultado é diferente se eu uso a propriedade de multiplicar no tempo por uma exponencial ou por uma senoide (deduzimos só pro cosseno, mas a resulução usa outra)? 