**O que é planejamento e qual sua importância no contexto organizacional?**

Planejar é definir objetivos e os meios para alcançar-los. Simples. Simples? 

Objetivos podem ser uma ferramenta poderosa para motivar as pessoas, para inspirar uma visão compartilhada. Qual membro da organização não ficaria orgulhoso de ver que o seu trabalho contribui para algo maior e que logo (e “logo” pode ser muito relativo) ele verá os resultados dos seus esforços? Entretanto a definição de tão maravilhosos objetivos não precisa acontecer em um momento de iluminação, pelo contrário: é fruto de um processo meticuloso e racional, cujo objetivo é encontrar objetivos que satisfaçam as características do acrónimo SMART:

![](../01 - Gestão de projetos/Images/7 - 1.0 - Escopo/media/image1.png)

Mas planejar vai além de definir o ponto de chegada, é preciso definir o caminho. Novamente a definição de um maravilhoso caminho não precisar vir por iluminação divina, e existem diversas técnicas e metodologias para construir-lo gradualmente, por exemplo produzindo uma Estrutura Analítica do Projeto: um mapeamento dos resultados entregáveis, por exemplo:

<img src="Images - Homework 3/eap12.png" alt="Image result for estrutura analítica do projeto exemplo" style="zoom:67%;" />

> Fonte: [Radar de Projetos, 2015](http://www.radardeprojetos.com.br/2015/03/o-que-e-uma-eap.html)

Um bom planejamento é a base para alcançar os objetivos e não pode ser confundido com um mero ritual, uma formalidade de apenas “inserir alguns dados no Microsoft Project”. Tamanha importância fica bem ilustra em uma frase atribuída a Abraham Lincoln:

> *"Se eu tivesse nove horas para cortar uma árvore, passaria seis horas
> afiando o meu machado"*

