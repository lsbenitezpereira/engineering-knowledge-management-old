#### 1) Qual a diferença entre eficiência e eficácia?

Dado um objetivo à ser atingido (por exemplo, responder estas 3 questões) ser *eficaz* é maximizar o cumprimento desse objetivo (tirar nota 10), independente do custo associado (eu poderia demorar 10 horas respondendo a prova, buscando atingir o objetivo o melhor possível). 

Ser *eficiente*, por outro lado, é maximizar o custo-benefício, atingindo o objetivo de forma satisfatória com um custo satisfatório (completar a prova em aproximadamente duas horas, tirando uma nota boa, porém não necessariamente a melhor possível). 



#### 2) **No que cada uma das abordagens, a seguir, pode contribuir com as organizações contemporâneas?**

**a) Abordagem Clássica da Administração:**

A divisão racional do trabalho nos permite organizar processos de forma altamente *eficiente*, maximizando o uso de nossos [quase sempre escassos] recursos. Para isso, podemos olhar para as técnicas da Abordagem Clássica para melhorarmos o “encaixe” das nossas tarefas no tempo e no espaço, 



**b) Abordagem Humanistica da Admnistração:**

Há situações em que precisamos mais do que um funcionário *eficiente*, precisamos de pessoas que alcancem seu máximo potencial. Para isso precisamos de um olhar *humano*, que entenda os fatores motivacionais por trás do comportamento das pessoas, algo que a Abordagem Clássica não foi capaz de alcançar. Segundo o autor Mihaly Csikszentmihalyi, conhecido por seus estudos da performance humana [1]:

> “One cannot lead a life that is truly excellent without feeling that one belongs to something greater and more permanent than oneself.”

*(Não se pode levar uma vida verdadeiramente excelente sem sentir que pertence a algo maior e mais permanente do que você mesmo, tradução própria)*



**c) Abordagem Estruturalista da Admnistração:**

Mas não só de indivíduos se faz uma organização, e maximizar o resultado dos funcionários (enquanto indivíduos) não necessariamente levará à maximizar o resultado da organização (enquanto todo). Para isso precisamos criar um *ambiente organizacional* que favoreça a motivação, usando tanto incentivos formais (por exemplo: aumento no salário) quanto informais (por exemplo: mais socialização).

Portanto, não podemos esquecer de olharmos para como nossa organização está estruturada, como os processos se encaixam (lembra da teoria clássica? poizé!).



**d) Abordagem Burocrática da Administração:**

Poxa “Senhor Estruturalista”, falar é fácil. Lá na nossa bandeira (e na boca do finado Weber) está escrito “Ordem e Progresso”, mas nem sempre conseguimos transformar essa visão em realidade. Para realmente maximizarmos os resultados da organização precisamos definir *mecanismos* que garantam a previsibilidade e repetibilidade dos processos, levando a uma organização efetiva (eficaz + eficiente) e com regras bem definidas.

Para realmente aproveitarmos a Abordagem Burocrática em uma organização contemporânea, precisamos nos livrar da nossa visão moderna de “burrocracia” como algo ineficiente e desnecessário, e olharmos para o que essa abordagem nos trouxe de melhor em termos de organização de processos e racionalização das estruturas.



**e) Abordagem do Desenvolvimento Organizacional:**

Mas o mundo está em constante mudança, e mesmo a melhor racionalização possível *hoje* pode facilmente se tornar ruim *amanhã*. Devemos nos preparar para mudar, estabelecendo uma cultura organizacional que favoreça a inovação e o enfrentamento de problemas. Precisamos estabelecer processos *previsíveis* para lidar com o *imprevisível*, de forma a aprendermos conforme sofrermos influência de forças exógenas (como a globalização) e endógenas (como a adoção/desenvolvimento de novas tecnologias).



**f) Abordagem Contingencial:**

E qual a “receita de bolo” para lidar com a mudança? Não há. 

Quando instável o ambiente, mais maior será o “terremoto” da mudança, e a organização precisa possuir contingências (relações do tipo “se-então”) para lidar com a mudança. Cada organização é diferente, cada ambiente é diferente, e até mesmo a forma como devemos conduzir o aprendizado é diferente (treinamentos formais? Colaborações entre profissionais de áreas diferentes? Horários para “aprenda o que quiser”, como por exemplo fez a empresa [Atlassian](https://www.atlassian.com/blog/archives/atlassians_20_time_a_year_in_review) [2]?), e o nosso estilo de administração deve refletir isso.



#### 3) As Teorias Administrativas foram criadas para resolver problemas  organizacionais de planejamento, ordem e disciplina devido ao  crescimento acelerado das organizações após a Revolução Industrial.  Sobre as Teorias da Administração pode-se afirmar:

1. O estudo das teorias nos ajuda a compreender as organizações e o papel dos gestores no contexto organizacional.
2. As teorias buscam identificar e estabelecer diretrizes, estratégias e técnicas para gerir as organizações.
3. A primeira teoria da administração começou com a ênfase nas pessoas.
4. Cada uma das teorias é produto das forças sociais, econômicas, políticas e tecnológicas presentes num dado tempo e lugar.

Assinale a alternativa correta:

**a)I, II e IV são verdadeiras.**





#### Referências

[1] - https://www.ted.com/talks/mihaly_csikszentmihalyi_flow_the_secret_to_happiness?language=pt-br

[2] - https://www.atlassian.com/blog/archives/atlassians_20_time_a_year_in_review

