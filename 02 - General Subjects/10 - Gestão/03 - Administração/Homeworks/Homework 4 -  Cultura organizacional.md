## Cultura organizacional

Todo grupo de pessoas desenvolve, ao longo do tempo, signos e simbolos compartilhados que se expressam na suas formas de agir, pensar, perceber e sentir. Esse padrão coletivo também surge em organizações, apesar do fenómeno só haver recebido o nome de *cultura organizacional* na década de 60. 

Shawn Achor aponta no livro The Happiness Advantage [1] que uma cultura organizacional voltada para o bem-estar e crescimento promove um grande aumento na produtividade dos funcionários, na sua conexão com a empresa e nos resultados da empresa como um todo. Dessa forma, estimular uma cultura positiva não deve ser apenas a *consequência* de um grupo de pessoas trabalhando juntas: ela deve ser ativamente cultivada e estimulada, em todos os níveis organizacionais. 

![https://neilpatel.com/wp-content/uploads/2013/02/google-dublin.jpg](Images - Homework 4/google-dublin.jpg)

> Ambiente de trabalho na Google [2]

![image-20200402203508736](Images - Homework 4/image-20200402203508736.png)

> Ambiente de trabalho na Google [3]

Mas o “cultutivo” uma de um corpo cultural não acontece por mera vontade gerencial, e está  intimamente relacionado ao desenvolvimento histórico e cultural do grupo:  os problemas enfrentados, as soluções encontradas, as pressões -  internas e externas - por mudanças, entre outros. 
A cultura brasileira, por exemplo, pode ser analisada em conjunto com a linha do tempo apresentada abaixo:

  <img src="Images - Homework 4/75275968c6b6b9dfccc6f57ec3d3af82.jpg" alt="https://i.pinimg.com/originals/75/27/59/75275968c6b6b9dfccc6f57ec3d3af82.jpg" style="width: 80%;" />

  > Períodos da História do Brasil [4]

José Pires e Kátia Macêdo (2006) focam sua análise no período republicado, por este caracterizar o período mais importante para o desenvolvimento da cultura organizacional em organizações públicas no Brasil.

Schall (1997) aponta que a periódica troca de gestão confere às organizações públicas 4 características, das quais eu gostaria de comentar uma: projetos de curto prazo, onde cada governo só privilegia projetos que  possa concluir em seu mandato, para ter retorno político.
Estamos vivendo um período de grande importância geopolítica - a epidemia de Covid-19 - onde percebe-se o impacto das deficiências acumuladas pelo estado brasileiro em áreas como saneamento básico, saúde pública, garantia de condições habitacionais mínimas, etc. Muitas dessas deficiências derivam dessas áreas não estarem cobertas por um plano de estado (em contraste à um plano de *governo*), um ideal de nação, que alinhe as políticas públicas de diferentes gestões em direção à uma visão compartilhada.

## Referências

1 - Achor, S. (2010). *The happiness advantage: The seven principles of positive psychology that fuel success and performance at work*. 

2 - https://neilpatel.com/blog/googles-culture-of-success/

3 - https://www.indesignlive.com/the-goods/sleeping-fit-wlenis-sleepfit

4 - http://leonidasfelipe.blogspot.com/2015/11/mapa-mental-periodos-da-historia-do.html

