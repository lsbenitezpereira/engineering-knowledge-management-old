**Equipe:**

1. Leonardo Benitez
2. Lucas Freitas Sagás
3. Lucas Duarte Gadelha
4. Bruna Américo Trento
5. Paulo Cesar dos Santos
6. Hazael dos Santos Batista



**Qual o significado de administração e como esta área do conhecimento humano pode ser útil na atualidade?**

Administrar é tomar decisões de forma a chegarmos nos objetivos que queremos com os recursos que temos. E tomar decisões é o que mais fazemos nas nossas vidas: o que comprar no mercado, para onde ir nas férias familiares, qual tema escolher no trabalho de Administração para Engenharia, onde investir o recursos da empresa, como o estado deve atuar frente ao Covid-19, quais as metas ambientais deste século, etc.

Pelos meus exemplos a administração parece permear tudo atualmente.
E no passado? Antes de descobrirem o fogo, havia administradores dos escassos recursos. 
E no futuro? Depois que sistemas de inteligência artificial forem capazes de fazer o trabalho de engenheiros, ainda vão ser necessários administradores para definir as direções estratégicas de desenvolvimento tecnológico e para onde nossa civilização caminha.

Portanto a administração é mais do que *útil*, ela é *essencial*.

**Para que servem as Teorias da Administração?**

Ok ok, administração é essencial, legal.
Mas será que a minha visão de administração é a mesma que a sua? Deveria ser? 
As mesmas técnicas e ferramentas que servem bem a uma empresa altamente estruturada e burocrática também são boas para um laboratório de pesquisa pequeno? 

Não, e não precisam ser. Diferentes escolas administrativas chegaram a interpretações diferentes do que significa administrar bem, que variam quanto às técnicas utilizadas, modelos de organização, modelos de gestão, entre outras diferenças. Cada uma dessas visões diferentes é chamada de uma Teoria da Administração, e ao longo desse semestre estudaremos algumas delas:

* Abordagem Científica e Teoria Clássica
* Abordagem humanística
* Abordagem organizacional
* Abordagem contingencial

