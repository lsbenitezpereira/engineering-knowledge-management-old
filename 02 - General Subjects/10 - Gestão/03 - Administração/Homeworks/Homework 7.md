## Pra que serve a função CONTROLE?

No jargão da engenharia, um sistema é controle é um “conjunto de equipamentos e dispositivos que gerenciam o comportamento de máquinas ou outros sistemas físicos” [1], monitorando uma saída para dar um feedback e corrigir o comportamento, garantindo que a saída segue um valor de referência.

![Sistemas de Controle - Wiki Cursos IFPR Foz](Images - Homework 7/unnamed.png)

> Fonte: [IFPG](http://wiki.foz.ifpr.edu.br/wiki/index.php/Sistemas_de_Controle)

Na área de administração o objetivo é análogo: garantir que a execução está de acordo com o planejamento. A nossa “variável de saída” pode ser diversas coisas:

* Desempenho
* Tomada de decisões
* Objetivos (resultados esperados)
* Atividades (a ser realizada)

O que em engenharia chamamos de “feedback em tempo real” para a adminstração equivale a controlar os processos enquanto estão acontecendo, ajustando os planos para garantir um melhoramento contínuo. Uma das metodologias para materializar esse ajuste constante é o ciclo PDCA (plan–do–check–act):

![img](Images - Homework 7/PDCA-Multi-Loop.png)

> Fonte: [[3]](https://en.wikipedia.org/wiki/PDCA)