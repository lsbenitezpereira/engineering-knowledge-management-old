# Teorias da Administração

* Visões diferentes sobre a mesma coisa: como organizações se organizam
* Teoria Geral da Administração: conjunto de conhecimentos e teorias
* Cada teoria administrativa enfatiza um aspecto da administração: organizar tarefas, pessoas, ambientes, etc
* Cada abordagem se subdivide em mais de uma teoria

![image-20200525171849853](Images - 1.1 - Teorias da administração/image-20200525171849853.png)

* [Vídeo aulas 1 (rápidas, diretas)](https://www.youtube.com/watch?v=9jzs5S-tnHw&list=PLPjPQR5HICvXhmuMSaVvx-WQGcmqZ4KfL)
* [Vídeo aulas 2 (mais ampla, formal)](https://www.youtube.com/watch?v=wkJmdM8ALSs&list=PLd62WfTsN2p4pZs6ryUmt6TfM3UIIjx71)

## Abordagem clássica

* Consolidação da administração enquanto ciência
* Mais produtividade = menos disperdício
* **Princípios**
  * divisão racional do trabalho
  * Supervisão funcional
  * Estudo da fadiga humana.
  * Incentivos salariais
  * Casar todos os processos no tempo e espaço

### Teoria científica

* Frederick Winslow Taylor (americano)
* Taylorismo
* foco em tarefas
* Bottom-up approach: das partes (operários, supervisores) para o todo (organização)
* Henry Ford como seguidor das ideias (Fordismo), voltado para a realidade industrial

### Teoria clássica

* Henri Fayol (europeu)
* Foco em estruturas
* Top-down aproach: da direção para os departamentos

## Abordagem humanística

* Baseada na psicologia
* Crise de 29 $\rightarrow$ busca de maior eficiência
* Começa nos Estados Unidos, mas só se espalha pro resto do mundo depois da Segunda Guerra
* **Princípios**
  * humanizar o trabalho
  * organização da equipes
  * Foco na estrutura informal da empresa

### Teoria das relações humanas

* Relações humanas são fundamentais para o trabalho
* Revisa as abordagens clássicas para focar na socialização
* **Experiência de Hawthorne**
  * de 1927 a 1932, bairro de Hawthorne
  * Ideia original: relacionar iluminação com produtividade
  * foram alterando vários parâmetros, 
  * conclusão: a socialização é fator mais importante
* **Princípios**
  * Organizar o trabalho de forma a favorecer a interação humana
  * Foca na equipe mais do que no indivíduo

### Teoria comportamentalista

* década de 40
* busca entender a motivação humana
* Baseada na Pscicologia Behavorista
* Menos prescritiva e mais descritiva
* **Críticas à Teoria das Relações Humanas**
  * Ingênua
  * TRH acha que o indivíduo será moldado pela organização
  * acha que um funcionário feliz necessariamente é um funcionário produtivo
* **Dois Fatores de Herzberg**
  * Estudo da motivação do funcionário
  * Fatores motivacionais
    * ou *intrínsecos*
    * estão sob o controle dos indivíduos
    * levam a satisfação
    * Aquilo que ele faz, sua rotina, seu desenvolvimento, etc
  * Fatores higiênicos
    * ou *extrínsecos*
    * Fora do controle do indivíduo
    * levam a insatisfação
    * Como são as políticas da empresa, ambiente de trabalho
* **Princípios**
  * A competição e cooperação entre os indivíduos devem ser consideradas
  * Estudo sistemático da liderança e tomada de decisões 
  * Foca no funcionário enquanto indivíduo único

## Abordagem organizacional

* Olhar só para a máquina não é eficiente, olhar só para o indivíduo não é eficaz
* Buscam compreender a organização como um todo: sua estrutura, sua relação com o ambiente, as mudanças pela qual ela passa

### Teoria neoclássica

* A partir de 1950
* Mais flexível e humana do que a abordagem clássica
* Muito da visão atual (2020) sobre administração vêm da Neoclássica
* **Princípios**
  * Planejamento, organização, direção e controle
  * Divisão e especialização do trabalho, porém de forma flexível

### Teoria burocrática

* Ênfase na estrutura
* A partir da década de 40
* predominantemente europeu
* Fortemente baseada em Max Weber (que já tinha morrido em 1920)
* Cuidado: a visão atual (2020) é de burocracia como algo negativo, por causa das disfunções do excesso burocrático
* **Princípios**
  * previsibilidade e repetibilidade
  * processos bem definidos

### Teoria estruturalista

* Ênfase no ambiente, estrutura e a relação das pessoas com eles
* Final da década de 1950
* Pensar no todo (organização) é mais importante do que pensar nas partes (funcionários)
* Muito próxima de Teoria das Relações Humanas, porem não foca tanto no indivíduo
* Critica as teorias anteriores por serem “narrow minded”
* **Princípios**
  * Flexibilidade
  * Criar um ambiente organizacional que favoreça a motivação
  * Usa tanto incentivos formais (ex: aumento no salário) quanto informais (ex: mais socialização)

### Teoria de desenvolvimento organizacional (DO)

* Principal autor: Leland Bradford

* a partir de 1962

* Mudança organizacional planejada

*  Toda organização precisa inovar e aprender para enfrentar os desafios que bloqueiam o seu
   progresso

* **Principais conceitos**

  * Cultura organizacional: longo prazo, relacionada à forma como as pessoas trabalham e se relacionam
  * Clima organizacional: dia a dia, humor da empresa
  * Quatro variáveis básicas: ambiente, organização, grupo e indivíduo
  * Pesquisa-ação: o DO utiliza a pesquisa para o diagnóstico e a ação da mudança
  * Retroação: informação de retorno e feedback às pessoas sobre suas ações

* **Administração da mudança** 
  
  *  forças exógenas: exigências da economia globalizada, da tecnologia, dos consumidores, dos concorrentes, etc.
  
  * forças endógenas: como decisões e atividades internas, demandas de novos processos e tecnologias, novos produtos ou serviços, exigências dos empregados e sindicatos, etc.
  
  * Processo de mudança:
  
     ![image-20200526135513135](Images - 1.1 - Teorias da administração/image-20200526135513135.png)
  
    > Fonte: Chiaveato, 2014
  
* **Princípios**
  
  * Pensar no longo prazo: desenvolvimento como um plano organizacional
  * Estabelecer um programa coerente de mudança
  * Trabalhar tanto com equipeis formais consolidadas (que vão precisar de um desenvolvimento de longo prazo) e equipes temporárias (que vão precisar de treinamentos de curto prazo)
  * Mudar para sobreviver
  * Renovação organizacional
  * cultura organizacional centrada na adaptação
  * Feedback constante e sistêmico
  * Orientação contingencial: adaptar-se às situações conforme elas forem diagnosticadas, sendo pragmático e pensando sempre na solução dos problemas	

### Teoria sistêmica

* Foco no ambiente, solucionando a complexidade com observação
* Fortemente baseada na Teoria Geral de Sistemas
* procura entender as inter-relações entre as diversas variáveis a partir da visão de um campo dinâmico de forças que atuam entre si
* Sistema = propósito + globalismo (se insere em um todo)
* Organização = sistema ou conjunto de sistemas
* Enxerga a organização como um sistema a ser otimizado
* **Ideias guia**
  * Expansionismo: a função da parte depende da sua inserção no todo
  * Sintetismo: juntar mais do que separar; tentar pensar sempre o mais macro possivel
  * Teleologia: olhar para os efeitos, que são mais do que uma função determinística das entradas
* **Princípios**
  * Olhar para o meio e para outras organização
  * Ajuste constante, olhando para o todo
  * Se preparar para interagir com o meio
  * Não tentar reduzir uma organização à uma cadeia de processos/blocos, e sim considerar-la como um todo que interage com o meio (que pode ser arbitrariamente complexo e imprevisível)

## Abordagem contingencial

* Foco no ambiente, solucionando a complexidade com tecnologia
* A partir da década de 60
* Não há formula única: aponta o que há de bom e ruim e cada uma das teorias anteriores
* Cada teoria anterior é boa para uma situação
* contingência: relação do tipo “se-então”
* **Princípios**
  * se preparar para sair da rota
  * Flexibilidade
  * Tecnologia é fundamental para se adaptar rapidamente
* **tpos de organização**
  * organizações mais estáveis tendem a possuir menos contingencia, entruturas mais fixas, ambientes mais tradicionais
  * organizaçoes menos instáveis tendem a possuir mais contingencia, serem flexíveis, e inovadoras
  * o mundo tende a estar mais conectado e, portanto, mais complexo e instável. consequentemente, essa teoria prevê que as empresas tendem a se tornar mais flexíveis (tanto na sua estrutura quanto na sua atuação)

## Abordagens modernas

* **Melhoria contínua**

  * mudança organizacional suave e contínua

  * todos os empregados da organização, de maneira que realizem suas tarefas um pouco melhor a cada dia

  * A filosofia da melhoria contínua deriva do kaizen (do japonês kai, que significa mudança e zen, que
    significa bom)

    ![img](Images - 1.1 - Teorias da administração/Mon, 25 May 2020 230730.png)

* **Total Quality Management (TQM)**

  * Atribui às pessoas, e não somente aos gerentes e aos dirigentes, a responsabilidade pelo alcance de padrões de qualidade
  
* **Six Sigma (6S)**

  * meados de 1987
  * Foco na satisfação dos clientes, através da redução de defeitos nos processos e o ótimo desempenho da empresa
  * Ciclo DMADV e DMAIC?
  
* **Benchmarking**

  * processo contínuo de avaliar produtos,
  * serviços e práticas dos concorrentes mais fortes e daquelas empresas que são reconhecidas como líderes empresariais















