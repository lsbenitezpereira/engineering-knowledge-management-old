## A better way to introduce digital tech in the workplace

https://news.mit.edu/2021/kate-kellogg-tech-workplace-0603

o [artigo](https://pubsonline.informs.org/doi/full/10.1287/orsc.2021.1445) que eles citam parece interessante

"I extend the political science perspective of experimentalist governance to examine how a digital technology-focused, iterative collective action process of **local experimentation followed by central revision** can facilitate mutually beneficial role reconfiguration during digital technology introduction and integration"