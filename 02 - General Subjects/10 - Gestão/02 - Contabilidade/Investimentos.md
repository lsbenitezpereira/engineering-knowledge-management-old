# Conceitos

* **Mercado primário**
  * Diretamente com a empresa/banco/governo
  * compra garantida
  * preço determinado, sem grande flutuação
* **Mercado secundário**
  * pagando o preço do mercado 

## Bibliografias

* Canais de youtube
  * Ganhando a vida adoidado (dicas de feeling)
  * Didi aguiar (dicas técnicas)
  * Portal do trade (tutoriais sobre análsie tecnica)

# Renda fixa

-   Investimentos indexados à uma taxa de mercado

-   Seguros e estáveis

-   Bom pra quem procura um rendimento maior que a poupança, porém sem
    todo o stress de bolsa e etc´s

-   **Fundos DI**

    -   Ou *fundo de renda fixa referenciada DI*

    -   Investem no mínimo 95% em títulos públicos atrelados à Selic

-   **Fundo Garantidor de Crédito (FGC)**

    -   Garante aplicações até 250k

    -   Se der merda, o governo te ressarce

    -   Caso queria investir mais do que isso, divida seu dinheiro entre
        bancos diferentes.

## Principais indicadores

-   **CDI**

    -   Certificado de Depósito Interbancário

    -   Quanto os bancos estão cobrando pelo dinheiro

    -   Segue sempre próximo à Selic, geralmente 0.1% abaixo

-   **IPCA**

    -   Índice Nacional de Preços ao Consumidor
-   Inflação
    -   O quanto as coisas sobem de preço
-   consumo aquecido (grande demada) costuma fazer o preço dos produtos subirem (considerando oferta constante) e, portanto, aumento da inflação
    
-   **SELIC**

    -   Taxa de juros

    -   Determinado pelo governo para controlar a inflação
    
    -   quando o governo sobe a selic geralmente é uma tentativa de diminuir o consumo (frear a economia) e diminuir/controlar a inflação
    
    -   subir os juros costuma atrair investidores internacionais, valorizando a moeda nacional
    
        ![image-20200718190754665](Images - Investimentos/image-20200718190754665.png)
        
        ![image-20200718190810664](Images - Investimentos/image-20200718190810664.png)

## Tesouro direto

-   Compra de títulos públicos

-   Emprestar para o governo

-   [Rentabilidade
    atualizada](http://www.tesouro.gov.br/pt/-/rentabilidade-acumulada)

-   Especular no Tesouro costuma valer a pena em cenário de queda da
    inflação/juros, já que os papeis se valorizam nesses períodos. Se a
    inflação/juros tende a subir, opte por um investimento mais
    arrojado, como no setor imobiliário (que se beneficia dos juros) ou
    multimercados (visto que *commodities* e serviços essenciais também
    se valorizam)

-   **Investimento mínimo** → 30

-   **Risco →** garantido pelo FGC

-   **Imposto de renda**

    -   Regressivo

    -   Cobrado sobre o rendimento

    -   Se tiver juros semestrais, o IR será sobrado sobre cada cupom

        | **Tempo**         | **IR** |
        | ----------------- | ------ |
        | Até 180 dias      | 22,5%  |
        | 181 a 360 dias    | 20%    |
        | 361 a 720 dias    | 17,5%  |
        | Acima de 720 dias | 15%    |

-   **Liquidez**

    -   Diária

    -   Tesouro Nacional garante a recompra (primário), podendo ser negociado no secundário 

    -   Das 18h até as 5h da manhã do dia seguinte em dias úteis

    -   Fins de semana e feriados à qualquer hora

-   **Data de vencimento**

    -   Quando vai ser devolvido o dinheiro

    -   Se vender seus títulos antes do vencimento, o valor estará
        sujeito ao mercado naquele momento (que pode ser maior ou menor
        que o previsto para o vencimento)

    -   Logo, não retire em papeis que oscilam bastante (pós IPCA)

    -   

    -   *Exemplo*

        -   Vender antecipadamente um préfixado

        -   Teoricamente, seu rendimento seria constante (curva azul)

        -   Porém, dependendo do mercado, esse papel pode ser mais ou
            menos vantajoso

        -   Logo, mais ou menos procurado

        -   Logo, mais ou menos valorizado (curva verde)

        -   Quando mais próximo do vencimento, menos ele oscila, pois o
            risco é menor
            
            ![image-20200718190848685](Images - Investimentos/image-20200718190848685.png)

-   **Como investir**

    -   Basta ter uma conta em corretora/banco (agente de custódia)

    -   Se investe pela internet, no site do Tesouro ou na página do
        agente de custódia (caso ele for "agente integrado")

    -   <http://www.clubedospoupadores.com/tesouro-direto/melhor-corretora-para-investir-no-tesouro-direto.html>

    -   <https://www.tesouro.fazenda.gov.br/tesouro-direto>

-   **Tipos**

    -   Variam quanto à forma de calcular o rendimento
    -   Pré fixados ou pós fixados atrelados à SELIC ou ao IPCA
-   **Juros semestrais**

    -   Você se paga mais IR no início, nos primeiros juros semestrais. Se fosse feita a retirada apenas ao fim do contrato, a taxa de IR seria menor.
    -   O valor de mercado de papeis com juros semestrais oscilam mais??

### Pós fixado Selic

-   LFT (Letras Financeiras do Tesouro)

-   Indexado à 100% da Selic

-   Risco → baixíssimo

-   Só recebe os juros no vencimento

-   Bom pra curto prazo (pois a Selic é mais estável e permite maior liquidez)
    
-   Vale a pena se os juros sobem ou se a inflação diminui

![image-20200718190904412](Images - Investimentos/image-20200718190904412.png)

### Pós fixado IPCA (inflação)

-   NTN-B Principal (Notas do Tesouro Nacional) → recebe no vencimento

-   NTN-B (Notas do Tesouro Nacional série B) → recebe juros semestrais

-   Risco → baixo-médio

-   **Dicas**

    -   Bom pra longo prazo
    -   Só resgate no vencimento: oscila bastante a curto prazo

-   **Rentabilidade**

    -   Definida parcialmente na compra + inflação do período

    -   Ou seja, SEMPRE vai ser maior que a inflação

    -   Se os juros sobem eles desvalorizam

    -   Se os juros caem eles valorizam

        ![image-20200718190919895](Images - Investimentos/image-20200718190919895.png)
    
        ![image-20200718190932404](Images - Investimentos/image-20200718190932404.png)

### Pré fixados 

-   LTN (Letras do Tesouro Nacional) → recebe no vencimento

-   NTN-F (Notas do Tesouro Nacional série F) → recebe juros semestrais

-   Bom pra longo prazo

-   Risco → médio

-   **Rentabilidade **

    -   Totalmente definida no momento da compra

    -   Se os juros sobem, você se fode

    -   Se os juros caem, você lucra

    -   [Especulando no
        pre-fixado](https://andrebona.com.br/como-especular-no-tesouro-direto/)
        
        ![image-20200718190946823](Images - Investimentos/image-20200718190946823.png)
        
        ![image-20200718191000972](Images - Investimentos/image-20200718191000972.png)

## Poupança

* 70% da selic + taxa base

## CDB

* Cerificado de depósito Bancário

* Empresta dinheiro para o banco emprestar para os outros

* **Ampliação mínima normal** → 2k \~ 10k

* **Imposto de Renda** → regressivo (ver *Tesouro Direto*)

* **Liquidez**: varia entre cada banco; somente no primário

* **Risco **

  * Baixo

  * Garantido pelo FGC

* **Tipos**
* Dá pra investir em CDB de outros bancos, através de corretoras
    * <u>Pós fixado</u>
    * Varia de acordo com o CDI ou Selic
        * Mais seguro
        * Bom quando os juros vão subir
    * <u>Pré fixado</u>
        * Rendimento determinado no momento da aplicação
        * Bom quando os juros vão cair
    * <u>Hibrido</u>
        * Renda determinada + IPCA

## LCI/LCA

* Letra de Crédito Imobiliário/Agrário

* Atrelado aos financiamentos ofertados pelo banco

* **Aplicação mínima normal** → 10k

* **Imposto de renda** → isento

* **Liquidez:** baixa (mesmo no mercado secundário), com carência de geralmente 90 dias

* **Risco**

  * Baixíssimo

  * Atrelado a um lastro real (imóvel)

  * Garantido pelo FGC

* **Tipos**

  * Pós fixada → paga uma % do CDI

  * Pré fixada → rentabilidade determinada

* **Comparando com CDB**

  * Como LCI/LCA é isento, é foda comparar

  * Sente o Bizú:
  $$
  Rendimento\ do\ LCI\ comparado\ ao\ CDB = \ \frac{\text{Taxa\ LCI}}{(1 - IR\ do\ CDB)}
  $$

  * <u>Exemplo</u>
  * LCI que rende 92% do CDI;
    
  * CDB que rende 122% do CDI com 22,5% de IR
    
  * 92/(1-0,225)=119%
    
  * Nesse caso, CDB vale mais a pena

## CRI/CRA

* Certificado de recebíveis imobiliários/agrário

* Atrelado ao recebimento futuro de aluguéis (empresas)

* **Aplicação mínima normal** → 300k

* **Imposto de renda →** isento

* **Liquidez →** zero no primário (paga só no vencimento), estável no secundário

* **Rendimentos**

  * Geralmente atrelados à inflação

  * CRA paga mais

* **Riscos**

  * Baixo médio

  * Baixa liquidez
    
  * CRA costumam ser mais arriscados

  * Não garantido pelo FGC

## Debêntures

* Or *bonds*
* Títulos de dívida emitidos por grandes empresas
* **Imposto de renda →** paga (alguns são isentos, atualmente)
* **Rendimento →** indexado à inflação ou CDI
* **Liquidez:** zero no primário (só no vencimento), +-estável no secundário
* Empresas menores (portanto mais arriscadas) = rendimento maior
* **Riscos**
  * Médio
  * Caso a empresa quebre, tem prioridade no pagamento
  * Bem menos arriscado que ações
  * Não garantido pelo FGC

# Renda variável

## Ouro

-   Oscilação senoidal

-   É um investimento fucking seguro (alto lastro)

-   Seu valor não é afetado pela inflação (o governo não pode "imprimir" ouro)
    
-   Porém, só vale a pena no longo prazo (tipo 3 anos)

-   **Rentabilidade**

    -   Valoriza em tempos de crise

    -   Desvaloriza em tempos de economia favorável

-   **Contratos na bolsa**

    -   250g (uns R\$ 35.000)

    -   10g (uns R\$ 1.400)

    -   0,225g (baixissima liquidez)

-   **Fundos de investimento**

    -   Geralmente a partir de R\$ 5.000

    -   Taxa de adm. Baixa e bem legalzinho

## Câmbio

-   Investir em outras moedas

-   Comprar na baixa e vender na alta

-   **Fundos cambiais**

    -   Se a economia tiver estável, vale a pena

    -   Se não é muito arriscado

## Derivativos

-   Investimentos que dependem da valorização de um ativo

-   Geralmente negociados na bolsa


## Fundos de Investimento (FI)

-   Muitas pessoas dão dinheiro para um gestor investir
-   Pode ou não ser atrelado à algum indexador (CDI, por exemplo)
-   A taxa de administração é cobrada sobre o montante, independente do
    fundo estar seguindo a lucratividade esperada ou não. Ou seja, se o
    fundo der prejuízo você se fode duas vezes, pois a taxa será cobrada
    igual.
-   Alguns fundos cobram também uma taxa de desempenho (que vai pro
    gestor), que só é aplicada caso os rendimentos ultrapassarem um
    nível show de bola
-   A composição da carteira, forma de distribuição dos rendimentos,
    taxas cobradas, aplicação mínima e lucratividade/risco estimado são
    especificados no regulamento
-   **Principais siglas**
    
    -   CP → Curto Prazo (quase sempre são indexados, para garantir
    liquidez)
    
    -   REF → referenciado (segue um índice)

### **Principais tipos**

-   **FIM** → Fundo de Investimento Multimercado
-   **FIA** → Fundo de Investimento em Ações
-   **RF** → Renda Fixa
-   **FIC**
-   Fundo de Investimento em Cotas
    -   Você investe nesse fundo para ele investir em outros
    
    -   "Fundo de fundos"
-   **FIDC**
    
    -   Direitos creditórios
    -   Compra o que a empresa deveria receber em uma data futura
    -   Imposto de renda → paga
    -   Rendimento → indexado à inflação ou CDI
    -   Riscos → é um fundo, depende do gestor
-   **Exchange Traded Fund (ETF)**

    -   é um fundo de ações que busca replicar a  carteira de um índice de referência no mercado, como o Ibovespa, por  exemplo.
-   **FII**
    -   Fundo de Investimento Imobilíario
    -   Tijolo: atrelado à imóveis físicos; mais seguro e estável
    -   Papel: ?
    -    IR: o dividendo é isento, e IR é aplicado sobre o lucro no momento da venda da cota (25%?)
    -    negociadas em bolsa
    -   (-) não costumam ter boa liquidez
    -   dividendos pagos todos os meses?
    -   pode ser monoativo ou multiativo (e nesse caso podem estar todos no mesmo local ou não)
-   

## Ações

-   Partes de uma empresa
-   **Tipos de ações**

    -   <u>Ordinárias</u>
        -   Direito e voto
    
    -   <u>Preferenciais</u>
        -   Prioridade na distribuição do lucro
-   **Acesso**
    -   <u>Home broker</u>
        -   principal meio de acesso à bolsa de valores
        -   usuário opera por conta própria
        -   (+) mais barato
        -   (+) agil
    -   <u>Mesa de Operações</u>
        -   operadores da corretora
-   **indices**
    -   Quem gerencia esses indices?
    -   <u>Ibovespa</u>
        -   indice de referencia?
        -   60 a 70 empresas
        -   trocadas a cada 4 meses
        -   participação proporcional à…?
        -   1 ponto = 1 real
        -   revisão a cada 4 meses
    -   <u>MSCI China</u>
    -   MSCI Europe
-   **Taxas**
    -   taxa de corretagem: por ordem executada

* **IPO**
  * periodo de resera: ?
  * você coloca o preço que você acha justa
  * Do ponto de vista da empresa: acesso à capital, diluição da empresa


* **Follow On**

  * similar ao IPO
  * a empresa já tem ações na bolsa, a empresa coloca mais 

### Estratégias

-   Análise técnica: ?
-   Análise fundamentalista: ?
-   stop loss: se menor, vende
-   stop gain: se maior, vende
-   **Day trade**
    -   Comprar e vender no mesmo dia
-   **swing trade**
    -   curto prazo: dias a semanas
    -   espera por eventos específicos
    -   compra e venda da mesma ação em dias diferentes
-   **buy and hold**
    -   empresas que passam por alguma crise ou estão subvalorizadas e aguarda períodos de médio e longo prazo com expectativa de valorização do ativo em carteira

### Outras anotações

* mercado fracionado = menos de 100 unidades (lote padrão)?
* oferta primária: vai direto pra empresa
* oferta secundária: vai algo pra empresa? 
* Ações terminadas com 3 (na B3): ordinários
* Ações terminadas com 4 (na B3): preferenciais
* 
* 
* geralmente quando o dolar sobe as ações caem
* dividendos são depositados na conta
* [Algumas ações brasileiras](https://www.infomoney.com.br/cotacoes/empresas-b3/)
* BDR: 
  * ações de empresas exteriores negociadas na bolsa brasileira
  * São como açoes normais
  * você fica  sujeito tanto à variação da bolsa quanto à variação da moeda
  * quando o dolar sobe, o seu DBR se valoriza também
* **exchange-traded fund (ETF)**
  * ou *fundo de índice*, ou *Ishares*
  * grupo de ações montado de acordo com critério
  * gestão “passiva”: os ativos são comprados, e pronto
  * Listados na B3 normalmente
  * possuem taxa de administração (~0.3 ao ano)
  * nacionais ou não, não faz diferença
  * <u>Tipos</u>
    *  de Índice
    * de Títulos
    * de Ações
    * de Commodities
* **bons indicadores**
  * múltiplos (P/L – preço sobre lucro, P/VPA – preço sobre valor patrimonial da Ação e P/FCF – preço sobre fluxo de caixa livre) abaixo da média do mercado

# Previdência

-   **Previdência Fechada**

    -   Pensão

    -   Oferecida aos funcionários de uma empresa

-   **Plano gerador de benefício livre (PGBL)**

    -   O valor pago pode ser abatido do importo de renda

    -   Bom pra quem paga IR completo

-   **Vida gerador de benefício Livre (VGBL)**

    -   O valor investido não pode ser abatido do IR

    -   Ao sacar o dinheiro, o IR é cobrado sobre o rendimento

    -   Bom pra quem declara imposto simplificado ou nem declara

# Pecúlio

-   Caso o cara morrer, o dinheiro investido é entregue à família

* * 