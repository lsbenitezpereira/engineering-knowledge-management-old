# Conceituação

-   Por que as pessoas compras? Muitas vezes elas selecionam a "meta"
    mais importante a ser cumprida, e simplesmente escolhem a marca que
    chega mais perto disso, descartando os outros critérios

-   Brand development Index (BDI) → relação percentual entre o valor das
    vendas per capita em uma certa região e a média nacional

-   Category development index (CDI) → idem, mas para toda a categoria

-   **E-business**

    -   Atividades online que facilitam a troca de produtos e serviços

    -   Ou seja, é toda a cadeia de valor online, não só a venda em si

-   **Tipos de negócios **

    -   *Business to business* (B2B)
    -   Business to consumers (B2C)
    -   *Costumer to business* (C2B) → dar informações de pessoas para
        empresas
    -   *Consumer to consumer* (C2C) → transações entre pessoas

## Psicologia de marketing

-   **gatihlos mentais**
    -   *Urgência: "não perca tempo, se inscreva..."*

    -   Escassez: "está acabando"
-   **Venda por lançamento**
-   **Saturação**
    -   Increases in advertising or sales calls on customers may, after a  certain level (the saturation point), fail to yield any additional  response from customers. Beyond a still higher level (the  supersaturation point) they may produce decreased response

## Estratégias de marketing

-   Pense, ouça e tente entender o seu cliente

-   Quando sua estratégia de marketing atacar um concorrente ou mirar
    diretamente seus clientes, prepare para uma resposta competitiva

-   Quando for investir em construir uma categoria E construir uma
    marca, use estratégias diferentes nas duas ações, evitando confundir
    o consumidor

-   **Estratégia do ponto de entrada**

    -   Casar seu marketing com momentos bem definidos na vida do
        cliente

    -   Ex: capturar e fidelizar uma mãe à uma marca infantil logo
        quando a criança nasce

    -   É o cliente que será novo na categoria, e não o produto que será
        novo no mercado

# Branding

-   Desenvolvimento da marca

-   Não venda só o produto/solução: venda a marca

-   Uma marca é um significado, algo na mente do cliente

-   A essência da sua marca deve conectar-se aos objetivos do consumidor

-   **Brand equity**

    -   Valor da marca, seu patrimônio imaterial

    -   Percepções que o consumidor associa a marca

-   **Personalidade da marca**

    -   Características que podem ser relacionadas à personalidade:
        sinceridade, entusiasmo, competência, sofisticação e aspereza

-   **Expressões da marca**

    -   Denominação, figura, simbolismo, animação, descrição e
        palavreado

    -   Devem ser construídas pensando na história do consumidor,
        acrescentando valor e significado

-   **Como fazer branding**

    -   Comunicação interna é fundamental: envolva todos os
        departamento, de forma a construir o branding a partir da
        identidade da empresa

    -   Sua marca deve ser sólida, e não uma carcaça maquiada, senão o
        consumidor irá acabar percebendo (ainda mais que hoje temos uma
        grande complexidade de mídias e diversas fontes de informação)

-   **Tipos de marca**

    -   Funcional → foque no desempenho ou economicidade superior

    -   De imagem → foque na divulgação dos seus valores

    -   De experiência → forneça algo único e envolvente

## Posicionamento 

-   Facilitar o entendimento do consumidor, geralmente associando o seu
    produto a algo que ele já conhece (uma categoria já existente, por
    exemplo)
-   Um posicionamento de sucesso envolve afiliar uma marca a alguma
    categoria que os consumidores possam compreender e reconhecer de
    imediato, além de diferenciá-la de outros produtos na mesma
    categoria
-   Informar os consumidores sobre a associação de uma marca antes de
    afirmar qual é o diferencial dela. É interessante fazer isso em duas
    campanhas separadas.

-   **Declaração de seu posicionamento**
-   Pública e clara (principalmente para seus próprios funcionários)
    
-   Não deve ser genérica
    
-   Deve deixar claro quem é o publico alvo
    
-   Deve ser atraente
-   **Diferencial**

    -   Primeiro identifique as crenças arraigadas no consumidor

    -   Geralmente é mais caro tentar mudar as crenças do consumidor do
        que apelar para uma crença já existente. Mas, se um outro
        concorrente já domina esse "nicho de pensamento", talvez seja
        necessário reeducar o consumidor

## Segmentação

-   Quando segmentar?

    -   Quando as respostas dos clientes às estratégias de marketing
        forem muito diferentes

    -   Quando diferentes segmentos possuem metas diferentes e
        conflitantes. Criar duas marcas separadas é uma boa opção

# Marketing digital

* customer lifetime value: ?

## Inbound marketing 

* The client triggers the contact, not the company

* must be interesting for the _client_

* How? Quality content, etc

* **Compensation methods**
  * CPM (cost per mille): Typically used for graphical/banners; can be paid in advance
  * CPC (cost per click): Typically used for textual ads
  * CPT/CPA = cost per transaction/action; ?
  * CPE (cost per engagement)
  * CPV (cost per view)
  * CPI (cost per install)
  * Attribution of ad value
  * Other performance-based compensation
  * Fixed cost


-   **Débora brauhardt**

    -   Gerente de marketing da Resultados Digitais

-   **Dicas**

    -   É o marketing que gera a demanda

    -   Trabalhe em todo o funil de marketing

<img src="./Images/3.0 - Marketing/media/image1.png" alt="Resultado de imagem para funil de
marketing" style="zoom:30%;" />

-   **Como criar leads**

    -   Tenha uma oferta

    -   Crie uma [landing page]{.underline}

    -   Promova

    -   Qualifique os leads
-   **Email marketing**

    -   Crie um calendário

    -   Segmentação

    -   Campanha (cuidado com o texto: menos é mais)

    -   Analise os resultados
-   **Planejando conteúdo**

    -   Defina o tema
-   Crie um calendário
    -   Cria um BLOG e CTA
-   Fazer uns posts (mantenha a regularidade estipulada)
    -   Fazer materiais (ebooks, etcs)
-   **Industry Authorities**
    -   Interactive Advertising Bureau (IAB)
    -   Direct Marketing Association (DMA)
-   **Materials**
    -   [MediaOcean guide](https://www.mediaocean.com/digital-marketing-guide)

## Email marketing

media: 20% de abertura, 5% de click

## Indicators

* https://www.youtube.com/watch?v=dssGTv5AAeY
* Metas de performance (conversion, consideration)
  * Visitas no meu site ou loja virtual
  * Downalods do meu aplicativo
  * Numero de leads
* Metas de awareness
  * Promover meus produtos
  * Reconhecimento de marca
* Others
  * Click through rate
* https://www.stitchdata.com/resources/google-analytics-metrics-for-marketers/
* **Audience** helps you explore *who* your  customers are, including information such as demographics, location,  retention, and device technology. With these metrics, you can interpret  the impact of your marketing efforts on various user segments.
  * Number of users
  * Ratio of new to returning visitors
  * Average session duration
  * Average pages per session
* **Acquisition** shows you *how* customer get  to your website. In the Channels section under All Traffic, you can dig  into what channels (organic traffic, social media, email, ads, etc.)  deliver the most traffic. You can compare incoming visitors from  Facebook versus Instagram, determine the efficacy of your SEO efforts on organic search traffic, and see how well your email campaigns are  running.
* **Behavior** explains *what* customers do on  your website. What pages do they visit? How long do they stay? You can  examine these metrics to understand the overall user experience and its  effects on retention and engagement.
* **Conversions** tracks whether customers take actions that you *want* them to take. This typically involves defining funnels for important  actions — such as purchases — to see how well the site encourages these  actions over time.
* **Scope of the metrics**
  * user-level, session-level, or hit-level
    only makes sense to combine dimensions and metrics that share the same scope.

## Programmatic advertising

* Programmatic Buying includes the use of DSPs, SSPs and DMPs. DSPs, (demand side platforms) which facilitate the process of buying ad inventory on the open market, provide the ability to reach your target audience due to the integration of DMPs (data management platforms).

* opposed to the traditional media buying that involves RFT (request for proposal)

* buying method accomplished via algorithmic technology

* Usually is highly data-driven

* purchasing digital ad space at a large scale

* 

* In 2018, [over 80% of digital display marketing in the US was done via programmatic advertising](https://www.appnexus.com/sites/default/files/whitepapers/guide-2018stats_2.pdf). 

* impression: each shown add

* **OpenMedia project**

  * provides standards for programmatic marketplaces of advertising

  * https://iabtechlab.com/standards/openmedia/

    Layer 1: moves bytes among parties

  * Layer 2: expresses the language of  these bytes

  * Layer 3: specifies a transaction using this language

  * Layer 4: describes the concepts being transacted

* **Problems**

  * Bot seeing the ad
  * people are on page, but didnt scrolled till the ad
  * those problems can be audited and do not computed as “shown ad”
  
* **ads.txt**

  * Authorised Digital Sellers

  * Normatizado pela IAB

  * informa para a DSP quem (SSP?) pode comercializar aquele site

  * esclarecer quais os parceiros autorizados a vender os espaços publicitários de  cada veículo e evitar a compra de inventário ilegítimo

  * Assegurando que as DSPs tenham a garantia de comprar o produto que realmente estão procurando e não uma impressão fake.

  * Várias linhas do tipo:

    ```
    < SSP/Exchange Domain >, < SellerAccountID >, < PaymentsType >, < TAGID >
    ```

    

### Roles

* **Advertiser**
  * advertiser/buyer or agency on its behalf
* **Agency ad server**
  * Talks with advertiser and deliver its ad
  * records the impressions
  * Examples: verizon, double click
* **DSP**
  * demand side platform
  * cliente 1: Agency ad server (who wants to be advertiser)
  * cliente 2: ad exchange
  * Place the real-time bid on behald of the advertiser
  * Examples: google bid manager, dataxu, MediaMath, and Turn
  * Allows very precise audience targeting
  * demand-side platforms () and three sell-side platforms ()
* **Ad exchange**
  * client 1: SSP
  * client 2: DSP
  * Determine the winning bid
  * Example: Nexage, yahoo right midia
* **SSP**
  * Supply side Platform, or *sell side*
  * client 1: ad exchange
  * client 2: publisher ad server (who wants to put an add)
  * Examples: Pubmatic, rubicon, Admeld
* **publisher ad server**
  * Or *ad network*
  * client 1: publisher
  * client 2: ssp
  * Pack the publisher’s inventory and make it ready to sell
  * Example: microsoft midia network
* **Publisher**
  * website owner
  * Examples: CNN, Estadão
* **Trade desk**
  * toda a cadeia
  * fácil pro usuario
  * Examples: google ads, facebook ads

### RTB

* Or *Open Auction*, or *Open Marketplace*

* Automatic purchsed at individual impression level 

* variable price (by auction, *leilão*)

* bidding happes in miliseconds

* RTB accounts for more than 90% of all programmatic buying

  ![img](Images - 3.0 - Marketing/How-programmatic-advertising-works.png)

* **Main standard: IAB OpenRTB**

  * RTB = Real Time Bidding
  * lances em até 0.5s
  * automatic sell: buyer doesnt talk with seller
  * floating price and schedule

* **Auction types**

  * Primary: highest bid win, and pay the price it bid
  * Secondary: highest  bid win, and pay 1 cent more than the second bid
  * ==severals rounds are done in sequence?==

* **depth**

  * good for publishers, bad for buyers
  * number of eligible demand sources participating in an auction.
  * heigher depth leads to higher price
  * number of rounds of auctions
  * ==if 1, it’s a blind bid?==
  
* **automated bid strategies**

  * automatically set bids to help you reach your performance goals
  
* **header bidding**

  * ==?==
  * pergunta para todos ao mesmo tempo
  * pass back = castada de consultas

### Direct

* Or *Programmatic Guaranteed*, or *Programmatic Reserved*

* involves manual human negotiations, usually made by in-house sales teams

* pre-sold (guaranteed sell)

* inventory is guaranteed or reserved on the fixed terms between the publisher and advertiser

* ad serving still occurs automatically

* **Main standard: IAB OpenDirect 2.0**
  * https://github.com/InteractiveAdvertisingBureau/OpenDirect/blob/master/OpenDirect.v2.0.final.md
  * Creative Commons Attribution 3.0 License
  * direct sells (the sellers talks with the buyer)
  * Mais voltado para venda não por leilão
  * Automated Guaranteed is the method of buying which automates the digital direct sale.
  * The RFP (Request for Proposal) and campaign trafficking process are completely automated within the technology platform
  * pedido de inserção midia (==same as RFP?==): formulário para compra

### Private Marketplace

* Or *Private Exchange*, or *Closed Auction*
* purchasing is happening within a real-time auction, but the buyer must be approved by the publisher beforehand

### Preferred Deals

* Pre-negotiated fixed price, but the inventory remains unreserved
* First offer to one specific advertiser. If he accepts, it’s done. Else, the slot goes to RTB

## Analitycs platforms

### Google analytics

### Matomo

* alterativa opensource ao google analytics

## News and companies

* **Hulu**
  * they are an streaming service?
  * ad selector product: let the use chose what ad to see, https://advertising.hulu.com/ad-products/ad-selector/

# TV Digital

* **DLNA protocol**
  * Digital Living Network Alliance
  * permite transmitir conteúdos do computador ou celular para a TV usando streaming através da rede Wi-Fi

* **compra de mídia na TV**
  * quanto mais antecipado, mais caro
  * na ultima hora a TV vai fazer de tudo (deixar barato) para vender
  * diferente do modelo de vendas da aviação (que prefere voar com um avião vazio), a TV vai fazer de tudo para vender tudo custe o que custar 
* **TV digital no BR**
  * a RIC é a que tá mais avançada?
* * * 

# Audience targeting

* Describe and segment audiences
* **Conceps**
  * <u>Data vendors</u>
    * Provides consumer marketing data
    * Experian, Acxiom, etc
  * <u>Data management platform (DMP)</u>
    * Collect and manage data about audiences over differente sources
    * Similar to a data warehouse
    * 1st Party Data: captured by your company
    * 2st Party Data: partner’s first-party data
    * 3st Party Data: any other data source
  * <u>Analytics providers</u>: Google Analytics, MailChimp, etc
* **Private companies**
  * <u>Lotame</u>
    * Focused on TV
    *  https://www.lotame.com/products/aitv/
  * <u>Rubicon project</u>
    * ad exchange: compra do veiculo e vende pro buyer
    * 100% leilão (Real Time)
    * tem mais proximidade com o veiculo
    * vende só em grande quantidade
* **Standards**
  * [ISO 20252](https://www.iso.org/standard/73671.html)
  * [IAB Audience Taxonomy](https://iabtechlab.com/standards/audience-taxonomy/) (registered below)

##  Taxonomy

* Each level is called tier 1 (demographics, purchase…), tier 2 (gender, age…), etc
* [Audience Taxonomy 1.0](https://iabtechlab.com/standards/audience-taxonomy/)
* Condensed name: unique identification of each class: `1st | 2nd | Last Tier`
* 1679 classes
* **Demographic**
  * statistical study of the audience
  * gender
  * age
  * finatial statusetc
  * Language
  * Marital Status
  * Personal Finance
    * Income (USD)
    * Personal Level Affluence (USD)
  * Personal Level Affluence Band: Negative Net Worth, Very Low Net Worth, Low Net Worth, Mid Net Worth, High Net Worth, Super High Net Worth
  * household composition
    * Home Location
    * Household Income
    * Length of Residence
    * Life Stage: Single Generation Household, Multi Generation Household
    * Median Home Value (USD)
    * Monthly Housing Payment (USD)
    * Number of Adults
    * Number of Children
    * Number of Individuals
    * Ownership: Home Owners, Renters, Owner, Renter, First Time Homeowner
    * Property Type: Multiple Family, Single Family
    * Urbanisation (number of inhabitants)
  * Education & Occupation
    * Education (Highest Level)
    * Employment Role
    * Employment Status
  * Politics
* **Purchase intent**
  * Short term interest in the buying
  * Apps (which categories of apps)
  * Arts and Entertainment
  * Automotive Ownership: New Vehicles, Pre-Owned Vehicles
  * Automotive Products
  * Automotive Services
  * Beauty Services
  * Business and Industrial
  * Clothing and Accessories
  * Collectables and Antiques
  * Consumer Electronics
  * Consumer Packaged Goods (which edibles, which non edibles)
  * Education and Careers
  * Family and Parenting
  * Finance and Insurance
  * Food and Beverage Services
  * Furniture
  * Gifts and Holiday Items
  * Hardware Supplies
  * Health and Medical Services
  * Hobbies and Interests
  * Home and Garden Services
  * Legal Services
  * Life Events
  * Logistics and Delivery
  * Mature: Alcoholic Beverages, erotic, firearms, tobacco, Attourneys, etc
  * Non-Profits
  * Office Equipment and Supplies
  * Pet Services
  * Pharmaceuticals, Conditions, and Symptoms
  * Real Estate
  * Recreation and Fitness Activities
  * Software
  * Sporting Goods
  * Travel and Tourism
  * Web Services
* **Interests**
  * medium and long term interest/behavioral patterns
  * Academic Interests (field)
  * Automotive (type of vehicle preference)
  * Books and Literature
  * Business and Finance
  * Careers
  * Education: Adult Education, Language Learning, Online Education
  * Family and Relationships
  * Fine Art
  * Food & Drink
  * Health and Medical Services
  * Healthy Living
  * Hobbies & Interests
  * Home & Garden
  * Medical Health
  * Movies
  * Music and Audio
  * News and Politics
  * Personal Finance
  * Pets (which animals)
  * Pharmaceuticals, Conditions, and Symptoms
  * Pop Culture
  * Real Estate
  * Religion & Spirituality
  * Shopping
  * Sports
  * Style & Fashion
  * Technology & Computing
  * Television
  * Video Gaming

## Content tagging

* IPTC Subject Codes
* IAB content taxonomy

# Outras anotações

* Venda “soluções”, e não “produtos”  
* Estratégias para enfrentar a concorrência: custo, diferenciação ou foco  

## **OTT**

* conteúdo transmitido pela internet para a TV, seja ou não ao vivo
* Example: netflix