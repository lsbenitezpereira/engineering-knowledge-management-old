# Software oriented licenses

* **SSPL**
  * By using an SSPL project in your code, you are agreeing that if you  provide an online service using that code then you will release not only that code but also the code for every supporting piece of software, all under the SSPL
  * is opensource… but an extremely aggressive one, and not considered one by the  Open Source Initiative (OSI)