

# Antropology

* Human history, as something separate from the history of animals, began there about 7 million years ago
  
* 4 million years ago: upright posture

* Australopithecus 		→ 3 milhões de anos

* 2.5 million years ago: Stone tools became common

* Homo habilis 		→ 2,4 e 1.5 milhões

* Homo erectus 		→ 8 milhões até 300.000 aC

* 500k years ago

  * Homo erectus evolved into Homo sapiens
  * use of fire
  * Homem de Neandertal 	→ 230.000 até 30.000aC
  * 130,000 and 40,000: Homo neanderthalensis competed with Homo sapiens
  * Homo Sapiens 		→ 120.000, paralelo ao Neandertal
  * 

* 50k years ago: Great Leap Forward
  
  * People termed Cro-Magnons
  * Begun probably in East Africa, then they expanded to the rest of the world (sustituing the poulations in Eurasia and colonizing the others)
  * More advanced tools
  * Art
  * Language
  
* 13k BC: first inital sedentary lifestyle, in some areas of the world

* 11k B.C: Americas were first settled around (but maybe sooner)

  * what was sea at that time? there was a corridor of dry land connecting Asia to Australia? https://en.m.wikipedia.org/wiki/Sundaland
  * :man_scientist: new findings? https://www.nature.com/articles/d41586-020-02137-3 sugests about 20kBC

* The spread of humans around the world:

  ![image-20191208134100292](Images - Antropology/image-20191208134100292.png)
  
  ![image-20200920002052294](Images - Antropology/image-20200920002052294.png)
  
* ?:

    ![image-20200906174648687](Images - Antropology/image-20200906174648687.png)

# Other annotaitons

Homo sapiens sapiens: aquele que sabe que sabe (ou seja, mais do que técnica, o homem pensa, reflete, compreende). ==isso tem algum fundamento????==