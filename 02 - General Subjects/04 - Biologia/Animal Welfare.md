# Conceptualization

* the physical and mental state of an animal in relation to the conditions in which it lives and dies.
* We’ll focures here in Terrestrial Animals welfare (rules by the standard *Terrestrial Code* from 2004)
* **Main standards**
  * OIE
    * main organization in Animal Health
    * OIE = French acronym Office International des Epizooties
  * ISO/TS 34700:2016
    * provide the management practical aspect to achieve the OIE recomendatios 
  * UE Welfare Quality
    * http://www.welfarequality.net/en-us/home/
    *  The 12 criteria, the causes of associated welfare problems and potential remedies
       Criterion 1: Absence of prolonged hunger
       Criterion 2: Absence of prolonged thirst
       Criterion 3: Comfort around resting
       Criterion 4: Thermal comfort.
       Criterion 5: Ease of movement (other than health or resting-related
      issues).
       Criterion 6: Absence of injuries (except those due to disease or
      therapeutic or preventative interventions).
       Criterion 7: Absence of disease (as well as neonatal and transport-related
      mortality).
       Criterion 8: Absence of pain induced by management procedures
      (including stunning).
       Criterion 9: Expression of social behaviours (balance between negative,
      e.g. prolonged and damaging aggression, and positive aspects, e.g. social
      licking).
       Criterion 10: Expression of other welfare-related behaviours (balance
      between negative, e.g. stereotypies, and positive behaviours, e.g.
      exploration).
       Criterion 11: Good human-animal relationship (reduced fear of humans).
       Criterion 12: Positive emotional state.

## Ethology foundations

* Study of animal behavior
* Behaviourism
  * describes the scientific and objective study of animal behaviour
  * Uses measured responses to stimuli or to trained behavioural responses in a laboratory context, without a particular emphasis on evolutionary adaptivity
  * For behaviorist in Psicology, see the specific file 

# OiE Terrestrial code

* **Five Freedoms**
  * freedom from hunger, malnutrition and thirst;
  * freedom from fear and distress; 
  * freedom from heat stress or physical discomfort; 
  * freedom from pain, injury and disease; and
  * freedom to express normal patterns of behaviour.
  * Each freedom is accompanied by one provision, about how to ensure that freedom 
* **Scope**
  * Transport
  * slaughter of animals for disease control  purposes
  * Use in   research  and education
  * stray dog population control 
  * working  equids
  * production systems
    * beef/dairy cattle
    * broiler chickens
    * pigs
* **Welfare Management levels**
  * Farm management
    * human resources
    * animal management
  * animal handler level
    * Skill in handling
    * Knowledge
* **Criterias**
  * or *measurables*
  * outcome-based
  * Can be considered as a tool to monitor the impact of design and management
  * Highly subjective
  * Need to be analysed by humans
  * Can we automate the evaluation of animals? 

## Dairy cattle production systems

* https://www.oie.int/index.php?id=169&L=0&htmfile=chapitre_aw_dairy_cattle.htm
* Housing

  * Housed
  * Pastured
  * Combination systems
* **Enviroment recomendations**
  * :red_circle:
* **Practice recommendations**
  * :red_circle:

### Measurables

* **Behaviour** 
* Negative: decreased feed intake, altered locomotory behaviour and posture, altered lying time, altered respiratory rate and panting, coughing, shivering and huddling, excessive grooming and the demonstration of stereotypic, agonistic, depressive or other abnormal behaviours.
* **Morbidity rates** 
  * infectious and metabolic diseases, lameness, peri-partum and post-procedural complications and injury rates, above recognised thresholds
  * Mastitis, and hoof, reproductive and metabolic diseases are also particularly important animal health problems for adult dairy cows
  * Scoring systems, such as for body condition, lameness and milk quality, can provide additional information
* **Mortality and culling rates**
* **Changes in body weight and body condition**
  * In lactating animals, body condition outside an acceptable range, significant body weight change and significant decrease in milk yield may be indicators of compromised welfare.
* **Reproductive efficiency**
  * anoestrus or extended post-partum interval, low conception rates, high abortion rates, high rates of dystocia, retained placenta, metritis, loss of fertility in breeding bulls.
* **Physical appearance**
  * presence of ectoparasites, abnormal coat colour, texture or hair loss, excessive soiling with faeces, mud or dirt (cleanliness), swellings, injuries or lesions, discharges (e.g. from nose, eyes, reproductive tract), feet abnormalities, abnormal posture (e.g. rounded back, head low), emaciation or dehydration.
* **Handling response**
  * evidence of poor human-animal relationship, such as excessive flight distance,

  * negative behaviour at milking time, such as reluctance to enter the milking parlour, kicking, vocalisation,

  * animals striking restraints or gates,

  * injuries sustained during handling, such as bruising, lacerations, broken horns or tails and fractured legs,

  * animals vocalising abnormally or excessively during restraint and handling,

  * disturbed behaviour in the chute or race such as repeated reluctance to enter,

  * animals slipping or falling.
* **Complications from common procedures** 
  * post procedure infection, swelling and pain behaviour,
  * reduced feed and water intake,
  * post procedure body condition and weight loss,
  * morbidity and mortality

## Pigs production systems

* best practice in housing and husbandry and implementation of welfare protocols and audits
* https://www.oie.int/index.php?id=169&L=0&htmfile=chapitre_aw_pigs.htm
* Stereotype behaviours
  * abnormal repretitive and without obvious purpose/function
  * chewing, stone chewing, tongue rolling, teeth grinding, bar biting and floor licking
* Enviroment recomendations
  * Air quality
  * Thermal confort
  * Noise
  * Light
  * Rich: complexity, manipulability and cognitive stimulation
  * Space allowance
    * different areas for lying, standing, feeding and elimination
    * Adequate surfases: Flooring, bedding, resting

* Criterias
  * Behaviour 
  * Morbidity rates 
  * Mortality and culling rates
  * Changes in body weight and body condition
  * Reproductive efficiency
  * Physical appearance
  * Handling response
  * Lameness 

# Positive emotions

* Environmental enrichment is often proposed as the way to
  enhance positive experiences in farm and laboratory animals.
* Enriching the envioroment
  * creating a situation where there is
    anticipation of a positive reward
  * offering more space to
    promote play
  * opportunities for positive contrast
    situations
  * opportunities for improving coping abilities
  * opportunities for information
    gathering

## Indications

* **play**
  * that is an rewarding activity
  * specially important in young animals
* **Affiliative behavior**
  * or *sociopositive*
  * maintaining proximity, providing food or protection or
    allogrooming between specific individuals
  * It is stronger between family-related animals 
  * appear to be promising indicators of long-term positive affective states in farm animals
  * social licking
    * subordinate animals might experience performance
      of the behavior as stressful if licking has been initiated by
      dominant animals
* **Self-grooming**
  *  maintenance of
    one's own body surface.
  *  licking, scratching, and rubbing of the fur, as
    well as through wallowing and bathing
* **Vocalizations**
  * some vocalizations can indicate positive states
* **Information gathering and exploration**
* Others goal directed behaviors