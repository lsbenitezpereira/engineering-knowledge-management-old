# Sensorial system

-   São os clássicos 5 sentidos

-   Como todos eles estão relacionados à percepção e processamento dessa
    informação, são estudados juntos com o sistema nervoso, apesar de
    que algumas características anatômicas são estudadas separadamente

-   Transdução → processo na qual um estimulo ambiental causa uma
    resposta elétrica

-   **4 elementos básicos**

    -   Receptor → terminação nervosa especializada na transdução

    -   Trajeto periférico → do nervo espinal/craniano até um gânglio
        sensitivo

    -   Trajeto central → via eferente até o encéfalo

    -   Projeção cortical → região do córtex cerebral ou cerebelar

## Gustação

-   Papilas gustativas → Células Receptoras Gustativas → podem ser do
    tipo Filiformes, Fungiformes, Circunvaladas, Foliáceas

-   Faringe, palato e epiglote também têm um pouco de sensibilidade
    gustativa

![Imagem
relacionada](./Images/3.0 - Sistema Sensorial - Gustação e Olfato//media/image1.jpeg){width="3.586443569553806in"
height="2.7264140419947505in"}

![img](./Images/3.0 - Sistema Sensorial - Gustação e Olfato//media/image2.png){width="5.081482939632546in"
height="3.5in"}

## Olfato

-   Percebem substâncias químicas no ar

-   As Células Receptoras Olfativas são neurônios mesmo (diferente das
    gustativas)

-   A superfície do nosso Epitélio Olfativo (região de transdução) tem
    cerca de 10cm². Alguns cães têm até 170cm² com 100x mais densidade
    de receptores

![Resultado de imagem para epitelio
olfativo](./Images/3.0 - Sistema Sensorial - Gustação e Olfato//media/image3.jpeg){width="6.104166666666667in"
height="2.7385629921259844in"}

-   **Feromônios**

    -   Substâncias químicas liberadas pelo corpo que desempenham
        diversas funções, principalmente sexuais

    -   Estudos recentes indicam que eles têm muito mais influência no
        nosso comportamento do que pensamos
## Vision

-   O fato de termos dois olhos permite uma visão em profundidade, além
    de corrigir pontos cegos e "sombras" causadas pelas veias dos olhos

-   A distância focal é variável (depende de quão longo estamos olhando)

### Anatomia do olho humano

-   Vertebrate eyes and modern cameras use a lens system to gather sufficient light while
    keeping the image in focus. A large opening is covered with a lens that focuses light from
    nearby object locations down to nearby locations in the image plane. However, lens systems
    have a limited depth of field: they can focus light only from points that lie within a range
    of depths (centered around a focal plane). Objects outside this range will be out of focus in
    the image. To move the focal plane, the lens in the eye can change shape (Figure 24.3); in a
    camera, the lenses move back and forth.
-   **Córnea** → superfície externa
-   **Cristalino** → lente convergente, formando imagens reais,
    invertidas e diminuídas
-   **Íris** → altera o tamanho da pupila (parte colorida do olho)
-   **Pupila** → abertura que permite entrada de luz (parte preta do
    olho)
-   **Retina** → possui diversos fotorreceptores (faz efetivamente parte
    do SNC)
-   **Esclera** → exterior rígido que protege o olho (parte branca do
    olho)
-   **Músculos extraoculares** → se ficam na esclera → movimentam o olho
    → são 6 pra cada
-   **Humor vítrio →** núcleo do olho
-   **Nervo** **ótico** → reúne os axônios da retina

![Resultado de imagem para fisica
olho](./Images/3.1 - Sistema Sensorial - Visão//media/image1.jpeg){width="6.336805555555555in"
height="3.75in"}

### Fotorreceptores

-   **Bastonetes** → muito sensíveis a luz

-   **Cone**
    
    -   pouco sensíveis à luz, mas percebem cor
    -   existem três tipos de cones, +- correspondentes à vermelho, verde e azul
    -   They are present in different amounts (much less blue ones)
    
    ![image-20201018155445405](Images - 2 - Sensors and actuators/image-20201018155445405.png)
    
-   Temos 20x mais bastonetes do que cones

-   Em situações de baixa luminosidade, basicamente só os bastonetes
    enxergam

-   The photoreceptors emiter their signals in terms of chemicals; the ginglion cells will capture and transform it into electric signals
    
-   Adaptação visual → capacidade de aumentar ou diminuir a exposição à
    luz (tanto pelo diâmetro da retina quanto pela ativação dos
    diferentes fotorreceptores)

-   O processamento da informação começa na própria retina: temos 125
    milhões de fotorreceptores, mas apenas 1 milhão de axônios levam
    informação do olho ao encéfalo

![Resultado de imagem para
fotorreceptores](./Images/3.1 - Sistema Sensorial - Visão//media/image2.jpeg){width="5.413366141732284in"
height="3.6695188101487313in"}

* The photoreceptors aren’t equally distributed in the retina:

  ![image-20191215142752050](Images - 2 - Sensors and actuators/image-20191215142752050.png)
  
* There is also some mecanimsm like receptive fields, where the activation of a region is related to the nearby activations; This helps with brightness adaptation

### Problemas visuais

-   **Miopia**

    -   Imagem se forma antes

    -   Precisa de lente divergente

![Resultado de imagem para fisica
olho](./Images/3.1 - Sistema Sensorial - Visão//media/image3.png){width="3.9704604111986in"
height="3.5208333333333335in"}

-   **Hipermetropia**

    -   Imagem se forma depois

    -   Precisa de lente convergente

![Resultado de imagem para fisica
olho](./Images/3.1 - Sistema Sensorial - Visão//media/image4.gif){width="4.239681758530184in"
height="3.8333333333333335in"}

### Pathway to cortex

* There is a lot of in-situ pre processing: There are roughly 100 million photoreceptors in the retina, but only 1 million axons leave the eye carrying information to the rest of the brain

* optic nerves: leave the eyes and 

* Optic chiasm (named for the X shape of the Greek letter chi): merge the optic nerves

* Optic tracts: after chiams

  ![image-20200123111000808](Images - 2 - Sensors and actuators/image-20200123111000808.png)

* Most of the axons go to thalamus, to the lateral geniculate nucleus (LGN)

* optic radiation: from the LGN to the cortex 

* LGN 

  * have six layers 
    * 1,4 and 6 come from one side
    * 2,3,5 comes from the other side
  * what it does
    * rapid retina movements, reflexes
    * The LGN also receives synaptic inputs from neurons in the brain stem
      whose activity is related to alertness and attentiveness, modulate the magnitude of LGN responses to visual stimuli.

* 

* 

  * 

### Primary Visual Cortex (V1)

* striate cortex = primary visual cortex = V1 (visual area 1)

* The binocular vision region have alternated dominance by each eye, organized in the cortex in zebra-like strips 

* there is a pretty good corresponde between the visual field and V1 cortical area (see this [paper](https://pubmed.ncbi.nlm.nih.gov/3367210/))

* Oututs: other cortical areas, superior colliculus and pons, back to the LGN

* 6 layers:

  ![image-20200126180608606](Images - 2 - Sensors and actuators/image-20200126180608606.png)

* Receptive fields

  * Circular perception “patches” that repond to some image property
  * Most of the first layer neurons are monocular, responding to light only in one of the eyes
  * orientation selectivity (orientation selectivity of nearby neurons is related)
  * direction selectivity: they respond when a bar of light at the optimal orienta-
    tion moves perpendicular to the orientation in one direction but not in
    the opposite direction.
  * Simple cells: one orientation, one region
  * Complex cells: one orientation, more than one region (normaly close), like sums of simple cells 
  
    

### Other visual areas

 * dorsal stream: analysis of visual motion and the visual control of action. “Where” pathway

 * ventral stream: involved in the perception of the visual world and the recognition of objects. “What” pathway

   ![image-20200126181151320](Images - 2 - Sensors and actuators/image-20200126181151320.png)

* **V2**
  
  * feature extraction, like V1
  * more complex patterns, like temporal dynamic (sinusoidal, polar, etc), edges (stars, specific angles, etc)
  
* **MT**
  
  * Middle Temporal, V5
  * almost all the cells are direction selective (movement)
  
* **MST**
  
  * medial superior temporal
  * Receptive field for linear motion (direction), radial motion (inwar/outward), and circular motion (clowise/counterclockwise)
  
* **V4**
  
  *  have larger recep-
    tive fields than cells in the striate cortex, and many of the cells are both
    orientation selective and color selective
  
* **IT**
  
  *  A wide variety of colors and abstract shapes have been found to be good stimuli for cells in IT
  * Respond to very high-level features (like faces looking left or specific objetc)
  * Outputs:  to temporal lobe structures involved in learning
    and memory
  * Mainly size, orientation and color invariat 
  
* **Visual perception**
  
  * the task of identifying and assigning meaning to
    objects in space
  * How the fuck the brain does that? Bottom-up aproach using all specific receptive field? good research question
  * scenes are recognized in about 250ms

## Audition

-   Audição e sistema vestibular
    -   As funções são muito diferentes (ouvir e equilibrar)
    -   Compartilham basicamente a mesma estrutura
    -   Ambos são iguais dos dois lados da cabeça
    -   Depois do sensoriamento, a informação vai para o tronco encefálico,
        tálamo e córtex auditivo

### Anatomia do ouvido

-   **Ossículos**

    -   Se dividem em martelo, bigorna e estribo

    -   Transferem a vibração do tímpano para a cóclea

    -   Amplificam a pressão em até 20 vezes

    -   São os menores ossos do corpo

    -   São capazes de atenuar sons muito altos, porém esse reflexo de
        atenuação demora de 50 a 100 ms

-   **Cóclea**

    -   Do latim *caracol*
    -   Preenchida com um fluído chamado endolinfa
    -   Possui dois pequenos orifícios cobertos por membranas: janela
        oval (que a conecta com o estribo) e janela redonda (que
        compensa a pressão)
    -   Faz a transdução do som
        -   Cada região da membrana possui uma frequencia central de ativação (onde a intensidade da resposta é máxima)
        -   as regiões tem overlaps de frequencia, que podem ser modelados como vários filtros passa-faixa
        -   a transdução é não linear
        -   começo da coclea responde a sinais de alta frequencia

-   **Tuba** **auditiva**

    -   Ou *tuba de Eustáquio*

    -   Igualam a pressão do interior com o exterior

    -   Normalmente está fechada por uma válvula

    -   Membrana basilar → coisa longa cheia de receptores

    -   Os receptores auditivos são células ciliadas (detectam uma
        frequência em específico)

    -   Quando a membrana basilar se eleva, os receptores percebem e
        geram um potencial de membrana

    -   Escala timpânica e escala vestibular

![Resultado de imagem para anatomia
ouvido](Images - 2 - Sensors and actuators/image1.jpeg){width="4.2625in"
height="3.562202537182852in"}

![Imagem
relacionada](Images - 2 - Sensors and actuators/image2.jpeg){width="4.145833333333333in"
height="2.8224978127734035in"}

### Codificação do som

-   Cada parâmetro é codificados de maneira diferente

-   **Frequência**

    -   Cada frequência é transmitida por um grupo diferente de
        neurônios

    -   os neurônios também têm um sistema de sincronia de fase.

-   **Intensidade**

    -   Codificada pela quantidade de neurônios ativos e pela frequência
        de disparo deles
    -    só 120 diferentes níveis podem ser detectados pelo ouvido

-   **Localização horizontal**
    -   Determinada pelo retardo temporal interauricolar entre as
        orelhas (varia de 0 a 0,6 ms)
    
    -   Para sons contínuos é muito difícil fazer essa percepção, então
        ou é feita uma comparação de fase (para baixas frequências) ou
        calculada pela diferença de intensidade causada pela atenuação
        do som pela cabeça.
    
-   **Localização vertical**

    -   Identificada pela diferença de tempo entre as reflexões no pavilhão da orelha
    -   Duas orelhas não ajudam, diferente da localização horizontal
    -   some animals have the ears at different height
    
-   **efeito de masaramento**

    -   Quando um som alto impede percebermos outro mais baixo

    -   Temporal: impede ouvirmos sons proximos no tempo, meio que “perdemos a sensibilidade”

        ![image-20210527111556412](Images - 2 - Sensors and actuators/image-20210527111556412.png)

    -   Frequencial: impede ouvirmos sons com frequencias parecidas, meio que “não conseguimos diferenciar”

        ![image-20210527111609531](Images - 2 - Sensors and actuators/image-20210527111609531.png)

### Auditory cortex

* Sinal from both ears get mixed very early (because sound location requires a lot of cross processing)

* There are 10x more connections from brain to ear than from ear to brain

  ![Image result for auditory pathway cortex](Images - 2 - Sensors and actuators/Inferior-Colliculus-and-Medial-Geniculate-Body-of-the-Auditory-Pathway-1024x934.jpg)

* A1 are relative to sound frequency

*  some
  neurons are intensity-tuned

  ![image-20200123105336443](Images - 2 - Sensors and actuators/image-20200123105336443.png)

* unilateral damage of A1 causes inability to localize the source of a sound, but the overall audition remains ok (becuase both ears send signals to both hemisferes)











## Sistema vestibular

-   Detecta movimentos da cabeça

-   O labirinto vestibular se localiza no ouvido interno, acima da
    cóclea

![Resultado de imagem para labirinto
vestibular](Images - 2 - Sensors and actuators/image3.jpeg){width="6.0625in"
height="3.40625in"}

-   **Órgãos otolíticos**

    -   Se dividem em sáculo e utrículo

    -   Detectam aceleração linear (e consequentemente inclinação
        estática)

    -   As células ciliadas da mácula (zona de transdução) percebem a
        força que atua sobre elas

-   **Canais semicirculares**

    -   São dois de cada lado e ortogonais entre si

    -   Fazem a percepção da aceleração angular

    -   A ampola faz a detecção da movimentação da endolinfa que
        preenche os canais

    -   Os sinais dos dois lados da cabeça são invertidos

-   **Processamento da informação**

    -   O sistema vestibular combina, além dos órgãos na orelha,
        informações e outras regiões

    -   Reflexo vestíbulo-ocular → estabiliza os olhos em um ponto fixo

## Somatic

-   Recebem tato, temperatura, dor e propriocepção

-   Receptores distribuídos ao longo de todo o corpo

### Percepção do tato

-   Identificamos tamanhos, texturas, pressão sobre um ponto, etc.

<!-- -->

-   **Mecanorreceptores**

    -   Sensíveis à deformação física

    -   Existem 4 tipos: Corpúsculo de Meissner, Corpúsculo de Pacini,
        Disco de merkel e Terminação e Ruffini

    -   A nossa capacidade de discriminação de dois pontos varia muito
        conforme a região do corpo

![img](Images - 2 - Sensors and actuators/image1.png){width="6.3346609798775155in"
height="3.0in"}

-   **Vias de condução**

    -   A informação chega ao cérebro por vias bem diferentes

    -   As sensações do corpo (transmitidas pelos neurônios espinhais
        aferentes) vai pela Via coluna Dorsal-Lemnisco Medial

    -   Dermátomos → regiões de percepção associadas à segmentos da
        medula

![Resultado de imagem para
dermátomos](Images - 2 - Sensors and actuators/image2.png){width="7.268055555555556in"
height="5.0289074803149605in"}

-   As informações do rosto (transmitidas por neurônios cranianos
    aferentes) vão pela Via Táctil Trigeminal (chega pela ponte
    encefálica)

<!-- -->

-   **Córtex Somatossensorial**

    -   Processa a maior parte da informação, porém muitas coisas já são
        processadas o caminho

    -   [Somatotopia]{.underline}

        -   Também chamado de *Homúnculo (pequeno homem)*

        -   Mapa cerebral das percepções somáticas

        -   O lado direito do córtex está associado ao ledo esquerdo do
            corpo

        -   O tamanho do desenho indica a sensibilidade do local

        -   É muito semelhante ao mapa motor

![Imagem
relacionada](Images - 2 - Sensors and actuators/image3.png){width="7.268055555555556in"
height="3.6822976815398074in"}

### Percepção da dor

-   A percepção subjetiva da dor nem sempre está diretamente relacionada
    à atividade fisiológica dos nociceptores

-   **Nociceptores**

    -   Do latim *nocere:* "ferir"

    -   Terminações nervosas livres, ramificadas e não-mielinizadas (o
        que as torna bem lentas, de 0,5 a 2 m/s)

    -   Indicam quando o tecido está sendo lesado

    -   Podem ser mecânicos, térmicos ou químicos

    -   A informação vai para o tálamo e depois para o córtex da região
        afetada pela Via Espinotalâmica (para sensações do corpo) ou
        pela Via Trigeminal (para sensações do rosto)

-   **Regulação da dor**

    -   O assunto é complexo, pois inclui mecanismos aferentes (que
        atuam na forma como os receptores enviam sinais), eferentes
        (atuam na forma como o cérebro interpreta a dar) e psicológicos.

    -   Hiperalgesia → amplificação da sensibilidade dos nociceptores de
        regiões machucadas

    -   Substância cinzenta periaquedutal → região do cérebro que,
        quanto estimulada, libera neurotransmissores com efeito
        analgésico

### Percepção da temperatura

-   Os termorreceptores não estão uniformemente distribuídos no corpo

-   Alguns são mais sensíveis ao frio e outros mais sensíveis ao calor

-   Alguns receptores são sensíveis tanto ao frio quanto ao calor, o que
    pode gerar uma sensação paradoxal de sentir uma temperatura
    diferente, mas não saber se é frio ou quente

-   Tanto os receptores de frio quando te calor respondem muito bem às
    mudanças bruscas de temperatura, mas se adaptam após algum tempo

-   As vias da temperatura são praticamente as mesmas das da dor

### Propriocepção

-   Percepção da posição do nosso próprio corpo

-   **Fusos musculares**

    -   Indicam o qual estendidos os músculos estão

    -   Localizados dentro dos músculos

    -   Estão dispostos em parelelo com as fibras musculares

    -   É preciso um sistema de realimentação (ver detalhes em *Controle
        Motor*)

![Resultado de imagem para fusos
musculares](Images - 2 - Sensors and actuators/image4.jpeg){width="5.377083333333333in"
height="2.9715277777777778in"}

-   **Órgãos Tendinosos de Golgi**

    -   Atua como sensor de tensão entre o músculo e o osso

    -   Localizado no tendão

    -   Estão dispostos em série com as fibras musculares

-   **Propriocepção das articulações**

    -   São capazes de detectar mudanças de ângulo, mas não ângulo
        estático
-   Conseguimos perceber com precisão o ângulo de uma articulação
        combinando os sinais dos fusos e dos órgãos de golgi

# Motor control

-   Alguns neurônios chegam a inervar mais de mil fibras musculares
    (como os que inervam nossa perna), enquanto alguns inervam apenas 3
    ou 4 fibras (como os que inervam os músculos oculares)

-   Graduação da força → variação da frequência de disparo neuronal

-   Miótomo → conjunto de músculos inervados por axônios motores à um
    segmento da medula

-   **Realimentação do sistema somático e tipos de neurônios**

    -   O correto funcionamento da percepção somática é fundamental para
        ao controle o controle do movimento

    -   Só que as fibras dos fusos se tornam frouxas durante a contração

    -   É preciso uma realimentação através de neurônios específicos

    -   Neurônios gama → faz uma realimentação nos fusos musculares

    -   Neurônios alfa → fazem efetivamente o movimento

![img](Images - 2 - Sensors and actuators/image1-1576290134955.png){width="6.817216754155731in"
height="2.320754593175853in"}

-   **Unidade motora**

    -   Um neurônio motor e as fibras que ele inerva

    -   Existem dois tipos de músculo estriado: rápidos e eu cansam
        rápido, lentos e resistentes.

    -   Consequentemente também existem dois tipos de neurônios motores:
        rápidos e lentos.

    -   [Unidades rápidas]{.underline} → formam junções maiores, possuem
        axônios de maior diâmetro e geram impulsos com uma frequência
        mais alta (de 30 a 60 Hz)

    -   [Unidades lentas]{.underline} → formam junções menores, possuem
        axônios de menor diâmetro e geram impulsos com uma frequência
        menor (de 10 a 20 Hz)

<!-- -->

-   **Junções neuromusculares**

    -   As sinapses são muito parecidas com as do SNC, porém possuem
        mais zonas ativas e uma maior área de contato

    -   São basicamente infalíveis (todo estímulo causará uma resposta
        muscular)

    -   São mais acessíveis e mais fáceis de serem estudadas

    -   O neurônio motor se comunica com a fibra muscular liberando o
        neutransmissor acetilcolina

![Resultado de imagem para junções
neuromusculares](Images - 2 - Sensors and actuators/image2-1576290134956.jpeg){width="3.5625in"
height="2.2708333333333335in"}

-   **Acoplamento excitação-contração**

    -   O potencial de ação chega no axônio dos neurônios motores alfa

    -   Este libera Acetilcolina na junção neuromuscular

    -   A somação das PEPS resultantes causa um forte potencial de ação,
        chamado de MUAP (*motor unit action potencial)*

    -   Desencadeia a liberação de íons Ca de organelas presentes na
        fibra (reticulo sarcoplasmático), levando à contração muscular

    <!-- -->

    -   [MUAP]{.underline}

        -   Cada MUAP duração de 2 a 10ms (uma contração precisa de um
            trem de MUAP's)

        -   Amplitude de 100uV a 2mV

        -   Frequência de 5Hz a 10kHz

        -   O campo gerado pelo MUAP pode ser detectado por um sensor
            eletromiográfico

<!-- -->

-   **Zona de inervação**

    -   Ou *ponto motor*

    -   Região do músculo com maior densidade de junções neuromusculares

    -   Seguem aproximadamente a linha dos principais nervos

    -   Uma corrente aplicada nesse ponto gera o maior movimento
        possível

    -   Um sensor posicionado nesse ponto gera um sinal instável, não
        sendo adequado para fins de eletromiografia

    -   [Guia para alguns
        músculos](https://pt.slideshare.net/painezee/zmpczm0160001216/6)

## Eletromiografia


-   Tentar ler a imensa zueira de muitos MUAPs somados e misturados

-   Amplitude entre 0 a 10mV pico a pico e de zero a 1,5mV RMS

-   Sua frequência está entre 0 e 500 Hz, dominantemente na faixa de 50
    a 150 Hz

-   Espectro de frequência típico:

![https://lh3.googleusercontent.com/WG8pod7ONZDIXvxEhWCJsIhLDQiSfUzarCu6ABthADiFwcM6V72Q5\_mDztNTUnCIORmxOapOLFmctcFJCc9USclu\_JKKmiqH3qo0FuHR7iNfPz7J9qwBvm0B1ns0jlzb4Ggpmw4J](Images - 2 - Sensors and actuators/image3-1576290134956.png){width="3.84375in"
height="2.53125in"}

-   **Eletrodos não-invasivos**

    -   Posicionados na superfície da pele

    -   São utilizados com auxílio de gel ou pasta condutora contendo
        íons de cloro, diminuindo a impedância entre pele e eletrodo

    -   Recomenda-se a remoção dos pelos e, se possível, da camada
        superficial da pele

    -   O eletrodo não deve ser posicionado sobre o ponto motor

    -   Coss-talk → detecção indesejada de sinais de músculos vizinhos

![](Images - 2 - Sensors and actuators/image4.png){width="3.5867115048118987in"
height="2.5520833333333335in"}

-   **Configuração bipolar**

    -   São usados 3 sensores

    -   Um no meio do musculo, sobre a linha média

    -   Um na ponta ou seguindo a linha das fibras musculares (não
        precisa estar longe)

    -   Um de referência, posicionado em uma região de osso e podendo
        estar longe dos outros dois. É preferível que este eletrodo seja
        maior.

    -   A diferença entre os dois primeiros gera um sinal que, tomando o
        terceiro como gnd, fornece um sinal AC proporcional ao estimulo
        muscular

-   **Processamento**

    -   A linearidade da relação emg-força tende a ser maior em músculos
        menores

    -   As análises mais comuns feitas sobre o sinal EMG são de valor
        médio/rms, valor de pico e frequência média.

## Vias para o encéfalo


-   Vias laterais → movimentos finos e voluntários dos músculos distais
    (antebraço, mão, perna e pé)

-   Vias ventromediais → controle da postura e locomoção

-   O dano em uma das duas irá prejudicar a capacidade de movimento do
    indivíduo, mas ele só ficará paralisado se as duas se fuderam

-   Danos nas vias laterais são mais difíceis de serem recuperados, mas
    a função das ventromediais pode ser gradualmente substituída pelas
    vias laterais

![Resultado de imagem para tractos espinhais
descendentes](Images - 2 - Sensors and actuators/image5.jpeg){width="3.882503280839895in"
height="4.5471708223972005in"}

## Modulação encefálica do movimento


-   Diversos neurônios codificam em conjunto a direção do movimento
    desejado

-   Vetores de população → conjunto de vetores determinados pelas
    células que, somados, resultam na direção do movimento

-   Quanto maior a população, mais fino o controle do movimento

-   O córtex planeja o movimento em um nível *estratégico,* mas quem
    controla a exata sequência de contrações (em nível *tático*) é o
    cerebelo