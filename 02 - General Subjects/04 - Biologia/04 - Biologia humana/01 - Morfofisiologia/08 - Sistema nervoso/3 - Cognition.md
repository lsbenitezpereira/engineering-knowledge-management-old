# Conceptualization

-   Estudo dos mecanismos neuronais responsáveis pelas atividades
    mentais superiores

-   **Neuroplasticidade**
    -   O encéfalo é dinâmico, tanto em termos funcionais quanto
            estruturais.
    -   Se perdemos um dedo, a região do córtex somático que processava
            aquela informação é remodelada e passa a processar informações
            de outras partes do corpo

## Neurogenesis

*  the vast majority of neocortical neurons are born between
  the fifth week and the fifth month of gestation

*  some restricted regions of the adult brain retain some capacity to generate new neurons: specially hipocampus (almost two percent of revenew per year, continouslly)

* once a daughter cell commits to a neuronal
  fate, it will never divide again.

* Cell Proliferation

  * radial glial cells: progenitors that give rise to all the
    neurons and astrocytes of the cerebral cortex. 
  * The newborn neuron is called neural precursor

* Cell Migration

  * Radial Glial cells also serve as temporary scaffold to guide newly formed neurons to their final destinations

    ![image-20200112000656110](Images - 3 - Cognition/image-20200112000656110.png)

*  Cell Differentiation

   * ?

* cell death

  * Newborn have aprox 2x more neurons, each with 50% more sinapses
  * apoptosis: programmed death in the initial years 

## Connection’s genesis

* development of long-range connections: pathway selection, target selection, and address selection
*  PNS axons are capable of regeneration (cut periferial nerves can regrown, slowly), but CNS axons do not. Aparentely because of the CNS have too much myelin, that speed up sinapse but inhibits grown	
* neurite: expantion process of both axon and dendrict
* Growth cone: growing tip of a neurite
* Lamellipodia: leading edge of the growth cone
* filopodia: thin spikes extending from the lamellipodia
* Synapse Formation
  *  filopodia are continually being formed and retracted from neuronal
    dendrites seeking innervation
* synaptic rearrangement
  * Sinaptic competition: The sinapses that receive more activity are maintained
  * hebian learning
    * neurons that fire together wire together
    * Synapses potentiate when they are active at the same time as their postsynaptic target neuron.
    * When the presynaptic axon is active and, at the same time, the postsynaptic neuron is strongly activated under the influence of other inputs, then the synapse formed by the presynaptic axon is strengthened
* Synaptic Homeostasis
  * mechanisms that provide stability and keep synaptic weights
    within a useful dynamic range
  * Metaplasticity: change the value of the *synaptic modification threshold* (the level between LPT and LPD)
  * Synaptic Scaling: neurons become more sensible if they dont recevie input (by farmacologycal blocking or nerve cut)

## Two sides of the brain

* :book: Bear 706
* The two seam semi independent
* If the corpus colosus is cut, all look normal… but is crazy
* There are also small anathomica diferences
* left side (control body’s right side): language
* Canhotesa
  * While animals of many species show a consistent preference for using one hand,
    there are typically equal numbers of left-handers and right-handers.
  * The human high number of right-handed can be related to language?

## Intelligence

* The discussion about what is intelligence is part here and part in Artificial Intelligence files, but I think everything should be centralize here
* predict and explain

# Memory

* retention of learned information
* learning and memory are lifelong adaptations of brain circuitry to the environment
* :book: Bear 823
* **Hippocampus**
  * consolidation of facts and events
  * Binding sensory information for the purpose of memory consolidation.
  * supports spatial memory of the location of objects of behavioral importance
  * does not seem related to procedual memory
  * Place cells
    * specific *place cells* are related to specific locations (*place field*)
    * the responses of place cells are related to where the animal thinks it is
  * Grid cells
    * active when the rat is at multiple locations that form a grid pattern
    * Also related to where the animal think he is
  * Other “mappings”
    * based on https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6608181/
    * organize memories in schemas
    * Social space
    * Music (maps through notes and rithm space)
* Distributed system
  * if an engram is based on information from only one sensory modality, it should be possible to localize it within the regions of cortex that serve this modality
  * ex: visual memory resided in visual cortex

## declarative memory

* or *explicit memory*

* Memory of facts and events (“yesterday I ate pizza”)

* episodic memory for autobiographical life experiences

* semantic memory for facts.

* Main structure: temporal lobe (critical for memory consolidation but not for the retrieval of memories)

  ![image-20200114143542399](Images - 3 - Cognition/image-20200114143542399.png)

* Long term memory
  
  * ?
  * ==Main structure: cortex, with possibles multitraces betwen cortex and hippocampus neurons==
  
* short term memory
  
  *  survives distraction and has a large capacity
  * can last minutes to hours
  
* working memory

  * the ability to hold in mind all information relevant to the task at
    hand
  *  achieved by keeping neural activity going with
    continuous rehearsal
  * Main structure: prefrontal cortex (is also related to feelings and emotions).

## nondeclarative memorie

* or *implicit memory*
* procedural memory
  * skills, habits, and behaviors (‘play piano’)
  * Main structure: striatum 
  * Nonassociative learning: a change
    in behavioral response that occurs over time in response to a single type
    of stimulus
  *  associative learning: behavior is altered by the
    formation of associations between events
  * :red_circle:bear 857
* Skeletal musculature
  * Main structure: cerebelum
*  Emotional responses 
  * Main structure: amygdala



Aprendizado
===========

* acquisition of new knowledge or skills
* memory = selective response of some neurons to some stimuli
* learning = Shifts in the selectivity of cortical neuron
* **Memory Aquisition**
  * Move to buffer: sensory -> short term
* **Memory Consolidation**
  * Move storage: short term -> long-term
  * long-term potentiation (LTP) and long-term depression (LTD)
    * Synaptic transmission occurring at the same time as strong depolar-
      ization of the postsynaptic neuron causes LTP of the active synapses.
    * Synaptic transmission occurring at the same time as weak or modest de-
      polarization of the postsynaptic neuron causes LTD of the active synapses.
    * cause increase or decrese in bioquemical receptors in teh post sinaptic axon
* **Reconsolidation**
  * Reactivate a memory makes it “fresh” again, suceptible to be modified or even erased
  * Occurs with both declarative and non-declarative (==maybe not so strong with procedural?==)
  * Lembrar da lembrança altera a lembrança, quase como se estivéssemos vivendo de novo

## Improving learning

* **Aprendizado é ativo**
  * Passivo → aluno
  * Ativo → estudante
* **timing**
  * As informações são gravadas no córtex durante a fase REM do sono
  * Mas o que é gravado?
  * Pois bem, o cérebro escolhe o que considera importante, relaciona as
    informações como bem entende e joga fora todo o resto
  * Para assegurarmos um bom aprendizado, devemos "martelar" o estudo de
    forma que, ao selecionar as informações, o cérebro tenha certeza que
    essa informação é importante
  * Além disso, essa "martelada" deve ser no mesmo dia da aula, senão as
    informações são perdidas e teremos que nos contentar com o que o
    cérebro escolheu gravar
  * “Aula dada é aula estudada HOJE”
* **impact of experience: "rich" and "poor" rats experiment**
* The "rich" rats lived in small groups in cages with plenty of rat diversions such as
    ladders and treadmills. The "poor" rats lived in cages that were similar but barren and lacking
    diversions. Over a period of months the neocortices of the rich rats developed far more complex
    networks of synaptic circuits interconnecting the neurons; the poor rats' neuronal circuitry was sparse
    by comparison. The difference was so great that the rich rats' brains were heavier, and, perhaps not
    surprisingly, they were far smarter at solving mazes than the poor rats
  *  putting a laboratory rat in a “com-
    plex” environment filled with toys and playmates (other rats) has been
    shown to increase the number of synapses per neuron in the occipital cor-
    tex by about 25%
* **latent learning**
  *  subconscious retention of information without reinforcement or motivation
  * Is not immediately expressed in an overt response
  * Experiment: Rats that had previusly explored the environment learn faster
* **Childhood: a crutial time**
* Children are born with many more neurons than their
    mature brain will retain; through a process known as "pruning" the brain actually loses the neuronal
    connections that are less used
  * While the sensory areas mature during early childhood, and the limbic system by
    puberty, the frontal lobes—seat of emotional self-control, understanding, and artful response—
    continue to develop into late adolescence, until somewhere between sixteen and eighteen years of
    age.
  * The end of a critical period does not necessarily signify an end to changes
    in the structure of axon terminals or the effectiveness of their synapses

## Pedagogia e educação

* ==Should I move this to…?==
* **:book: Data Science in Kindergarten**
  * https://hdsr.mitpress.mit.edu/pub/wkhg4f7a/release/1
* **educative team “games”**
  * About cyber security: https://livingsecurity.com/products/escape-rooms/
* **VR applications**
  * ==Should I move this to VR?==
  * Workplace situations similations and comportamental learning: https://www.mursion.com/

# Attention

* Selective attention: directed to select objects
* Overall arousal: unselective
* consciousness
  * awareness of something
  * easy problems of consciousness:answerable by standard scientific methodology. Ex: being awake or asleep
  * Hard problems: why experiences feel the way they do
  * it is possible to have your attention drawn to an object but still not perceive it
* resting state
  * Not paying attention to some specific task
  * The brain is full of activity
  * Some areas are only active during rest
  * Hipothesis of activities: broad sensorial monitoring, remembering and day dreaming, 

# Sleep

