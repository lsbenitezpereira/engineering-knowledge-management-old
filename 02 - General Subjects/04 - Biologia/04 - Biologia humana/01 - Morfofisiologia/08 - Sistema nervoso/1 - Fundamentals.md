> "understanding how intelligent behavior is produced by the brain helps to replicate it in machines" Me
>
> “Understanding neuroanatomy is necessary for understanding how the brain works” Bears

# Components

* Relatively little genomic code is responsible for the actual "wiring." In the cerebellum, the basic wiring method is repeated billions of times. It is clear that the genome does not provide specific information about each repetition of this cerebellar structure but rather specifies certain constraints as to how this structure is repeated (just as the genome does not specify the exact location of cells in other organs

## Cells

### Neurônios

-   Principal célula especializada do sistema nervoso
-   Temos mais de um trilhão de neurônios
-   Praticamente não se renovam
-   Medem em média 1cm, mas podem chegam a medir mais de 1 metro
-   tempo de processamento de um neurônio individual: na ordem de milisegundos 
-   fan out (number of interneuronal connections): mean 103
-   Digital controled analog processes:  for example, the firing of a neuron can be considered a digital event whereas neurotransmitter levels in the synapse can be considered analog values
-   **Divisão estrutural**

    -   Corpo (ou *soma*) → núcleo e pericário

    -   Pericário → citoplasma da célula

    -   Dendrito → receptor → ramificação curta e "arborizada"

    -   Axônio → emissor → prolongamento longo e único

![img](./Images/1.0 - Fundamentos - Células//media/image1.jpeg){width="3.78125in"
height="2.39375in"}

-   **Bainha de mielina**

    -   Aumenta a velocidade de condução do impulso elétrico

    -   É formada por algumas células da glia (Schwann e
        oligodendrócito)

    -   Não permite a troca de íons com o meio

    -   Mede de 0,2 a 2 milímetros.

-   **Nodo do ranvier**
-   Constrição entre bainhas
    
-   Permite a troca de ions
    
-   Como o impulso nervoso se dá pelo estabelecimento de um
        potencial elétrico devido ao desequilíbrio iônico, o impulso de
        propaga de nodo em nodo
    
-   Velocidade do impulso chega a 100m por segundo (em seres vivos
        sem bainha e nodo essa velocidade de, em média, 25cm por
        segundo)
    
-   **Classificação quanto ao número de dendritos**

    -   Neurônio unipolar

        -   Um só prolongamento

        -   Nos vertebrados só existe durante o período embrionário

    -   Pseudo-unipolar

        -   Apenas um prolongamento deixa no corpo celular, mas logo se
            divide entre um que vai pra periferia (forma a terminação
            nervosa sensitiva) e o outro vai pro SNC

        -   Presente nos gânglios sensitivos

    -   Neurônio bipolar

        -   Apenas um dendrito

        -   Presente principalmente no sistema visual a auditivo

    -   Neurônio multipolar 

        -   Vários dendritos

        -   Tipo mais comum

  ![img](./Images/1.0 - Fundamentos - Células//media/image2.png)
### Células de glia 

-   São células anexas ao neurônio

-   Desempenham diversas funções, mas todas "auxiliares" ao neurônio

-   São capazes de se regenerar

![Resultado de imagem para celula da
glia](./Images/1.0 - Fundamentos - Células//media/image3.jpeg){width="4.166666666666667in"
height="3.0625in"}

-   **Células de Schwann**

    -   No sistema nervoso periférico

    -   Revestem os axônios

    -   Esse revestimento pode ser mielínico ou amielínico

    -   O corpo da célula de Schwann fica achatado em volta da bainha de
        mielina, formando o chamado [neurilema ]{.underline}

![img](./Images/1.0 - Fundamentos - Células//media/image4.png){width="2.6145833333333335in"
height="1.4375in"}

-   **Oligodendrócito **

    -   No sistema nervoso central

    -   Na substância branca **→** forma a bainha de mielina

    -   Na substância cinzenta **→** estabelece uma simbiose entre os
        neurônios

![img](./Images/1.0 - Fundamentos - Células//media/image5.png){width="2.625in"
height="1.2604166666666667in"}

-   **Astrócito protoplasmático**

    -   Retira nutrientes do sangue

    -   Participa da hematoencefálica

    -   Localizados na substância cinzenta

-   **Astrócito fibroso**

    -   Retira resíduos

    -   Contribuem para o metabolismo energético do córtex

    -   Presentes na substância branca

-   **Micróglia**

    -   Defesa e proteção

    -   Atuam como fagócitos na limpeza de resíduos

    -   Origem mesodérmica

-   **Células ependimárias**

    -   Revestimento
## Electric potentials
### Potencial de membrana

-   Ou *Potencial de repouso*

-   Diferença de potencial intra e extracelular na ausência de estímulo

-   Fica na ordem de -65mV (negativo dentro)

-   Essa diferença é gerada por bombas (transporte ativo) de íons

-   **Despolarização** → quando fica menos negativo

-   **Hiperpolarização** → quando fica mais negativo

### Alterando o equilíbrio elétrico

-   PEPS → potenciais pós-sinápticos excitatórios
-   PIPS → potenciais pós-sinápticos inibitórios
-   IPSPs reduce the size of EPSPs (but will no be less than -65mV, potecial e repouso)

![img](./Images/1.1 - Fundamentos - Potenciais elétricos//media/image1.png)

![img](./Images/1.1 - Fundamentos - Potenciais elétricos//media/image2.png)

-   Somação
    -   Or *summation*
    -   integração dos sinais elétricos na membrana pós-sináptica
    -   summation represents the simplest form of synaptic integra-
        tion
    -   Spatial summation
        -   adding together of EPSPs generated
            simultaneously at many different synapses on a dendrite
    -    Temporal summation
        -   is the adding together of EPSPs generated at the same
            synapse if they occur in rapid succession (period 1-15ms)
-   Somação de PEPS pode gerar um novo potencial de ação (trigger an spike)
-   Dendrites rarely have enough ion channels to generate fully propagating
    action potentials, as axons can. But the voltage-gated channels in den-
    drites can act as important amplifiers of small PSPs generated far out
    on dendrites

![img](./Images/1.1 - Fundamentos - Potenciais elétricos//media/image3.png){width="5.867662948381453in"
height="2.027316272965879in"}

### Potencial de ação

-   Quando uma somação despolariza a membrana até um certo limiar,
    acontece um BANG: potencial de ação rápido e potente

-   **Limiar** → potencial de membrana no qual um número suficiente de
    canais de sódio dependentes de tensão abre-se de forma que a
    permeabilidade iônica relativa da membrana favoreça o sódio sobre o
    potássio

-   Essa perturbação propaga-se ao longo da membrana de toda da célula
    em uma sequência de realimentação, ativando todos os canais próximos
    e fazendo com que a informação não diminua de intensidade ao longo
    do trajeto

-   Na fase de repolarização, a bomba de sódio-potássio volta a deixar o
    meio intracelular negativo

![img](./Images/1.1 - Fundamentos - Potenciais elétricos//media/image4.png){width="4.669293525809274in"
height="2.773584864391951in"}

-   Esse impulso nervoso se propaga pela célula (sentido dendrito →
    axônio)

-   Geralmente são gerados diversos impulsos em rápida sucessão,
    codificando algum tipo de estimulo

-   **Tipos de transmissão do impulso**

    -   [Saltatória]{.underline} → pela abertura de canais nos nodos de
        ranvier → células mielínicas

    -   [Contínua]{.underline} → pela abertura de canais próximos →
        amielínicas

![img](./Images/1.1 - Fundamentos - Potenciais elétricos//media/image5.png){width="6.860364173228346in"
height="4.145833333333333in"}

## Sinaptic trasmission

-   Transmissão de informação entre neurônios
-   Sinapse = Local de transmissão
-   Veja bem, o impulso é sempre elétrico, mas a sinapse em si é quase
    sempre química
-   Um neurônio faz na faixa de 10 a 100.000 sinapses
-   Espícula dendrítica → expansão do dendrito conectada ao axônio

-   **Tipos de sinapse**
-   Axossomática → do axônio para o corpo
    
-   Axo-axônica → do axônio para outro axônio
    
-   Axodendrítica → do axônio para o dendrito
    
-   Axo-espícula → do axônio para uma espícula
    
-   Dendro-dendríticas → do dendrito para outro dendrito

![img](./Images/1.2 - Fundamentos - Transmissão Sinaptica//media/image1.png){width="4.802083333333333in"
height="3.1072298775153104in"}

-   **Tipos de transmissão**
-   Elétrica
    
-   Química

### Transmissão elétrica

-   Unidirecional

-   Rápida

-   Pouco frequente em humanos

-   Quando acontece em nós não é entre neurônios

-   O contato direto entre os neurônios é chamado de junção comunicante

### Transmissão química

-   Bidirecional
-   Lenta (10–100 msec, sometimes more)
-   Ocorre através de neurotransmissores
-   Fenda sináptica → espaço entre neurônios
-   Potencial de ação (emissor) -> liberação de químicos (meio de comunicação) -> PEPS/PIPS (receptor)

![Sinapses](./Images/1.2 - Fundamentos - Transmissão Sinaptica//media/image2.jpeg){width="3.9798370516185475in"
height="2.433477690288714in"}

- **Liberação**

  -   A chegada do impulso nervoso abre os canais para entrada de
      cálcio, que estimulam a liberação dos neurotransmissores

  -   As vesículas jogam os neurotransmissores na fenda sináptica via
      exocitose
      
  - The amount of neurotransmitters in a vessel is basically constant, so the amplitude of EPSP is quantizised 

  -   Neuromuscular juntion: single action potential in the presynaptic terminal triggers the exocytosis
      of about 200 synaptic vesicles (fail-safe), causing an EPSP of 40 mV or more 
      
  - CNS sinapse: one single vessel, so the EPSP is literaly binary 

  - recording from a transmitter-gated ion channel in the post sinaptic neuron:

    ![image-20191214134144528](Images - 1 - Fundamentals/image-20191214134144528.png)

-   **Recepção**

    -   Os neurotransmissores estimulam receptores a abrirem canais
        iônios
-   Todos os sinais que foram reconhecidos são integrados para
        formar PEPS ou PIPS
    -   Modulation
        -    receptors that does
            not directly evoke EPSPs and IPSPs but instead modifies the effectiveness
            of EPSPs generated by other synapses with transmitter-gated channels.
    
- Autoreceptors: receptors in the pre sinaptic neuron. Does some control/feedback

- The sinaptic cleft must be cleaned before the next transmission
## Neurotransmissors
-   Um mesmo neurotransmissor pode causar mais de um efeito
    (divergência) e diferentes neurotransmissores podem causar o mesmo
    efeito (convergência), because of the different receptors
-   What is
    -   A molecule
    -    synthesized and stored in the presynaptic neuron.
    -   releaseble in the presynaptic axon
    -   will produce some effect when reach the postsinaptic neuron
-   Major groups
    -   amino acids
    -   amines
    -   peptides
-   **Classificação**
    -   Moléculas transmissoras pequenas → *ex: acetilcolina*
    -   Neuroecptídeos → *ex: encefalinas*
    -   Gases → *ex: CO*
-   Receptors to neurotransmitters
    -   no two neurotransmitters bind to the same
        receptor; however, one neurotransmitter can bind to many different
        receptors
-   **Comparação com hormônios**

    -   Não confunda os dois

    -   A função endócrina está sob forte regulação do sistema nervoso

![img](./Images/1.3 - Fundamentos - Neurotransmissores//media/image1.png){width="5.037794181977253in"
height="3.9166666666666665in"}

### Principais neutransmissores

-   Acetilcolina → principalmente ativação muscular,
    memória e aprendizado
-   Endorfina → sensação de bem-estar
-   GABA → neurotransmissor inibitório mais comum
-   Glutamato → neurotransmissor excitatório mais comum
-   norepinephrine=noradrenaline
-   Serotonin
    -   Serotonin synthesis appears to be limited by the availability
        of tryptophan in the extracellular fluid bathing neurons (blood)
    -   the source of blood tryptophan is the
        diet (grains, meat, dairy products, and chocolate are particularly rich in
        tryptophan)

![Imagem
relacionada](./Images/1.3 - Fundamentos - Neurotransmissores//media/image2.jpeg)

### Farmacology basics

* Psychoactive drugs:  act on the
  central nervous system, and most do so by interfering with chemical synaptic
  transmission
* Hallucinogens
  * Ex: LSD
* Stimulants
  * Ex: cocaine and amphetamine

# Structure

## SNC

-   SNC → Sistema Nervoso Central

-   É onde acontece a maior parte do processamento

-   Composto pelo encéfalo e medula espinhal

-   O encéfalo é a coisa dentro da nossa caixa craniana, que por sua vez
    pode ser subdividido em cérebro, cerebelo e tronco encefálico

![img](Images - 000/image1.png){width="3.103772965879265in"
height="3.8742115048118984in"}

![img](Images - 000/image2.png){width="3.9722222222222223in"
height="2.9791666666666665in"}

![img](Images - 000/image3.png){width="3.6763429571303585in"
height="1.6978521434820648in"}

-   **Componentes**

    -   [Substância branca]{.underline} → prolongamentos e células da
        glia (tem essa cor por causa da mielina)

    -   [Substância cinzenta]{.underline} → composto principalmente por
        corpos celulares

    -   No encéfalo → cinza fica fora e a branca dentro

    -   Na medula → cinza dentro e branca fora (em forma de H)

-   **Principais nomenclaturas**

    -   [Núcleo]{.underline} → acúmulo de corpos celulares do SNC

    -   [Lócus]{.underline} → pequeno grupo neuronal bem definido

    -   [Trato]{.underline} → agrupamento de axônios com mesma origem e
        destino

    -   [Lemnisco]{.underline} → trato que atravessa o encéfalo

    -   [Feixe]{.underline} → agrupamento de axônios que se projetam
        juntos (não necessariamente com a mesma origem e destino)

    -   [Comissura]{.underline} → agrupamento de axônios que conecta um
        lado do encéfalo com o outro

### Encéfalo

* o encéfalo consome 20W de potencia 
* 10^10 neurônios no cerebro, e 10^11 no cerebelo

#### Tronco encefálico

-   "Talo" que une cérebro, cerebelo e medula espinhal

-   Localizado entre a medula e o tálamo

-   Cápsula → agrupamento de neurônios que une o tronco com o cérebro

-   **Bulbo**
    -   *Ou Miencéfalo ou medula oblonga*
        
    -   Tronco de cone conectado na medula
        
    -   Movimentos autônomos dos órgãos e funções vitais

-   **Ponte**

    -   Transmissão de informações para o cerebelo

    -   Também ajuda na comunicação do cérebro com a medula

-   **Mesencéfalo**

    -   Localizado entre a ponte e o cérebro

#### Cerebelo

-   *Metencéfalo*

-   Coordenação motora

-   É dividido em dois hemisférios, mas eles não são tão isolados como
    os do cérebro

-   Ao contrário do cérebro, o hemisférios esquerdo controla o lado
    esquerdo do corpo

-   É muuuuito segmentado em folhas e lóbulos, possuindo uma grande
    superfície marcada por sulcos e fissuras (sulcos profundos)

-   Corresponde a 10% do volume encefálico, mas contém mais de 50% dos
    neurônios

#### Cérebro

-   **Análise do desenvolvimento embrionário**

    -   A única coisa que eu sei é que tudo surge do tubo neural

    -   Essa estrutura é quase comum a todos os mamíferos, evoluindo
        apenas em complexidade (contrariando a teoria do cérebro trino)

    -   Neocortex → estrutura mais recente

-   **Classificação anatômica**

    -   Telecéfalo → parte mais externa

    -   Diencéfalo → parte interna

-   **Classificação evolutiva**

    -   Ou *Teoria do Cérebro Trino*

    -   Cada parte do cérebro surgiu em um estágio diferente da nossa
        evolução

    -   Essas partes operariam de forma relativamente independente e
        teriam origem em momentos diferentes da evolução

    -   Atualmente essa divisão não tem lá muita credibilidade, pois
        essa divisão hierárquica baseada em um princípio evolutivo até é
        bonito, mas não corresponde ao observado no processo evolutivo
        animal

    -   [Cérebro reptiliano]{.underline}

        -   Instintos e reflexos
    -   É difícil gravar algo nele, mas quase impossível de esquecer
        -   The most primitive part of the brain, shared with all species that have more than a minimal nervous
        system, is the brainstem surrounding the top of the spinal cord
        -   basic emotion reactions

    -   [Sistema límbico]{.underline}

        -   Comportamentos emocionais
    -   Armazena informações temporárias
        -   Inclui diversas estruturas, como o hipocampo e os núcleos
            anteriores do tálamo
    -   Because this part of the brain rings and borders the brainstem, it was
            called the "limbic" system, from "limbus," the Latin word for "ring." This new neural territory added
        emotions proper to the brain's repertoire
        -   also learning and memory

    -   [Córtex]{.underline}
    
        -   Parte racional
    -   Responsável pelo aprendizado
        -   The Homo sapiens neocortex, so much larger than in any other species, has added all that is distinctly human. Adds up complexity to emotions: Species that have no neocortex, such as reptiles, lack maternal affection; when their young hatch, the newborns must hide to avoid being cannibalized

##### Telencéfalo

-   Pensamento voluntário, linguagem, julgamento, percepção e memória

-   Dividido em dois hemisférios

-   **Fissura Sagital** → separa em dois hemisférios

-   **Corpo caloso** → une os dois hemisférios

-   **Sulcos e giros**

    -   Aumentam a superfície cerebral

    -   Sulcos → depressões na superfície

    -   Giros → regiões determinadas pelos sulcos

![img](Images - 000/image1-1576289454860.png){width="5.851388888888889in"
height="4.479166666666667in"}

-   **Lobos**

    -   Grandes regiões cerebrais

    -   Frontal, Temporal, Parietal, Occipital e o Lobo da Ínsula (bem
        lá dentro)

    -   A divisão não é muito funcional, porém o lobo occipital parece
        estar relacionado somente com a visão

  ![img](Images - 000/image2-1576289454860.png)

-   **Cortex**

    -   Camada superficial formada por substância cinzenta (espessura
        varia de 1,5mm a 3,5mm)
    -   Corresponde a 40% do peso do encéfalo
    
    -   Concentra as mais "altas" funções da percepção e cognição humana
    -   É dividido em camada (a primeira é a mais externa)
    
    -   the different functions are determined by the connections
    -   Neocortex → estrutura complexa presente só em mamiferos (90% do
            córtex)
    -   Áreas de projeção → sensibilidade e moticidade
    
    -   Áreas de associação
        -   funções psíquicas
    -    more recent evolutionary development (probably just primates)
        -   
    
    ![img](Images - 000/image3-1576289454860.png)
-   Núcleos da base?

##### Diencéfalo

-   Localizado na parte inferior do cérebro

> ![Related
> image](Images - 000/image4.jpeg){width="5.104166666666667in"
> height="2.34375in"}

-   **Tálamo**

    -   Alterações no comportamento emocional

    -   Todas as mensagens sensoriais, com exceção das provenientes do
        olfato, passam pelo tálamo antes de atingir o córtex cerebral.

    -   Compõe 80% do diencéfalo

-   **Hipotálamo**

    -   Integração das atividades dos órgaõs viserais

    -   Controle da temperatura corporal, regula o apetite e o balanço
        de água no corpo

-   **Epitálamo**

    -   Regulação hormonal e do comportamento emocial

-   **Subtálamo**

### Medula espinhal

-   Protegida pela coluna vertebral

-   Rodovia do sistema

-   Possui um canal central onde circula LCR

-   Sua espessura é variável: possui dois espessamentos (intumescência
    cervical e intumescência lombossacral) e vai afinando no final (cone
    medular)

-   Possui uma pequena fissura na parte anterior (frontal)

-   **Raízes dorsais** → axônios entram trazendo informação → possuem
    gânglios

-   **Raízes ventrais** → axônios saem levando informação → sem gânglios

![Resultado de imagem para raizes dorsais e
ventrais](Images - 000/image1.jpeg){width="4.442148950131234in"
height="2.6875in"}

![img](Images - 000/image2.jpeg){width="6.4835531496062995in"
height="3.375in"}

#### Arcos reflexos

-   Ou *Reflexo Miotático *

-   Reflexos involuntários

-   Reflexos vem direto da medula\... ==algumas atividades simples também?==

-   *Exemplo:* reflexo patelar

-   *Exemplo:* reflexo ao queimar a mão

![img](Images - 000/image3-1576289512283.png){width="7.525527121609799in"
height="3.8541666666666665in"}

## SNP

-   SNP → Sistema Nervoso Periférico

-   A maior parte do processamento acontece no SNC, cabendo ao SNP o
    sensoriamento e transmissão das informações

-   **Componentes funcionais**

    -   Nervos → comunicação

    -   Gânglios → corpos neuronais

    -   Meninges → revestem o SNC

    -   Ventrículos → mantêm a limpeza

-   **Divisão**

    -   SNP autônomo → involuntário
-   SNP somático → voluntário

### blood-brain barrier

* ==should it be here?==

In the late nineteenth century, scientists discovered that when they injected blue dye into an animal's
bloodstream, all the organs of the animal turned blue with the exception of the spinal cord and brain. They accurately
hypothesized a barrier that protects the brain from a wide range of potentially harmful substances in the blood,
including bacteria, hormones, chemicals that may act as neurotransmitters, and other toxins. Only oxygen, glucose,
and a very select set of other small molecules are able to leave the blood vessels and enter the brain.
Autopsies early in the twentieth century revealed that the lining of the capillaries in the brain and other nervous-
system tissues is indeed packed much more tightly with endothelial cells than comparable-size vessels in other organs.
More recent studies have shown that the BBB is a complex system that features gateways complete with keys and
passwords that allow entry into the brain. For example, two proteins called zonulin and zot have been discovered that
react with receptors in the brain to temporarily open the BBB at select sites. These two proteins playa similar role in
opening receptors in the small intestine to allow digestion of glucose and other nutrients.

###  Nervos

-   Feixes formados por fibras nervosas

-   Transmitem os impulsos

-   Podem ser mielínicas ou amielínicas

-   Podem deixar o SNC pelo encéfalo (nervos cranianos) ou pela medula
    (nervos espinhais)

-   **Classificação quando à função**

    -   Sensitivo (ou *aferente*) → vão pro SNC

    -   Motores (ou *eferentes*) → vão do SNC para os órgãos efetores

    -   Mistos → possui sensitivo e motor (a maioria dos nervos são)

-   **Nervos cranianos**

    -   Origem no encéfalo

    -   Deixam a cavidade craniana através de forames

    -   Existem 12 pares, sendo que 10 originam-se no tronco encefálico

    -   I -- Olfatório → pertence ao SNC

    -   II -- Ótico → pertence ao SNC

    -   III -- Oculomotor → movimenta o olho

    -   IV -- Troclear → movimenta o olho

    -   V -- Trigêmio → sensibilidade da face, movimenta a mandíbula

    -   VI -- Abducente → movimenta o olho

    -   VII -- Facial → expressão facial, gustação e glândula lacrimar

    -   VIII -- Vestibulococlear → equilíbrio e audição

    -   IX -- Glossofaríngeo → gustação, sensibilidade e movimentação da
        faringe

    -   X -- Vago → Todas as vísceras torácicas, maioria das abdominais

    -   XI -- Acessório → músculo trapézio e esternocleidomastóide

    -   XII -- Hipoglosso → movimenta a língua

![img](Images - 000/image1-1576289599328.png){width="6.036883202099737in"
height="4.958333333333333in"}

-   **Nervos espinais**

    -   Origem na medula

    -   Inervam pele e músculos. E os nervos do sistema simpático porra?

    -   Nervos motores → saem pelas raízes ventrais → corpos na medula

    -   Nervos sensitivos → entram pelas raízes dorsais → corpos nos
        gânglios

    -   As duas raízes se fundem nos [nervos espinais ]{.underline}

    -   Entre cada vértebra (forame intervertebral) há um nervo espinal

    -   As raízes dorsais e ventrais se unem em um único nervo e logo
        após isso voltam a se dividir, sendo que os ramos posteriores
        (mais finos) inervam o dorso e os ramos anteriores (mais
        grossos) inervam os membros e a porção ântero-lateral do tronco.

-   **Tecidos de sustentação**

    -   Camadas de tecido conjuntivo denso dão sustentação às fibras

    -   [Epineuro]{.underline} → camada mais externa, reveste o nervo
        todo

    -   [Perineuro]{.underline} → reveste cada um dos feixes de fibras

    -   [Endoneuro]{.underline} → envolve cada fibra nervosa

![img](Images - 000/image2-1576289599328.jpeg){width="3.4811329833770777in"
height="3.0383573928258967in"}

![img](Images - 000/image3.jpeg){width="2.2805205599300087in"
height="2.3333333333333335in"}

### Gânglios

-   Ponto de acúmulo de diversos corpos celulares de neurônios fora do
    SNC

-   Podem ser sensitivos, motores ou mistos (ambos)

-   *Curiosidade:* o SNC também tem gânglios, mas apenas um pequeno
    grupo: os gânglios da base

### Meninges

-   Revestem o SNC para não ficar em contato com o osso

-   Dura-máter → revestimento externo, resistente e inervado

-   Aracnóidea → avascular, esponjosa e preenchida por LCR

-   Pia-máter → vascularizada e em contato direto com o tecido nervoso

![Resultado de imagem para
meninges](Images - 000/image4-1576289599328.jpeg){width="5.712121609798775in"
height="3.9270833333333335in"}

### Sistema ventricular

-   O encéfalo possui cavidades preenchidas pelo Líquor, também chamado
    de Líquido Céfalo Raquidiano (LCR) ou Líquido Cérebro Espinhal
    (LCE).

-   O LCR é formado pelos plexos corióides

-   Fornece proteção mecânica, nutrição e excreção

-   Vista lateral esquerda:

![img](Images - 000/image5.jpeg)
![img](Images - 000/image6.jpeg)

### Terminações nervosas

-   Estruturas especializadas em perceber estímulos físicos ou químicos
-   Ver detalhes em *Sistema sensorial*

### Division

#### Somático

-   Ações voluntárias

-   Os neurônios têm origem na medula espinhal ou no encéfalo

-   Apenas um neurônio motor liga o SNC ao órgão efetuados

-   *Ex: mover um braço.*

#### Autônomo

-   Ou *visceral* ou *vegetativo*

-   Ações involuntárias

-   Inervam órgãos internos, vasos e glândulas

-   Dois neurônios ligam o SNC aos órgãos efetuadores: um
    pre-glanglionar e outro pós-ganglionar

![img](Images - 000/image1-1576289664980.png){width="3.6875in"
height="1.2539982502187226in"}

-   As fibras pós-ganglionares são amielínicas, porém com neurilema

-   **Simpático**

    -   Estimula o funcionamento dos órgãos (de modo geral)
-   O axônio pré-ganglionar é curto e o pró-ganglionar é longo
    -   Os neurônios pré-ganglionares saem da medula torácica e lombar
-   Se os nervos simpáticos saem da medula, o que acontece nos
        tetraplégicos? Como esses estímulos chegam aos órgãos viscerais?
-   *Exemplo de função: acelera o coração*
    -   *Exemplo de neurotransmissor*: *noradrenalina (prepara o corpo
        para a ação)*
-   [Tronco simpático]{.underline}
    -   Cadeia de gânglios
    
    -   Estendem-se de cada lado do crânio ao cóccix e se unem no
            final
    
    -   Unidos por ramos interganglionares
    
    -   Une-se ao nervo espinal através de ramos comunicantes
    
    -   Um gânglio pode mandar ramos pra vários nervos espinais,
            pois há menos gânglios do que nervos
    
    -   Ramos brancos → fibras pré-ganglionares (mielínicas)
    
    -   Ramos cinzentos → fibras pós-ganglionares (amielínicas)
    
    -   Os neurônios pós-ganglionares podem seguir até seu destino
            junto dos nervos espinais, por um nervo independente ou
        seguindo uma artéria

![Image result for tronco
simpático](Images - 000/image2-1576289664980.jpeg){width="2.8571423884514435in"
height="3.3333333333333335in"}

-   **Parassimpático**

    -   Inibe o funcionamento dos órgãos (de modo geral)

    -   O axônio pré-ganglionar é longo e o pró-ganglionar é curto

    -   Os neurônios pré-ganglionares saem do tronco encefálico e da
        medula sacral

    -   *Exemplo de função: desacelera o coração*

    -   *Exemplo de neurotransmissor*: *acetilcolina (conservação e
        repouso)*

-   **Comparação entre simpático e parassimpático**

    -   Os sistemas simpático e parassimpático possuem ação antagônica
        em um mesmo órgão

    -   Alguns órgãos possuem inervação puramente simpática (como
        glândulas sudoríparas e músculos eretores dos pelos)

    -   As ações do sistema parassimpático tendem a ser mais
        localizadas, enquanto às do simpático são mais difusas

    -   Plexos viscerais → emaranhado de fibras simpática e
        parassimpáticas que acorrem nas fibras

![Resultado de imagem para sistema nervoso
autônomo](Images - 000/image3-1576289664980.jpeg){width="5.141666666666667in"
height="4.207638888888889in"}

![img](Images - 000/image4.png){width="7.107280183727034in"
height="4.375in"}

* Enteric Division
  * Part of autonomic system
  * neural system
  * embedded in: the lining of the esophagus, stomach, intestines, pancreas, and gallbladder
  * 500 million neurons (same number of neurons as the entire spinal cord)









# Brain imaging and sensing

* MEG and EEG: Magnetic and electric (respectivly) mesuarements from  outside the skull. High tempoal resolution (miliseconds), but poor  spatial resolution (because the areas' signals get mixed)
* fMRI are generaly done togueder with MRI, to allow se the brain and its activation
  * is slow
    Differential, not abxolute
  * Mvpa technique: allow nine grained spatial analysis





























































