# Introduction

## Chemical control

* Neurohormones: from which much of the hypothalamus “speaks” to the body

* Hypothalamus

  * Control de pituitary gland
  * Regulate homeostasis
  * Control the autonomic nervous system (ANS)

* pituitary gland

  * pituitary gland
  * from which much of the hypothalamus “speaks” to the body
  * Posterior lobe: neurons secreting neurohormones in the bloodstream
  * Anterior lobe: endocrine system “master gland” (but do not forget that the hypothalamus control the pituitary)

  ![image-20200117142518859](Images - 4 - Behavior/image-20200117142518859.png)

* Diffuse Modulatory Systems
  
  * ?

Fundamentos de psicologia
=========================

-   
-   somaticizing: mistaking an emotional ache for a physical one
-   Border line
    * mercurial, intense emotional ups and downs that are sometimes diagnosed as
      "borderline personality disorder." Many such people are gifted at sensing what others around them
      are feeling, and it is quite common for them to report having suffered emotional abuse in childhood
-   neuroeconomics
    * combine the tools and insights
      from economics, neuroscience, and psychology to deter-
      mine how individuals make economic decisions.
# Psicologia - Linhas

## Piaget

* until ==1.5== years, the infant think object apper/disappear;   put a toy behind a courtain, remove the courtain and the car isnt there, the child is not suprised

## Gestalt

* Development, perception 

## Psycoanalysis

* **Freud**
  * even the small child develops a lively sexuality, that, in other words,
    sexuality and procreation are not the same thing
  * sexuality, or, rather, its energy, the libido, which derives from
    bodily sources, is the central motor of psychic life
  * Oedipus complex
    * ?
* **Lacan**
  * said the same as Freud, in a harder way
  * "Lacan systematically questioned those psychoanalytic developments from the 1930s to the 1970s, which were increasingly and almost exclusively focused on the child's early relations with the mother... the pre-Oedipal or Kleinian mother"

## Behavorismo

* only behavior could be seen objectively
* reduced behavior mainly to simple stimulus and response
* **Skynner**
  * ?
* **Hierarquia das Necessidades de Maslow**
  * Fisiológicas: base, precisa ser satisfeita
  * De segurança
  * Sociais
  * De estima
  * De auto-realização: topo
  * To see more about motivation, see the book *Drive*

## Cognitivo-comportamental

* Baseada no behavorismo
* **cognitive moviment**
  * turned to how the mind registers and stores information, and the nature of intelligence
  * mainly ignores emotion, observig just hw we think abot emotion, our amotional awerness 
  * Terapia cognitiva → racionalização das emoções e eliminação das crenças

## Psychoneuroimmunology

* the chemical messengers that operate most extensively in
  both brain and immune system are those that are most dense in neural areas that regulate emotion
* immune cells could be targets
  of messages from the nerves of the autonomic system, and they are essential for
  proper immune function.
* stress suppresses immune resistance, at least
  temporarily
* occasional display
  of hostility is not dangerous to health; the problem arises when hostility becomes constant

# Psicologia - Transversal topics

## Grief

* Elisabeth Kübler-Ross’s theory of the five stages of grief
  * Denial
  * anger
  * bargain
  * depression
  * acceptance

## Stress and axiety

* Anxiety vs performance
  * Yerkses Dodson Law 
  * People seem to concentrate best when the demands on them are a bit greater than usual, and they are able to give more than usual.
  * If there is too little demand on them, people are bored.
  * If there is too much for them to handle, they get anxious. 
  * Flow (optimal performance, “easy” doing) occurs in that delicate zone between boredom and anxiety.
* Is stress bad?
  * A [large-scale study](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3374921/) of stress and its impact on health demonstrated that stress is only harmful when we believe it is, even when it is severe.
  * Stress->cortisol. Excess of cortisol, continously, seems to be related to neuron death. “These effects of cortisol and stress resemble the effects of aging on the brain. Indeed, research has clearly shown that chronic stress causes premature aging of the brain.” Bears

## personality assessment

* Myers-Briggs Type Indicator 

## Others

* recovery from trauma
  * three stages: attaining a sense of safety; remembering the details of the trauma and mourning the loss it has brought; reestablishing a normal life.
  * we need a constant recycling and reliving of the trauma's terror by the emotional circuitry

# Motivation

* Voluntary movements are incited to occur, or motivated,
  in order to satisfy a need

* To see more about motivation, see the book *Drive*
  
* Needs

  * Mantain homeostasis 
  * satisfy survival needs (food, water, etc)

* Hipotalamic control system

  * Although
    homeostatic reflexes occur at many levels of the nervous system, the
    hypothalamus plays a key role in the regulation of body temperature,
    fluid balance, and energy balance.

  * Hipotalamus possibilities of actuation: stimulating or inhibiting the release of pituitary hormones, balance  sympathetic and parasympathetic outputs, somatic motor behavioral response.

    ![image-20200120163505268](Images - 4 - Behavior/image-20200120163505268.png)

* Measurements of serotonin in the hypothalamus reveal that levels
  are low during the postabsorptive period, rise in anticipation of food,
  and spike during a meal, especially in response to carbohydrates

# Language

* Language is a system for representing and communicating information
  that uses words combined according to grammatical rules
* can
  be expressed in a variety of ways including gestures, writing, and speech
* 
* Language pathologies
  * verbal dyspraxia: inability to produce the coordinated
    muscular movements needed for speech
  *  specifi c language impairment (SLI):  developmental
    delay in the mastery of language that may persist into adulthood
  * dyslexia: difficulty learning to read despite normal intelligence and train-
    ing
  * Aphasia: any type of language impairment 
* Brain structures
  *  language is processed in multiple, anatomically distinct,
    stages
  * We’ll follow here the Wernicke–Geschwind model. Isnt perferct, but is simple
  * Broca’s area: frontal lobe, specially in the left hemisphere
    * more about putting words together and speech production
    * Output:  motor cortical areas responsible for
      moving the lips, tongue, larynx, etc
  * Wernicke’s area: superior surface of the temporal lobe
    between the auditory cortex and the angular gyrus
    * more about meaning and compreention 
    * transformation of sounds into words 
    * Output: abstract representation?
  * arcuate fasciculus (a bundle of axons connecting Broca’s and Wernicke’s areas)
  * angular gyrus
    * ==maps vision to their “sound” equivalent==
    * Input: from visual corti-
      cal areas
    * Output: sound equivalent, go to Wernicke’s area
* 
  * 

# Emotion

* The fact that the thinking brain (neocortex) grew from the emotional reveals much about the relationship of thought to feeling; there was an emotional brain long
  before there was a rational one

* **Hipocampo**
  * Pre rocessamento dos sentido
  * The hippocampus's main input is in providing a
    keen memory of context, vital for emotional meaning (mas nao é ele quem faz a emoçaõ ou a razão); it is the hippocampus that recognizes the
    differing significance of, say, a bear in the zoo versus one in your backyard.
  * The hippocampus is crucial in recognizing a face as that of your cousin. But it is the amygdala
    that adds you don't really like her."
*  hypothalamus
  * governs the behavioral
    expression of emotion.
  * communication between the cortex and hypothalamus is bidirectional: emotion affets all body, all body affetcs emotion
  * integrates the actions of the ANS
* Limbic ring
  * or *limbic lobe*  (Latin *limbus* meaning *border*)
  * group of cortical areas that are distinctly different from the surrounding cortex
  * medial surface of the cerebrum
* Amygdala
  * amygdala (from the Greek word for "almond") is an almond-shaped cluster of
    interconnected structures perched above the brainstem, near the bottom of the limbic ring. There are
    two amygdalas, one on each side of the brain, nestled toward the side of the head.
  * specialist for emotional matters
  * This amygdala arousal seems to imprint in memory most moments of emotional arousal with an
    added degree of strength—
  * "precognitive emotion,": a reaction based on neural bits and pieces of sensory information that have
    not been fully integrated
  * The very regions of the cortex where the emotion-specific neurons concentrate are also, Brothers
    notes, those with the heaviest connection to the amygdala;
  * His research has shown that sensory signals from eye or ear travel first in the brain to the thalamus, and then—across a single synapse—to the amygdala; a second signal from the thalamus is routed to the neocortex—the thinking brain. This branching allows the amygdala to begin to respond before the neocortex (allows the emotional hijack). this circuit from thalamus to amygdala carries only a small portion of sensory messages, with the majority taking the main route up to the neocortex
* prefrontal cortex
  * govern our emotional reactions
  * planning and organizing actions
    toward a goal
  * prefrontal lobotomy: surgical "cure" to emotional distress, in 1940s
  * ==left and right lobes diffenrences?==

## Emotional Intelligence

Emotional intelligence: ability to perceive and respond appropriately to emotion, to interact in social situations, to have a moral sense, to get the joke, and to respond emotionally to art and music, among other high-level functions. 

# Theory of mind

* our unique
  ability to interpret behavior (our own and that of others) in terms of
  unobservable mental states, such as desires, intentions, and beliefs
* “we wont need a theory of mind if everyone spoke their minds” some austistcana