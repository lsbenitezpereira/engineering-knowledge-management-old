## Estrutura cristalina do silício

CFC

Rede cúbica do tipo diamante

## Estrutura cristalina do germânio

CFC

Muito utilizado em transístores e fotodedectores, porém menos que o Silício devido ao seu alto custo [3]

## Estrutura cristalina do tântalo

CCC

Muito utilizado em instrumentos cirúrgicos e em implantes, pois é um material fisiologicamente inerte

## Referências

[1] - http://www.lsi.usp.br/~eletroni/milton/cristl.htm

[2] - https://pt.wikipedia.org/wiki/Germ%C3%A2nio

[3] - http://www.quimlab.com.br/guiadoselementos/germanio.htm

[4] - https://pt.wikipedia.org/wiki/T%C3%A2ntalo_(elemento_qu%C3%ADmico)