

## Antecedentes

No século XVIII, o ateísmo dos filósofos
elimina a noção de Deus, porém não suprime a idéia de que a essência precede a
existência.

Ou seja, mantêm a ideia de que o homem possui uma “naturaza humana”

O existencialismo ateu, que eu represento, é mais coerente. Afirma que, se Deus
não existe, há pelo menos um ser no qual a existência precede a essência,

## Bases

* existência e essencia

  * O que todas as correntes do Existencialisto têm em comum é simplesmente o fato de todos considerarem que a
    existência precede a essência, ou, se se preferir, que é necessário partir da
    subjetividade.
  * O homem nada mais é do que aquilo que ele faz de si mesmo: é esse o
    primeiro princípio do existencialismo

* Subjetividade humana

  * Subjetivismo significa, por um lado, escolha do sujeito individual por si próprio e, por
    outro lado, impossibilidade em que o homem se encontra de transpor os limites da
    subjetividade humana. É esse segundo significado que constitui o sentido profundo do
    existencialismo

* Engajamento

  * não há um único de nossos atos que, criando o
    homem que queremos ser, não esteja criando, simultaneamente, uma imagem do
    homem tal como julgamos que ele deva ser
  * Portando, ao tomar minhas decisões devo agir de tal forma que os meus atos sirvam de norma para toda a humanidade
  * minha decisão engaja toda a humanidade, e isso sempre me causa angústia
  * “O existencialista declara freqüentemente que o homem é angústia.”
  * Desamparo e angústia caminham juntos

* Desamparo

  * Quando falamos de desamparo, expressão cara a Heidegger, queremos
    simplesmente dizer que Deus não existe e que é necessário levar esse fato às últimas
    conseqüências

* Liberdade

  * Dostoievski escreveu: “Se Deus não existisse, tudo seria permitido”. Eis o
    ponto de partida do existencialismo: não existe determinismo, o homem é
    livre
  * Estamos sós, sem desculpas:  homem está condenado a ser livre
  * o homem, sem apoio e sem ajuda, está condenado a  inventar o homem a cada instante.
  * Sem dúvida, a liberdade,
    enquanto definição do homem, não depende de outrem, mas, logo que existe um
    engajamento, sou forçado a querer, simultaneamente, a minha liberdade e a dos
    outros; não posso ter como objetivo a minha liberdade a não ser que meu objetivo seja
    também a liberdade dos outros.
  * “Ser-se livre não é fazermos aquilo que queremos, mas querer-se aquilo que se pode. ”

* O homem é a sua ação, o que ele FAZ de si mesmo

  * o homem nada mais é do que o seu projeto; só existe na medida em que se realiza; não é nada
    além do conjunto de seus atos, nada mais que sua vida.
  * se nós renacemos a cada instante, a cada ciclo, entao nós somos apenas o que somos agora. Para além disso, não existe nada
  * O existencialismo  define o homem pela ação
  * Evidentemente, tal pensamento pode parecer
    difícil de aceitar por alguém que tenha fracassado em seus projetos de vida

* O existencialismo é um humanismo

  * a palavra humanismo tem dois significados
    muito diferentes: Podemos considerar como humanismo uma teoria que toma o
    homem como meta e como valor superior. Este é um humanismo que recusamos
  * O humanismo com o qual nos identificamos é: o homem está constantemente
    fora de si mesmo; é projetando-se e perdendo-se fora de si que ele faz com que o
    homem exista
  * O homem é quem decide sobre si
  * O homem deve procurar uma meta fora de si (individuo) para se realizar como ser humano (raça, coletivo)

## Merleau-Ponty

 “tipo, tem o autor que pretendo trabalhar com vocês na aula que vem

 mas eu sei como é a questão da liberdade nele

 o nome é Merleau-Ponty

 ele e Sartre eram bastante amigos, só que romperam por questões ideológicas

 assim como Sartre, ele também trabalhou a questão do corpo

 tem a questão da sexualidade

 da percepção

 em alguns pontos fala sobre o MARXISMO

 (ponto em que, imagino, eles se desentenderam)”

<http://anfic.org/a-liberdade-em-merleau-ponty/>

<https://pt.scribd.com/doc/58600844/A-concepcao-de-liberdade-na-Fenomenologia-da-percepcao-de-Merleau-Ponty>