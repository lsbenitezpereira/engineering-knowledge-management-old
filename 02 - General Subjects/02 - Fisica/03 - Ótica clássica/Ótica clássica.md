# 	Luz

-   Luz visível→ energia radiante.

-   A frequência determina diversas características da luz (detalhes
    mais a frente), porém não varia com o meio.

-   **Princípios fundamentais da luz** (segundo a ótica clássica)

    -   [Propagação Retilínea]{.underline}

        -   A luz sempre se propaga em linha reta em meio homogêneos e
            transparentes.

    -   [Independência]{.underline}

        -   Raios de luz não interferem um nos outros, mesmo que se
            cruzem

    -   [Reversibilidade]{.underline}

        -   Os raios de luz sempre são capazes de fazer o caminho na
            direção inversa. 

        -   EXEMPLO: se vemos alguém através de um espelho, certamente
            essa pessoa também nos verá.

## Classificação

* **Meios de propagação**

  * Transparente

    * Luz de propaga de maneira ordenada.

    * Nitidez.

    * Exemplos: vidro polido, ar atmosférico, etc. 

  * Translúcido

    * Luz se propaga de maneira DESordenada.

    * Distorção

    * Exemplos: vidro fosco, plásticos, etc. 

  * Opaco

    * Impedem a passagem de luz

    * Exemplos: portas de madeira, paredes de cimento, pessoas, etc. 

    ![](./Images/1.0 - Caracteristicas e classificações da luz/media/image1.png)

* **Formado do feixe**

    -   [Cônico convergente]{.underline} **→** os raios de luz convergem
        para um ponto
        
        ![img](./Images/1.0 - Caracteristicas e classificações da luz/media/image2.jpeg)
    
    -   [Cônico divergente]{.underline} **→** os raios de luz divergem a
        partir de um ponto
        
        ![img](./Images/1.0 - Caracteristicas e classificações da luz/media/image3.jpeg)
        
    -   [Cilíndrico paralelo]{.underline} **→ **os raios de luz são
        paralelos entre si.

![http://www.sofisica.com.br/conteudos/Otica/Fundamentos/figuras/luz3.jpg](./Images/1.0 - Caracteristicas e classificações da luz/media/image4.jpeg){width="2.084722222222222in"
height="1.0375in"}

* **Fonte do feixe**
  * [Fontes primárias]{.underline}
    * Corpos luminosos
    * Emitem luz própria
    * As estrelas, a chama de uma vela, uma lâmpada acesa\...
  * [Fontes secundárias]{.underline}
    * Corpos iluminados
    * Refletem a luz.
    * A Lua, os planetas, as nuvens, os objetos visíveis que não
      têm luz própria\...

* **Dimensões da fonte**
  * [Pontual ou puntiforme]{.underline}
    * Uma fonte sem dimensões consideráveis que emite infinitos
      raios de luz.
    * Geram sombra uniforme

    ![img](./Images/1.0 - Caracteristicas e classificações da luz/media/image5.jpeg)

    ![img](./Images/1.0 - Caracteristicas e classificações da luz/media/image6.jpeg)

  * [Extensa]{.underline}

    * Uma fonte com dimensões consideráveis em relação ao ambiente.

    * Geram sombra e penumbra

    ![jpg](./Images/1.0 - Caracteristicas e classificações da luz/media/image7.jpeg)
# materiais e propriedades opticas

* Transparência

  * Se a luz, naquela frequência, pode ser absorvida

* indice de refração

# Cor

* O que exatamente é a luz só vai ser estudado na parte de ondulatória e, mais aprofundado ainda, em física moderna

* Para a ótica clássica, vamos esquecer sua origem e seu comportamento e nos focar apenas no que "vemos" dela: a cor.

* Monocromática → não pode ser decomposta, ou seja, a cor pura.

* Policromática → pode ser decomposta em mais de uma luz monocromática.

![](./Images/1.0 - Caracteristicas e classificações da luz/media/image8.png)

* Bizú para cores primárias: Amo ver o mar (amarelo -- vermelho -- azul)

# Effects and interaction

## Refração

-   Mudam de velocidade e direção

-   Frequência não muda

-   Quando mudam de meio

-   Também ocorre reflexão junto.

-   Dioptro **→** todo sistema formado por dois meios homogêneos e
    transparentes

-   **Índice de refração absoluto (n)**

    -   Relação entre a velocidade da luz no vácuo (c) e no meio em
        questão (v).

    -   $N = \frac{c}{v}$

    -   Quase sempre ligado à densidade: quanto maior, mais lenta a onda
        e, consequentemente, maior o N.

-   Quando vai de um meio menos refringente (n menor) para outro mais
    refringente (n maior), ela se aproxima na normal.

-   **1º lei** da refração → O raio incidente e o raio refratado
    pertencem ao mesmo plano.

-   **2º lei** da refração (lei de Snell) → A razão entre o seno do
    ângulo de incidência e o seno do ângulo de refração será sempre
    constante para um par de meios transparentes.

    ![http://www.sofisica.com.br/conteudos/Otica/Refracaodaluz/leis\_de\_refracao\_clip\_image002\_0002.gif](Images - 1.0 - Caracteristicas e classificações da luz/image3.gif){width="1.6076388888888888in"
    height="0.4534722222222222in"}

![http://n.i.uol.com.br/licaodecasa/ensmedio/fisica/refra4.jpg](Images - 1.0 - Caracteristicas e classificações da luz/image4.jpeg){width="3.14375in"
height="2.359722222222222in"}

![http://fisicaevestibular.com.br/novo/wp-content/uploads/migracao/optica/lei-refracao/i\_bc04d0bee499cfe4\_html\_91d39246.png](Images - 1.0 - Caracteristicas e classificações da luz/image5.png){width="3.8743055555555554in"
height="1.2180555555555554in"}

![](Images - 1.0 - Caracteristicas e classificações da luz/image6.png){width="3.591666666666667in"
height="1.9277777777777778in"}

-   **Ângulo limite de refração**

    -   Quando a onda incidir em praticamente 90º, a onda estará sendo
        refratada ao seu limite.

![http://n.i.uol.com.br/licaodecasa/ensmedio/fisica/retolu3.jpg](Images - 1.0 - Caracteristicas e classificações da luz/image7.jpeg){width="2.7597222222222224in"
height="1.9680555555555554in"}

-   **Ângulo limite de incidência**

    -   O método para encontrar é o mesmo

    -   Porém o valor é diferente, já que agora ele vai do meio B para
        o A.

![](Images - 1.0 - Caracteristicas e classificações da luz/image8.jpeg){width="2.7597222222222224in"
height="1.9680555555555554in"}

-   **Reflexão total**

    -   Ultrapassando o limite de incidência, a onda é refletida.

    -   Para que ocorra, a luz tem que estar no meio mais denso (n
        menor)

    -   EXEMPLO 1

        -   Na estrada, o asfalto quente aquece o ar imediatamente
            acima.

        -   Com a luz vem de um meio mais denso (ar frio), ela reflete
            totalmente no asfalto, dependendo do ângulo.

            ![http://n.i.uol.com.br/licaodecasa/ensmedio/fisica/retolu8.jpg](Images - 1.0 - Caracteristicas e classificações da luz/image9.jpeg){width="2.7597222222222224in"
            height="2.0722222222222224in"}

    -   EXEMPLO 2

        -   Fibra ótica, largamente usada hoje em dia.

        -   As paredes espelhadas e o uso da reflexão total fazem com que a
            luz perca muita pouca intensidade no percurso.

![http://www.infoescola.com/wp-content/uploads/2009/08/fibra\_optica.jpg](Images - 1.0 - Caracteristicas e classificações da luz/image10.jpeg){width="4.504166666666666in"
height="1.9597222222222221in"}



* **Refração depende do comprimento de onda**

  * ?

    ![image-20210520110003153](Images - Ótica clássica/image-20210520110003153.png)

## Reflexão

* Bate a volta

* Explicação físca

  * Quando a radiação luminosa de um meio passa para o outro com um índice de refração diferente

  * some of the light is scattered at the interface between the two media, even if both are transparent

  * reflectivity R represents the fraction of the incident light thatis reflected at the interface: R=Ir/I0

  * R depende do ângulo de inciencia e do indice de refreção dos dois materiais

* **Types of reflection**

  * Regular (ou *especular*): raios refletidos em uma única direção
  * Difusa (or *body reflection*): em todas as direções; body will look equally bright in all directions

  ![img](Images - 1.0 - Caracteristicas e classificações da luz/image1.gif)Glossy reflection (imperfect specular, blurry):

  ![image-20200719120842843](Images - 1.0 - Caracteristicas e classificações da luz/image-20200719120842843.png)
  
* **Basic laws**
  
  * 1º lei da reflexão → os raios incidentes, refletivos e normais pertencem ao mesmo plano.
  * 2º lei da reflexão → os raios refletidos e incidentes tem o mesmo ângulo em relação à normal.

  ![](Images - 1.0 - Caracteristicas e classificações da luz/image2.jpeg)

## Absorção

* Parte da luz que incide é absorvida

* Explicação físca

  * Two mecanism: electronic polarization and fotoeelectric absortion

  * electronic polarization

    * only at light frequencies in the vicinity of the relaxation frequency of the constituent atoms.

  * Fotoelectric absortion

    * poderá ser absorvida se e somente se a energia do fóton hv for igual à uma banda de energia do material

    * A maioria dos materiais é transparente para frequências altas

    ![](Images - 1.0 - Caracteristicas e classificações da luz/image11-1595171899672.png)

    ![img](Images - 1.0 - Caracteristicas e classificações da luz/image12-1595171899673.jpeg)

    ![img](Images - 1.0 - Caracteristicas e classificações da luz/image13-1595171899673.jpeg)

## Polarização

* característica de todas as ondas eletromagnéticas

# Dispositivos óticos

-   **Prisma**

    -   Diferente da geometria, aqui prisma é simplesmente algo que
        refrata a luz e separa a luz policromática.

    -   Cada cor é desviada em um angulo diferente, e voualá

    -   Pode ser dispersivo, refletivo ou polarizado (?)

![Resultado de imagem para prisma
luz](Images - 1.0 - Caracteristicas e classificações da luz/image1.jpeg){width="5.4527777777777775in"
height="4.094444444444444in"}

-   **Câmara escura**

![http://www.sofisica.com.br/conteudos/Otica/Fundamentos/figuras/cameraescura1.gif](Images - 1.0 - Caracteristicas e classificações da luz/image2.gif){width="5.026388888888889in"
height="1.6784722222222221in"}

![http://www.sofisica.com.br/conteudos/Otica/Fundamentos/camaraescura\_clip\_image002.gif](Images - 1.0 - Caracteristicas e classificações da luz/image3-1595172094814.gif){width="0.5333333333333333in"
height="0.48680555555555555in"}

-   **Lupa**

![Resultado de imagem para lupa
fisica](Images - 1.0 - Caracteristicas e classificações da luz/image4.png){width="5.98125in"
height="3.415277777777778in"}

-   **Luneta**

    -   Pode ser tanto astronômica quanto terrestre

    -   $\text{Aumento} = \frac{distância\text{focal}\text{da}\text{objetiva}}{distância\text{focal}\text{da}\text{ocular}}$

![Resultado de
imagem](Images - 1.0 - Caracteristicas e classificações da luz/image5.jpeg){width="4.131944444444445in"
height="1.1701388888888888in"}

-   **Microscópio**

    -   Tipo uma luneta, so que com distância focal menor, permitindo
        observar objetos próximos.

    -   "Na microscopia óptica, a observação com uma objetiva de 40 x e
        uma ocular de 10 x resulta em um aumento final de 400 x"

## Câmera fotográfica

-   Uma lente convergente capaz de fornecer imagens reais, invertidas e
    maiores

![Resultado de imagem para fisica camera
fotografica](Images - 1.0 - Caracteristicas e classificações da luz/image6.gif){width="3.263888888888889in"
height="2.21875in"}

![](Images - 1.0 - Caracteristicas e classificações da luz/image7.png){width="6.6506944444444445in"
height="7.74375in"}

## Olho

lens system: gather sufficient light

have a limited depth of field: they can focus light only from points
that lie within a range

of depths (centered around a focal plane)

Objects outside this range will be out of focus

To move the focal plane, the lens in the eye can change shape (Figure
24.3); in a

camera, the lenses move back and forth