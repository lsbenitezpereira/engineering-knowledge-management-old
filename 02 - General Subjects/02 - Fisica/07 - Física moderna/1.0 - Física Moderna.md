> “Em momentos de crise, só a imaginação é mais importante que o conhecimento" Albert Einstein

# Teoria da Relatividade
## Relatividade restrita

-   Para altas velocidades (mais que 10% da velocidade da luz), a
    mecânica clássica já não representava a realidade.

-   ==Nessas condições o tempo dilata e o espaço contrai==

-   Albert Einstein elaborou a teoria da relatividade

-   **1º Postulado**

    -   "As leis da física são as mesmas para todos os observadores em
        qualquer sistema de referência inercial"

    -   Adote sempre uma referência

    -   Por exemplo, a terra

    -   Se você considerar a terra como "parada", os eventos analisados
        na terra seguem as leis físicas normais

    -   Os outros planetas, porém, terão uns movimentos locões

    -   Nesse caso, adote o sol como sistema de referencial inercial
        (supondo que este não sofra forças externas)

-   **2º postulado**

    -   "A velocidade da luz no vácuo tem o mesmo valor para todos os
        observadores, qualquer que seja a característica do seu
        movimento ou da fonte de luz"

    -   A luz viaja a 300.000 km/s, sempre

    -   Se você se mover no sentido contrário de sua propagação, a luz
        não se movera mais rápido em relação à você

    -   Você continuará "vendo" a luz à 300.000 km/s

    -   Antes de Einstein, o tempo era absoluto (igual para todos os
        referenciais)

    -   Depois de Einstein, a velocidade de luz é absoluta e o tempo se
        "molda" a ela

    -   <u>phase velocity</u>
        -   Speed of light in a medium
        
        -   While the speed of light in a vacuum is a universal constant
            (c = 299,792,458 m/s), the speed in a material may be
            significantly less, as it is perceived to be slowed by the
            medium. For example, in water it is only 0.75c. Matter can
            accelerate beyond this speed (although still less than c)
            during nuclear reactions and in particle accelerators
        
    -   <u>group velocity of light</u>
    -   ?

## Relatividade temporal

-   O tempo é relativo à velocidade

-   Quando mais rápido, mais o tempo desacelera

-   Na velocidade da luz, o tempo para

$$
t = \frac{t_{0}}{\sqrt{1 - \frac{v^{2}}{c^{2}}}}
$$

> T = tempo na terra (referencia)

> T~0~ = tempo para o corpo se movendo

> V = velocidade do corpo

> C = velocidade de luz no vácuo

## Relatividade inercial

-   A massa é relativa à velocidade

-   ==AUMENTA A INÉRCIA PORRA, NÃO A MASSA==

-   ==MASSA É UM INVARIÁVEL==

-   Quanto maior a velocidade, maior a massa

-   O número de moléculas não aumenta, o que aumenta é a "inercia" delas

-   É preciso uma força infinita para acelerar o corpo acima da
    velocidade da luz

$m = \frac{m_{0}}{\sqrt{1 - \frac{v^{2}}{c^{2}}}}$

M = tempo para o corpo se movendo

M~0~ = tempo na terra (referencia)

V = velocidade do corpo

C = velocidade de luz no vácuo

-   **Equivalência matéria-energia**

    -   Você fornece energia (aceleração) e a massa aumenta

    -   Logo, seguindo a lei da conservação da energia, deve existir uma
        relação direta entre os dois

$E = \text{mc}²$

E = energia

M = massa em repouso

C = velocidade da luz

-   <u>Exemplo</u>
-   Aquecendo um gás em um recipiente, a velocidade das partículas
        aumenta
    -   Maior velocidade, maior massa
-   Energia (calor) se transformou em massa
-   <u>Energia e momento linear</u>
    -   teoria especial da relatividade, toda partícula que possui energia também deve possuir momento linear.
    -   

## Relatividade espacial

-   O tamanho do corpo é relativo à velocidade

-   Quando mais rápido, menor o corpo

-   A contração se dá apenas na direção do movimento

$L = L_{0}\sqrt{1 - \frac{v^{2}}{c}²}$

L = comprimento do corpo se movendo

C~0~ = comprimento na terra (referencia)

V = velocidade do corpo

C = velocidade de luz no vácuo

-   **Equivalência gravidade-velocidade**

    -   "Um campo gravitacional homogéneo é equivalente a um referencial
        uniformemente acelerado"

    -   A massa, o tempo e o tamanho são relativos à velocidade

    -   Logo, não é possível diferenciar gravidade de velocidade

-   **Interdependência espaço/tempo**

    -   Se seu tamanho varia, então suas "coordenadas" dependem do tempo

    -   Para analisar um objeto/plano/evento, é preciso saber também sua
        velocidade

    -   Assim, o mundo deve ser analisado em 4 dimensões

    -   As 3 primeiras variam se acordo com a 4º

    -   A velocidade e a massa (equivalência) "deformam" o espaço

![Resultado de imagem para teoria da
relatividade](./Images/1.0 - Teoria da relatividade /media/image1.jpeg){width="3.9340277777777777in"
height="2.4479166666666665in"}

-   Se o espaço se deforma, a luz também demora mais tempo para
    percorre-lo

-   Eclipse de 1919 → comprovou a deformação do espaço

[[http://www.fisica.net/fisica-moderna/]{.ul}](http://www.fisica.net/fisica-moderna/)

# Física quântica

* Estudo sobre coisas na natureza que são quantizadas, discretas

## Teoria do quantum

-   Max Planck

-   As radiações eletromagnéticas não são absorvida/liberadas de forma
    contínua, mas em pulsos

-   Quanto maior a frequência, maior a energia desse pulso

-   "Pacotes de energia" → fóton

-   Quantum → energia do fóton

$$
E = \text{hf}
$$

> E = energia do fóton

> H = constante de Planck (6,62x10^-34^ J.s)

> F = frequência da radiação

<http://www.comciencia.br/reportagens/fisica/fisica02.htm>

  

According to Schrödinger, the correct answer is both (at least while the
box remains closed). Every time a subnuclear reaction takes place that
has two possible outcomes, the universe is cloned. In one, the event
occurred, in the other it didn't. The cat's alive in one universe, dead
in another. Only when you open the box do you know which universe you
are in.

## Radiação de corpo negro

-   Ao aquecer um corpo, ele emite luz
-   Os elétrons ficam agitados e liberam fótons
-   Cada material possui um espectro de luz específico
-   Portanto, qualquer forma de matéria quente é uma fonte de luz
-    
-    está relacionado à exitação dos elétrons, que vão para uma camada mais elevada ao receber energia (calor), emitndo luz ao volta
-   o espectro de emissão é equivalente ao espectro de absorção
-    
-   ==apenas gases emitem espectros em feixes, e sólidos emitem um espectro contínuo?==
-   **Efeito da Catástrofe do ultravioleta**
    -   relacionado à radiação do corpo negro; se observa que a radiação emitida não é a esperada, na faiax ultravioleta? 

## Outras anotações

* pósitron
  * uma partícula que tem a mesma massa de repouso m que um elétron, mas possui uma carga positiva +e
    em vez da carga negativa -e de um elétron
  * um fóton pode gerar um par positron-eletron
  * um aniquilamento positron-eletron gera foton

# Luz

* fenômenos ondulatórios: polarização, interferência, difração
* fenôenos corpusculares: quantização

-   historia

    -   Teoria corpuscular da luz → newton → luz é uma partícula
    -   Já existiam teorias de que a luz seria uma onda, mas a teoria de
        newton prevalecia.
    -   1865 → Maxwell, ao unificar a elétrica e o magnetismo, percebeu que
        a velocidade da luz é igual à velocidade das ondas eletromagnéticas.

-    efeito compton

    * demonstra o comportamento corpuscular

    * é similar ao experimento de rutherford (que só faz sentido caso a “coisa” viajante for uma partícula)

    * luz incide no materal

    * é espalahada, e com frequencia diferente

    * O fóton é reletido em anguos diversos, com menos energia (portanto frequencia diferente)

    * A energia após a colisão depende do angulo

      ![image-20210605194356612](Images - 1.0 - Física Moderna/image-20210605194356612.png)

## Experiência de Young

-   Difração da luz

-   Comprovava seu comportamento ondulatório

-   Observou a interferência da luz, que formava "franjas" após
    atravessar dois buracos, causadas pela interferência construtiva e
    destrutiva das ondas.

-   Quanto menor as fendas, mais definidas as franjas.

-   Cor das franjas

    -   Luz monocromática vermelha → franjas vermelho-escuro

    -   Luz monocromática Verde → franjas verde-escuro

    -   Luz Policromática branca → faixa central branca e demais
        coloridas

![Resultado de imagem para Experiência de
Young](./Images/3.0 - Dualidade da luz /media/image1.png){width="2.6125in"
height="2.3625in"}![Resultado de imagem para Experiência de
Young](./Images/3.0 - Dualidade da luz /media/image2.jpeg){width="3.4625in"
height="1.8770833333333334in"}

## Efeito Fotoelétrico 

-   Indica a natureza discreta (fotons) da luz, ao invés de uma onda eletromagnética

-   Portanto, seu comportamento corpuscular

-   **History**
    -   1873 (W. Smith), a resistência do Selênio depende da iluminação.
    -   1887 (H. Hertz e W. Hallwachs), luz pode ionizar um gás.
    -   1900 (Plank), teoria da quantização da luz.
    -   1905 (A. Einstein), explicação fotoelétrica.

-   
    
-   
    
-   luz incide -> libera elétron
    
-   Quando a luz incide sobre um material condutor, os elétrons do material são excitados para um estado vazio (possivelente
    condução)
    
-   Photons lose energy to electrons in a material

-   To cause excitation (elétrons "vão embora" ), the light that strikes the semiconductor must have enough energy to raise electrons across the band gap
    
-   If (energy of an interacting photon (hn) \> semiconductor bandgap
    (Eg)) then electron is promoted from valence to conduction;
    formation of an electron-hole pair

-   Para uma certa frequênca, a energia cinética máxima dos elétrons ejetados tem sempre o mesmo valor, idenpendnente da intensidade da luz. 
    
-   **Exited State**

    -   a stimulated electron cannot remain in an excited state
        indefinitely

    -   Falls back with with reemission of electromagnetic radiation
        (não necessariamente no espectro visível.

    -   Energia de Fermi: Energia que corresponde ao estado preenchido
        mais elevado a 0K (E2, na imagem abaixo)

        ![](./Images/3.0 - Dualidade da luz /media/image3.png)
        
        > > Source: materials science and engineering, William D. Callister, David G. Rethwisch; Wiley, 2014

-   **Função de trabalho (Φ)**

    -   Energia mínima para o elétron escapar do corpo

    -   Depende exclusivamente do corpo

    -   Para o elétron fugir efetivamente, o fóton precisa de uma
        energia maior que Φ (para sair com energia cinética)

    -   A energia desse fóton está relacionada à frequência (e=hf), e
        não à "intensidade"

    -   Cada elétron pode absorver um único fóton

-   **Fotoelétrons**

    -   Elétrons que escaparam do corpo

    -   Permitem gerar uma corrente elétrica

$\text{Energia do fotoelétron} = \text{hf} - \varnothing = \frac{mv^{2}}{2}$

![](./Images/3.0 - Dualidade da luz /media/image4.png){width="3.6930555555555555in"
height="1.9638888888888888in"}

-   **Optical versus electronic bandgap**

    -   optical = enough to exite, but not to completely separe (because
        holes and electrons are atracted)

    -   In almost all inorganic semiconductors, such as silicon, gallium
        arsenide, etc., both re almost equal

-   **Fosforecencia**
    -   Absorve radiação, eletron fica exitado, um tempo depois volta e
        libera no espectro visível
    
    -   O tempo para emissão da radiação fosforescente é extremamente
        maior que o tempo usualmente levado por transições eletrônicas
        devido a essas transições estarem associadas com transições de
        estado de energia \"proibidos\" pela mecânica quântica, ou seja,
        por envolverem estados eletrônicos metastáveis
    
-   **Aplicações**

    -   Placas fotovoltaicas

    -   Sensores de luminosidade ou radiação

## Dualidade onda-partícula

-   A luz é entendida, atualmente, como sendo partícula e onda ao mesmo
    tempo (dualidade)

-   Os fenômenos da ondulatória (refração, difração, polarização, etc)
    também se aplicam à luz.

-   Ao mesmo tempo, os fenômenos da ótica clássica (reversibilidade,
    propagação retilínea e independência) também podem ser observados.

-   ==Em um dado instante, a luz se comporta como onda ou como partícula, nunca os dois==

-   **foton**

    -   não possui massa

    -   energia do foton: $E=hf$

    -   O conceito de fóton se aplica a todas as regiões do espectro eletromagnético,
        inclusive as ondas de rádio, os raios X e assim por diante.

    -   Momento linear do eletron (se tem energia, tem momento, segundo a Relatividade):

        ![image-20210605193136721](Images - 1.0 - Física Moderna/image-20210605193136721.png)

## E o elétron\...onda ou partícula?

-   A experiência de Young também funciona com um feixe de elétrons
-   Logo, elétrons também possuem caráter dualístico.
-   “O físico francês Louis de Broglie apresentou uma teoria ousada, baseada na seguinte hipótese: "se fótons apresentam características de onda e partícula \[\...\], se elétrons são partículas, mas também apresentam características ondulatórias, talvez todas as formas de matéria tenham características duais de onda e partícula”
-   [Elétrons podem ter natureza dupla, constituídos por duas partículas](https://www.inovacaotecnologica.com.br/noticias/noticia.php?artigo=eletrons-ter-natureza-dupla-constituidos-duas-particulas&id=010110210524#.YKzwbybQ-u0)

## Laser

* os átomos são induzidos para emitir luz de modo organizado e consistente
* intenso e fino, além de muito monocromático (frequência única)
*   
* absorve energia; ao invés de emitir (voltar para o nível fundamental) de forma espontânea em qualquer direção, emite de forma estimulada
* emissão estimulada: ainda no estado exitado, recebe energia; a emissão acontece na mesma direção da onda recebida
* é preciso ter muitos elétrons no estado excitado para que isso dê certo, só funciona em alguns materiais
* 

## quantum oscillations in metals ?

## Características físicas da luz

![Resultado de imagem para velocidade da luz
tabela](./Images/3.0 - Dualidade da luz /media/image5.gif){width="2.4618055555555554in"
height="1.6229166666666666in"}

![](./Images/3.0 - Dualidade da luz /media/image6.png){width="5.990277777777778in"
height="2.688888888888889in"}

-   **Cor da mancha de óleo**

![](./Images/3.0 - Dualidade da luz /media/image7.png){width="7.740972222222222in"
height="3.3680555555555554in"}

# Buracos negros
## A história de uma estrela

-   **Estrela "ativa"**

    -   Gravidade funde hidrogênio em hélio (e as vezes outros
        elementos)

    -   Sol → funde 654 milhões de toneladas de H em 650 mt de He

    -   Esses 4 mt de massa viram energia (e=mc²)

    -   Equilíbrio contração-expansão

        -   Gravidade mantêm junto

        -   Calor força a expansão

-   Quando a estrela "fica velha", a força de expansão (calor) diminui

-   O raio da estrela diminui

-   **Anã branca**

    -   A força gravitacional é [menor]{.ul} que a repulsão dos elétrons

    -   O Sol provavelmente vai virar uma

-   **Estrela de neutros**

    -   Força gravitacional [maior]{.ul} que a repulsão

    -   Prótons e neutros se fundem

    -   Vira uma sopa puta densa

-   **Buraco negro**

    -   Funde tudo, até os neutros

    -   Volume é reduzido à zero

    -   $Gravidade\  = \ G\frac{\text{massa}}{\text{rai}o^{2}}$

    -   Se o raio é zero, então a gravidade é infinita

## Características do buraco negro

-   Suga luz, udhasudhasd

-   Horizonte de eventos

-   Radiação hawking

-   Buraco branco

# Aceleradores de partículas

-   **Cíclotron**

    -   São aceleradas por um campo elétrico oscilante

    -   Descrevem uma trajetória circular por causa de um campo
        magnético

    -   A frequência de oscilação do campo elétrico deve ser "casada"
        com o período de revolução da partícula

    -   Só funciona para acelerar particular abaixo de 10% da velocidade
        da luz, pois a partir daí a relação q/m começa a mudar
        significativamente e, consequentemente, o período de revolução
        muda

![ciclotron](./Images/5.0 - #Física de partículas /media/image1.jpeg)

-   **Síncroton**

    -   Resolve o problema de sincronia do cíclotron, além de ser muito
        mais escalável para partículas com raio grande (pois o campo
        magnético não precisa ser imenso)

    -   Controlamos os campo de modo que a trajetória das partículas
        seja circular

