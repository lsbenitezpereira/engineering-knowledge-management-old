# Ondas

### conceituação

* Qualquer sinal que se transmite de um ponto ao outro com velocidade definida
* Transporta energia e momento linear sem, no entanto, transportar matéria 
* Um sinal pode ser transmitido através de uma corda por meio de um pulso
* Simplificadamente, é uma perturbação que se propaga 
* curiosidades

  * COne da lancha: cone de moch 

  * *Patrões de Chladni*, sobre ondas estacionárias em superfícies bidemensionais (geralmente visualizamos pela acumulação de algum tipo de pó sobre os nós)

  * Sobreposições de ondas bi/tri dimensionais dão padrões bem loucos

  	![1544226893824](images/1544226893824.png)

### Classificações

* **Quanto a forma de oscilação**

  * <u>Onda transversal</u>

    * particulas da corda se movimentam perpendicularmente à diverção de $\vec{v}$ 

    	![1544226156392](images/1544226156392.png)

  * <u>Onda longitudinal</u>

    * percorre um meio na mesma direção de v

    * ex: onda em uma mola, ondas sonoras

    	![1544226118770](images/1544226118770.png)

  * <u>Onda mista</u>

    * Movimento em duas direções
    * Ex: onda na agua 

* **classifiação quanto ao tipo**

  * Onda mecânica
    *  precisa de um meio para se propagar 
    *  o meio deve possuir massa (para que possa haver energia cinética) e elasticidade (para
        que possa haver energia potencial)

  * Eletromagnéticas

  	*  Originadas por cargas elétricas oscilantes

  	* Não precisa de meio

  	* Bizu: Ra-Mi-L-U-X   G 

  		​	![1544226312128](images/1544226312128.png)

  * Ondas de matéria

  	* Vei… loucuras de fisica moderna
  	* de eletrons, protons, etc
### Dimensões de propagação
* 1 dimensão
    * ex: corda
    * Crista 		→ pico positivo
    * Vale 			→ pico negativo
    * Amplitude		→ diferença entre a crista e o vale
    * Comprimento (λ)	→ distância entre duas cristas
    * Período (T)		→ tempo entre duas cristas

  ![1544225839417](images/1544225839417.png)

  * 2 dimensões

      * ex: onda na agua

      * Tem uma zueira louca sobre o movimento ser ao mesmo tempo longitudinal e transversal 

      	![1544226014624](images/1544226014624.png)

  * 3 diensões

        * ex: som

### Equacionamento de uma onda

* Faremos aqui apenas para uma onda se propagando em uma dimensão

* Será dada por:
    $$
    y(x, t)
    $$

* onde y é a altura da corda na posição x, no tempo t

* **Ondas progressivas (Caso geral)**

  * qualquer equação que queira descrever uma onda que se propaga no espaço deve ter +- essa cara
  * Criamo dois referenciais: um parado e um se momento junto com a corda (ambos são referenciais inerciais)
  * No referencial que vai junto, y é função só da posição  
  * A posição de uma certa altura é x (para o referencial parado) ou x’ (para o referencial que vai junto) 
  * Os dois referenciais estão relacionados por uma transformação de Galileo, $x=x'-vt$, ou seja:

  $$
  f(x')=f(x-vt)=y(x,t)
  $$

  * Se ela estiver se propagando no sentido negativo, a parte temporal terá sinal positivo

  * Se devirarmos parcialmente essa equação, chegamos em uma EDP que descreve a onda (veremos mais a frente)

  * Quando encontrar uma das extremidades, será refletida e se propagará no sentido contrário, entaõ:

    $$
    y(x,t)=f(x-vt)+g(x+vt)
    $$

* **Equação das Ondas hamônicas**

  * Podem ser descritas por um cosseno

  * Quanto a perturbação em um dado ponto x corresponde a oscilaçoes harmônicas

  * Existe todo um formalismo matemático para funções harmônicas, mas nao entraremos nesse mérito

  * Essa é a equação que utilizaremos durante a maior parte deste curso 

  * Fazemos isso para não precisarmos descrever uma onda por uma equação diferencial (cái em uma EDP)

  * Podemos interpretar uma onda como um conjunto de infinitos osciladores 

  * Fazendo aquela análise para uma onda parada (um cosseno, para facilitar a análise) “andando” junto de um MRU, chegamos em:
    $$
    y(x,t)=Acos(kx-kvt+\phi)
    $$

  * Essa equação satisfaz a EDP da equação da onda

  * Se ela estiver se propagando no sentido negativo, a parte temporal terá sinal positivo

  * $\phi=$ constante de fase (deslocamento do ponto x=0 no tempo t=0)

  * $k=\frac{2\pi}{\lambda}$ onde $\lambda$ = comprimento de onda

  * $v=\frac{\omega}{k}$

  *  k é chamado numero da onda, ou vetor da onda (SI=radiano por metro). Sua interpretação vetorial é util quando analisarmos espalhamento (ao bater em um objeto, por exemplo).

  * Formulas uteis

  	* $\omega=\frac{2\pi}{ T}$, onde T é o período
  	* $\omega=kv$
  	* $f=\frac{\omega}{2\pi}$
  	* $v=\lambda f$




* **EDP da onda** 

  * Uma função que pretenda descrever uma ondadeve ser do tipo $y(x,t)=f(x \pm t)$

  * Uma função desse tipo é descrita por meio de uma EDP, com derivadas particiais em relação ao tempo e a posição

  * chegamos nela aplicando a segunda lei de newton a um elemento de uma onda transversal (mas a mesma equação é válida para todos os tipos de onda)

  * será do tipo:
    $$
    \frac{\partial^2y(x,t)}{\partial t^2}-v^2\frac{\partial^2y(x,t)}{\partial x^2}=0
    $$

  * Essa descrição é valida não apenas para ondas mecanicas, mas também eletromagnéticas, etc

# onda na corda

* Quase tudo o que for feito aqui pode ser extendido para outros tipos de ondas 
* Equação das cordas vibrantes
* 

$$
\frac{\partial^2y(x,t)}{\partial t^2}-\frac{T}{\mu}\frac{\partial^2y(x,t)}{\partial x^2}=0
$$

* Onde T é a tensão da corda e mi a sua densidade 

* Comparando com a equação da onda, chegamos em na velocidade com que a onda se propaga:
  $$
  v^2=\frac{T}{\mu}
  $$

* Perceba que a velocidade de propagação depende apenas das propriedades da corda, e não da onda 

* Lembre-se que $\mu=m/L$

* A velocidade de um pedaço da corda se movendo verticalmente será chamado de u e dado pela derivada parcial em relação ao tempo:
  $$
  u=wA\sin(kx-wt)
  $$








### Intensidade de uma onda

* Quanta energia a onda esta transmitindo 
* Para qualquer onda, temos:

$$
P=(-T\frac{\partial y}{\partial x})\frac{\partial y}{\partial t}
$$
* Consideraremos aqui uma onda harmônica que se transmite como uma onda progressiva 

* Lembre-se que a potência média de um seno é 1/2
* **Energia cinética**
  *  taxa com a qual a energia cinética é transportada pela onda:
  $$
  \frac{dK}{dt}=\frac{1}{2}\mu v w^2A^2\cos^2(Kx-wt+\phi)
  $$
  * a taxa média será:
  $$
  \frac{dK}{dt}_{med}=\frac{1}{4}\mu v w^2A^2
  $$

* **Energia potencial elástica**

  * Exatamente igual à energia cinética
  * Lembre-se que cada pedaço da crda pode ser interpretado como um oscilador hamônico e, portanto, K e U são análogos
* **Potência total**

  * taxa com a qual a energia é trnsportada pela onda

  * Soma da taxa da cinética e da potencial

  * A potência instantânea será dada por:
      $$
      P=TkwA^2\sin^2(Kx-wt+\phi)
      $$

  * A potência média será 
    $$
    \overline{P}=\frac{\mu v w^2 A^2}{2}=2\frac{dK}{dt}_{med}
    $$








### interferência

* princípio de superposição: podemos somar ondas distintas

* Sejam y1 e y2 duas ondas harmônicas

* a resultante será dada por:
  $$
  y'(x,t)=y_1(x,t)+y_2(x,t)
  $$

* A diversão está em analisar a difença entre as defasagens

* A intensidade da onda resultante será:
  $$
  I=I_1 + I_2 + 2\sqrt{I_1I_2}\cos{\phi_{12}}
  $$

* **Interferência destrutiva**

  * fases opostas

  * se subtrairão

  * A maior interferêcia destrutiva será quando $\phi_{12}=(2n+1)\pi, \forall n \in \Z$

  * :red_circle: Nessa situação, simplificamos a equação:
    $$
    I_{min}=(\sqrt I_1 + \sqrt I_2)^2
    $$

  * Mesmo que as ondas se anulem instantâneamente, a corda se "lembra" do seu estado pois ainda possui velocidade 
* **Interferênia construtiva**

  * mesma fase

  * se somarão

  * :red_circle: Na máxima e na condição em que $I_1=I_2$:
    $$
    I_{max}=4I_1
    $$

  *  Se deslocando no mesmo sentido, o halliday diz o seguinte:

  ![1544224797440](images/1544224797440.png)

==2 ou 4 vezes maior???==

### modos normais

* Situação onde tudo fica simplificado, ou onde acontecem coisas legais

* Modos normais também podem ser analisados para osciladores (pendulos se movendo juntos, se movendo de forma espelhada, 3 osciladores acoplados onde um pontos pontos é estacionário, etc)

* **Batimento**

  * Efeito estranho quando duas ondas de frequências semelhantes oscilam 

  * Tendo duas ondas de frequências diferentes

  * Consideraremos as amplitudes iguais, só para facilitar 

  * no final chega em uma expressão com amplitude variável
    $$
    y(x,t)=a(x,t)cos\left(\overline{k}x-\overline{w}t\right)
    \\
    a(x,t)=2Acos\left(\frac{\Delta k}{2}x-\frac{\Delta w}{2}t\right)
    $$

  * Se w1 for proxima de w2, a variação de frequência será baixa, igual a:
  	$$
  	f_{bat}=f_1 - f_2
  	$$

  * Ou seja, a amplitude vai oscilar com uma frequencia menor do que a das duas ondas, o que é chamado de batimento. 

  * Ex: afinando a corda do violão  

  * Também percebemos essa variação na posição, ==mas acho que só perceberiamos esse efeito se nos movermos proximo da velocidade do som ou se medíssemos varios pontos simultaneamente==

* **Ondas estacionárias**

  * Causada pela interferência de duas ondas senoidais iguais que se propagam em sentidos opostos 

  * A equação da onda resultante será:
    $$
    y(x,t)=2Acos(kx)cos(wt)
    $$

  * Perceba que isso não é uma onda progressiva 

  * Teremos um ponto em que $cos(kx)$ forçará a amplitude para zero, independente do tempo

  * Ventre (ou *antinós)*: Deslocamento horizontal máximo

  * Nós: Deslocamento horizontal zero

  * Fixando as extremidades

    * Apenas poderemos observar a formação de determinadas frequências
    * chegamos em $L=n(\frac{1}{2}\lambda)$, onde L é o comprimento do tubo e lambda o comrpiemtno da onda. n é o numero de meios comprimentos de onda
    * $f_n=\frac{n}{2L}v=\frac{v}{\lambda}$
    * Fora dessas frequências, a onda não será estacionária (e será apenas algumas pequenas oscilações, provavelmente caóticas)
    * Lembre-se que, em uma corda, a velocidade será dada por $v=\sqrt{\frac{T}{\mu}}$
    * nº de nós = nós de ventres + 1
    * Isso tambem é chamado de *Lei das cordas vibrantes*
    * Podemos visualizar facilmente isso se pensarmos em uma fonte emitindo uma onda e uma parede refletindo. Como as duas irão se sobrepor, senão duas senoides idênticas em sentidos opostos. 

    ![1544226737741](images/1544226737741.png)

    ![1544226745196](images/1544226745196.png)
  * Ondas estacionárias e ressonânscia
    * Essas frequências tambem sao chamadas de *frequências de ressonancia*, e é somente nelas  que se formam grandes oscilações
    * Nessa situação, podemos interpretar que a frequência externa coincide com a “frequência natural” do sistema (quando o comprimento de onda coincide com alguma distância de vibração)
    * a amplitude de vibração será maior
    * Essas frequências serão múltiplas da fundamental (também chamada de *modo mais baixo*) e chamadas de *harmônicas*  
    * Ex: Ponte de Tacoma Narrows. Taça quebrando

### Reflexão em uma inferface

* dura
	* A interface não permite movimentação (um nó)
	* Pela terceira lei de newton, a reação da interface forçara a corda para baixo
	* o pulso refletido no sentido contrário terá fase oposta ao original
* Mole
	* A interface permite movimentalção livre (um anel em uma barra sem atrito, por exemplo)
	* o deslocamento máximo do anel é duas vezes maior que a amplitude de
		um dos pulsos.
	* o pulso refletido no sentido contrário terá a mesma fase do original

# Ondas sonoras

* Ondas mecanicas longitudinais 

* A descrição matemática para uma dimensão é muito similar à onda em uma corda, então nao sera feita aqui

* Para mais dimensões, k torna-se um vetor

* Velocidade:
  $$
  v=\sqrt{\frac{B}{\rho}}
  $$

* Onde B é o  módulo de elasticidade volumétrico e p a massa específica (medido em Pascal)

* A velocidade do som no ar a 20 C é 343 m/s.
  $$
  B=\frac{\Delta P}{\Delta V/V}
  $$

*  ΔV/V é a variação relativa de volume produzida por uma variação de pressão ΔP

* Frentes de onda

  * superfícies nas quais as oscilações produzidas pelas ondas sonoras têm o mesmo valor

* Raios

  * retas perpendiculares às frentes de onda 

     indicam as direções de propagação





### Intensidade sonora (I)

* razão entre a potência P da onda sonora e a área A da superfície.
* I=P/A
* nível sonoro β em decibéis é dado por:
* $\beta=10log(\frac{I}{I_0})$
* em que I0 é um nível sonoro de referência. I0=10^-12 W/m^2.  próximo do limite inferior da faixa de audição humana

* **Biofísica do ouvido**
  * O timpano humano pode suportar uma variação de pressão (==em qualquer situação, ou só com sons==) de 28Pa, o que para uma onda de 1kHz no ar equivale a um deslocamento de 11um. 
  * Exposição temporária a sons proximos disso diminui a irrigação sanguinea do timpano e causa perda momentanea de audião. 
  * O menor deslocamento que o timpano pode detectar é de aproximadamente 11pm; 





### Efeitos supersônicos

* as frentes de onda se combinam de um padrão chamado de cone de match

* o ângulo de abertura do cone é dado por:
  $$
  \sin \theta=\frac{v}{v_F}
  $$




* sendo v a velocidade do som e $v_F$ a velocidade da fonte (do objeto que se move)
* Uma onda de cque e causada na superficie desse cone pois a superposição das frente de onda causa uma elevação e uma queda brusca na pressão do ar, tornando-se visiveis pois as moéculas de vapor se condensam 
* Também acontece um estrondo supersônico (exemplos: disparo de fusil; chicole estalando)