

# Conceitos básicos

* Fluido é uma substância com capacidade de deformar-se e escoar
* Adquire o formato do recipiente
* Não é capaz de equilibrar tensões de cisalhamento (tangenciais), ou
  seja, permanece em movimente enquanto a força estiver sendo aplicada
* Ou seja, o fluído só estará em equilíbrio se não atuarem forças de
  cisalhamento
* Líquidos → volume bem definido
* Gases → volume varia com o recipiente (ocupam todo o espaço)
* **No slip condition**
  * the water velocity (normal and tangencial) is zero at at the surface
  * due to viscous effects
  * boundary layer: flow region adjacent to the wall in which the viscous effects (and thus the velocity gradients) are significant
* **no-temperature-jump condition**
  
  * a fluid and a solid surface have the same temperature at the points of contact
* **Forças de atuam em um fluido**
  * *Volumétricas*
    * Constantes em todo o volume
    * São de longo alcance
    * Diretamente proporcional ao volume
  * *Superficiais*
    * Entre uma porção limitada e suas porções adjacentes
    * São de curto alcance
    * Diretamente proporcional a área delimitante S
* **Surface tension** ($\sigma_s$)

  * the surface of the liquid acts like a stretched elastic membrane under tension

  * The pulling force that causes this tension acts parallel to the surface

  * SI: N/m

  * due to the attractive forces between the molecules of the liquid (that tends to pull the molecules on
    the surface toward the interior of the liquid)

  * The resulting compression effect causes the liquid to minimize its
    surface area (an then tends to assume an perfect sphere)

    ![1556412759977](Images - Mecânica de Fluídos/1556412759977.png)

* **Capillary Effect**
  * consequence of surface tension 
  * contact angle?


## Viscosidade

* Resistencia ao deslocamento de camadas adjacentes

* Representa quão espesso é o fluido (tende a diminuir com a
  temperatura)

* SI: poise (N*s/m^2)

* Unidade usual: Pascal*segundo

* **Situação 1**

  * laminar

  ![img](Images - 2.0 - Hidrodinâmica/media/image8.png){width="4.15625in"
  height="2.6145833333333335in"}

  * A força tangencial T é proporcional a variação de v

  $$
  T = \frac{F}{A} = \eta\frac{\text{dv}}{\text{dy}}
  $$

  * Onde n é o coeficiente de viscosidade dinâmica (também simbolizado por $\mu$)
  * Essa tensão T também pode ser chamada de tensão cisalhante $\tau$
  * Velocidade com que o liquido se movimenta em relação a força
    aplicada.

* **Situação 2**

  * tubular 
  * Agua passando por um tubo

![img](Images - Mecânica de Fluídos/image9.png){width="2.7708333333333335in"
height="0.8020833333333334in"}

![img](Images - 2.0 - Hidrodinâmica/media/image10.png){width="3.8125in"
height="1.9166666666666667in"}

![1559152902522](Images - Mecânica de Fluídos/1559152902522.png)

* **Viscozidade cinemática**

  * Uma das formas de medir é avaliando o tempo que um fluido demorar para escorrer de um recipiente 
* Unidade: stoke
  
* **Fluido newtoniano ou não**

  * Newtoniano: deformação é proporcional à força aplicada. Viscosidade constante

  * Não newtoniano

    * deformação varia de acordo com a força

    * Viscosidade variável

    *  Fluids for which the shear stress is not linearly related to the
    shear strain rate 
      
    * ![1556411950272](Images - Mecânica de Fluídos/1556411950272.png)

      > slope = viscosity

  * Reologia: estudo dos materiais que se comportam tanto como
    líquidos quanto como sólidos.

  * Ex: agua + amido de milho = aperta e vira sólido.

* **Molecular vision**

  * caused by cohesive forces between the molecules in liquids and by molecular colli-
    sions in gases

* **relation with temperature**

  * ?

    ![1556412010208](Images - Mecânica de Fluídos/1556412010208.png)

## Classificações the fluxos

* flow = escoamento 

* **Viscous versus Inviscid Regions of Flow**

  * inviscid flow regions: where viscous forces are negligibly
  * viscous flow region: boundary layer in which the viscous effects are significant (near the plate surface)

* **Internal versus External Flow**

  * external flow:  flow of an unbounded fluid over a surface (such as a plate or a wire)
  * internal flow: the fluid is completely bounded by solid surfaces (such as a pipe or duct)

* **Compressible versus Incompressible Flow**

  * incompressible:  the density remains nearly constant

* **Laminar versus Turbulent Flow**

  * laminar: highly ordered fluid motion characterized by smooth layers of fluid 
  * Turbulent:  highly disordered fluid motion. greatly enhance mass, momentum, and heat transfer. Higher friction.

* **Steady versus Unsteady Flow**

  * <u>Escoamento estacionário</u>

    * no change at a point with time (in values and in derivates)
    * Onde v é função apenas da posição (e não do tempo)
    * Ou seja:

    $$
      {\overrightarrow{v}}_{r} \equiv {\overrightarrow{v}}_{r}\left( t \right)
    $$

    * Do not confuse with steady with uniform (no change with location)

    * Não significa que está tudo parado, mas que a solução é constante no
      tempo.

    * Diferentes partículas do fluido sempre passarão pelo mesmo lugar com
      a mesma velocidade

    * Boa aproximação para velocidades pequenas, como um rio calmo

    * Linhas de corrente não se cruzam e coincidem com a trajetória

    * Massa que atravessa uma seção transversal:
      $$
      dm = vA\rho dt
      $$

      $$
      dm = \left( v*\overrightarrow{n} \right)\text{Aρdt}
      $$

  * <u>Escoamento turbulento</u>
    * any flow that is not steady
    * É necessário empregar variáveis hidrodinâmicas no gás, ponto a
      ponto,
    * Impossível descrever em termos de variáveis de estado P e V
    * Velocidade varia ao longo do percurso
    * periodic: unsteady flow in which the flow oscillates about a steady mean
    * Transient: unsteady that is still reaching an steady flow 

## Properties of fluids

* **Propriedades extensivas (B)**

  * Proporcionais à quantidade de matéria
  * If we divide the system in two, each half will have <u>half</u> the value of the extensive properties 
  * massa ($m$), energia, quantidade de movimento linear ($\vec Q = m\vec{v}$), quantidade de movimento angular ($\vec{J} \times \vec{\omega}$, onde J é o momento de inércia)

* **Propriedades intensivas (b)**

  * independent of the mass of a system

  * If we divide the system in two, each half will have the <u>same</u> value of the extensive properties 

  * Extensive properties per unit mass are called specific properties

  * b=B/m

    ![1556128870130](Images - 2.0 - Hidrodinâmica/1556128870130.png)

  * energia específica ($e=E/m$), velocidade ($\vec v$), energia cinética especifica ==$k=V^2/2$==, entalpia específica $h=H/m$

  * Temperature, pressure, density, etc

  * massa especifica *não* é uma proriedade intensiva

* **Outras anotações**

  * Para B=m, b=1.
  * Em um sistema fechado, $\frac{dm}{dt}\big |_{SC}=0$
  * Se B=mv, b=v






# Hidrostática

* **Densidade**$\ (\rho)$

  * Massa em relação ao volume total do corpo

  * SI Kg/m³

  $$
  \rho = \lim_{\Delta V \rightarrow 0} \frac{\Delta m}{V} = \frac{\text{dm}}{\text{dv}}
  $$

  * Massa especifica ($\mu$): massa de um corpo maciço pelo seu volume.

  $$
  \mu = \frac{m}{V}
  $$

  * Veja bem, se o corpo tiver “espaços vazios”, a densidade dele
    diminui. Já a massa especifica não, pois ela é uma propriedade da
    substância, independente do seu formato.

* **Pressão**

  * Força por unidade de área

  * Quando lidamos com fluídos, trabalhar em termos de pressão é
        muito mais conveniente do que trabalhar em termos de força

  * SI → Pascal (Pa) → N/m²

  $$
  p = \frac{dF_{\bot}}{\text{dA}}
  $$

* **Princípio de Stevin**
  * A uma mesma altura, a pressão exercida por um fluído será igual
    
  * Mergulhado em um fluído com densidade uniforme e dada a pressão
        P em um ponto 1, podemos descobrir a *pressão absoluta* em um
        ponto 2 localizado abaixo dele por:

  $$
  P_{2} = P_{1} + \rho g(y_{2} - y_{1})
  $$

  $$
  P = P_{0} + \rho gh
  $$

  * O produto $\rho gh = P - P_{0}$ é chamado de *pressão manométrica*
    (diferença entre dois pontos)

  * Interpretação instintiva: a pressão depende só da altura (ou seja,
    só do que está em cima) pois o que está dos lados se anula, afinal,
    a pressão é igual em todas as direções

  * Um objeto mergulhado estará em equilíbrio quando a pressão em baixo
    menos a pressão em cima for igual a força gravitacional

  * A componente z da densidade de força volumétrica $f_{z}$ é igual a
    taxa de variação de pressão em relação a z (equação diferencial a
    hidrostática):

$$\left( f_{z} - \frac{\text{dP}}{\text{dz}} \right)dS\ dz = 0$$

$$f_{z} = \frac{\text{dP}}{\text{dz}}$$

$${\overrightarrow{f}}_{z} = grad\ P$$

-   **Pressão hidrostática**

    -   Força que o liquido exerce sobre os corpos

    -   Depende da profundidade do objeto e da densidade do liquido.

    -   Se $p_{0}$ for tomado na superfície (pressão atmosférica sobre o
        líquido, ou zero caso desconsiderarmo-las), então $\text{ρgh}$
        será a pressão hidrostática.

![http://n.i.uol.com.br/licaodecasa/ensmedio/fisica/hidrostatica/hidro2.jpg](./Images - 1.0 - Hidrostática/media/image1.jpeg){width="1.0695647419072616in"
height="1.6574300087489064in"}

$$P_{h} = d.g.h + P_{\text{atm}}$$

-   **Pressão atmosférica**

  * O ar é um fluido, logo ele também exerce pressão.

  * Por ser compressível, $\rho$ varia com o aumento da pressão

  * Considerando que a densidade $\rho$ é diretamente proporcional à
        pressão $P$, temos:

  $$
  \text{dP} = - \rho gdh
  $$

  $$
  P = P_{0}*e^{- \frac{\rho_{0}}{P_{0}}\text{gh}}
  $$

  * Coluna de ar média = 18.000m de altitude

  * Quanto maior a altura, menor a pressão.

  * Nível do mar 1amt = 101325 Pa

## Teorema de Pascal

-   O acréscimo de pressão exercida num ponto em um líquido ideal em
    equilíbrio se transmite integralmente a todos os pontos desse
    líquido e às paredes do recipiente que o contém.
-    the pressure applied to a confined fluid increases the pressure throughout by the same amount.
-   Pela Lei de Stevin, a pressão depende apenas do desnível:

$$
P = P_{0} + \rho gh
$$

$$
P = P_{0}
$$

-   Dessa forma, uma *variação* de pressão e um dado ponto do fluído
    será transmitida para todo o fluído

-   Essa variação é absoluta, em não relativa: se aumentou 10Pa aqui,
    aumenta 10Pa ali

-   **Aplicação**

    -   Prensa hidráulica

    -   A força aplicada ao longo de uma dada distância é transformada
        em uma força maior ao longo de uma distância menor

    -   Ou seja, o trabalho total não muda

$$P = \frac{F_{1}}{A_{1}} = \frac{F_{2}}{A_{2}}$$

![img](./Images - 1.0 - Hidrostática/media/image2.png){width="2.842772309711286in"
height="2.6458333333333335in"}

## Vasos comunicantes

-   **Pontas abertas**

    -   Consideraremos um recipiente formado por diversos vasos ligados

    -   Visto que todas as aberturas estão à mesma pressão $P_{a}$,
        todas as superfícies estarão na mesma altura

![img](./Images - 1.0 - Hidrostática/media/image3.png){width="3.6666666666666665in"
height="1.9166666666666667in"}

-   **Tubo em U com fluidos diferentes**

    -   Dado um tubo em U, com pontas abertas, preenchido por fluídos
        com densidades diferentes

    -   A relação pressão-altura será dada por:

$$\frac{\rho_{1}}{\rho_{2}} = \frac{h_{2}}{h_{1}}$$

![img](./Images - 1.0 - Hidrostática/media/image4.png){width="2.0833333333333335in"
height="1.5729166666666667in"}

## Princípio de Arquimedes

-   Um corpo imerso em um fluído em equilíbrio sofre uma força, chamada
    *empuxo* (or *thrust*)
-   A força de empuxo (or *buoyant force*) existe porque a pressão que a água exerce sobre o
    objeto aumenta com a profundidade.
-   *A* força será vertical pra cima (contrária a gravidade), com
    intensidade numericamente igual ao peso da água deslocada

$$E = \rho V_{f}g = m_{f}g$$

![img](./Images - 1.0 - Hidrostática/media/image5.png){width="2.6041666666666665in"
height="2.09375in"}

-   **Boia ou afunda?**

    -   Densidade do corpo &gt; densidade do fluido: o corpo afunda

    -   Densidade do corpo = densidade do fluido: o corpo fica em
        equilíbrio com o fluido

    -   Densidade do corpo &lt; densidade do fluido: o corpo flutua na
        superfície do fluido

> ![https://amigopai.files.wordpress.com/2015/06/drawing1.jpg](./Images - 1.0 - Hidrostática/media/image6.jpeg){width="3.3455107174103236in"
> height="2.0320002187226596in"}

-   **Peso aparente**

    -   Peso que um corpo em um fluido “parece” ter

    -   Diferença entre o peso real e o empuxo

![http://www.sofisica.com.br/conteudos/Mecanica/EstaticaeHidrostatica/figuras/e7.GIF](./Images - 1.0 - Hidrostática/media/image7.gif){width="0.7916666666666666in"
height="0.2722222222222222in"}

* Stability of Immersed and Floating Bodies

  * The rotational stability of an immersed body depends on the relative loca-
    tions of the center of gravity G of the body and the center of buoyancy B,
    which is the centroid of the displaced volume

  * the metacentric
    height GM, which is the distance between the center of gravity G and the
    metacenter M—the intersection point of the lines of action of the buoyant
    force through the body before and after rotation.

  * The length of the metacentric height GM above
    G is a measure of the stability: the larger it is, the more stable is the floating
    body.

  * If the Gm is negative, the buoyant force acting on the
    tilted body generate an overturning moment instead of a restoring moment,
    causing the body to capsize

    ![1556417208190](Images - Mecânica de Fluídos/1556417208190.png)




# Hidrodinâmica

* **Vazão (Q, ou $\dot \forall$)**

  * Ou *descarga*, ou *flow rate*
  * Volume de fluído que atravessa uma seção transversal por unidade
    de tempo

  * SI → m³/s

  $$
  Q = \frac{\text{Vol}}{t} = Av
  $$

    -   Podemos também expressar grandezas análogas: vazão de massa (SI kg/s), vazão de peso (SI N/s), etc
    -   Elemento de vazão: $(\vec{v}*\hat{n})dA=d\dot V$ [$m^3/s$]
    -   Mass flow rate (vazão de massa) is denoted by $\dot m$
  
      ![1560301636110](Images - Mecânica de Fluídos/1560301636110.png)
  
* **Fluxo ($\phi, \forall$)**

  -   ==mesma coisa que vazão de massa?==
  -   Fornece o decréscimo por unidade de tempo da massa contida em v

  $$
  \varnothing = \oint_{A}^{\ }{\rho\left( \overrightarrow{v}*\overrightarrow{n} \right)\text{dA}}
  $$

  -   Lei geral da conservação da massa em um fluido:

  ![img](./Images - 2.0 - Hidrodinâmica/media/image1.png){width="3.59375in"
  height="0.8125in"}

## Cinemática de fluídos

* Estamos interessados só em descrever o comportamento do fluído, sem nos importarmos com as causas ou com o desenvolvimento de leis gerais

* system analyse as a control mass (follow the particles) like LaGranje, and control volume as a boundary throgh witch mass can flow like Euler

  ![1560301234065](Images - Mecânica de Fluídos/1560301234065.png)

* **Descrição de LaGrange**

  * Interpretação de um fluído como partículas
  * Pensamos na trajetória de cada partícula
  * Dada n partículas, precisamos de ==2n== equações
  * A posição r pode ser dada por:

  $$
  \overrightarrow{r} = \overrightarrow{f}\left( {\overrightarrow{r}}_{0},t_{0},t \right)​
  $$

  * Método pouco usado, por não ser prático

* **Descrição de Euler**

  * Interpretação de um fluído como um contínuon
  * Dado um ponto r, descrevemos a velocidade no fluido nesse ponto:

  $$
  \overrightarrow{v} = \overrightarrow{f}(\overrightarrow{r},\ t)
  $$

  * Ou seja, definiremos um campo vetorial de velocidades
  
  * Linhas de corrente → vetores tangentes (auxilia no estudo e
    visualização)
    
  * Tubo de corrente → superfície delimitada por linhas de corrente
  
  * Volume control
  
    * or *flow domain*
    * Finite volume where we define  field variables (functions of space and time)
    * Definição das fronteiras onde são computadas as entradas e saidas
    * Collectively, these (and other) field variables define the flow field
    * Geralmente a fronteira é fixa, but it can moves and/or deforms with time
  
  * Material Derivative
  
    * or *total, particle, Lagrangian, Eulerian, and substantial derivative.*
  
    * following a fluid particle as it moves through the flow field 
      
  
  ![1556417731463](Images - Mecânica de Fluídos/1556417731463.png)

## Teorema dos transporte de Reynolds

* fornece leis para sistemas abertos (aqueles que possuem entrada e saída de matéria)

* provides the link between the system and control volume approaches 

* Este teorema descreve o comportamento de uma propriedade extensiva B em um sistema aberto, podemos encontrar o comportamento da propriedade intensiva correspondente $b=\frac{B}{m}$ em um volume de controle VC delimitado por uma supericie de controle SC

*  the time rate of change of the property B of the system is
  equal to the time rate of change of B of the control volume plus the net flux
  of B out of the control volume by mass crossing the control surface: 	
  
  ![1556128023020](Images - 2.0 - Hidrodinâmica/1556128023020.png)
  
* Os termos Bin e Bout serão determinados por uma integral de superficie ao da SC, enquanto o termo B vêm da propria definição (integrando b)

* Chegamos assim na forma tradicional do Teorema de Transportes de Reynolds:
$$
\left.{\frac{dB}{dt}}\right|_{SF} = \frac{\partial}{\partial t}\iiint_{VC}\rho b dV + \oiint \rho(\vec{V}*\hat{n})bdA
$$

* A integral de superficie representa a energia saindo co sistema (em W)
  
* A integral tripla representa a energia dentro do sistema (em J)
  
* Caso a propria surface control (SC) esteja se movendo, o termo $\vec {V}$ será a velocidade relativa, onde:
  $$
  \vec V_r = \vec V - \vec V_{SC}
  $$
  


* Energia
  
  * Se B=energia, b=energia por unidade de massa
  
  * A padronização do livro é: o calor que entra é positivo e o trabalho é positivo qunado entra (quando é reaizado sobre o sistema)
    $$
    \left.\frac{dE}{dt}\right|_{SF}=\dot Q+\dot W
    $$
  
  * u=energia interna = h - pressão/densidade = h-p/rho [J/kg]
  
* **Conservation of some properties**

  * Conservation of mass:

    ![1560301478420](Images - Mecânica de Fluídos/1560301478420.png)

  * Conservation of energy:

    ![1560301488325](Images - Mecânica de Fluídos/1560301488325.png)

  * Conservation of momentum: the momentum of a system remains constant when the net force acting on it is zero

* **Other annotations**

  * During a steady-flow process, the total amount of mass contained within a control volume does not change with time (mCV ! constant)

## Princípio da continuidade

-   Vamos considerar um escoamento estacionário, em que existe uma
    mudança na área (ou de algum outro parâmetro)

-   Pelo princípio da conservação da massa, temos:

$$dm_{1} = dm_{2}$$

$$A_{1}v_{1}\rho_{1} = A_{2}v_{2}\rho_{2}$$

-   Para um fluído incompressível (densidade constante), teremos:

$$v_{2} = \frac{A_{1}}{A_{2}}v_{1}$$

-   Para um fluído compressível e área constante, teremos:

$$\rho_{2} = \frac{v_{1}}{v_{2}}\rho_{1}$$

## Equação de Bernoulli

-   Analisa um fluído se deslocando, variando altura e área
-   Exprime a conservação da energia por unidade de massa
-   O fluido tem que realizar trabalho contra a gravidade e também contra ela mesmo (já que as próximas partículas imporão uma resistência ao movimento)
-   is valid in regions of steady, incompressible flow ( $\rho$ constant )where net frictional forces are negligible

![img](./Images - 2.0 - Hidrodinâmica/media/image2.png){width="4.052083333333333in"
height="3.2291666666666665in"}

$$
\frac{1}{2}\rho v_{1}^{2} + P_{1} + \rho gy_{1} = \frac{1}{2}\rho v_{2}^{2} + P_{2} + \rho gy_{2}
$$

$$
\frac{1}{2}\rho v^{2} + P + \rho gy = constante
$$

> $$\rho = densidade$$

> $$P = pressao$$

> $$y = variacao\ de\ altura$$

* Significado dos termos: 

  ![img](./Images - 2.0 - Hidrodinâmica/media/image3.png){width="7.270833333333333in"
height="2.25in"}

-   é muito comum vermos a equação de bernoulli toda dividida por $\rho$. A equação toda fica com unidade de J/Kg, ou seja, energia por unidade de massa

-   Também é comum ver ela dividida por $g$. Fica com unidade de metros [de coluna de flúido]. 

-   **Demonstração**

    -   Durante um intervalo dt, uma porção fina (filete) se desloca de
        1\~1´ e de 2\~2´ (consideramos o escoamento estacionário)

    -   Pela concentração da massa e pela equação da continuidade, temos
        que a variação da energia cinética será dada por:

  $$
  k = \frac{1}{2}dm_{2}v_{2}^{2} - \frac{1}{2}dm_{1}v_{1}^{2}
  $$

    -   Variação da energia cinética = Trabalho total = trabalho das forças de pressão + trabalho da força gravitacional (consideramos viscosidade zero, campo gravitacional uniforme)

  $$
  W_{pressao} = (\rho_{1}A_{1})(v_{1}dt) - (\rho_{2}A_{2})(v_{2}\text{dt})
  $$

  $$
  W_{\text{gravitacional}} = - g(dm_{2}z_{2} - dm_{1}z_{1})
  $$

    -   Consideraremos $dm_{1} = dm_{2}$ (consideramos incompressível)
  -   Igualando os trabalhos à variação cinética e multiplicando tudo por $\rho$

* Hydraulic Grade Line (HGL) and Energy Grade Line (EGL)
  *  to represent the level of mechanical energy graphically
    using heights
  
*  Equação de bernoulli generalizada
  
  * adiciona a perda de energia (de carga) no equacionamento
  
  * Head loss $h_L$ = perda ao longo do tubo + perdas localizadas (na entrada, na saída, em valvulas, etc)
  
  * Contabilizaremos também a energia bombeada para o fluído $h_{bomba}$ (usando um ventilador, bomba, etc)
  
  * Podemos ainda contabilizar a energia retirada por alguma turbina/gerador presente no sistema
    $$
    \left(\frac{P}{\rho g} + \frac{V^2}{2g} + z \right)_{entrada} + h_{bomba}= \left(\frac{P}{\rho g} + \frac{V^2}{2g} + z \right)_{saída} + h_{turbina} + h_L
    $$
  
  * Usamos essa padrozinação até mesmo para sistemas que não são de hidraulicos (ventiladores, etc)
  
* Considerações sobre potência

  * potência = $\dot W$ (para diferenciar de trabalho, *work*)
  * $\dot W = F d/t = \Delta P AV = \Delta P \dot \forall$
  
  $$
  \dot W_{bomba}*\eta = \dot m * h_{bomba}*g
  $$
  
  $$
  \dot W_{turbina} = \dot m * h_{turbina}*g*\eta
  $$
  
> $\eta$ = eficiência

  * para um sistema em que a velocidade e altura de entrada e saída são iguais (um ventilador, por exemplo), aplicamos bernoulli e chegamos em  $\frac{P_1}{\rho} + \frac{\dot w}{\dot m}=\frac{P_2}{\rho}$. Manipulando, chegamos na útil equação $\dot W = (P_{in} - P_{out})\dot \forall$
  * $h_{bomba}=\frac{\dot W}{\dot m g}$
  * $\dot W = \dot \forall \Delta P$

## Aplicações

-   **Furo em um reservatório**

    -   Aplicamos a eq. De Bernoulli em cima e em baixo (tem que ser
        igual)

    -   $P_{1} = p_{0} = atmosferica$

    -   Densidade igual

    -   Considerando $A_{0} > > A_{1}$, então $v_{0} = 0$

$$v_{1}^{2} = v_{0}^{2} + 2gh$$

$$v_{1} = \sqrt{2gh}$$

- **Tubo de pito**

  -   Usamos um manômetro diferencial para calcular a velocidade de
      algo

  -   Ambas as entradas têm a mesma altura

  -   Dado um P0 na frente e P1 em cima, temos, pelo princípio de
      stevin:

  $$
  P_{0} = P_{1} + \rho gh
  $$

  -   Consideramos $v_{0} = 0$ (o ar para na direção horizontal)
  -   a pressão nas entradas laterais é a pressão estática ($P_e$)
  -   Pressão na frente é a pressão de estagnação ($P_s$)
  -   Pressão dinâmica ($P_d$) = $P_s - P_e$

  $$
  v = \sqrt{\left( \frac{\rho_{\text{fluido}}}{\rho_{\text{ar}}} \right)2gh}
  $$

  $$
  v = \sqrt{\frac{2(P_s-P_e)}{\rho_{ar}} }
  $$

  ![Related image](Images - Mecânica de Fluídos/pitot.gif)

  ![1560303574803](Images - Mecânica de Fluídos/1560303574803.png)

  *  it is important that it be properly aligned with the flow
  * can be used to measure velocity in both liquids and gases.

-   **Tubo de venturi**

    -   Estrangulamos um cano

    -   Aplicamos Bernoulli nos dois pontos (sobre o eixo central) e
        depois a eq da continuidade

    ![img](./Images - 2.0 - Hidrodinâmica/media/image4.png){width="4.010416666666667in"
height="2.4583333333333335in"}

    ![img](./Images - 2.0 - Hidrodinâmica/media/image5.png){width="1.9895833333333333in"
height="1.0in"}

    $$
    P_{1} - P_{2} = \frac{1}{2}\rho v_{1}^{2}\left\lbrack \left( \frac{A_{1}}{A_{2}} \right)^{2} - 1 \right\rbrack
    $$

    -   $\text{Se }A_{1} > A_{2},\ entao\ P_{1} > P_{2}$

    - Se a velocidade é maior, a pressão é menor
    
    - Manipulando as equaçoes e deixando-as em função da diferença de pressão, obtemos: 
      $$
      V_2=\sqrt{\frac{2(P_1-P_2)}{\rho (1-\beta^4)}}
      $$
      $$
      \beta = d_2/D_1 = \text {diameter ratio}
      $$
      
    - Podemos adicionar um fator de correção $C_d$ (coeficiente de descarga) multiplicando a expressão acima inteira, com valor entre zero e um (adimensional), para compensar o atrito (que foi inicialmente descartado)
    
      ![1560303319498](Images - Mecânica de Fluídos/1560303319498.png)
    
      > $ A_0 = A_2 = \pi d^2/4$ = area of the hole
    
    -   Vazão (que é igual na região 1 e 2)
        $$
        \dot \forall = V_2A_2 = V_2 \pi d_2^2/4
        $$

-   **Asa do avião**

    -   A compressão do ar, na parte superior da asa, faz ele circular mais rápido
    -   Se a velocidade do ar é maior em cima, então a pressão é menor

> ![http://4.bp.blogspot.com/-EjdQ\_D9s\_XU/Ud\_pq7UZoOI/AAAAAAAAAF4/Z9dBCwc4jC8/s1600/asa.JPG](./Images - 2.0 - Hidrodinâmica/media/image6.jpeg){width="3.5391447944006997in"
> height="2.773584864391951in"}

* **Cavitation**
  * When the pressure of the liquid in liquid-flow systems dropping below the vapor pressure at some
    locations, and the resulting unplanned vaporization
  *  form bubbles
  * Cause a lot of damage, because that vaporization is violent

## Flow analysis

* depends on the geometry, surface roughness, flow velocity, surface temperature, and type of fluid, among other things.

* viscocity supress fluctuations, and keep the fluid “in line”

* In the entrance region, we’ll have an velocity boundary layer

  ![1560360792578](Images - Mecânica de Fluídos/1560360792578.png)

* transitional flow

  * varia para cada ponto

    ![1560302437338](Images - Mecânica de Fluídos/1560302437338.png)

* **Numero de Reynolds (Re)**
  
  * adimensional
  
  * 
  
    ![1560302203405](Images - Mecânica de Fluídos/1560302203405.png)
  
    >  Vavg = average flow velocity (m/s)
    >
    > D = characteristic length of the geometry (generally diameter)
    >
    >  n = m/$\rho$ = kinematic viscosity
    >
    > 
  
  * <u>valores usuais</u>
    
    * Laminar: Re < 2300
    * transitional: 2300 < Re < 4000
    * Turbulento: Re > 4000
  
* Pressure Drop ($\Delta P_L$) and Head Loss ($h_L$)

  * Esse *head* representa uma diferença de pressão, diferença na altura manometrica de um fluido.  represents the additional height that the fluid needs to be
raised by a pump in order to overcome the frictional losses in the pipe
  
*  the pressure loss for all types of fully developed internal flows (laminar or turbulent flows, circular or noncircular pipes, smooth or rough surfaces, horizontal or inclined pipes) is:
  
  * $\Delta P_L=f\frac{L}{D}\rho \frac{v^2}{2}$
  
    $h_L=\frac{\Delta p_L}{\rho g}=f\frac{L}{D}\frac{v^2}{2g}$
  
    >  D = characteristic length of the geometry (generally diameter)

* Turbulent flow is characterized by random and rapid fluctuations of
swirling regions of fluid, called eddies

* Friction Factor ($f$)

  * or *Darcy–Weisbach friction factor*

    ![1560361064907](Images - Mecânica de Fluídos/1560361064907.png)
    
  * In laminar flow throgh circular pipe: $f=64/ Re$

*  relative roughness

  * e/D
    *  ratio of the
      mean height of roughness of the pipe to the pipe diameter.
  * Used to calculate Friction Factor ($f$)

* Colebrook equation

  * Implicity relationates f and e/D
  * If we want to find $f$, the solution must be iterative 
  * can be used only to fully developed turbulent flow
  
* The plot of this formula is known as the Moody chart.
  
  ![1560304937777](Images - Mecânica de Fluídos/1560304937777.png)
  
* Procedimento de resolução

  * Calcula-se Re=$\rho vD/ \mu$

  * Se Re<2300, $f=64/ Re$

  * Se 2300<Re<4000

    * use o fator de atrito (fator de Darcy-Weisbach) e o fator de segurança 

      $\Delta p_L=f\frac{L}{D}\rho \frac{v^2}{2}$

      $h_L=\frac{\Delta p_L}{\rho g}=f\frac{L}{D}\frac{v^2}{2g}$

  * Se Re>4000

    * Calcule a rugosidade relativa e/D
    * use colebrook ou Moody para calcular f

  

## Análise dimesional

* Similaridade entre modelo e realidade
  * Se houver uma relação linear entre o modelo e a realidade, para alguma variável, queremos analisar isso
  * geometrica: comprimentos relacionados por um mesmo fator de escala
  * cinemática: velocidades relacionados por um mesmo fator de escala
  * Dinâmica: forças relacionados por um mesmo fator de escala
* Todos os numeros adimensionais relevantes representando relaçẽos entre forças devem ser iguais no modelo e na realidade
* Geralmente o modelo nos dá a situação máxima (ex: velocidade maxima), enquanto na realidade a coisa será menor por causa das perdas não levadas em consideração

# Others

*  The forms of energy related to the molecular structure of a system and the degree of
  the molecular activity are referred to as the microscopic energy. The sum of
  all microscopic forms of energy is called the internal energy of a system,
  and is denoted by U (or u on a unit mass basis).

* 





