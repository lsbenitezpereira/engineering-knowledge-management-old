# Sex-economic sociology

* the diverse political and ideological groups in human society correspond to the various layers of human character
  structure.
* One has to distinguish ordinary militarism from fascism. Germany under the Kaiser was militaristic, but not
  fascist
* born out of the attempts to harmonize the depth psychology of Freud with the
  economic theory of Marx.
* method of research which developed over many years through the application of functionalism
  to human sex life
* Concepts

  *  "communistic," "socialistic," "class-conscious" were replaced by
     such sociologically and psychologically unequivocal terms as "revolutionary" and "scientific." What they mean is "radically changing things," "rationally active," "going to the roots of things."
  *  As thinking develops, old concepts change and new concepts take the place of obsolete
     ones. The Marxist "consciousness" was replaced by "dynamic structure," "needs" by "orgonotic instinctual processes," "tradition" by "biological and characterological rigidity,"
* substitute gratifications

  * The suppression of natural sexual gratification leads to various kinds of substitute gratifications. 
  * Example: Natural aggression becomes brutal sadism which then is an essential mass-psychological factor in imperialistic wars
  * Example: militarism is essentially libidinous, to attract youths that, as a result of sexual suppression, is sex-starved

* I presented the sex-economic viewpoint in the form of a few questions:
  1. The church contends that the use of contraceptives, like any interference with natural procreation, is against
     nature. Now, if nature is so strict and so wise, why has it created a sexual apparatus which impels to sexual
     intercourse not only when one wants children, but on an average of two to three thousand times in the period of
     adult life?
  2. Will the representatives of the church who are present here openly state whether they have sexual intercourse
     only when they want to create children? (They were Protestant pastors.)
  3. Why did God create two kinds of glands in the sexual apparatus, one for sexual excitation and one for
     procreation?
  4. Why do infants develop a sexuality, long before the function of procreation develops?
     The stammering answers of the church representatives evoked a roar of laughter. I then explained the role of the
     negation of the pleasure function on the part of the church and of reactionary science; that the suppression of
     sexual gratification had precisely the function of making people submissive and resigned, incapable [109] of
     rebelling against their economic position

## Sex-economic analysis of autoritarism

* Much of this material is based on Wilhelm Reich’s book “The Mass Psychology of Fascism”

* **Family and autoritarism**

  * At first, the child has to adjust to the structure of the authoritarian miniature state, the family; this
    makes it capable of later subordination to the general authoritarian system. The formation of the authoritarian
    structure takes place through the anchoring of sexual inhibition and sexual anxiety
  * The difference between the sexual
    ideology of the average revolutionary and the average reactionary woman is decisive: the anti-sexual, moralistic
    structure of the conservative woman makes it impossible for her to develop a consciousness of her social position,
    it ties her to the church as much as it makes her afraid of "Sexualbolschewismus."
  * The authoritarian state has a representative in every family, the father; in this
    way he becomes the state's most valuable tool
  * <u>Dictator as the Father</u>
    * national and family fixation are
      identical. This fixation [53] is intensified by another process. The Nationalist Führer means, to the masses, the
      personification of the nation. A personal fixation on him develops only to the extent to which he actually
      personifies the nation in terms of the nationalistic feeling of the masses. If he knows how to arouse the familial
      fixation in the mass individual he also becomes an authoritarian father figure
  * <u>motherhood is not feminism</u>
    *  The
       idolatrous idealization of motherhood is grossly at variance with the brutality with which mothers among the
       working population are actually treated. This idealization of motherhood is essentially a means of keeping women
       from developing a sexual consciousness and from breaking through the barriers of sexual repression, of keeping
       alive their sexual anxieties and guilt feelings.
    *  sexual act for pleasure degrades the woman and mother; she who affirms pleasure and lives
       accordingly is a "whore."
    *  motherhood
       must be idealized at the expense of the sexual function of woman

* **Homeland and autoritarism**

  * The subjective, emotional core of the ideas of homeland and nation are ideas of
    mother and family. The mother is the homeland of the child, as the family is its "nation in miniature."

  *  state imperialism reproduces itself in family imperialism.

  * Goebbels about “whether the Jews are humans”:

    > If somebody hits your mother in the face with a whip, are you going to say, thank you? Is he human? He is not human, he is a beast! How many worse things has the Jew done to our mother Germany and is he still doing! He has spoiled our race, he has undermined our morality and has sapped our strength… The Jew is the personified demon of decay… he begins
    > his criminal butchery of the nations

* **Religion and autoritarism**

  * Every religion is liberation from the world and its forces by union with God. For this reason, bolshevism will not be able
    completely to put people in chains as long as there is any religion left in them. (Braumann, p. 12)
  * Nationalistic and family feelings are closely interlinked with more or less vague, more or less mystical, religious
    feelings
  * the function of "liberating from the world,"
    of diverting attention from everyday misery, to prevent a rebellion against [107] the true causes of the misery
  * Religion acts on the masses like opium (Marx)

* **Sexual supression and autoritarism**

  *  economic interests of marriage and inheritance,  preserving the possessions which were
     gained through exploitation of the lower strata
  *  ruling caste begin to show an
     interest in the "morality" of the suppressed. With the development of an organized working-class, then, there goes
     hand in hand an opposite process, that of an ideological adaptation to the ruling class
  *  sub that to more conservadorism, homeland fixation, and stregeninng of traditional family
  *  the sexual inhibition of the average adolescent blocks his way to rational thinking and
     feeling.
  *  Ungratified sexuality readily turns into rage.
  *  The suppression of the natural sex life of children and adolescents serves the function of structuring people
     who are willing bearers and re-producers of a mechanistic authoritarian civilization;
  *  The suppression of nature, of "the animal" in children, was the first tool for the production
     of machine-like obedient humans; it still is

## Work democracy

* Natural work democracy is politically neither "left" nor "right." It embraces anyone who does vital work; for
  this reason, its orientation is only and alone forward
* authoritarian mechanistic civilization which has ruined human
  biological functioning
* Civilization can consist in nothing but the establishment of the optimal conditions for the development
  of the natural functions of love, work and knowledge
* natural work democracy is not a social system yet to be established but an
  existing system, one that is to the political party system as is water to fire.
* Work democracy does not restrict the concept of "worker" to the industrial worker. It calls a worker everyone
  who does socially necessary work
* the political machinery is often a necessity, but it is a makeshift made necessary by human irrationalism. If
  work were identical with the social ideology, if needs, gratification of needs and means of gratifying them were
  identical with the human structure, then there would be no politics, because then it would be superfluous
* Sex and work
  *  the more satisfactory the sex life, the fuller and more pleasurable is the work
     achievement
  *  Work and sexuality derive from the same biological
     energy
  *  Natural love, vitally necessary work and scientific search are rational life functions.