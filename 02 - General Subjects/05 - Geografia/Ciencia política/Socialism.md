# Conceptualization

* The sociological analysis of Karl Marx is written on *Sociology*
* We’ll deal here mainly with the politics and history of socialism, not the socioogical basis

# Marxism

* The Marxist concept of private property did not pertain to people's shirts, pants, typewriters, toilet paper, books, beds, savings, residences or plots of land. It referred, exclusively, to the private possession of the social means of production which determine the social process, such as railroads, power plants, mines, etc. The "socialization of the means of production" became a bogey because it was confused with the "expropriation of private property" such as chickens, shirts, books, residences, etc
* The revolutionary achievement of Marx did not consist in writing proclamations or pointing to revolutionary goals, but in recognizing the industrial productive powers as the progressive social force, and in realistically describing the contradictions in capitalist economy.

# Lenin

*  first act of Lenin's program, the establishment of the "dictatorship of the proletariat," succeeded.
*  The second and most important act, the replacement of the proletarian administrative apparatus by social selfgovernment, failed to come about.
*  **Dictatorship of the proletariat**
   *  serves as a transition from the previous form of society to the striven-for "communistic" form.
   *  It’s necessary, given the actual conditions 
   *  When the seizure of power was at hand, in 1917, Lenin had to ascribe to the "transition period" a greater significance than Engels
*  **State fade away**
   *  State have to make itself progressively unnecessary, just as an educator becomes unnecessary after having done his job with the child