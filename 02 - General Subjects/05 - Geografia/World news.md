# Politics

## USA

* 2020 - Iran vs USA
  Iraque Prime Minister Adel Abdul Mahdi labelled the missile strike that killed Soleimani (Iran military chief) as a "brazen violation of Iraq's sovereignty and a blatant attack on the nation's dignity".

* 2020 elections
  * Biden: democrat. trustable, popular among blacks, but also with Olds And moderates

## Brazil

* Governo bolsonaro
  
  * Bolsonaro assumiu com apoios de três fontes: ala militar (mais sóbria), ala técnica, ala ideológica (onde alguns são militares)
  * general heleno: ala ideológica. 2020/05/22: deu declarações que podem ser  interpretadas como uma ameaça ao supremo e à estabilidade das  instituições
  * Mandetta: técnico; médico ministro da saúde, com excelente comunicação com a população; demitido.
  * Taich: técnico; médico ministro da saúde (após mandetta); se demitiu depois de 1 mês
  
* Bolsonaro perde apoio e governança por não saber lidar com o covid19
  
  * https://revistaforum.com.br/politica/por-pressao-do-exercito-braga-netto-atua-como-presidente-no-lugar-de-bolsonaro/
  * https://www.diariodocentrodomundo.com.br/acordo-das-forcas-armadas-coloca-general-braga-netto-como-presidente-operacional/
  * General Braga Neto (o da invervençaõ milat no rio, atual chefe da Casa Civil e coordenador o comitê do coronavírus do Governo Federal) começa a ter mais importância 
  * Uma onda de notícias sobre isso parece ter começado depois de um areportagem publicado no DefesaNet [site de direita]
  * Uma posição mais racional, apesasr de ter sido publicada em um jornalzinho direitista militar: https://www.sociedademilitar.com.br/wp/2020/04/braga-neto-e-sua-nova-funcao-de-presidente-operacional-interpretacoes-grotescas.html
  
* 2020/05/22: gravação reunião ministerial

  * Foi o Moro que disse pra vazar, dizendo que o Bolsonaro explicitava que queria intervir na PF
  * Vazada pelo Celso de Mello (STF)
  * Só que tudo foi muito mais positivo do que eu pensava, todos falaram de  mudar o brasil e nada de politicagemos discursos agradam MUITO 70% do  brasil, desagradam MUITO 20%, e é inóquo para o resto
  * Linguajar super informal, sem muito stress
  * cheio de palavrão, com trechos bem agressivos. A globo acha que isso será  negativo para ele... eu acho que talvez seja até positivo
  * será que foi armado? pode ser, por que o impacto líquido na população acho que vai ser positivo
  * O que outros disseram
    * o ministro da educação (weitrauber) falou de coração que estava decepcionado por não estarem lutando contra os privilégios
    * Ministro da economia (paulo guedes) reforçou, bem falado, de reformar para crescer
    * Ministro da educação (ricardo Salles) defendeu a simplificação das leis; 
    * Ministro da justiça (Moro): eforçou o combate a corrupção; falou de forma informal, não parecia discurso; 
    * Ministra da mulher e direitos humanos (damares alves): falou em prol da cultura  BRASILEIRA, incluindo seringueiros, mulheres vítimas de violência, e...? Falou mal de cigano?? Falou contra aborto e contra feministas

  * O que bolsonaro disse
    * fez um discurso que não queria ditadura
    * a favor do POVO, da sua liberdade e da sua soberania
    * reforçou suas pautas de Deus, Família, etc; falou que não vai governar com o rabo entre as pernas e que governa sem  frescurada, e que se ele cometer (no futuro) corrupção então tem que ser preso mesmo
    * “eu to me lixando pra reeleição, quero governar bem” (citação não exata)
    * Reclamou que a PF não dava informações

## Coreia do norte

* abril 26: será que morreu? quem assume?
  * https://exame.abril.com.br/mundo/quem-e-a-irma-que-suceder-kim-jong-un-na-coreia-do-norte/
  * “Seria uma tremenda bagunça, como reconhece a consultoria. Caso venha a  ser a escolhida, Kim Yo Jong assumiria um dos trabalhos mais difíceis do planeta.”
  * Se ele morrer pode acontecer uma briga internacional por “apadrinhar” essa nação, travestida de ajuda humanitária

# Natural disasters

## Covid19

China may have hidden 43000 cases (2020/03/23): https://www3.nhk.or.jp/nhkworld/en/news/20200323_37/

was not created in lab: https://jornal.usp.br/ciencias/ciencias-biologicas/estudo-genetico-mostra-por-que-virus-da-covid-19-nao-foi-feito-em-laboratorio/?fbclid=IwAR17Ic_9B42IVjD6GzXMX50ktpMUjPgvpPtTeB-2pS0_tRVnWCo3aodHEus

* COVID atach more than lungs: https://www.todayonline.com/world/coronavirus-attacks-lining-blood-vessels-all-over-body-swiss-study-finds
* Long tail desease
  * https://www.theguardian.com/world/2020/may/15/weird-hell-professor-advent-calendar-covid-19-symptoms-paul-garner
  * about one in 20 Covid patients experience long-term on-off symptoms
  *  The best parallel is dengue fever, Garner suggests – a “ghastly” viral infection of the lymph nodes which he also contracted. “Dengue comes and goes. It’s like driving around with a handbrake on for six to nine months.”
  * Lynne Turner-Stokes, professor of rehabilitation medicine at King’s  College, says Covid is a “multi-system disease” which can potentially  affect any organ. It causes microvascular problems and clots. Lungs,  brain, skin, kidneys and the nervous system may be affected.  Neurological symptoms can be mild (headache) or severe (confusion,  delirium, coma)
* Possibilidade de resurgir
  * https://www.reuters.com/article/us-health-coronavirus-wuhan-secondwave/beware-second-waves-of-covid-19-if-lockdowns-eased-early-study-idUSKBN21D1M9
  * “If they relax the restrictions gradually, this is likely to both delay and flatten the peak.” 
* **Tech options**
  * all except from AI
  * Ventilators
    * https://www.cbc.ca/news/canada/london/pandemic-ventilator-design-covid19-1.5511412
    * How they work: https://hackaday.com/2020/03/25/ventilators-101-what-they-do-and-how-they-work/
    * University of FL also open sourced a ventilator design that uses parts obtained from Home Depot and an Arduino: https://simulation.health.ufl.edu/technology-development/open-source-ventilator-project/
    * [one on Sky](https://www.youtube.com/watch?v=xdZtMgpxnPI).
    * Others: https://www.reddit.com/r/worldnews/comments/fpw6ec/amid_a_critical_shortage_pandemic_ventilator/
* Vaccine
  * [in a post about dont count on a vaccine]\: “im not gonna rely on rain putting out a fire, though it doing just that is a very real possibility” [Random Guy](https://www.reddit.com/r/worldnews/comments/gno75y/top_hiv_scientist_says_he_wouldnt_count_on_a/frb6jb7?utm_source=share&utm_medium=web2x) 
  * "holding out until a vaccine" is counter-productive

### Future of society

* ‘We can’t go back to normal’ The guardian
  * https://www.theguardian.com/world/2020/mar/31/how-will-the-world-emerge-from-the-coronavirus-crisis
  * Three sections: world will change, my change for bad (state survailance and citizen’s privacity), may change for good (polical progress) 
  * “Any glance at history reveals that crises and disasters have continually set the stage for change, often for the better. The global flu epidemic of 1918 helped create national health services in many European countries”
  * “After the terrorist attacks of September 11, government surveillance of citizens exploded”
  * “People who study disasters – and especially pandemics – know all too well their tendency to inflame xenophobia and racial scapegoating”
  * “Although Covid-19 is likely the biggest global crisis since the second  world war, it is still dwarfed in the long term by climate change”
  * “What happens next might depend on the optimists’ ability to transport  such moments of solidarity into the broader political sphere”
* “We cant go back to normal” BBC
    * https://www.bbc.com/future/article/20200424-why-it-will-be-so-hard-to-return-to-normal
    * The stay-at-home order may be new, but I can’t pretend that social  distancing is unprecedented. Our technologies and social media have been distancing us from each other for years
    * old normal is the one in which our healthcare systems and governments are not prepared to deal with things like Covid-19
    * These three definitions of normality – (1) statistical, (2)  aspirational, (3) functional – often end up sliding into each other  during everyday conversation. This collapse is evident in many of our  discussions about what “the new normal” will look like once Covid-19 is  under control. The new normal will mean that most of us will go back to  most of what we were doing before the pandemic struck (1), but that our  societies will make changes for the better (2), which will end up being  good for the survival of our communities (3).
* “De esta experiencia podemos ver surgir una civilización más sensible”
    * https://www.latercera.com/tendencias/noticia/gay-talese-de-esta-experiencia-podemos-ver-surgir-una-civilizacion-mas-sensible/7XZH6TXZSJCQDIXKDDIQ3BPRGM/

### Countries

* Brasil
  * mais um PORNunciamento absurdo do bolsonaro: https://www1.folha.uol.com.br/poder/2020/03/em-pronunciamento-bolsonaro-critica-fechamento-de-escolas-ataca-governadores-e-culpa-midia.shtml
  * Coronavírus já estava em esgoto no Brasill em novembro... - Veja mais em https://www.uol.com.br/vivabem/noticias/agencia-estado/2020/07/03/estudo-detecta-coronavirus-em-esgoto-de-santa-catarina-em-novembro.htm?cmpid=copiaecola: 
* Russia
  * The Putin Regime Cracks
  * The image of ‘strong putin’ desapeared in this crisis, and the seam just a normal leader wanting to fix the economy
  * https://www.themoscowtimes.com/2020/05/07/the-putin-regime-cracks-a70211
* Sweden
  * Softer approach, based on personal responsibility
  * gatherings of up to 50 people.
  * https://www.bbc.com/news/world-europe-52076293
  * https://unherd.com/2020/03/all-eyes-on-the-swedish-coronavirus-experiment/?tl_inbound=1&tl_groups[0]=18743&tl_period_type=3
  * https://www.foreignaffairs.com/articles/sweden/2020-05-12/swedens-coronavirus-strategy-will-soon-be-worlds?utm_campaign=fb_daily_soc&utm_source=facebook_posts&utm_medium=social&fbclid=IwAR19CLhtAcxoCg0S0mbaQzURzi4cxl9tdeCfoY9FzmFoAluoZa5MORWTA-4
    * As the pain of national lockdowns grows  intolerable and countries realize that managing—rather than  defeating—the pandemic is the only realistic option, more and more of  them will begin to open up.
    * the chief epidemiologist at  Sweden’s Public Health Agency, has projected that the city of Stockholm  could reach herd immunity as early as this month [mai]
    * Lockdowns are simply not sustainable for the amount of time that it will likely take to develop a vaccine
* Sweden vs BRazil
  * http://www.rfi.fr/br/brasil/20200413-modelo-de-isolamento-da-su%C3%A9cia-contra-covid-19-n%C3%A3o-deve-ser-seguido-pelo-brasil-opina-cientista-sueco?ref=wa
  * O país também está triplicando sua capacidade hospitalar, e o  governo anunciou desde a primeira hora da crise uma rede de proteção  econômica dos cidadãos, com medidas como o subsídio de até 90 por cento  do salário de trabalhadores afastados dos empregos.
  * “Implementar medidas de contenção mais brandas, como fez a Suécia, e que têm sido respeitadas pela maioria dos cidadãos, pode ser mais eficaz do que intervenções mais rígidas que são desprezadas com frequência por  cidadãos”
* Germany
  * Why it relaxed measures so soon (april)? see Angela Merkel explanation: https://9gag.com/gag/a9RQDRomach

### Cloroquina

* DIDIER RAOULT interview
  * Translation: https://gloria.tv/post/Yedr9NP3eKZV1MwMVc2Zvwaq4
  * Original: https://www.upr.fr/actualite/pour-comprendre-les-enjeux-du-traitement-du-coronavirus-par-la-chloroquine-nous-vous-conseillons-de-lire-lentretien-du-professeur-raoult-dans-le-parisien-du-22-mars-2020/
  * He say that cloroquinas seams effective, so its cruel to wait for extensive evidence, we should use it now
  * O pânico também trará consequências fortes
  * virus disappears after six days, e o medicamneto  é ministrado ao longo de 10
  * “ Since Hippocrates, the doctor does what is best, to the best of his knowledge according to the state of scientific knowledge.”
  * isolation -> people just go to hospital when very sick -> die easily
  * [large scale isolation] have never done this in modern times
  * We don’t really know how far the virus reaches. But certainly not more than a meter
  * Adaptação para o brasil
    * Preço do medicavamento
    * disponibilidade do medicamento (em número)
    * As medidas de contenção não seriam efitivas e deleyar a doença, e assim ganharmos tempo para produzirmos nacionalmente e em larga escala (solucionando os dois problemas acima)?
    * He defends tests on a great scale… if brazil cant do it, would not be better to isolate all?

### Rise of dictatorships

Russia have nationwide proxy server since 2019

In this crisis, russia is being super aggressive in lockdown: https://www.bbc.com/news/world-europe-52109892, https://www.insider.com/putin-told-russia-take-a-holiday-kremlin-clarified-2020-4

Russia and chine are acused from spreading fake news (https://www.politico.com/news/2020/04/01/russia-china-fake-news-158739), “to fuel distrust in the ability of democratic institutions to deliver effective responses” 

Hackers attack social Italy’s security website: http://www.rfi.fr/en/europe/20200401-hackers-attack-social-security-website-as-italians-apply-for-covid-19-aid-aid

Bolsonaro spread fake news about desabastecimento: https://politica.estadao.com.br/noticias/geral,bolsonaro-publica-video-sobre-risco-de-desabastecimento-mas-depois-apaga,70003256103

* Surveylance and privicity
  * “People need to know there is a possibility that the media and those in  power will create fear and panic, which causes people to more willingly  give up their rights and privacy” Reddit, https://www.reddit.com/r/Futurology/comments/fts4pd/big_brother_in_the_age_of_coronavirus_100_groups/fm9l1pp?utm_source=share&utm_medium=web2x
  * "By selling tools of surveillance as public health solutions,  authorities and all-too-willing companies could rewrite the rules of the digital ecosystem in corona-colored ink—which we fear is permanent."
    —Peter Micek, Access Now
* After plages the world gets worse? gets better?
  * https://www.theguardian.com/commentisfree/2020/apr/18/history-fairer-society-coronavirus-workers-black-death-spanish-flu
* Antidemocratic governments in Europe
  * https://www1.folha.uol.com.br/mundo/2020/03/primeiro-ministro-da-hungria-obtem-poder-para-governar-por-decreto.shtml
  * https://www1.folha.uol.com.br/mundo/2020/04/europa-abre-investigacao-contra-reforma-do-judiciario-na-polonia.shtml

### Disaster readiness

* USA lost its rediness from Cold War: https://theconversation.com/cold-war-style-preparedness-could-help-fight-future-pandemics-135893
* Finland
  * https://globoplay.globo.com/v/8482511/?utm_source=whatsapp&utm_medium=share-bar

### Global politics

* Economia mundial e crises [epidêmica]
  * Átila entrevistando Monica de Bolle, Especialista em Crises Financeiras
  * https://www.youtube.com/watch?v=rPU28Sa9OJw&feature=youtu.be
  * PIB chines caiu 7% (1º trimestre 2020 vs 1º trimestre 2019)
* o virus como uma arma biológica
  * Os países estão buscando aumentar sua autonomia produtiva, que antes era baseada na china, então dificilmente a economia chinesa se beneficiará
  * pode ter sido uma arma biológica indireta? 
    * quero dizer, eles sabiam que iria surgir naturalmente
    * Se eles tivessem feito em laboratório, isso seria descoberto
    * Deixa surgir naturalmente, maqueia quando surgir, segura internamente, deixa espalhar pelo mundo com o maximo de dano no exterior e o mínimo de dano possível na china
    * a china vai perder espaço, ou ganhar espaço?
    *  

## Climatic changes

On 26 March, following lobbying from the energy industry, the US Environmental Protection Agency [announced that](https://www.theguardian.com/environment/2020/mar/27/trump-pollution-laws-epa-allows-companies-pollute-without-penalty-during-coronavirus), in recognition of the pandemic’s effects on the workforce, it will not  punish violations of pollution regulations so long as companies can link those violations to the pandemic



* In the past 500 million years, our planet has witnessed five mass  extinctions. Many scientists believe we are currently living through a  sixth. At some point in the future, our species will no longer be  considered the pinnacle of evolution, human beings having been surpassed by other forms of life. [bbc](https://www.bbc.com/future/article/20200424-why-it-will-be-so-hard-to-return-to-normal)


## Gafanhotos 2020

* https://veja.abril.com.br/blog/impacto/nuvem-de-gafanhotos-qual-e-a-relacao-entre-o-clima-e-os-insetos/
  * Relação com o clima
  * “As ondas de gafanhotos se formam após uma forte chuva, seguida por um período de calor, depois de uma estação muito seca. Com essa combinação de fatores, os gafanhotos começam a se reproduzir….”
  * Os extremos do clima já ocorrem, como os períodos intensos de seca e de chuva. Essa é uma consequência das mudanças climáticas, que tornam as transições abruptas mais comuns 
  * 





# Others

* 2020 gas leakage India
  * 
  *  LG Polymers facility, shut for over 40 days due to the nationwide  coronavirus lockdown, was restarted past midnight allegedly without  precautions [1]
  * styrene leaked from the plant during the early hours of the morning, when families in the surrounding villages were asleep [2]
  * muitos expostos (1000 [1], 400 [2]), mas os mortos imediatos foram de quedas e acidentes similares
  * [1] https://www.ndtv.com/india-news/visakhapatnam-lg-polymers-gas-leakage-8-dead-over-1-000-sick-after-gas-leak-at-andhra-chemical-plant-2224643
  * [2] https://www.aljazeera.com/news/2020/05/deadly-gas-leak-india-chemical-plant-hundreds-hospitalised-200507035742356.html
  * History in INdia
    * India witnessed in December 1984 one of the worst industrial disasters  in history when gas leaked from a pesticide plant in the central city of Bhopal. About 3,500 people, mainly in shanties around the plant operated by  Union Carbide, died in the days that followed and thousands more in the  following years. [2]

## Slow rise of dictadorships

* I’m also registering something it in Covid19 section
* Facist learders make fascist populations
  * Talks specifically about BRazil and Bolsonaro
  * https://brasil.elpais.com/opiniao/2020-04-20/preparar-se-para-a-guerra.html?fbclid=IwAR2yEp2XQQq9pmyrVZ1CtTB2e2wwMgtlCxkwoq2pgbURoPwWS1iPt9Rnnbo
  * “[…] há uma parte da população brasileira que deseja isto e se dispôs a jogar roleta russa com todos e com elas mesmas. É este desejo que deve ser  compreendido. Pois esta será sua forma de se sacrificar por um ideal,  mesmo que este ideal não prometa nada mais do que o próprio sacrifício,  nada além de um movimento permanente em direção à catástrofe.” Qual a relação disso com o Equivalente Moral?
  * “Em manifestações pró-governo, cidadãos e cidadãs oposicionistas foram  violentamente agredidos. Quantas semanas ainda faltam para começar os  linchamentos e as balas reais?”