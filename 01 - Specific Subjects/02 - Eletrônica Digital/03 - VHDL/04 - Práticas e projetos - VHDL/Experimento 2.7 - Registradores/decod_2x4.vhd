LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY decod_2x4 IS
	PORT(
		X	: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		Y	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END decod_2x4;

ARCHITECTURE comportamento OF decod_2x4 IS
BEGIN
	Y <= "1000" WHEN X = "00" ELSE
		"0100" WHEN X = "01" ELSE
		"0010" WHEN X = "10" ELSE
		"0001";
END comportamento;