--=================================================--
--	4x16 Bits SIPO Register 
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Version: 1.0 					       16/10/2017
--=================================================--

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

--=================================================--

ENTITY main IS
	PORT(
		SW		: in	std_logic_vector (17 downto 0);
		KEY		: in	std_logic_vector (3 downto 0);
		HEX0	: out	std_logic_vector (6 downto 0);
		HEX1	: out	std_logic_vector (6 downto 0);	
		HEX2	: out	std_logic_vector (6 downto 0);	
		HEX3	: out	std_logic_vector (6 downto 0)	
	);
END main;

--=================================================--

ARCHITECTURE struct OF main IS
	COMPONENT decod_2x4 IS
		PORT(
			X	: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
			Y	: OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT reg_4b_pipo IS
		PORT (
			D		: IN	STD_LOGIC_VECTOR(3 DOWNTO 0);
			CLK		: IN	STD_LOGIC;
			EN		: IN	STD_LOGIC;
			Q		: OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT decod_7seg IS
		PORT(
			X	: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
			Y	: OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
		);	
	END COMPONENT;
	
	SIGNAL F0, F1, F2, F3	: std_logic_vector (3 DOWNTO 0);
	SIGNAL EN				: std_logic_vector (3 DOWNTO 0);
BEGIN
	DC: decod_2x4	PORT MAP (SW (17 DOWNTO 16), EN);
	
	R0: reg_4b_pipo	PORT MAP (SW (3 DOWNTO 0), KEY(0), EN(0), F0);
	R1: reg_4b_pipo	PORT MAP (SW (3 DOWNTO 0), KEY(0), EN(1), F1);
	R2: reg_4b_pipo	PORT MAP (SW (3 DOWNTO 0), KEY(0), EN(2), F2);
	R3: reg_4b_pipo	PORT MAP (SW (3 DOWNTO 0), KEY(0), EN(3), F3);
	
	D0: decod_7seg	PORT MAP(F0, HEX0);
	D1: decod_7seg	PORT MAP(F1, HEX1);
	D2: decod_7seg	PORT MAP(F2, HEX2);
	D3: decod_7seg	PORT MAP(F3, HEX3);
END struct;