-- Four bits adder/subtractor
library ieee;
use ieee.std_logic_1164.all;

entity adder_subtractor_4b is 
	port( 
		mode	: in	std_logic;							-- 0=adder  1=subtractor
		A		: in	std_logic_vector (3 downto 0);		-- Number A (4  bits)
		B		: in	std_logic_vector (3 downto 0);		-- Number B (4 bits)
		S		: out	std_logic_vector (3 downto 0);		-- Result
		Cout	: out	std_logic;							-- Carry out
		V  		: out	std_logic							-- 1=Overflow
	);	
end adder_subtractor_4b;

architecture struct of adder_subtractor_4b is
	-- Full Adder Component
	component Full_Adder is	         			
		port( 
			X, Y, Cin : in std_logic;			-- Positional structure: (X, Y, Cin, Sum, Cout)	
			sum, Cout : out std_logic
		);
	end component;

   -- Interconnecting wires
   signal C : std_logic_vector (4 downto 1);	--intermediate carries
   
begin
	FA0: Full_Adder port map(A(0), B(0) xor mode, mode,	S(0), C(1));	-- Bit 0
	FA1: Full_Adder port map(A(1), B(1) xor mode, C(1),	S(1), C(2));  	-- Bit 1
	FA2: Full_Adder port map(A(2), B(2) xor mode, C(2),	S(2), C(3));  	-- Bit 2
	FA3: Full_Adder port map(A(3), B(3) xor mode, C(3),	S(3), C(4));  	-- Bit 3

	V <= C(3) xor C(4); 												-- Overflow verification 
	Cout <= C(4);                                    	  				-- Cout
end struct;