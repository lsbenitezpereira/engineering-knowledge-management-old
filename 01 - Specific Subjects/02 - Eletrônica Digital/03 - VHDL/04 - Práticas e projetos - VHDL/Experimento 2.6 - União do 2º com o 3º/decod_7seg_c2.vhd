-----------------------------------------------------
--	Decoder binary to 7 segments comom anode display
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Version: 1.0 					       02/10/2017
-----------------------------------------------------

library ieee;   
use ieee.std_logic_1164.all;


entity decod_7seg_c2 is
	port(
		binary_in	: in	std_logic_vector(3 downto 0);		-- 4 binary inputs
		display_out	: out	std_logic_vector(6 downto 0);		-- 7 display outputs
		signal_out	: out	std_logic							-- Sinal (use only "g" led) 
	);	
end decod_7seg_c2;


architecture ARCH of decod_7seg_c2 is
begin
	signal_out <=  not binary_in(3);
	process (BINARY_IN)
	begin
		case BINARY_IN is				
			    --ABCD		    	    -- gfedcba
			when "0000" => DISPLAY_OUT <= "1000000";	-- 0
			when "0001" => DISPLAY_OUT <= "1111001";	-- 1
			when "0010" => DISPLAY_OUT <= "0100100";	-- 2
			when "0011" => DISPLAY_OUT <= "0110000";	-- 3
			when "0100" => DISPLAY_OUT <= "0011001";	-- 4
			when "0101" => DISPLAY_OUT <= "0010010";	-- 5
			when "0110" => DISPLAY_OUT <= "0000010";	-- 6
			when "0111" => DISPLAY_OUT <= "1111000";	-- 7
			when "1000" => DISPLAY_OUT <= "0000000";	-- 8
			when "1001" => DISPLAY_OUT <= "1111000";	-- 9
			when "1010" => DISPLAY_OUT <= "0000010";	-- A
			when "1011" => DISPLAY_OUT <= "0010010";	-- B
			when "1100" => DISPLAY_OUT <= "0011001";	-- C
			when "1101" => DISPLAY_OUT <= "0110000";	-- D
			when "1110" => DISPLAY_OUT <= "0100100";	-- E
			when "1111" => DISPLAY_OUT <= "1111001";	-- F
		end case;
	end process;
end ARCH;