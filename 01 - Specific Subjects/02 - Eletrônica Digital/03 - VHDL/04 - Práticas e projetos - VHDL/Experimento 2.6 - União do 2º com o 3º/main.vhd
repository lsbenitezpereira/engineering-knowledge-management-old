--=================================================--
--	Four bits adder/subtractor + display 7 seg
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Version: 1.0 					       25/09/2017
--=================================================--

library ieee;
use ieee.std_logic_1164.all;

entity main is
	port(
		SW		: in	std_logic_vector (17 downto 0);
		HEX0	: out	std_logic_vector (6 downto 0);
		HEX1	: out	std_logic_vector (6 downto 0);
		HEX4	: out	std_logic_vector (6 downto 0);
		HEX5	: out	std_logic_vector (6 downto 6);
		LEDG	: out	std_logic_vector (8 downto 0)
	);
end main;

architecture struct of main is
	component adder_subtractor_4b is
		port(
			mode	: in	std_logic;							-- 0=adder  1=subtractor
			A		: in	std_logic_vector (3 downto 0);		-- Number A (4  bits)
			B		: in	std_logic_vector (3 downto 0);		-- Number B (4 bits)
			S		: out	std_logic_vector (3 downto 0);		-- Result
			Cout	: out	std_logic;							-- Carry out
			V  		: out	std_logic							-- 1=Overflow		
		);
	end component;
	
	component decod_7seg is
		port(
			binary_in	: in	std_logic_vector(0 to 3);			-- 4 binary inputs
			display_out	: out	std_logic_vector(6 downto 0)		-- 7 display outputs
		);	
	end component;
	
	component decod_7seg_c2	is
		port(
			binary_in	: in	std_logic_vector(0 to 3);			-- 4 binary inputs
			display_out	: out	std_logic_vector(6 downto 0);		-- 7 display outputs
			signal_out	: out	std_logic							-- Sinal (use only "g" led) 
		);	
	end component;
	
	signal result	: std_logic_vector( 3 downto 0);
begin
	D0: decod_7seg				port map (SW(3 downto 0), HEX0(6 downto 0));
	D1: decod_7seg				port map (SW(7 downto 4), HEX1(6 downto 0));

	OP0: adder_subtractor_4b	port map (SW(17), SW(7 downto 4), SW(3 downto 0), result, LEDG(7), LEDG(8));
	D2: decod_7seg_c2			port map (result, HEX4(6 downto 0), HEX5(6));
end struct;
