--=================================================--
--	Four bits ALU
--	Author: Leonardo Benitez
--	Endianess: litle endian
--	Negative representaion: 2 complement's
--	Version: 1.0 					       14/08/2017
--=================================================--

-- Full adder
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity full_Adder is
   port( X, Y, Cin : in std_logic;
         sum, Cout : out std_logic);
end full_Adder;

architecture struct of full_Adder is
begin
   sum <= (X xor Y) xor Cin;
   Cout <= (X and (Y or Cin)) or (Cin and Y);
end struct;

-----------------------------------------------------
-- Nibble full adder
library ieee;
use ieee.std_logic_1164.all;

entity nibble_full_adder is
	port(
		NIBBLE_A	: in	std_logic_vector (3 downto 0);
		NIBBLE_B	: in	std_logic_vector (3 downto 0);
		CARRY_IN	: in	std_logic;
		RESULT		: out	std_logic_vector (3 downto 0);
		OVERFLOW	: out	std_logic;
		CARRY_OUT	: out	std_logic
	);
end nibble_full_adder;

architecture struct of nibble_full_adder is
	component full_Adder is	         			-- Full Adder component
		port( X, Y, Cin : in std_logic;			-- Positional structure: (X, Y, Cin, Sum, Cout)	
              sum, Cout : out std_logic);
	end component;
   --interconnecting wires
   signal C: std_logic_vector(4 downto 1);		--intermediate carries
begin
	BIT_0: full_Adder port map(NIBBLE_A(0), NIBBLE_B(0), CARRY_IN,	RESULT(0), C(1));
	BIT_1: full_Adder port map(NIBBLE_A(1), NIBBLE_B(1), C(1),  	RESULT(1), C(2)); 
	BIT_2: full_Adder port map(NIBBLE_A(2), NIBBLE_B(2), C(2),		RESULT(2), C(3)); 
	BIT_3: full_Adder port map(NIBBLE_A(3), NIBBLE_B(3), C(3),		RESULT(3), C(4)); 
	OVERFLOW <= C(3) xor C(4);
	CARRY_OUT <= C(4);
end struct;

-----------------------------------------------------
-- Fout Bits ALU
library ieee;
use ieee.std_logic_1164.all;
USE ieee.numeric_std.ALL;

entity lab4 is 
   port(  
			MODE: in		std_logic_vector(3 downto 0);	-- Operation (details in anexed table)
			A	: in		std_logic_vector(3 downto 0);	-- Nibble A
			B	: in		std_logic_vector(3 downto 0);	-- Nibble B
			S	: buffer	std_logic_vector(3 downto 0);	-- Result
			F1	: buffer	std_logic;						-- Flag-1 (1 if the result is '0000')
			F2	: buffer	std_logic						-- Flag-2 (logic logic mode: test if A>B)(arithmetic mode: overflow)			
	);
end entity lab4;

architecture struct of lab4 is
	-- Nibble full adder component
	component nibble_full_adder is
		port(
			NIBBLE_A	: in	std_logic_vector (3 downto 0);
			NIBBLE_B	: in	std_logic_vector (3 downto 0);
			CARRY_IN	: in	std_logic;
			RESULT		: out	std_logic_vector (3 downto 0);
			OVERFLOW	: out	std_logic;
			CARRY_OUT	: out	std_logic
		);
	end component;	
	
   -- Sum arithmetic operation wires
   signal SUM_S	: std_logic_vector(3 downto 0);		-- Result
   signal SUM_F2: std_logic;						-- Flag 2 (overflow)
   
   -- Subtraction arithmetic operation wires
   signal SUB_S	: std_logic_vector(3 downto 0);		-- Result
   signal SUB_F2: std_logic;						-- Flag 2 (overflow)
   
   -- A plus 1 arithmetic operation wires
   signal AP_S	: std_logic_vector(3 downto 0);		-- Result
   signal AP_F2: std_logic;							-- Flag 2 (overflow)
   
   -- A minus 1 arithmetic operation wires
   signal AM_S	: std_logic_vector(3 downto 0);		-- Result
   signal AM_F2: std_logic;							-- Flag 2 (overflow)
   
   -- Sagnitude comparator wires
   signal COMP	: std_logic;

begin
	-- Sum arithmetic operation
	SUM: nibble_full_adder port map(A, B, '0', SUM_S, SUM_F2);

	-- Subtraction arithmetic operation
	SUB: nibble_full_adder port map(A, NOT B, '1', SUB_S, SUB_F2);	
	
	-- A + 1 arithmetic operation
	AP: nibble_full_adder port map(A, "0001", '0', AP_S, AP_F2);
	
	-- A - 1 arithmetic operation
	AM: nibble_full_adder port map(A, "1111", '0', AM_S, AM_F2);
	
	-- Magnitude comparator
	COMP <= '1' when A > B else '0';
	
	process (MODE, A, B) is
	begin
		case MODE is
			when "0000" => 		-- OR logic bitwise
				S <= A or B;
				F2 <= COMP;
			when "0001" => 		-- AND logic bitwise
				S <= A and B;
				F2 <= COMP;				
			when "0010" => 		-- NOR logic bitwise
				S <= A nor B;
				F2 <= COMP;				
			when "0011" => 		-- NAND logic bitwise
				S <= A nand B;
				F2 <= COMP;				
			when "0100" => 		-- XOR logic bitwise
				S <= A xor B;
				F2 <= COMP;				
			when "0101" => 		-- XNOR logic bitwise
				S <= A xnor B;	
				F2 <= COMP;				
			when "0110" => 		-- NOT A logic bitwise
				S <= not A;
				F2 <= COMP;				
			when "0111" => 		-- NOT B logic bitwise
				S <= not B;
				F2 <= COMP;				
			when "1000" => 		-- SUM arithmetic operation
				S <= SUM_S;
				F2 <= SUM_F2;
			when "1001" => 		-- SUBTRACTION arithmetic operation
				S <= SUB_S;
				F2 <= SUB_F2;
			when "1010" => 		-- A + 1 arithmetic operation
				S <= AP_S;
				F2 <= AP_F2;
			when "1011" => 		-- A - 1 arithmetic operation
				S <= AM_S;
				F2 <= AM_F2;
			when "1100" =>		-- A shift left logical 1
				S <= std_logic_vector(unsigned(A) sll 1);
				F2 <= '0';
			when "1101" =>		-- A shift right arithmetic 1
				S <= std_logic_vector(unsigned(A) srl 1);
				F2 <= '0';
			when "1110" =>		-- A shit left logical B		
				S <= std_logic_vector(unsigned(A) sll to_integer(unsigned(B)));
				F2 <= '0';
			when "1111" => 		-- A shift right arithmetic B
				S <= std_logic_vector(unsigned(A) srl to_integer(unsigned(B)));
				F2 <= '0';
		end case;
	end process;
	F1 <= '1' when S="0000" else '0';	
end struct;
