
library ieee;
use ieee.std_logic_1164.all;

entity mensageiro is
	port(
		clk			: in std_logic;
		busy		: in std_logic;
		rst			: in std_logic;
		lcd_clear	: out std_logic;
		lcd_enable	: out std_logic;
		lcd_bus		: out std_logic_vector(9 downto 0)
	);
end mensageiro;

architecture behavior of mensageiro is
	type		estado is (s0, s1, s2, s3, s4, s5);
	signal		estado_atual, proximo_estado: estado;
	constant	A: std_logic_vector (9 downto 0) := "1001000001";
	constant	B: std_logic_vector (9 downto 0) := "1001000010";
	constant	C: std_logic_vector (9 downto 0) := "1001000011";
	constant	D: std_logic_vector (9 downto 0) := "1001000100";
	constant	E: std_logic_vector (9 downto 0) := "1001000101";
	constant	F: std_logic_vector (9 downto 0) := "1001000110";
	constant	G: std_logic_vector (9 downto 0) := "1001000111";
	constant	H: std_logic_vector (9 downto 0) := "1001001000";
	constant	I: std_logic_vector (9 downto 0) := "1001001001";
	constant	J: std_logic_vector (9 downto 0) := "1001001010";
	constant	K: std_logic_vector (9 downto 0) := "1001001011";
	constant	L: std_logic_vector (9 downto 0) := "1001001100";
	constant	M: std_logic_vector (9 downto 0) := "1001001101";
	constant	N: std_logic_vector (9 downto 0) := "1001001110";
	constant	O: std_logic_vector (9 downto 0) := "1001001111";
	constant	P: std_logic_vector (9 downto 0) := "1001010000";
	constant	Q: std_logic_vector (9 downto 0) := "1001010001";
	constant	R: std_logic_vector (9 downto 0) := "1001010010";
	constant	S: std_logic_vector (9 downto 0) := "1001010011";
	constant	T: std_logic_vector (9 downto 0) := "1001010100";
	constant	U: std_logic_vector (9 downto 0) := "1001010101";
	constant	v: std_logic_vector (9 downto 0) := "1001010110";
	constant	W: std_logic_vector (9 downto 0) := "1001010111";
	constant	X: std_logic_vector (9 downto 0) := "1001011000";
	constant	Y: std_logic_vector (9 downto 0) := "1001011001";
	constant	Z: std_logic_vector (9 downto 0) := "1001011010";
begin
	-- Fisrt Pprocess
	process(clk)
	begin
		if (rst = '0') then
			estado_atual <= s0;
		elsif (clk'event and clk='1' and busy='0') then
			estado_atual <= proximo_estado;
		end if;
	end process;
	
	-- Second process
	process(estado_atual)
	begin
		case estado_atual is
			when s0 =>
				lcd_clear <= '0';
				lcd_enable <= '0';
				proximo_estado <= s1;
				
			when s1 =>
				lcd_clear <= '1';
				proximo_estado <= s2;
			
			when s2 =>
				lcd_bus <= B;
				lcd_enable <= '1';
				proximo_estado <= s3;
			
			when s3 =>lcd_enable <= '0';
				proximo_estado <= s4;

			when s4 =>
				lcd_bus <= L;
				lcd_enable <= '1';
				proximo_estado <= s5;
				
			when s5 =>
				lcd_enable <= '0';
				proximo_estado <= s5;		
				
		end case;
	end process;
end behavior;