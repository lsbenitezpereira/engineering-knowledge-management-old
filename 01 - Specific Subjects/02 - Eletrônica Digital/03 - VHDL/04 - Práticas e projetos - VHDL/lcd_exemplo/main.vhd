--=================================================--
--	Project:	Color indentifier with FPGA
--	File:		main.vhd 
--	Author: 	Leonardo Benitez and Gencen
--	Endianess:	litle endian
--	Version:	1.0 					   29/10/2017
--=================================================--

library ieee;
use ieee.std_logic_1164.all;

-----------------------------------------------------

entity main is
	port(
		CLOCK_50	: in	std_logic;
		LCD_RW		: out	std_logic;
		LCD_RS		: out	std_logic;
		LCD_EN		: out	std_logic;
		LCD_DATA	: out	std_logic_vector (7 downto 0)	
	);
end main;

-----------------------------------------------------

architecture funcional of main is
	signal lcd_busy, lcd_clear, lcd_enable: std_logic;
	signal lcd_bus: std_logic_vector (9 downto 0);
	component lcd_controller is
		port(
			clk        : IN    STD_LOGIC;						--system clock
			reset_n    : IN    STD_LOGIC;						--active low reinitializes lcd
			lcd_enable : IN    STD_LOGIC;						--latches data into lcd controller
			lcd_bus    : IN    STD_LOGIC_VECTOR(9 DOWNTO 0); 	--data and control signals
			busy       : OUT   STD_LOGIC := '1';				--lcd controller busy/idle feedback
			rw, rs, e  : OUT   STD_LOGIC;						--read/write, setup/data, and enable for lcd
			lcd_data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0)		--data signals for lcd
		); 
	end component;
	
	component lcd_printer is
		port(
			clk       	: in  	std_logic;  					-- System clock
			word  		: in	std_logic;						-- What the printer should print
			rw, rs, en	: out 	std_logic;  					-- Read/write, setup/data, and enable for lcd
			lcd_data  	: out 	std_logic_vector(7 downto 0)	-- Data signals for lcd
		);
	end component;
begin
	--MS: mensageiro		port map (CLOCK_50, lcd_busy, '1', lcd_clear, lcd_enable, lcd_bus);
	--CT: lcd_controllerS	port map (CLOCK_50, lcd_clear, lcd_enable, lcd_bus, lcd_busy, LCD_RW, LCD_RS, LCD_EN, LCD_DATA);
	LP:	lcd_printer		port map (CLOCK_50, '1', LCD_RW, LCD_RS, LCD_EN, LCD_DATA);
end funcional;