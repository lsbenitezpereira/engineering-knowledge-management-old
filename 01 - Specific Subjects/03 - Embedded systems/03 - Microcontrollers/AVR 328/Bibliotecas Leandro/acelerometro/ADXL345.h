/* -----------------------------------------------------------------------------
 * File:			ADXL345.h
 * Module:			ADXL345 Accelerometer Interface
 * Author:			Leonardo Santiago
 * Version:			1.0
 * Last edition:	02/12/2014
 * -------------------------------------------------------------------------- */
 
 
// -----------------------------------------------------------------------------
// Header files ----------------------------------------------------------------

#include "LS_defines.h"
#if __LS_DEFINES_H < 86
	#error Wrong definition file (LS_defines.h).
#endif
#include "LS_i2c_master.h"

// -----------------------------------------------------------------------------
// Constants definitions -------------------------------------------------------
#define ADXL345_DEVICE		0x53
#define ADXL345_BW_RATE		0x2c
#define ADXL345_POWER_CTL	0x2d
#define ADXL345_DATA_FORMAT	0x31
#define ADXL345_DATAX0		0x32

// -----------------------------------------------------------------------------
// Functions declarations ------------------------------------------------------

// Private functions
void ADXL345_read_register(uint8 address, uint8 *buf, uint8 num);
void ADXL345_write_register(uint8 address, uint8 val);

// low power mode
void ADXL345_low_power_on();
void ADXL345_low_power_off();
uint8 ADXL345_get_power_mode();
void ADXL345_measure_on();
void ADXL345_measure_off();
uint8 ADXL345_get_measure_mode();

// Range selection
void ADXL345_full_resolution();
void ADXL345_range_2g();
void ADXL345_range_4g();
void ADXL345_range_8g();
void ADXL345_range_16g();
uint8 ADXL345_get_range();

// Output data rate
void ADXL345_output_rate_3200();
void ADXL345_output_rate_1600();
void ADXL345_output_rate_800();
void ADXL345_output_rate_400();
void ADXL345_output_rate_200();
void ADXL345_output_rate_100();
void ADXL345_output_rate_50();
void ADXL345_output_rate_25();
uint16 ADXL345_get_output_rate();

// measure 
void ADXL345_get_acceleration(int16 *ax, int16 *ay, int16 *az);


/* -----------------------------------------------------------------------------
 * FUTURE IMPLEMENTATION
 * WHO I AM test
 * interrupt configuration
 * FIFO mode
 * SPI communication mode
 * Calibration routine
 * -------------------------------------------------------------------------- */
