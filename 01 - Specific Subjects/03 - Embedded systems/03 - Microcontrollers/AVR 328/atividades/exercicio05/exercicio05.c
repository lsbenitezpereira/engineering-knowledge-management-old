/* -----------------------------------------------------------------------------
 * File:			exercicio05.c
 * Project:			exemplo com teclado matricial
 * Author: 			Leonardo S. e Hazael B.
 * Data de edi��o:	19/08/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_keypad.h"

// Constant definitions
#define COMMON_ANODE	0
#define COMMON_CATHODE	1

// Project definitions
#define DISPLAY_DDR	DDRD
#define DISPLAY_PORT	PORTD

// Function declarations
unsigned char display7Seg (unsigned char hex, unsigned char common);

// Main function
int main()
{
	uint8 key;

	// Display Configuration
	DISPLAY_DDR = 0xFF;
	DISPLAY_PORT = 0xFF;

	// Keypad Configuration
	keypadInit(	0x07, 0x08, 0x09, 0x0A,
				0x04, 0x05, 0x06, 0x0B,
				0x01, 0x02, 0x03, 0x0C,
				0x0E, 0x00, 0x0F, 0x0D);

	while(1){
		key = keypadRead();
		if(key != 0xFF){
			DISPLAY_PORT = display7Seg(key, COMMON_ANODE);
		}
	}

	return 0;
}

unsigned char display7Seg (unsigned char hex, unsigned char common)
{
	unsigned char segmentos;
	
	switch (hex){
		case 0x00:	segmentos = 0b00111111; break;
	    case 0x01:	segmentos = 0b00000110; break;
	    case 0x02:	segmentos = 0b01011011; break;
	    case 0x03:	segmentos = 0b01001111; break;
	    case 0x04:	segmentos = 0b01100110; break;
	    case 0x05:	segmentos = 0b01101101; break;
	    case 0x06:	segmentos = 0b01111101; break;
	    case 0x07:	segmentos = 0b00000111; break;
	    case 0x08:	segmentos = 0b01111111; break;
	    case 0x09:	segmentos = 0b01101111; break;
	    case 0x0A:	segmentos = 0b01110111; break;
	    case 0x0B:	segmentos = 0b01111100; break;
	    case 0x0C:	segmentos = 0b00111001; break;
	    case 0x0D:	segmentos = 0b01011110; break;
	    case 0x0E:	segmentos = 0b01111001; break;
	    case 0x0F:	segmentos = 0b01110001; break;
		default:	segmentos = 0b00000000; break;
	}
	
	if(common == COMMON_ANODE)
		segmentos = ~segmentos;

	return segmentos;
}
