// -----------------------------------------------------------------------------
// File:			ATmega328.h
// Module:			Microcontroller basic interface
// Author:			Leandro Schwarz
// Version:			3.1
// Last edition:	09/04/2012
// -----------------------------------------------------------------------------

#ifndef __ATMEGA328_H
#define __ATMEGA328_H

// -----------------------------------------------------------------------------
// Header files ----------------------------------------------------------------

#include <avr/io.h>
#include <stdio.h>
#include "defines.h"

// -----------------------------------------------------------------------------
// Status Register -------------------------------------------------------------

#define GLOBAL_INTERRUPT_ENABLE()		set_bit(SREG,I)
#define GLOBAL_INTERRUPT_DISABLE()		clr_bit(SREG,I)
// The T, H, S, V, N, Z, and C bits manage internal operations

// -----------------------------------------------------------------------------
// EEPROM Data Memory ----------------------------------------------------------

void EEPROM_write(unsigned char data,unsigned int address);
unsigned char EEPROM_read(unsigned int address);

// -----------------------------------------------------------------------------
// General purpose registers ---------------------------------------------------

#define GERAL_PURPOSE_REGISTER_2_READ(data)		data = GPIOR2
#define GERAL_PURPOSE_REGISTER_2_WRITE(data)	GPIOR2 = data
#define GERAL_PURPOSE_REGISTER_1_READ(data)		data = GPIOR1
#define GERAL_PURPOSE_REGISTER_1_WRITE(data)	GPIOR1 = data
#define GERAL_PURPOSE_REGISTER_0_READ(data)		data = GPIOR0
#define GERAL_PURPOSE_REGISTER_0_WRITE(data)	GPIOR0 = data

// -----------------------------------------------------------------------------
// External memory -------------------------------------------------------------
// Clock system ----------------------------------------------------------------
// Power management ------------------------------------------------------------
// Interrupts ------------------------------------------------------------------

// -----------------------------------------------------------------------------
// I/O ports -------------------------------------------------------------------

#define PULL_UP_DISABLE()						set_bit(MCUCR,PUD)
#define PULL_UP_ENABLE()						clr_bit(MCUCR,PUD)

// -----------------------------------------------------------------------------
// External interrupts ---------------------------------------------------------

#define INT0_SENSE_LOW_LEVEL()			clr_bit(EICRA,ISC01);clr_bit(EICRA,ISC00)
#define INT0_SENSE_ANY_EDGE()			clr_bit(EICRA,ISC01);set_bit(EICRA,ISC00)
#define INT0_SENSE_FALLING_EDGE()		set_bit(EICRA,ISC01);clr_bit(EICRA,ISC00)
#define INT0_SENSE_RISING_EDGE()		set_bit(EICRA,ISC01);set_bit(EICRA,ISC00)
#define INT1_SENSE_LOW_LEVEL()			clr_bit(EICRA,ISC11);clr_bit(EICRA,ISC10)
#define INT1_SENSE_ANY_EDGE()			clr_bit(EICRA,ISC11);set_bit(EICRA,ISC10)
#define INT1_SENSE_FALLING_EDGE()		set_bit(EICRA,ISC11);clr_bit(EICRA,ISC10)
#define INT1_SENSE_RISING_EDGE()		set_bit(EICRA,ISC11);set_bit(EICRA,ISC10)
#define INT0_ACTIVATE()					set_bit(EIMSK,INT0)
#define INT0_DEACTIVATE()				clr_bit(EIMSK,INT0)
#define INT1_ACTIVATE()					set_bit(EIMSK,INT1)
#define INT1_DEACTIVATE()				clr_bit(EIMSK,INT1)
#define PCINT7_0_ENABLE()				set_bit(PCICR,PCIE0)
#define PCINT7_0_DISABLE()				clr_bit(PCICR,PCIE0)
#define PCINT14_8_ENABLE()				set_bit(PCICR,PCIE1)
#define PCINT14_8_DISABLE()				clr_bit(PCICR,PCIE1)
#define PCINT23_16_ENABLE()				set_bit(PCICR,PCIE2)
#define PCINT23_16_DISABLE()			clr_bit(PCICR,PCIE2)
#define PCINT23_ACTIVATE()				set_bit(PCMSK2,PCINT23)
#define PCINT23_DEACTIVATE()			clr_bit(PCMSK2,PCINT23)
#define PCINT22_ACTIVATE()				set_bit(PCMSK2,PCINT22)
#define PCINT22_DEACTIVATE()			clr_bit(PCMSK2,PCINT22)
#define PCINT21_ACTIVATE()				set_bit(PCMSK2,PCINT21)
#define PCINT21_DEACTIVATE()			clr_bit(PCMSK2,PCINT21)
#define PCINT20_ACTIVATE()				set_bit(PCMSK2,PCINT20)
#define PCINT20_DEACTIVATE()			clr_bit(PCMSK2,PCINT20)
#define PCINT19_ACTIVATE()				set_bit(PCMSK2,PCINT19)
#define PCINT19_DEACTIVATE()			clr_bit(PCMSK2,PCINT19)
#define PCINT18_ACTIVATE()				set_bit(PCMSK2,PCINT18)
#define PCINT18_DEACTIVATE()			clr_bit(PCMSK2,PCINT18)
#define PCINT17_ACTIVATE()				set_bit(PCMSK2,PCINT17)
#define PCINT17_DEACTIVATE()			clr_bit(PCMSK2,PCINT17)
#define PCINT16_ACTIVATE()				set_bit(PCMSK2,PCINT16)
#define PCINT16_DEACTIVATE()			clr_bit(PCMSK2,PCINT16)
#define PCINT14_ACTIVATE()				set_bit(PCMSK1,PCINT14)
#define PCINT14_DEACTIVATE()			clr_bit(PCMSK1,PCINT14)
#define PCINT13_ACTIVATE()				set_bit(PCMSK1,PCINT13)
#define PCINT13_DEACTIVATE()			clr_bit(PCMSK1,PCINT13)
#define PCINT12_ACTIVATE()				set_bit(PCMSK1,PCINT12)
#define PCINT12_DEACTIVATE()			clr_bit(PCMSK1,PCINT12)
#define PCINT11_ACTIVATE()				set_bit(PCMSK1,PCINT11)
#define PCINT11_DEACTIVATE()			clr_bit(PCMSK1,PCINT11)
#define PCINT10_ACTIVATE()				set_bit(PCMSK1,PCINT10)
#define PCINT10_DEACTIVATE()			clr_bit(PCMSK1,PCINT10)
#define PCINT9_ACTIVATE()				set_bit(PCMSK1,PCINT9)
#define PCINT9_DEACTIVATE()				clr_bit(PCMSK1,PCINT9)
#define PCINT8_ACTIVATE()				set_bit(PCMSK1,PCINT8)
#define PCINT8_DEACTIVATE()				clr_bit(PCMSK1,PCINT8)
#define PCINT7_ACTIVATE()				set_bit(PCMSK0,PCINT7)
#define PCINT7_DEACTIVATE()				clr_bit(PCMSK0,PCINT7)
#define PCINT6_ACTIVATE()				set_bit(PCMSK0,PCINT6)
#define PCINT6_DEACTIVATE()				clr_bit(PCMSK0,PCINT6)
#define PCINT5_ACTIVATE()				set_bit(PCMSK0,PCINT5)
#define PCINT5_DEACTIVATE()				clr_bit(PCMSK0,PCINT5)
#define PCINT4_ACTIVATE()				set_bit(PCMSK0,PCINT4)
#define PCINT4_DEACTIVATE()				clr_bit(PCMSK0,PCINT4)
#define PCINT3_ACTIVATE()				set_bit(PCMSK0,PCINT3)
#define PCINT3_DEACTIVATE()				clr_bit(PCMSK0,PCINT3)
#define PCINT2_ACTIVATE()				set_bit(PCMSK0,PCINT2)
#define PCINT2_DEACTIVATE()				clr_bit(PCMSK0,PCINT2)
#define PCINT1_ACTIVATE()				set_bit(PCMSK0,PCINT1)
#define PCINT1_DEACTIVATE()				clr_bit(PCMSK0,PCINT1)
#define PCINT0_ACTIVATE()				set_bit(PCMSK0,PCINT0)
#define PCINT0_DEACTIVATE()				clr_bit(PCMSK0,PCINT0)

// -----------------------------------------------------------------------------
// Timer/counter ---------------------------------------------------------------

#define TIMER0_CLOCK_DESABILITA()		clr_bit(TCCR0B,CS02);clr_bit(TCCR0B,CS01);clr_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_SEM_PRESCALER()	clr_bit(TCCR0B,CS02);clr_bit(TCCR0B,CS01);set_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_PRESCALER_8()		clr_bit(TCCR0B,CS02);set_bit(TCCR0B,CS01);clr_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_PRESCALER_64()		clr_bit(TCCR0B,CS02);set_bit(TCCR0B,CS01);set_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_PRESCALER_256()	set_bit(TCCR0B,CS02);clr_bit(TCCR0B,CS01);clr_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_PRESCALER_1024()	set_bit(TCCR0B,CS02);clr_bit(TCCR0B,CS01);set_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_T0_BORDA_DESCIDA()	set_bit(TCCR0B,CS02);set_bit(TCCR0B,CS01);clr_bit(TCCR0B,CS00)
#define TIMER0_CLOCK_T0_BORDA_SUBIDA()	set_bit(TCCR0B,CS02);set_bit(TCCR0B,CS01);set_bit(TCCR0B,CS00)
#define TIMER0_ATIVA()					set_bit(TIMSK0,TOIE0)
#define TIMER0_DESATIVA()				clr_bit(TIMSK0,TOIE0)
#define TIMER0_CONFIGURA(valor)			TCNT0 = valor
//#define TIMER1_CLOCK_DESABILITA()		clr_bit(TCCR1B,CS12);clr_bit(TCCR1B,CS11);clr_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_SEM_PRESCALER()	clr_bit(TCCR1B,CS12);clr_bit(TCCR1B,CS11);set_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_PRESCALER_8()		clr_bit(TCCR1B,CS12);set_bit(TCCR1B,CS11);clr_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_PRESCALER_64()		clr_bit(TCCR1B,CS12);set_bit(TCCR1B,CS11);set_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_PRESCALER_256()	set_bit(TCCR1B,CS12);clr_bit(TCCR1B,CS11);clr_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_PRESCALER_1024()	set_bit(TCCR1B,CS12);clr_bit(TCCR1B,CS11);set_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_T1_BORDA_DESCIDA()	set_bit(TCCR1B,CS12);set_bit(TCCR1B,CS11);clr_bit(TCCR1B,CS10)
//#define TIMER1_CLOCK_T1_BORDA_SUBIDA()	set_bit(TCCR1B,CS12);set_bit(TCCR1B,CS11);set_bit(TCCR1B,CS10)
//#define TIMER1_ATIVA()					set_bit(TIMSK1,TOIE1)
//#define TIMER1_DESATIVA()				clr_bit(TIMSK1,TOIE1)
//#define TIMER1_CONFIGURA(valor)			TCNT1 = valor
//#define TIMER2_CLOCK_DESABILITA()		clr_bit(TCCR2B,CS22);clr_bit(TCCR2B,CS21);clr_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_SEM_PRESCALER()	clr_bit(TCCR2B,CS22);clr_bit(TCCR2B,CS21);set_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_8()		clr_bit(TCCR2B,CS22);set_bit(TCCR2B,CS21);clr_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_32()		clr_bit(TCCR2B,CS22);set_bit(TCCR2B,CS21);set_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_64()		set_bit(TCCR2B,CS22);clr_bit(TCCR2B,CS21);clr_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_128()	set_bit(TCCR2B,CS22);clr_bit(TCCR2B,CS21);set_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_256()	set_bit(TCCR2B,CS22);set_bit(TCCR2B,CS21);clr_bit(TCCR2B,CS20)
//#define TIMER2_CLOCK_PRESCALER_1024()	set_bit(TCCR2B,CS22);set_bit(TCCR2B,CS21);set_bit(TCCR2B,CS20)
//#define TIMER2_ATIVA()					set_bit(TIMSK2,TOIE2)
//#define TIMER2_DESATIVA()				clr_bit(TIMSK2,TOIE2)
//#define TIMER2_CONFIGURA(valor)			TCNT2 = valor

// -----------------------------------------------------------------------------
// Serial Peripheral Interface -------------------------------------------------

#define SPI_CLOCK_PRESCALLER_2			4
#define SPI_CLOCK_PRESCALLER_4			0
#define SPI_CLOCK_PRESCALLER_8			5
#define SPI_CLOCK_PRESCALLER_16			1
#define SPI_CLOCK_PRESCALLER_32			6
#define SPI_CLOCK_PRESCALLER_64			2
//	#define SPI_CLOCK_PRESCALLER_64			7		// Seems to be wrong
#define SPI_CLOCK_PRESCALLER_128		3
#define SPI_ENABLE()					set_bit(SPCR,SPE)
#define SPI_DISABLE()					clr_bit(SPCR,SPE)
#define SPI_INTERRUPT_ENABLE()			set_bit(SPCR,SPIE)
#define SPI_INTERRUPT_DISABLE()			clr_bit(SPCR,SPIE)
#define SPI_MSB_FIRST()					clr_bit(SPCR,DORD)
#define SPI_LSB_FIRST()					set_bit(SPCR,DORD)
#define SPI_SCK_IDLE_LOW()				clr_bit(SPCR,CPOL)
#define SPI_SCK_IDLE_HIGH()				set_bit(SPCR,CPOL)
#define SPI_SAMPLE_LEADING_EDGE()		clr_bit(SPCR,CPHA)
#define SPI_SAMPLE_TRAILING_EDGE()		set_bit(SPCR,CPHA)

void SPI_master_init(unsigned char clock_prescaller);
void SPI_master_transmit(unsigned char data);
void SPI_slave_init(void);
unsigned char SPI_slave_receive(void);

// -----------------------------------------------------------------------------
// USART -----------------------------------------------------------------------

typedef struct USART_my_control
{
    unsigned char mode								: 2;
    unsigned char polarity							: 1;
    unsigned char frame_error						: 1;
    unsigned char buffer_overflow_error				: 1;
    unsigned char parity_error						: 1;
	unsigned char transmitter_enabled				: 1;
	unsigned char receiver_enabled					: 1;
	unsigned char transmitter_interrupt_enabled		: 1;
	unsigned char receiver_interrupt_enabled		: 1;
	unsigned char buffer_empty_interrupt_enabled	: 1;
	unsigned char data_bits							: 3;
	unsigned char parity							: 2;
	unsigned char stop_bits							: 1;
	unsigned int ubrr								: 16;
}USART_my_control;

USART_my_control USART_control;

#define USART_MODE_ASYNCHRONOUS_NORMAL				0
#define USART_MODE_ASYNCRHONOUS_DOUBLE_SPEED		1
#define USART_MODE_SYNCHRONOUS_NORMAL				2
#define USART_MODE_SYNCHRONOUS_SPI					3
#define USART_TRANSMITTER_RISING_EDGE				0
#define USART_TRANSMITTER_FALLING_EDGE				1
#define USART_PARITY_DISABLED						0
#define USART_PARITY_EVEN							2
#define USART_PARITY_ODD							3
#define USART_SINGLE_STOP_BIT						0
#define USART_DOUBLE_STOP_BIT						1
#define USART_5_DATA_BITS							0
#define USART_6_DATA_BITS							1
#define USART_7_DATA_BITS							2
#define USART_8_DATA_BITS							3
#define USART_9_DATA_BITS							4
#define USART_STDIO()											stdin = stdout = stderr = &USART_stream
#define USART_RECEPTION_COMPLETE()								tst_bit(UCSR0A,RXC0)
#define USART_TRANSMISSION_COMPLETE()							tst_bit(UCSR0A,TXC0)
#define USART_TRANSMITTER_BUFFER_EMPTY()						tst_bit(UCSR0A,UDRE0)
#define USART_RECEPTION_COMPLETE_INTERRUPT_ENABLE()				set_bit(UCSR0B,RXCIE0);USART_control.receiver_interrupt_enabled = 1
#define USART_RECEPTION_COMPLETE_INTERRUPT_DISABLE()			clr_bit(UCSR0B,RXCIE0);USART_control.receiver_interrupt_enabled = 0
#define USART_TRANSMISSION_COMPLETE_INTERRUPT_ENABLE()			set_bit(UCSR0B,TXCIE0);USART_control.transmitter_interrupt_enabled = 1
#define USART_TRANSMISSION_COMPLETE_INTERRUPT_DISABLE()			clr_bit(UCSR0B,TXCIE0);USART_control.transmitter_interrupt_enabled = 0
#define USART_BUFFER_EMPTY_INTERRUPT_ENABLE()					set_bit(UCSR0B,UDRIE0);USART_control.buffer_empty_interrupt_enabled = 1
#define USART_BUFFER_EMPTY_INTERRUPT_DISABLE()					clr_bit(UCSR0B,UDRIE0);USART_control.buffer_empty_interrupt_enabled = 0
#define USART_RECEIVER_ENABLE()									set_bit(UCSR0B,RXEN0);USART_control.receiver_enabled = 1
#define USART_RECEIVER_DISABLE()								clr_bit(UCSR0B,RXEN0);USART_control.receiver_enabled = 0
#define USART_TRANSMITTER_ENABLE()								set_bit(UCSR0B,TXEN0);USART_control.transmitter_enabled = 1
#define USART_TRANSMITTER_DISABLE()								clr_bit(UCSR0B,TXEN0);USART_control.transmitter_enabled = 0
/// UCSR0A	MPCM0

void USART_init(void);
unsigned char USART_check_error(void);
void USART_transmit(char data);
void USART_transmit_9bits(unsigned int data);
unsigned char USART_receive(void);
unsigned int USART_receive_9bits(void);
void USART_clear_buffer_receptor(void);
int USART_transmit_std(char data,FILE * stream);
int USART_receive_std(FILE * stream);

// I2C
// COMPARARDOR ANALOGICO
// ADC

#define ADC_REFERENCIA_AREF()				clr_bit(ADMUX,REFS1);clr_bit(ADMUX,REFS0)
#define ADC_REFERENCIA_AVCC()				clr_bit(ADMUX,REFS1);set_bit(ADMUX,REFS0)
#define ADC_REFERENCIA_INTERNA()			set_bit(ADMUX,REFS1);set_bit(ADMUX,REFS0)
#define ADC_AJUSTA_RESULTADO_ESQUERDA()		set_bit(ADMUX,ADLAR)
#define ADC_AJUSTA_RESULTADO_DIREITA()		clr_bit(ADMUX,ADLAR)
#define ADC_SELECIONA_CANAL_0()				clr_bit(ADMUX,MUX3);clr_bit(ADMUX,MUX2);clr_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_1()				clr_bit(ADMUX,MUX3);clr_bit(ADMUX,MUX2);clr_bit(ADMUX,MUX1);set_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_2()				clr_bit(ADMUX,MUX3);clr_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_3()				clr_bit(ADMUX,MUX3);clr_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);set_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_4()				clr_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);clr_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_5()				clr_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);clr_bit(ADMUX,MUX1);set_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_6()				clr_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_7()				clr_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);set_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_TEMPERATURA()	set_bit(ADMUX,MUX3);clr_bit(ADMUX,MUX2);clr_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_REFERENCIA()	set_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);clr_bit(ADMUX,MUX0)
#define ADC_SELECIONA_CANAL_GND()			set_bit(ADMUX,MUX3);set_bit(ADMUX,MUX2);set_bit(ADMUX,MUX1);set_bit(ADMUX,MUX0)
#define ADC_HABILITA()						set_bit(ADCSRA,ADEN)
#define ADC_DESABILITA()					clr_bit(ADCSRA,ADEN)
#define ADC_INICIA()						set_bit(ADCSRA,ADSC)
#define ADC_ATIVA()							set_bit(ADCSRA,ADIE)
#define ADC_DESATIVA()						clr_bit(ADCSRA,ADIE)
#define ADC_HABILITA_MODO_AUTOMATICO()		set_bit(ADCSRA,ADATE)
#define ADC_DESABILITA_MODO_AUTOMATICO()	clr_bit(ADCSRA,ADATE)
#define ADC_TRIGGER_CONTINUO()				clr_bit(ADCSRB,ADTS2);clr_bit(ADCSRB,ADTS1);clr_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_COMPARADOR()			clr_bit(ADCSRB,ADTS2);clr_bit(ADCSRB,ADTS1);set_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_INT_0()					clr_bit(ADCSRB,ADTS2);set_bit(ADCSRB,ADTS1);clr_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_TIMER_0_COMPARADOR()	clr_bit(ADCSRB,ADTS2);set_bit(ADCSRB,ADTS1);set_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_TIMER_0_ESTOURO()		set_bit(ADCSRB,ADTS2);clr_bit(ADCSRB,ADTS1);clr_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_TIMER_1_COMPARADOR()	set_bit(ADCSRB,ADTS2);clr_bit(ADCSRB,ADTS1);set_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_TIMER_1_ESTOURO()		set_bit(ADCSRB,ADTS2);set_bit(ADCSRB,ADTS1);clr_bit(ADCSRB,ADTS0)
#define ADC_TRIGGER_TIMER_1_EVENTO()		set_bit(ADCSRB,ADTS2);set_bit(ADCSRB,ADTS1);set_bit(ADCSRB,ADTS0)
#define ADC_CLOCK_SEM_PRESCALER()			clr_bit(ADCSRA,ADPS2);clr_bit(ADCSRA,ADPS1);clr_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_2()				clr_bit(ADCSRA,ADPS2);clr_bit(ADCSRA,ADPS1);set_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_4()				clr_bit(ADCSRA,ADPS2);set_bit(ADCSRA,ADPS1);clr_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_8()				clr_bit(ADCSRA,ADPS2);set_bit(ADCSRA,ADPS1);set_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_16()			set_bit(ADCSRA,ADPS2);clr_bit(ADCSRA,ADPS1);clr_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_32()			set_bit(ADCSRA,ADPS2);clr_bit(ADCSRA,ADPS1);set_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_64()			set_bit(ADCSRA,ADPS2);set_bit(ADCSRA,ADPS1);clr_bit(ADCSRA,ADPS0)
#define ADC_CLOCK_PRESCALER_128()			set_bit(ADCSRA,ADPS2);set_bit(ADCSRA,ADPS1);set_bit(ADCSRA,ADPS0)
#define ADC_DESABILITA_ENTRADA_DIGITAL_0()	set_bit(DIDR0,ADC0D)
#define ADC_HABILITA_ENTRADA_DIGITAL_0()	clr_bit(DIDR0,ADC0D)
#define ADC_DESABILITA_ENTRADA_DIGITAL_1()	set_bit(DIDR0,ADC1D)
#define ADC_HABILITA_ENTRADA_DIGITAL_1()	clr_bit(DIDR0,ADC1D)
#define ADC_DESABILITA_ENTRADA_DIGITAL_2()	set_bit(DIDR0,ADC2D)
#define ADC_HABILITA_ENTRADA_DIGITAL_2()	clr_bit(DIDR0,ADC2D)
#define ADC_DESABILITA_ENTRADA_DIGITAL_3()	set_bit(DIDR0,ADC3D)
#define ADC_HABILITA_ENTRADA_DIGITAL_3()	clr_bit(DIDR0,ADC3D)
#define ADC_DESABILITA_ENTRADA_DIGITAL_4()	set_bit(DIDR0,ADC4D)
#define ADC_HABILITA_ENTRADA_DIGITAL_4()	clr_bit(DIDR0,ADC4D)
#define ADC_DESABILITA_ENTRADA_DIGITAL_5()	set_bit(DIDR0,ADC5D)
#define ADC_HABILITA_ENTRADA_DIGITAL_5()	clr_bit(DIDR0,ADC5D)

#endif
