// --------------------------------------------------------------------------------------------
// File:			modulo_portatil.c
// Module:			M�dulo remoto do NOME DO PROJETO
// Author:			Leandro Schwarz
// Version:			4.0
// Last edition:	04/05/2012
// --------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------
// System definitions -------------------------------------------------------------------------

#define HANDLER_USART_8BITS
//#define DEBUG
#define DEBUG_BASIC

#define BUFFER_SIZE 50

#define ZB_RESET		PB0
#define ZB_SLEEP		PB1
#define ZB_COMISS		PD5
#define ZB_ASSOC		PD6
#define ZB_SLEEP_REQ	PD7

#define ACCEL_SEL1		PD3
#define ACCEL_SEL2		PD4
#define ACCEL_X			PC0
#define ACCEL_Y			PC1
#define ACCEL_Z			PC2
#define ACCEL_SLEEP		PC3

#define ADC_BAT			PC4

#define FREE_P1			PD2// Interrupt INT0
#define FREE_P2			PC5
#define FREE_P3			PB6
#define FREE_P4			PB7

enum STATES
{
	IDLE			= 0,
	PREPARE_FILE_NAME,
	FILE_CREATE,
	START_ACQUISITION,
	STORING_DATA,
	STOP_ACQUISITION
};

// --------------------------------------------------------------------------------------------
// Include files ------------------------------------------------------------------------------

#include "global.h"
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>				// BIBLIOTECA DE DEFINI�OES DO COMPONENTE ESPECIFICADO("ATMEGA328")
#include <avr/interrupt.h>		// Macros para o tratamento de interrup��es
#include "ATmega328.h"			// Macro-fun��es de configura��o para o ATmega328
#include "defines.h"			// Defini��es para o projeto
#include "acelerometro.h"		// Macro-fun��es de configura��o de pinos para o Acelerometro
#include "ff.h"

// --------------------------------------------------------------------------------------------
// Function declarations ----------------------------------------------------------------------

void die (FRESULT rc);
ISR(ADC_vect);
ISR(TIMER0_OVF_vect);
ISR(INT0_vect);

// --------------------------------------------------------------------------------------------
// Global variables ---------------------------------------------------------------------------

volatile unsigned char estado;
volatile unsigned char canal;
volatile unsigned int buffer_x[2][BUFFER_SIZE];
volatile unsigned int buffer_y[2][BUFFER_SIZE];
volatile unsigned int buffer_z[2][BUFFER_SIZE];
volatile unsigned int buffer_t[2][BUFFER_SIZE];
volatile unsigned char coluna_adc;
volatile unsigned int tempo;
volatile unsigned char linha_adc;
volatile unsigned char linha_cartao;
volatile unsigned char adquirindo;

// --------------------------------------------------------------------------------------------
// Function main ------------------------------------------------------------------------------

int main(void)
{
	// Local variable declaration
	unsigned char aux;
	unsigned int file_number;
	char temp_string[9];
	char file_name[13];
	char valor_linha[25] = "";

	FRESULT rc;				// Result code
	FATFS fatfs;			// File system object
	FIL fil;				// File object
	DIR dir;				// Directory object
	FILINFO fno;			// File information object
	UINT bw;

	// Global variable definition
	canal = 0;
	linha_adc = 0;
	coluna_adc = 0;
	linha_cartao = 0;
	adquirindo = 0;
	tempo = 0;

	// I/O configuration
	DDRB = 0x00;
	DDRB = (1<<PB2) | (1<<PB3) | (1<<PB5);
	set_bit(DDRB,ZB_RESET);//Sa�da
	set_bit(DDRB,ZB_SLEEP);//Sa�da
	set_bit(DDRB,FREE_P3);//Sa�da(LED)
	set_bit(DDRB,FREE_P4);//Sa�da(LED)
	DDRC = 0x00;
	set_bit(DDRC,FREE_P2);//Sa�da(LED)
	set_bit(DDRC,ACCEL_SLEEP);//Sa�da

	DDRD = 0x00;
	set_bit(DDRD,ACCEL_SEL1);
	set_bit(DDRD,ACCEL_SEL2);
	set_bit(DDRD,ZB_COMISS);
	set_bit(DDRD,ZB_ASSOC);
	set_bit(DDRD,ZB_SLEEP_REQ);
	set_bit(PORTC,ACCEL_SLEEP);//desativa_modo_sleep
	set_bit(PORTD,FREE_P1);//BOTAO INT0
	set_bit(PORTC,PC6);//Pull up Reset

	// USART configuration
	USART_control.mode		= USART_MODE_ASYNCHRONOUS_NORMAL;
	USART_control.data_bits	= USART_8_DATA_BITS;
	USART_control.parity	= USART_PARITY_DISABLED;
	USART_control.stop_bits	= USART_SINGLE_STOP_BIT;
	USART_init();
	USART_TRANSMITTER_ENABLE();
	USART_RECEIVER_ENABLE();
	USART_RECEPTION_COMPLETE_INTERRUPT_ENABLE();
	USART_STDIO();

	// ADC configuration
	ADC_REFERENCIA_AVCC();
	ADC_AJUSTA_RESULTADO_DIREITA();
	ADC_DESABILITA_ENTRADA_DIGITAL_0();
	ADC_DESABILITA_ENTRADA_DIGITAL_1();
	ADC_DESABILITA_ENTRADA_DIGITAL_2();
	ADC_TRIGGER_CONTINUO();
	ADC_DESABILITA_MODO_AUTOMATICO();
	ADC_CLOCK_PRESCALER_16();
	ADC_HABILITA();
	ADC_ATIVA();
	ADC_SELECIONA_CANAL_0();

	// Timer configuration
	TIMER0_CLOCK_PRESCALER_1024();
	TIMER0_CONFIGURA(177);

	// Accelerometer configuration
	ACCEL_rang_1_5g();	//Escolhe rang de 1.5g
//	ACCEL_desativa_modo_sleep();

	// External Interrupt 0 configuration
	EIFR = 3;
	INT0_SENSE_RISING_EDGE();

	// Enable interrupts
	sei();

	// Mount SD card
	f_mount(0, &fatfs);

	// Search in the directory for the last number named file
	file_number = 1;
	aux = 0;
	printf("\r\nOpen root directory.\r\n");
	printf("\r\nDirectory listing...\r\n");
	while(aux == 0)
	{
		// Open the root directory
		
		rc = f_opendir(&dir, "");
		if(rc) die(rc);
		while(1)
		{
			// Search for the number
			// Read a directory item 
			rc = f_readdir(&dir, &fno);
			if(rc || !fno.fname[0])
			{
				aux = 1;
				break;	// Error or end of dir
			}
			if(!(fno.fattrib & AM_DIR))
			{
				if(atoi(fno.fname) == file_number)
				{
					file_number++;
					break;
				}
			}
		 }
	}

	// Main loop
	estado = PREPARE_FILE_NAME;
	while(1)
	{
		// States machine
		switch(estado)
		{
			case IDLE:	
				break;

			case PREPARE_FILE_NAME:
				// File name
				itoa(file_number,temp_string,10);
				file_name[0] = '\0';
				strcat(file_name,temp_string);
				strcat(file_name,".txt");
				printf("\r\nNew file name: %s.\r\n",file_name);
				printf("\r\nAwaiting button\r\n");
				canal=0;
				linha_adc=0;
				coluna_adc=0;
				linha_cartao=0;
				adquirindo=0;
				EIFR = 3;	// Clears external interrrupt flags
				INT0_ACTIVATE();
				estado = IDLE;
				break;

			case FILE_CREATE:
				_delay_ms(500);          		 // Delay
				// Opens the file
				rc = f_open(&fil,file_name, FA_WRITE | FA_CREATE_ALWAYS);
				// Writes the header
				rc = f_write(&fil,"t;\tx;\ty;\tz\r\n",12, &bw);
			
				if(rc) die(rc);
				estado = START_ACQUISITION;
				break;

			case START_ACQUISITION:
				TIMER0_ATIVA();
				estado = IDLE;
				EIFR = 3;		// Clears external interrrupt flags	
				INT0_ACTIVATE();
				break;

			case STORING_DATA:
				linha_cartao = (linha_adc == 1)?0:1;
				for(aux = 0;aux < BUFFER_SIZE;aux++)
				{
					sprintf(valor_linha, "%u;%d;%d;%d\r\n", buffer_t[linha_cartao][aux],
							   								   buffer_x[linha_cartao][aux],
															   buffer_y[linha_cartao][aux],
															   buffer_z[linha_cartao][aux]);
					
					rc = f_write(&fil, valor_linha, strlen(valor_linha), &bw);
					if(rc) die(rc);
				
				}
				if(estado == STORING_DATA)
					estado = IDLE;
				break;
			case STOP_ACQUISITION: //Fecha arquivo
				// Stores the rest of the data
				for(aux = 0;aux < coluna_adc;aux++)
				{		
					sprintf(valor_linha, "%u;%d;%d;%d\r\n", buffer_t[linha_adc][aux],
							   								   buffer_x[linha_adc][aux],
															   buffer_y[linha_adc][aux],
															   buffer_z[linha_adc][aux]);
					
					rc = f_write(&fil, valor_linha, strlen(valor_linha), &bw);
					if(rc) die(rc);

					
				}
				// Closes the file
				printf("\r\nClose the file.\r\n");
				rc = f_close(&fil);
				if(rc) die(rc);
				file_number++;
				tempo=0;
				estado = PREPARE_FILE_NAME;
				break;
		}
	}
	return 0;		
}



//---------------------------------------------------------------------------------------------
// User Provided Timer Function for FatFs module           
//---------------------------------------------------------------------------------------------

DWORD get_fattime (void)
{
	return	  ((DWORD)(2010 - 1980) << 25)	// Fixed to Jan. 1, 2010 
			| ((DWORD)1 << 21)
			| ((DWORD)1 << 16)
			| ((DWORD)0 << 11)
			| ((DWORD)0 << 5)
			| ((DWORD)0 >> 1);
}

//---------------------------------------------------------------------------------------------
// Tratamento da interrup��o INT0 -------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(INT0_vect)
{
	EIFR = 3;//Limpa Flag da interrup��o
	INT0_DEACTIVATE();
	if(adquirindo == 0)
	{
		#ifdef DEBUG_BASIC
			printf("Acquiring data...");
		#endif
		estado = FILE_CREATE;
		adquirindo = 1;
		set_bit(PORTC,FREE_P2);
	}
	else
	{
		#ifdef DEBUG_BASIC
			printf("\r\nShutting down...\r\n");
		#endif
		TIMER0_DESATIVA();
		estado = STOP_ACQUISITION;
		adquirindo = 0;
		clr_bit(PORTC,FREE_P2);
	}
	return;
}

//---------------------------------------------------------------------------------------------
// Tratamento da interrup��o do ADC -----------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(ADC_vect)
{
	switch(canal)
	{
		case 0:
			buffer_t[linha_adc][coluna_adc] = tempo;
			tempo++;
			buffer_x[linha_adc][coluna_adc] = ADC;			 //Pega o valor do reg ADC e coloca em valor_x.
			ADC_SELECIONA_CANAL_1(); //Seleciona o canal da pr�xima convers�o.
			ADC_INICIA();          
			canal++;
			break;					
		case 1:
			buffer_y[linha_adc][coluna_adc] = ADC;
			ADC_SELECIONA_CANAL_2();
			ADC_INICIA();
			canal++;
			break;
		case 2:
			buffer_z[linha_adc][coluna_adc] = ADC;
			ADC_SELECIONA_CANAL_0();
			canal = 0;
			if(coluna_adc == (BUFFER_SIZE-1))
			{
				coluna_adc = 0;
				linha_adc = (linha_adc == 1)?0:1;
				estado = STORING_DATA;
			}
			else
				coluna_adc++;
			break;
	}
	return;
}
//---------------------------------------------------------------------------------------------
// Tratamento da interrup��o Timer0 -----------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(TIMER0_OVF_vect)
{
	TIMER0_CONFIGURA(177);	// Timer configured to 10ms
	TIMER0_ATIVA();
	ADC_INICIA();	// Activates ADC
	return;
}

//---------------------------------------------------------------------------------------------

void die (FRESULT rc)
{
	printf("Failed with rc=%u.\n", rc);
	while(1);
}

