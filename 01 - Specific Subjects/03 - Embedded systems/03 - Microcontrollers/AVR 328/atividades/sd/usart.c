#include "usart.h"

//Inicialização da USART
void USART_Init(void)
{
	UBRR0H = (unsigned char)(UART_UBRR>>8); //Configura taxa de transmissão
	UBRR0L = (unsigned char)UART_UBRR;

	UCSR0C = (0<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00); //Configura para 8N1

	UCSR0B = (1<<RXEN0)|(1<<TXEN0); //Habilita USART
}

//Envia um byte pela USART
void USART_SendByte(char data)
{
	//aguarda fim da transmissão anterior
	while(! (UCSR0A & (1<<UDRE0)));
	//transmite
	UDR0 = data;
}

//Recebe um byte pela USART
char USART_ReceiveByte()
{
	//aguarda fim da recepção
	while((UCSR0A & (1<<RXC0)) == 0);
	//retorna dado recebido
	return UDR0;
}

//Funções utilizadas em conjunto com a stdio
int USART_PutChar(char c, FILE *stream)
{
	if(c == '\n')
	{
		USART_SendByte('\r');
	}
	USART_SendByte(c);
	return 0;
}

int USART_GetChar(FILE *stream)
{
	return USART_ReceiveByte();
}
