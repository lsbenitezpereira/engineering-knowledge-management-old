// -----------------------------------------------------------------------------
// Arquivo:			ACELEROMETRO.h
// Modulo:			Aceler�metro
// Autores:			Mayara de Sousa
//					Leandro Schwarz
// Vers�o:			1.0
// Modificado em:	07/09/2011
// Observa��es:		Para utilizar, � necess�rio alterar os valor das constantes
//					LCD_DADOS, LCD_CONTROLE, LCD_RS, LCD_E e LCD_D4, definidas 
//					na seq��ncia
// -------------------------------------------------------------------------

#ifndef __ACELEROMETRO_H
#define __ACELEROMETRO_H

// -------------------------------------------------------------------------
// Defini��es configur�veis ------------------------------------------------

#define ACCEL_SEL1			PD3
#define ACCEL_SEL2			PD4
#define ACCEL_SLEEP			PC3

#define ACCEL_SLEEP_CONTROL	PORTC
#define ACCEL_CONTROL		PORTD
//Eixo x est� em ADC1
//Eixo y est� em ADC2
//Eixo z est� em ADC0
// -------------------------------------------------------------------------
// Defini��es das fun��es macro necess�rias --------------------------------

#define __acel_set_bit(endereco,bit) (endereco|=(1 << bit))
#define __acel_clr_bit(endereco,bit) (endereco&=~(1 << bit))
#define __acel_tst_bit(endereco,bit) ((endereco>>bit)&1)
#define __acel_cpl_bit(endereco,bit) (endereco^=(1 << bit))
#define pulso_enable 	_delay_us(1); __lcd_set_bit(LCD_CONTROLE,LCD_E); _delay_us(1); __lcd_clr_bit(LCD_CONTROLE,LCD_E); _delay_us(45)

// -------------------------------------------------------------------------
// Defini��es das fun��es macro de controle --------------------------------

#define ACCEL_desativa_modo_sleep()		__acel_set_bit(ACCEL_SLEEP_CONTROL,ACCEL_SLEEP)
#define ACCEL_rang_1_5g()				__acel_clr_bit(ACCEL_CONTROL,ACCEL_SEL1);__acel_clr_bit(ACCEL_CONTROL,ACCEL_SEL2)
#define ACCEL_rang_2g()					__acel_set_bit(ACCEL_CONTROL,ACCEL_SEL1);__acel_clr_bit(ACCEL_CONTROL,ACCEL_SEL2)
#define ACCEL_rang_4g()					__acel_clr_bit(ACCEL_CONTROL,ACCEL_SEL1);__acel_set_bit(ACCEL_CONTROL,ACCEL_SEL2)
#define ACCEL_rang_6g()					__acel_set_bit(ACCEL_CONTROL,ACCEL_SEL1);__acel_set_bit(ACCEL_CONTROL,ACCEL_SEL2)


// -------------------------------------------------------------------------
// Declara��o das fun��es da bibioteca -------------------------------------


#endif
