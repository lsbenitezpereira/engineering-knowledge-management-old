/* -----------------------------------------------------------------------------
 * File:			com_sinc_transm.c
 * Project:			
 * Author:			Leandro Schwarz
 * Last modified:	25/09/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU	16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"

// Project definitions
#define STATE_IDLE			0
#define STATE_TRANSMISSION	1

#define DATA_DDR	DDRD
#define DATA_PORT	PORTD
#define DATA_PIN	PD2

// Function declaration

// Global variables
volatile uint16 rawADC = 0;
volatile uint8 state = 0;
volatile uint8 transmissionState = 0;

// main function
int main()
{
	setBit(DATA_DDR, DATA_PIN);
	setBit(DATA_PORT, DATA_PIN);

	// TIMER1 configuration
	timer1CTCMode();
	timer1ClockPrescaller1024();
	timer1ClearCompareBInterruptRequest();
	timer1SetCompareBValue(1561);
	timer1SetCompareAValue(1561);

	// ADC configuration
	adcReferenceAvcc();
	adcClockPrescaler128();
	adcEnableAutomaticMode();
	adcTriggerTimer1CompareMatchB();
	adcSelectChannel(ADC0);
	adcDisableDigitalInput0();
	adcClearInterruptRequest();
	adcActivateInterrupt();
	adcEnable();

	// TIMER2 configuration
	/*timer2ClockPrescaller8();
	timer2CTCMode();
	timer2ClearCompareAInterruptRequest();
	timer2SetCompareAValue(255);
*/
	sei();

	while(1){
		switch(state){
		case STATE_IDLE:
			break;
		
		/*case STATE_TRANSMISSION:
			if((transmissionState % 2) == 0){
				if(isBitSet(rawADC, (transmissionState / 2)))
					setBit(DATA_PORT, DATA_PIN);
				else
					clrBit(DATA_PORT, DATA_PIN);
				clrBit(DATA_PORT, COM_CLOCK);
			}else{
				setBit(DATA_PORT, COM_CLOCK);
			}
			transmissionState++;
			if(transmissionState == 20){
				timer2DeactivateCompareAInterrupt();
			}
			state = STATE_IDLE;
			break;*/
		}
	}
	return 0;
}

ISR(ADC_vect)
{
	timer1ClearCompareBInterruptRequest();
	rawADC = ADC;
	timer2SetCounterValue(0);
	timer2ClearCompareAInterruptRequest();
	timer2ActivateCompareAInterrupt();
	transmissionState = 0;
	//setBit(DATA_PORT, DATA_PIN);
	//_delay_ms(1);
	clrBit(DATA_PORT, DATA_PIN);
}

ISR(TIMER2_COMPA_vect)
{
	state = STATE_TRANSMISSION;
}
