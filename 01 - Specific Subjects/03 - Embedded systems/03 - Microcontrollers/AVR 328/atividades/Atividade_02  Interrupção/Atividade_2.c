/* -----------------------------------------------------------------------------
 * File:			externalInterrupt1.c
 * Project:			External interrupt example
 * Author:			Hazael dos Santos Batista
 * Last modified:	11/09/2014
 * -------------------------------------------------------------------------- */

// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS

// Header files
#include "LS_defines.h"
#include "LS_ATmega328.h"
#include "LS_HD44780.h"

// Project definitions
#define NORMAL_COUNT	0
#define CHANGE_CONFIG	1
#define DEC_HOURS		2
#define UNI_HOURS		3
#define DEC_MINUTES		4
#define UNI_MINUTES		5
#define DEC_SECONDS		6
#define UNI_SECONDS		7

#define BUTTON_PIN		PIND
#define BUTTON_BIT		PD2

volatile uint8 seconds		= 56;
volatile uint8 minutes 		= 34;
volatile uint8 hours		= 12;

volatile uint8 count_10m	= 0;

volatile uint8 state = 0;
volatile uint8 update = 1;

// Main function
int main()
{
	
	
	//LCD configuration
	lcdInit();
	lcdStdio();
	printf("  Atividade 2  \n  Leonardo S.  ");
	_delay_ms(500);
	lcdClearScreen();
	
	int0PullUpEnable();
	int0DDRConfigure();
	int0SenseAnyEdge();
	int0ClearInterruptRequest();
	int0ActivateInterrupt();
	
	//Timer 0 configuration
	timer0CTCMode();
	timer0ClockPrescaller1024();
	timer0SetCompareAValue(155);
	timer0ClearCompareAInterruptRequest();
	timer0ActivateCompareAInterrupt();
	
	//Timer 1 configuration
	timer1ClockDisable();
	timer1CTCMode();
	timer1SetCompareAValue(31249);
	//timer1SetCompareAValue(15000);

	//Global interrupts configuration
	sei();
	
	while(1){
		if(update == 1){
			if (seconds==60){
				minutes++;
				seconds=0;
			}

				if(minutes==60){
				hours++;
				minutes=0;
			}

			if(hours==24)
				hours = 0;

			switch(state){
			case DEC_HOURS:
				printf(" Fit Dec. Hours \n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case UNI_HOURS:
				printf(" Fit Unit Hours \n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case DEC_MINUTES:
				printf("Fit Dec. minutes\n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case UNI_MINUTES:
				printf("Fit Unit minutes\n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case  DEC_SECONDS:
				printf("Fit Dec. Seconds\n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case  UNI_SECONDS:
				printf("Fit Unit Seconds\n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
				break;
			case NORMAL_COUNT:
				printf("  Digital Clock \n    %u%u:%u%u:%u%u    \n", hours / 10, hours % 10, minutes / 10, minutes % 10, seconds / 10, seconds % 10);
			}
			update = 0;
		}	//if(update)
	}	//while(1)
	
	return 0;
}	//main

ISR(TIMER0_COMPA_vect)
{
	count_10m++;
	if(count_10m == 100){
		seconds++;
		update = 1;
		count_10m = 0;
	}

	return;
}

ISR(INT0_vect)
{
	_delay_ms(10);
	if(isBitClr(BUTTON_PIN, BUTTON_BIT)) {				//PRESSED
		timer1SetCounterValue(0);
		timer1ClearCompareAInterruptRequest();
		timer1ClockPrescaller1024();
	}
	
	else { 												//RELEASED    
		timer1ClockDisable();
		if(isBitSet(TIFR1, OCF1A)){						//long Pulse
			state++;
			switch(state){
			case NORMAL_COUNT:
				count_10m = 0;
				timer0SetCounterValue(0);
				timer0ClockPrescaller1024();
				break;

			case CHANGE_CONFIG:
				timer0ClockDisable();
				state++;
				break;

			case 8:
				count_10m = 0;
				timer0SetCounterValue(0);
				timer0ClockPrescaller1024();
				state = NORMAL_COUNT;

			
			}
			
		} 
		else {										//short pulse
			switch(state){
			case DEC_HOURS:
				hours+=10;
					if(hours > 23)
						hours-=30;
				break;
			case UNI_HOURS:
				hours++;
				if((hours) % 10 == 0)
					hours -= 10;
				else if (hours == 24)
					hours = 20;
				break;

			case DEC_MINUTES:
				minutes+=10;
				if(minutes > 59)
					minutes-=60;
				break;

			case UNI_MINUTES:
				minutes++;
				if((minutes) % 10 == 0)
					minutes-=10;
				break;

			case  DEC_SECONDS:
				seconds+=10;
				if(seconds > 59)
					seconds-=60;
				break;

			case  UNI_SECONDS:
				seconds++;
				if((seconds) % 10 == 0)
					seconds-=10;
				break;

			}	//switch

		}	//else (short)
	
	update = 1;

	}	//else (released)

}	//ISR





//PARTIU MODIFIQUEIRAS+++++++++++++++++++++++++++++++++++++++++
