/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Arquivo: exercicio03
 * Projeto: display de sete segmentos
 * Autor: Leonardo S. e Hazael 
 * Data de edi��o: 14/08/14
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// Defini��es do sistema
#define F_CPU 16000000

//Inclu��o de cabe�alhos
#include <AVR/io.h>
#include <util/delay.h>

//defini��o de macrofun��es
#define setBit(byte, bit) byte = byte | (1 << bit)
#define clrBit(byte, bit) byte = byte & (~(1 << bit))
#define cplBit(byte, bit) byte = byte ^ (1 << bit)
#define isBitSet(byte, bit) ((byte >> bit) & 1)
#define isBitClr(byte, bit) (!((byte >> bit) & 1))

//defini��es do projeto
#define DISPLAY_DDR		DDRD
#define DISPLAY_PORT	PORTD
#define SWITCH_DDR		DDRD
#define SWITCH_PORT		PORTD
#define SWITCH_PIN		PIND
#define SWITCH_BIT		PD7

#define COMMON_ANODE	0
#define COMMON_CATHODE	1

//#define TIME 500

// Declara��o de fun��es
unsigned char display_7_seg(unsigned char hex, unsigned char common);

//main
int main()	
{
	// Vari�veis locais
	unsigned char numero = 0;
	//configura��o dos PORTS
	DISPLAY_DDR = 0x7F;
	clrBit(SWITCH_DDR, SWITCH_BIT);
	setBit(SWITCH_PORT, SWITCH_BIT);	

	while(1){
		DISPLAY_PORT = (DISPLAY_PORT & 0x80) | display_7_seg(numero, isBitSet(SWITCH_PORT, SWITCH_BIT));
		_delay_ms(500);
		numero = (numero == 0x0F)?0x00:(numero +1);
	}

	return 0;
}



unsigned char display_7_seg (unsigned char hex, unsigned char common)
{
	unsigned char segmentos;
	
	switch (hex){
		case 0x00:	segmentos = 0b00111111; break;
	    case 0x01:	segmentos = 0b00000110; break;
	    case 0x02:	segmentos = 0b01011011; break;
	    case 0x03:	segmentos = 0b01001111; break;
	    case 0x04:	segmentos = 0b01100110; break;
	    case 0x05:	segmentos = 0b01101101; break;
	    case 0x06:	segmentos = 0b01111101; break;
	    case 0x07:	segmentos = 0b00000111; break;
	    case 0x08:	segmentos = 0b01111111; break;
	    case 0x09:	segmentos = 0b01101111; break;
	    case 0x0A:	segmentos = 0b01110111; break;
	    case 0x0B:	segmentos = 0b01111100; break;
	    case 0x0C:	segmentos = 0b00111001; break;
	    case 0x0D:	segmentos = 0b01011110; break;
	    case 0x0E:	segmentos = 0b01111001; break;
	    case 0x0F:	segmentos = 0b01110001; break;
		default:	segmentos = 0b00000000; break;
	}
	
	if(common == COMMON_ANODE)
		segmentos = ~segmentos;

	return segmentos;
}
