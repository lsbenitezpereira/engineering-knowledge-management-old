#ifndef GLOBAL_H
#define GLOBAL_H

#define F_CPU 16000000UL

#define UART_BAUD 9600

#define UART_UBRR (((F_CPU/16)/UART_BAUD)-1)

#endif
