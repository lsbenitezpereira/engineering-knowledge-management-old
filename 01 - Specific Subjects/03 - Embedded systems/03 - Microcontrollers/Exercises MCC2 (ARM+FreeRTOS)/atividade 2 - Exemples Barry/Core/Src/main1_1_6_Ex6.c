#include "init_from_hal.h"
#include "atraso.h"

//--------------------------------------------------------------------------------

const char *pcTextForTask1 = "Continuous task 1 is running.\n";
const char *pcTextForTask2 = "Continuous task 2 is running.\n";


static void vContinuousProcessingTask(void *pvParameters)
{
	while(1)
	{
		printf((char *) pvParameters);
		atraso_ms(50);
	}
}
//--------------------------------------------------------------------------------
static void vPeriodicTask(void *pvParameters)
{
	portTickType xLastWakeTime;
	
	xLastWakeTime = xTaskGetTickCount();
	
	while(1)
	{
		printf("Period task is running.\n");
		vTaskDelayUntil(&xLastWakeTime, (250/portTICK_RATE_MS));
	}
}
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS

	xTaskCreate(vContinuousProcessingTask, "Task 1", 100, (void *) pcTextForTask1, 1, NULL);
	xTaskCreate(vContinuousProcessingTask, "Task 2", 100, (void *) pcTextForTask2, 1, NULL);
	xTaskCreate(vPeriodicTask,"Task 3", 100, NULL, 2, NULL);
	
	vTaskStartScheduler();	

	while (1);
}
//--------------------------------------------------------------------------------
