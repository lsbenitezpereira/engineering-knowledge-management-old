#include "init_from_hal.h"

//--------------------------------------------------------------------------------

xTaskHandle xTask2Handle;

void vTask2(void *pvParameters);

//--------------------------------------------------------------------------------
void vTask1(void *pvParameters)
{
	const portTickType  xDelay100ms = 100/portTICK_RATE_MS;
	
	while(1)
	{
		printf("Task 1 is running.\n");
		xTaskCreate(vTask2, "Tarefa 2", 100, NULL, 2, &xTask2Handle);
		vTaskDelay(xDelay100ms);
	}
}
void vTask2(void *pvParameters)
{
	
	while(1)
	{
		printf("Task 2 is running and about to delete itself.\n");
		vTaskDelete(xTask2Handle);
	}
}
//--------------------------------------------------------------------------------
/* A funcao Idle Hook n�o � uma tarefa, n�o pode conter um la�o infinito,
 * caso contr�rio, a tarefa Idle n�o consegue libera os recursos alocado pelo Kernel (tarefas apagadas).
 * A tarefa Idle fica escondida e chama a fun��o Idle Hook se estiver habilitada,
 * A tarefa Idle � respons�vel por liberar a mem�ria alocada pelo kernel depois de desnecess�ria.
 * A �nica mem�ria que � automaticamente liberada � a que n�o � explicitamente alocada.
 * Caso haja aloca��o explicita de mem�ria para uma tarefa, esse mem�ria deve ser liberada pelo programador
 * depois que a tarefa for excluida. Aloca��o din�mica versus est�tica.
 */
/*void vApplicationIdleHook()
{
	while(1){};//NAO FA�A ISSO!!!!
}*/
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xTaskCreate(vTask1,"Tarefa 1",100, NULL, 1, NULL);
	
	vTaskStartScheduler();	// apos este comando o RTOS passa a executar as tarefas

	while (1);
}
//--------------------------------------------------------------------------------
