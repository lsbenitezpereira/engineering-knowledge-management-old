#include "init_from_hal.h"
#include "atraso.h"
//--------------------------------------------------------------------------------

const char *pcTextForTask1 = "Task 1 is running.\n";
const char *pcTextForTask2 = "Task 2 is running.\n";

unsigned int uiIdleCycleCount=0;

static void vTaskFunction(void *pvParameters)
{
	
	while(1)
	{ 
		printf((char *) pvParameters);
		printf("uiIdleCycleCount = %u\n", uiIdleCycleCount);
		
		vTaskDelay(250/portTICK_RATE_MS);
	}
}
//--------------------------------------------------------------------------------
void vApplicationIdleHook()	//sobrou tempo faz essa funcao, chamada pela Idle Task
{ 
	uiIdleCycleCount++;
	atraso_ms(1);
}
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xTaskCreate(vTaskFunction, "Task 1", 100,(void *) pcTextForTask1, 1, NULL);
	xTaskCreate(vTaskFunction, "Task 2", 100,(void *) pcTextForTask2, 1, NULL);
	
	vTaskStartScheduler();	// apos este comando o RTOS passa a executar as tarefas

	while (1);
}
//--------------------------------------------------------------------------------
