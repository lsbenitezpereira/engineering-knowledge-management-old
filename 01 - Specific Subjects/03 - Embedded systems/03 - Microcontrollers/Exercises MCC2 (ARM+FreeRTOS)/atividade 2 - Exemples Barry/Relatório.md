## 1_4_ex1

São criadas duas tarefas diferentes, cada uma instanciada uma vez, ambas fazendo a mesma coisa: print-atrado-print-atraso, eternamente. Cada uma printa uma string diferetne	

The main() function simply creates the tasks before starting the scheduler. O padrão de execução delas será: 



![image-20210301214816583](Images - Relatório - Capítulo 1/image-20210301214816583.png)

como não estamos usando `vPrintString`, não necessariamente o intervalo de execução de cada tarefa concide com o tempo necessário para printar a string, e portanto a saída do programa pode variar dependendo da implementalção do protocolo de comunicação.



## 1_4_Ex2

Uma unica task, fazendo printa-atraso. Ao invés de uma string fixa, a task irá printar uma variável recebida como parâmetro por meio da `*pvParameters`

Essa task é instanciada duas vezes, rebendo duas strings diferentes como parametro, de forma que o padrão de execução seja igual ao anterior.





## 1_5_Ex3
Mudou-se a prioridade de uma das tasks (quanto mais alto, maior a prioridade). Como essa tarefa está sempre apta a rodar, o scheduler irá sempre selecionar ela para rodar, de forma que outra tarefa **nunca** execute. Ou seja, a tarefa de baixa prioridade é “starvada”, passa fome. 

![image-20210301231625722](Images - Relatório - Capítulo 1/image-20210301231625722.png)




## 1_6_Ex4
Aqui usamos `vTaskDelay` ao invés de uma implementação prorpia do delay, sendo que o numero de tickts  à esperar é calculado dividindo pela frequência do tick, `<tempo em ms>/portTICK_RATE_MS`

Dessa forma a tarefa irá efetivametne **dormir** nesse intervalo, Blocked state até acordar.

haverá idle time entre as execuções, pois as terafas não usam todo o tempo de processador, permitindo que ambas as tasks sejam executadas mesmo com prioridades diferentes (ao contrário do exemplo anterior, onde apenas uma rodou)

![image-20210301232430878](Images - Relatório - Capítulo 1/image-20210301232430878.png)


## 1_6_Ex5

A vTaskDelayUntil() is similar to vTaskDelay(), mas recebe o tempo absoluto em que a tarefa deve acordar (ao invés do tempo em que a tarefa deve ficar dormindo). Dessa forma, conseguimos garantir que uma tarefa periódica tenha uma frequência precisa e fixa. 

## 1_6_Ex6

São instanciadas 3 tarefas:

* duas tarefas rodando continuamente,sem bloquar
* uma tarefa rodando periodicamente, entrando em estado bloqueado logo apos terminar de executar

Para a tarefa periódica possui prioridade mair, irá preempter as outras quando ela tiver que executar

![image-20210301233656079](Images - Relatório - Capítulo 1/image-20210301233656079.png)



## 1_7_Ex7

Aqui tornamos explicito a execução da tarefa idle, usando do contador `uiIdleCycleCount`, que é incrementado por meio do idle hook `vApplicationIdleHook` (uma função de callback hat is automatically called by the idle task once per iteration of the idle task loop)

A unica tarefa possui um objetivo simples: printar periodicamente o valor de `uiIdleCycleCount`, onde é possível ver que a tarefa idle ficou executando enquanto a tarefa principal dormia



## 1_8_Ex8

Sõa instnaciadas duas tasks, sendo que a prioridade da task 2 é trocada ao longo da execução do código. Dessa forma ambas as tarefas podem executar concomitantemente, mesmo que no inicio da execução a tarefa 1 possua uma prioridade mais alta

![image-20210301234710193](Images - Relatório - Capítulo 1/image-20210301234710193.png)



## 1_9_Ex9

Uma task roda periodicamente a cada 100ms, criando uma nova tarefa.

Essas novas tarefas criadas, por sua vez, só fazem uma coisa: se deletam logo após iniciarem, são terafas kamikaze!! Após serem deletadas, essas tasks não podem mais voltar a rodar e a aidle task irá cuidar de liberar a memória delas.

![image-20210302000318061](Images - Relatório - Capítulo 1/image-20210302000318061.png)



## 2_3_Ex10

This example demonstrates a queue being created, data being sent to the queue from multiple tasks,
and data being received from the queue

The priority of the tasks that send to the queue is lower than the priority of the task that receives from
the queue. This means the queue should never contain more than one item



cada sender manda um valor diferente (100 e 200). Ambos enviam de forma alternada, pois possuem a mesma prioridade

o mutex é utilizado para garantir que as tasks consigam acessar a UART de forma exclusiva, sem serem preempted por outras, garantindo a integridade da string enviada.

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xQueueHandle xQueue;
xSemaphoreHandle xMutexUART;	// a UART é compartilhada e deve ser alocada por cada tarefa

//--------------------------------------------------------------------------------
static void vSenderTask(void *pvParameters)
{
	uint32_t uiValueToSend;
	portBASE_TYPE xStatus;
	
	uiValueToSend = (uint32_t) pvParameters;
	
	while(1)
	{
		xStatus = xQueueSendToBack(xQueue, &uiValueToSend, 0);
		
		if(xStatus != pdPASS)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);				// pede o recurso
			printf("Could not send to the queue.\n");
			xSemaphoreGive(xMutexUART);								// libera o recurso
		}
		taskYIELD();
	}
}
//--------------------------------------------------------------------------------
static void vReceiverTask(void *pvParameters)
{
	uint32_t uiReceivedValue;
	portBASE_TYPE xStatus;
	const portTickType xTicksToWait = 250/portTICK_RATE_MS;
	
	while(1)
	{
		if(uxQueueMessagesWaiting(xQueue)!=0)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Queue should have been empty.\n");
			xSemaphoreGive(xMutexUART);
		}
			
		xStatus = xQueueReceive(xQueue, &uiReceivedValue, xTicksToWait);
		
		if(xStatus == pdPASS)
		{
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Received = %u\n", uiReceivedValue);
			xSemaphoreGive(xMutexUART);
		}
		else
		{	
			xSemaphoreTake(xMutexUART,portMAX_DELAY);
			printf("Could not Receive from the queue.\n");
			xSemaphoreGive(xMutexUART);
		}
		
	}
}

//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xQueue = xQueueCreate(5, sizeof(uint32_t));
	xMutexUART = xSemaphoreCreateMutex();	// acesso restrito ao hardware para o uso da UART
	
	if(xQueue != NULL)
	{	
		xTaskCreate(vSenderTask,"Sender 1",100, (void *)100, 1, NULL);
		xTaskCreate(vSenderTask,"Sender 2",100, (void *)200, 1, NULL);
		xTaskCreate(vReceiverTask, "Receiver", 100, NULL, 2, NULL);
		
		vTaskStartScheduler();	
	}
	else
	{
		// A fila nao pode ser criada
	}
	
	while (1);
}
//--------------------------------------------------------------------------------
```
## 2_3_Ex11

 to Transfer Compound Types, source e value

Often the receiver of
the data needs to know where the data came from so it can determine how it should be processed.

As tarefas executadas são similares ao exemplo anterior, mas como a tarefa de leitura possui prioridade _menor_ que as tarefas de esccrita então a fila estará sempre cheia (assim que um item é retirado as tarefas de escrita já voltam para o estado Ready)

desenhar com exemplo de jogo (joystick, acelerometro, botões)

![image-20210305232737421](Images - Relatório - Capítulo 1/image-20210305232737421.png)

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xQueueHandle xQueue;
xSemaphoreHandle xMutex;

typedef struct  
{
	unsigned char ucValue;
	unsigned char ucSource;
}xData;

xData xStructsToSend[2]=
{
	{100, 1},
	{200, 2}
};
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static void vSenderTask(void *pvParameters)
{
	portBASE_TYPE xStatus;
	const portTickType xTicksToWait = 250/portTICK_RATE_MS;
	
	while(1)
	{
		xStatus = xQueueSendToBack(xQueue, pvParameters, xTicksToWait);
		
		if(xStatus != pdPASS)
		{
			xSemaphoreTake(xMutex,portMAX_DELAY);				// pede o recurso
			printf("Could not send to the queue.\n");
			xSemaphoreGive(xMutex);								// libera o recurso
		}
		taskYIELD();
	}
}
//--------------------------------------------------------------------------------
static void vReceiverTask(void *pvParameters)
{
	xData xReceivedStructure;
	portBASE_TYPE xStatus;
	
	while(1)
	{
		if(uxQueueMessagesWaiting(xQueue)!=3)
		{
			xSemaphoreTake(xMutex,portMAX_DELAY);
			printf("Queue should have been full!\n");
			xSemaphoreGive(xMutex);	
		}
			
		xStatus = xQueueReceive(xQueue, &xReceivedStructure, 0);
		
		if(xStatus == pdPASS)
		{
			if(xReceivedStructure.ucSource == 1)
			{
				xSemaphoreTake(xMutex,portMAX_DELAY);
				printf("From Sender 1 = %u\n", xReceivedStructure.ucValue);
				xSemaphoreGive(xMutex);
			}
			else
			{
				xSemaphoreTake(xMutex,portMAX_DELAY);
				printf("From Sender 2 = %u\n", xReceivedStructure.ucValue);
				xSemaphoreGive(xMutex);				
			}
		}
		else
		{	
			xSemaphoreTake(xMutex,portMAX_DELAY);
			printf("Could not Receive from the queue.\n");
			xSemaphoreGive(xMutex);	
		}
		
	}
}
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//RTOS
	
	xQueue = xQueueCreate(3, sizeof(xData));
	xMutex = xSemaphoreCreateMutex();	// acesso restrito ao hardware para o uso da UART
	
	if(xQueue != NULL)
	{	
		xTaskCreate(vSenderTask,"Sender 1",100, &(xStructsToSend[0]), 2, NULL);
		xTaskCreate(vSenderTask,"Sender 2",100, &(xStructsToSend[1]), 2, NULL);
		xTaskCreate(vReceiverTask, "Receiver", 100, NULL, 1, NULL);
		
		vTaskStartScheduler();	
	}
	else
	{
		
	}
	
	while (1);
}
//--------------------------------------------------------------------------------
```
## 3_2_Ex12

A Binary Semaphore can be used to unblock a task each time a particular interrupt occurs, effectively
synchronizing the task with the interrupt.

uma tarefa periodica aciona a ISR (que em uma aplicação real poderia ser acionada um timer, onversão de ADC, um evento externo, entre outros). A ISR libera o semáforo permitindo que a função de handler execute

![image-20210306000348813](Images - Relatório - Capítulo 1/image-20210306000348813.png)

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xSemaphoreHandle xMutexUART;
xSemaphoreHandle xBinarySemaphore;

//--------------------------------------------------------------------------------
static void vPeriodicTask(void *pvParameters)
{
	while(1)
	{	
		vTaskDelay(500/portTICK_RATE_MS);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - About to generate an interrupt.\n");
		xSemaphoreGive(xMutexUART);
		
		NVIC_SetPendingIRQ((IRQn_Type)EXTI0_IRQn);	// ativa interrupção por software
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - Interrupt generated.\n\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
static void vHandlerTask(void *pvParameters)
{
	
	while(1)
	{
		xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Handler task - Processing event.\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
void EXTI0_IRQHandler()
{
	static portBASE_TYPE xHigherPriorityTaskWoken;

	xHigherPriorityTaskWoken = pdFALSE;

	xSemaphoreGiveFromISR(xBinarySemaphore, &xHigherPriorityTaskWoken);

	if(xHigherPriorityTaskWoken == pdTRUE)
	{
		//printf("xHigherPriorityTaskWoken == pdTRUE.\n");
	}

	//use to force a context switch from an ISR
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	/* Giving the semaphore unblocked a task, and the priority of the
	unblocked task is higher than the currently running task - force
	a context switch to ensure that the interrupt returns directly to
	the unblocked (higher priority) task.*/
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//---------------------------------------------------------------------------------------------
	// Habilitar interrupção por SW
	//---------------------------------------------------------------------------------------------
	NVIC_EnableIRQ((IRQn_Type) EXTI0_IRQn);		// usa EXTI0 para gerar interrupção por software

	/* configura Preempt Priority and Subpriority interrupt para o valor default (no STM32 é diferente!)*/
	NVIC_SetPriorityGrouping(0);

	/*IMPORTANTISSIMO
    	The priority of the interrupt has to be set to below configMAX_SYSCALL_INTERRUPT_PRIORITY
    	(FreeRTOSConfig.h), caso contrario a ISR que chama API do FreeRTOS vai travar!
    	Cuidado, pois há uma inversão, no ARM quando menor o nr, maior a prioridade!*/

	NVIC_SetPriority(EXTI0_IRQn, 6);	// ajusta prioridade para nivel 6, com 	configMAX_SYSCALL_INTERRUPT_PRIORITY = 5
	//---------------------------------------------------------------------------------------------

	//RTOS
	xMutexUART = xSemaphoreCreateMutex();
	xBinarySemaphore = xSemaphoreCreateBinary();
	
	if(xBinarySemaphore != NULL)
	{
		xTaskCreate(vHandlerTask,"Handler",100, NULL, 3, NULL);
		xTaskCreate(vPeriodicTask, "Periodic", 100, NULL, 1, NULL);
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
```
## 3_3_Ex13

the previous sequence is perfectly adequate if interrupts can only occur at a relatively low frequency.

3 hadler s]ao ativadas

Essa situação pode ocorrer, por exemplo, se uma tecla de interação com o usuário acionar uma ISR e o usuário apertar repetidamente a tecla MUITO rapido, situação na qual ainda gostariamos de saber exatamente quantas vezes a tecla foi pressionada

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xSemaphoreHandle xMutexUART;
xSemaphoreHandle xCountingSemaphore;

//--------------------------------------------------------------------------------
static void vPeriodicTask(void *pvParameters)
{
	while(1)
	{
		vTaskDelay(500/portTICK_RATE_MS);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - About to generate an interrupt.\n");
		xSemaphoreGive(xMutexUART);
		
		NVIC_SetPendingIRQ((IRQn_Type) EXTI0_IRQn);	// ativa interrupção por software
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - Interrupt generated.\n\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
static void vHandlerTask(void *pvParameters)
{
	
	while(1)
	{
		xSemaphoreTake(xCountingSemaphore, portMAX_DELAY);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Handler task - Processing event.\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
// Interrupção ativa por software - Emprego do TC0 canal 0
//--------------------------------------------------------------------------------
void EXTI0_IRQHandler()
{
	static portBASE_TYPE xHigherPriorityTaskWoken;
	
	xHigherPriorityTaskWoken = pdFALSE;
	
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
	xSemaphoreGiveFromISR(xCountingSemaphore, &xHigherPriorityTaskWoken);
			
	if(xHigherPriorityTaskWoken == pdTRUE)
	{
		//printf("xHigherPriorityTaskWoken == pdTRUE.\n");
	}

	//use to force a context switch from an ISR
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	/* Giving the semaphore unblocked a task, and the priority of the
	unblocked task is higher than the currently running task - force
	a context switch to ensure that the interrupt returns directly to
	the unblocked (higher priority) task.*/
}
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//---------------------------------------------------------------------------------------------
	// Habilitar interrupção por SW
	//---------------------------------------------------------------------------------------------
	NVIC_EnableIRQ((IRQn_Type) EXTI0_IRQn);		// usa EXTI0 para gerar interrupção por software

	/* configura Preempt Priority and Subpriority interrupt para o valor default (no STM32 é diferente!)*/
	NVIC_SetPriorityGrouping(0);

	/*IMPORTANTISSIMO
    	The priority of the interrupt has to be set to below configMAX_SYSCALL_INTERRUPT_PRIORITY
    	(FreeRTOSConfig.h), caso contrario a ISR que chama API do FreeRTOS vai travar!
    	Cuidado, pois há uma inversão, no ARM quando menor o nr, maior a prioridade!*/

	NVIC_SetPriority(EXTI0_IRQn, 6);	// ajusta prioridade para nivel 6, com 	configMAX_SYSCALL_INTERRUPT_PRIORITY = 5
	//---------------------------------------------------------------------------------------------

	//RTOS
	xMutexUART = xSemaphoreCreateMutex();
	xCountingSemaphore = xSemaphoreCreateCounting(10,0);
	
	if(xCountingSemaphore != NULL)
	{
		xTaskCreate(vHandlerTask,"Handler",100, NULL, 3, NULL);
		xTaskCreate(vPeriodicTask, "Periodic", 100, NULL, 1, NULL);
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
```
## 3_4_Ex14

Pela primeira vez, utilizaremos xQueueReceiveFromISR() para ler valores da queue de dentro da propria ISR

A periodic task is created that sends five numbers to a queue every 200 milliseconds.

Os nueros são incrementados em aritimética módulo 4, sendo uma string diferente printada para cada valor envado. Quando a interrução for acionada, essa irá consumir todos os numeros adicionado à queue.

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

xSemaphoreHandle xMutexUART;
xQueueHandle	 xIntegerQueue;
xQueueHandle	 xStringQueue;

//--------------------------------------------------------------------------------
static void vIntegerGeneration(void *pvParameters)
{
	portTickType xLastExecutionTime;
	unsigned int ulValueToSend = 0;
	unsigned int i;
	
	while(1)
	{	
		vTaskDelayUntil(&xLastExecutionTime, 200/portTICK_RATE_MS);
		
		for(i=0; i<5; i++)
		{
			xQueueSendToBack( xIntegerQueue, &ulValueToSend, 0);
			ulValueToSend++;
		}
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - About to generate an interrupt.\n");
		xSemaphoreGive(xMutexUART);
		
		NVIC_SetPendingIRQ((IRQn_Type) EXTI0_IRQn);	// ativa interrupção por software
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf("Periodic task - Interrupt generated.\n\n");
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
static void vStringPrinter(void *pvParameters)
{
	char *pcString;
	
	while(1)
	{
		xQueueReceive(xStringQueue, &pcString, portMAX_DELAY);
		
		xSemaphoreTake(xMutexUART,portMAX_DELAY);
		printf(pcString);
		xSemaphoreGive(xMutexUART);
	}
}
//--------------------------------------------------------------------------------
// Interrupção ativa por software - Emprego do TC0 canal 0
//--------------------------------------------------------------------------------
void EXTI0_IRQHandler()
{
	static portBASE_TYPE xHigherPriorityTaskWoken;
	static uint32_t ulReceivedNumber;
	
	static const char *pcStrings[]=
	{
		"String 0\n",
		"String 1\n",
		"String 2\n",
		"String 3\n",
	};
	
	xHigherPriorityTaskWoken = pdFALSE;
	
	while(xQueueReceiveFromISR(xIntegerQueue, &ulReceivedNumber, &xHigherPriorityTaskWoken) != errQUEUE_EMPTY)
	{
		ulReceivedNumber &= 0x03;
		xQueueSendToBackFromISR(xStringQueue, &pcStrings[ulReceivedNumber], &xHigherPriorityTaskWoken);
	}
			
	if(xHigherPriorityTaskWoken == pdTRUE)
	{
		//printf("xHigherPriorityTaskWoken == pdTRUE.\n");
	}
	//use to force a context switch from an ISR
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	/* Giving the semaphore unblocked a task, and the priority of the
	unblocked task is higher than the currently running task - force
	a context switch to ensure that the interrupt returns directly to
	the unblocked (higher priority) task.*/
}
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	//---------------------------------------------------------------------------------------------
	// Habilitar interrupção por SW
	//---------------------------------------------------------------------------------------------
	NVIC_EnableIRQ((IRQn_Type) EXTI0_IRQn);		// usa EXTI0 para gerar interrupção por software

	/* configura Preempt Priority and Subpriority interrupt para o valor default (no STM32 é diferente!)*/
	NVIC_SetPriorityGrouping(0);

	/*IMPORTANTISSIMO
    	The priority of the interrupt has to be set to below configMAX_SYSCALL_INTERRUPT_PRIORITY
    	(FreeRTOSConfig.h), caso contrario a ISR que chama API do FreeRTOS vai travar!
    	Cuidado, pois há uma inversão, no ARM quando menor o nr, maior a prioridade!*/

	NVIC_SetPriority(EXTI0_IRQn, 6);	// ajusta prioridade para nivel 6, com 	configMAX_SYSCALL_INTERRUPT_PRIORITY = 5
	//---------------------------------------------------------------------------------------------

	//RTOS

	xMutexUART = xSemaphoreCreateMutex();
	
	xIntegerQueue = xQueueCreate(10, sizeof(uint32_t));
	xStringQueue  = xQueueCreate(10, sizeof(char *));	
	
	xTaskCreate(vIntegerGeneration, "IntGen",100, NULL, 1, NULL);
	xTaskCreate(vStringPrinter, "String", 100, NULL, 2, NULL);
		
	vTaskStartScheduler();
		
	while (1);
}
//--------------------------------------------------------------------------------
```
## 4_3_Ex15

The two instances of prvPrintTask() are created at different priorities so the lower priority task will
sometimes (em intervalos de tempo aleatórios) be pre-empted by the higher priority task 

Como o mutex é utilizado para permitir o compartilhamento da UART, pode-se garantir que as informações serão enviadas de forma íntegra, sem a operação ser preemted no meio de sua execução.



![image-20210306122616704](Images - Relatório - Capítulo 1/image-20210306122616704.png)

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

unsigned int lfsr;			// variável global para a geração de números pseudo-aleatorios

xSemaphoreHandle xMutex;

void init_LFSR(unsigned int valor);
unsigned int prng_LFSR() ;

//--------------------------------------------------------------------------------
static void prvNewPrintString( const portCHAR * pcString)
{
	xSemaphoreTake(xMutex, portMAX_DELAY);
	{
		printf(pcString);
		printf("%u\n",prng_LFSR()& 0x000003E8);
	}
	xSemaphoreGive(xMutex);
}
//--------------------------------------------------------------------------------
static void prvPrintTask(void *pvParameters)
{
	char *pcStringToPrint;
	
	pcStringToPrint = (char *)pvParameters;
	
	while(1)
	{	
		prvNewPrintString(pcStringToPrint);
		
		vTaskDelay(prng_LFSR() & 0x000003E8); // valor entre 0 1000 ms;
	}
}
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	
	/* semente para o PRNG (deve ser gerada automaticamente em cada reinicialização do uC)
	 * pode ser empregado o ADC com algum entrada flutuante para gerar a aleatóriedade desse nr.
	 */
	init_LFSR(13);		// valor de semente fixo para efeito de teste.

	
	//RTOS
	
	xMutex = xSemaphoreCreateMutex();
	
	if(xMutex != NULL)
	{
		xTaskCreate(prvPrintTask, "Print1", 100, "Task1 ----------------- = ", 1, NULL);
		xTaskCreate(prvPrintTask, "Print2", 100, "Task2 ***************** = ", 2, NULL);
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
/*
 * Função que passa o valor "semente" para a variavel utilizada na função prng_LFSR()
 */
void init_LFSR(unsigned int valor)
{
	lfsr = valor;
}
//--------------------------------------------------------------------------------
/*
 * Função PRNG - toda vez que for chamada, um novo número é gerado e salvo em lfsr.
 * 	O primeiro valor de lfsr deve ser diferente de zero e deve ser gerado como semente
 */
unsigned int prng_LFSR() 	// Galois LFSRs, Liner-Feedback Shift Register, (PRNG)
{
	unsigned int lsb;

	if (lfsr==0)		// garantia para que valor nao seja zero
	    lfsr = 1;

	lsb = lfsr & 0x00000001;
	lfsr = lfsr >> 1;

	if (lsb)
	    lfsr = lfsr ^ 0x80000057;	//polinomio retirado de http://users.ece.cmu.edu/~koopman/lfsr/

	return lfsr;
}
//--------------------------------------------------------------------------------

```
## 4_4_Ex16

Usamos um gatekeer para evitar deadock e priority invertion, então the string is sent on the queue to the gatekeeper task rather than written out directly

Além das duas tasks igual ao exemplo anterior, adicionou-se uma terceira fonte de dados: um TickHook (uma função de callback similar ao ide hook, executado a cada tick interrupt) que regularmente envia uma string.

```C
#include "init_from_hal.h"
//--------------------------------------------------------------------------------

unsigned int lfsr;			// variável global para a geração de números pseudo-aleatorios

xQueueHandle	xPrintQueue;

void init_LFSR(unsigned int valor);
unsigned int prng_LFSR() ;

//--------------------------------------------------------------------------------
static char *pcStringToPrint[] = 
{
	"Task 1 ********************************************* \n",
	"Task 2 --------------------------------------------- \n",
	"Message printed from the tick hook interrupt ####### \n",
};
//--------------------------------------------------------------------------------
static void prvStdioGateKeeperTask(void *pvParameters)
{
	char *pcMessageToPrint;
	
	while(1)
	{
		xQueueReceive(xPrintQueue, &pcMessageToPrint, portMAX_DELAY);
		printf(pcMessageToPrint);
	}
}
//--------------------------------------------------------------------------------
static void prvPrintTask(void *pvParameters)
{
	uint32_t uiIndexToString;
	
	uiIndexToString = (uint32_t)pvParameters;
	
	while(1)
	{	
		xQueueSendToBack(xPrintQueue, &(pcStringToPrint[uiIndexToString]), 0);
		
		vTaskDelay(prng_LFSR() & 0x000001FF); // valor entre 0 511 ms;
	}
}
//--------------------------------------------------------------------------------
void vApplicationTickHook()	
{
	static uint32_t uiCount = 0;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	
	uiCount++;
	
	if(uiCount>=200)
	{
		xQueueSendToFrontFromISR(xPrintQueue, &(pcStringToPrint[2]), &xHigherPriorityTaskWoken);
		uiCount=0;
	}
}
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
int main (void)
{
	HAL_Init();

	SystemClock_Config();

	MX_GPIO_Init();

	MX_USART3_UART_Init();

	
	/* semente para o PRNG (deve ser gerada automaticamente em cada reinicialização do uC)
	 * pode ser empregado o ADC com algum entrada flutuante para gerar a aleatóriedade desse nr.
	 */
	init_LFSR(13);		// valor de semente fixo para efeito de teste.

	
	//RTOS
	
	xPrintQueue = xQueueCreate(5, sizeof(char *));
	
	if(xPrintQueue != NULL)
	{
		xTaskCreate(prvPrintTask, "Print1", 100, (void *) 0, 1, NULL);
		xTaskCreate(prvPrintTask, "Print2", 100, (void *) 1, 2, NULL);
		xTaskCreate(prvStdioGateKeeperTask, "GateKeeper", 100, NULL, 0, NULL);		
		
		vTaskStartScheduler();
	}
		
	while (1);
}
//--------------------------------------------------------------------------------
/*
 * Função que passa o valor "semente" para a variavel utilizada na função prng_LFSR()
 */
void init_LFSR(unsigned int valor)
{
	lfsr = valor;
}
//--------------------------------------------------------------------------------
/*
 * Função PRNG - toda vez que for chamada, um novo número é gerado e salvo em lfsr.
 * 	O primeiro valor de lfsr deve ser diferente de zero e deve ser gerado como semente
 */
unsigned int prng_LFSR() 	// Galois LFSRs, Liner-Feedback Shift Register, (PRNG)
{
	unsigned int lsb;

	if (lfsr==0)		// garantia para que valor nao seja zero
	    lfsr = 1;

	lsb = lfsr & 0x00000001;
	lfsr = lfsr >> 1;

	if (lsb)
	    lfsr = lfsr ^ 0x80000057;	//polinomio retirado de http://users.ece.cmu.edu/~koopman/lfsr/

	return lfsr;
}
//--------------------------------------------------------------------------------

```