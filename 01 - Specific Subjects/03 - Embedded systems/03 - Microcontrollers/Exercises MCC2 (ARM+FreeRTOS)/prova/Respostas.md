## 1)


text: contem o código executavel

data: contem os valores de inicialização das variáveis globais ou estáticas

bss: contém as variáveis globais e estáticas não inicializadas

dec: sum of text, data and bss (0x1408+0x18+0x81C = 0x1C3C = 7228 em decimal)

hex: mesmo valor acima, em hexadecimal

filename: nome do arquivo compilado elf

## 2)

A task 2 possui prioridade maior que a 1 e nunca entra no estado bloqueado, impedindo a task 1 de executar

## 3)

Um ponteiro para o segmento de memória contendo os parâmetros passados para a função

## 4)

A tarefa fica em estado bloquado até a ocorrência de uma situação (evento) que dispare a sua execução

## 5)

Qualquer RTOS terá - no mínimo - um overhead causado pelo escalonador comutando as tarefas. 

No cenário A as duas tarefas ficarão dividindo igualmente o tempo do processador, o que pode ser implementado de forma aproximada executando duas tarefas sequencialmente dentro de um loop infinito. Entretanto, nessa implementação em loop teríamos dificuldade em garantir que as duas tarefas compartilham o processador _igualmente_.

No cenário B um RTOS seria de pouca utilidade, visto que estaríamos executando sempre a mesma tarefa. Caso tivermos suficiente poder de processamento e existir a possibilidade de, no futuro, expandir a aplicação para um cenário mais complexo, então talvez a utilização de um RTOS se justifique para implementarmos um código mais fácil de ser modificado. 

## 6)

Quando, por exemplo, essa está esperando a liberação de algum recurso, como um semáforo ou uma queue. 

## 7)

A mínima possível.

## 8)

O escalonador não é sincronizado com a UART, então antes de uma tarefa terminar de enviar a sua mensagem a outra tarefa pode ser entrar no seu lugar e começar a enviar a outra mensagem. 

Existe diversos mecanismos para permitir o compartilhamento adequado de recursos, como critical section, scheduler suspension, mutex ou gatekeeper task.

## 9)

Uma tarefa que é a **única** que pode acessar diretamente um certo recurso, como uma UART. As requisições para acessar o recurso devem ser serializadas em uma queue, e a gatekeeper irá ler as requisições da queue e acessar o recurso. 

## 10)

Se TRNG_ODATA for uma variáve inteira de 8 bits (0 a 255), podemos fazer:

meu_nr = ((uint16)(TRNG->TRNG_ODATA))\*18/255 + 1

## 11)

É a estrutura utilizada pelo RTOS para armazenar informações sobre cada task, como por exemplo os campos ID, prioridade, program counter, entre outros.

## 12)

fenomeno onde uma tarefa de alta prioridade é obrigada a ficar esperando por que uma tarefa de baixa prioridade ficou ocupando o semáforo ou mutex.

## 13)

| Loop                     | RTOS                               |
| ------------------------ | ---------------------------------- |
| tarefas são sequenciais  | tarefas são concorrentes           |
| Uso otimizado da CPU  | Perda de processamento pelo kernel |
| As funções não podem conter um laço infinito dentro                         |  As tarefas são independentes, podendo inclusive conter laços infinitos                                  |



## 14)

Vamos supor a existencia de duas tarefas A e B, e dois recursos R1 e R2.

Um deadlock pode acontecer caso A seja impedida de executar por que ela precisa de R1, que está sendo utilizado por B, ao mesmo tempo em que B precisa do recurso R2 que está sendo utilizado por A.

Ou seja, uma tarefa bloqueia a outra e não é possível proceder.



## 15)

Queue: uma estrutura de dados que permite armazenar um número finito de items e que pode ser acessada por várias tarefas.

Semáforo: um mecanismo que permite a várias tarefas adquirem e liberarem o uso de um recurso.

## 16)

É um mecanismo que garante o uso exclusivo de um recurso, sem ser *preemted* no meio da utilização (como ocorreu na pergunta 8 com a UART). 

## 17)

São dois algoritmos de scheduling diferentes.

O Round-Robin executa as tarefas de forma sequencial e circular, executando cada uma por um slot de tempo fixo. Ou seja, todas as tarefas possuem a mesma prioridade.

O RMS atribui uma prioridade fixa para cada tarefa tal que a prioridade seja inversamente proporcional ao tempo de execução. Ou seja, as tarefas mais curtas receberão uma prioridade mais alta.

## 18)

A delay_ms entra em um laço, portanto a tarefa se mantem exeutando durante o período do delay.

a vTaskDelay bloqueia a tarefa por aquele tempo, permitindo que outra tarefa seja executada

## 19)

Podemos utilizar o idle hook para realizar uma ação sempre que a idle task for executada, para por exemplo entrar em modo de baixo consumo energético.

## 20)

Além de gravar e limpar a memória, o ST LINK permite executar o código em modo de depuração: acompanhando a execução linha a linha, adicionando breakpoints, pausando a execução, entre outros.

