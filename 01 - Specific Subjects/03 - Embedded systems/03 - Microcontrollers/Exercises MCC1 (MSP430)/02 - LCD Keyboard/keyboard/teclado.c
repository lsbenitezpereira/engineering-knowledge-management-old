/*  Keyboard library
 *  Created on: March 20th, 2020
 *      Author: Leonardo Benitez
 *
 *      Adaptado de AVR e Arduino: Técnicas de Projeto, 2a ed. - 2012.
 *      Instituto Federal de Santa Catarina
 */

#include "teclado.h"

/* MACROS */
/* O normal seria essas macros ficarem em um arquivo separado, mas como os envios
 * do moodle são apenas para os arquivos "main.c, lcd.c, lcd.h, teclado.c, teclado.h"
 * então resolvi colocar aqui
*/
#define SET(BIT) (1 << BIT)
#define SET_BIT(Y,BIT) (Y |= (BIT))
#define CLR_BIT(Y,BIT) (Y &= ~(BIT))
#define CPL_BIT(Y,BIT) (Y ^= (BIT))
#define TST_BIT(Y,BIT) (Y & (BIT))

/* Convert Px to PxOUT */
#define PORT_OUT(...) PORT_OUT_SUB(__VA_ARGS__)
#define PORT_OUT_SUB(port) (port##OUT)

/* Convert Px to PxDIR */
#define PORT_DIR(...) PORT_DIR_SUB(__VA_ARGS__)
#define PORT_DIR_SUB(port) (port##DIR)

/* Convert Px to PxREN */
#define PORT_REN(...) PORT_REN_SUB(__VA_ARGS__)
#define PORT_REN_SUB(port) (port##REN)

/* Convert Px to PxIN */
#define PORT_IN(...) PORT_IN_SUB(__VA_ARGS__)
#define PORT_IN_SUB(port) (port##IN)

/* Convert Px to PxIE */
#define PORT_IE(...) PORT_IE_SUB(__VA_ARGS__)
#define PORT_IE_SUB(port) (port##IE)

/* Convert Px to PxIES */
#define PORT_IES(...) PORT_IES_SUB(__VA_ARGS__)
#define PORT_IES_SUB(port) (port##IES)

/* Convert Px to PxIFG */
#define PORT_IFG(...) PORT_IFG_SUB(__VA_ARGS__)
#define PORT_IFG_SUB(port) (port##IFG)



//matriz com as informações para decodificação do teclado
const unsigned char teclado[4][4] = {{0x01, 0x02, 0x03, 0x0F},
                                    {0x04, 0x05, 0x06, 0x0E},
                                    {0x07, 0x08, 0x09, 0x0D},
                                    {0x0A, 0x00, 0x0B, 0x0C}};

void key_4x4_init(){
    PORT_DIR(KEY_PORT) = 0b11110000;
    PORT_REN(KEY_PORT) = 0b11111111;
    PORT_OUT(KEY_PORT) = 0b00000000;
}

unsigned char read_key(){
    unsigned char n, j, linha, tecla=255;

    for(n=4;n<8;n++){
        SET_BIT(PORT_OUT(KEY_PORT), SET(n)); //liga uma coluna
        __delay_cycles(10000); // ~=10ms. atraso para uma varredura mais lenta
        linha = PORT_IN(KEY_PORT); //lê o valor das linhas
        for(j=0;j<4;j++){
            if(TST_BIT(linha,SET(j))) //se foi pressionada alguma tecla, decodifica
                tecla = teclado[j][n-4];
        }
        CLR_BIT(PORT_OUT(KEY_PORT), SET(n)); //limpa o bit setado anteriormente
    }
    return tecla;
}





