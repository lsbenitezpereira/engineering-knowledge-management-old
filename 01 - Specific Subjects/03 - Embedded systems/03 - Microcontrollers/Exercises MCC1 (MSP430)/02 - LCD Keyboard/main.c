/*
 * 02_main_lcd.c
 *
 *  Created on: Mar 2, 2020
 *      Author: Renan Augusto Starke and Leonardo Benitez
 *      Instituto Federal de Santa Catarina *
 *  Description:
 *      Read a 4x4 keyboard
 *      Print sequentially in a LCD display
 *      Key C clears the display
 */


#include <msp430.h>
#include <stdio.h>

#include "displays/lcd.h"
#include "keyboard/teclado.h"

void main(){
    unsigned char key;
    uint8_t position = 0;

    /* Configuração de hardware */
    WDTCTL = WDTPW | WDTHOLD;
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode  (para o FR2355)

    lcd_init_4bits();
    key_4x4_init();
    lcd_send_data(LCD_CLEAR, LCD_CMD);
    _delay_cycles(100000);

    while (1){
        key = read_key();
        if (key==0x0C){ // clear display
            lcd_send_data(LCD_CLEAR, LCD_CMD);
            position=0;
        } else if (key!=255){
            // hex to ascii
            key+=48;
            if (key>57) key+=7;

            // break line
            if (position==16) lcd_send_data(LCD_LINE_1, LCD_CMD);

            // if display not full, write data
            if (position<32){
                lcd_send_data(key, LCD_DATA);
                position++;
            }
        }
        __delay_cycles(1000);
    }
}

