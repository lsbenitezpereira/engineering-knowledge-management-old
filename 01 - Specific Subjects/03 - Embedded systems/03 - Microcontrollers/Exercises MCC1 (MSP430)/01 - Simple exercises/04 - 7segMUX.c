//***************************************************************************************
//  MSP usage example
//  Author: Leonardo Benitez
//  Date: 2020-1
//***************************************************************************************

#include <msp430.h>
#include <stdint.h>
#include "lib/bits.h"
#include "displays/simple_display_mux.h"

#define LED_1   BIT0 //P1.0
#define LED_2   BIT6 //P6.6
#define BUTTON_1  BIT1 //P4.1
#define BUTTON_2  BIT3 //P2.3


void main(void) {
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
                                            // to activate previously configured port settings
    P6DIR |= LED_2;       // Set to output direction
    P6OUT = 0x0;


    P4REN |= BUTTON_1; //enable pull resistor
    P4OUT |= BUTTON_1; //pull is up

    P2REN |= BUTTON_2; //enable pull resistor
    P2OUT |= BUTTON_2; //pull is up

    display_mux_init();

    volatile uint16_t i;
    uint64_t x = 0x00654321;

    for(;;) {
        P6OUT ^= LED_2;                      // Toggle leds

        /* write and increment */
        display_mux_write(x);

        _delay_cycles(600);

        /// Read buttons for extended delay
        if (!TST_BIT(P4IN, BUTTON_1)) x+=0x00000001;
        if (!TST_BIT(P2IN, BUTTON_2)) x+=0x00001000;
    }
}
