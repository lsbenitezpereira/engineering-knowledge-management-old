/***************************************************
 * Created by: Leonardo Benitez
 * Date: 2020/03
 * License: MIT
****************************************************/

#ifndef LIB_DELAY_H
#define LIB_DELAY_H

#include <stdint.h>
void delay_ms_init();
void delay_ms(int time);

#endif
