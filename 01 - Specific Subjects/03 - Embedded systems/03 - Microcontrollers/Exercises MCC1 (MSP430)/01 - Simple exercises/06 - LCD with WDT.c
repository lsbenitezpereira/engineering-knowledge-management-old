//***************************************************************************************
//  MSP usage example
//  Author: Leonardo Benitez
//  Date: 2020-1
//***************************************************************************************

#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include "lib/bits.h"
#include "lib/gpio.h"
#include "lib/delay_wdt.h"
#include "displays/lcd.h"

#define LED_BIT BIT6
#define LED_PORT P6

void init_clock_system(void) {

    // Configure two FRAM wait state as required by the device data sheet for MCLK
    // operation at 24MHz(beyond 8MHz) _before_ configuring the clock system.
    FRCTL0 = FRCTLPW | NWAITS_2 ;

    P2SEL1 |= BIT6 | BIT7;                       // P2.6~P2.7: crystal pins
    do
    {
        CSCTL7 &= ~(XT1OFFG | DCOFFG);           // Clear XT1 and DCO fault flag
        SFRIFG1 &= ~OFIFG;
    } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag

    __bis_SR_register(SCG0);                     // disable FLL
    CSCTL3 |= SELREF__XT1CLK;                    // Set XT1 as FLL reference source
    CSCTL0 = 0;                                  // clear DCO and MOD registers
    CSCTL1 = DCORSEL_7;                          // Set DCO = 24MHz
    CSCTL2 = FLLD_0 + 731;                       // DCOCLKDIV = 327358*731 / 1
    __delay_cycles(3);
    __bic_SR_register(SCG0);                     // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));   // FLL locked

    /* CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
     * set XT1 (~32768Hz) as ACLK source, ACLK = 32768Hz
     * default DCOCLKDIV as MCLK and SMCLK source
     - Selects the ACLK source.
     * 00b = XT1CLK with divider (must be no more than 40 kHz)
     * 01b = REFO (internal 32-kHz clock source)
     * 10b = VLO (internal 10-kHz clock source) (1)   */
    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;
}
void main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;     // Disable the GPIO power-on

    uint8_t i = 0;
    char string[8];

    init_clock_system(); // f = 24MHz
    delay_ms_init(24);
    lcd_init_4bits();

    lcd_send_data(LCD_CLEAR, LCD_CMD);
    delay_ms(100);

    lcd_write_string("MSP430");

    SET_BIT(PORT_DIR(LED_PORT), LED_BIT);
    while(1){
        CPL_BIT(PORT_OUT(LED_PORT), LED_BIT);

        lcd_send_data(LCD_LINE_1, LCD_CMD);
        snprintf(string, 8, "%d", i);
        lcd_write_string(string);
        i++;

        delay_ms(1000);
    }
}
