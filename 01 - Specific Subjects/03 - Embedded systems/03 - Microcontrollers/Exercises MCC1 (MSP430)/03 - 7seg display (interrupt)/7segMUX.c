//***************************************************************************************
//  MSP interrupt example
//  Author: Leonardo Benitez
//  Date: 2020-1
//  Description
//    Writes is a 7 seg display
//    It can be multiplexed, but here I'm using just one display (because it's the only hard I have)
//    The count is increment through a button, using an interruption
//  Slides: are together with "LCD keyboard" exercise
//***************************************************************************************

#include <msp430.h>
#include <stdint.h>
#include "lib/bits.h"
#include "lib/gpio.h"
#include "displays/simple_display_mux.h"

// Button1: used for interruption
#define BUTTON1_BIT  BIT3
#define BUTTON1_PORT P2

volatile uint16_t count = 0;

void config_ext_irq(){
    /* Direction = input */
    CLR_BIT(PORT_DIR(BUTTON1_PORT), BUTTON1_BIT);

    /* Pull up/down enable */
    SET_BIT(PORT_REN(BUTTON1_PORT), BUTTON1_BIT);

    /* Pull up */
    SET_BIT(PORT_OUT(BUTTON1_PORT), BUTTON1_BIT);

    /* Habilitação da IRQ apenas botão */
    SET_BIT(PORT_IE(BUTTON1_PORT), BUTTON1_BIT);

    /* Transição de nível alto para baixo */
    SET_BIT(PORT_IES(BUTTON1_PORT), BUTTON1_BIT);

    /* Limpa alguma IRQ pendente */
    CLR_BIT(PORT_IFG(BUTTON1_PORT), BUTTON1_BIT);
}

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the high-impedance mode

    config_ext_irq();
    display_mux_init();

    /* Habilita IRQs*/
    __bis_SR_register(GIE);

    while(1) {
        display_mux_write(count);
        __bis_SR_register(LPM4_bits);
    }
}

/* Port 2 ISR (interrupt service routine) */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_2 (void)
#else
#error Compiler not supported!
#endif
{
    /* Conta o número de pulsos */
    count++;

    /* Limpa sinal de IRQ */
    CLR_BIT(PORT_IFG(BUTTON1_PORT), BUTTON1_BIT);

    /* Turn CPU on */
    __bic_SR_register_on_exit(LPM4_bits);
}
