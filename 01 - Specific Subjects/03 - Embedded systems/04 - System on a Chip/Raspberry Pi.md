# Conceptualization

* series of small single-board computers 

*  ARM-based CPU

* None of the models have a built-in real-time clock.

* Não é considerado uC e sim SOC (System on a chip)

* Não possui interface analógica

* Good to work with media

* OS:  Raspbian (debian-based Linux distribution)

  ![Graphical Raspberry Pi GPIO Pinout](Images - Raspberry Pi/raspberry-pi-pinout.png)

* **Extensions and shield**
  * Rhubarb
    * For industrial control
    * https://www.arrow.com/en/products/rhubarb-ad1a/3ml-llc
  * UniPi: industry. Open?
  * MyPI
    * http://www.embeddedpi.com/
  
* piwheels

  * Python package repository providing Arm platform wheels (pre-compiled binary Python packages)

## Models

* No on board storage  (==all of them?==), but have sd connector 
* **Pi zero**
  * Does not have power led
  * no Ethernet circuitry 
  * 1GHz single-core CPU 512MB RAM
  * CSI camera connector (v1.3 only)
  * ==no GPU?==
  * Pi zero W
    * wireless embedded
    * An 0.5A power source was not enogth. Worked with 2A (leonardo)
  * 802.11n wireless LAN (wifi)
  * Bluetooth 4.0
  * mini-HDMI
  * Camera (v1.3 cable)

* **Pi 3**
  * HDMI
  * Gpu from broadcom
  * 4 cores

## Linux distros

* raspbian_lite

  * No graphical interface

* http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-08-17/	

## GPIO

* 3.3V
* `import RPi.GPIO as GPIO`
* `GPIO.cleanup()`
  * Put any ports you have used in this program  back to input mode
  * only affects any ports you have set in the  current program
  * used only for protection, so its a good practice to call it before finish the program 
* **Modes**
  * Define what names you’ll use
  * you can only use one system in each program
  * Board mode
    * Or *pin numbers*
    * If you use pin numbers, you don’t have to bother about revision checking, as RPi.GPIO takes care of that for you. You still need to be aware of which pins you can and can’t use though, since some are power and GND.
  * BCM mode
    * Or *Broadcom GPIO numbers*

## Comunications

* **I2C**
  
  * Enable I2C: https://www.raspberrypi-spy.co.uk/2014/11/enabling-the-i2c-interface-on-the-raspberry-pi/
  * show connected devices: `i2cdetect -y 1`
  
* **uart**

  * 3.3V only 

  * Which version is primary and which secondary depends of the device

  * By default, the primary UART is assigned to the Linux console

  * [Configuration docs](https://www.raspberrypi.org/documentation/configuration/uart.md)

  * Code sample:

  ```python
  import serial
  ser = serial.Serial("/dev/ttyS0", 115200, parity=serial.PARITY_NONE, stopbits = serial.STOPBITS_ONE, bytesize = serial.EIGHTBITS)
  ser.write(b"test666")
  ```

  * **Versions PL011**

    * Complete
    * UART 0
    * on `/dev/ttyAMA0`

  * **mini UART**

    * reduced
    * UART 1
    * on `/dev/ttyS0`
    * smaller FIFOs
    * baud rate linked to the CPU clock
    * No parity bit
    * among others

  * **Annotations for Pi Zero**

    * `miniuart-bt` switches the Bluetooth function to use the mini UART, and makes the first PL011 (UART0) the primary UART

    * configuei o arquivo com `enable_uart=1 setting core_freq=250 dtoverlay=pi3-disable-bt`

      

# Basic usage

* Show pinout: `pinout`
  
* Python programming
  *  https://learn.sparkfun.com/tutorials/python-programming-tutorial-getting-started-with-the-raspberry-pi/all#programming-in-python	

## SSH access

* *headless*

* https://www.losant.com/blog/getting-started-with-the-raspberry-pi-zero-w-without-a-monitor
  
* Rapibian lite SO

* `ssh pi@raspberrypi.local`

* Need to configure before boot. To <u>my</u> wifi:

  ```
  country=US
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  
  network={
  ssid="Net Virtua-35"
  scan_ssid=1
  psk="6223233240"
  key_mgmt=WPA-PSK
  }
  ```

## Multithread

"threading" or the "multitasking" module.

## Time

 fixed cycle time of 1s using the pygame clock.tick() function

# Camera

* Most cameras requires 250mA when running
* **Setting up the camera software**
  * `sudo raspi-config`
  *  Interfacing Options
  * Camera
  * https://www.raspberrypi.org/documentation/configuration/camera.md
* **Picamera library**
  * To Python
  * <camera-obj> = PiCamera()
  * <camera-obj>.start_preview()
  * <camera-obj>.stop_preview()
  * <camera-obj>.capture(<path>)
    * Photo
    * sleep for at least two seconds before capturing an image
    * capturing to a numpy array: https://picamera.readthedocs.io/en/release-1.12/recipes2.html#capturing-to-a-numpy-array
  * <camera-obj>.start_recording(<path>)
    * What formats? just .h264?
  * processing options
    * <camera-obj>.rotation = <angle>
  * motion vector
    * outputting the motion vector estimates that the camera’s H.264 encoder calculates while generating compressed video
    * https://picamera.readthedocs.io/en/release-1.10/recipes2.html#recording-motion-vector-datasmbus
* **Sending stream**
  * To youtube
    * https://www.filipeflop.com/blog/imagens-live-stream-no-youtube-com-raspberry-pi-zero-w/
    * avconv package: para fazer o stream para o YouTube
  * mjpg-streamer
    * https://medium.com/home-wireless/headless-streaming-video-with-the-raspberry-pi-zero-w-and-raspberry-pi-camera-38bef1968e1
* **Raspstill package**
  * photos
  * `raspistill -v -o image.jpg`
* **raspivid package**
  
  * videos



## OpenCV

* This section is just about raspbian particularities to openCV

* To openCV itself, I have other files

* I have raspbian strech lite, witn openCV 3.3.0 and Python 3.5 

* **Dependencies: apt codecs/etc**

  * libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
  * libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
  * libxvidcore-dev libx264-dev
  * libgtk2.0-dev libgtk-3-dev
  * libatlas-base-dev gfortran
  * libgl1-mesa-dev libxt-dev libosmesa-dev
  ```
  sudo apt install -y libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libgtk2.0-dev libgtk-3-dev libatlas-base-dev gfortran libgl1-mesa-dev libxt-dev libosmesa-dev
  ```
  
* **Dependencies: apt others**

  * pkg-config

  * build-essential

  * git

  * cmake

  * python3-dev

    ```
    sudo apt install -y pkg-config build-essential git cmake python3-dev
    ```

* **Depenndency: VTK**

  * ==is this really necessary, to work headless? It was not needed in Tutorial 3==
  * VTK=low lever graphic library
  * Used Cmake [https://cmake.org/files/v3.12/](https://cmake.org/files/v3.12/) ([tutorial](http://osdevlab.blogspot.com/2015/12/how-to-install-latest-cmake-for.html)) with VTK [16.0](https://gitlab.kitware.com/help/user/project/releases/index)
  * Install ccmake (if you want to use a ‘graphical’ configuration for the make, but I did by command line): `sudo apt-get install cmake-curses-gui`
  * [Tutorial 1](https://blog.kitware.com/raspberry-pi-likes-vtk/): comprehensible, but outdated version
  * [Tutorial 2](https://vtk.org/Wiki/VTK/Building/Linux): from docs
    ` git clone https://gitlab.kitware.com/vtk/vtk.git ~/bin/VTK`
    `mkdir ~/bin/VTK-build`
    `cd ~/bin/VTK-build`
    `~/bin/VTK-build $ cmake -DCMAKE_BUILD_TYPE:STRING=Release -DBUILD_EXAMPLES=FALSE -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=ON ~/bin/VTK`

* **venv**

  * Had to do [this](https://stackoverflow.com/questions/26215790/venv-doesnt-create-activate-script-python3) workarud

  * `apt-get install python3-venv`

  * Create env: `python3 -m venv /home/pi/.virtualenvs/cv`

  * To activate: `source .virtualenvs/cv/bin/activate`

  * To deadtivate, just `deactivate`

  * If you wanna follow tutorial 3, do on ~/.profile:

    ```
    # virtualenv and virtualenvwrapper
    export WORKON_HOME=$HOME/.virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    source /home/pi/.virtualenvs/cv/bin/activate
    ```

* **dependencies pip3**

  * If using venv, this should be done with the enviroment activated

  * numpy

    ```
    sudo pip3 install numpy
    ```

* **Installing**

  * [tutorial 1](https://tutorials-raspberrypi.com/installing-opencv-on-the-raspberry-pi/)
  * [Tutorial 3](https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/)  (TOP, e funciona)

  ~/bin/opencv $: 

  ``` bash
  cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_PYTHON_EXAMPLES=ON -D INSTALL_C_EXAMPLES=ON -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules -D BUILD_EXAMPLES=ON ..
  
  ```

  ~/bin $: `sudo make install && sudo ldconfig`

## Keras

* 