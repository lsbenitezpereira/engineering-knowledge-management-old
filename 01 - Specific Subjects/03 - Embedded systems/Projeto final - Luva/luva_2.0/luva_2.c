/* ----------------------------------------------------------------------------
 * File:			Luva_2.c
 * Project:			Measure tilt angle with accelerometer and gyroscope
 * Author:			Leonardo Santiago Benitez Pereira
 * Version:			2.0
 * Last modified:	25/11/2014
 * -------------------------------------------------------------------------- */
 
// System definitions
#define F_CPU 16000000UL
#define MCU_8_BITS


// Header files
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "LS_i2c_master.h"
#include "LS_ATmega328.h"
#include "LS_defines.h"
#include "L3G4200D.h"
#include "ADXL345.h"

USART_RECEIVER_BUFFER_FUNCTION_HANDLER

//Projet definitions
#define GLOVE_ADRESS	01+

#define READ_PIPE		0
#define READ_ADRESS		1
#define READ_DATA		2
#define PROCESS_DATA	3

// New type definitions
typedef struct gyroscope_t {
	int16 x, y, z;         		    //row read
	int16 x_high, y_high, z_high;	//for calibration
	int16 x_low, y_low, z_low;
} gyroscope_t;

typedef struct accelerometer_t {
	int16 x,y,z;					//row read
	int16 last_x, last_y, last_z;
	int16 angle_z, angle_y;			//calculated angle
} accelerometer_t;

typedef struct system_flags_t{
	uint8 usart_state	: 2;
	uint8 send_data		: 1;
	uint8 process_data	: 1;
	uint8 cd			: 5;
} system_flags_t;


//Function declaration
void L3G4200D_calibrate(float x_gain, float y_gain, float z_gain);
void serial_flush();


// Global variables
system_flags_t system_flags;
gyroscope_t gyro;
accelerometer_t accel;
float time;
volatile uint16 extensometer;
int16 real_angle_y, real_angle_z;


//---------------------------------------------------------------------------------------------
// Main function ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

int main(){

	int8 buffer[30];
	uint8 count = 0;

	//Port configuration
	setBit(DDRB, PB5);
	
	// Variable initialization
	system_flags.usart_state = READ_PIPE;
	accel.last_x = 0;
	accel.last_y = 0;
	accel.last_z = 0;
	real_angle_y = 0;
	real_angle_z = 0;
	
	// USART configuration
	usart8DataBits();
	usartSingleStopBit();
	usartParityNone();
	usartEnableReceiver();
	usartEnableTransmitter();
	usartAsynchronousMode();
	usartActivateReceptionCompleteInterrupt();
	usartEnableReceiverBuffer();
	usartInit(38400);
	usartStdio();

	//Global Interrupt enable
	sei();

	// I�C begin
	i2cMasterInit();
	
	//ADXL345 configuration
	ADXL345_output_rate_3200();
	ADXL345_full_resolution();
	ADXL345_low_power_off();
	ADXL345_measure_on();
	ADXL345_range_2g();
	
	//L3G4200D configuration
	L3G4200D_output_rate_800();
	L3G4200D_normal_mode();
	L3G4200D_range_2000();
	L3G4200D_calibrate(0.85, 1.0, 0.9);
	
	//ADC configuration 
	adcClearInterruptRequest();
	adcDisableDigitalInput0();
	adcEnableAutomaticMode();
	adcClockPrescaler128();
	adcSelectChannel(ADC0);
	adcActivateInterrupt();
	adcReferenceAvcc();
	adcEnable();

	//Timer1 configuration
	timer1OC1AOff();	
	timer1OC1BOff();	
	timer1NormalMode();
	timer1SetCounterValue(0);
	timer1ClockPrescaller64();
	
	while(1){
	
		/*** READ SERIAL  ***/
		while(!usartIsReceiverBufferEmpty()){
			switch(system_flags.usart_state){
			
			//ignores anything before the pipe
			case READ_PIPE:									
				buffer[0] = usartGetDataFromReceiverBuffer();
				if(buffer[0] == '|'){
					system_flags.usart_state = READ_ADRESS;
					printf("\r\n\r\n");
					printf("pipe recebido\r\n");
				}
				break;
			
			//if the adress is right (01), continue reading
			case READ_ADRESS:
				buffer[0] = usartGetDataFromReceiverBuffer();
				buffer[1] = usartGetDataFromReceiverBuffer();
				buffer[2] = usartGetDataFromReceiverBuffer();
				buffer[3] = '\0';
				if(strcmp(buffer, "01+") == 0){
					printf("endere�o correto\r\n");
					system_flags.usart_state = READ_DATA;
					count = 0;
				}
				else {
					system_flags.usart_state = READ_PIPE;
					printf("endere�o ERRADO\r\n");
				}
					
				break;
				
			//looks for "\n" to process the package, or "|" to back to the top
			case READ_DATA:
				buffer[count] = usartGetDataFromReceiverBuffer();
				printf("caracter = %c\r\n", buffer[count]);
				
				if(buffer[count] == '\n'){
					buffer[count] = '\0';
					count = 0;
					printf("line feed recebido\r\n");
					system_flags.process_data = 1;
					system_flags.usart_state = READ_PIPE;
					
									/* LEANDRO*/
					//se eu dou flush, as vezes ele l� uma string pela metade
					//se nao dou, aparentemente o problema se torna mais frequencia
					//????????????
					//serial_flush();
				}
				
				else if(buffer[count] == '|')
					system_flags.usart_state = READ_ADRESS;
					
				else
					count++;
					
				break;
			}
		}	
		
		/*** PROCESS SERIAL BUFFER ***/
		if(system_flags.process_data){
				system_flags.process_data = 0;

				if(strcmp(buffer, "mv") == 0){
					printf("recebeu mv!!!!!!\r\n");
					system_flags.send_data = 1;	
				}
				
				if(strcmp(buffer, "START") == 0)
					printf("leitura iniciada\r\n");
				
				if(strcmp(buffer, "STOP") == 0)
					printf("leitura finalizada\r\n");
				
				
				system_flags.usart_state = READ_PIPE;
		}
			
		/*** READ SENSORS ***/
		L3G4200D_get_angular_speed(&gyro.x, &gyro.y, &gyro.z);		//in degrees per seconds(x10)
		ADXL345_get_acceleration(&accel.x, &accel.y, &accel.z);		//In Gravity(x1000)
		adcStartConversion();
		
		/*** PRE-FILTER ***/
		//low pass filter for accelerometer
		accel.x = 0.4*accel.x + 0.6*accel.last_x;
		accel.last_x = accel.x;
		
		accel.y = 0.4*accel.y + 0.6*accel.last_y;
		accel.last_y = accel.y;
		
		accel.z = 0.4*accel.z + 0.6*accel.last_z;
		accel.last_z = accel.z;
		
		//threshold of gyro
		if(gyro.x > gyro.x_high)
			gyro.x -= gyro.x_high;
		else if(gyro.x < gyro.x_low)
			gyro.x -= gyro.x_low;
		else
			gyro.x = 0;
		
		if(gyro.y > gyro.y_high)
			gyro.y -= gyro.y_high;
		else if(gyro.y < gyro.y_low)
			gyro.y -= gyro.y_low;
		else
			gyro.y = 0;
		
		if(gyro.z > gyro.z_high)
			gyro.z -= gyro.z_high;
		else if(gyro.z < gyro.z_low)
			gyro.z -= gyro.z_low;
		else
			gyro.z = 0;	
		
		/*** CONVERT TO DEGREES ***/		
		accel.angle_y = atan2(((float)accel.x), sqrt(square(((float)accel.y)) + square(((float)accel.z)))) * 1800.0/M_PI;	//frontal
		accel.angle_z = atan2(((float)accel.y), sqrt(square(((float)accel.x)) + square(((float)accel.z)))) * 1800.0/M_PI;	//lateral
		
		/*** COMPLEMENTARY FILER ***/
		time = (timer1GetCounterValue())*4.0/1000000.0;
		timer1SetCounterValue(0);
		
		gyro.x*=-1;
		
		real_angle_y = 0.90*(real_angle_y + gyro.y*time) + 0.1*accel.angle_y;
		real_angle_z = 0.90*(real_angle_z + gyro.x*time) + 0.1*accel.angle_z;
		
		/*** USART OUT ***/
		/* ---------------------------------------------
		*	Angle						degrees	(x10) 
		*	Angular speed 				dps		(x10)
		*	Time since last read		s		(float)
		*	Extensometer				row		(uint16)
		*
		*	Real angle(accel+giro*T)	degrees	(x10)
		* ------------------------------------------- */
		
		if(system_flags.send_data){

			system_flags.send_data = 0;
			usartStdio();
			//printf("%u;%u | Fz=%d	Fz=%d	|	Gx=%d	Gy=%d	Gz=%d\r\n", (uint16)(time*1000), extensometer, accel.angle_z, accel.angle_y, gyro.x, gyro.y, gyro.z);
			printf("%04d;%04d;%04u\n", real_angle_z+900, real_angle_y+900, extensometer);
		}
	}
	return 0;
}

//---------------------------------------------------------------------------------------------
// ADC Interrupt ------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

ISR(ADC_vect)
{
	extensometer = ADC;
}

//---------------------------------------------------------------------------------------------
// User Functions -----------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

void L3G4200D_calibrate(float x_gain, float y_gain, float z_gain){
	uint8 count = 0;

	for(count = 0; count < 100; count++){
		L3G4200D_get_angular_speed(&gyro.x, &gyro.y, &gyro.z);   // read row angular speed in md/s

		if(gyro.x > gyro.x_high)
			gyro.x_high = gyro.x;
		else if(gyro.x < gyro.x_low)
			gyro.x_low = gyro.x;
		  
		if(gyro.y > gyro.y_high)
			gyro.y_high = gyro.y;
		else if(gyro.y < gyro.y_low)
			gyro.y_low = gyro.y;
		  
		if(gyro.z > gyro.z_high)
			gyro.z_high = gyro.z;
		else if(gyro.z < gyro.z_low)
			gyro.z_low = gyro.z;
	}
	
	count = 0;
  
	gyro.x_high *= x_gain;
	gyro.y_high *= y_gain;
	gyro.z_high *= z_gain;
	
	gyro.x_low *= x_gain;
	gyro.y_low *= y_gain;
	gyro.z_low *= z_gain;
  
}


void serial_flush()
{
	uint8 aux;
	//usartClearReceptionBuffer();
	
	while(!usartIsReceiverBufferEmpty())
		aux = usartGetDataFromReceiverBuffer();
		
	return;
}
