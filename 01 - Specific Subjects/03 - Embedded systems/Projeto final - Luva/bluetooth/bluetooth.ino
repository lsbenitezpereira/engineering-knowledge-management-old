#include <SoftwareSerial.h>

#define bt_power 12
#define bt_key_power 11
#define indication_led 13

SoftwareSerial BT(9, 10); // RX | TX

void setup()
{
  // set the pins to OUTPUT
  pinMode(bt_power, INPUT);  
  pinMode(bt_key_power, OUTPUT);
  pinMode(indication_led, OUTPUT);
  
  // set the pins to LOW
  digitalWrite(bt_key_power, LOW);
  digitalWrite(indication_led, LOW);
  
  /************************************************
  Setting the pins to low is important because 
  in order for us to get into AT mode the key pin
  has to be set to Ground FIRST. Many tutorials out
  there fail to mention this important fact and 
  therefore many people have problems with getting 
  into the AT mode of the HC-05
  ************************************************/
  
  delay(100);
  digitalWrite(bt_key_power, HIGH);
  delay(100);
  
  // now power on the BT
  //digitalWrite(bt_power, HIGH);
   digitalWrite(indication_led, HIGH);
   while(!digitalRead(bt_power));
   digitalWrite(indication_led, LOW);
   
  // start serial
  Serial.begin(38400);
  BT.begin(38400);
  
  Serial.write("Type AT commands  \n\n");
 }


void loop()
{ 
  // Bluetooth to Monitor
  if (BT.available()){
    Serial.println(BT.read());
  }
  
  //  Monitor to Bluetooth
  if (Serial.available()){
    BT.write(Serial.read());
  }  
}
