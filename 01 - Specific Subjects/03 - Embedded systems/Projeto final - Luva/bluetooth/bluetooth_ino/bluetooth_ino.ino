#include <SoftwareSerial.h>

SoftwareSerial BT(9, 10); // RX | TX

#define bt_key_power 11
#define led 13

int caractere;
 
void setup() {
 // set the pins to OUTPUT
 pinMode(bt_key_power, OUTPUT);
 pinMode(led,OUTPUT);
 
 // set the pins to LOW
 digitalWrite(bt_key_power, LOW);
 digitalWrite(led, LOW);
  
 BT.begin(57600);
}
 
void loop() {
  if (BT.available()) {
    caractere = BT.read();
    if(caractere == '1') {
      digitalWrite(led,HIGH);
      BT.println("OK, led ligado.");
    }
    if (caractere == '0') {
      digitalWrite(led,LOW);
      BT.println("OK, led desligado.");
    }
  }
 
  delay(100);
}
