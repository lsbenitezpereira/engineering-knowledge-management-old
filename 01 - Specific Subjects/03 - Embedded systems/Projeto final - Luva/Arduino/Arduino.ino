/* -----------------------------------------------------------------------------
 * File:			Luva.ino
 * Project:			Measure tilt angle with gyro an accelerometer
 * Author:			Leonardo Santiago Benitez Pereira
 * Last modified:	        09/11/2014
 * -------------------------------------------------------------------------- */
 
// Header files
#include <Wire.h>
#include <math.h>

// Project definition
#define ACCEL_DATARATE 1000
#define ACCEL_SCALE 2
#define N_AVERAGE 5

#define GYRO_SCALE 2000

// Type definitions
struct gyroscope {
  long x, y, z;              //row read
  long xHigh, yHigh, zHigh;  //for calibration
  long xLow, yLow, zLow;
  int time = 0;              //time used in integration
  int lastTime = 0;
  double angleX, angleY , angleZ;
};

struct accelerometer {
  double x,y,z;                                                //row read
  double lastX[N_AVERAGE], lastY[N_AVERAGE], lastZ[N_AVERAGE]; //for calibration
  double angle_z, angle_y;                                     //calculated angle
};


// Global variables
gyroscope gyro;
accelerometer accel;
double realAngleZ = 0;
double realAngleY = 0;
double lastRealAngleZ = 0;
double lastRealAngleY = 0;
char buff;
int count;

void setup(){
  Serial.begin(9600);
  gy80_powerOn();
 
  adxl345_setRate(ACCEL_DATARATE);
  adxl345_SetRange(ACCEL_SCALE);
 
  setupL3G4200D(GYRO_SCALE); // Configure L3G4200  - 250, 500 or 2000 deg/sec

  delay(1500); //wait for the sensor to be ready 
  
  calibrateGyro();
}


void loop(){  
  /*** READ SENSORS ***/
  readAccel(&accel.x, &accel.y, &accel.z, ACCEL_SCALE);    //read aceleration in gravity(g)
  getGyroValues(&gyro.x, &gyro.y, &gyro.z, GYRO_SCALE);   // read angular speed in mº/s
  
  
  /*** PRE-FILTER ***/
  //threshold of gyro
  if(gyro.x > gyro.xHigh)
    gyro.x -= gyro.xHigh;
  else if(gyro.x < gyro.xLow)
    gyro.x -= gyro.xLow;
  else
    gyro.x = 0;
    
  if(gyro.y > gyro.yHigh)
    gyro.y -= gyro.yHigh;
  else if(gyro.y < gyro.yLow)
    gyro.y -= gyro.yLow;
  else
    gyro.y = 0;
    
  if(gyro.z > gyro.zHigh)
    gyro.z -= gyro.zHigh;
  else if(gyro.z < gyro.zLow)
    gyro.z -= gyro.zLow;
  else
    gyro.z = 0;
    
  //averange value from accelerometer
  for(count = N_AVERAGE; count<0; count--){
    accel.lastX[count-1] = accel.lastX[count];
    accel.lastY[count-1] = accel.lastY[count];
    accel.lastZ[count-1] = accel.lastZ[count];
  }
    
  accel.lastX[N_AVERAGE] = accel.x;
  accel.lastY[N_AVERAGE] = accel.y;
  accel.lastZ[N_AVERAGE] = accel.z; 
  
  for(count = N_AVERAGE; count<0; count--){
    accel.x += accel.lastX[count];
    accel.y += accel.lastY[count];
    accel.z += accel.lastZ[count];
  }
  accel.x /= N_AVERAGE;
  accel.y /= N_AVERAGE;
  accel.z /= N_AVERAGE;
   
   
  /*** CONVERT TO DEGREES ***/
  //get the angles from ACCELETOMETER
  accel.angle_y = atan2(accel.x, sqrt(accel.y*accel.y + accel.z*accel.z)) * 180/M_PI;    //frontal
  accel.angle_z = atan2(accel.y, sqrt(accel.x*accel.x + accel.z*accel.z)) * 180/M_PI;    //lateral
  
  //Calculate the time for integration
  gyro.lastTime = gyro.time;
  gyro.time = millis();
   
  //get the angles from GYROSCOPE
  gyro.angleX += (double)gyro.x*((double)(gyro.time - gyro.lastTime)/1000000.0);    //estacionaria
  gyro.angleY += (double)gyro.y*((double)(gyro.time - gyro.lastTime)/1000000.0);    //frontal
  gyro.angleZ += (double)gyro.z*((double)(gyro.time - gyro.lastTime)/1000000.0);    //lateral
 
  
  /*** COMPLEMENTARY FILER ***/
  realAngleZ = 0.98*(lastRealAngleZ + (double)gyro.z*((double)(gyro.time - gyro.lastTime)/1000000.0)) + 0.02*(accel.angle_z*-1);
  lastRealAngleZ = realAngleZ;
  realAngleY = 0.5*(lastRealAngleY + (double)gyro.y*((double)(gyro.time - gyro.lastTime)/1000000.0)) + (1-0.5)*accel.angle_y;
  lastRealAngleY = realAngleY;
 

   //prints
	
  Serial.print("Az=");
  Serial.print(accel.z, 0);
  Serial.print("     Gz=");
  Serial.print(gyro.z, 0);
  Serial.print("     Fz=");
  Serial.println(realAngleZ, 0);
}


// -----------------------------------------------------------------------------
// Register Name ---------------------------------------------------------------
#define ADXL345_BW_RATE 0x2c
#define ADXL345_POWER_CTL 0x2d
#define ADXL345_DATA_FORMAT 0x31
#define ADXL345_DATAX0 0x32

#define ADXL345_ERROR 0                  // indicates error is predent
#define ADXL345_READ_ERROR 1             // problem reading accel
#define ADXL345_TO_READ (6)              // num of bytes we are going to read each time (two bytes for each axis)

#define CTRL_REG1 0x20			 //L3G4200D Control registers
#define CTRL_REG2 0x21
#define CTRL_REG3 0x22
#define CTRL_REG4 0x23
#define CTRL_REG5 0x24

#define ADXL345_DEVICE	0x53             // ADXL345 device address
#define L3G4200D_DEVICE	105              //I2C address of the L3G4200D

// -----------------------------------------------------------------------------
// Global variables ------------------------------------------------------------
 byte _buff[6];                          //6 bytes buffer for saving data read from the device
 byte error_code;                        // Initial state
 bool status;                            // set when error occurs 
 
// -----------------------------------------------------------------------------
// Public functions ------------------------------------------------------------

//Turning on the ADXL345
void gy80_powerOn() {
    Wire.begin();    // join i2c bus (address optional for master)
    
    //writeRegister(ADXL345_DEVICE, ADXL345_POWER_CTL, 0);   
    //writeRegister(ADXL345_DEVICE, ADXL345_POWER_CTL, 16);
    writeRegister(ADXL345_DEVICE, ADXL345_POWER_CTL, 8); 
}

//inicializate the L3G4200D
int setupL3G4200D(int scale){
  // Enable x, y, z and turn off power down:
  writeRegister(L3G4200D_DEVICE, CTRL_REG1, 0b00001111);

  // If you'd like to adjust/use the HPF, you can edit the line below to configure CTRL_REG2:
  writeRegister(L3G4200D_DEVICE, CTRL_REG2, 0b00000000);

  // Configure CTRL_REG3 to generate data ready interrupt on INT2
  // No interrupts used on INT1, if you'd like to configure INT1
  // or INT2 otherwise, consult the datasheet:
  writeRegister(L3G4200D_DEVICE, CTRL_REG3, 0b00001000);

  // CTRL_REG4 controls the full-scale range, among other things:
  if(scale == 250){
    writeRegister(L3G4200D_DEVICE, CTRL_REG4, 0b00000000);
  }else if(scale == 500){
    writeRegister(L3G4200D_DEVICE, CTRL_REG4, 0b00010000);
  }else{
    writeRegister(L3G4200D_DEVICE, CTRL_REG4, 0b00110000);
  }

  // CTRL_REG5 controls high-pass filtering of outputs, use it
  // if you'd like:
  writeRegister(L3G4200D_DEVICE, CTRL_REG5, 0b00000000);
}

//select the datarate of acelerometer
void adxl345_SetRange(int val) {
    byte _s;
    byte _b;
    
    switch (val) {
        case 2: 
            _s = B00000000; 
            break;
        case 4: 
            _s = B00000001; 
            break;
        case 8: 
            _s = B00000010; 
            break;
        case 16: 
            _s = B00000011; 
            break;
        default: 
            _s = B00000000;
    }
    readFrom(ADXL345_DATA_FORMAT, 1, &_b);
    _s |= (_b & B11101100);
    writeRegister(ADXL345_DEVICE, ADXL345_DATA_FORMAT, _s);
}

// Sets the range setting, possible values are: 2, 4, 8, 16
void adxl345_setRate(double rate){
    byte _b,_s;
    int v = (int) (rate / 6.25);
    int r = 0;
    while (v >>= 1)
    {
        r++;
    }
    if (r <= 9) { 
        readFrom(ADXL345_BW_RATE, 1, &_b);
        _s = (byte) (r + 6) | (_b & B11110000);
        writeRegister(ADXL345_DEVICE, ADXL345_BW_RATE, _s);
    }
}

//return the datarate of acelerometer
double adxl345_getRate(){
    byte _b;
    readFrom(ADXL345_BW_RATE, 1, &_b);
    _b &= B00001111;
    return (pow(2,((int) _b)-6)) * 6.25;
}

//state 0 to normal mode, 1 to power-save operation
void adxl345_LowPower(bool state) { 
    setRegisterBit(ADXL345_BW_RATE, 4, state); 
}



// Reads the acceleration into three variable x, y and z
void readAccel(double *x, double *y, double *z, int range) {
    readFrom(ADXL345_DATAX0, ADXL345_TO_READ, _buff); //read the acceleration data from the ADXL345
    
    // each axis reading comes in 10 bit resolution, ie 2 bytes. Least Significat Byte first!!
    // thus we are converting both bytes in to one int
    *x = (((int)_buff[1]) << 8) | _buff[0];  
    *y = (((int)_buff[3]) << 8) | _buff[2];
    *z = (((int)_buff[5]) << 8) | _buff[4];
    
    //Convert the row read axis aceletarion into Gravity(g) unit
/*    switch(range){
    case 2:
      *x = (*x)/256;
      *y = (*y)/256;
      *z = (*z)/256;
      break;
  }
*/ 
}

//return the giros read in º/s
void getGyroValues(long *x, long *y, long *z, int scale){

  //read the row aceleration data
  byte xMSB = readRegister(L3G4200D_DEVICE, 0x29);
  byte xLSB = readRegister(L3G4200D_DEVICE, 0x28);
  *x = ((xMSB << 8) | xLSB);

  byte yMSB = readRegister(L3G4200D_DEVICE, 0x2B);
  byte yLSB = readRegister(L3G4200D_DEVICE, 0x2A);
  *y = ((yMSB << 8) | yLSB);

  byte zMSB = readRegister(L3G4200D_DEVICE, 0x2D);
  byte zLSB = readRegister(L3G4200D_DEVICE, 0x2C);
  *z = ((zMSB << 8) | zLSB);
  
  //convert to mº/s
/*  switch(scale){
  case 2000:
    *x = (*x)*70;
    *y = (*y)*70;
    *z = (*z)*70;
    break;
  }
*/
}

void calibrateGyro(){
  for(int i = 0; i < 100; i++){
    getGyroValues(&gyro.x, &gyro.y, &gyro.z, GYRO_SCALE);   // read row angular speed in mº/s

    if(gyro.x > gyro.xHigh)
      gyro.xHigh = gyro.x;
    else if(gyro.x < gyro.xLow)
      gyro.xLow = gyro.x;
      
    if(gyro.y > gyro.yHigh)
      gyro.yHigh = gyro.y;
    else if(gyro.y < gyro.yLow)
      gyro.yLow = gyro.y;
      
    if(gyro.z > gyro.zHigh)
      gyro.zHigh = gyro.z;
    else if(gyro.z < gyro.zLow)
      gyro.zLow = gyro.z;
  }
  
  gyro.xHigh *= 0.9;
  gyro.yHigh *= 0.9;
  gyro.zHigh *= 0.9;
  
  gyro.xLow *= 0.9;
  gyro.yLow *= 0.9;
  gyro.zLow *= 0.9;
  
}


// -----------------------------------------------------------------------------
// Private functions -----------------------------------------------------------

// Reads num bytes starting from address register on device in to _buff array
void readFrom(byte address, int num, byte _buff[]) {
    Wire.beginTransmission(ADXL345_DEVICE);              // start transmission to device 
    Wire.write(address);                                 // sends address to read from
    Wire.endTransmission();                              // end transmission
    
    Wire.beginTransmission(ADXL345_DEVICE);		 // start transmission to device
    Wire.requestFrom(ADXL345_DEVICE, num);		 // request 6 bytes from device
    
    int i = 0;
    while(Wire.available())                              // device may send less than requested (abnormal)
    { 
        _buff[i] = Wire.read();                          // receive a byte
        i++;
    }
    if(i != num){
        status = ADXL345_ERROR;
        error_code = ADXL345_READ_ERROR;
    }
    Wire.endTransmission();                              // end transmission
}

// Writes val to address register on device
void writeRegister(int deviceAddress, byte address, byte val) {
    Wire.beginTransmission(deviceAddress); // start transmission to device 
    Wire.write(address);       // send register address
    Wire.write(val);         // send value to write
    Wire.endTransmission();     // end transmission
}

//Set one bit of an register
void setRegisterBit(byte regAdress, int bitPos, bool state) {
    byte _b;
    readFrom(regAdress, 1, &_b);
    if (state) {
        _b |= (1 << bitPos);			        // forces nth bit of _b to be 1. all other bits left alone.
    } 
    else {
        _b &= ~(1 << bitPos);				// forces nth bit of _b to be 0. all other bits left alone.
    }
    writeRegister(ADXL345_DEVICE, regAdress, _b); 
}

int readRegister(int deviceAddress, byte address){

    int v;
    Wire.beginTransmission(deviceAddress);
    Wire.write(address); // register to read
    Wire.endTransmission();

    Wire.requestFrom(deviceAddress, 1); // read a byte

    while(!Wire.available()) {
        // waiting
    }

    v = Wire.read();
    return v;
}
