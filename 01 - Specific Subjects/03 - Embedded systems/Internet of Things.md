# Conceptualization

* also known as the Internet of Everything
* collections of real-world, internet-connected devices that can convey  data from the world around them to other devices in the network
* Transforms objects from being traditional to smart, exploiting “new” technlogies, like: Ubiquitous and pervasive computing; embedded systems; communication technologies; sensor networks; Internet protocols and applications
* Is primarily about data and gaining actionable insights from that data
* connecting objects together is not an objec-
  tive by itself, but gathering intelligence from such objects to enrich products and
  services is
* ITU-T Y.2060, Overview of the Internet of Things, provides a
  definition of IoT, terming it: “A global infrastructure for the Information Society,
  enabling advanced services by interconnecting (physical and virtual) things
  based on, existing and evolving, interoperable information and communication
  technologies.”
* **Main classes of services**
  * Identity-related Services
  * Information Aggregation Services: collect and summarize raw sensory
  * Collaborative-Aware Services: obtained data to make decision
  * Ubiquitous Services: provide that data anytime, to anyone and anywhere
* * 
* **History**
  * 1982, a soda
    machine at CMU became arguably the first Internet-connected appliance, announced
    by a broadly distributed email that shared its instrumented and interconnected story
    with the world
* **O2O Revolution**
  * online-to-offline
  * things that can’t be put in a cardboard box and shipped across country, like hot food, a ride to the bar, or a new haircut.
  * This is not exactly IoT, but it opens space to talk about OMO
* **OMO revolution**
  * online-merge-offline
  * [In the future, the IoT node] “will be so pervasive that it no longer makes sense to think  of oneself as ‘going online.’ When you order a full meal just by  speaking a sentence from your couch, are you online or offline? When  your refrigerator at home tells your shopping cart at the store that  you’re out of milk, are you moving through a physical world or a digital one?” Kai fuu lee
* **OT/IT convergence**
  * ?
  * Deterministic Networks (see in Computer Networks, Network Layer) are an important enabler of it
* **Machine-to-Machine communications**
  * The expansion of IP networks around the world has made machine to machine communication quicker and easier while using less power
  * <u>Standard - oneM2M</u>
    * standardize the
      common horizontal functions of the IoT Application Services layer under the
      umbrella of the oneM2M Partnership Project

## Other IoT stuffs

* companies:

![Figure 3: The Emerging IoT Value Chain](Images - Internet of Things/Slide2.png)

* standards landscape:

  ![image-20201226145149976](Images - Internet of Things/image-20201226145149976.png)

* Entity Sensor Management Information Base (MIB) as described by
  IETF RFC 3433:

  ![image-20201226141840863](Images - Internet of Things/image-20201226141840863.png)

## 4-layer arquiteture

* From Ammar Rayes (IoT book)
* IoT devices (things)
* IoT network (infrastructure transporting the
  data)
* IoT Services Platform (software connecting the things with applications and
  providing overall management)
* IoT applications (specialized business-based applications such as customer relation management (CRM), Accounting and Billing, and Business Intelligence applications)

## 5-layer architecture

* **perception layer**
  * or *Objects*
  *  functionalities such as querying location, temperature, weight, motion, vibration, etc
  * Standardized plug-and-play mechanisms need to be used
  *  digitizes and transfers data to the Object Abstraction layer through secure channels
* **network layer**
  * or *object abtraction*
  *  transfers the data to the Service Management layer through secure channels
  * Communication technologies: RFID, 3G, GSM, UMTS, WiFi, Bluetooth Low Energy, infrared,
    ZigBee, etc.
  * Computing and data management begins here (==or is totally done here?==)
* **service management**
  * or *Middleware*
  *  pairs a service with its requester based on addresses and names
  * processes received data, makes decisions, and delivers the required services over the network
    wire protocols
* **application layer**
  * provides the services requested by
    customers
  * interface by which end-users can interact with a device
  * control mechanisms of accessing data are also handled at this layer.
* **bussines layer**
  * or *management layer*
  * manages the overall IoT
    system activities and services.
  * build a business model, graphs, flowcharts, etc.
  *  design, analyze, implement, evaluate, monitor, and develop
    IoT system related elements.

## Domain independent technologies

* or *horizontal markets*
* Are the basic elements of IoT

![1566532440252](Images - Internet of Things/1566532440252.png)

* **Identification**
  *  Many identification methods are
    available for the IoT such as electronic product codes (EPC) and
    ubiquitous codes (uCode)
  * id $\neq$ address (one is the name, other the an protocol parameter) (identification methods are not globally unique, so addressing assists to uniquely identify objects.)
  * To many devices = need of large addresses (IPv6)
* **Sensing**
  * can be sensors, actuators or wearable sensing devices.
  * Single Board Computers (SBCs) integrated with sensors and
    built-in TCP/IP and security functionalities are typically used
    to realize IoT products
* **Communication**
  * connect heterogeneous objects together to deliver specific services
  * Tipicly require low energy consupmtion 
  * Technologies: WiFi, Bluetooth, IEEE
    802.15.4, Z-wave, and LTE-Advanced,  RFID, Near Field Com-
    munication (NFC) and ultra-wide bandwidth (UWB)
* **Computation**
  * Processing units (e.g., microcontrollers, microprocessors,
    SOCs, FPGAs) and software applications represent the “brain”
    and the computational ability of the IoT.
  *  hardware platforms: Arduino, UDOO, FriendlyARM, Intel Galileo, Raspberry PI, Gadgeteer,
    BeagleBone, Cubieboard, Z1, WiSense, Mulle, and T-Mote Sky

* **Semantics**
  * ability to extract knowledge to provide the required services
  * recognizing and analyzing data to make sense of the right decision to provide
    the exact service
  * the conventional analytic tools that rely on offline analysis are no longer interesting
  * Requires Semantic Web technologies
    * Resource Description Framework (RDF)
    * Web Ontology Language (OWL)
    * Efficient XML Interchange (EXI): converts XML messages to binary to reduce the needed bandwidth and minimize the required storage size
  

## Domain specific applications

* or *vertical markets*
* Some of those areas are deeply explored in specific files

* **Smart homes**
  * are required to have regular ineraction with their internal and external environments
  * Building Automation Systems (BAS)
    * easier and more convenient
    * Automatically open their garage
    * Prepare their coffee
    * monitor and control HVAC (Heating, Ventilation, and Air Conditioning) 
    * TVs and other appliances
  * Residential Energy Maganement Systems
    * ?
* **Smart grids**
  * improve and enhance the energy consumption of houses and buildings
  * helps power suppliers to control and man-
    age resources to provide power proportionally to the population
    increase
  *  enables energy pro-
    viders to improve their services to meet consumers’ needs
  *  reduces the potential failures,
    increases efficiency and improves quality of services.
* **Smart cities**
  * emergency response desasters (natural or not), where human decision making is difficult.
  * improve the quality of life in the city
  *  easier and more convenient for the residents to find information of interest
* **Vehicles**
  * or *Internet of Vehicles (IoV)*
  * make driving more reliable, enjoyable and efficient
* **School**
* **Market**
* **Industry**
  * computerizing robotic de-
    vices to complete manufacturing tasks with a minimal human
    involvement
  * produce products
    quickly and more accurately
  * control and monitor production ma-
    chines’ operations, functionalities, and productivity rate
  * 
* **Transportation network**
  * or *Intelligent transportation systems (ITS)* or **Transportation Cyber-Physical Systems (T-CPS)* 
  * reliability, efficiency, availability and safety of the transportation infrastructure
  * vehicle subsystem (consists of GPS, RFID reader, OBU, and
    communication)
  * station subsystem (road-side equipment)
  * monitoring center
  * security subsystem
* **Healcare**
  * mobile health (m-Health)
  * telecare
  * embedding sensors and actuators in patients
  * Wearable IoT
    * This extension can be found in the new generation of wearable sensors such as electrocardiograms, cadence and speed sensors, activity monitors such as HOP Extended from HOP Ubiquitous (www.hopu.eu), etc.
    * Communication: low energy
  * Examples
    * Masimo Radical-7:  monitors the patient’s status remotely and reports that
      to a clinical staff
    * IBM utilized RFID technology at one of OhioHealth’s hospitals to track hand washing after
      checking each patient 
* **Agriculture**
  * Precision livestock farming: continuous automatic real-time monitoring and control of production/reproduction, animal health and welfare, and the environmental impact of livestock production.

## Challenges

* **availability**
  * It will be there when I want it
  * must be realized in the hardware and
    software levels
  * solutions
    * provide redundancy for critical devices and services
* **reliability**
  * proper working of the system 
  * must be implemented in software and
    hardware throughout all the IoT layers
  * large-scale deployment of
    inexpensive sensors (i.e., with very limited processing capability, storage capacity,
    and limited energy) means that failures from various defects will not be uncommon.
* **mobility**
  * Connecting users with their desired services con-
    tinuously while on the move
* **performance**
  *  needs to continuously develop and improve its
    services to meet customers’ requirements
* **scalability**
  * ability to add new de-
    vices, services and functions for customers without negatively
    affecting the quality of existing services.
  * must be designed from the
    ground up to enable extensible services and operations 
* **interoperability**
  * need to handle a large number of heterogeneous things
    that belong to different platforms
  * ensure the delivery of services for all customers
    regardless of the specifications of the hardware platform that they
    use.
* **security**
  * play a significant role in all markets globally due to the sensitivity of consumers privacy
  * In heterogeneous networks as in the case
    of the IoT, it is not easy to guarantee the security and privacy of
    users
  * has not
    been considered in the standards is the distribution of the keys
    amongst devices 
  * solutions
    * standard  Smart Object Lifecycle Architecture for Constrained Environments (SOLACE)
* **management**
  * trillions of smart devices pre-
    sents service providers with daunting issues to manage the Fault,
    Configuration, Accounting, Performance and Security (FCAPS)
    aspects of these devices.
  * solutions
    *  Light-weight M2M (LWM2M) [136] is a standard
      that is being developed by the Open Mobile Alliance to provide
      interface between M2M devices and M2M Servers to build an
      application agnostic scheme for the management of a variety
      of devices

* 

# Others

## RDID

* radio-frequency identification

* mechanism to capture information pre-embedded into a “Tag”

* Chip or label attached to provide object’s identity. 

* The RFID reader transmits a query signal to the tag and receives reflected signal from the
  tag
  
* Range: 10 cm to 200 m

* Range: does not need to be within direct line of sight of the reader and can be
  read from a distance up to 12 m for passive ultrahigh frequency (UHF) system.
  Battery-powered tags typically have a reading range of 100 m.
  
* (+) durable (hard to destroy, can be hidden, etc)

* (+) can store much information (in comparrison with, for instance, a bar code)

* oes not required line-of-sight

* data may be encrypted

* RFID systems can read hundreds of tags simultaneously

* readers may interfere with each other

* EPC = electronic product C? = unique identifycation

* (-) susceptibility of the tags to jamming by
  blocking the RFID radio waves (for instance, by wrapping the tags with metallic
  material such as aluminum foil)
  
* ==is this link layer?==

* **Reader:** includes a signal generator and a receiver.;

* **Tag**: incudes a microcontroller and memory;

* The “answer” (communication from the tag to the reader) is sent with load modulation, turning the tag on and off in a given frequency?

* **Types of tags**
  
  * Active (batery)
  
  * passive (no batery)
  
  * semi-passive/active (use board power when needed)
  
    ![image-20210331152507077](Images - Internet of Things/image-20210331152507077.png)
  
    ![image-20210331155054023](Images - Internet of Things/image-20210331155054023.png)
  
* **Standards**

  * iso 18000-63:2013
  * iso 14443
  * publications from RAIN RFID
  * NFC forum

## Fog computing

* ==put this in the same place as Cloud?==
* Extend computing services to the edge devices of the network
* Embedded processing and cloudlets
* Similar to cloud, but closer to the data sources
* After processing and aggregating the col-
  lected data, the fog device forwards these data to the cloud domain
* (-) security is harder

![1566538517828](Images - Internet of Things/1566538517828.png)

![image-20201226140813389](Images - Internet of Things/image-20201226140813389.png)

* **Characteristics**
  * high mobility location awareness
  * geographical distribution
  * low latency, real-time interaction
  * predominance of wireless access.
  * extremely large number of nodes.
  * heterogeneity of resources, both the nodes’ and devices’
  * Computing, storage and networking are highly distributed

## Atividade Samir

Atividade Samir: IoT, redes e projeções do mercado

*Seguindo a temática apresentada no video do TED Talks indicado no roteiro de  estudos, a projeção de bilhões de dispositivos conectados à Internet,  enviando e recebendo dados para aplicações diversas, chamada de Internet das Coisas, não se concretizou. Existem algumas cidades que se  aproximam mais do conceito de cidades inteligentes mas a grande maioria  possue pouco ou nenhum serviço que se enquadre no conceito de Internet  das Coisas.*

*Dê sua opinião e disserte sobre possíveis limitações  das tecnologias de comunicação atuais (limitações ou inadequações nos  protocolos das camadas física, de enlace, de rede e transporte) que  possam explicar o fracasso das projeções da IoT.*



Vou organizar meus apontamentos em 3 clusters: hardware, comunicação e serviços. Cada problema individual parece perfeitamente solucionável, mas - como a implementação exitosa de aplicações IoT envolve - somados são capazes de explicar o fracasso das projeções da IoT

\> Hardware
Coisas inteligente são caras, e geralmente exigem a troca de coisas atuais. 
Itens de alto valor (exemplo: carros) estão ficando mais inteligentes, mas itens simples (exemplo: geladeira) continuam burros. Quando não há uma vantagem óbvia para a adoção de novas tecnologias (uma casa 100% conectada, por exemplo), apenas entusiastas embarcam na primeira onda, e IoT ainda está vencendo  essa resistência inicial.

\> Comunicação
Aplicações IoT geralmente envolvem um tráfego gigante de dados, de todos os tipos: multimídia, texto, dados que exigem alta segurança, dados que não exigem segurança nenhuma, etc. Os requisitos variam, e não há nenhuma solução one-size-fits-all (um protocolo de transporte que seja leve E garanta a entrega de pacotes, por exemplo), aumentando a complexidade das soluções e exigindo maior esforço de desenvolvimento.
Outra dificuldade é que a conectividade muitas vezes não é ubíqua, e soluções móveis que dependam de redes wireless muitas vezes enfrentam dificuldades. Entretanto, não me parece distante que alguma IEEE 802.666 esteja disponível em qualquer lugar da cidade, com alto troughput e serviço confiável.
Identificar cada "coisa" na rede também é um desafio, mas com a gradual adoção do  IPv6 esse problema vai provavelmente desaparecer logo.

\> Serviços
Sobre a vantagem competitiva que está sendo obtida pela China devido à colossal quantidade de dados que ela consegue coletar, o escrito Kay Fu Lee aponta:

"In China, companies tend to go 'heavy.' They don’t  want to just build  the platform—they want to recruit each seller,  handle the goods, run the delivery team, supply the scooters, repair  those scooters, and control  the payment. 
But that American  reluctance to go heavy has slowed adoption of mobile payments and will  hurt these companies even more in a data-driven AI  world. Data from mobile payments is currently generating the richest maps of consumer activity the world has ever known [...]" (Kay Fu Lee, AI superpowers, 2018)

Fu Lee manteve sua discussão à Estados Unidos e China, mas a abordagem americana de empreender com uma abordagem “leve” domina a maior parte do mundo ocidental. Somasse à isso à crescente regulamentação sobre uso de dados (por exemplo GDPR), e temos um cenário onde mesmo que as “coisas” se tornem mais inteligentes, ainda faltarão serviços que efetivamente justifiquem toda essa inteligência. Dados precisos sobre seus usuários são a chave para modelos de negócios feitos sob medida para algum público.

## IoT platforms

### NodeRed

plataforma de desenvolvimento

fluxo de bloquinhos

## Constrained devices

* IETF RFC 7228 defines a taxonomy of constrained devices based on the first two
  dimensions above, which recognizes three classes

  ![image-20201226111619975](Images - Internet of Things/image-20201226111619975.png)

## Others

* **QR code**
  * Quick Response Code
  * is the trademark for a type of matrix barcode.
* **SCADA**
  * connect differ-
    ent industrial devices to a common communication system
  * facilitate post-process and visualizing the data
  * Open source SCADA: http://www.scadabr.com.br/
* **Discovery of Entities, Services, and Location**
  * Process of identifying and transferring information regarding existing IoT entities and/or resources with their locations
  * registration: process of delivering the device infor- mation to the management entity (or to another server) in order for IoT devices to communicate and exchange information.
* **FOTA (Firmware Over-The-Air)**
  * download upgrades directly from the service provider.