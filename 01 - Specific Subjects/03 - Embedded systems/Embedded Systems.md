# Conceptualization

* dedicated function within a larger mechanical or electrical system
* **Board seachers**
  * https://www.hackerboards.com/home.php

## Automated testing

* :man_scientist: TCC do rafael reis, IFSC
* LAVA
  * for hardware
  * https://docs.lavasoftware.org/lava/index.html

## Open hardware projets

* ==where to put this?==
* Arduino
* GizmoSphere
* Tinkerforge
* BeagleBoard

# Sistemas Ubíquos

* Or *pervasive computing*
* ==Should I move it==
* computing is made to appear anytime and everywhere
* focus a lot in the interaction with humans, not only about the computing itself
* **Types of deviced**
  * This classification was proposed by Mark Weiser
  * Tabs: a wearable device that is approximately a centimeter in size
  * Pads: a hand-held device that is approximately a decimeter in size
  * Boards: an interactive larger display device that is approximately a meter in size
* **Types of devices 2**
  * This classification was proposed by Stefan Poslad 
  * Dust: from nanometres through micrometers to millimetres
  * Skin: smart surfaces (like fabrics)
  * Clay: three dimensional shapes as artefacts

## Real time systems

* A resposta deve ser dada dentro de um certo tempo
* Hard real time: qualquer resposta depois do tempo é inútil
* Soft real time: atrasos prejudicam o desempenho do produto, mas são aceitos