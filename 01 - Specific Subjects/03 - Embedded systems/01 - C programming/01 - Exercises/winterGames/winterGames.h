#ifndef WINTER_GAMES_INCLUDED
#define WINTER_GAMES_INCLUDED

typedef struct Nations nations_t;

nations_t* readFile(char* file, FILE** fp);
void printNations(nations_t* nation);
int closeFile(nations_t* nation, FILE** fp);

#endif	//WINTER_GAMES_INCLUDED
