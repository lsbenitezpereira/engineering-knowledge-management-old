/******************************************************************
 Title			Fie exercise 2
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	test of the winterGames l
 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "winterGames.h"

int main (void){
    nations_t* nations1,* nations2;
    FILE* fp1, *fp2;

    nations1 = readFile("winterGames1.csv", &fp1);
    nations2 = readFile("winterGames2.csv", &fp2);

    printf("NATIONS ONE FILE\n");
    printNations(nations1);
    printf("\n\nNATIONS TWO FILE\n");
    printNations(nations2);

    printf("\n\n\n");
    printf("Close result: %s\n", closeFile(nations1, &fp1)==0?"ok":"shit");
    printf("Close result: %s\n", closeFile(nations2, &fp2)==0?"ok":"shit");
    return 0;
}

