/********************************************************
 Title  		Arabic and roman numerals converter
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description 	It's a class exercise from Programing I.
 *******************************************************/

// NOTE: HOW THE CODE WAS TESTED
// Brute (and manual) force, simple as that :)
// I've tested and compared the conversion arabic>roman and roman>arabic of a lot of numbers, especially the most problematic ones (like 9, 49, 99, 104, 1000, etc)
// I've also tested the limits of my input range (verifying the numbers 1, 2, 3900, 3999, etc)

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <strings.h>

#define	STR_LEN_ROMAN	16                  // Max number is 3888 = MMMDCCCLXXXVIII (15 characters)

///Function prototype
int roman_to_arabic(char *, short int *);
void arabic_to_roman (int, char *);

///Main function
int main() {
	///Declaration
	short int D;                    // Decimal Arabic number
	char roman[STR_LEN_ROMAN]="";   // Roman number string

	/// Main loop
	while (1){
		/// Processing
		printf("What you want to do?");
		printf("\n<1> Arabic-to-roman conversion");
		printf("\n<2> Roman-to-arabic conversion");
		printf("\n<3> Get out of here\n");
		D = getche();

        if (D == '1') {
            strcpy(roman, "");
            printf("\nSay a integer number (1 to 3999):");
            scanf("%hi", &D);
            if (D<1) D=1;           // Verification
            if (D>3999) D=3999;
            arabic_to_roman(D, &roman[0]);
            printf("Roman equivalent = %s", roman); // Program output
        } else if (D == '2'){
            D=0;
            printf("\nRoman number (I to MMMCMXCIX):");
            fflush(stdin);
            gets(roman);
            if (roman_to_arabic(&roman[0], &D))
                printf("Arabic equivalent: %d", D);
            else
                printf("Invalid input, this is not a roman number");
        } else if (D == '3')
            break;
        else printf ("\nInvalid input, please try again\n");

        printf("\n\n--------------------------------------------\n\n");
	}
	system ("pause");
	return 0;
}

int roman_to_arabic (char *roman_str, short int *D){
    int i;
	int roman [STR_LEN_ROMAN];
	/// Converting each symbol to decimal
    for (i=0; roman_str[i]!='\0'; i++){
        switch (roman_str[i]){
            case 'i':
            case 'I':       roman[i]=1;         break;
            case 'v':
            case 'V':       roman[i]=5;         break;
            case 'x':
            case 'X':       roman[i]=10;        break;
            case 'l':
            case 'L':       roman[i]=50;        break;
            case 'c':
            case 'C':       roman[i]=100;       break;
            case 'd':
            case 'D':       roman[i]=500;       break;
            case 'm':
            case 'M':       roman[i]=1000;      break;
            default:        return 0;
        }
    }
    roman[i]=-2;       // End of vector mark: -2

    /// Sum everything
    for (i=1; roman[i]!=-2; i++){
        if (roman[i]>roman[i-1]) roman[i-1]*=-1;
        *D+=roman[i-1];
    }
    *D+=roman[i-1];

    /// Verification
    for (i=0; roman[i]!=-2; i++){
        if (roman[i]==roman[i+1] && roman[i+1]==roman[i+2] && roman[i+2]==roman[i+3]) return 0; // More than 3 equals
        if (roman[i]==-roman[i+1]) return 0;                                                    // More than one sequent subtraction
        if (roman[i]<0){
            if (roman[i]==-1 && !(roman[i+1]==5 || roman[i+1]==10)) return 0;                   // 1 just subtract from 5 or 10
            if (roman[i]==-10 && !(roman[i+1]==50 || roman[i+1]==100)) return 0;                // 10 just subtract from 50 or 100
            if (roman[i]==-5) return 0;                                                         // Can't subtract 5
            if (roman[i]==-50) return 0;                                                        // Can't subtract 50
            if (roman[i]==-500) return 0;                                                       // Can't subtract 500
            if (roman[i]==-1 && roman[i+1==10] && roman[i+2]==5) return 0;                     // XCL is invalid
            if (roman[i]==-10 && roman[i+1==100] && roman[i+2]==50) return 0;                  // XCL is invalid
            if (roman[i]==-100 && roman[i+1==1000] && roman[i+2]==500) return 0;               // XCL is invalid
        }
        if (roman[i]==5 && roman[i+1]==5) return 0;                                             // Can't sum 5s
        if (roman[i]==50 && roman[i+1]==50) return 0;                                           // Can't sum 50s
        if (roman[i]==500 && roman[i+1]==500) return 0;                                         // Can't sum 500s
    }

    /// Last verification, just to be sure. Is very inefficient to verify that way, so it must be removed in the final version.
    char roman_test [STR_LEN_ROMAN] = "";
    arabic_to_roman (*D, &roman_test[0]);
    if (strcmp(&roman_str[0], &roman_test[0])) return 0;

    return 1;   // If nothing goes wrong, return 1
}

void arabic_to_roman (int D, char *roman){
    while(D>=1000){
        D-=1000;
        strcat(roman,"M");
    }

    if(D>=900){
        D-=900;
        strcat(roman,"CM");
    } else{
        if (D>=500){
            D-=500;
            strcat(roman,"D");
        } else if (D>=400){
            D-=400;
            strcat(roman,"CD");
        }
        while (D>=100){
            D-=100;
            strcat(roman,"C");
        }
    }

    if(D>=90){
        D-=90;
        strcat(roman,"XC");
    } else{
        if (D>=50){
            D-=50;
            strcat(roman,"L");
        } else if (D>=40){
            D-=40;
            strcat(roman,"XL");
        }
        while (D>=10){
            D-=10;
            strcat(roman,"X");
        }
    }

    if(D>=9){
        D-=9;
        strcat(roman,"IX");
    } else{
        if (D>=5){
            D-=5;
            strcat(roman,"V");
        } else if (D>=4){
            D-=4;
            strcat(roman,"IV");
        }
        while (D>=1){
            D-=1;
            strcat(roman,"I");
        }
    }
}

