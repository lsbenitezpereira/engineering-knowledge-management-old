/****************************************************************************************************
 Title  		Arabic to roman converter
 Language 		Scilab
 Author			Leonardo Benitez
 Date  			05/04/2018
 Description 	It's a class exercise from Programing I, so there isn't functions, vectors or pointer.
 ****************************************************************************************************/
 
    ///Inicializations
	running = %t;
    roman = "";
    
    ///Main loop
	while running then
		/// Input
		D = input ("Say a integer number (1 to 3999):");
		if D<1 then D=1 end;              // Verification
		if D>3999 then D=3999 end;

        /// Processing
		while D>=1000 then
            D=D-1000;
            roman = roman + "M";
        end

		if D>=900 then
			D=D-900;
			roman = roman + "CM";
		else 
			if D>=500 then
				D=D-500;
				roman = roman + "D";
			elseif D>=400 then
				D=D-400;
				roman = roman + "CD";
			end
			while D>=100 then
				D=D-100;
				roman = roman + "C";
			end
		end

		if D>=90 then
			D=D-90;
			roman = roman + "XC";
		else
			if D>=50 then
				D=D-50;
				roman = roman + "L";
			elseif D>=40 then
				D=D-40;
				roman = roman + "XL";
			end
			while D>=10 then
				D=D-10;
				roman = roman + "X";
			end
		end

		if D>=9 then
			D=D-9;
			roman=roman+"IX";
		else
			if D>=5 then
				D=D-5;
				roman = roman + "V";
			elseif D>=4 then
				D=D-4;
				roman = roman + "IV";
			end
			while D>=1 then
				D=D-1;
				roman = roman + "I";
			end
		end

		///Output
        printf("\n\nRoman equivalent = %s", roman); // Program output
        roman="";                                   // Initializes the string to the next iteration

        /// Main loop end verification
        printf("\n\n--------------------------------------------\n\n");
        if "y" == input("Wanna get out? y/n", "s") then running=%f end
        printf("\n\n--------------------------------------------\n\n");
	end
