/****************************************************
 * C++ test 2
 * Author: Leonardo Benitez
 * Utils 
 *   Build: g++ benitez_p2.cpp -o benitez_p2
 *   Run: ./benitez_p2
****************************************************/

#include <iostream>
#include <vector>
#include <math.h>
#include "benitez_p2.h"
using namespace std;

#define PI 3.14159265

int main(){
    teste();
    return 0;
}


/* Funcao */
double Funcao::integrar(Funcao* f, double x0, double x1, double step){
    double total = 0;
    for (double x=x0; x<x1; x+=step){
        total += (*f)(x)*step; 
    }
    return total;
};

/* FuncaoAgregada */
FuncaoAgregada::FuncaoAgregada(){};
void FuncaoAgregada::agrega(Funcao *f){
    _vec.push_back(f);
};
double FuncaoAgregada::operator()(double x){
    double total=0;
    for (FuncaoVector::iterator it = _vec.begin(); it != _vec.end(); it++){
        total += (*static_cast<Funcao*>(*it))(x);
    }
    return total;
}

/* Constante */
Constante::Constante(double val){
    _value = val;
};
Constante::Constante(){
    _value = 1;
};
double Constante::getValue(){
    return _value;
};
double Constante::operator()(double x){
    return _value;  
};

/* Escalar */
Escalar::Escalar(double val, Funcao* f){
    _value = val;
    _f = f;
    cout << "Constructor 0 scalar \n";
};
Escalar::Escalar(Funcao* f){
    _value = 1;
    _f = f;
    cout << "Constructor 1 scalar \n";
};
Escalar::Escalar(double val){
    _value = val;
    _f = NULL;
    cout << "Constructor 2 scalar \n";
};
Escalar::Escalar(){
    _value = 1;
    _f = NULL;
    cout << "Constructor 3 scalar \n";
};
double Escalar::getValue(){
    return _value;
};
double Escalar::operator()(double x){
    if (_f != NULL){
        return _value * (*_f)(x);
    } else {
        return _value * x;
    }
};


/* Seno */
Seno::Seno(double val, Funcao* f){
    _value = val;
    _f = f;
};
Seno::Seno(Funcao* f){
    _value = 1;
    _f = f;
};
Seno::Seno(double val){
    _value = val;
    _f = NULL;
};
Seno::Seno(){
    _value = 1;
    _f = NULL;
};
double Seno::getValue(){
    return _value;
};
double Seno::operator()(double x){
    if (_f != NULL){
        return _value * sin((*_f)(x));
    } else {
        return _value * sin(x);
    }
};

/* Cosseno */
Cosseno::Cosseno(double val, Funcao* f){
    _value = val;
    _f = f;
};
Cosseno::Cosseno(Funcao* f){
    _value = 1;
    _f = f;
};
Cosseno::Cosseno(double val){
    _value = val;
    _f = NULL;
};
Cosseno::Cosseno(){
    _value = 1;
    _f = NULL;
};
double Cosseno::getValue(){
    return _value;
};
double Cosseno::operator()(double x){
    if (_f != NULL){
        return _value * cos((*_f)(x));
    } else {
        return _value * cos(x);
    }
};

/* Potencial */
Potencial::Potencial(double val, Funcao* f){
    _value = val;
    _f = f;
};
Potencial::Potencial(Funcao* f){
    _value = 1;
    _f = f;
};
Potencial::Potencial(double val){
    _value = val;
    _f = NULL;
};
Potencial::Potencial(){
    _value = 1;
    _f = NULL;
};
double Potencial::getValue(){
    return _value;
};
double Potencial::operator()(double x){
    if (_f != NULL){
        return pow((*_f)(x), _value);
    } else {
        return pow(x, _value);
    }
};

/* Exponencial */
Exponencial::Exponencial(double val, Funcao* f){
    _value = val;
    _f = f;
};
Exponencial::Exponencial(Funcao* f){
    _value = 1;
    _f = f;
};
Exponencial::Exponencial(double val){
    _value = val;
    _f = NULL;
};
Exponencial::Exponencial(){
    _value = 1;
    _f = NULL;
};
double Exponencial::getValue(){
    return _value;
};
double Exponencial::operator()(double x){
    if (_f != NULL){
        return pow(_value, (*_f)(x));
    } else {
        return pow(_value, x);
    }
};


void teste(){
    double y0,y1,y2;
    bool pass = true;

    Potencial f0 = Potencial(2);
    Constante f1 = Constante(-1);
    Escalar f2 = Escalar(2);
    FuncaoAgregada f3 = FuncaoAgregada();
    f3.agrega(&f1);
    f3.agrega(&f2);
    Seno f4 = Seno(&f3);
    Escalar f5 = Escalar(5, &f4);
    Constante f6 = Constante(5);
    FuncaoAgregada f = FuncaoAgregada();
    f.agrega(&f0);
    f.agrega(&f5);
    f.agrega(&f6);


    Exponencial foo(2);// = Exponencial(2);
    cout << "Valor de 2^10: " << foo(10) << "\n";

    cout << "Testing f(x) =  x^2 + 5sen(2x − 1) + 5\n";
    y0 = f(0);
    cout << "f(0): " << y0 << " (expected 0.79264)\n";
    y1 = f(5);
    cout << "f(5): " << y1 << " (expected 32.06059)\n";
    y2 = Funcao::integrar(&f, 0, 5, 0.01);
    cout << "Integral from 0 to 5: " << y2 << " (expected 70.29525)\n";

    for (double x=0; x<5; x+=0.01){
        cout << "f(" << x << ") = " << f(x) << endl;
    }

    if (!(y0>(0.79264-0.1) && y0<(0.79264+0.1))) pass=false;
    if (!(y1>(32.06059-0.1) && y1<(32.06059+0.1))) pass=false;
    if (!(y2>(70.29525-1) && y2<(70.29525+1))) pass=false;

    if (pass){
        cout << "Test result: approved\n";
    } else{
        cout << "Test result: fail\n";
    }
}
