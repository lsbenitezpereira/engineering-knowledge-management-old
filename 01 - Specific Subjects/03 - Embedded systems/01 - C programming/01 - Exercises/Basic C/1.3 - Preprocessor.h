/********************************************************
 Title			Basic bitwise macros
 Language 		C
 Author			Leonardo Benitez
 Date  			31/07/2018
 Description	Especially useful to embedded systems
 *******************************************************/

#ifndef BASIC_BITWISE_MACROS
#define BASIC_BITWISE_MACROS

#define SET_BIT(x,bit)	    x=x|1<<bit
#define CLEAR_BIT(x,bit)	x=x&~(1<<bit)
#define CPL_BIT(x,bit)      x=x^1<<bit
#define IS_BIT_SET(x,bit)   x&1<<bit

#endif	//BASIC_BITWISE_MACROS included