#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define DIMENTION 100
#define FILENAME "dados.dat"

struct pessoa{
	char nome [20];
	float alt;
	float peso;
	int idade;
};

///Function prototype
void mostrar_cadastro (struct pessoa *ptr);
float calcular_imc (struct pessoa *ptr, int i);
int altera_cadastro (struct pessoa *ptr, int i);

///Main function
int main (void){
	int i;
	char r='M';
	struct pessoa pessoas[DIMENTION];
	for (i=0; i<DIMENTION; i++) pessoas[i].nome[0] = '\0';    //Initializin the data

	FILE *fp;
	fp = fopen (FILENAME, "r+w");
	if (fp==NULL) {printf("error"); return 0;}

    fread (pessoas, sizeof (struct pessoa), DIMENTION, fp);

    //printf ("%s", pessoas[1].nome);
    printf ("IMC CALCULATOR\n");
	while (r != 'G'){
        printf("\n<M>ostrar cadastro");
        printf("\n<C>alcular IMC");
        printf("\n<A>lterar cadastro");
        printf("\n<I>ncluir pessoa");
        printf("\n<E>xcluir pessoa");
        printf("\n<G>ravar alteracoes e sair\n");
        r = getche();
        switch (r){
            case 'M':
                printf("\n\n-----------------");
                mostrar_cadastro (&pessoas[0]);
                printf("\n\n-----------------");
                break;
            case 'C':
                printf("\n\n-----------------");
                printf("\nDe quem voce quer calcular?");
                fflush(stdin);
                scanf("%d", &i);
                printf("\nIMC:%.2f\n", calcular_imc (&pessoas[0], i-1));
                printf("\n\n-----------------");
                break;
            case 'A':
                printf("\n\n-----------------");
                printf("\nDe quem voce quer alterar?");
                fflush(stdin);
                scanf("%d", &i);
                if (altera_cadastro (&pessoas[0], i-1))printf("\nAlteracao realizada!!"); else printf("\nErro na gravacao");
                printf("\n\n-----------------");
                break;
            case 'I':
                break;
            case 'E':
                break;
            default: printf("\nInvalid input, try again\n");
        }
	}
	return 0;
}

void mostrar_cadastro (struct pessoa *ptr){
    int i;
    for (i=0; i<DIMENTION; i++) if((ptr+i)->nome[0]!='\0') printf("\nPessoa %d: %s\naltura: %.2f\t|\tpeso: %.2f\t|\tidade:%d", i+1, (ptr+i)->nome, (ptr+i)->alt, (ptr+i)->peso, (ptr+i)->idade);
}

float calcular_imc (struct pessoa *ptr, int i){
    return (ptr+i)->peso/((ptr+i)->alt * (ptr+i)->alt);
}

int altera_cadastro (struct pessoa *ptr, int i){
    printf("\nInsira o nome da nova pessoa:");
    fflush(stdin);
    scanf("%[^\n]s", (ptr+i)->nome);
    printf("\nInsira a altura da nova pessoa:");
    scanf("%f", &(ptr+i)->alt);
    printf("\nInsira o peso da nova pessoa:");
    scanf("%f", &(ptr+i)->peso);
    printf("\nInsira a idade da nova pessoa:");
    scanf("%d", &(ptr+i)->idade);
    return 1;
}
