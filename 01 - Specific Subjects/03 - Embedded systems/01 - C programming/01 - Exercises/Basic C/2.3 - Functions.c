/****************************************************************************************************
 Title : 		mean calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date :			15/05/2018
 Description :  Resistor association calculator
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/// Function prototypes
float equivalent (float, float, char);

/// Main function
int main(void) {
	/// Main local variables
	char running=1;     // Main loop control
    char association;
    float R1, R2;

	/// Main loop
	while (running){
        /// Value input
        printf("First resistor value: ");
        scanf("%f", &R1);
        printf ("Second resistor value: ");
        scanf("%f", &R2);
        printf("Serial or parallel (s/p): ");
        association = getch();

        /// processing and output
        printf ("\n Equivalente resistor: %f", equivalent(R1, R2, association));
        /// Main loop end verification
        printf("\n\n--------------------------------------------\n\n");
        printf("Wanna get out? y/n");
        if(getch() == 'y' ) running=0;
        printf("\n\n--------------------------------------------\n\n");
	}
	system ("pause");
	return 0;
}

float equivalent (float R1, float R2, char association){
    return (association == 's') ? R1+R2 : (R1*R2)/(R1+R2);
}


/****************************************************************************************************
 Title : 		mean calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date :			15/05/2018
 Description :  how many days have a month? 
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/// Function prototypes
int days (int);

/// Main function
int main(void) {
	/// Main local variables
	char running=1;     // Main loop control
    int month;

	/// Main loop
	while (running){

        printf("what month: ");
        scanf("%d", &month);
        printf(" number of days: %d", days(month));

        /// Main loop end verification
        printf("\n\n--------------------------------------------\n\n");
        printf("Wanna get out? y/n");
        if(getch() == 'y' ) running=0;
        printf("\n\n--------------------------------------------\n\n");
	}
	system ("pause");
	return 0;
}

int days(int month){
    switch (month)
        case 1: return 30;break;
        //incomplete 
}

/********************************************************
 Title  		Prime numbers printer
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description 	find the prime numbers using the Eratóstenes algorith 
 *******************************************************/
 
#include <stdio.h>
#include <stdlib.h>

#define MAX	1000
void primes (short *numbers);

int main (void){
	///Main variables
	short numbers [1001];
	short i, p;

	///Variables initialization
	for (i=0; i<MAX; i++) numbers [i] = i+1;
	numbers [MAX] = -1;

	///Main program
	primes (&numbers[0]);
	for (i=0, p=0; numbers[i]!=-1;i++) {
		if (numbers[i]) {
			printf("%d\t", numbers[i]);
			p++;
			if (p%10==0) printf ("\n");
		}

	}
	return 0;
}

void primes (short *numbers){
	int i, j;
	for (i=1; numbers[i]!=-1; i++){
		//if (numbers[i])
			for(j=1+2*i;j<MAX;j=j+i+1)
                numbers [j]=0;

	}
}


/**********************************************
 Name        : aumenta a matriz com a com a soma dos elementos da linha e coluna 
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : 
 **********************************************/

function B = aumenta (A)
    [m][n]=size (A);
    if m~=n then disp ("a matrix nao é quadrada");
    else
        for i = 1:m 
            A (i, m+1) = sum (A(i,1:m));
            A (m+1, i) = sum (A(1:m, i));
         end 
		 A (m+1, m+1) = trace (A); 
    end
endfunction
