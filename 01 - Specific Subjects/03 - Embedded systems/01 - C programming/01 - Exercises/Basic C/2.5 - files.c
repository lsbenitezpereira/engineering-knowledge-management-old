/********************************************************
 Title  		
 Language 		C
 Author			Leonardo Benitez
 Date  			01/06/2018
 Description 	what??
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>
 int main (void){
    float buffer[101];
    int i;
   float mean = 0;
    FILE *fp;
     ///text
    fp = fopen ("text.txt", "r+");
    if (fp == NULL) {
        printf ("Erro ao abrir o arquivo.\n");
        exit(1);
    }

    for (i=0; !feof(fp); i++){
        fscanf (fp, "%f", &buffer[i]);
    }
    buffer[i]=-999;
    for (i=0; buffer[i]!=-999; i++)
        mean+=buffer[i];
    mean/=i;
    //printf("%f", mean);
    fprintf(fp, "\n%f", mean);
    fclose (fp);
    ///binary
    fp=fopen("bin.bin","wb");
    if (fp==NULL) {
        printf("erro\n");
        return 0;
    }
    for (i=0;buffer[i]!=-999; i++) printf ("%f\n", buffer[i]);
    fwrite(buffer,sizeof(float),1,fp);
    if(fclose(fp)!=0)
        printf("Erro ao fechar o arquivo");
 ///////////////////

    fp=fopen("bin.bin","rb");
    if (fp==NULL) {
        printf("erro\n");
        return 0;
    }
    i=fread(buffer,sizeof(float),100,fp);
    buffer[i+1]=-999;
    for (i=0; buffer[i]!=-999; i++) printf ("%f", buffer[i]);
    return 0;*/
 }

 /********************************************************
 Title			Fie exercice 1
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	Read a .txt memory map and burn into a binary file
				Lines example: 	0	:	080F4200;
								[1..255]	:	FFFFFFFF;
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>

#define TAM_BUFFER 30

int main (void){
    ///Local variables
    char buffer [TAM_BUFFER];
    int i;
    int n1, n2;
    unsigned int hex;

    ///Open file
    FILE *fp_in, *fp_out;
    fp_in = fopen ("arquivo_entrada.mif", "r");
    fp_out = fopen ("output.bin", "wb");
    if (fp_in==NULL || fp_out==NULL){
        perror ("Error in Main");
        return (EXIT_FAILURE);
    }

    ///Process file
    while (fgetc(fp_in)!='\n'); //ignore first line
    while (fgets (buffer, sizeof(buffer), fp_in)!=NULL) {
        if (buffer[0]!='['){
            sscanf(buffer, "%d : %x", &n1, &hex);
            fwrite(&hex, sizeof(n1), 1, fp_out);
            printf("escreveu em %d: %x\n",n1, hex);
        }else{
            sscanf(buffer, "[%d..%d] : %x", &n1, &n2, &hex);
            printf("tem asterisco (de %d ate %d)\n", n1, n2);
            for (i=n1; i<=n2; i++){
                fwrite(&hex, sizeof(hex), 1, fp_out);
                printf("escreveu em %d: %x\n",i, hex);
            }
        }
    }

    ///Close and exit
    if (fclose(fp_in)!=0 || fclose(fp_out)!=0){
        perror ("Error in Main");
        return (EXIT_FAILURE);
    }
    return 0;
}


/********************************************************
 Title			Fie exercice 2
 Language 		C
 Author			Leonardo Benitez
 Date  			09/08/2018
 Description	Read a .csv file with the winner of the last winter games.
                The alocation of the vector is dinamic, and the alocation of the string "name" in
                each element of the struct is dinamic too.
				Line example: 1,Norway (NOR),13,13,10,36
 *******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM_BUFFER 90   //max size of the string name

///Global declaration
struct Nations {
    int position;
    char *name;
    int gold;
    int silver;
    int bronze;
    int total;
};

int main (void){
    ///Local variables
    char buffer [TAM_BUFFER];
    int n_nations=0, max_name=0;
    int i=0, j=0;
    char c;
	struct Nations *nation;

    ///Open file
    FILE *fp = fopen ("winterGames.csv", "r");
    if (fp==NULL){
        perror ("Error in Main");
        return (EXIT_FAILURE);
    }

    ///Count lines e bigger name (TODO: its reading character by character, that is ridiculus, next time use just fgets)
    while (!feof(fp)){
        c=fgetc(fp);
        if (i==1) j++;
        if (c==',') i++;
        if (c=='\n'){
            n_nations++;
            if (j>max_name) max_name=j;
            j=0;
            i=0;
        }
    }
    rewind(fp);
	nation = malloc (sizeof(struct Nations) *n_nations);
	//printf("max name: %d\n", max_name);

    ///Process file (TODO: leia tudo primeiro e printe sรณ no final, em um for separado)
    printf("| Pos.| %36s | gold | sil. | bro. | tot. |\n", "Nation");
    for (i=0; i<=73; i++) printf("-"); printf("\n");
	for (i=0; i<n_nations; i++){
		if (fscanf(fp, "%d,%90[^,],%d,%d,%d,%d", &nation[i].position, buffer, &nation[i].gold, &nation[i].silver, &nation[i].bronze, &nation[i].total) != 6){
            printf("Arquivo nao estruturado, erro na linha %d", i);
            return (EXIT_FAILURE);
        }
        nation[i].name = malloc (strlen(buffer)+1);
        strcpy(nation[i].name, buffer);
		printf("| %4d| %36s | %4d | %4d | %4d | %4d |\n", nation[i].position, nation[i].name, nation[i].gold, nation[i].silver, nation[i].bronze, nation[i].total);
	}
	for (i=0;i<=73; i++) printf("-"); printf("\n");

    ///Close and exit
    for (i=0; i<n_nations; i++) free (nation[i].name);
    free(nation);
    if (fclose(fp)!=0){
        perror ("Error in Main");
        return (EXIT_FAILURE);
    }
    return 0;
}

