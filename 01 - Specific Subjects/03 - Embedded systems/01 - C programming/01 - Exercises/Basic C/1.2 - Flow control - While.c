/**********************************************
 Name        : Counter and de-counter
 Date        : 20/03
 Description : Go until a number and back to zero
 **********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(void) {
	int x=1;			// Actual count
	int increment = 2;  // Increment value
	int limit = 7;	    // Will count until here
	char anwser;

	/// Main loop
	while (1){
        /// Read the limit
        while(1){
            printf("Count until what number?");
            fflush(stdin);
            if (scanf("%d", &limit)==1){
                if (!(limit%2)) limit++;
                break;
            }
        }

        /// Count until the limit
        while (x>0){
            printf("%d ", x);
            x=x+increment;
            if (x==limit) increment=increment*(-1);
        }

        /// Main loop verification
        printf("\n\n--------------------------------------------\n\n");
        printf("Wanna get out? Y/N");
        anwser = getch();
        if(anwser == 'Y' || anwser == 'y') break;
        printf("\n\n--------------------------------------------\n\n");
	}
	system ("pause");
	return 0;
}

/**********************************************
 Name        : Fibonacci printer
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Print the fibonacci sequence until de x_maxº number
 **********************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int x;              // for variable
	int x_max=15;       // for limit
	int n_current = 1;
	int n_last = 1;
	/// Main loop
	for(x=0; x<=x_max; x++){
        printf("%d\n", n_current);
        n_current+=n_last;
        n_last=n_current - n_last;
	}
	system ("pause");
	return 0;
}

/**********************************************
 Name        : Sum and mean calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Keep calculating sum, mean and total entries while the entry is positive 
 **********************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    /// Global variables
    float x_sum =0;
    float x_temp = 1;
    int x_number = 0;
	/// Main loop
    while (1){
        printf("Say a number: ");
        scanf("%f", &x_temp);
        if(x_temp<0)
            break;
        else{
            x_sum+=x_temp;
            x_number++;
        }
    }
	/// End program
	printf("\n\n-------------\n\n");
	printf("Total = %f \nMean = %f \nNumbers = %d\n\n", x_sum, x_sum/x_number, x_number);
	system ("pause");
	return 0;
}


/**********************************************
 Name        : Hand multiplication calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Receive the integers A and B e multiply then in the ROOT way 
 **********************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    /// Global variables
    int result=0;
    int a, b;
    int x;              // "for" variable
	/// Main
    printf("Say a number: ");
    scanf("%d", &a);
    printf("\nSay another number: ");
    scanf("%d", &b);
    for (x=0; x<b; x++){
        result+=a;
    }
	/// End program
	printf("Result = %d\n\n", result);
	system ("pause");
	return 0;
}

/**********************************************
 Name        : Hand divider calculator
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Receive the integers A and B e divide then in the ROOT way
 **********************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    /// Global variables
    int result=0;
    int a, b;
    int x;              // "for" variable
	/// Main
    printf("Say a number: ");
    scanf("%d", &a);
    printf("\nSay another number: ");
    scanf("%d", &b);
    for (x=0; b*x<a; x++){}
	/// End program
	system ("pause");
	printf("\nResult = %d\n\n", x);
	return 0;
}

/****************************************************************************************************
 Title : 		square root calculator
 Language :		C
 Author	:		Leonardo Benitez
 Date  			10/04/2018
 Description : 	calculates the square root by the babylonian method 
****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>

float a, x, y, e;

 int main(){
    printf("Say a number: ");
    scanf("%f", &a);

    x=1;
    e=1;
    while(e>0.1){
       y=(x+a/x)/2;
       e=((y-x)/y)*100;
       printf("\n\nerro:%f\n\n", e);
       x+=0.01;
  //     if (e>10) x+=5;
    //   else if (e>1) x+=1;
      // else if (e>0.1) x+=0.1;
    }
    printf("\n\nRaiz:%f", x);
    system("pause");
    return 0;
 }

