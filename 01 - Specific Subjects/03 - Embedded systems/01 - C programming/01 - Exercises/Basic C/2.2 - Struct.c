/****************************************************************************************************
 Title : 		
 Language :		C
 Author	:		Leonardo Benitez
 Date  			24/04/2018
 Description : 	
****************************************************************************************************/

#include <stdio.h>
#include <stdib.h>

/// Preprocessor definition
#define DEBUG;

/// User defined variables
struct stundent {
	char name [20];
	char id [10];
	int g1, g2, g3;
	float grade_mean;
}

/// Main function
int main(){
	struct student programming_student [20];
#IFDEF DEBUG 
	programming_student[0]={"Leonardo", "171001463", 10, 5, 10, (10+5+10)/3};
#ELSIF

#ENDIF
	return 0;
}