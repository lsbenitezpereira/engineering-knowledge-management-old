# Calcula o somatório de um vetor de inteiros
#   int sum(int* v, int size) {
#	int sum = 0;
#	while(size--)
#		sum += *v++;
#	return sum;
#   }

.data
array:	.word 0, 1, 1, 2

.data 0x10008000
ptr_array: .word array
tam:	.word 4

.text
.globl main
main:
	lw	$a0, ptr_array
	lw	$a1, tam
	jal	sum
	move	$s0, $v0
	j	exit
		
sum:
	li	$v0, 0
loop:	beqz	$a1, end
	lw	$t0, 0($a0)
	addi	$a0, $a0, 4	
	add	$v0, $v0, $t0
	subi	$a1, $a1, 1
	b	loop
	
end:	
	jr	$ra

exit:


		
	
