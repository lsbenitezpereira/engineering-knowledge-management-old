.data
_intro: .asciiz "Digite uma palavra: "
.align 2
_text: .space 1024
_bufferSize: .word 1024
.text
.globl main
main:
# t2  ---- 24($sp)
# t1  ---- 20($sp)
# t0  ---- 16($sp)
# empty -- 12($sp)
# ra  ---- 8($sp)
# a1  ---- 4($sp)
# a0  ---- 0($sp)
	addi $sp,$sp,-28
	sw   $ra,8($sp)
	#print(intro);
	la 	$a0,_intro
	li 	$v0,4
	sw	$a0,0($sp)
	syscall
	#text = readString();
	la 	$a0,_text
	lw 	$a1,_bufferSize
	li 	$v0,8
	sw	$a0,0($sp)
	sw	$a1,4($sp)
	syscall
	#for(i=0;text[i] != NULL; i++);
	la	$t0,_text
	add	$t2,$zero,$zero
loop1:
	lb 	$t1,0($t0)
	beqz	$t1,loop2
	addi	$t0,$t0,1
	addi	$t2,$t2,1
	j	loop1
	#while(i>0)
	
loop2:	
	la	$t0,_text
	beqz	$t2,fim
	addi	$t2,$t2,-1
	add	$t1,$t0,$t2
	#printChar(text[--i])
	lb	$a0,($t1)
	li	$v0,11
	sw	$a0,0($sp)
	syscall
	j	loop2
fim:
	addi	$sp,$sp,28
