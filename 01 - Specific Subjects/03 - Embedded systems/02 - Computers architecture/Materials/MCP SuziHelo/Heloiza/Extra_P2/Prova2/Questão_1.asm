.data
_pilha:	.space	84
.text
.globl main
main:
# empty ---- 12($sp)
# ra	---- 8($sp)
# a1	---- 4($sp)
# a0	---- 0($sp)
addi	$sp,$sp,-16

sw	$ra,8($sp)

#chamada de init(_pilha)
la	$a0,_pilha #--INIT
sw	$a0,0($sp)
jal	INIT
#.... codigo anterior...
#chamada de push(_pilha,X)
la	$a0,_pilha #--PUSH
addi	$a1,$zero,1
sw	$a0,0($sp)
sw	$a1,4($sp)
jal	PUSH
#.... codigo anterior...
#chamada de push(_pilha,X)
la	$a0,_pilha #--PUSH
addi	$a1,$zero,2
sw	$a0,0($sp)
sw	$a1,4($sp)
jal	PUSH
#.... codigo anterior...
#chamada de push(_pilha,X)
la	$a0,_pilha #--PUSH
addi	$a1,$zero,3
sw	$a0,0($sp)
sw	$a1,4($sp)
jal	PUSH
#.... codigo anterior...
#chamada de push(_pilha,X)
la	$a0,_pilha  #--PUSH
addi	$a1,$zero,4
sw	$a0,0($sp)
sw	$a1,4($sp)
jal	PUSH
#.... codigo anterior...
#chamada de size(_pilha)
la	$a0,_pilha  #--SIZE
sw	$a0,0($sp)
jal	SIZE
#.... codigo anterior...
#chamada de pop(_pilha)
la	$a0,_pilha  #--POP
sw	$a0,0($sp)
jal	POP
#.... codigo anterior...
#chamada de pop(_pilha)
la	$a0,_pilha  #--POP
sw	$a0,0($sp)
jal	POP
#.... codigo anterior...
#chamada de pop(_pilha)
la	$a0,_pilha  #--POP
sw	$a0,0($sp)
jal	POP
#.... codigo anterior...
#chamada de pop(_pilha)
la	$a0,_pilha #--POP
sw	$a0,0($sp)
jal	POP
#.... codigo anterior...
#chamada de pop(_pilha)
la	$a0,_pilha #--POP
sw	$a0,0($sp)
jal	POP
#chamada de size(_pilha)
la	$a0,_pilha #--SIZE
sw	$a0,0($sp)
jal	SIZE
#FIM da MAIN
lw	$ra,8($sp)
addi	$sp,$sp,16
j	FIM #jr	$ra

#void init(pilha_t* pilha)
INIT:
	addi	$t0,$zero,-1
	sw	$t0,84($a0)	
	jr 	$ra
#int push(pilha_t* pilha,int valor)
PUSH:
	lw	$t0,84($a0)
	addi	$t1,$zero,19
	slt	$t2,$t1,$t0
	bnez	$t2,push_cheia
	addi	$t0,$t0,1
	sw	$t0,84($a0)
	sll	$t0,$t0,2
	add	$t0,$t0,$a0
	sw	$a1,0($t0)
	addi	$v0,$zero,1
	jr	$ra
push_cheia:	add	$v0,$zero,$zero
		jr	$ra
#int pop(pilha_t* pilha)
POP:
	lw	$t0,84($a0)
	slt	$t1,$t0,$zero
	bnez	$t1,pop_vazia
	addi	$t0,$t0,-1
	sw	$t0,84($a0)
	sll	$t0,$t0,2
	add	$t0,$t0,$a0
	lw	$t0,0($t0)
	add	$v0,$t0,$zero
	jr	$ra
pop_vazia:	add	$v0,$zero,$zero
		jr	$ra
#int size(pilha_t* pilha)
SIZE:
	lw	$v0,84($a0)
	addi	$v0,$v0,1
	jr	$ra
FIM:
	
	
