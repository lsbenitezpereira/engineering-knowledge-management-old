
 .data
 struct_pilha: .space 84
 
 .text
 .globl main
 main:
 	la	$a0, struct_pilha
 	
 	jal	init
 	
 	li	$a1, 2
 	jal	push
 	li	$a1, 4
 	jal	push
 	jal	push
 	move	$s0, $v0
 	
 	jal	pop
 	move	$s1, $v0
 	
 	jal	size
 	move	$s2, $v0
 	
 	li	$v0, 10
	syscall
 	
 init:
 	li	$t0, 0
 	sw	$t0, 80($a0)
 	jr	$ra
 	
 push:
 	lw	$t1, 80($a0)		# pilha[topo]
 	bgt	$t1, 20, exit_failure
 	addi	$t1, $t1, 1		# topo++
 	sw	$t1, 80($a0)
 	sll	$t1, $t1, 2		# topo*4
 	add	$t1, $t1, $a0		# pilha->data[topo]
 	sw	$a1, 0($t1)		# pilha->data[topo] = dado
 	li	$v0, 1
 	jr	$ra
 	
 exit_failure:
 	li	$v0, 0
 	jr	$ra
 	
 pop:
  	lw	$t1, 80($a0)		# pilha[topo]
  	slt	$t2, $t1, $zero		# topo < 0
  	sgt	$t3, $t1, 20		# topo > 20
  	or	$t3, $t2, $t3
  	beq	$t3, 1, exit_failure
  	beqz	$t1, exit_failure

 	subi	$t0, $t1, 1		# topo--
 	sw	$t0, 80($a0)
 	sll	$t1, $t1, 2		# topo*4
 	add	$t1, $t1, $a0		# pilha->data[topo]
 	lw	$v0, 0($t1)
 	
 	jr	$ra	
 	
 size:
 	lw	$v0, 80($a0)
 	jr	$ra
	
 	