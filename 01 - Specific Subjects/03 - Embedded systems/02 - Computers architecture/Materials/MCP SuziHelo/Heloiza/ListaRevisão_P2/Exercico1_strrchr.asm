#char string[] = "Teste"
#int strrchr(char *string, int caracter){
#	int i;
#	int j = -1;
#	for(i = 0; string[i] != '\0'; i++){
#		if(string[i] == caracter)
#			j = i;
#	}
#	if(caracter== 0) //NUL -> zero
#		return i;
#	return j;
#}
 
 .data 
 string: .asciiz "Teste ola"
 caracter: .ascii "o" 
 
 .text
 	
 	la	$a0, string
 	la	$a1, caracter
 	jal	strrchr
 	j	exit
 	
 strrchr:
 	li	$t0, 0			# $t0 -> i = 0
 	li	$t1, -1			# $t1 -> j = -1
 for:
 	lb	$t2, 0($a0)		# string[i]
 	lb	$t3, 0($a1)		# caracter
 	beqz	$t2, end_for		# if (string[i] == 0)
 	
 	beq	$t2, $t3, end_if	# if (string[i] == caracter)
 	addi	$a0, $a0, 1		# string[i] ++
 	addi	$t0, $t0, 1		# i++
 	b	for
 	
 end_if:
 	add	$t1, $t0, $zero		# j = i;

 end_for:
 	beqz	$t3, if			# if (caracter == 0)
 	move	$v0, $t1		# retur j;
 	j	finish
 	
 if:
 	move	$v0, $t0		# return i;

finish:
	jr	$ra
	
exit:
