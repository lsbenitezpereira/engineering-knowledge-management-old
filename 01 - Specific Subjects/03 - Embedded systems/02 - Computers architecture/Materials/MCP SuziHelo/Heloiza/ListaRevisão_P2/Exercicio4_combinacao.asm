 # int combinacao(int n, int s)
 #	int fat_n, fat_s, fat_n_s
 #	if(s > n)
 # 		v1 = 1
 # 	if(n < 0 || s < 0)
 # 		v1 = 2
 # 	if(n == s)
 #		v1 = 3
 #	if( n == 0 || s == 0)
 #		v1 = 4
 #
 #	fat_n = fatorial(n)
 #	fat_s = fatorial(s)
 #	fat_n_s = fatorial(n-s)
 #	mult =  fat_s * fat_n_s
 #	return fat_n/mult
 #
 # int fatorial(int i) {
 #     if( i == 1 )
 #	 return 1;
 #     else
 #	 return i * fatorial(i-1);
 # }
 
 .data
 n: .word 8
 s: .word 3
 COMB: .word 0
 
 .text
 main:
 	addiu	$sp, $sp, -24
 	lw	$a0, n
 	lw	$a1, s
 	sw	$a0, 0($sp)
 	sw	$a1, 4($sp)
 	sw	$ra, 16($sp)
 	jal	combinacao
 	lw	$a0, 0($sp)
 	lw	$a1, 4($sp)
 	lw	$ra, 16($sp)
 	j	exit
 	
 combinacao: 	
 	ble	$a1, $a0, end_if1
 	addi	$v1, $zero, 1
 end_if1:
 	bgtz	$a0, ou
 	addi	$v1, $zero, 2
 ou:	bgtz	$a1, end_if2
 	addi	$v1, $zero 2
 end_if2:	
 	bne	$a0, $a1, end_if3
 	addi	$v1, $zero, 3
 end_if3:
 	bnez	$a0, ou1
 	addi	$v1, $zero, 4	
 ou1:	bnez	$a1, end_if4
 	addi	$v1,$zero, 4
 	jr	$ra
 end_if4:
 	addiu	$sp, $sp, -24
 	sw	$a0, 0($sp)
 	sw	$a1, 4($sp)
 	sw	$ra, 16($sp)
 	
 	jal	fatorial
 	move	$s0, $v0
 	
 	sw	$a0, 0($sp)
 	move	$a0, $a1
 	jal	fatorial
 	
 	lw	$a0, 24($sp)
 	move	$s1, $v0
 	
 	sub	$a0, $a0, $a1
 	jal	fatorial
 	move	$s2, $v0
 
 	mul	$t2, $s1, $s2
 	div	$t3, $s0, $t2
 	move	$v0, $t3
 	
 	lw	$a0, 0($sp)
 	lw	$a1, 4($sp)
 	lw	$ra, 16($sp)
 	addiu	$sp, $sp, 24
 	jr	$ra
 
 fatorial:
 	addiu	 $sp, $sp, -24
 	sw	 $ra, 16($sp)
 	# Implementação da fatorial
 	li	$v0, 1
 	seq	$t0, $a0, $zero
 	or	$t0, $t0, $a0
 	beq	$t0, 1, fatorial_end
 	
 	sw	$a0, 24($sp)
 	addiu	$a0, $a0, -1
 	jal	fatorial
 	lw	$a0, 24($sp)
 	mul	$v0, $a0, $v0	# return i * fatorial (i - 1) 	
 fatorial_end:
 	lw	$ra, 16($sp)
 	addiu	$sp, $sp, 24
 	jr	$ra
 # ---------------------------
 	
 exit:	
 
 	