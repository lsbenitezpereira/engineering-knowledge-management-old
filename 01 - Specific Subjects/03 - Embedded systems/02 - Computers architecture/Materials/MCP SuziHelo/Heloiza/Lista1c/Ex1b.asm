# a = 0;
# for (i = 0; i < n; i++) {
# a = a + ~i;
# }
# Mapeamento dos registradores:
# a => $t0, i => $t1 e n => $t2
.text
.globl main
main:
	li 	$t0, 0
	li	$t1, 0
loop:	bge	$t1, $t2, exit
	addi	$t1, $t1, 1
	xori	$t3, $t1, 1
	add	$t0, $t0, $t3
	j	loop
exit:
	