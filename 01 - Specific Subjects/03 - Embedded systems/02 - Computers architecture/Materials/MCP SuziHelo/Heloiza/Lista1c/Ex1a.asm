# if ( (a > b) && (c == d) ) {
# a = b*a + c / d;
# } else {
# a &= ( b | c );
# }
# a += 3;
# Mapeamento dos registradores:
# a => $t0, b => $t1, c => $t2, d => $t3

.text
.globl main
main:
	seq 	$t4, $t2, $t3
	sgt 	$t5, $t0, $t1
	and 	$t4, $t4, $t5
	beqz	$t4, else
	div	$t2, $t3
	mflo	$t2
	mul	$t0, $t0, $t1
	add	$t0, $t0, $t2
	j 	exit
else:
	or	$t1, $t1, $t2
	and	$t0, $t0, $t1
	j 	exit
exit:
	addi	$t0, $t0, 3