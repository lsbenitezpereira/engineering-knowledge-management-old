# i = 10
# do {
# i--;
# if( i > 5 )
# A[i] = i*i;
# else
# A[i] = 2*i;
# } while (i >= 0);
#(Mapeamento dos registradores:)
#(end. base de A =>$t0, i => $t1, j => $t2)
.text
.globl main
main:
	li	$t1, 10
loop:	subi	$t1, $t1, 1
	ble	$t1, 5, else
	mul	$t3, $t1, $t1
	sw	$t3, 0($t0)
	j	exit
else:
	mul	$t3, $t1, 2
	sw	$t3, 0($t0)
	j	exit
exit:
	bgez	$t1, loop
	