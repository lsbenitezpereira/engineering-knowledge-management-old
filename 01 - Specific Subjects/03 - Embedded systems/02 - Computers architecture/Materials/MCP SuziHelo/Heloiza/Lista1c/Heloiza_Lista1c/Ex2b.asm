# q = 10;
# while (q){
# a += a*q;
# q--;
# }
#(Mapeamento dos registradores:)
#(a => $t0, q => $t1)
.text
.globl main
main:
	li	$t1, 10
loop:	beqz	$t1, exit
	mul	$t2, $t0, $t1
	add	$t0, $t0, $t2
	subi	$t1, $t1, 1
	j	loop
exit: