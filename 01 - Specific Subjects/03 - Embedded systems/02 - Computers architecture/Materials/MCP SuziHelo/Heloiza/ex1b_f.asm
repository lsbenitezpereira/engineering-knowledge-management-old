.text
.globl main
main:	
	add 	$s0, $zero, $gp
	lb	$s1, 0($s0)
	li	$t0, 2
	li	$t1, 1
	li	$t2, 2

loop:
	beq	$t1, $s1, exit
	addi	$t1, $t1, 1
	mult	$t0, $t2 
	mflo	$t0
	j loop
exit:
	addi	$s0, $t0, 0