 # int fatorial(int i) {
 #     if( i == 1 )
 #	 return 1;
 #     else
 #	 return i * fatorial(i-1);
 # }

 .data

 s0: .asciiz "Fatorial(?): "
 s1: .asciiz "\n"
 s2: .asciiz "O resultado �: "

.text

 ## Fun��o de inicializa��o do meu sistema
init:
	addiu	$sp, $sp, -24
	jal	main
	addiu	$sp, $sp, 24
	li	$v0, 10
	syscall
 ##---------------------------------------
 
 # Fun��o main
 main:
 	addiu	$sp, $sp, -24
 	sw	$ra, 16($sp)
 	la	$a0, s0
 	li	$v0, 4
 	syscall
 	li	$v0, 5
 	syscall
 	# fatorial
 	move	$a0, $v0
 	jal	fatorial
 	move	$t0, $v0
 	
 	la	$a0, s2
 	li	$v0, 4
 	syscall
 	move	$a0, $t0
 	li	$v0, 1
 	syscall
 	
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra

 # a0 (fatorial) 24(sp)
 # ------------- 24 Bytes
 #    espa�o     20(sp)
 # -------------
 # ra		16(sp)
 # -------------
 # a3		12(sp)
 # a2		8(sp)
 # a1		4(sp)
 # a0		0(sp)
 # -------------
 # int fatorial (int i);
 fatorial:
 	addiu	 $sp, $sp, -24
 	sw	 $ra, 16($sp)
 	# Implementa��o da fatorial
 	li	$v0, 1
 	seq	$t0, $a0, $zero
 	or	$t0, $t0, $a0
 	beq	$t0, 1, fatorial_end
 	
 	sw	$a0, 24($sp)
 	addiu	$a0, $a0, -1
 	jal	fatorial
 	lw	$a0, 24($sp)
 	mul	$v0, $a0, $v0	# return i * fatorial (i - 1) 	
 fatorial_end:
 	lw	$ra, 16($sp)
 	addiu	$sp, $sp, 24
 	jr	$ra
 # ---------------------------

	
