 .data 
 array: .word 7, 9, 8, 6, 5
 
 
 .text
 main:
 	addiu	$sp, $sp, -24
	la	$a0, array
	sw	$a0, 0($sp)
	
	li	$a1, 0
	sw	$a1, 4($sp)
	li	$a2, 5
	addi	$a2, $a2, -1
	sw	$a2, 8($sp)

 	jal	quicksort
 	
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
	j	exit


 # a0 (up) 	 24(sp)
 # ------------- 24 Bytes
 #    espa�o     20(sp)
 # -------------
 # ra		16(sp)
 # -------------
 # a3		12(sp)
 # a2		8(sp) -> int ub
 # a1		4(sp) -> int lb
 # a0		0(sp) -> int*x
 # -------------

 particiona:
 	sll	$t0, $a1, 2		# $t0 = lb * 4
 	add	$t0, $t0, $a0		# $t0 = x + (lb * 4)
 	lw	$t1, 0($t0)		# $t1= x[lb] --------- int a
 	addi	$t2, $a1, 0		# $t2 = lb  ---> down
 	addi	$v0, $a2, 0		# $t3 = ub  ---> up
 	
 while:	bge	$t2, $v0, end_while
 while2:
 	sll	$t4, $t2, 2
 	add	$t4, $t4, $a0		
 	lw	$t5, 0($t4)		# $t5 = x[down]
 	slt	$t6, $t2, $a2		# down < ub
 	sle	$t7, $t5, $t1		# x[down] <= a 
 	and	$t7, $t6, $t7		# x[down] <= a && down < ub
 	beqz	$t7, while3
 	addi	$t2, $t2, 1
 	b	while2
 while3:	
	sll	$t8, $v0, 2
	add	$t8, $t8, $a0
	lw	$t9, 0($t8)		# $t9 = x[up]
	ble	$t9, $t1, end_while3
	subi	$v0, $v0, 1
	b	while3
 end_while3:
	bge	$t2, $v0, while
	sw	$t5, 0($t8)		# x[up] = x[down]
	sw	$t9, 0($t4)		# x[down] = x[up]
	b	while

 end_while:
 	sw	$t9, 0($t0)		# x[lb] = x[up];
 	sw	$t1, 0($t8)		# x[up] = a;
 	jr	$ra

 quicksort:
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$ra, 16($sp)
	
	bge	$a1, $a2, else
	jal	particiona

	lw	$a0, 0($sp)
	lw	$a1, 4($sp)

	subi	$a2, $v0, 1
	jal	quicksort
	
	lw	$a0, 0($sp)

	addi	$a1, $v0, 1
	lw	$a2, 8($sp)
	
	jal	quicksort
 else:
 	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	lw	$a2, 8($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
 exit:
