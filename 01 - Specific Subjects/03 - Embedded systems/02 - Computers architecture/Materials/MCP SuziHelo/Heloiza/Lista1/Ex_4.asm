.text
.globl main
main:
	add $s0, $zero, $gp
	addi $t1, $zero, 1
	lb  $t0, 0($s0)
	slti $s1, $s0, 0
	beqz $s1, label
	sw $t0, 8($s0)
label:
	sw $t0, 4($s0)
	