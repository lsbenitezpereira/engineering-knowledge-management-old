.data
string: .asciiz "string"

.data 0x10008000
ptr_string: .word string
size: .word 0

.text
.globl main
main:
	lw 	$t0, ptr_string
	li 	$t1, 0
loop:	
	addi 	$t0, $t0, 1
	lb 	$t2, 0($t0)
	addi	$t1, $t1, 1
	bnez	$t2, loop
exit:	
	sw 	$t1, size