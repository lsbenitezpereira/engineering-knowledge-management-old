
(f → $s0, g → $s1, h → $s2, i → $s3, j → $s4)
Base dos vetores: A → $s6, B → $s7


a)f = g*i + i
	mult	$s1, $s3	#g($s1) x i($s3)
	mflo	$s0		#f($s0) = g*i
	addi	$s0, $s0, $s3	#f = f + i

b)f = g*(h + i)
	addi	$s0, $s2, $s3	#f($s0) = h($s2) + i($s3)
	mult	$s0, $s1	#f * g($g1)
	mflo	$s0		#f = f*g
	
c) f = g + (h - 5)
	subi	$s0, $s2, 5	#f = h - 5
	add	$s0, $s0, $s1	#f = f + g

d) [f,g] = (h*i) + (i*i)

e) f = g*9

f) f = 2g

g) h = min(f,g) # mínimo valor entra f e g
h) h = max(f,g) # máximo valor entra f e g
i) B[8] = A[i-j]
j) B[32]= A[i] + A[j]


2)Considerando $t0 = 0xAAAAAAAA e $t1 = 0x12345678

a)
 	sll 	$t2, $t0, 4	#$t2 = $t0 << 4, t2 = 0xAAAAAAA0
	addi 	$t2, $t2, -1	#t2 = 0xAAAA0000 + 0XFFFFFFFF = AAAAAA9F

b) 	
	srl $t2, $t1, 3		#$t2 = $t1 >> 3,  t2 = 0x02468ACF
	andi $t2, $t2, 0xFFEF	#t2 = $t2 + 0XFFEF = 0x00008ACF

3 – Asssuma que $t0 = 0x00101000. Qual é o valor de $t2 após as seguintes instruções?


	slt $t2, $0, $t0	#if 0 < t0 -> t2 = 1, else t2 = 0 ---- t2 = 1
	bne $t2, $0, else	#if t2 ~= 0 -> else 	----- else
	j done
	else: addi $t2, $t2, 2	#t2 = t2 + 2
	done:

R: $t2 = 3


4 – $s0 = A00DEFFF, $s1 = 00FF1234.
Escreva o código MIPS para as questões abaixo, considere desconhecido o estado do bit a ser alterado.

a) Ative o bit 24 de $s0, qual o valor de $s0?
b) Limpe os bits 2, 8 e 13 de $s0, qual o valor de $s0?
c) Complemente os bits 0 e 31 de $s1, qual o valor de $s1?
d) Ative os bits 1, 21, 28 e 30 de $s1, qual o valor de $s1?
