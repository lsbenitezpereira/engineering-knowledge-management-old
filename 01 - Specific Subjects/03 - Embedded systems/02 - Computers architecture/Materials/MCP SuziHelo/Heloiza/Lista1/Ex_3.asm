.text
.globl main
main:
	add $s0, $zero, $gp
	lw $t0, 0($s0)
	lw $t1, 4($s0)
	sub $t0, $t0, $t1
	sw $t0, 8($s0)