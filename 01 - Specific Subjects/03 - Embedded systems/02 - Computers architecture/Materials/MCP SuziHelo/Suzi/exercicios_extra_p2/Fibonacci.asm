.text
.globl main
main:
	addiu	$sp, $sp, -24
	
	li	$a0, 2
	
	jal	fibonacci
	move	$a0, $v0
	
	li	$v0, 1
	syscall
	
	addiu	$sp, $sp, 24
		
	li	$v0, 10
	syscall
	
fibonacci:
	addiu	$sp, $sp, -32
	sw	$a0, 32($sp)
	sw	$s0, 20($sp)
	sw	$s1, 24($sp)
	sw	$ra, 16($sp)
	
	seq	$t0, $a0, 1
	seq	$t1, $a0, 0
	or	$t0, $t0, $t1
	beqz	$t0, else
	move	$v0, $a0
	j	end
else:
	addi	$a0, $a0, -1
	jal	fibonacci
	move	$s0, $v0
	
	lw	$a0, 32($sp)
	addi	$a0, $a0, -2
	jal	fibonacci
	move	$s1, $v0
	
	add	$v0, $s0, $s1
end:
	lw	$a0, 32($sp)
	lw	$s0, 20($sp)
	lw	$s1, 24($sp)
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 32
	jr	$ra