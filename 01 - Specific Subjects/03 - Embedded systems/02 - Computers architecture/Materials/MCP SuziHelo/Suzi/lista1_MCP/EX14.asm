.data 
fonte: 	.word 11,12,14,10
destino: .space 16 

.data 	0x10008000
fonte_ptr: 	.word fonte
destino_ptr: 	.word destino
qtd_pos:	.word 4

.text
.globl main
main:
	lw	$t0, fonte_ptr
	lw	$t1, destino_ptr
	lw	$t2, qtd_pos
loop:
	beqz	$t2, exit
	lb	$t3, 0($t0)
	sw	$t3, 0($t1)
	addi	$t0, $t0, 4
	addi	$t1, $t1, 4
	subi	$t2, $t2, 1
	j 	loop
	
exit: