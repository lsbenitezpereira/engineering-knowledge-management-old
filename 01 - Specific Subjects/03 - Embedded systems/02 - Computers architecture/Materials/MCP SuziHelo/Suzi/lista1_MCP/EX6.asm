.text
.globl main
main:
	add $s0, $zero, $gp
	lb  $t0, 0($s0)
	lb  $t1, 4($s0)
	slt $t2, $t1, $t0 #if t1 < t0  --- t2=1, else t2=0
	bnez  $t2, label1 #if t2!=0
	beqz $t2, label2  #if t2=0
label1:
	sw $t0, 8($s0) 
	b Exit
	
label2:
	sw $t1, 8($s0) 
	b Exit

Exit: 
	li $v0, 10
	syscall 
