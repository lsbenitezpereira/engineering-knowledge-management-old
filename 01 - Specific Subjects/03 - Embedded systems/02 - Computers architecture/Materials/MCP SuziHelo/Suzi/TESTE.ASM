.data
buffer: .space 1024
msg2: .asciiz "you said:"
msg1: .asciiz "say something:"
msg3: .asciiz "quer repetir:"
msg4: .asciiz "sim"

.text
main:
	#imprimir msg1
	li $v0, 4  #carregar no reg $v0 o numero da chamada do sistema
	la $a0, msg1
	syscall 
	
	#ler a entrada do usuario
	li $v0, 8
	la $a0, buffer
	li $a1, 1024
	syscall
	
	#imprimir msg2
	li $v0, 4  #carregar no reg $v0 o numero da chamada do sistema
	la $a0, msg2
	syscall 
	
	#imprimir buffer
	li $v0, 4  #carregar no reg $v0 o numero da chamada do sistema
	la $a0, buffer
	syscall

	#imprimir msg3
	li $v0, 4  #carregar no reg $v0 o numero da chamada do sistema
	la $a0, msg3
	syscall 
	
	#ler a entrada do usuario
	li $v0, 8
	la $a0, buffer
	li $a2, 1024
	syscall
		 	 
	sw $t1, msg4
	
	beq $t1, $a2, main
	
