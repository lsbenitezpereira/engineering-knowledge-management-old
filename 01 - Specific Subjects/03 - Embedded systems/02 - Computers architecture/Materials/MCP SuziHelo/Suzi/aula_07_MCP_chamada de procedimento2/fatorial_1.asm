#-----------------------------------	
#a0 (fatorial)  24(sp)	
#---------------24(sp)

#	espaco	20(sp)
#---------------
#ra		16(sp)
#---------------
#a3		12(sp)
#a2		8(sp)
#a1		4(sp)
#a0		0(sp)
#---------------

.globl main
main:
	li	$a0, 3
	jal	fatorial
	move	$t0, $v0
	j	end
fatorial:
	addiu	$sp, $sp, -24
	sw	$ra, 16($sp)
	li	$v0, 1
	beq	$a0, 1, end_fatorial
	sw	$a0, 24($sp)
	addiu	$a0, $a0, -1
	jal	fatorial
	lw	$a0, 24($sp)
	mul	$v0, $a0, $v0
end_fatorial:
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra  
	 	 
	 	 	 
	 	 	 	 
end:	 