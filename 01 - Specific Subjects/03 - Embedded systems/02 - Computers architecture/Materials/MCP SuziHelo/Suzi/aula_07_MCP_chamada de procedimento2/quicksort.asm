.data
vector: .word	3, 1, 9, 5

#-----------------------------------	
#a0 		  24(sp)	
#---------------24(sp)

#	espaco	20(sp)
#---------------
#ra		16(sp)
#---------------
#a3		12(sp)
#a2		8(sp)--->ub
#a1		4(sp)--->lb
#a0		0(sp)--->x
#---------------

.text
.globl main
main:
 	addiu	$sp, $sp, -24
	la	$a0, vector
	sw	$a0, 0($sp)
	
	li	$a1, 0
	sw	$a1, 4($sp)
	li	$a2, 3
	sw	$a2, 8($sp)

 	jal	quicksort
 	
	lw	$ra, 16($sp)
	addiu	$sp, $sp, 24
	jr	$ra
	j	end

particiona:
	sll	$t1, $a1, 2		# 4 * lb
	add	$t1, $t1, $a0		# & x[lb*4]
	lw	$t1, 0($t1)		# t1 = a = x[lb]
	
	move	$t2, $a1		# t2 = down = lb 
	move	$v0, $a2		# v0 = up = ub 
	li	$t0, 0			# t0 = temp = 0
while:
	bge	$t2, $v0, end_while	# down >= up
while2:
	sll	$t3, $t2, 2		# 4 * down
	add	$t3, $a0, $t3		# & x[4*down]
	lw	$t3, 0($t3)		#t3 = x[4*down]
	
	slt	$t4, $t2, $a2		# t4 = down < ub
	sle	$t5, $t3, $t1		# t5 = x[4*down] < = a
	and	$t6, $t4, $t5		# t6 = (x[down]<=a)&&(down<ub)
	beqz	$t6, while3
	addi	$t2, $t2, 1		#down++
	j	while2
while3:
	sll	$t7, $v0, 2		# 4*up
	add	$t7, $a0, $t7		# & x[4*up]
	lw	$t7, 0($t7)		# t7 = x[4*up]
	
	ble	$t7, $t1, if		# x[4*up] <= a
	addi	$v0, $v0, -1		#up--
	j	while3
if:
	bge	$t2, $v0, while		#down >= up
	
	sll	$t3, $t2, 2		# 4 * down
	add	$t3, $a0, $t3		# & x[4*down]
	lw	$t3, 0($t3)		#t3=  x[4*down]
	move	$t0, $t3		#temp = t3 = x[down]
	
	sll	$t8, $v0, 2		# 4*up
	add	$t8, $a0, $t8		# & x[4*up]
	lw	$t8, 0($t8)		# t8 = x[4*up]
	
	sll	$t3, $t2, 2		# 4 * down
	add	$t3, $a0, $t3		# & x[4*down]
	
	sw	$t8, 0($t3)		# x[down] = x [up]
	
	sll	$t9, $v0, 2		# 4*up
	add	$t9, $a0, $t9		# & x[4*up]
	sw	$t0, 0($t9)		# x[up] = temp
	
	j	while
end_while:
	sll	$t7, $v0, 2		# 4*up
	add	$t7, $a0, $t7		# & x[4*up]
	lw	$t7, 0($t7)		# t7 = x[4*up]
	
	sll	$t6, $a1, 2		# 4 * lb
	add	$t6, $t6, $a0		# & x[lb*4]
	
	sw 	$t7, 0($t6)		#x[lb] = x[up]
	
	sll	$t7, $v0, 2		# 4*up
	add	$t7, $a0, $t7		# & x[4*up]
	
	sw	$t1, 0($t7)		#x[up] = a
	 
	jr	$ra

quicksort:
	bge	$a1, $a2, end_quicksort
	addiu	$sp, $sp, -24
	sw	$a0, 0($sp)
	sw	$a1, 4($sp)
	sw	$a2, 8($sp)
	sw	$ra, 16($sp)
	sw	$s0, 20($sp)
	
	jal	particiona
	move	$s0, $v0
	
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	subi	$a2, $s0, 1
	jal 	quicksort
	
	lw	$a0, 0($sp)
	addi	$a1, $s0, 1
	sw	$a2, 8($sp)
	jal	quicksort
		
	lw	$ra, 16($sp)
	lw	$s0, 20($sp)
	addiu	$sp, $sp, 24
end_quicksort:
	jr	$ra
end:
