# Calcula a raiz quadrada (inteiro)
#	int isqrt(int num) {
#		int res = 0;
#		int bit = 1 << 30;
#		while (bit > num)
#			bit >>= 2;
#		while (bit != 0) {
#			if (num >= res + bit) {
#				num -= res + bit;
#				res = (res >> 1) + bit;
#			}
#		else
#			res >>= 1;
#		bit >>= 2;
#		}
#		return res;
#	}
.text
.globl main
main:
	li 	$a0, 100			#num
	jal	raiz_quadrada
	move	$s0, $v0	#pega o resultado
	j	end
	  	
raiz_quadrada:
	li	$v0, 0			#res=0
	li	$t1, 1			
	sll	$t2, $t1, 30		#bit = 1 << 30, t2=bit
while1:
	ble	$t2, $a0, while2
	srl	$t2, $t2, 2		#bit >>= 2
	j	while1
	
while2:
	beqz	$t2, end_while
	add	$t3, $v0, $t2		#res + bit
	blt	$a0, $t3, else		#if (num < res + bit)
	sub	$a0, $a0, $t3		#num -= res + bit
	srl	$t4, $v0, 1
	add	$v0, $t4, $t2
	srl	$t2, $t2, 2		#res = (res >> 1) + bit
	j	while2
	
	
else:	srl	$v0, $v0, 1		#res >>= 1;
	srl	$t2, $t2, 2
	j	while2
		
end_while:
	jr	$ra			#return resultado
end:
	
	
	
	
	
	
	
	
