.data
string_1: .asciiz "eu quero bolo"
char: .asciiz ""

.text
.globl main
main:
	la	$a0, string_1
	la	$a1, char
	
	jal 	strrchr
	move	$s0, $v0

	li	$v0, 10
	syscall	


strrchr:
	li	$v0, -1			#j = -1
	li	$t0, 0			# i = 0
	lb	$t2, 0($a1)		# t2 = c
		
	
if1:
	lb	$t1, 0($a0)		# string[i]
	beqz	$t1, if2	
	bne 	$t1, $t2, for
	add	$v0, $zero, $t0	# v0 = i
	j	for
	
for:
	addi	$a0, $a0, 1	
	addi	$t0, $t0, 1		# i++
	j	if1
		
if2:
	bnez	$t2, end
	add	$v0, $zero, $t0	# v0 = i
	
end:
	jr $ra