> ucontrolers: "its not difficult, but can be tedious and tie consuming"
> who?

# Conceituação

* ARM é uma empresa, e as arquitecturas são informalmente chamadas “ARM”
* eles fazem só a arquitetura da coisa
* **Vantagens**
  * o instruction set é foda, o que gera um assembly otimizado pra caralho
  * a maioria das instruções são 16 bits, mesmo que a CPU seja 32
* **History**
  * On 13 September 2020, it was announced that [Nvidia](https://en.wikipedia.org/wiki/Nvidia) would buy majoritary part of Arm from SoftBank for $40 billion, subject to usual scrutiny, with the latter acquiring a 10% share in Nvidia.[[8\]](https://en.wikipedia.org/wiki/Arm_Ltd.#cite_note-Nvidia_details-8)[[16\]](https://en.wikipedia.org/wiki/Arm_Ltd.#cite_note-NVIDIA_bought-16)

## main ARM architectures

* **armv6, armv7**
  * Usually just named “arm”
  * 32bit
* **Aarch74**
  * 64bit

# STM32

* The STM32 is the third ARM family by STMicroelectronics, probably the most sucessfull one, manufectured by ST electronic
* RISC processors
* Use numerically low priority numbers to represent logically high priority interrupts.
* saída 3.3V, mas o pino não queima se botar 5V

## Setup

* Instalando o compialador GCC para arm: `sudo apt-get install gcc-arm-none-eabi`

* if you need to update ST-LINK: https://www.st.com/en/development-tools/stsw-link007.html#

* burn to flash: https://www.st.com/en/development-tools/stsw-link004.html

* another flahs tool: https://www.st.com/en/development-tools/flasher-stm32.html

* tutorial legal com ST-LINK: https://www.embarcados.com.br/blue-pill-instalacao-dos-softwares-e-bibliotecas/

* Os bluepill xingling precisam trocar um id no CubeIDE: [ver msg do guillerme]

* Se não for em modo deburação é mais fácil de rodar

* Pode ser C ou C++

  

  
  
  

## Programming libraries

* **Standard Peripheral Interface**
  * Just for legacy now, substituted by HAL
  * É o que usamos no labsmart PGEN STM
  * Possui uma camada geral, para todos os STM32, e drivers específicos para a versão utilizada 
  * made a lot easier
* **HAL**
  * Hardware abstraction layer

## IDEs and softwares

* **Mbed**
  * free online
  * Have a lot of examples, materials, etc
  * not just to STM, mas para todos os ARMs
* **MXCube**
  * IDE
  * free
  * For windows and linux and mac
  * Lotsa grafical configuration
* CubeIDE
  * ==same as cubeMX???==
  * ==só gera o arquivo a ser gravado??==
  * Gerar .hex para o proteus: project -> properties -> settings -> MCU post build outputs -> convert to .hex file
  * no proteus, as vezes precsa configurar:  design -> config power rail -> bota VCC e GND nos pinos que precisa
* **Atollic**
  * era usado pra gravar, agora é uma IDE completa integrada com o cube
* **uVision**
  * IDE
  * Lotsa grafical configuration
* **Microsoft visual studio**
  * Permite botar uns breakpoints e tal
  * Legal para programar com a Standard Peripheral Interface
* **Arduino IDE**
  * the arduino-compatible libraries for STM32 are available in [link](http://dan.drown.org/stm32duino/package_STM32duino_index.json)
  * [Easy instructions, for F103C8T6](https://www.filipeflop.com/blog/stm32-com-ide-arduino-primeiros-passos/)

* * https://slack-redir.net/link?url=https%3A%2F%2Fmongoose-os.com%2F&v=3)

## CMSIS OS

* Generic API for RTOSs that run on STM32
* developed by arm
* based and compatible with FreeRTOS

## Processor cores

*  Each STM32 microcontroller series is based upon either a Cortex-M7F, Cortex-M4F, Cortex-M33, Cortex-M3, Cortex-M0+, or Cortex-M0 ARM processor core. 

*  **FAmilies**

   *  A: graphics; not much suitable for real time applications, because performs virutal memory mapping (see MMU and MPU)
   *  R: real time processing, control
   *  M: uso geral

* **O que eu já usei**

  * O que eu usei no labsmart:  STM32F429ZI (placa [nucleo-144](https://www.st.com/en/evaluation-tools/nucleo-f429zi.html))

    O que eu tenho: stm32f103c8t6
  
  

### Cortex M0

* O cortex M0 é o basicão e baratasso

#### Serie F103

* members are fully pin-to-pin, software and
  feature compatible
* 64KB flash, 20KB RAM
* **Blue pill development board**
  * F103C8T6
  * widelly used
  * cheap, ~1.8USD
  * [pinout](https://img.olx.com.br/images/95/958813016104220.jpg)
  * 20k da RAM; o freeRTOS consume uns 10k, e o framebuffer do charles +- 1k
  * Resistor fix bluepill (R10): http://amitesh-singh.github.io/stm32/2017/05/27/Overcoming-wrong-pullup-in-blue-pill.html
  * <u>ajuste do ID para as copias chinesas</u>
    * noa funcionam bem nas IDE
    * Caminho para alterar id do chip stm32
    * Em `D:\Programas Instalados\STM32CubeIDE_1.4.0\STM32CubeIDE\plugins\com.st.stm32cube.ide.mcu.debug.openocd_1.4.0.202007081208\resources\openocd\st_scripts\target`, trocar `set _CPUTAPID 0x1ba01477` para `set _CPUTAPID 0x2ba01477`
  * <u>como gravar o bluepill sem a IDE</u>
    * https://www.embarcados.com.br/blue-pill-instalacao-dos-softwares-e-bibliotecas/ (e aqui tem as linhas de comnado pra escrever na memória do mic)
    * https://github.com/stlink-org/stlink/blob/develop/doc/compiling.md (aqui no README do repositório da o caminho pra instalar o que precisa pra gravar pelo stlink)
    * https://github.com/libopencm3/libopencm3
    * https://www.embarcados.com.br/blue-pill-alimentacao-e-primeiros-passos-com-st-link/ (no mesmo link mas em outro tópico tem um exemplo de como usar os comandos)
    * 

### Cortex M3

* Harvard architecture (have separate interfaces to fetch instructions Instructions and Data)
* In a cold start, the receiver takes up to 30 seconds to acquire its position.

### Cortex M4

*  The Cortex-M4F is conceptually a Cortex-M3 + DSP and single-precision floating-point instructions



* 

































