# Conceptualization

* inferece right in the edge

* specially important to contexts of high volumes data, like video streams

* (+) lower latency

* (+) lower communication bandwidth

* (-) higher cost in the embedded device

* you may train the model in the edge as well, or train in a server very near the edge (maybe an high performance edge device, centralizing the training task)

  ![image-20200326181947670](Images - Edge Computing/image-20200326181947670.png)
  
* Nvidia motivational video: [IA at the edge, powered by jetson](https://developer.nvidia.com/gtc/2020/video/s22410)

* edge server vs end devices:

  ![image-20200609004424757](Images - Edge Computing/image-20200609004424757.png)

> Source: Xukan Ran, 2019

## Methods for inference

* This is an overview, and more detailed approches are given in specific files (ex: reduzing NN is written in NN file)
* **On-device computation**
  * Model is fully executed on the end device
  * (-) low latency
  * (-) low bandwith
* **Edge-server based**
  * Half end device, half edge server
  * Data from the end devices are sent to one or more edge servers for computation
  *  Since the edge server is close to users
    and can respond quickly to users’ request, it becomes the
    first-choice helper.
    * data preprocessing is useful to reduce data
      redundancy and thus decrease communication time.
* **Joint computation**
  
  * among end devices, edge servers, and the cloud.
  
  ![image-20200609221918387](Images - Edge Computing/image-20200609221918387.png)
  

# Optimizad libraries

## CoreML

* from apple

## MLKit

* from google
* interfaces well with Tensorflow

## TensorFlow Lite

* To microcontrollers or mobile
* Support for a limited subset of TensorFlow operations
* Traning is not done in edge
* experimental GPU capabilities
* Substituiu o predecessor Tensorflow Mobile

# Nvidia options

* ==Maybe I should have an specific file for GPU stufs==
* Nvidia GPUs are usualy released first to servers, then enthusiasts, then normal consumers
* **CUDA**
  * Compute Unified Device Architecture
  * Traditional programs cannot access GPUs directly
  * Solution: CUDA
  * parallel computing platform and programming model for general computing on GPUs
  * (-) You dont have fully control of the hardware like in a FPGA
  * (-) you need a CPU to interact with the world
  * (-) generally is slower than FPGA to CNNs
  * (+) easier to interact with an existing CPU program than FPGA
* **cuDNN**
  * CUDA Deep Neural Network library
  * GPU-accelerated library of primitives for deep neural networks
  * Provide standard routines (such as forward and backward convolution, pooling, normalization, and activation layers)
* **TensorRT**
  * SDK for high-performance deep learning
  * Focused for deployment
  * Compile trained models (comming from Keras, pytorch, among others) to optimize for nvidia architecture
  * performs several optimizations for the target GPU

## GPU basic

* FLOP: FLoating-point Operations Per Second
* TPU
  * specific for tensor operators
  * nowadays (2020) is only available on cloud
* On premisse pre build
  * Nvidia: quite expensive
  * Lambda Labs: cheap and good
  * [Good guide](http://
    timdettmers.com/2018/12/16/
    deep-learning-hardware-guide/)

## EGX edge server

* General nvidia solution to edge computing
* Software stack: kubernets, containers, cudaX and IoT stuffs
* run linux
* GPU optimization
* Node Feature Discovery: allow easy integration between hardwares
* ==not compatible yet with yetson?==
* AI repository
  * Trained models
  * Ready containers
* GPU operator
  * container to run GPU
  * cloud-native

## Jetson

* embedded computing boards with GPU
* Desined for Machine Learning
* They all have the same software stack
* runs linux
* “low” comsumption
* tey have simple board and also deselopment kits
* Models
  * Nano
    * cheap, 129 USD	
    * 128-Core Maxwell GPU, 4-Core 64-bit ARM CPU, 4GB LPDDR4, 16GB eMMC
    * 472 GFLOPS
    * 4W

## **GPU on Docker**

* Specially to cloud applications, but it can be used anywhere
* This is not the best place to put this section, but OK
* On versions including and after 19.03, you will use the nvidia-container-toolkit package and the --gpus all flag
* only the NVIDIA® GPU driver is required on the host machine (the NVIDIA® CUDA® Toolkit does not need to be installed)

## Triton

* inference server

* Cloud

* aimed for fast prototypation, so it is not AWS competitor

* GPU optimized

  ![img](Images - Edge Computing/model_deployment.png)





## Tranfer learning toolkit

* preparete and augmentate data

* train

* prune

* built on top of keras

* nvidia have a repository wiht several pre trained model

  ![image-20200326180705709](Images - Edge Computing/image-20200326180705709.png)

# Intel options

* vision processing unit (VPU)
  * processor specific to image
  * I’m registering here because I think it’s propretary from Intel

## OpenVINO

* Open Visual Inference and Neural network Optimization
* Intel’s toolkit to optimize neural networks for its hardware
* Focus on computer vision
* Free (Apache License Version 2.0), but not open source
* Similar to Nvidia’s TensorRT
* “write once, run everywhere”

## Intel® DevCloud

* train and inference
* cloud

##  Neural Compute Stick (NCS)

* connects by ==usb== to a main processor
* auxiliaria processor, optimized for NN
* Myriad X VPU
* supported by the OpenVINO
* NCS 2
  * 16 cores
  * ~USD 99 (2020)

## Edison

* ==have GPU?==