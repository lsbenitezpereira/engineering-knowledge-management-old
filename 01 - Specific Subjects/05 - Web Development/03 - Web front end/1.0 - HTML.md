# **Conceituação**

-   HyperText Markup Language
-   Marca as coisas na pagina (não a sua posição, mas a sua existência)
-   Não é *case sentisive*
-   É baseada em tags e cada tag pode ter parâmetros associados
-   É baseada em caracteres ASCII, mas pode receber símbolos especiais
-   HTML validator:
    [[https://www.w3schools.com/html/html\_validate.html]{.underline}](https://www.w3schools.com/html/html_validate.html)
-   **DOM**

    -   Document Object Model

    -   A forma como o browser representa os elementos html, xhtml e xml

    -   in-memory tree structure

    -   Através dessa API podemos acessar e manipular os elementos
-   **Elementos semânticos**

    -   Novidade do HTML 5

    -   Definem claramente qual será o conteúdo de cada marcação

    -   [[Main elements]{.underline}](https://www.w3schools.com/htmL/html5_semantic_elements.asp)
    
-   [[Full list]{.underline}](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
        
    -   [[Diferença section e article]{.underline}](http://www.kadunew.com/blog/html/html5-diferenca-de-section-e-article) (article fica dentro da section)

[![](./Images/1.0 - HTML/media/image1.png){width="6.041666666666667in"
height="4.166666666666667in"}](http://www.kadunew.com/blog/html/html5-diferenca-de-section-e-article)

-   [**Metatags**](http://www.kadunew.com/blog/html/html5-diferenca-de-section-e-article)
-   Definidas no \<head\> do documento
    
-   \<title\> → aparecerá na busca
    
-   \<meta descripton\> → descrição em até 156 caracteres

## Atributos

-   Vinculados aos elementos;

-   Atributos contém informação adicional sobre um elemento;

-   Sempre são colocados na Tag Inicial;

-   Normalmente atributos vêm em par com o valor ( Name="value" )

![](./Images/1.0 - HTML/media/image2.png){width="7.495833333333334in"
height="2.8333333333333335in"}

## Principais tags

-   **Escrita geral**

    -   \<H\> → titulo (1 até 6, sendo o primeiro mais importante)

    -   \<P\> → paragrafo

    -   \<Br\> → quebra de linha

    -   \<Blockquote\> → citação

    -   \<HR\> → linha horizontal (divisória)

-   **Marcadores de estilo**

    -   Se for algo recorrente deve ser feito via CSS

    -   \<Strong\> ou \<B\> → negrito

    -   \<I\> → itálico

    -   \<U\> → sublinhado

    -   \<TT\> → letra monoespaçada

    -   \<BIG\> → aumenta a fonte + negrito

    -   \<SMALL\> → diminui a fonte

    -   \<SUP\> → sobrescrito ^exemplo^

    -   \<SUB\> → subscrito ~exemplo~

    -   \<BLINK\> → texto fica pulsante

-   **Links**

    -   \<a\>

    -   Href → endereço do link, página, arquivo ou \#âncora

    -   Name → cria uma âncora para rolagem

    -   Target → forma de abertura (nova guia = \_blank)

    -   Enviando e-mails → *exemplo:*
    
        ```
        <a href=“MAILTO: fulano1@algo.com?SUBJECT=oi ”> texto </a>
        ```

-   **Imagens**

    -   \<img\>

    -   Scr → endereço da imagem

    -   Alt → descrição (sempre descrever, por questões de acessibilidade)
    
-   *Exemplo*: pegando a imagem de outro servidor
    
        ```
        <img src="http://www.servidor.com/imagem.jpg">
        ```
    
    -   Definir largura e altura no CSS
    
    -   Formato: JPG é mais leve e PNG é mais pesado e permite transparência

-   **Lista**

    -   Ordenada → \<ul\>

    -   Não ordenada → \<ol\>

    -   Definições → \<dl\>

    -   Ordenadas e não ordenadas são marcadas por \<li\>

    -   Definições são marcadas por \<dt\> e \<dd\>

    -   Listas servem não apenas quando queremos escrever em tópicos, mas também para relacionar semanticamente conteúdos relacionados e/ou sequencial (como itens de um menu, por
        exemplo)
    
-   **Tabelas**

    -   \<table\>

    -   \<caption\> → legenda (título) → aceita o parâmetro

    -   \<title\> → Títulos em geral

    -   \<tr\> → table row (linha)

    -   \<td\> → table data → define o conteúdo de uma célula dessa linha
    
-   \<th\> → table heading → título para a linha (é tipo uma \<td\>, mas com formatação diferente)

## Formulários

-   \<form\>

-   Permite enviar dados pro servidor

-   Obviamente, lá precisa existe uma linguagem de programação para
    receber esses dados

-   **Método**

    -   Diferem quanto à forma de trocar informações com o servidor

    -   Podem ser pelo método get ou post (ver detalhes em arquivo específico)

-   **Action**
    -   Especifica o local (URL) do servidor e do programa que vai processar os dados

-   **Input**
    -   Campos de entrada

    -   Name → nomeia a variável (fundamental na hora do recebimento)

    -   Type → o tipo de coisa que será escrito (texto, senha, chebox, submit, etc)
        
    -   Value → texto padrão exibido

    -   Size → número de caracteres exibidos

    -   Maxlength → tamanho máximo da string a ser inserida

-   **Outros marcadores**

    -   Select e option → lista de opções pro usuário escolher

    -   Textarea → área de texto livre

    -   Fieldset → envolve todos os campos dentro dele em uma moldura

    -   Legend → dá um título ao fieldset

    -   Acesskey → define uma tecla de atalho

-   **Alinhando a tabela**

    -   Podemos usar a gambiarra de colocar uma letra monoespaçada (tag PRE) e completar os nomes com espaços
    
-   O Jeito tradicional é usando tabelas

## Caracteres HTML

-   Existe no HTML códigos que possibilitam o uso de caracteres
    especiais, por exemplo:

![](./Images/1.0 - HTML/media/image3.png){width="7.495833333333334in"
height="3.9583333333333335in"}

-   Caso você queira dar espaço entre palavras, ou entre frases, etc..
    você pode utilizar a expressão &nbsp; (nobreak-space) obs.não
    esquecendo do ponto e vírgula

-   +INFO
    [[http://erikasarti.com/html/acentuacao-caracteres-especiais/]{.underline}](http://erikasarti.com/html/acentuacao-caracteres-especiais/)

## Cores

-   Existem diferentes formas de representar cores

-   **Representações aceitam em HTML 5**

    -   Nome da cor

    -   RGB (Red/Green/Blue)

    -   HEX(Hexadecimal)

    -   HSLA(pouco utilizado)

    -   RGBA(A= Alfa, escala de transparência que assume valores de 0 até 1)

-   **Exemplos**

    -   Background-Color ( \<p style=\"background-color:Tomato;\"\>Fundo\</p\> )
        
    -   Text Color ( \<h1 style=\"color:Tomato;\"\>Olá Mundo\</h1\> )

    -   Border Color ( \<h1 style=\"border:2px solid Tomato;\"\>Hello
        
        > World\</h1\> )

-   **Links uteis**

    -   [[https://www.w3schools.com/html/html\_colors.asp]{.underline}](https://www.w3schools.com/html/html_colors.asp)

## Frames

![](./Images/1.0 - HTML/media/image4.png){width="5.40625in"
height="1.9479166666666667in"}

## Imagem mapeada

-   Cada região da imagem funciona como um link

-   **Mapa server side**

    -   O browser envia uma requisição com a localização do seu click e o servidor retorna o link daquela região
    
-   São criadas por programas separados, que exportam o arquivo ".map"
        
    -   *Exemplo*:

        ```
        <a href=“url_do_programa/arquivo.map”><img src=“nome_da_imagem” ISMAP></a>
        ```

-   **Mapa client side**

    -   As marcações são feitas no próprio documento html

    -   Existem programas que fazem e exportam o código html correspondente

# Others

## Web rendering engines

* ==Where do I put that?==
* WebKit
  * iOS
  * used in Safari and Chrome
* Blink
  * Usually used in Android
* Trident
  * Internet Explorer