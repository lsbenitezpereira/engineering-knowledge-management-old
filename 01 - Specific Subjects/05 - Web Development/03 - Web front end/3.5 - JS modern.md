# Conceituação

* **SPA**
* ?

## Frontend arquitectures

* **Padrão MVC**

  * Model View Controller
  * About the application of MVC beyond frontend, see in *Software Engineer*
  * “ don't find it terribly useful to think of it as a pattern because it contains quite a few different ideas. Different people reading about MVC in different places take different ideas from it and describe these as 'MVC'” Martin Fowler
  * Separa as responsabilidades e organiza a arquitetura
  * *Dica:* evite ler o scope dentro do controller
  * Model → APIs e aquivos → services do angular
  * View → exibição → é o html (DOM)
  * Controller → gerencia os dados que trafegam entre model e view
  
* ?

  * ?

    ![diagram of a layered architecture approach: router, API/data, UI layer, and business logic](Images - 3.5 - JS modern/1*iC1ID849pXfZIwoMrsuIkA.png)

* https://martinfowler.com/eaaDev/uiArchs.html

* **Flux architecture**

  * or fluxy
  * I write this in opposition to specific implementations, like Redux
  * Single source of truth: no two-way data binding

## Data interaction

* architectural solutions for managing interactions with data

* creates like an “global scope”

* makes easier to manage shared states

* **Redux**
  * Complex
  
  * application state management.
  
  * enforces an unidirectional flow of data: the store is the single source of truth, and everyone will get/push from there
  
  * Allow one component to directly share data with another one that is not his father 
  
  * the approach is more declarative, like react ifself
  
  * 
  
  * `connect`?
  
  * data can’t be changed directly: must use setters and/or reducers
  
  * run in different environments (client, server, and native)
  
  * Enforcing that every change is described as an action lets us have a clear understanding of what’s going on in the app
  
  * 
  
  * <u>store</u>: where data rest
  
  * <u>action</u>
  
    * Are called (dispatched)
    *  plain object (with type and optional datas), or fuction that return a plain object
    * just indicate what should be done and who does it is the reducer
    * `dispath(<actionObject>)` method: execute an action (send to the reducer)
  
  * <u>reducer</u>
  
    * function that takes state and action as arguments, and returns the next state of the app
    * implement the logic of “how the action affects the state”
    * several reducers can (or should?) be combined into one single reducer
  
    ![image-20210122234338038](Images - 3.5 - JS modern/image-20210122234338038.png)
    
  * <u>redux saga</u>
  
    * extension of redux (is a reux middleware)
    * for asynchorunous stufs
    * saga is like a separate thread in your application that's solely responsible for side effects
    * (+) easy to test
    * the saga is implemented with Generators
    * receive the action as parameter
    * effects = plain JS objects that are yieled by the saga
    * `yield call`: call function
    * `yield put`: call dispatch
    * `yield <promise>`: whatever you want 
    * the sagas will be watching the actions (`takeEvery(<action>, <saga>)`); you can also use `takeLatest`
  
  * <u>redux-thunk</u>
  
    * similar to saga
    * (+) more simple
  
* **Flux**
  
  * very similar to Redux
  * do not confuse with the arquitecture Flux
  
* **VueX**

  * state management pattern + library for Vue.js applications

# Modern frameworks

* Main characteristics
  * Virtual DOM
  * SPA
  * component-based
  * MVC

## Angular

* Maintained by Google
* (-) Steep learning curve

-   Facilita a implementação de um padrão MCV e de aplicações SPA

-   Funciona como uma extensão do HTML

-   Incluindo o Angular.js:

    ```
    <html ng-app>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
    ```

-   **Variáveis** → tipo dinâmico

-   **Funções**

    -   Criamos uma variável e adicionamos uma function nela

    -   *Exemplo:*

![](Images - 3.5 - JS - Vue/image3.png){width="3.6354166666666665in"
height="2.1666666666666665in"}

### Diretivas

-   Novos parâmetros adicionados ao html

-   Permitem implementar novos comportamentos de forma declarativa

-   Ng-controller → cria um vínculo entre um elemento do view ao controller

-   Ng-bind → substitui um elemento por uma expressão

-   Ng-repeat → varre uma string faz alguma coisa em loop

-   Ng-model → vincula uma propriedade ao scope (envia um dado)

-   Ng-click → reage ao evento click

-   Ng-disable → desabilita um elemento dinamicamente

-   Ng-option → renderiza as opções de um select

-   Ng-class → aplica uma classe

-   Ng-style → aplica um estilo css

-   Ng-show → exibe somente se o parâmetro for true

-   Ng-hide → mesma lógica

-   Ng-if → o elemento só existe se for true (mais o que só não exibi-lo)

-   Ng-include → carrega um arquivo na página. *Exemplo:* load footer.html:

    ```
    <div ng-include="'footer.html'"></div>
    ```

-   **Diretivas de validação de formulários**

    -   Ng-required → declara um campo de formulário como obrigatório

### Databind

-   Liga uma variável a alguma coisa no html

-   Scope → faz a ligação entre view e controller (ponte bidirecional)

-   O que não for ser compartilhado com a vier não precisa ser feito via scope

-   *Exemplo*: o html irá exibir "Olá Nicolas"

![](Images - 3.5 - JS - Vue/image2.png){width="3.9895833333333335in"
height="1.8958333333333333in"}

### Filtros

-   Transformam alguma coisa

-   Ou seja: formata, ordena, etc

-   São aplicados usando um "\|"

-   Podem ser somados

-   Uppercase → bota uma string em letra maiúscula.

-   Lowercase → bota em letra minúscula. *Exemplo:* puxa o nome em lowercase:

    ```
    <p>{{contato.nome | lowercase}}</p>
    ```

-   Data → formata usando uma máscara

-   Filter → filtra um array baseado em um critério

-   Orderby → ordena

-   Currency → converte um número para uma moeda

-   Number → formata um número (casa decimais, arredondamentos, etc)

-   LimitTo →

### Serviços

-   Objeto único para ser usado em outros componentes

-   Criado na inicialização

    ```
    angular.module("nome_modulo").factory("nome_serviço", function ($http, config) {
          //serviço
    });
    ```

-   Tipo factory

-   Tipo service

-   Tipo provider → permite inicialização

### Rotas

-   Serviço para realizar roteamento de páginas

-   Determinam qual html conversar com qual controller

-   *Ex:* Criando um módulo para o "app" através de uma rota:

    ```
    angular.module('App', ['ngRoute']);
    ```

-   Este módulo será executado dentro de uma função

### Others

* **Constant**
  * Cria um negócio pra enfiar valores constantes
* **Módulos**
  * Conjunto de controlers, diretivas, filtros, serviços, etc.

## React

* maintained by Facebook
* About *React Native*, see in *Mobile Development*
* How thing works are much more transparent (pure JS) than the other libraries
* **DOM update**
  * only applies the DOM updates necessary to bring the DOM to the desired state
* **Bibliography and materials**
  * https://www.youtube.com/watch?v=DLX62G4lc44

### JavaScript Expressions (JSX)

* syntax extension to JavaScript

* combine HTML and CSS together into JavaScript

* **JS expressions**
  * You can put any valid JavaScript expression inside the curly braces in JSX
  * XSS automatic prevention: By default, React DOM escapes any values embedded in JSX before rendering them. Thus it ensures that you can never inject anything that’s not explicitly written in your application. 
  
* **React elements**
  * descriptions of what you want to see on the screen
  * Used to construct the DOM
  * The application will have a single root DOM node
  * `render(<what>, <where>)`
  * Example: rendering title into a root DOM node: `ReactDOM.render(<h1> Hello! </h1>, document.getElementById('root'));`
  
* **Style**
  * The class (css) of a html tag is called, in JSX, `className`; It cannot be applied to components
  * Inline style with parameter `style` which receves a dictionary
  * Example: `<h1 style={{color: 'blue', fontSize: '10px'}}> hello </h1>`
  * All properties in camelCase
  
* **Events**
  
  * `onClick`
  * `onMouseOver`
  * receives the function inside curly braces
  
* **Conditional rendering**

  * Use a ternary, or and operator, of something similar

  * `mustDisplayStuff && <p>hi!!</p>`

  * `mustDisplayStuff ? <p> yes! </p> : <p> no! </p>`

    

### Components

* The HTML tag name will be the component’s name
* Always start component names with a capital letter
* return React elements describing what should appear on the screen
* It’s tag cannot receive `className` (because the whole element will be substituted)
* **Class vs Function**
  * Class
    * The documentation say it’s “better”
    * Feel more elegant
    * The JSX is passed to the method `render`
  * Function
    * return the JSX
    * ==doesnt allow Lifecycle methods?==
* **Single file component**
  * Every component should import React
  * must export the component as default
* **Props**
  * Read-Only inputs
  * atributes of the HTML tag will be passed as props
  * there are no limitations on what you can pass as props (including other components)
  * Sintax: the component receive as parameter one single object, which store the props
  * ==can be changes and received dinamically?==
* **State**
  * Atributes, private and fully controlled by the component
  * to ensure DOM binding, the state must be updated with `this.setState()`
  * Just to class-based component
  * initialized in the constructor method: `this.state = {name: 'value'}`
  * if a state is set inside a function, that funtion must be bindede in the constructor of the class: `this.foo = this.foo.bind(this)`
  * `setState` received a function that will return the new state
  * if you wanna access the previus state, `setState` must receive the argumet `prevState`
  * React may batch multiple `setState()` calls into a single update for performance.
* **Lifecycle Methods**
  * Just for class-based
  * mount: rendered to the DOM for the first time
  * componentDidMount()
  * componentWillUnmount()
  * componentWillReceiveProps()
* presentation components?

### Hooks

* let you use state (and other features) in function-based components
* cannot be used in class-based components
* functions that let you “hook into” React state and lifecycle features from function components
* reuse stateful behavior between different components
* 
* https://reactjs.org/docs/hooks-state.html

### Others

* **Routing**
  * routing is not included in React.js, so that would have to be added manually through third-party tools.
  * Pages
    * ==Is this a different thing than Router? Or it’s just the folder name?==
    * Will existe as route
    * File = endpoint
    * Folder = path 
* **Deploy**
  * Building and deploying React.js apps is also easy. With `create-react-app`, you have a tool that generates new, fully pre-configured React.js apps  which include everything you need for development and for building your  project in the end. With the provided configuration, you also get a  fully optimized project which you can deploy to any static host. 
* **Forms**
  * You can bind directly to the State
  * on event `onChange`, you validade and bind
  * Active form: the binding is two way

### Tools and Libraries

* Not everything here is exclusive to React
* **NextJS**
  * Server side
  * Similar to Electron
* **Immutable**
  * ?
* **Internationalization**
  * To work with multilanguage applications
* **GraphQL**
  * ?

## VueJS

-   Baseado no padrão MVVC (Model, View e View-Controller), com separação baseada em instâncias

-   uses ECMAScript 5

-   open source since the begining

-   (+) easy to learn

-   (+) easy to integrate with current applications

-   **Data binding**

    -   Ou *Renderização declarativa*

    -   Coloca dados no DOM de forma fucking fácil

    -   É reativo, ou seja, atualiza automaticamente quando a variável muda
    
-   Pode ser usado dentro de atributos HTML (e não apenas no texto)
    
-   Podem conter uma (e só uma) expressão HTML (atribuição ou condicional não, só expressão)
        
    -   *ex:*

        ```
        <p> {{variável_JS}} </p>
        ```

-   `Object.freeze(<object>)`: prevents existing properties from being changed

-   **Limitações à reatividade**

    -   Databind é legal, mas existe alguns casos em que o vue não dá conta
        
    -   Cannot detect property addition/deletions of objects. If you really need, use the global
        `Vue.set` or `Vue.delete`
        methods 
        
    -   Cannot detect array setting by index (e.g. arr\[0\] = val).
        
        > Instead, use \<array\>.\$set(\<index\>, \<value\>)
        
    -   The DOM update is not instantaneous, it happens asynchronously.
        
        > To speed up the update, use Vue.nextTick.

### Vue CLI

-   tool for rapid Vue.js development

-   runs in the back

-   requires nodejs

-   Instalando nodejs

    -   sem docker, sem apache

    -   curl -sL
        > [[https://deb.nodesource.com/setup\_11.x]{.underline}](https://deb.nodesource.com/setup_11.x)
        > \| sudo -E bash -

    -   apt-get install -y nodejs

-   Instalndo vue

    -   npm install vue \# (criará a pasta node\_modules)

    -   npm install -g \@vue/cli

    -   npm install -g \@vue/cli-init

-   Criando um projeto webpack

    -   allows you to choose from a range of build tools, which it
        
    > will then install and configure for you
    
    -   they have great scafolds

    -   vue init webpack vue-router

-   Main Node packages

    -   named as "vue-cli-plugin-\<name\>"

    -   Babel: allows us to write ES6 code which it then compiles
        
    > into ES5 (for browser compatibility)
    
    -   Webpack: is a module bundler. It merges code from multiple
        
    > files into one
    
    -   Axios: Promise based HTTP client

-   Starting the server

    -   npm run dev

    -   vue serve \<componentName\>: prototype a server only for
        that component
    
-   **Enviroment ariables**

    -   .env file

    -   names must begin with VUE\_APP\_

    -   you can have variations for each mode: .env.production,
        
    > .env.development, .env.test
    
    -   automatically loaded troughout the code


### Development Tools

-   **Vue UI**
    -   started frm command line `vue ui`
-   Vue Devtools
    -   browser plugin

### Instância vue

-   Fornece um padrão MVVC

-   Como convenção, muitas vezes usamos a variável vm (abreviação de *ViewModel*)
    
-   **Opções**

    -   Definem alguma característica da instância

    -   <u>el</u>
-   Provide the Vue instance an existing DOM element to mount on. It can be a CSS selector string or an actual HTMLElement.
            
    
        -   [[https://vuejs.org/v2/api/\#el]{.underline}](https://vuejs.org/v2/api/#el)
    
-   Pode ser uma ID ou uma tag personalizada
    
-   data
    
-   computed
    
-   watch
    
-   methods
    
-   **Gatilhos (hooks)**

    -   Funções executadas em certos momentos do ciclo de vida da instância
    
-   \<gatilho\>: function (){}
    
-   Created → executa código logo após a instância ser criada
    
-   mounted →
    
-   Updated →
    
-   Destroyed →

### Diretivas

-   Atributos interativos adicionados ao HTML

-   prefixadas com `v-`

-   Parâmetros → agregados que vem depois de dois pontos (:)

-   Modificadores → vinculam algo à diretiva. Vem depois de ponto (.)

-   **Bind**

    -   Mantêm o atributo atualizado com a variável

    -   Bidirecional

    -   Pode ser abreviado para simplesmente dois pontos (:)

    -   *Exemplo:* mantenha o atributo "title" atualizado em relação à propriedade var
        
        ```
        <div id="app"> <span v-bind:title="var"></span> </div>
        ```

-   **If**

    -   Diretiva condicional
-   *Example:* `<p v-if="seen">Agora você me viu</p>`
    
-   **For**
    -   for = "(\<item\> in \<array\>)"

    -   renderizar uma lista de elementos com base nos dados de um objeto/array
        
    -   Pode suportar uma sintaxe especial

    -   v-for = "((\<item\>, \<index\>) in \<array\>)" → sintaxe com index
        
    -   v-for=\"(\<value\>, \<key\>, \<index\>) in object\" → to objects

    -   Its a good practice to provide a unique "key" to each item (especially to allow reuse and reorder existing elements, plus butter debug).

-   **On (event handling)**

    -   On:\<evento\> = "\<código\>"

    -   Adiciona um event listener

    -   Pode ser abreviado para simplesmente @

    -   click → evento de click

-   **Model**

    -   Implementa um two-way data binding (inputs do usuário no html também altera do JS)

-   **Once**

    -   Caga pro data binding: o valor no html não atualiza com a mudança na variavel JS

-   **HTML**

    -   permite renderizar marcação HTML (normalmente ele só considera como texto simples)
    
-   *data bindings* são ignorados

### Componentes

-   Auto-contidos e reutilizáveis

-   Uma vez criado o componente, é só adicionar a sua tag no HTML

-   São considerados pelo Vue como instância e, portanto, aceitam gatinhos e opções
    
-   Podem ser globais ou locais ao escopo de uma instância

-   hey must be registered so that Vue knows about them (globaly or localy)
    
-   every component must have a single root element (html should by wrapped in a single div)
    
-   **Opções**

    -   Como componentes são instancias, permitem as mesmas opções (data, computed, etc)
        
    -   Não pode "el"

    -   tudo que estiver dentro do Data do componente pode ser usado dentro do proprio componente (e apenas dentro, a principio), sem "this"

-   **Propriedades**

    -   Permitem one way data binding entre o componente (pai) e a marcação (filho).
        
    -   Ou seja, de fora pra dentro

    -   props: \[ '\<variáveis\>'\]

-   **Componentes single-file**

    -   Extensão .vue

    -   dividido nas seções \<template\>, \<script\> e \<style\>

    -   Precisa de uma ferramentas de *build* (como Webpack ou Browserify)
        
    -   hot-reloading → recompilação e atualização em tempo real ao salvar o código

-   Emit method

    -   allow to pass event to outside the component

    -   from the component tag, its just add v-on:\<emit-name\>= "action"
        
    -   From inside the component, its just add v-on\<event\> = "\$emit ('\<emit-name\>')

-   Slots

    -   ?

-   Dinamic components

    -   ao invés de dizer explicitamente qual será o componente rendezida, passamos isso como variável dentro de uma tag genérica "component"
        
    -   Passamos a variável através da diretiva "is": \<component v-bind:is="\<componentName\>"\>\</component\>

### Mixins

-   options that will be valid for different components

-   When a component uses a mixin, all options in the mixin will be "mixed" into the component's own options.
    
-   **Conflits**
-   data: component's data have priority
    
-   Hook functions: they all will be executed sequentially (mixin's first)

### Outras coisas

-   Dados computadores e observadores?

### Front UI

-   Vuetify

    -   [[https://vuetifyjs.com/en/introduction/why-vuetify/]{.underline}](https://vuetifyjs.com/en/introduction/why-vuetify/)

-   BoostrapVue

### Production build

* as normal app, as npm package, or as webcomponent (to be embedded in another application)

# Mobile development

## PWA

* Progressive Web App
* aplicação híbrida entre web e mobile
* páginas web (ou sites) tecnicamente  regulares, mas podem aparecer ao usuário como aplicativos tradicionais  ou aplicativos móveis (nativos)
* Filosofia PWA: se o celular não tiver os recursos necessários, ainda assim a página funcionará
* (-) Doesnt work so well on iOS
* (+) Can have icon 
* (+) Can be put on webstore (this is more than just the icon)
* (+) Lightweight in size (usually lighther than native apps)
* (-) May have less performance than native
* **Funcionalidade que diferenciam PWA de uma simples página responsiva**
  * Tráfego offline
  * Instalável
  * Acesso à recursos do celular (ex: acelerometro, contatos, galeria, camera, etc)
  * Ícone na tela home do smartphone
  * Push Notification (even with the browser closed)
  * Splash screen
  * Processos rodando em background
* **Install** 
  * Not required
  * ==How the user install it?==
  * Make it available on stores
    * Não pode ser colocado na playstore/appstore/etc
    * Até dá pra botar nessas stores… mas é meio esquisito (ainda, 2020)
* **Requirements**
  * In order to call a Web App a PWA, technically speaking it should have the following features: Secure contexts (HTTPS), one or more Service Workers, and a manifest file.
  * Service workers
    * A service worker is a script that allows intercepting and control of how a web browser handles its network requests and asset caching. With service workers, web developers can create reliably fast web pages and offline experiences.
  * Manifest file
    * A JSON file that controls how your app appears to the user and ensures that progressive web apps are discoverable
    * It describes the name of the app, the start URL, icons, and all of the other details necessary to transform the website into an app-like format.

## React native

* You can use React Native today in your existing Android and iOS projects
* Works well on TV
* (+) Cross-Platform