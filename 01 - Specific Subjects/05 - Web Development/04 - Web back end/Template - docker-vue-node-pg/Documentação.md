# Passos
1. Ver esse vídeo do link abaixo
2. Criar exatamente o que ele fez dentro de uma pasta qualquer
```
https://www.youtube.com/watch?v=AVNADGzXrrQ&t=558shttps://www.youtube.com/watch?v=AVNADGzXrrQ&t=558s
```
3. Após isso, clonar o repositório do link abaixo dentro da raíz dessa pasta qualquer
```
https://github.com/rexuli/docker-compose-vue.githttps://github.com/rexuli/docker-compose-vue.git
```
4. Após isso, ainda na raíz criar um arquivo chamado docker-compose.yml com o conteúdo abaixo.
```
version: "2"
services:
    node:
        build: ./back/
        restart: always
        command: npm start
        ports:
            - 4000:4000enter no md
        volumes:
            - ./:/app
    front:
        build: ./front/
        ports:
            - 8080:8080
        command: sh -c "npm install && npm run dev"
        volumes:
            - ".:/app"
        environment:
HOST: 0.0.0.0

```
5. Deve funcionar

# Explicão

```
1 version: "2"
2 services:
3    node:
4        build: ./back/
5        restart: always
6        command: npm start
7       ports:
8           - 4000:4000
9        volumes:
10            - ./:/app
11    front:
12        build: ./front/
13        ports:
14            - 8080:8080
15        command: sh -c "npm inenter no mdstall && npm run dev"
16        volumes:
17            - ".:/app"
18        environment:
19 HOST: 0.0.0.0

```
Na linha 1 a versão é relacionada com a versão do Docker, atualmento a maior é a 3 instalada no PC, mas no servidor é um docker mais antigo, logo deve-se colocar a versão com o parâmetro 2. <br />
Na linha 2 diz ao Docker que vai começar a informar os serviços. <br />
Na linha 3 e 11 são os nomes do serviços, podem ser praticamente qualquer um. <br />
Na linha 4 e 12 serão informados o lugar onde está o Dockerfile (quando for abrir eles e ver o package*.json e depois tudo, é para um melhor aproveitamento das camadas do docker, segundo o site próprio do vue). <br />
Na linha 5 significa que em caso de erro, reinicia. <br />
Na linha 6 e 15 são os comandos que executada (imagina como se fosse um pc virtual com terminal, esses comandos rodam nesse terminal). <br />
Na linha 7, 8, 13 e 14 são as portas que está executando na máquina e os que o Docker está deixando aberto ao mundo externo. <br />
Na linha 9, 10, 16 e 17 são os volumes, espécie de comunicação da pasta virtual do Docker com a pasta no computador. <br />
Na linha 18 e 19 é requisito pro front. <br />
