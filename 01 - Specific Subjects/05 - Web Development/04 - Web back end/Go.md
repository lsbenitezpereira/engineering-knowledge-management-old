# Concetualization

* fast
* statically typed
* compiled
* By google
* Garbage collection
* package management
  * `go get <package>`
  * `import {“name”}`
* How to run
  * go run <file>
  * go build <file>: compile to binary
  * go modules?





## Operatores

\+ concat string



## Datatypes

* variables are explicitly declared

* the type can be explicit of infered

* The `:=` syntax is shorthand for declaring and initializing a variable

* **Map**

  * are Go’s built-in associative data type 
  * `a:= make(map[string]int)`

* **Array**

  * Example: `var a[5]int`

* **Slices**

  * array with …   `a:=[]int{1,2,3}`; `a.append(666)`

* **Structs**

  * ex:

    ```
    type person struct {
      name string
      age int
    }
    
    p := person(name: "leo", age: 666)
    ```

* **String**
  
  * `strconv.Atoi()`
* **bool**
* **float64**
* **int**



## Pointers

* `&x`: take address
* `*x = 666`: deference

## Others

* fmt.printLn()
* goroutines: concurrent processes
* code start executing by `main`
* **Flow control**
  * `if <condition> { ... }`
  * `for i:=0, i<5, i++ { ... }`
  * `for i<5 { ... }`: equivalent to a while loop
  * `for index, value := range <iterable-type> { ... }`: equivalent to while with iterartor
* **Functions**
  * `func foo(x int, y int) { ... }`
  * the return types can be defined like: `func square(x float64) (float64) { return math.Sqrt(x)}`
* **Others**
  *  A `defer` statement defers the execution of a function until the surrounding function returns.
  * <u>Pgx</u>
    * pgx is a pure Go driver and toolkit for PostgreSQL.
    * faster and with more features than `database/sql`
    * Use Exec to execute a query that does not return a result set.
    * Extension: concurrent safe connection pool; https://pkg.go.dev/github.com/jackc/pgx/v4/pgxpool
  * 





# Frameworks

## gofiber

* Express inspired
* built on top of Fasthttp
* feito para receber MUITAS requisições
* [Great tutorial](https://www.youtube.com/watch?v=MfFi4Gt-tos) (and its [code](https://github.com/EQuimper/youtube-go-fiber-introduction/blob/master/main.go))
* **Context**
  * https://pkg.go.dev/github.com/valyala/fasthttp#RequestCtx
  * `.bodyParser()`
  * `.SendStatus(404)`
  * `.JSON(value)`
  * `.Params("name")`



## Fasthttp

fast, probable the faster go http framework

## Buffalo

SImilar to django?