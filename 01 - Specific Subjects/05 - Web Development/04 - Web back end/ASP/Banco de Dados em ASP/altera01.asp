<html>

<head>
<title>Altera (consulta)</title>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
style="font-family: Arial" bgcolor="#FFFFFF" text="#5B2D02">
<script language="javascript">
function verifica(frm)
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "NOME" --- INICIO
// Verifica o campo NOME.
{
   {
   var str = frm.nome.value
   // Verifica se foram digitados mais de 10 caracteres.
   if (str.length > 10)
         {
         alert("\nO campo  NOME  est� limitado a\n\n10 caracteres p/efeito de exemplo")
         return false;
         }
   // Return false se o campo NOME estiver vazio.
   if (str == "")
      {
      alert("\nO campo NOME est� em branco.\n\nPor favor entre com o nome.")
      return false;
      }
// TRECHO DESABILITADO POIS N�O ACEITA CARACTERES ACENTUADOS --- INICIO
//   // Return false se os caracteres n�o forem de a-z, A-Z, ou um espa�o.
//   for (var i = 0; i < str.length; i++) 
//      {
//      var ch = str.substring(i, i + 1);
//      if (((ch < "a" || "z" < ch) && (ch < "A" || "Z" < ch)) && ch != ' ') 
//         {
//         alert("\nO campo NOME s� aceita letras e espa�os.\n\nPor favor entre com o nome.");
//         return false;
//         }
//      }
// TRECHO DESABILITADO POIS N�O ACEITA CARACTERES ACENTUADOS --- FINAL
   }
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "NOME" --- FINAL
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "CONTEUDO" --- INICIO
// Verifica o campo CONTEUDO.
   {
   var str = frm.conteudo.value
   // Verifica se foram digitados mais de 10 caracteres.
   if (str.length > 10)
         {
         alert("\nO campo CONTEUDO est� limitado a\n\n10 caracteres para efeito de exemplo")
         return false;
         }
   // Return false se o campo CONTEUDO estiver vazio.
   if (str == "")
      {
      alert("\nO campo CONTEUDO est� em branco.\n\nPor favor entre com o conteudo.")
      return false;
      }
   }
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "CONTEUDO" --- FINAL
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "NUMERO" --- INICIO
// Verifica o campo NUMERO.
   {
   var str = frm.numero.value
   // Verifica se foram digitados mais de 5 numeros.
   if (str.length > 5)
         {
         alert("\nO campo NUMERO est� limitado a\n\n5 numeros p/efeito de exemplo")
         return false;
         }
   // Return false se o campo NUMERO estiver vazio.
   if (str == "")
         {
         alert("\nO campo N�MERO est� em branco.\n\nPor favor entre com o n�mero.");
         return false;
         }
   // Return false se os numeros n�o forem de '0-9'. 
   for (var i = 0; i < str.length; i++) 
      {
      var ch = str.substring(i, i + 1);
      if ((ch < "0" || "9" < ch)) 
         {
         alert("\nO campo N�MERO s� aceita n�meros. \n\nPor favor entre com o n�mero.");
         return false;
         }
      }
   }
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "NUMERO" --- FINAL
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "DATA" --- INICIO
// Verifica o campo DATA.
   {
   var str = frm.data.value
         // Verifica se foram digitados 8 caracteres.
         if (str.length != 8)
            {
            alert("\nO campo DATA requer 8 d�gitos no formato:\n\nDD/MM/AA")
            return false;
            }

       // Verifica se os caracteres s�o n�meros e barra.
         for (var i = 0; i < str.length; i++) 
            {
            var ch = str.substring(i, i + 1);
            if ((ch < "0" || "9" < ch) && ch != "/") 
               {
               alert("\nO campo DATA aceita somente n�meros e um barra no formato:\n\nDD/MM/AA");
               return false;
               }
            }
         // Verifica o valor do dia.
         if ( (str.substring(0, 2) < 1)  ||  (str.substring(0, 2) > 31)  ) 
            {
            alert("\nDia incorreto.");
            return false;
            }
         // Verifica o valor do dia no valor do m�s.
            // Fevereiro
               if ( (str.substring(3, 5) == 2 )  &&  (str.substring(0, 2) > 29)  )
                  {
                  alert("\nFevereiro n�o tem mais que 29 dias.");
                  return false;
                  }
               if ( (str.substring(3, 5) == 2 )  &&  (str.substring(0, 2) == 29)  )
                  { alert("\nVoc� entrou com 29 de Fevereiro...\n\nVoc� tem certeza de que � ano bissexto?"); }
            // Abril
               if ( (str.substring(3, 5) == 4 )  &&  (str.substring(0, 2) > 30)  )
                  {
                  alert("\nAbril n�o tem mais que 30 dias.");
                  return false;
                  }
            // Junho
               if ( (str.substring(3, 5) == 6 )  &&  (str.substring(0, 2) > 30)  )
                  {
                  alert("\nJunho n�o tem mais que 30 dias..");
                  return false;
                  }
            // Setembro
               if ( (str.substring(3, 5) == 9 )  &&  (str.substring(0, 2) > 30)  )
                  {
                  alert("\nSetembro n�o tem mais que 30 dias..");
                  return false;
                  }
            // Novembro
               if ( (str.substring(3, 5) == 11 )  &&  (str.substring(0, 2) > 30)  )
                  {
                  alert("\nNovembro n�o tem mais que 30 dias.");
                  return false;
                  }
         // Verifica o valor do m�s.
         if ( (str.substring(3, 5) < 1)  ||  (str.substring(3, 5) > 12)  ) 
            {
            alert("\nM�s incorreto.");
            return false;
            }
         // Verifica o valor do ano.
         if ( (str.substring(6, 8) < 1)  ||  (str.substring(6, 8) > 99)  ) 
            {
            alert("\nAno incorreto.");
            return false;
            }
         // Verifica posicionamento da barra.
         if ( str.substring(3, 4) == "/"  || str.substring(4, 5) == "/" ) 
            {
            alert("\nBarra misturada com o m�s.");
            return false;
            }
         if ( str.substring(0, 1) == "/"  || str.substring(1, 2) == "/" ) 
            {
            alert("\nBarra misturada com o dia.");
            return false;
            }
         if ( str.substring(6, 7) == "/"  || str.substring(7, 8) == "/" ) 
            {
            alert("\nBarra misturada com o ano.");
            return false;
            }
         if ( str.substring(2, 3) != "/"  ||  str.substring(5, 6) != "/" ) 
            {
            alert("\nBarra misturada com a data.");
            return false;
            }
   }
   return true;
}
// TRECHO JAVASCRIPT PARA VERIFICAR O CAMPO "DATA" --- FINAL
</script>
<%@ LANGUAGE=VBScript%>
<% 
Dim recnum
recnum     =  replace(request.form("recnum"), "'", "''")

'***Declarando Conex�es com Banco de Dados***
Set DB = Server.CreateObject("ADODB.Connection")
DB.Open "DBQ=D:\inetpub\wwwroot\asp\db_folder\db_file.mdb;Driver={Microsoft Access Driver (*.mdb)};"

'***Pesquisando registros***
Comando = "SELECT * FROM db_table WHERE recnum = " & (recnum) & ""
Set RecSet = DB.Execute(Comando)
%>
<%if RecSet.EOF then%>
<div align="center"><center>

<table border="1" width="150" cellspacing="0" cellpadding="0">
  <tr>
    <td width="190" align="center">N�o Encontrado!</td>
  </tr>
</table>
</center></div>

<form>
  <div align="center"><center><p><input type="button" value="Voltar" name="B1"
  onClick="history.back()"></p>
  </center></div>
</form>
<a target="direita2" href="../fontes/altera01_asp.asp">

<p align="center"></a>&nbsp;</p>

<p><%else%> <%do while NOT RecSet.EOF%> </p>
<%
'***Necessario apenas por que a data do servidor e' mm/dd/aa - INICIO
'mes  = Left(RecSet("data"),2)
'dia  = Mid(RecSet("data"),4,2)
'ano  = Right(RecSet("data"),2)

'data = dia & "/" & mes & "/" & ano
'***Necessario apenas por que a data do servidor e' mm/dd/aa - FINAL
%>

<form method="post" action="altera02.asp" onsubmit="return verifica(this);">
  <input type="hidden" name="recnum" value="<%= RecSet("recnum")%>"><div align="center"><center><table
  border="0" width="103" cellspacing="0" cellpadding="0">
    <tr>
      <td width="290" colspan="2" align="center">Digite - Alterar</td>
    </tr>
    <tr>
      <td width="290" colspan="2" align="center">Nome</td>
    </tr>
    <tr>
      <td width="290" colspan="2" align="center"><small><font face="Arial"><div align="center"><center><p><small><input
      type="text" name="nome" size="20" value="<%= RecSet("nome")%>"></small></font></small></td>
    </tr>
    <tr align="center">
      <td width="290" colspan="2" align="center">Conte�do</td>
    </tr>
    <tr align="center">
      <td width="290" colspan="2" align="center"><small><font face="Arial"><div align="center"><center><p><small><textarea
      name="conteudo" rows="1" cols="17"><%= RecSet("conteudo")%></textarea></small></font></small></td>
    </tr>
    <tr align="center">
      <td width="152" align="center">N�mero</td>
      <td width="138" align="center">Data</td>
    </tr>
    <tr align="center">
      <td width="152" align="center"><div align="left"><p><small><small><font face="Arial"><input
      type="text" name="numero" size="9" value="<%= RecSet("numero")%>"></font></small></small></td>
      <td width="138" align="center"><div align="right"><p><small><small><font face="Arial"><input
      type="text" name="data" size="9" value="<%= RecSet("data")%>"></font></small></small></td>
    </tr>
    <tr align="center">
      <td width="152" align="center"><div align="left"><p><small><small><font face="Arial"><input
      type="submit" value="   Altera  "></font></small></small></td>
      <td width="138" align="center"><div align="right"><p><small><small><font face="Arial"><input
      type="reset" value="  Limpa  "></font></small></small></td>
    </tr>
  </table>
  </center></div>
</form>
<%RecSet.MoveNext%>
<%loop%>

<form>
  <div align="center"><center><p><input type="button" value="Voltar" name="B1"
  onClick="history.back()"><br>
  Os campos est�o limitados (JavaScript):<br>
  Nome = 10, Descri��o = 10<br>
  N�mero = 5 e o formato da Data:<br>
  dia/m�s/ano = 30/12/98<br>
  <br>
  </p>
  </center></div>
</form>
<%end if%>
</body>
</html>
