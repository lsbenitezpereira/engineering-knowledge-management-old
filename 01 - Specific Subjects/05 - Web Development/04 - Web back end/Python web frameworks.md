# Conceptualizatinon

* To general python, see the file *Data Science - Python* 
* To servers and web related stuff, see *Computers Networks*

*  * 

## **marshmallow**

* Serialize and deserialize
* allows to work with nested structures 
* converting complex datatypes, such as objects, to and from native Python datatypes

## JSON Schema

* ==should not be here==
* Automatic data model documentation 

## OpenAPI

* ==should not be here==
* previously known as *Swagger*
* make easier to make automatic documentation
* organizes the organization’s taxonomy
* language agostic, software agnostic 
* high level description
* based on JSON Schema
* Schema
  * abstract description of the API
  * 



# Others

* **Django**
  * full-stack web framework
  * More robust and complete than Fask
  * Django-admin 
    * automatic admin interface
    * reads metadata from your models to provide a quick, model-centric interface
  * ==have?== a database abstraction layer 
* TurboGears
* web2py
* Pylons
* Zope
* WebWare

# FastAPI

* Simple and good for ML
* Like Flask, but with more built-in features 
* Based on (and fully compatible with) the open standards for APIs: OpenAPI and JSON Schema.
* (+) high-performance, specially for parallel requests
* ASGI used: Starletter (so also above uvicorn)
* Python 3.6+
* MIT license
* **Security and authentication integrated**
  * OAuth2
* **Automatic docs**
  * interative interface: Swagger UI (in route `/docs`) or ReDoc (in `/redoc`)
  * The OpenAPI schema is available in `http://127.0.0.1:8000/openapi.json`
* **Typer**
  * CLI?
* **FastAPI class**
  * inherits directly from Starlette.
* **Endpoint decorator**
  * Documentation parameters: summary, description, response_description
  * Other parameters: deprecated
  * You can write Markdown in the docstring (not in the decorator,but in the begining of the function)
* **Middleware**
  * function that works with every request/response before it is processed/sent
  * you can use, for instace, to allow CORS (situations when a frontend running in a browser has JavaScript code that communicates with a backend, and the backend is in a different "origin" than the frontend)

## Deployment considerations

* Run server with: `uvicorn main:app --reload`
* **Docer - stack of images**
  * Tiangolo loves several layers of images, lol
  * the base server is [uvicorn-gunicorn-docker](https://github.com/tiangolo/uvicorn-gunicorn-docker/blob/master/docker-images/python3.7.dockerfile)
  * the higer level is [uvicorn-gunicorn-fastapi-docker](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker/blob/master/docker-images/python3.7.dockerfile)
  * your dockerfile is usually pretty simple, just `FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7`
  * you can run with something like: `docker run -p 80:80 --rm -v "$(pwd)":"/app" --name foo-running foo /start-reload.s`

## Parsing input

* Uses Pydantic
* If the type is wrong, the user recives a clear message 
* validation constraints as maximum_length or regex
* Validation and conversion for more exotic types, like:
  * JSON, cookies, forms
  * URL
  * Email.
  * UUID
  * decimal
  * binary treated as str
  * times
    * ISO 8601 format
    * datetime, example: `2008-09-15T15:53:00+05:00`)
    * date, example: `2008-09-15`
    * time
    * timedelta (float of total seconds)
* All the input classes inherits from the same common `Param` class
* **Query parameters**
  * on URL (`?key=value`)
  * `from fastapi import Query`
  * `parameterName: str = Query('defaultValue', min_length=3, max_length=50)`
  * String validations: min_length, max_length, regex
  * Number validation: gt (greater than), lt, ge, le,  
  * documentation: title, description, deprecated
  * Others: alias (synonim name), example
  * put the parameter `*` changes the required/optional order… I dont know
* **Path parameters**
  * On in the middle of URL (`/value/`)
  * `from fastapi import Path`
  * similar sintax do Query
* **Body parameters**
  * `from fastapi import Body`
  * similar sintax
  * if you parameters in the only body one, it will merge with the request itself
  * to keep an separete key, use `embed=True`
  * Aditional validations are available in Field (parameters still come on body)
  * `from pydantic import Field`
  * nested types
    * List subtype: `from typing import List; my_list: List[str]`
    * accept any `dict` as long as it has `int` keys with `float` values: `Dict[int, float]`
* **Cookie parameters**
  * just like the ones before
  * `parameterName: str = Cookie(None)`
  * ==study more==
* **Header Parameters**
  * variable come in the HTTP header	
  * And don't worry about underscores in your variables, FastAPI will take care of converting them
* **Form Fields**
  * The way HTML forms (<form></form>) sends the data to the server normally uses a "special" encoding for that data, it's different from JSON.
  * `username: str = Form(...)`
* **Files**
  * `file: bytes = File(...)`
  * https://fastapi.tiangolo.com/tutorial/request-files/
  * UploadFile class allow to deal with big files, receive metadata, etc

## Response

* Response Model

  * enforce the response to be a Item class: `@app.post("/items/", response_model=Item)`
  * Convert the output data to its type declaration.
  * Validate the data.
  * Add a JSON Schema for the response, in the OpenAPI path operation.
  * Will be used by the automatic documentation systems.
  * Will limit the output data to that of the model. We'll see how that's important below.
  * https://fastapi.tiangolo.com/tutorial/response-model/
* Response Status Code
  * ==?????==
  * `raise HTTPException(status_code=404, detail="Item not found")`
  * Because it's a Python exception, you don't `return` it, you `raise` it
  * the details comes in a JSON response

## DB

* ORganizing classes models: https://fastapi.tiangolo.com/tutorial/extra-models/

## Others

* **Internal Events**
  * Hooks to certains moments of the app’s lifecicle
  * functions with decorators
  * startup: your application won't start receiving requests until all the startup event handlers have completed.
  * shutdown


# Flask

  * Python Framework for back-end web development

  * lightweight and minimalist, but extensible

  * Creates a microwebserver

  * Flask class

      * central callable object that implements the actual application

        ```python
        from flask import Flask
        app = Flask(__name__)        
        ```
        
        * To run the application, command on terminal:

            ```
            export FLASK_APP=hello.py
            flask run
            ```

      * Have a simple builtin server, which is good enough for testing but probably not what you want to use in production

      * if you wanna run from inside docker, you should run with `host=0.0.0.0`

      * Debug: the server will pip install Flaskreload itself on code changes, and it will also provide you with a helpful debugger if things go wrong

  * **Routing System**

        * Werkzeug routing system
        * automatically order routes by complexity
        * <u>Endpoints</u>
              * Function with the decorator `@app.route('/...')`
              * Keyword argument: `<variableName>` or `<type:variableName>` (see [types](https://flask.palletsprojects.com/en/1.1.x/quickstart/#variable-rules))
              * By default, a route only answers to GET requests
        * <u>`request` object</u>
              * This is a global object… I dont know, flask does some magics about local contexts
              * `.method` (‘GET’, ‘POST’, etc)
              * `.form['parameterName']` (read parameter from body, not JSON)
              * `request.args.get('parameterName')` (read parameter from URL)
              * Other ways of reading parameters: https://stackoverflow.com/a/16664376/12555523
        * <u>Response</u>
              * https://flask.palletsprojects.com/en/1.1.x/quickstart/#about-responses
              * If you return a `dict` it will be converted to a JSON response
              * Use `jsonify()` to return JSON from `list`
      
  * **Template engine**

        * Jinja2 system
        * templates and static files are stored in subdirectories within the application’s Python source tree, with the names `templates` and `static` respectively
        * Para referenciar arquivos estáticos, use `url_for()`
        * <u>Templates</u>
            * To return a template, use `render_template('hello.html')`
            * Variables: `{{ name }}`
      
* **Unit testing**

     * You’ll have problems because the endpoint function depends on a request object

     * Flask provides a context manager:

          ```python
          from flask import request
          
          with app.test_request_context('/hello', method='POST'):
              # now you can do something with the request until the
              # end of the with block, such as basic assertions:
              assert request.path == '/hello'
              assert request.method == 'POST'
          ```

* **Plugins and tools**

     * Flask-MonitoringDashboard: see more in *SRE*
     
* **Machine Learning deploy**

     * [Tutorial](https://towardsdatascience.com/simple-way-to-deploy-machine-learning-models-to-cloud-fd58b771fdcf) and [code](https://github.com/tanujjain/deploy-ml-model) 
