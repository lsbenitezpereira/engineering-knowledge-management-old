# Software engineering in web

* APIs
  * *Application Programming Interface*
* ==iframes are a type of API?==
  * *Ex*: se você quiser procurar por informações de uma empresa no
    reclame aqui, pode fazer um crawler que irá vascular as páginas
    deles ou usar a API fornecida pela ReclameAqui (nem sei se eles
    têm), que já te retorna os dados que você quer em um formato
    compreensível por máquinas.
* Two-layer Web application architecture:

![1546019334324](Redes de Computadores - Images/1546019334324.png)

## WebServices

-   *Web services são componentes de software acessados pela Internet e
	fornecem uma funcionalidade específica e útil. Aplicações são
	construídas integrando esses web services, os quais podem ser
	fornecidos por empresas diferentes. A princípio, essa ligação pode
	ser dinâmica, para que a aplicação possa usar web services
	diferentes toda vez que é executada. (ver capitulo 19 do
	sommerville)*
-   ==É apenas a formalização e estruturação daquilo que eu sempre fiz==
-   Para que servem

	-   Facilitam a integração de sistemas diferentes (independente de
	    SO ou linguagem de programação utilizada) que se comunicam via
	    HTTP

	-   Padroniza a comunicação e facilita o desenvolvimento

	-   Permitem enviar e receber dados em formato *XML* (uma “linguagem
	    universal”)

![1546016964551](Redes de Computadores - Images/1546016964551.png)

-   **WebService SOAP**
    -   Normatizado pelo W3C
    -   baseado em XML
    -   O protocolo possui uma linguagem para descrição dos serviços disponibilizados, a Web Service Description Language (WSDL)
-   **WebService REST**

	-   Representational State Transer
	-   set of constraints to be utilized for creating HTTP web services
	-   architectural style
	-   Totalmente baseado em HTTP (um pouco diferente do SOAP)
	-   tenta ao máximo usar os métodos do HTTP corretamente
	-   You can consider just as “canonical HTTP”
	-   Melhor performance que o SOAP
	-   designed around resources, which are any kind of object, data, or service that can be accessed by the client.
	-   Clients interact with a service by exchanging *representations* of resources. Many web APIs use JSON as the exchange format.
	-   Design rules
	    * **Client-Server**: There should be a separation between the server that offers a service, and the client that consumes it.
	    * **Stateless**: Each request from a client must contain  all the information required by the server to carry out the request. In  other words, the server cannot store information provided by the client  in one request and use it in another request.
	    * **Cacheable**: The server must indicate to the client if requests can be cached or not.
	    * **Layered System**: Communication between a client and a  server should be standardized in such a way that allows intermediaries  to respond to requests instead of the end server, without the client  having to do anything different.
	    * **Uniform Interface**: The method of communication between a client and a server must be uniform.
	    * **Code on demand**: Servers can provide executable code  or scripts for clients to execute in their context. This constraint is  the only one that is optional.
	-   **Maturity level**
	    -   Proposed by Leonard Richardson in 2008
	    -   If an application completelly implement level 3, is called HATEOAS
	    -   Level 0: Define one URI, and all operations are POST requests to this URI.
	    -   Level 1: Create separate URIs for individual resources.
	    -   Level 2: Use HTTP methods to define operations on resources.
	    -   Level 3: Use hypermedia (HATEOAS, described below).

### HATEOAS

* Hypertext as the Engine of Application State.

* RESTfull pattern

* enable navigation to related resources.

*  it should be possible to navigate the entire set of resources without requiring prior knowledge of the URI scheme

* One of the return sttufs is `"links": []`

  

### REST best practices

* When possible, resource URIs should be based on nouns (the resource) and not verbs (the operations on the resource).
* Avoid creating APIs that simply mirror the internal structure of a database
* collections: group of entities accessed togueder
* organize routes as hierarquies
*  the `/customers/5/orders` usually represent all of the orders for customer 5
* avoid "chatty" web APIs that expose a large number of small resources; [post about it](https://docs.microsoft.com/pt-br/azure/architecture/antipatterns/chatty-io/)
* Filter and paginate data (with limit and offset parameters)
* [good guidelines](https://docs.microsoft.com/pt-br/azure/architecture/best-practices/api-design)
* **API versioning**
  * if big changes have to be made
  * implemented as parameter: `https://adventure-works.com/customers/3?version=2`
  * implemented as path: `https://adventure-works.com/v2/customers/3`
*  **Open API Initiative**
  * created by an industry consortium to standardize REST API descriptions across vendors
  * opinionated guidelines on how a REST API should be designed

## Microservices

* architecture pattern
* mdularizar em vários serviços cada um com algo muito específico
* é o oposto à fazer uma aplicação monolítica, onde um serviço roda lógica inteira do produto
* decouple the databases by giving each microservice its own database
* Interaction with data only by using the exposed methods from the service, rather than going directly to the database
* Do not mix up singular and plural nouns (plural is more common?)
* there is a trend monolith -> microservices
* projetos pequenos permitem ter equipes pequenas, o que é muito mmais ágil
* Discussão
  * “Nada como um monolito bem pensado.”
  * microservices não são legais em 99% dos casos
  * Uma aplicação razoável é quando muitas coisas precisam rodar em parelelo

## Virtualization

* virtual machines… everywhere
* Enable to share resources of on fuck-yeah hardware between several virtual machines
* (+) cost saing
* (+) easy to setup
* (+) easy to move and recover from disaster
* (-) less performance
* Each VM will have its own OS kernal (diffent of Container)
*  **hypervisor**
  * provides emulated hardware to a VM
  * can be installed as the operating system (on the bare metal of the hardware host server) or above another operating system 

* **Software tools**
  * VMware Workstation: ?
  * <u>VirtualBox</u>
    * ?
    * Use USB in virtualbox: https://www.techrepublic.com/article/how-to-enable-usb-in-virtualbox/
    * Makig the live iso persistent
      - you can create very quickly simple VM and boot from the CD/DVD (or the ISO) and to preserve changes made, you could "save the VM state"
      - [https://forums.virtualbox.org/viewtopic.php?f=1&t=48898](https://forums.virtualbox.org/viewtopic.php?f=1&t=48898)
      - what about `VBoxManage convertfromraw LiveDVD.iso Linux.vdi` ? works? no?
  * <u>Vagrant</u>
    * To automate the configuration of VMs
    * things become like docker, in the automation sense

### Formats

* .vmdk (contains all the information of a virtual machine) (formato de VMWare)
* ==.iso?==
* VDI (only for VirtualBox?)
* VDH 
* QCOW2 (KVM, Xen)
* QED (KVM)
* VHD (Hyper-V)
* **exporting a VM image to bootable device**
  * easier with Unpartitioned HDD
  * then you can boot nativelly, with the host OS overhead
  * the format should be RAW… what exaclty is that?
  * escrever o RAW para um HD físico em sdb: `sudo dd if=./sdb.raw of=/dev/sdb`
  * mount as a loopback device: `mount -o loop converted.raw /mnt`
* **Convertions**
  * <u>VirtualBox</u>
    * VDI to RAW ([Showrt explanation](https://superuser.com/a/241489)) ([longer explanation](https://askubuntu.com/a/32506)) ([another explanation]( https://askubuntu.com/a/588427)): `VBoxManage clonehd sdb.vdi sdb.raw –format RAW`
    * RAW de um disco que esteja no sdb: `sudo dd if=/dev/sdb of=./sdb.raw`
    * RAW para o formato VDI: `VBoxManage convertdd sdb.raw sdb.vdi –format VDI`
    * RAW para VMDK: `VBoxManage convertdd sdb.raw sdb.vmdk –format VMDK`
    * VDI para VMDK: `VBoxManage clonehd sdb.vdi sdb.vmdk –format VMDK`
    * VMDK para VDI: `VBoxManage clonehd sdb.vmdk sdb.vdi –format VDI`
    *  
    * Convert static to dynamic: VBoxManage clonemedium disk "<in>.vdi" "<out>.vdi" –variant Standard
  * <u>VmWare</u>
    * VMDW para RAW ([explanation](https://www.howtoforge.com/converting-a-vmware-image-to-a-physical-machine)): `qemu-img convert -f vmdk sdb.vmdk -O raw sdb.raw`
  * <u>qemu-img</u>
    * convert between several formats

## Containerization

* We’ll write here mainly about **Docker**, bu there are other (Rocket, Warden, Windows Containers…)
* Alternative to Virtualization
* define and package a runtime environment for a process

-   Complely isolate the apps/threads/all of your application
-   

![1546016987898](Redes de Computadores - Images/1546016987898.png)

-   **Advantages**

    -   The application runs always on the same environment
    -   No interference between projects
    -   Fast deploy (it’s just put to work, no need to install and
        configure lotsa thing)
    -   lightweight
    -   “easy” to configure
    -   higly automatable
    -   the artifact generated is portable and easily shared
    -   The resources are isolated 
-   **How it works**
    -   The kernel of the OS is not repeated, we use just it’s API (application layer)
    -   is not possible to deploy applications that require different OS kernels
    -   the container runs as a service on top of a common operating system kernel.
    -   the abstractions from different kernels is made by docker 
    -   Todos os processos dentro do container vão rodar em foreground, sempre
    -   The host system only needs to have the container runtime software installed
    -   Container instances share the operating system kernel of their host system, so they
        can’t run a different OS. Containers can, however, run different distributions of the
        same OS—for example, CentOS Linux on one and Ubuntu Linux on another
-   **Orquestration**

    -   Managing several containers
    -   Usually there is a lot of dependencies and relationships amont the containers 
    -   Usually one single machine wont be enough for running all the containers
    -   The term “orquestration” usually refers to tools to deal with such complexity, like kubernets
    -   (+) high availability
    -   (+) scalability
    -   (+) rolling update
    -   (+) disaster recovery
    -   (-) complex
    -   (-) ==overhead?==

### Basic docker

* 
* [Tutorial - Basic python image](https://www.docker.com/blog/containerized-python-development-part-1/)

-   **Docker file**

    -   Use code to configure the environment
    -   plain text file
    -   Creates the image
    -   <u>Multi-stage builds</u> 
        -   ?
        -   speed up rebuilds, diminish final image size
    -   <u>main commands</u>
        -   RUN: ?; each run created a new layer?
        -   COPY: from host to container
        -   ADD: ?
        -   CMD: entrypoint, program that wil be running in foreground; you can have several RUNs, but only one CMD

-   **Image**

	-   Snapshop of the system (all that you need, in a single file)
	-   We can find images ready shared
	-   :keyboard:Shell: `docker build .` Constructs the image (you need to rebuild to reflect new changes)
	-   layer: ?
	
-   **Container**

    -   Instance of image

    -   Recommended to run a single main process per container

    -   `docker run <imageID>`: creates and starts a container; if the image is not present locally, already does the pull

    -   `docker start <containerID>`: restart an stoped container

    -   `docker inspect <container-id>`

    -   Stop all running containers: docker stop $(docker ps -aq)

    -   Remove all containers: docker rm $(docker ps -aq)

    -   

    	![1546017022932](Redes de Computadores - Images/1546017022932.png)
    	
    -   <u>Build Arguments</u>

        -   can be defined with a default value `ARG=VAL` in the Dockerfile or just with the name
        -   While building an image, we use the `--build-arg <key>=<val>` flag with the `$ docker build` command to override or provide values of these variables.
        
    -   <u>options do docker run</u>

        -   -p
        -   `--name`
        -   --net
        -   `-e`: pass environment variables
        -   `--env-file`: pass environment variables from a file
        -   `-v hostDir:containerDir`: create anonymous volume
        -   `-v name:containerDir`: create named volume
        -   adding something (like `docker run . /start.sh`) at the end of the command, replaces the default "command" with this one?

    -   <u>More details on containers</u>

        -   stack of images
        -   usually the base image is an small linux, like alpine
        -   A container’s main running process is the `ENTRYPOINT` and/or `CMD` at the end of the `Dockerfile`. 

-   **Volume**
	
	-   Compartilha arquivos entre SO e container
	-   Dessa forma, os arquivos vão ser automaticamente atualizados no
	    container, sem precisar fazer o rebuild
	-   `docker run -v <Hostfolder>:<VolumeFolder>`
	-   Tip: Mount your code as a volume to avoid image rebuilds: Any time you  make a change to your code, you need to rebuild your Docker image (which is a manual step and can be time consuming). To solve this issue, mount your code as a volume
	-   named volumes: ?; the hostDir is by default /var/lib/docker/volumes
	-   you can specify the driver and …?
	-   
	
-   **Network**

    -   To deal with internal networking
    -   by default, docker creates it’s own isolated network
    -   `docker network inspect`
    -   each container is assigned an IP address for every Docker network it connects to
    -   the Docker daemon effectively acts as a DHCP server
    -   Each network also has a default subnet mask and gateway.
    -   connect a running container to multiple networks using `docker network connect`.
    -   You can specify a proxy for the containers in  `~/.docker/config.json`, or with env variables
    -   Expose command: ?; does not publish the port, can be udp with the postfix `/udp`
    -   Published ports
        -   make a port available to services outside of Docker
        -   To pubish, use the `--publish` or `-p` flag in the docker run
        -   creates a firewall rule which maps a container port to a port on the Docker host to the outside world
        -   you can specify a IP to receive from
        -   You can specify UDP, `docker run -p 53160:53160/udp` (forward a UDP port from my Docker container to the host machine)
        -   “Opening ports is only needed when you want to Listen for the requests not sending.”
    -   Type of network: bridge, an overlay, a macvlan network, or a custom network ??

-   **Labels**

    -   mechanism for applying metadata to Docker objects (images, containers, anything)
    -   key-value pair
    -   `LABEL <key>=<value>`
    -   If you `docker inspect` your image(s) you’ll be able to see those labels
    
-   **Other commands**

    -   `docker ps` (lista os containers em execução; `-a` mostra inclusive os parados)
    -   `docker image ls`
    -   `docker container ls`
    -   `sudo docker system prune --all`
    
-   **Other tips**

    -   In produciton, Always run an NTP client on the Docker host and within each container  process and sync them all to the same NTP server. If you use swarm  services, also ensure that each Docker node syncs its clocks to the same time source as the containers

### Docker compose

* allow to configurate several containers in one single file
* it defines the service, not the image, so you’ll still need a dockerfile
* All optional parameters of docker run (like `-v` for volumes) are written in a single file
* docker-compose maps the host address of the services to the name of the service, so it can be accessed by other services
* by default looks for the file named docker-compose.yml/.yaml in the current directory, however, you can override the filepath using -f or --file flag
* `docker-compose down -v`: exclui também inclui volume
* The `version` key specifies the version of the Compose file to use
* All relative paths (of the host) used inside the Compose file will be relative to the `docker-compose.yml` file
*  Instead of the `image` field, we specify a path to the context directory using the `build` field
* Having `depends_on` field in the service will help Docker compose to construct a startup order
* `docker-compose up`: start the services from the docker-compose.yml file
* healthcheck configuration ==????==
* **Project**
  -   running compose?
  -   By default, Docker Compose uses the **project directory name** as the project name. To provide a custom project name, we can use the `--project-name` 
  -   `docker ps --filter "label=com.docker.compose.project" -q | xargs docker inspect --format='{{index .Config.Labels "com.docker.compose.project"}}'| sort | uniq`: List all Docker Compose projects currently running  
* **multiple Compose files**
  * merge configurations from right to left; overrides
  * Using the `-f` flag
* **volumes**
  * Named volumes: declared at the end of the file, at the same leve as the services
* **Network**
  * Docker Compose creates a new bridge network per project and all containers of that project join this network
  * See more in *Docker - Network*
* **Restart behavior**
  * `restart` field
  * no (default), on-failure, always, unless-stopped (will restart if the container wasn’t stopped manually.)
* **Environments variables**
  -   An environment variable can be used to substitute variables in `docker-compose.yml` file
  -   for instance, `image: nginx:${NGINX_VERSION}`
  -   You can use the `${NGINX_VERSION:-latest}` syntax to fallback to `latest` value if the `NGINX_VERSION` environment variable is not present or empty
  -   You can specify the path to a custom `.env` file (*such as* `*.prod.env*`) using `--env-file` flag with the `$ docker-compose` command
  -   by default looks for a file named `.env`
  -   

### **Kubernets**

-   Or *k8s*
-   container orchestration
-   containers are organized on clusters
-   Build and run and connect containers on multiple hosts
-   (+) facilitates to deploy containerized apps to a cluster of computers instead of a single machine
-   other options: Docker Swarm, Mesos Marathon
-   ==Kubernetes Deployment Controller== can be created using the kubectl run command or by configuring a YAML file (which is the recommended way)
-   [tutorial deploy hello owrld fastAPI](http://richard.to/programming/hello-world-fast-api-kubenetes.html)
-   [another fastAPI tutorial](https://medium.com/codex/a-journey-with-kubernetes-part-4-apis-19b311290a4f)
-   [and another fastAPI tutor](https://betterprogramming.pub/python-fastapi-kubernetes-gcp-296e0dc3abb6)
-   **Concepts**
    -   <u>Pods</u>
        -   abstraction of container
        -   group of one or more containers that share network and storage
        -   can only be created within a cluster
        -   By default, the Pod is only accessible by its internal IP address within the Kubernetes cluster.
        -   each pod will receive one IP (internal)
        -   are ephemeral
    -   <u>ReplicaSets</u>
        -   blueprint/abstraction for pods
        -   groups of pods to be deployed together
        -   are usually made automatically in the Deployement
    -   -   
    -   <u>Deployement</u>
        -   blueprint/abstraction for pods
        -   will already create the necessary ReplicaSets
        -   manifest specs
            -   replicas
            -   selector: defines how the Deployment finds which Pods to manage
            -   template: define the pods
    -   <u>StatulSets</u>
        -   Similar to deployment
        -   for things like database
        -   guarantee consistency
        -   just as Deployment,manges replication, etc
        -   as opposite to a Deployment, the pods are not identical and interchangable; each pod will have their own identity, maintaining the data consistency;
        -   if data loss is completely unaceptable, you must configurate a persisent volume to the StatefulSets??
        -   how it works
            -   within the replicas, one pod will be the master and the others the workers
            -   only the master can write to the data storage
            -   the pods are continously sycronizing their data storage
            -   when a new pods joins, it first clones the data storage from the last pod
            -   when a pod dies and restart, it will be restarted with the same identity
    -   <u>Service</u>
        -   exposed pods
        -   have a “permanent” IP address; is maintained even if the pod dies and restart
        -   defines a logical set of Pods and a policy by which to access them
        -   external service: normal one; receive an external IP
        -   internal service: not exposed
        -   
        -   usually you’ll need both service and deployment
        -   if a service have multiple input ports, they must be named
        -   One service, one deployment (unless you are doing A/B, canary, etc)
        -   Types:
            -   custerIP (default): inernal?
            -   headless: to address ndividual pods? internal?
            -   nodePort: acts like a Ingress; not recommended; external?
            -   loadbalancer: ?; you can use an external service like fromm aws? external?
    -   <u>Ingress</u>
        -   Similar to service
        -   fowarding
        -   ==substitute the service, or just foward to it?==
    -   <u>Nodes</u>
        -   represents a worker machine?
        -   if it’s on AWS, node=EC2
        -   have an internal network
        -   every node must have a container runtime
        -   Types: worker or master
        -   join: when a new node is created, it must _join_ a cluster?
    -   <u>cluster</u>
        -   group of nodes
        -   master node: or ==*Control Plane*==; manage and coordinate;always required; a cluster can have several master nodes
        -   worker nodes: ?	
        -   takes care of handling and distributing your programs into particular nodes
        -   the master is the only entrypoint to the cluster
        -   API server: ?
        -   scheduler: ?
        -   controller manager: ?; detect changes in the nodes (like a node that crashed)
        -   etcd: stores the cluster state? key-value DB?
    -   <u>proxy</u>: is needed to expose the application for use by other services; routes internal netoworking between the nodes, choosing the most efficient routes;
    -   <u>namespaces</u>
        -   organize resources into groups	
        -   One resource can use resoures from several namespaces? or just some can be shared?
        -   some resources can be shared, like Secrets or ConfigMap?
        -   resouce reosuces cant be bounded by a namesapce: volumes, ?
        -   the internal references will be <nameResource>.<namespace>
        -   good uses: group shared resources; separate A/B deployment or dev/prod; 
    -   <u>Ingress</u>
        -   routing rules
        -   foward external requests to the internal service
        -   ==why just a Service is not enough==
        -   IngressController: auxiliry pod; Every Ingress needs one; evaluate the rules and manages the redirections
        -   manifest specs
            -   rules: ?; the “http” is the internal protocol, not necessarily how the ingress receive
            -   TLS: you set the tls.crt and tls.key
    -   <u>PersistentVolume</u>
        -   you can set several backends/drivers
        -   acessible to the whole cluster, cant be namespaced
        -   the data will persist in the backend, the cluster itself dont care about the data
        -   PersistentVolumeClaim: define requirements for the storage; how much the application needs?
        -   StorageClass: bastraction over persistent volumes; creates persistent voluems automatically; defines the provisioner (ex: aws-ebs) and the parameters (ex: 10GB)
        -   [great tutorial on volumes](https://newrelic.com/blog/how-to-relic/how-to-use-kubernetes-volumes)
-   **more about the cluster’s API server**
    -   There are clients/SDK to abstract it; [python cient](https://github.com/kubernetes-client/python/)
    -   
-   **Manifest file**
    -   Types: Deployment, Pod, Service, Replicasets, Secret, Jobs and much more.
    -   2 parts: metadata, specifications|data
    -   status: it’s part of the configuration file, but is automatically generated; continously updated
    -   
    -   metadata: name, namespace
    -   each kind have it’s own specifications
    -   
    -   pod specs: ports, env
    -   service specs: type (loadBalancer or ?)
    -   configMap data: …?
    -   secret data: …?
    -   
    -   selector - defines which Pods should be managed by the Deployment.
    -   
    -   Secret: for credentials and etcs; there is one specific type for TLS
    -   config-map: ?
    -   
    -   you can write more than one configuration in the same file, separing them by `---`
-   **Tools**

    -   

    -   -   

    -   <u>kubectl</u>
        -   command-line tool
        -   to interact with the cluster
        -   allows you to run commands against Kubernetes clusters (minikube, kind… any)
        -   `kubectl proxy`: creates a connection between your cluster and the virtual terminal window
        -   `kubectl run <name> --image=<imgPath> --port=<portNumber>`: ?
        -   `kubectl create|edit|delete deployment <name>`
        -   `kubectl apply -f <file>`: takes a configuration file and created all that it specifies; difference to “create”??; can also update a running stuff; 
        -   `kubectl delete -f <file>`: all that was created by the file
        -   
        -   `kubectl cluster-info`
        -   `kubectl describe pod <podName>`: ?
        -   `kubectl logs <podName>`
        -   `kubectl get all|nodes|pods|services|replicaset|deployment`: print names, status, etc
        -   `kubectl exec --it <podName> bash`
        -   
        -   `kubectl port-forward <podName> 9000:9000`: open port to exterior world, in case you dont wanna use a service
        -   `kubectl rollout restart deployment <name>`: perform rolling update
        -   
        -   [Tutorial - Configure to pull image from AWS ECR](https://dpjanes.medium.com/kubernetes-how-to-accessaws-ecr-bd1e6e6c061)

        -     
        -    Extensions (like [Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)) can usually be deployed in one line
        -     
        -    
        -   

    -   <u>kind</u>
        -   Creates the cluster
        -    tool for running local Kubernetes clusters using Docker container “nodes”
        -   stand for “Kubernetes IN Docker”.
        -   uses single container of Docker
        -   “node” kubertes = one container, que roda dentro dele vários containers
        -   Good for local development
        -   similar e substitui o minikube
        -   (+) good for local development
        -   (+) run on containers, portanto não reserva recursos fixos
        -   `kind create cluster`: this will create a single Kubernetes node running as a docker container named kind-control-plane and configures kubectl to use this cluste
        -   By default, the cluster access configuration is stored in ${HOME}/.kube/config
        -   `kind get clusters`
        -   `kind load docker-image <my-custom-image-0>`
        
    -   -   -   

    -   <u>minikube</u>
        * Creates the cluster
        * (+) good for local development
        * (-) roda em VM, portanto reserva recursos fixos
        * By default, you can only create one virtual node using the minikube
        * To switch Docker to the context of Minikube, enter the following command on the command line: eval $(minikube docker-env)
        * `minikube start`: create a cluster
        * `minikube dashboard`: open (in browser) management dash
        * `minikube stop`
        * `minikube delete`
        
    -   <u>Helm</u>
        -   package manager
        -   manage templates
        -   Chart: package; have everything needed to run something; set of configuration files; good when some pluging require to configure several resources togueder; some populat helm charts: elastic stack, prometheus, …
        -   Template: just abstraction over the configuration files, with placeholders to be substituted
        -   Repository: where charts can be collected and shared
        -   Release: instance of a chart running in a Kubernetes cluster
        -   tiller: server side part of helm; removed on helm 3.0 for security
        -   `upgrade`: ?
        -   `rollback`: ?
        -   chart.yaml
            -   chart definition
        
    -   <u>Kustomize</u>
        -   substitui o Helm?
        -   from docker-compose to kubernets??
        
    -   <u>rancher</u>
        -   painel administrativo
        -   pode gerenciar varios clusters juntos
        -   My experience: o bruno usava na zads
        
    -   <u>Openshift?</u>
    
        -   cretes the cluster, + a pile of other things
    
    -   <u> minishift?</u>

### Registries

* stores and lets you distribute Docker images
* usually, cloud based
* we upload our named image to it
* Image names as used in typical docker commands to reflect their origin
* naming convention: `registryDomain/imageName:tag`
* by default, the registryDomain is docker-hub
* 
* docker psuh
* docker pull
* 
* Docker Hub: from docker; free + paid enterprise features
* **private registries**
  * you can create a local registry, based on the docker-hub
  * ECR: from Amazon; you need to login with aws credentials before pull images; 
  * GCR: from Google

### Other tools

-   **Docker cloud**
	-   Automated the process of take an image to the container
-   **Docker swarm**
    -   container orchestration
    -   multiple Docker hosts which run in swarm mode and act as managers (to manage membership and delegation) and workers (which run swarm services)
    -   It’s native from docker, not a separate software
    -   mais fácil e mais limitado que kubertes
    -   vai direto em docker-compose
    -   ==will soon [2022] be dead?==
    -   `docker swarm init`: inicia, você será o manager
    -   `docker stack deploy --compose-file=docker-compose.yml` command can be used to deploy a Compose file to Swarm mode
    -   <u>Roles</u>
        -   managers e workers
        -   you can have more than one manager, but one manager is the leader
        -   the cluster will keep running if more than 50% of the managers are up
        -   if the manager leader dies, another leader will be automatically elected
    -   <u>Services</u>
        -   one or more containers with the same configuration running under docker's swarm mode
        -   When you create a service, you specify which container image to use and which commands to execute inside running containers.
        -   restarts your container if it stops, finds the appropriate node to run  the container on based on your constraints, scale your service up or  down, allows you to use the mesh networking and a VIP to discover your  service, and perform rolling updates to minimize the risk of an outage  during a change to your running application
        -   :keyboard: Shell: `docker service create --replicas <n> <image-name>`
        -   `docker service update –image <img>`: will gradually change the containers, without disrupting the application
    -   <u>Task</u>
        -   If the task fails the orchestrator removes the task and its container and then creates a new task to replace it according to the desired state specified by the service.
    -   <u>Stack</u>
        -   ?
-   **Portainer**
    -   GUI for orchestration
    -   works above swarm or kubernets
    -   Acessed by browser
    -   easy and lighweigh
    -   fazer por GUI o que também daria pra fazer por codigo
-   **Openshift**
    -   k8s cluster with oppinated decisions 
    -   From RedHat
    -   (+) secure
    -   (+) less “degree of freedom” in decisions 
    -   jenkins: when you push to github, it does build and upload to registry (within openshift), deploy

## CORS

* Cross-origin resource sharing
* Mechanism that allows resources to be requested from another domain (outside the domain from which the web page was served)
* especificação do W3C 
* **Examples**
  * Hotlinks to images, stylesheets, etc
  * Iframes, embbedding, etc
  * Ajax requests
  * WebSockets
* **Same-origin policy (SOP)**
  * What the browser allows
  * scripts contained in a first web page to access data in a second web page, but only if both web pages have the same origin
  * origin = URI scheme, host name, and port number
  * CORS is the most common technique to relax the policy, but there are others
  * Does not apply to:
    * HTML tags
    * Web Sockets
* **How it works**
  * Some cross-domain request are blocked by default
  * The server must  use a header to explicitly list origins that may request a file (you can also allow all)
  * A wildcard same-origin policy (allow all) is appropriate when a page or API response is considered completely public content and it is intended to be accessible to everyone, including any code on any site. A freely-available web font on a public hosting service like Google Fonts is an example. 
  * the `Access-Control-Allow-Origin` header is set by the external web service (service.example.com), not the original web application server (www.example.com)
  * Em alguns tipos de requisições, é necessário enviar uma requisição  preliminar chamada de preflighted request. O preflighted request utiliza o método OPTIONS do HTTP informando o método e domínio que será  invocado para assim garantir que a requisição seguinte pode ser  realizada.

## Application Architectures

* We’ll study here the Model View Controller architecture, but there are others 

* Model

  * business-logic layer
  * high-level view of data and actions
  * ensures that business rules are satisfied

* View
  * Visual layer
  * a single model can have different views (depending on device, user, etc)
  
* Controller
  * receives events (user actions), executes actions on the model, and returns a view to the user.

  ![1546036877574](Redes de Computadores - Images/1546036877574.png)

## Semantic web

* Idea that websites must be machine-readale
* “database of everything.”
* **Resource Description Framework (RDF)**
  * mechanism for different websites to publish data in a consistent format
  * Written in XML or graph-based langugages (more usual) (see in *Databases - Graph model*)

# Cloud computing

* provides users with on-demand access
  of a shared pool of computing resources over a network
* Allow individuals and companies to use remote third-party software and hardware components
* resource pooling: the workloads associated with multiple users (or *tenants*) are typically colocated on the
  same set of physical resources
* (+) doesn't require a large upfront investment
* (+) easy to resize
* (+) geografically distributed
* **Comparison**
  * ==Are they considered Infrastructure as a Services (IaaS), or what?==
  * AWS
    * breadth and depth cloud service
    * mais barata que Azure (source: bruno)
    * todos os serviços deles possem um selinho da AWS (ex: RDS), o que dificulta a migração
    * However, it allows good integration with opensource softwares
  * Azure
    * Complete ecosystem: Azure, PowerBI, Office 365, Teams, etc
  * Google cloud
  * IBM
    * caro, muito fácil
  * Digital Ocean
    * barato, user friedly, fácil
    * Sem tantas opções avançadas por cima (ML, etc)
  * Alibaba cloud
    * ?

## Cloud business models

* **Infrastructure as a service**
  * shared hardware infrastructure
  * Provides full control over the virutal machine
  * Can offer a variaty of services of top of that
  * Examples: Amazon EC2, Azure, Linode, etc
* **Network as a service**
  * You dont have to deal with network (security, load distribution, etc), just build you application
  * optimizatimize its of resource allocations by considering network and computing resources as a unified whole
  * Ex: providers of lorawan networks
* **Platform as a service**
  * Pre installed applications
  * For developers to build, test, and host their software
  * You just code, no adm worries
  * Ex: heroku, 000webhost, Engine Yard, and AppFog

## AWS

* have sereval tools, but we’ll use here mainly EC2

* Some things are free for 1 year

* ![img](Images - Redes de Computadores/image-20200129104838683.png)

  ![img](Images - Redes de Computadores/1570194270192.png)

* **Boto**

  * SDK for Python

  * create, configure, and manage AWS services

  * the sintax depends on the availability zone

  * Creating S3 (us-east-1):

    ```
    s3 = boto3.resource('s3')
    s3.create_bucket(Bucket='bucket_name')
    ```

    

* **Instance type**

  * T family: burstable compute (incluse T2.micro)

  * M family: general pourpose

  * R family: memory optimized

* **Storage type**

  * ==Elastic Block Store (Amazon EBS)?==

  * General purpose SSD: 

  * Provisioned IOPS SSD: 

* **IAM (Identity and Access Management)**

  * Can users can do

  * it’s not a role, but a whole account: you can acess the IAM directly, as a user

  * Good practice: dont use the root account, use IAM for everything

  * The manager is charged by the users’ actions

  * individual security credentials

  * Defined using either the visual editor or JSON

  * By default, you’re denied everything

  * can be integrated with external mangament access, like from MS active directory

  * pricing: free of charge

  * Big picture:

    ![image-20200723172506778](Images - Sotfware Eng in web/image-20200723172506778.png)

  * entities

    * User: ?; can have credentials
  * Groups: collection of users managed as a unit
    * Role: permission policies that determine what the identity can and cannot do in AWS
    * Organizational Unit: ?
    * Each identity can be associated with one or more policies
    
  * Defining a policy
  
  * attached to entitites
    * Principal: entity that is allowed (ex: user)
    * Action: what is done (example: `ec2.RunInstance`)
    * Resource: whats acessed
    * Condition: when that policy is true
    * Effect: deny or allow
  
* **Access key**

    * to use aws CLI and SDK
    * The only time that you can view or download the secret access key is when you create the keys. You cannot recover them later. 
    * Security: No one who legitimately represents Amazon will ever ask you for your secret key
    * you can set credential by env variables, like AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION, etc

* **AWS CLI**

    * there are thing that are easier by CLI than by Boto
    * download S3 bucket folder: `aws s3 cp s3://BUCKETNAME/PATH/TO/FOLDER LocalFolderName --recursive`

* **Cloud Formation**

    * Templates for web applications

    * Instances described in a yaml file

* **Deep lens**: Hardware to computer vision. See in *Edge Computing*

* **AWS IoT Greengrass**

    * deploy the application project and a Lambda runtime 

* **Lambda options**

  * Serverless
  * The AWS API Gateway is the only way to expose your lambda function over HTTP
  * pay by call
  * ex: *IoT send -> DB store* workflow without an instance 
  * S3 Obejct Lambda: will be executed when object is requested, https://aws.amazon.com/blogs/aws/introducing-amazon-s3-object-lambda-use-your-code-to-process-data-as-it-is-being-retrieved-from-s3/
  
* **autoscaling**

    * defined by a scaling policy
    * target emtric
    * min and max nubmer of instnce
    * cool down period
    
* **Elastic IP**

    * public IPv4 address
    * static; it does not change over time
    * You cannot reuse a public IPv4 address, and you cannot convert a public IP to an Elastic IP

* * * 

* **Key Management Service **

    * centrally manage and securely store your keys.
    * pricing: cada chave custa \$1/month + um pequeno valor por request (​\$0.03 per 10,000 requests)
    * prices in Amazon S3 are more or less the same as in Google Cloud
    * 
    
* **Simple Queue Service (SQS)**

    * ?

* **CloudWatch**

    * monitore services
    * metrics, alerts, etc

* **SES**

    * email
    * Provides SMTP Service
    * Pricing: 1K email for 0.10 dolares
    * requires a domain *or* a emails account somewhere
    * will give you host, port, username and password, and from there you can use any mail software to manage the email  sending (mailwizz, mailchimp, etc)
    * you can use by HTTP (their API) or SMTP
    
*  **Elastic MapReduce (EMR)**

    *  Apache Spark, Apache Hive, Apache HBase, Apache Flink, Apache Hudi…

    * Amazon EMR File System (EMRFS): integra com o S3

    * EMR studio: their IDE; jupyter based

    * pricing: per instance per hour?

    * currently there is not a way to stop and EMR cluster in the same sense you can with EC2 instances

    * [Tutorial - Getting Started with PySpark on AWS EMR](https://towardsdatascience.com/getting-started-with-pyspark-on-amazon-emr-c85154b6b921)

    * bootstrap phase: occurs before Amazon EMR installs and configures applications 

    * steps: what to do after the cluster starts; you can submit jobs, sending just the .py file; 

    * the cluster can run on EC2 machines or EKS cluster; then the spark cluster is called *virtual cluster*; job-centric workflow, where the users submit job to be executed in the pods; 

    * example CLI cluster creation:

      ```bash
      aws emr create-cluster --name "Spark cluster with step" \
          --release-label emr-5.24.1 \
          --applications Name=Spark \
          --log-uri s3://your-bucket/logs/ \
          --ec2-attributes KeyName=your-key-pair \
          --instance-type m5.xlarge \
          --instance-count 3 \
          --bootstrap-actions Path=s3://your-bucket/emr_bootstrap.sh \
          --steps Type=Spark,Name="Spark job",ActionOnFailure=CONTINUE,Args=[--deploy-mode,cluster,--master,yarn,s3://your-bucket/pyspark_job.py] \
          --use-default-roles \
          --auto-terminate
      ```

      

* **Amazon Managed Workflows for Apache Airflow (MWAA)**

    * ?

* **Glue**

    * ?

### S3

* pricing: $0.023 per GB per month
* See more in AWS
* Simple Storage Service
* a bucket name is globally unique
* Use Amazon Athena: query S3 data with SQL
* 
* You can define Access Points with specifics permissions
* **Storage class**
  * standard
  * inteligent tiering: automatically archives old stuff, without you having to manage or take decisions?; have a monitoring fee, but not a retrieval fee?
  * standard-IA
  * glacier
  * glacier deep archive
  * one zone-ai
  * outpost
* **lifecycles policies**
  * rules to manage storage
  * when to archive, etc
* **other features**
  * <u>replication</u>
    * Same region or cross-region
  * <u>versioing</u>
    * ?
  * <u>object lock</u>
    * ?
* **Storage lens**
  * service that analyse and recomend how to optimize?
  * 29 metrics
  * analyse up to 15 months?
* **Macie**
  * automatically provides an inventory of Amazon S3 buckets including a list of unencrypted buckets, publicly accessible buckets, and buckets shared with AWS accounts outside those you have defined in AWS Organizations, PII information, etc
  * Continouslly monitore and alert
* **Amazon Redshift**
  * Data Warehouse
  * Amazon Redshift Spectrum: analyze data in S3 and other AWS warehouses
  * See more in *databases*

### SDK - Python

* Boto
* you must have both AWS credentials and an AWS Region set in order to make requests
* support pagination
* **client**
  * low-level interface
  * map 1:1 with API
  * response: usually a dict with the the result and HTTP response headers
  * 
* **resource**
  * higher-level abstraction
  * object-oriented
  * lazy loaded
  * actions: ?
  * collection: group of resources
* **waiter**
  * blocks until something is finished
  * have a `.wait()` method
* **config**: client-specific configurations; overwrite the default congifurations in `~/.aws/`
* 
* **Service - EC2**
  * Get basic information about your Amazon EC2 instances
  * Start and stop detailed monitoring of an Amazon EC2 instance
  * Start and stop an Amazon EC2 instance
  * Reboot an Amazon EC2 instance
  * describe_instances.
  * monitor_instances.
  * unmonitor_instances.
  * start_instances.
  * stop_instances.
  * reboot_instances.
  * `create_key_pair()`
* **Service - Comprehend**
  * `client.detect_pii_entities(Text=<lalala>, LanguageCode='en')`

### **EC2**

* Elastis computer cloud

* works like a remote computer (but are clusters)

* enables you to increase or decrease capacity easily

* You can lauch a cluster of instances

* Can be accessed by the graphical dashboard, by CLI or by SDKs (for mobile)

* Username: *ubuntu* on ubuntu instances; *ec2-user* on amazon instances ([others)](https://stackoverflow.com/a/57939041/12555523)

*  it is fairly common for virtual machine instances to
  become unavailable without warning, as the platforms are designed to prioritize
  flexibility and elasticityi over single-machine reliability

*  security group: acts as a virtual firewall for your cluster nodes to control inbound and outbound traffic
  
* <u>Amazon Machine Image (AMI)</u>

  * determines the OS and etcs that the VM will be running
  * Type: hardware configurations

* <u>Basic setup</u>

  * Launch using the AWS browser interface

  * Select the instance size/power

  * you have to download/select your private key file

  * access via SSH ([tutorial](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html))

    `ssh -i /path/my-key-pair.pem ec2-user@ec2-198-51-100-1.compute-1.amazonaws.com` 

  * After finish using, stop the instance (the biling is by time of use)

  * Terminate: delete permanently 

    ![         The instance lifecycle       ](Images - Redes de Computadores/instance_lifecycle.png)
    
  * [increase EBS size tutorial](https://medium.com/@m.yunan.helmy/increase-the-size-of-ebs-volume-in-your-ec2-instance-3859e4be6cb7)

* <u>Usage: Python Notebook</u>

  * Python and anaconda need to be installed
  * Simillar to local instalation (manually, not using jupyter notebook)
  * tem que fazer algumas tretas entre o *jupyter notebook configuration file* e o *certificado ssl*
  * After that, you will be able to access the notebook throgh the browser, using the AC2 instance public DNS and the configurated port in the *jupyter notebook configuration file*
  * [Tutorial](https://chrisalbon.com/aws/basics/run_project_jupyter_on_amazon_ec2/) (just change to rsa to from 1024 to 2048)

* <u>Usage: Python Flask</u>

  * [Tutorial 1](https://towardsdatascience.com/simple-way-to-deploy-machine-learning-models-to-cloud-fd58b771fdcf)
  * Train and export (localy) -> setup instance -> run docker+flask

* <u>Usage: Spark</u>

  * need to install first default-jre (java), scala, hadoop

* <u>Usage: LAMP stack</u>

  * Linux, Apache, MySQL/MariaDB and PHP
  * https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html
  * EC2 with Amazon Linux 2
  * Install all softwares by yum
  * Create a linux group called *apache*: ec2-user (and any future members of the apache group) can add, delete, and edit files in the `/var/www` directory

### **RDS**

* Relational database
* simplify a lot of management tasks such as updating minor versions, handling regular backups and even scaling up
* You can chose the engine you want (including their Amazon Aurora)
* It’s just a management layer
* Resizeble capability
* (+) Easy to setup
* Can be deployed (in a weird hibrid way) on premise
* 
* **Aurora**
* Similar to MySQL and Postgres
* **How they ensure availablitity**
  * two identical and sincronized database, one primary and one secondary 
  * mirror read-only replicas, to speed up
* **Backup**
  * by snapshots
  * no performance overhead in operation
  * you can save snapshots, evenafter the DB is deleted. You can export the snapshot to S3, which is much more space efficient
  * ==can be “frozen” to S3==
* **Encription**
  * Amazon RDS encrypted DB instances provide an additional layer of data protection by securing your data from unauthorized access to the underlying storage. You can use Amazon RDS encryption to increase data protection of your applications deployed in the cloud, and to fulfill compliance requirements for encryption at rest. 
  * https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.Encryption.html
  *  uses an AWS KMS customer master key (CMK) to encrypt these resources
  * Just chose “Enable encryption” on the Amazon RDS console, or `--storage-encrypted` parameter when creating by CLI

### SageMaker

* provides an integrated Jupyter authoring notebook instance

* train and validate a CNN model

* can import several pre-trained models   

* available on free tier (t2.medium or t3.medium, 250hrs)

* separe the application in 3 components: build, train and deploy

* SageMaker Studio: web IDE for developing the whole cicle

* 

  The input_fn and output_fn methods will be used by Amazon SageMaker to parse the data payload and reformat the response.

#### **SageMaker Ground Truth**

* Annotaion tool

* Is independent of sagemaker, but I’ll register here

* Optional:  pre-trained ML models to speed up labeling; when the data is ambiguius, a human labelers is used

* Labeling workforces: crowdsource (mechanical turk), private workers (hire by you) or third party (companies)

* If you use multiple workers for labeling jobs; annotation consolidation (join together multiple workers’ output) is automatically done; each object and each worker will have a difference confidence score; 

* input formats: images, videos, text

* output formats: detection, classsification, custom labels, etc

* manifest file

  * pointing to the locations of the input images stored in S3 that require annotation. Each line corresponds to a single image and is an independent JSON document.

  * each image must have an entry in the manifest file in the following format:

     ```
     {"source-ref": "s3://sm-gt-dataset/ground-truth-demo/images/2563c7e7e3432a6e.jpg"}
     ```
  
*  augmented manifest file

  * The output of the SageMaker Ground Truth bounding box labeling job

  * includes several metadatas

  * can only support Pipe input mode

  * The `AttributeNames` parameter is an ordered list of attribute names that                                    SageMaker looks for in the JSON object to use as training input.                                 

  * Each line of the augmented manifest file is like:

    ```
    {"source-ref":"s3://pizza-dataset/images/images(1005).jpg","pizza-dataset-labeling":{"image_size":[{"width":194,"height":259,"depth":3}],"annotations":[{"class_id":1,"top":85,"left":13,"height":139,"width":180}]},"pizza-dataset-labeling-metadata":{"objects":[{"confidence":0.09}],"class-map":{"1":"box"},"type":"groundtruth/object-detection","human-annotated":"yes","creation-date":"2020-11-01T18:43:38.057459","job-name":"labeling-job/pizza-dataset-labeling"}}
    ```

  * Post processing the labels: [example](https://github.com/aws-samples/amazon-sagemaker-aws-greengrass-custom-object-detection-model/blob/master/training/01_Process%20SageMaker%20Ground%20Truth%20labeling%20job%20outputs%20for%20training.ipynb)

  * [this tutorial](https://docs.aws.amazon.com/sagemaker/latest/dg/incremental-training.html) says that the Manifest File can be used for incremental training, but not the Augmented Manifest File

* Steps
  
  * create an S3 bucket that will store your data
  * create job

#### **build**

* Initial setup
* configured instances
* jupyter preinstalled, along several libraries (apache MX, tensorflwo, all anaconda)
* you can provide your docker image
* **Data Wrangler**:  a feature which aims to automate the preparation of data for machine learning

#### **train**

* includes hyperparameter optimization
* recomended format for data training (better performance): recordIO
* you can save files directly to S3 using AWS CLI
* channel: data segments, like train and test??
* runs on containers, be them from AWS or provided by you
* pipe mode: accepts an augmented manifest file as input (from Ground Truth); takes data directly from s3; only accepts RecordIO as format; FIFO queue from S3 (stream);
* script mode (same as BYO?): custom code; may be custom container; data is copied from S3 to the notebook instance; support any data format
* marketplace mode: they have several ready notebooks
* Tutorial: [train tensorflow script mode](https://towardsdatascience.com/train-a-tensorflow-model-in-amazon-sagemaker-e2df9b036a8) (simple and direct)
* <u>ML SDK</u>
  * they have their own 
  * you can configurate all parameter with a settings dictionary
  * `.Session()`
  * `.Estimator()`
* <u>Ready made models</u>
  * You just retrain to your task
  * The SageMaker XGBoost algorithm is an implementation of the open-source DMLC XGBoost package
  * Object detection: https://aws.amazon.com/blogs/iot/sagemaker-object-detection-greengrass-part-2-of-3/
* <u>Custom training tensorflow</u>
  * https://towardsdatascience.com/implementing-yolo-on-a-custom-dataset-20101473ce53
  * https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html
  * https://docs.aws.amazon.com/sagemaker/latest/dg/tf.html
  * https://sagemaker.readthedocs.io/en/stable/frameworks/tensorflow/using_tf.html#train-a-model-with-tensorflow
  * https://medium.com/@julsimon/making-amazon-sagemaker-and-tensorflow-work-for-you-893365184233
* <u>training job</u>
  * runs on a difference instance type than the notebook
  * must configure: S3 url for trainngi data; instance resources, S3 url for output model, ==where is the code?==, metrics
  * generate several logs
  * can be monitored by AWS cloudWatch 
  * artifacs: training outputs
  * model: ?
  * the models should be saved in a folder with numerical name (ex: '0001')
* <u>training locally</u>
  * for debug, to early catch mistakes
  * train_instance_type='local'
* <u>Hyperparameter tunning</u>
  * HPO (automatic hyperparameter optimization)
  * if the model is builtin in sagemaker, the metric are already defined
  * otherwise, you emmit your metric by stdout
  * dictionary of configurations
  * strategy: random, bayesian
  * metric: name and max/min
  * 

#### **deploy**
* self hoested on AWS
* lambda or rest endpoints
* monitoring features
* easy escale (bacause it’s aws based)
* can be deployed to several availability zones
* production variants: different version of the model, each receiving a percentual of the traffic, for comparisson
* API gateway + lambda + sagemakerAPI: ?
* <u>endpoint</u>
  * ?
  * can have a load balancer to several instances; can be distributed over several availability zones
  * can be invoked by other services, like lambda
  * ==you can created an endpoint with a doker image, like [here](https://medium.com/theteammavericks/deploy-scalable-ml-model-using-aws-sagemaker-a817e6001034), and then deploy the entire aplication togueder?==
* <u>multimodel endpoint</u>
  * support A/B testing
  * You can use unrelated models
  * [sample notebook with XGboost](https://github.com/aws/amazon-sagemaker-examples/blob/master/advanced_functionality/multi_model_xgboost_home_value/xgboost_multi_model_endpoint_home_value.ipynb)
* <u>Other tips to save costs</u>
  * start endpoint only on predition: have to wait  5-10 minutes do start; pseudo batch; must monitore requests, sotre them, and answer every x time or x number of requests, then another lambda to stop de endpoint;
* <u>deploy directly to lambda</u>
  * load model from S3
  * under 50MBs model
  * must load all dependencies in lambda
  * ==what are the other limitations????==
  * [doest seem possible](https://stackoverflow.com/a/59312347)
  * [but this one say that it is](https://stackoverflow.com/a/62776279)
* <u>deploy for batch transform</u>
  * ?
  * data must be in S3
  * can be invoked from notebook, or lambda
* <u>inference pipeline</u>
  * for pre and pos processing
  * each one in a separate instance
* **About pricing**

  * endpoint: Enquanto o endpoint estiver rodando, a AWS irá nos cobrar
  * it’s possible to use one endpoint for more than one model (https://docs.aws.amazon.com/sagemaker/latest/dg/multi-model-endpoints.html)

### Rekognition

  * Both image and video
  * Is in free tier
  * Usage: face recognition, emotion detection, object recognition, text in image
  * both CLI, API or Console
  * Basic tutorialge: https://aws.amazon.com/getting-started/hands-on/detect-analyze-compare-faces-rekognition/

### Comprehend

* **Pricing**
  * <u>PII Detection</u>
    * measured in units of 100 characters, with a 3 unit (300 character) minimum charge per request.
    * $0.0001 per unit

## Others

* **Top general pourpose**
  * Google Cloud
  * Azure
* **Simple options**
  * <https://www.gearhost.com>
  * <https://www.000webhost.com>
    * tested
    * interface chata pra caralho
    * Free options
      * 2 servers
      * MySQL
      * Very limited data transfer (they will automaticly block the website)
  * <https://profreehost.com>
  * <https://www.netlify.com/> (cool)
  * Heroku
    * integrates with github: git push heroku master já faz uploadToServer
    * integrates with docker, but not with docker-compose
    * com docker pode ser tanto pelo docker-registry quanto escrevendo um arquivo de configuração `heroku.yml`
    * have free plans
  * 
* **IoT driven**
  * ThingWorx
  * OpenIoT
  * GENI
  * Xively
    *  Open source, free and easy to use
    * integrate devices with the plat-
      form by ready libraries (such as ARM mbed, arduino, etc) and facilitate communication
    *  Enables users to visualize their data graphically in real-time
    * enables users to control sensors remotely
  * Nimbits
    *  performs data analytics, gener-
      ates alerts, and connects with social networks and spreadsheets
*  **Others tools and softwares**
  *  Grafana
    * Monitoring application
    * query, visualize, alert
    * Easy to build and share dashboards
    * data source: Graphite, Prometheus, Elasticsearch, several DBs, etc

# IT management

* Or *system administration*

* COnfiguration and management of IT hardware and software

* https://www.youtube.com/watch?v=1DvTwuByjo0

* Sub roles

  * network adm
  * server adm
  * security adm
  * database adm
  * system adm (overall)

* Professionals Certifications

  * CompTIA (specific to vendor agnostic)
  * Microsoft certifications
  * Redhat
  * Cisco
  * AWS

* KVM switch

  * keyboard, video and mouse switch

  * hardware device that allows a user to control multiple computers from one or more

    ![img](Images - Redes de Computadores/220px-Kvm-switch-diagram.svg.png)
  
* ticketing system

  * customer service tool
  * helps companies manage their service and support cases
  * ticket: record of the interactions on a support or service case.

## Minizing disasters

* **Objectives**
  * Identify critical systems
  * Create a disaster recovery plan
  * Test the plan
  * Re-evaluate periodicaly
* **Recovery process**
  * restore data after shits happen
  * do not gurantee full recovery
  * sometimes require hardware analysis
* **Metrics**
  * recovery time objective
  * time to restore 100% of the data

### Backup

* Backus should be tested and documented
* **Where to backup**
  * Local: easy to access, fast to recover
  * Remote: prevents agaist physical disasters. Have to consider ciber security
    * `rsync`: sinronize files. Can run above ssh (secure)
    * Only transfeers  files that have changed since the last backup
  * its a good idea to have both, to different types of files
  * on cloud, always backup in other availability zones
* **How to backup**
  * full backup
    * Image of the disk, including OS
    * (+)Fast to recover
    * Mirror: when you keep just the last image (delete history). Can  be done frequently (RAID 1), but does not ensure restaurability
  * Incremental back
    * what changed since the last incremental backup
    * To restore: take the last full backup + all differentals after
    * (+) keep versions
  * Differential backup
    * what changed since the last full backup
    * To restore: take the last full ackup + the last differencial
    * (-) more storage than incremental
    * (+) easr to restore
  * its a good idea to have both, in different freqencues
* Backup plan
  * preventative
  * monitoring
  * recovery plan

### **Post-mortem process**

* intended to help you learn from past incidents

* helps encourage a culture of learning from mistakes, 

* “ sharing is caring”

* Sumary

  * what the incident was? 
  * How long it lasted? 
  * And what the impact was?
  * how it was fixed?

* timeline of key events

* root causes

* resolution and recovery

* What was learned and To-Improve list

  

### RAID systems

* Redundant Arrays of Inexpensive Disks
* To improve the availability
* Use many cheap disk, instead of few good disk
* RAID is not a replacement for backup: doesn't protect against data corruption or deletion. Is still good to have full backups somewhere
* **Level 0 - No Redundancy** 
  * or *striping*
  * Simply spreading data over multiple disks
* **Level 1 - Mirroring**
  * or *shadowing*
  * Writing the identical data to multiple disks
  * Expensive, but easy
* **Level 2 - Error Detecting and Correcting Code**
  *  has fallen into disuse
* **Level 3 - Bit-Interleaved Parity**
  * Rather than have a complete copy of the original data for each disk, we need only add enough redundant information to restore the lost information on a failure
  * one extra disk to hold the check information in case there is a failure
  * (-) take longer to recover from failure
  * (+) less expense on redundant storage
* **Level 4 - Block-Interleaved Parity**
  * Uses the same ratio of data disks and check disks as RAID 3
  * Used in application that prefer smaller accesses (like networks)
* **Level 5 - Distributed block interleaved parity**
  * Similar to level 4

## Network management

* **Softwares**

  * windows server manager
  * Cacti: browser based. Uses MySQL
  * MRTG - Monitoring and statistics
  * Iperf: monitore TCP/UDP performance
  * Bonjour
    * From Apple, but runs of Windows ==and linux?==
    *  zero-configuration networking: to service discovery, address assignment, and hostname resolution

* **Attributions**

  * fault analysis

  * performance management

    provisioning of networks

    maintaining quality of service

* **Falhas usuais**

  *  disponibilidade do serviço, latência,
    vazão
  * Tráfego de uma fonte/destino suspeita
  * Taxa de erro alta
  * Interface up/down repetidamente
  * Taxa de repasse de um roteador diminui
  * Aumento de pacotes com o CRC incorreto

## Padrões de gerenciamento de redes

* OSI CMIP: complexo e pouco usado

* SNMP: Simple network management protocol

  * for collecting and organizing information about managed devices

  * Extensível, permitindo aos fabricantes adicionar funções de gerenciamento aos seus produtos

  * Formally, it can be considered a protocol of the Application Layer

  * Components

    * Agentes: Coletam junto aos objetos gerenciados as informações relevantes
      para o gerenciamento da rede
    * Gerente: Processa as informações recolhidas pelos agentes
    * Objeto: recurso a ser gerenciado. Representado por uma coleção de variáveis

    ![image-20200515150422458](Images - Redes de Computadores/image-20200515150422458.png)

  * SNMP messages

    *  são transportadas sobre UDP
    * Portas 161 e 162
    * criptografada com DES
    * Tipos: GetRequest, GetNextRequest, GetResponse, SetRequest, Trap

  * Management Information Base (MIB)

    * Informações dos objetos mantido pela entidade gerenciadora
    * Uniquely identify every fucking object
    * Cada objeto possui OID (nome), syntax (tipo) e ==…?==
    * apresentada como uma árvore de dados estruturada (object identifier tree)
    * leaf = object
    * O nome é formado pela organização hierarquica da Arvore, with each "dot" segment identifying a tree level

  * Structure of Management Information (SMI)

    * Linguagem de definição de objetos da MIB

## DB admin

* **usual processes**
  * monitoring and check availability
  * backup ad restore
  * migrate, adapt, move
  * Optimize queries, help with the DB design
  * software update, security pathces, etc
  * Error review, post mortens

## Site Reliability Engineering (SRE)

* Software engineeing in infrastructure and operations problems
* Like a devOps, but to infrastructure
* https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/
* 50% solving today’s problems and 50% working on things today that'll make tomorrow better
* **Reliability**
  * The system should continue to work correctly even in the face of adversity
  * faults: things that can go wrong
  * Resilience: to be able to cope with one particular fault
  * failure:  when the system as a whole stops providing the required service to the user.
* **Scalability**
  * As the system grows, there should be reasonable ways of dealing with that growth
  * vertical scaling: moving to a more powerful machine
  * horizontal scaling, distributing the load across multiple smaller machines
* **Maintainability**
  * Over time, many different people will work on the system, and they should all be able to work on it productively
  * “we can and should design software in such a way that it will hopefully minimize pain during maintenance, and thus avoid creating legacy software ourselves.” Clappmann

### Pratices

* **Infrastruture as Code**
  * Virtualization/conteinerization
  * server automation
  * configuration management
  * Monitoramento avançado
  * <u>Principles</u>
    * Systems Can Be Easily Reproduced
    * Systems Are Disposable
    * Systems Are Consistent
    * Processes Are Repeatable
    * Design Is Always Changing
* **Service Level Agreement (SLA)**
  * commitment between a service provider and a client
  * Um sistema 100% perfeito não faz nada: não muda, não se adapta, não melhora agressivamente
  * You can work with the idea of “Error Budget”: how much you can still fail this month.
  * Service Level Objective (SLO): specific measurable of the SLA
* **Network Operation Center (NOC)**
  * Tempo real, cheio de tela e gráfico e estatística e caralho a quatro
  * É bem a ideia de um “centro de operações” mesmo, Comand and Control centralizado
  * Empresa terceirizada: [magnet.inf.br](http://magnet.inf.br)
  * SOC: Security Operation Center
* **Centralized log server**
  * ?

### **Monitoring**

* Make the right information visible to the people who need it
* Monitoring information comes in two types: state and events. State is concerned with
  the current situation, whereas an event records actions or changes
* **Top**
  * CLI
  * Linux only
  * usage by process
  * CPU usage, Memory usage, Swap Memory, Cache Size, Buffer Size
* **Glances**
  * CLI
  * linux only
* **Flask-MonitoringDashboard**
  * To embed in a flask app
  * outlier detection and profilling
  * moniteres each endpoint individually;
    Excelent tutorial: https://medium.com/flask-monitoringdashboard-turtorial/monitor-your-flask-web-application-automatically-with-flask-monitoring-dashboard-d8990676ce83#6cbb
* **pyDash**: django based; quite flexible, I guess; https://linoxide.com/monitoring-2/pydash-python-app-monitoring-linux-server/
* list of self hosted: https://geekflare.com/self-hosted-server-monitoring/
* **NetData:** https://github.com/netdata/netdata; simple and easy to install
* **Nagios**
  * monitoring software
  * similar to Cacti
  * open source
  * windows and linux
  * Each node is a specific part of the API that gets specific data and functions a certain way
  * Focus: network traffic and security
* **Splunk**
* **Elastic stack**
* **ops genie**
  * Atlassian ecosystem
  * focus on incident management
* **CheckMK**
  * Integrates well with Nagios
* Prometheus
  * open-source
  * Very used in containerazed applications
  * pull metrics, store them, and expose metrics 
  * targets: from where it will monitore (which servers, databases, etc)
  * metric types: counter (just go up), gauge (up and down, instantaneos), histogram (distribuiion, over time)
  * prometheus will regularlly access API (by default, `/metrics`) and collect metrics; the service must expose in the right format
  * exporters? what will expose APIs in the format that prometheus is expecting?
  * comes with a built-in database for time series and a query language (PromQL)
* Grafana: visualization tool; integrates very well with prometheus

### Metrics

* 

* Do not think of metric as a single value, but as a statistical distribution of values (Clappmann, pg 14)

* tail latencies: end of distriution (95% percentile, etc)

* Most important metrics to the user: latency, traffic, errors, and saturation

* **web related metrics**

  * 4xx errors
  * 5xx errors
  * number of invocations
  * number of invocations per instance

* **HArdware related metrics**

  * CPU usage
  * memory usage
  * etc

* **Basic computing metrics**

  * troughput: maximum amount of data moved successfully
    between two end points in a given amount of time.
  * aquecimento
  * uso de recursos
  * sanidade de cada equipamento
  * Replication/backup log
  * 
  * Fanout: number of requests to other services that we need to make in order to serve one incoming request.
  * throughput: the number/size of records we can process per second
  * Latency: duration that a request is waiting to be handled
  * Response time: total time that the user sees

* **Dependability**

  * Is the quality of delivered service such that reliance can justifiably be placed on this service

* **Reliability**

  * continuous service accomplishment
  * If we consider the mean, reliability = MTTF
  * Can also be measuared by max time or by non-temporal variables

* **Availability**

  * Time service accomplishment with respect to the alternation between the two states of accomplishment and interruption.
    $$
    availability = \frac{MTTF}{MTTF + MTTR}
    $$

    > Fonte: patterson

#### Qualitative questions

* how incidents are resolved
* what works and what doesn’t, and how, when, and why issues escalate or deescalate.
* Severity level of events.
* **Questions about marginal performance**
  * When you increase a load parameter and keep the system resources (CPU, mem‐
    ory, network bandwidth, etc.) unchanged, how is the performance of your system
    affected?
  * When you increase a load parameter, how much do you need to increase the
    resources if you want to keep performance unchanged?
* **Performance testing**
  * When generating load artificially in order to test the scalability of a system, the load-
    generating client needs to keep sending requests independently of the response time.
* 

#### Failure-centered metrics

![Image result for MTTF](Images - Redes de Computadores/A-schematic-diagram-of-MTTF-MTTR-and-MTBF.png)

![Illustration showing how the use of MTBF, MTTR, MTTA, and MTTF together can improve incident management](Images - Redes de Computadores/tracking-incident-managment.png)

> [Fonte](https://www.atlassian.com/incident-management/kpis/common-metrics)


* **MTBF**
  
  * mean time before failure
  * average time between repairable failures
  * higher = better
  * $MTBF=\frac{total\_operational\_time}{number\_of\_failures}$
  * $MTBF=MTTF + MTTR$ (patterson)
  * Usage
    * Reparable systems
    * to chose the most reliable 
  
* **MTTR**
  * mean time to: recovery *or* repair *or*  respond *or* resolve
  * It can mean any, but are different things
  * Average time to eliminate the problem
  * lower = better
  *  $MTBR=\frac{total\_down\_time}{number\_of\_failures}$
  
* **MTTF**
  * mean time to failure
  * higher = better
  * alguns autores, como o patterson, definem como sendo o tempo de correct behavior (valido então para reparable systems)
  * Usage
    * non-rreparable systems (need to replace all)
    * To chose the most realiable (never fails)
  
* **MTTA**
  * mean time  to acknowledge
  * when an alert is triggered to when work begins on the issue
  * Measure team’s responsiveness 
  * lower=better
  * hard t calculate precisely to long durating products

