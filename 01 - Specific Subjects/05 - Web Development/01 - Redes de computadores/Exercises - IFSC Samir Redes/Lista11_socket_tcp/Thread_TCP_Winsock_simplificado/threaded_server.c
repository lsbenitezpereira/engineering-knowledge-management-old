#include <winsock.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

   

//// Prototypes ////////////////////////////////////////////////////////

SOCKET SocketConfig(const char* pcAddress, int nPort);
void AcceptConnections(SOCKET ListeningSocket);
int EchoIncomingPackets(SOCKET sd);
int ShutdownConnection(SOCKET sd);


const int kBufferSize = 1024;
int fila_conexoes=10;
const int false=0;
const int true=1;




int Connection(const char* pcAddress, int nPort)
{
    printf("Configurando socket...\n");
    SOCKET ListeningSocket = SocketConfig(pcAddress, htons(nPort));
    if (ListeningSocket == INVALID_SOCKET) {
        printf("Erro na chamada da funcao socketConfig(): %ld.\n", WSAGetLastError());
		WSACleanup();
        return 3;
    }

    printf("Aguardando conexoes...\n");
    while(true) {
        AcceptConnections(ListeningSocket);        
    }

}

SOCKET SocketConfig(const char* pcAddress, int nPort)
{
    u_long nInterfaceAddr = inet_addr(pcAddress);
    if (nInterfaceAddr != INADDR_NONE) {
        SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
        if (sd != INVALID_SOCKET) {
            struct sockaddr_in sinInterface;
            sinInterface.sin_family = AF_INET;
            sinInterface.sin_addr.s_addr = nInterfaceAddr;
            sinInterface.sin_port = nPort;
            if (bind(sd, (SOCKADDR *)&sinInterface,sizeof(sinInterface)) != SOCKET_ERROR) {
                listen(sd, fila_conexoes);
                return sd;
            }
            else {
                printf("Erro no bind(): %ld.\n", WSAGetLastError());
            }
        }
    }

    return INVALID_SOCKET;
}



DWORD WINAPI EchoHandler(void* sd_) 
{
    int nRetval = 0;
    SOCKET sd = (SOCKET)sd_;

    if (!EchoIncomingPackets(sd)) {
        printf("Erro na chamada da funcao EchoIncomingPackets()\n");
        nRetval = 3;
    }
    
    
    printf("Fechando a conexao...\n");
    if (ShutdownConnection(sd)) {
         printf("Conexao finalizada.\n");
    }
    else {
        printf("Erro na finalizacao do socket.\n");
        nRetval = 3;
    }
    return nRetval;
}


void AcceptConnections(SOCKET ListeningSocket)
{
    struct sockaddr_in sinRemote;
    int nAddrSize = sizeof(sinRemote);

    while (1) {
        SOCKET sd = accept(ListeningSocket,NULL, NULL);
        if (sd != INVALID_SOCKET) {
            printf("Conexao aceita de %s:%d \n",inet_ntoa(sinRemote.sin_addr),ntohs(sinRemote.sin_port)) ;

            DWORD nThreadID;
            //Cria a thread
            CreateThread(0, 0, EchoHandler, (void*)sd, 0, &nThreadID);
        }
        else {
            printf("Erro na chamada da funcao accept(): %ld.\n", WSAGetLastError());
            return;
        }
    }
}


int EchoIncomingPackets(SOCKET sd)
{
    // l� dados do cliente
    char acReadBuffer[kBufferSize];
    int nReadBytes;
    do {
        nReadBytes = recv(sd, acReadBuffer, kBufferSize, 0);
        if (nReadBytes > 0) {
            printf("Recebidos %d bytes do cliente.\n",nReadBytes);
            acReadBuffer[nReadBytes] = '\0';	
	        printf("Dados: %s\n", acReadBuffer);
        
            int nSentBytes = 0;
            while (nSentBytes < nReadBytes) {
                int nTemp = send(sd, acReadBuffer + nSentBytes,
                        nReadBytes - nSentBytes, 0);
                if (nTemp > 0) {
                    printf("Enviados %d bytes de volta ao cliente.\n",nTemp);
                    nSentBytes += nTemp;
                   
                }
                else if (nTemp == SOCKET_ERROR) {
                    return false;
                }
                else {
                    printf("Cliente fechou a conex�o inesperadamente!\n");
                    return true;
                }
            }
        }
        else if (nReadBytes == SOCKET_ERROR) {
            return false;
        }
    } while (nReadBytes != 0);

    printf("Conexao finalizada pelo cliente.\n");
    return true;
}

int ShutdownConnection(SOCKET sd)
{
    // Envia um FIN pra conex�o
    if (shutdown(sd, SD_SEND) == SOCKET_ERROR) {
        return false;
    }

    // Recebe dados enviados pelo cliente at� que este
    // sinalize o final da conex�o, confirmanddo o FIN 
    char acReadBuffer[kBufferSize];
    while (1) {
        int nNewBytes = recv(sd, acReadBuffer, kBufferSize, 0);
        if (nNewBytes == SOCKET_ERROR) {
            return false;
        }
        else if (nNewBytes != 0) {
            printf("Recebidos %d bytes durante a finalizacao da conexao.\n", nNewBytes);
        }
        else {
            break;
        }
    }

    // Fecha o socket.
    if (closesocket(sd) == SOCKET_ERROR) {
        return false;
    }

    return true;
}




