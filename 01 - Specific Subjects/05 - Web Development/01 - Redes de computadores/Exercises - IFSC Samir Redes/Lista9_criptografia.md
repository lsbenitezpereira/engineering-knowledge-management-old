**Pergunta 1)** Se você está em uma rede interna, os pacotes podem apenas ser bisbilhotados ou adulterados de dentro da rede. Portanto, o acesso se torna menos propenso à problemas de segurança. 

**Pergunta 3)** As mensagens fora trocadas usando TLS v1.2

**Pergunta 5)** Se diferentes hardwares estão conectados, estes podem estar implementando protocolos de segurança diferentes ou, pior ainda, alguns deles não suportarem (por limitações de processamento ou similares) nenhum protocolo seguro

