function y=nrzbi(x,level,treshold);

% Function to code binary signal using NRZ (Non-return zero) bipolar
% Parameters:   x = signal to code;
%               level = level of out after code
%               treshold = Level to comparation

for i = 1:length(x);
    if(x(i)>= treshold)
        y(i)=(-1)*level;
    else
        y(i)=level;
    end
end