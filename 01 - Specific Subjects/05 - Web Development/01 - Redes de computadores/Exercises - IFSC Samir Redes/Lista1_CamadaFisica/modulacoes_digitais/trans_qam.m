function trans_qam(noise)
%-------------------------------------------------------------------------%
% clear all;
%Call function to create file with signal to be used to transmission
gera_sinal_txt;


%------------------------------Constants----------------------------------%
%Table used to make the constellation to 16-QAM in agreement with
%MIL_STD_188_110B
in_phase    = [0.866025, 0.5, 1, 0.258819, -0.5, 0, -0.866025, -0.258819, ... 
    0.5, 0, 0.866025, 0.258819, -0.866025, -0.5, -1, -0.258819];
quadrature  = [0.5, 0.866025, 0, 0.258819, 0.866025, 1, 0.5, 0.258819, ...
    -0.866025, -1, -0.5, -0.258819, -0.5, -0.866025, 0, -0.258819]; 
%-----------------------Systems�s Parameters------------------------------%
machinefmt  = 'native';             % Order for read/write bytes or bits
encoding    = 'UTF-8';              % Character encoding
fs          = 10e3;                 % Sample Frequency
fc          = 50e3;                 % Carrier Frequency
br          = 5e3;                  % Bit Rate
M           = 16;                   % Number of Symbols
ts          = 1/fs;                 % Sample Time
k           = log2(M);              % Bits for Symbols
m           = 0:M-1;                % Symbols�s Vector
baud        = br/k;                 % Baud rate
T           = 1/br;                 % Symbol�s Period
ppp         = round(fs/br);         % Points per period

%-------------------- Modulation's Parameters ----------------------------%
Eg          = 16;                    % Energy to basic pulse
d           = sqrt(2/Eg);           % control factor power
Eb          = d*Eg/log2(M);         % Mean Energy Bit

%-------------------------Information Signal------------------------------%
% Is used a pattern signal defined by a file txt
FID = fopen('signal_dig.txt','rb', machinefmt, encoding);
x1  = fscanf(FID,'%d');
ST  = fclose(FID);
t   = 0:ts:(length(x1)-1)*ts;
%x1  = x1(1:32);
%----------------------------Modulation QAM-------------------------------%
%Conversion Serial to Parallel to have 2 bit for each symbol, the
%conversion is get for one period of bit.
j=1;
k=(2*ppp)-1;
for i = 1:(4*ppp):length(x1);
    x_i(j:j+k) = x1(i:i+k);
    x_q(j:j+k) = x1(i+k+1:(i+k*2+1));
    j=j+k+1;
end

% Make map of bits using standard MIL_STD_188_110B
clear i j;
aux = 2*ppp-1;
ind = 0;            % index to find element in table of map 
for i = 1:2*ppp:length(x_i);
    %To find index from table is used conversion binary to decimal
    ind = x_i(i)*(2^3) + x_i(i+aux)*(2^2) + x_q(i)*(2^1) + x_q(i+aux)*(2^0);
    %The index is add by 1 beacause in matlab the index start in 1.
    x1_i(i:i+aux) = Eb*in_phase(ind+1);
    x1_q(i:i+aux) = Eb*quadrature (ind+1);
end

% Interpolation by 20 to application of carrier using external function
x2_i = inter20(x1_i);
x2_q = inter20(x1_q);

% Define parameters to modulation QAM
fs1         = fs*20;                % Increase Sample frequency due interpolation
t1          = 0:(1/fs1):(length(x2_i)-1)*(1/fs1);
f       = ([-2^15:2^15-1]/2^16)*fs1;
% Defined frequency and carrier to each component IxQ
carrier_i   = cos(2*pi*fc.*t1);
carrier_q   = sin(2*pi*fc.*t1);
y_i         = x2_i.*carrier_i;
y_q         = x2_q.*carrier_q;

% Application of carrier in the signal used equation to modulation
y1          = y_i + y_q;
%-------------------------------------------------------------------------%
%------------------------ Add Noisy AWGN ---------------------------------%
y = awgn(y1,noise);
y1_i  = awgn(y_i,noise);
y1_q  = awgn(y_q,noise);
t3 = complex(y1_i,y1_q);
%-------------------------------------------------------------------------%
%-------------------------- Plot Results ---------------------------------%
% Modulate Signal
figure(1)
subplot(2,1,1)
plot(t1,y1);
title('Sinal Modulado QAM - Tempo')
subplot(2,1,2)
Y1 = abs(fftshift(fft(y1, 2^16)));
plot(f,Y1);
title('Sinal Modulado QAM - Frequencia')

% Modulate Signal whith Noisy
figure(2)
subplot(2,1,1)
plot(t1,y);
title('Sinal Modulado QAM com Ruido- Tempo')
subplot(2,1,2)
Y = abs(fftshift(fft(y, 2^16)));
plot(f,Y);
title('Sinal Modulado QAM com Ruido- Frequencia')

%Plot Constellation of signal
t2 = complex(x1_i,x1_q);
scatterplot(t2)
grid;
%-------------------------------------------------------------------------%
%---------------------------Demodulation QAM------------------------------%
% Multiply carrier to make downconverter for each component
h_i     = y.*carrier_i;
h_q     = y.*carrier_q;
% Decimation of signal using matched filter (Raised Cosine) 
h1_i    = decim20(h_i);
h1_q    = decim20(h_q);
%Plot Constellation after decimation
scatterplot(complex(h1_i,h1_q));
grid;
%Create a buffer with two bit�s period to make a mean and to decided wich
%bit is received.
clear i aux;
aux = 2*ppp-1;
d_i = 0;
d_q = 0;
% Angle to define symbol (value are given in radians)
% This angle must be decided through of constellation
tetha1  = 1e-6;                 %  0.0000 degrees
tetha2  = 1.0122;            % 57.9946 degrees
tetha3  = 0.5586;            % 32.0054 degrees
tetha4  = 1.5708;            % 90.0000 degrees
% Treshold to define limite of variations of angle (in per cent)
v_ang   = 45;
v_inf   = (100 - v_ang)/100; % lower limit
v_sup   = (100 + v_ang)/100; % upper limit
r   = 0.0225;    %radius minor
for i = 1:2*ppp:length(h1_i)
    % Mean for component I
    med_i(i:aux+i) =  sum(h1_i(i:i+aux))/(2*ppp);
    % Mean for component Q
    med_q(i:aux+i) =  sum(h1_q(i:i+aux))/(2*ppp);
    % Variable to auxiliar block decision
    ang = atan (abs(med_i(i)/med_q(i))); %find
    %Block decision used to find quadrant
    if (med_i(i) > 0 && med_q(i) > 0)
        % 1� Quadrant
        if (abs(med_i(i)) < r && abs(med_q(i)) < r)  %Symbol 3
            z_i(i:aux+i) = [0 0 0 0];
            z_q(i:aux+i) = [1 1 1 1];
        elseif (ang <= 3e-1 && ang >= 1e-9) %Symbol 5
            z_i(i:aux+i) = [0 0 1 1];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha3) && ang >= (v_inf*tetha3)) %Symbol 1
            z_i(i:aux+i) = [0 0 0 0];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha2) && ang >= (v_inf*tetha2)) %Symbol 0
            z_i(i:aux+i) = [0 0 0 0];
            z_q(i:aux+i) = [0 0 0 0];
        elseif (ang <= (v_sup*tetha4) && ang >= (v_inf*tetha4)) %Symbol 2 angle
            z_i(i:aux+i) = [0 0 0 0];
            z_q(i:aux+i) = [1 1 0 0];
        end
    elseif (med_i(i) < 0 && med_q(i) > 0)
        % 2� Quadrant
        if (abs(med_i(i)) < r && abs(med_q(i)) < r) %Symbol 7
            z_i(i:aux+i) = [0 0 1 1];
            z_q(i:aux+i) = [1 1 1 1];
        elseif (ang <= 3e-1 && ang >= 1e-9) %Symbol 5
            z_i(i:aux+i) = [0 0 1 1];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha3) && ang >= (v_inf*tetha3)) %Symbol 4
            z_i(i:aux+i) = [0 0 1 1];
            z_q(i:aux+i) = [0 0 0 0];
        elseif (ang <= (v_sup*tetha2) && ang >= (v_inf*tetha2)) %Symbol 6
            z_i(i:aux+i) = [0 0 1 1];
            z_q(i:aux+i) = [1 1 0 0];
        elseif (ang <= (v_sup*tetha4) && ang >= (v_inf*tetha4)) %Symbol 14
            z_i(i:aux+i) = [1 1 1 1];
            z_q(i:aux+i) = [1 1 0 0];
        end
    elseif (med_i(i) < 0 && med_q(i) < 0)
        % 3� Quadrant          
        if (abs(med_i(i)) < r && abs(med_q(i)) < r) %Symbol 15
            z_i(i:aux+i) = [1 1 1 1];
            z_q(i:aux+i) = [1 1 1 1];
        elseif (ang <= 3e-1 && ang >= 1e-9) %Symbol 9
            z_i(i:aux+i) = [1 1 0 0];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha3) && ang >= (v_inf*tetha3)) %Symbol 13
            z_i(i:aux+i) = [1 1 1 1];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha2) && ang >= (v_inf*tetha2)) %Symbol 12
            z_i(i:aux+i) = [1 1 1 1];
            z_q(i:aux+i) = [0 0 0 0];
        elseif (ang <= (v_sup*tetha4) && ang >= (v_inf*tetha4)) %Symbol 14
            z_i(i:aux+i) = [1 1 1 1];
            z_q(i:aux+i) = [1 1 0 0];
        end
    elseif (med_i(i) > 0 && med_q(i) < 0)
        % 4� Quadrant
        if (abs(med_i(i)) < r && abs(med_q(i)) < r) %Symbol 11
            z_i(i:aux+i) = [1 1 0 0];
            z_q(i:aux+i) = [1 1 1 1];
        elseif (ang <= 3e-1 && ang >= 1e-9) %Symbol 9
            z_i(i:aux+i) = [1 1 0 0];
            z_q(i:aux+i) = [0 0 1 1];
        elseif (ang <= (v_sup*tetha3) && ang >= (v_inf*tetha3)) %Symbol 8
            z_i(i:aux+i) = [1 1 0 0];
            z_q(i:aux+i) = [0 0 0 0];
        elseif (ang <= (v_sup*tetha2) && ang >= (v_inf*tetha2)) %Symbol 10
            z_i(i:aux+i) = [1 1 0 0];
            z_q(i:aux+i) = [1 1 0 0];
        elseif (ang <= (v_sup*tetha4) && ang >= (v_inf*tetha4)) %Symbol 2
            z_i(i:aux+i) = [0 0 0 0];
            z_q(i:aux+i) = [1 1 0 0];
        end
    end   
end
%Plot Constellation after mean
scatterplot(complex(med_i,med_q));
grid;

% Conversion Parallel to serial, the conversion is get for one period of
% bit.
clear j;
j=1;
for i = 1:(4*ppp):length(x1);
    z(i:2*(k)+i+1)= [z_i(j:j+k) z_q(j:j+k)];
    j=j+k+1;
end

ler_sinal_dig; %Convert demodulate signal to txt file.

%----------------------------Plot Results---------------------------------%
figure(6)
plot(t,z,t,x1-1.5,'r-');
title('Sinal Demodulado com Matlab');
axis([0 max(t) -max(x1)-1 max(x1)+1]);
grid;
legend('Sinal Demodulado','Sinal Original')
%-------------------------------------------------------------------------%


