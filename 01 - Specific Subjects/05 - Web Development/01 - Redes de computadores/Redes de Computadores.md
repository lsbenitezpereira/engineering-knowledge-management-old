# Conceptualization

* a collection of autonomous computers interconnected by a single technology
* Network architecture: set of layers and protocols
* **Internetwork**
  * Connection of two or more networks 
  * Gateway (or router): device that connects two/more networks
  * Example: The worldwide Internet
* **Service**
  * set of primitives (operations) that a layer provides to the layer above it
  * defines what operations the layer is prepared to perform, but not how 
* **Stream**
  * Informação que produzida e transmitida continuamente
  * Formal definiton: infinite sequence
* **Bloco**
  * Informação contida em um bloco
* **Peers**
  * Entities comprising the corresponding layers on different machines 
  * May be processes, hardware devices, or even human beings
  * In other words, it is the peers that communicate by using the protocol.

## Topologies and models

* **Modes of physical transmission**
  * smaller, geographically localized networks tend to use broadcasting, whereas larger networks usually are point-to-point.
  * <u>Broadcast</u>
    * Ou *multiponto*
    * have a single communication channel that is shared by all the machines on the network
    * três ou mais dispositivos de comunicação com possibilidade de utilização do mesmo enlace
  
    ![image](Images - Redes de Computadores/image-20200302210933518.png)
  
  * <u>Point to point</u>
    * *ponto a ponto*
    * many connections between individual pairs of machines
    * packet on this type of network may have to first visit one or more intermediate machines
    * apenas dois pontos de comunicação, um em cada extremidade do enlace ou ligação
      
      ![image](Images - Redes de Computadores/image-20200302210922443.png)
  
* **Physical connection Topology**

  * <u>Fully connected</u>
    * Todas as estações são interligadas duas a duas entre si através de um caminho físico dedicado
    * $N(N-1)/2$ ligações
  * <u>Mesh</u>
    * Or *Partially connected*, or *peer to peer*
    * Just enought redundancy and alternatives
    * any device can communicate with any other device as long as the two are within some constrain (example: radio range)
    * communication between nodes can happen in a different protocol than between nodes and end-devices
    * (+)  can be ad hoc in formation, self-organizing, and self-healing on node or link failures
    * (-) too many nodes can lead to higher collisions 
    * cluster tree topology: special case of mesh, where the connections are hierarquical
    * routing protocols for mesh networks: OLSR, HWMP, …; [paper about it](https://www.youtube.com/watch?v=uWAqKAGXPL8)
    * [Wireless Mesh Network Introduction (from Eli)](https://www.youtube.com/watch?v=T7fJwAyALss)
  * <u>Ring</u>
    * Sequence of simplex point-to-point connections
    * cada estação funciona como repetidor
    * Padrão mais conhecido: Token Ring (IEEE 802.5)
  * <u>Star</u>
    * Cada nó é interligado a um nó central (mestre)
    * Mestre ode ser hub ou switch (ver detalhes em *Link layer - MAC*)
    * (-) Muitos cabos
  * <u>Em barramento</u>
    * todas as estações se ligam ao mesmo meio de transmissão
    * Physical transmission is broadcast
    * Exige um mecanismo de controle de acesso ao barramento por multiplexaçãono tempo
    * (+) escalável

* **Modos de Endereçamento**
  
  * <u>Unicast</u>: um endereço refere-se a um único sistema
  * <u>Broadcast</u>: endereçar todas as entidades dentro de um domínio
  * <u>Multicast</u>: endereçar um subconjunto de entidades dentro de um domínio
  
* **Modes of communication**
  * <u>Unidirecional</u>
    * Ou *simplex*
    * enlace é utilizado apenas em um dos dois possíveis sentidos de transmissão
  * <u>Bidirecional</u>
    * Ou *Half-duplex*
    * enlace é utilizado nos dois possíveis sentidos de transmissão, porém apenas um por vez
  * <u>Bidirecional</u> 
    * Ou *Full-duplex*
    * enlace é utilizado nos dois possíveis sentidos de transmissão
      simultaneamente
  
* **How to transfer data**

  * <u>circuit switching</u>	
    
    * end-to-end connection
    
    * channel is reserved
    
    * Example: telephone system
    
    * (+) quality is ensured (no degradation)
    
      ![image-20200627160201271](Images - Redes de Computadores/image-20200627160201271.png)
  * <u>packet switching</u>
    
    * point-to-point
    
    * resources as used just as needed
    
    * data is divided into chunks
    
    * (+) no reservation overhead
    
    * (+) easily scalable
    
      ![image-20200627160222072](Images - Redes de Computadores/image-20200627160222072.png)

## Protocols

* set of rules governing the format and meaning of the packets

* Entities use protocols to implement their service definitions

* An entity can change the protocol, but not the  service visible to their users

* service and protocol are completely decoupled

* **Elementos de um protocolo**

  * Sintaxe
  * semantica
  * temporização

* **Encapsulamento**
  
  * o que cada protocolo adiciona ai blocão sendo transmitido
  * Informações de controle, header
  
* **Protocol Data Unit (PDU)**
  
  * Dados do usuário + controle
  
  * O PDU de uma camada virará os dados do usuário da proxima camada
  
    ![image-20200301145015037](Images - Redes de Computadores/image-20200301145015037.png)
  
* **Controle de conexão**
  
  *  <u>Connection-Oriented</u>
  *  first establishes a connection, uses the connection, and then releases the connection.
     
  *  <u>Connectionless</u>
  * ==orientado a datagrama??==
     * Each message carries the full destination address, and each one is routed through the system independent of all the others
     * o enviador só manda, não faz handshake nem espera ack, fodase
     
     * IP is like that
     * utilização: low power. 


## Classification by physical size
* **Personal area networks**
  * meant for one person
  * Ex: wearables

* **Local area networks**
  * Size: kilometers

* **metropolitan area networks**
  * Size: city
  * Ex: cable television
* **wide area networks**
  * Size: often a country or continent

## Network devices

* **access point**
  * ?
* **Repeter**
  * physical layer
  * Se conecta com 1 dispositivo
  * Sempre repete o pacote 
  * ==do not amplify the signal, just repeat==
  * gera uma nova rede, conectada ao rotear principal
  * O usuŕio deve se conectar ao repetidor (nova rede), e não à rede original; deixa mais lento
* **Hub**
  * physical layer
  * Se conecta com varios dispositivo
  * repete todos os pacotes para todas as portas
  * collision domain of all hosts connected through Hub remains one
  * can amplify and filter the signal (active hub) or nor (passive)
* **Bridge**
  * Link layer
  * Se conecta com 1 dispositivo
  * Filtra os pacotes: se receber um pacote pra ele, repete o pacote, senão descarta
* **Switch**
  * Link layer
  * Se conecta com varios dispositivo
  * analisa o cabeçalho e envia os dados diretamente ao destino
  * can perform error checking before forwarding data
  * Separe the colision domains
  * If the switch do not know to which port to send the package, it broadcast the package (and after the answer the switch will learn). ==If no one answer?==
* **Router**
  * Network layer
  * Forwarding
  * Routing (find shortest path)
  * Port: physical, link and network components.
  * Switching fabric: Buffering and routing. 
  * ==firewall== (hardware based): block specific traffics. Firewalls an also be inplemented in software (in the OS)
  * mesh router
    * tambem diminui a velocidade, mas menos do que um repetidor
    * a rede é unificada
    * não substitui o roteador principal
    * routes both the end-user traffic and the backbone traf
* **modem**
  * modulator-demodulator
  * ==physical layer?==
  * converte a informação para a tecnologia física utilizada (fibra, par trançado, GSM, etc)
* **Gateway**
  * between networks
* **Brouder**
  * ?

## Class annotations

* Samir

* Video classes from Kenan Casey (https://www.youtube.com/channel/UCAmmlJdeE_yJR3Z8n_4fN3Q), from FHU (Freed-Hardeman University, Tenesse), in a Major in Computer Science

* **Final project**
  * ESP8266 e FreeRTOS
  * Cloud PaaS, quê acesso a um socket tcp/IP
  * bidirecional
  * DB relacional
  * Basic frontend vizualization

# Modelo OSI

* Open Systems Interconnection

* Modelo de referencia para comunicação

* was devised before the corresponding protocols were invented

* A maioria dos protocolos reais implementa camadas misturadas

  ![1546016563872](Redes de Computadores - Images/1546016563872.png)
  
* **TCP/IP model**

  * Based on OSI

  * What Internet actually implements

  * We’ll study OSI formally and point the differences to the TCP/IP model

  ![image-20200220181525558](Images - Redes de Computadores/image-20200220181525558.png)

  * Internet layer: this in NOT the World Wide Internet, this is just the name of one layer

  * The protocols are more complex and the devices must be more complex to implement them, but the overall network can be simpler 

    ![image-20200302213042058](Images - Redes de Computadores/image-20200302213042058.png)

* **ATM model**

  * Asynchronous Transfer Mode
  * backbone of the public switched telephone network
  * approximately maps to the three lowest layers of the ISO-OSI reference model: physical layer, data link layer, and network layer
  * Have bet largely replaced by TCP/IP
  * Consider that the devices are “dump”, so the network must be more robust
  * The size of an ATM cell is 53 bytes: 5 byte header and 48 byte payload
  * To see moro in the Telephone Infrastructure, see in it’s specific file

## Camada física

- Transmission and reception of raw bit stream

- Voltage levels, mecanical devices, test proceadures, etc

- Node-to-node

- Can be simplex, half-duplex or duplex 

- Physical and link are generally implemented in the same protocol 

- Details like modulation or channel coding are discussed in *Comunication Syst*

- **Forma de transmissao**
  - corrente: boa imunidade a ruido
  - tensao: facil
  
-  **Forma de envio de bits**
  - Paralela: ruim para longas distancias, pois dá interferencia e limita o clock máximo
  - serial: permite alta velocidade
  
- **Forma de temporização**
  - Síncrona: clock é enviado junto
  - Assincrona: receptor e emissor devem estar configurados de forma igual
  
- **Principais métricas**
  - bit rate: taxa de transmissão
  - baud rate: intervalos de transmissão por segundo (igual ao bitrate quanto a comunicação tiver só dois níves, 0 e 1)
  
- **Types of wired links**

  - Single-ended: ?

  - differential signaling

    - each link is represented by the voltage difference between
      two wires

    - provides more robust data transmissions (better CMRR)

      ![image-20200926170328302](Images - Redes de Computadores/image-20200926170328302.png)

### Transmission media

- **Guided**

  - <u>Coaxial Cable</u>

    ![Par trançado vs cabo coaxial vs cabo de fibra óptica](Images - Redes de Computadores/Coaxial-Cable.jpg)

  - <u>Fiber Optics</u>

    - can handle much higher bandwidths than copper
    -  thin and lightweight

    ![image-20200627143138433](Images - Redes de Computadores/image-20200627143138433.png)

  - <u>Twisted pair</u>

    - Copper

    - STP: blinded

    - cabo UTP

      - Sem blindagem

      - Alcance máximo é de 100 metros

      - taxa de transmissão de 1 à 10 Gbps.

        ![Cabo de par trançado](Images - Redes de Computadores/Twisted-Pair-Cable.png)

- **Wireless**

  -   They all use eletromagnetics wares, in different frequencies 

  -   Lightwave Transmission

  -   Infrared

  -   Microwave

  -   Radio 

- **Satellite**

  - Basically the same as wireless, with its specific demands
  - about 4 GHz a new problem sets in: absorption by water. These waves are only a few
    centimeters long and are absorbed by rain

### Protocolos

* RS232C
  *  ±12 V
  * 9 in connectors (we may not need them all)
  * the most common configuration became that including only three
    wires: TxD, RxR, and GND
  *  only point-to-point connections
  * narrow bandwidth
    (up to 20 Kbps at 50 ft)
  * Common level shifter: MAX232

-   RS485
    -   (+) several devices share the same bus.
-   RS422

## Link layer

- Ou *enlace*

- Determine the frames boundaries (from the raw binary representation)

- Transforma binário em dados

- Check error

- Reliable transmission of data frames

- Node-to-node

- A maioria dos protocólos não implementa controle de fluxo (apesar do OSI falar que deve)

- Dividido em duas sub camadas (nos protocolos da  IEEE 802): Medium Access Control (MAC) e Logical Link Control (LLC)

-   **Técnicas para identificação dos frames**
    -   <u>Contagem por tamanho</u>
        -   O tamanho pode ser fixo (pré determinado) ou variável (determinado no pacote)
        -   Esse é o mais comum
        -   possui um campo para o tamanho
        -   (-) dificuldade de recoperação após erro
        -   (-) o payload precisa ser preenchido (padding) para chegar no tamanho minimo/fixo (perda de eficiência)
    -   <u>Usando flags</u>
        -   Sequencia de bits para sinalizar
        -   fazendo com que a flag precise ser grande (8~bits).
        -   O payload nao pode conter essa sequencia.
        -   Payload grade -> Sequência grande (para diminuir o overhead por bit stuffing)
        -   tamaho da sequencia =~ log 2 tamanho do payload (isso é uma heuristica porbabilistica)
        -   Bit stuffing
            -   Elimina/escapa a sequencia do payload inserindo dummy bits.
            -   Já predefinido no protocolo
            -   What happens if an escape byte occurs in the middle of the data? The answer is that it, too, is stuffed with an escape byte
    -   <u>Usando caracteres</u>
        -   Reservando caracteres especiais para estabelecer a comunicação
        -   (-) muito propenso a erro
        -   (-) o payload nao pode conter esses caracteres especiais
    
-   **Detecção de erro**
    -   <u>CRC</u>
        -   cyclic redundancy check
        -   de 16 a 32 bits
        -   CRC8 algorithm: see in *exercices* (fiz com ajuda desse [calculator](http://www.sunshine2k.de/coding/javascript/crc/crc_js.html))
        -   para pacotes grandes é mais rápido usar CRCs grandes
        -   (+) garante a detecção do erro
    -   <u>Checksum</u>
        -   soma (normalmente em complemento de 1, por que dá pra enviar de forma mais compacta) do conteúdo
        -   Checksum16 algorithm: see in *exercices*
        -   (-) um erro pode compensar o outro
        -   (+) rápido
    -   <u>Luhn algorithm</u>
        -   not much used in web, but I register here for convenience
        -   much used to validate documents
    
-   **Correção de erro**
    -   precisa de alguma redundancia
    -   cada sequencia é recodificada de alguma forma (ex: um piece-wise checksum)
    -   se der erro, aquela sequencia é alterada de forma que a detecção de erro bata
    
-   **Others**

    -   Link layer challenges (Source Cisco BRKIOT-2020, 2015):

        ![image-20201226113031179](Images - Redes de Computadores/image-20201226113031179.png)

### Logical Link Control (LLC)

* The primary function of LLC is to multiplex  protocols over the MAC layer while transmitting and likewise to  de-multiplex the protocols while receiving.
* LLC provides hop-to-hop flow and error control.
* It allows multipoint communication over computer network.
* Frame Sequence Numbers are assigned by LLC.
* In case of acknowledged services, it tracks acknowledgements
* **Multiplexação**

  - Como os frames são organizados na transmissao

  - Necessário se o número de canais de entrada/saida é maior que o numero de canais de comunicação (ou seja, quase sempre)

  -   <u>Em frequencia</u>
      
      - *Frequency Division Multiplex (FDM)*, ou *Multiplexação por Divisão em Frequência* (MDF)
      
      - Comunicações “lado a lado” na frequência
      
        <img src="Images - Redes de Computadores/image-20200308182627936.png" alt="image-20200308182627936" style="zoom:67%;" />
      
  -   <u>Em tempo</u>
      
      -   *Time Division Multiplex (TDM)* ou *Multiplexação por Divisão em Tempo* (MDT)

### Medium Access Control (MAC)

* How LLC will interact with the physical media
* How they share it? For how long each one uses it?
* **Functions**
  * It provides an abstraction of the physical layer to the LLC
  * Encapsulates frames so that they are suitable for transmission via the physical medium.
  * Resolves the addressing of source station as well as the destination station, or groups of destination stations.
  * who is allowed to access the media at any one time: solve collisions, determine channels, initiate retransmissions, etc
  * Generates the frame check sequences, thus contributing against transmission errors
* **MAC address**
  * único de cada de rede
  * dividio em duas partes: fabricante e unico
  * broadcast = FF-FF-FF-FF-FF-FF
  * multicast =  01-00-5E-XX-XX-XX
* **Protocols - Reserva fixa**
  * Or *Channels partioning*
  * fair, but not efficient
  * inefficient at low load
  * <u>acesso múltiplo por divisão nas frequências (FDMA)</u>
    * cada banda, um utilizador
    * Variação para full-duplex: FDD, duas bandas (transmissão e recepção)
  * <u>acesso múltiplo por divisão nos tempos (TDMA)</u>
    * cada intervalo, um utilizador
    * Variação para full duplex: TDD, dois intervalos (transmissão e recepção)
  * <u>acesso múltiplo por divisão de código (CDMA)</u>
    - hum…wtf? 
    - Uma loucura que envolve modular seus dados por uma sequência pseudo aleatória, espalhar isso no espectro, e invocar o demônio
    - (+) Não há limitação para o numero de peers conectados, mas quanto maior a chance de colisão também aumenta
    - (+) acesso simples, sem intervalos de tempo ou bandas de frequência
    - (+) imune a multipercursos
    - (-) complexo pra porra
    - (-) exige controle de potência preciso
* **Protocols - Reserva aleatória**
  * Or *Random access*
  * inefficient at high load
  * <u>Slotted ALOHA</u>
    * Have fixed slots, but thry Are not pre assigned
    * You transmit, if no collision then keep transmitting I  the next slot
    * If collision, retransmit In next slot with a fixed probability $p$
    * Simple, but not that efficient
    * Required clock
    * Simple Aloha (no slotted): No synchronisation; more energy efficient
  * <u>CSMA/CA</u>
    * Carrier sense Multiple acess with colision detection
    * Carrier Sense: Antes de transmitir é verificado se o meio está
      disponível. If idle, transmit a frame
    * Multiple Access: Vários equipamentos podem transmitir no mesmo
      meio
    * Collision can happen because of propagation delay
    * Does not work that well in wireless
    * Colition Detection
      * Durante a transmissão do pacote é verificado se
        ocorreu uma colisão
      * Um adaptador aborta transmissão ao perceber que outro
        transmite
      * Emite um sinal de congestionamento (para todos saberem que houve colisão)
    * backoff exponencial
      * Ao retransmitir após colisão aguardam um breve período de tempo
      * Após m colisões, espere $k*512*temposDeBit$
      * $k$ = numero inteiro aleatório entre $0$ e $2^m -1$ 
      * Tempo de bit = 0,1us para ethernet 10Mbps
* **Protocols - Reserva dinâmica**
  * Or *Taking turns*
  * <u>Pooling</u>
    * Not robust: because of master
    * Pooling overhead
    * A node can talk if the master allows
  * <u>Token ring</u>
    * Not robust: One computer have the token
### Protocols

* **IEEE 802.3: ethernet**
  * Wired lan
  * diferentes velocidades: 2 Mbps, 10 Mbps, 100
    Mbps, 1Gbps, 10G bps
  * diferentes meios da camada física: fibra, cabo
  * Cada variação é regido por um protocolo IEEE diferente
  * Diferentes formas de codificação física (NRZ, MLT,)
  * Frame format
    -   preambulo: emula um clock
    -   Campo de dados: 46 a 1500 bytes
    -   SFD: para sincronizar
    -   CRC 4 bits
  * Detecção de erros
    -   colisões no meio
    -   frames com tamanhos errados
    -   corrupção (CRC não bate)
    -   entre outros
  
* **HDLC**
  
  -   Normally implements both LLC and MAC, but some variations (like Cisco HDLC) implements just MAC sublayer
  
* **ITU-T G.hn**
  -   To local area network
  -   Uses existing home wiring (power lines, phone lines and coaxial cables)
  -   Have 3 sublayers: application protocol convergence, logical link control and media access control
  
* **Auxilary: ARP**
  * to discover Mac address having ip address 
  * Broadcast asking, receive the unicast answer
  * defined by RFC 826
  * ==se a camada de rede for IPv6, o ARP é substitudo por ICMPv6==
  * Tabela ARP
    * Cada nó IP (Host, Roteador) de uma LAN possui tabela ARP mapeando IP->MAC de alguns dispositivos
    * Possui um TTL (Time To Live): tempo a partir do qual o mapeamento de endereços será esquecido
  
* **Auxiliary: FDDI**
  * Used in local area networks
  * uses optical fiber as its standard underlying physical medium
  * Nowedays obsolete

* **Modbus**

  * can take place over RS 232,Rs 485,RS 422,Radio,Satelite,TCP/IP, etc]

  * ==is it here or application layer?==

* * 

* **IEEE 802.15.4**

  * specifies both a physical layer and a medium
  * access control for low power wireless networks targeting reliable and scalable communications

* **LTE (Long-Term Evolution)**

  * high-speed data transfer between mobile phones
  * Based on GSM/UMTS network technologies 
  * multicasting and
    broadcasting services.
  * LTE-A (LTE Advanced)
    * including bandwidth extension which
      supports up to 100 MHz
    * downlink and uplink spatial multiplexing
    * extended coverage
    * higher throughput
    * lower latencies

* * 

#### **USART**

* Universal Synchronous/Asynchronous Receiver Transmitter

* Implementado em hardware na maioria dos microcontroladores

* full duplex (registradores e fios independentes)

* sincrono ou assincrono

* envia um bite de cada vez

* start bit, dados, paridade, 2 stop bits

* tensão idle em alto

* LSB goes first

* (-) only one-to-one communication

* (+) simple

* **Wiring**

  * rx no tx
  * tx no rx
  * é push/pull, se conectar errado queima

* **Dataframe:**

  ![image-20200919165721382](Images - Redes de Computadores/image-20200919165721382.png)
  
* **Errors**

  * Buffer overrun: data-in buffer is filled faster than the CPU retrieve the incoming characters; data will be lost	

#### I2C

* Ou *twin wire*

* A intel desenvolveu um protocolo parecido, *System Management Bus* (SMBus)

* síncrona

* para pequenas distâncias

* permite conectar vários devices

* Um master, varios slaves

* half duplex

* MSB goes first

* **Endereçamento**

  * cada device possui um endereço (7 ou 10 bits)
  * o endereço vêm fixo no device
  * Alguns devices permitem trocar o endereço por hardware (deixam um pino disponível)

* **Wiring**

  * coletor aberto (precisa de pull up, geralmente 4k7 à 3.3V)

  * SDA: dados

  * SCL: clock

  * todos conectados juntos

    ![image-20200919215025802](Images - Redes de Computadores/image-20200919215025802.png)

* **Usage**

  * mestre escreve o endereço

  * slave dá o ack

  * se for write, master manda

  * se for read, slave manda

    ![image-20200919215800947](Images - Redes de Computadores/image-20200919215800947.png)

#### SPI

* Mais simples que I2C

* Síncrona

* para pequenas distâncias

* full duplex

* 1 master, varios slaves, mas 1 ativo de cada vez 

* MSB goes first

* (-) use more wires than I2C

* (+) simple

* (+) faster than I2C

* **Wiring**

  * SS: slave select; ativade em baixo

  * MOSI: master output, slave input

  * MISO: master input, slave output

  * SCLK: clock, gerado pelo master

  * podem estar em parelelo (um SS para cada device), ou em série, como à seguir:

    ![image-20200919230234928](Images - Redes de Computadores/image-20200919230234928.png)

#### OneWire

* síncrona, sincronizados pelo tempo ($us$) a invez de clock, com pulsos de sincronismo
* para pequenas distâncias
* half duplex
* simples
* taxas bem menores que SPI e I2C
* 1 master, varios slaves
* Múltiplos dispositivos comunicam-se com o uso de identificadores
  pré-definidos (64 bits, 8 da familia, 48 do numero de serie, 8 de crc)
* ==o proprio fio consegue alimentar????==
* **Wiring**
  * precisa de pull up (geralmente 4k7, à 3.3 ou 5V)
* **How it works**
  * lı́der inicia transmissão com um pulso de reset com, no mı́nimo, 480μs
  *  Um seguidor indica sua presença com um pulso de, no mı́nimo, 60μs
    após o lı́der liberar o barramento.
  * Para enviar “1”, o lı́der envia um pulso de 1 − 15μs.
  * Para enviar “0”, o lı́der envia um pulso de 60μs
  * …

#### USB

* Universal Serial Bus

* Also implements physical layer

* Master slave

* Up to 127 conected devices, directly or using hubs

* (-) very complex

* allow interrupts… I read that this is quite misleading, and is still some kind of pooling

* **Physical layer**

  * Each USB cable can be up to 5 m
  * encoded using NRZI (non return to zero inverted)
  * connector “A” for the upstream side and connector “B” for the downstream side

  ![image-20200926164201420](Images - Redes de Computadores/image-20200926164201420.png)

  ![image-20200926172938926](Images - Redes de Computadores/image-20200926172938926.png)

  > 2.0 connetors

* **IDs**

  * The Device Descriptor includes the USB VID (Vendor Identification) and PID (Product Identification) numbers. 
  * The operating system uses this pair of numbers, VID_PID, to determine which device driver shall be used for this device. 
  * Note that the VID number is issued by having membership in the USB implementors forum, so that's kind of a problem if you're an individual inventor. The USB Implementers Forum (USB-IF) charges a fee to license a Vendor ID.

* **Conversores serial-USB**

  * FTDI chip such as FT220X datasheet provides the USB "serial interface engine"

### Protocols - wireless

* **IEEE 802.11: wifi**

  -   For Wireless Local Area Network (LAN)

  -   including but not limited to 2.4 GHz, 5 GHz, and 60 GHz frequency bands. 

  -   uses radio waves

  -   unlicensed spectrum

  -   uses CSMA/CA

  -   the sender wait some time (DIFS) before sending data, to avoid colision 

  -   if the receiver receives correctly, waits for some time (SIFS) before send ACK

  -   Uses CRC

  -   Compared with bluetooth, have higher-power, medium-range, higher-rate

  -   Range: 100m

  -   <u>Scanning</u>

      -   Search for devices

      -   An AP periodically send beacon frames, each of which includes the AP’s SSID and MAC address

      -   passive: the host waits to be contacted by the access point

      -   active: the host requests (Probe Request)

          ![image-20200627135452116](Images - Redes de Computadores/image-20200627135452116.png)

  -   <u>Mobility</u>

      -   if host moves from one Access Point to another, inside the same subnet, it remains with the same IP

*   **IEEE 802.15.1: Bluetooth**

    * For wireless  personal area networks (PAN)
    * short-wavelength radio
    * short range, at low power, low cost, low-rate
    * unlicensed spectrum
    * Bluetooth Low Energy
      * ?
    * How it works
      * primeiro pareia, depois comunica
      * ==what else?==

*   **IEEE 802.15: Zigbee**

    * For wireless  personal area networks (PAN)
    * Compared with bluetooh, is lower-power, lower-data-rate, lower-duty-cycle applications
    * channel rates of 20, 40, 100, and 250 Kbps, depending on the channel frequency
    * unlicensed spectrum
    * Type of nodes
      * reduced-function devices: slave
      * full-function device: master (single device)
    
* **Z wave**

  * unlicensed spectrum

* **WiMAX**

  * IEEE 802.16
  * includes speficication for physical layer
  * wireless

* **Near Field Commuication (NFC)**

  * ==similar to RDID?==
  * frequency band at 13.56 MHz
  * supports data rate up to 424 kbps
  * Range: 10cm
  * communication between active readers and
    passive tags or two active readers can occur 

* **RFID**: see in IoT

#### **LoRa**

* communication protocol for low-power wide area networks (LPWANs)

* The overall architecuture is called LoRaWan

* Data rate: Upto 50Kbps

* (+) low power consumption

* (+) long range: a single base station can cover up to 20km, sometimes even 100km

* (+) high capacity

* (+) low cost

* (-) the gateway msut have high capacity

* Star topology

* End nodes are not associated with a particular
  gateway. Rather, when a node sends data, it is typically received by multiple gateways

* Energy efficiency is achieved in LoRaWAN through the use of the ALOHA method
  of communication: nodes are asynchronous and only communicate when they have
  data ready to be sent, whether scheduled or event driven

* uses AES
  encryption with key exchanges based on the IEEE EUI64 identifier.

* **LoRaPhy**

  * LoRaWan also defines the physical layer
  * have it’s own modulation technique
  * the specification varies slightly from region to region.

* **LoRaWan**

  * Overall arquitecture

    ![image-20201226115020470](Images - Redes de Computadores/image-20201226115020470.png)

  * <u>Device classes</u>

    * LoRaWan defines three device class capabilities targeting different applications with varying needs
    * Class A:
    * Class B:
    * Class C: 

## Network layer

* Logical communication <u>between devices</u>
* Structuring and managing a multi-node network
* address and route (roteamento)
* Node-to-node
* Some network protocols (not ip, but yes ATM architecture) guarantee delivery with a given order and delay
* **Network types**
  * See more about connection and connection-less services in *Protocols*
  * virtual circuit networks
    * connection-oriented
    * ex: ATM architeccture
  * datagram networks
    * connection-less services
    * Ex: IP
* **fragmentation** 
  * MTU max ? U unit, how much the link layer can handle
  * Network layer have to break the pachages
* * * 

### **Routing**

* How to go from A to B 
* objective: find the shortest path 
* A network can be modeled as a graph (with the edge’s distance normally representing the transmission delay, but it could be reliability or others)
* The network can be static (change slowly) or dinamic (change fast). Normally it is static
* :keyboard:Shell: `traceroute` (successive packets with increasing Time-To-Live, reporting where expired)
* :keyboard:Shell: `ping` (just test conectivity)
* **Autonomous Systems (AS)**

  * Agregação de roteadores por região
  * hierarquical organization of “separreted networks”
  * Roteadores da mesma AS usam o mesmo protocolo de roteamento
  * cada região tem autonomia para rotear como quiser 
  * ASN (autonomous system number): is a unique number assigned to an autonomous system by the Internet Assigned Numbers Authority (IANA)
  * interior routers:  information exchange within AS
  * Roteador de borda
    * identifica que o pacote não é da tua AS (aprende a artir de um proocolo iner AS)
    * Propaga isso para os outros roteadores da sua AS
    * “sumariza” distâncias
      às redes na sua própria área, anuncia a outros
      roteadores de fronteira de área
* **Forwarding table**

  * to each route ip , to what port send.
  * Allows to route intra and inter (through border router) Autonomous Sistems
  * Created by an routing algorithm
* **Algorithm: Dijkstra**
  
  * link state method: know global information ()
  * requires global knowledge 
  * optimal path
* **Algorithm: Bellman Ford**
  
  * distance vector method: known only local information
  * only need to know the neighbours
  * less messages exchanged
  * sub optimal (but converges to optimal)
  * good to dinamic situations 
* **Protocols: intra-AS**

  * Or *Interior Gateway Protocols (IGP)*
  * foca em desempenho

  * <u>Routing Information Protocol (RIP)</u>
    * Uses distance vector, updated every 30 seconds
    * uses hop count as a routing metric to find the best path
    * hops stored in 4 bits, so your network can have at max 15 hops of distance
  * <u>Open Shortest Path First (OSPF)</u>
    * Uses link state
    * Open source
    * Newer and more advanced than RIP
    * secure (every message is authenticated)
  * <u>Enhanced Interior Gateway Routing Protocol (EIGRP)</u>
    * Uses distance vector
    * Main standard
    * Proprietary from Cisco
* **Protocols: inter-AS**

  * foca em políticas administrativas: o quê e através de quem se roteia
  * <u>Border Gateway Protocol (BGP)</u>
    * Provê ao roteador de borda os mecanismos para ele cumpirir suas funções
    * determina as rotas a partir de políticas gerenciais
    * permite a uma nova subrete anunciar sua existencia
    * pode aprender sobre mais de 1
      rota para algum prefixo
    * Cada provedor só anuncia as rotas para seus clientes diretos

### Deterministic networks

* the time it takes for each packet to traverse a path from its source to its
  destination should be determined

* for real-time applications

* extremely low data loss rates, packet delay variation (jitter), and bounded latency

* most of internet runs on top of best effort technologies, while most of industrial applications run on top of fully deterministic technologies

* enables the migration of applications that have so far
  relied on special-purpose non-packet-based (fieldbus) technologies (e.g., HDMI,
  CAN bus, Profibus, etc.) to Internet Protocol technologies; See more in *IoT - OT/IT convergence*

  ![image-20201226112655159](Images - Redes de Computadores/image-20201226112655159.png)

### **IP (internet procotol)**

- For addressing and routing

- each machine is assigned an unique IP address

- Non reliable

- Controle de erros somente sobre seu cabeçalho (com checksum)

- Identificar unicamente uma rede na Internet; Identificar unicamente cada máquina de uma rede;

- hierarchical organization of the network

- addressing: Each IP identify an interface (one port of the router, uma placa de rede, etc)

- :keyboard:Shell: `ifconfig`

-   **Protocol: DHCP**
    
    - *Dynamic Host Configuration Protocol*
    
    - Formally, this belongs to the Application Layer
    
    - automatically gives you a ip, default gateway, subnet mask
    
    - cada vez que o dispositivo se reconecta recebe um IP novo do roteador
    
    - Assigned by a DHCP server 
    
    - Existe outras formas de implementar um IP dinâmico, além do DHCP
    
    - How it works
    
      - Runs above UDP
    
      - Broadcast dispovery: “hi DHCP server, can you gimme IP?”
    
      - A client can receive DHCP offers from multiple servers, but it will accept only one DHCP offer.
      
      - Unicast answers: “Yeah, this one is fine? Yes, please”
      
        ![img](Images - Redes de Computadores/DHCP_session.svg.png)
        
        - O que é configurado: IP, mascare de sub-rede, gateway padrão, IP de um ou  mais servidores DNS
        - A convessão de configurações pode ter um tempo limitado 
        - Intervalo de exclusão: sequencia de IPs configurados para não serem fornecidos (para reserva-los para IPs fixos, por exemplo)
        - depois de receber o IP, você pode reversar para receber sempre esse mesmo 
        - Depois de configurado uma vez, caso você precise configurar novamente, irá tentar mandar direto um request… se não houver resposta (ou seja, você não está na mesma rede), você manda um discovery 
    
-   **Recebendo um IP**
    
    - Entidade chamada [IANA](http://www.iana.org/)/[ICANN](http://www.icann.org/) distribui "cotas" de IP para entidades regionais
    
    - Os provedores (*Internet Service Provider*) recebem e repasam pros usuarios finais 
    
      ![image-20200311221236056](Images - Redes de Computadores/image-20200311221236056.png)


#### **Domain**

-   Registro nomimal de um IP

-   To make IP easier to remember

-   The translation is done consulting an DNS server 


-   :keyboard:Shell: `nslookup <domain>`: returns the IP address

-   Autoritative DNS servers: maintained by an organization the have the files (ex: facebook). holds the definitive information about a domain name

-   Non-autoritative DNS server: just send it foward to the autoritative (ex: dns root, 1º level, etc)

-   **Domain Name System (DNS)**

    -   Maps IP <–> Domain name

    -   distributed lookup tables

    -   Formally, it’s a protocol from the Application Layers

    -   generalized, distributed and hierarquical database system for storing a variety of information related to naming

    -   Each record (resource record, see below) in the database have a 

    -   Especificado na RFCs 1034 e 1035 

    -   Roda sobre UDP e usa a porta 53

    -   mensagens query e reply, com o mesmo formato de mensagem

    -   DNS message format:

        ![Image result for DNS protocol message](Images - Redes de Computadores/Dns_message.jpg)
        
    -   resource record (RR)

        -    Each record has name, type, class and Time To Live + type-specific data
        -   CNAME: maps one domain name (an alias) to another (the canonical name); the DNS lookup will continue by retrying the lookup with the new name
        -   A: IPv4 Address record
        -   AAAA: IPv6 Address record
        -   

-   **Hierarquical organization**

    -   You first ask the host file, computer cache, then local DNS, then root. After that the request goes to TLD and then go down in the hierarquy
    
    -   Nenhum servidor mantém todos os mapeamento
        nome-para-endereço IP
        
        ![image-20200710203758065](Images - Redes de Computadores/image-20200710203758065.png)
        
    -   DNS root
        -   Contactado pelo servidor de nomes local
            que não consegue resolver o nome
        -   possui uma tabela que
            indica qual DNS será responsável pela
            resolução dos domínios para cada extensão
            de domínio (TLD) diferente
        -   Who they are? https://root-servers.org/
        
    -   Top-level domain (TLD)
        -   Mais um nível nessa hierarquia
        -   Terminação com, org, net, edu…
        -   Can be generic (gTLD, like .net) or coutry code (ccTLD, like .br)
        
    -   DNS local server
        -   you can setup a server to provide the local DNS file 
        -   only caches information for local clients once it has been retrieved from an authoritative name server
        
    -   local DNS mapping
        -   Local to your machine
        -   in `/etc/host`
        -   you can setup your own mapping table

<img src="Images - Redes de Computadores/Hierarchical-DNS-system.ppm" alt="Image result for DNS hierarchy" style="zoom:67%;" />

-   **Subdomain**
    -   To organize sections of your domain 
    -   Ex: *store.yourwebsite.com*, where ‘store’ is the subdomain, ‘yourwebsite’ is the primary domain and ‘.com’ is the top level domain (TLD)
    -   You have to register it in a Domain Register
    -   SEO will interpret as a different website, so its better to keep all public website in one single domain
    -   Cookies set from a hostname, will also be sent to all subdomains
    -   Ex: if the website on “example.com” sets a cookie, the browser will also send this cookie when visiting “store.example.com”
    -   Do not confuse with Subdirectory
        -   Subdirectory ex: `yourwebsite.com/store`
        -   SEO will interpret as the same website
    -   www
        -   without www you will pass the cooekies set in the domian down to subdomains, always, and that: hurts performance; create privacity problems 
        -   This will not be a problem if you dont use subdomains (or use to something simple), but if it is as problem anything will stop the cookies from being passed
        -   https://www.bjornjohansen.com/www-or-not
        -   There is a problem that without www the DNS should point to a fixed IP address
        -   Good practice: choose one and the other should point there (with a HTTP 301 response code)
-   **Domain registers**
    -   god daddy
    -   blueHost

#### **Network Address Translation (NAT)**

* allow multiple devices to access the Internet through a single public address

* More secure, because internal computers are not directly acessable by others

* The router can change the internal ips without having to communicate it to  the others, or changing the isp without communicate the hosts

* **How it works**

  * Just the router receives an public IP 
  * Host computers are in a local network
  * We add info about the port to be able to map the packages back to the host
  * Router stores an NAT translation table

* **IP privado**

  -   Ou *interno*
  -   endereços reservados para aplicações locais
  -   Podem se repetir em redes diferentes
  -   Cada classe possui um range de IPs privados
  -   ==Para acessar um computador na mesma rede, precisa usar obrigatoriamente o IP interno? Não consigo acessar apenas com o externo?==

  ![img](Images - Redes de Computadores/Network-Address-Translation-NAT-Working-Principle.jpg)

#### IPv4

- IP version 4

- 4 bytes (e.g., 165.1.2.3)

- it is still most used than ipv6

-   **Campos**

    - Possui varias flags que não são mais usadas (por que outros protocolos já implementam ou por que são ruins)

    - Fife time: deveria ser por segundo, mas na prátca é feito por numero de roteadores

    - Opções: ninguem usa

    - Protocolo (8 bits): indica o protocolo do nível superior ( TCP ou UDP)

    - padding: gerante que o cabeçalho é múltiplo inteiro de 32bits

    - PayLoad: at least 20 bytes

    - Fragflag: I'm I  a fragment? (The last fragment is zero). Datagrams are fragmented by the limits of the link layer

    - Offset: if fragmented, how much displace the package when reassembling (in bytes)

      <img src="Images - Redes de Computadores/image-20200308192004044.png" alt="image-20200308192004044" style="zoom:80%;" />

      ![image-20200313154647338](Images - Redes de Computadores/image-20200313154647338.png)

- **IPs Especiais**

  -   0.0.0.0: mesma máquina
  -   127.0.0.1: loopback (Domain name “localhost”)
  -   255.255.255.255: para todos os host simultaneamente 
  -   Bits do host zero (0) identificam a rede, ex: 145.235.0.0 (classe B)
  -   Bits do host um (255) identificam broadcast, ex: 200.19.73.255 (classe C)


-   **Classes**

    - Ranges de IPs

    - Determined by prefix matching

    - possuem finalidades específicos

    - para acomodar as redes de tamanhos variados

    - ==bits reservados do host: 0 e 255==

    - Cada endereço IP inclui uma identificação de rede e uma de
      host, com tamanhos variáveis dependendo da classe

    - identificação de rede

      -    Todos os sistemas na mesma rede física devem ter a
           mesma identificação de rede

    - Identificação de host

      -   identifica uma estação de trabalho, servidor,
          roteador, ou outro host TCP/IP dentro de uma rede
      -   endereço para cada host deve ser único

      ![Image result for IPv4 classes](Images - Redes de Computadores/unnamed.png)

    -   Classe A (0~127): poucas redes, muitos dispositivos

        -   países, grupos de países, etc

    -   Classe B (127~191): equilibrado

        -   estados, regiões, etc
        -   o ifsc está nessa classe

    -   Classe C (192~223): muitas redes, poucos dispositivos 

        -   Cidades, bairros, etc

    - Classe D (224~239): pacotes especiais

    - Classe E (240~255): reservado

    -   Quem organiza quem vai pra onde?

        -   IANA (mundial)
        -   RNP (Brasil)

-   **Endereços privativos**

    * Não propagados por roteadores, ficam só interno
    * Classe A: 10.0.0.0
    * Classes B: de 172.16.0.0 até 172.32.0.0
    * Classes C: de 192.168.0.0 até 192.168.255.0

-   **mascara de subrede**

    -   Permite uma organização hierarquica, com cada roteador organizando uma sub rede
    -   Separa-se bit para identificar o Host (dispositivo) e a network (rede)
    -   daquela faixa de enreços, quais bits vão ser utilizados pros dispositivos 
    -   Cada byte é 0 (bits da rede) ou 255 (bits do host)
    -   ==outra forma de representar, ex:== “192.168.4.25/24” means that there are three consecutive sets of eight 1’s in the subnet mask. (And 3 x 8 =24.), so it is 255.255.255.0.
    -   Todos os hosts de uma mesma rede deverão ser configurados com a mesma máscara de sub-rede
    -   Submask calculator tool: http://sipcalc.tools.uebi.net/index.php
    -   <u>Why it is needed</u>
        -   Necessário para configuração do cliente: the network and host addresses above cannot be deter-
            mined unless you have more information.
        -   Necessário para o roteamento dos pacotes (the routers that pass pack-
            ets of data between networks do not know the exact location of a host for which a
            packet of information is destined. Routers only know what network the host is a
            member of and use information stored in their route table to determine how to get
            the packet to the destination host’s network)
    -   <u>Definido pelas classes</u>
        -   Classe A: máscara será 255.0.0.0
        -   Classe B: máscara padrão será 255.255.0.0
        -   Classe C: máscara padrão será 255.255.255.0
    -   <u>CIDIR</u>
        -   like sub net mask, but to arbiraty ranges

* **Endereço da rede**

  * Atribuidos à ==redes?==
  * endereços abstratos, não estão associados a nenhum equipamento (o campo HOSTID é nulo [all zeros]).
  * Ex: 192.128.3.0
  * usados para roteamento

* **Protocolo auxiliar: ICMPv4**

  * Internet Control Message Protocol

  * Slightly above IP (runs of top of it)

  * protocolo para temporização

  * para fornecer relatórios de erros à fonte original

  * Especificado em RFC 792

  * “parceiro” do protocolo IP

  * relatar eventos inesperados, erros (ex: novo roteador adicionado, fluxo excessivo, Destination Unreachable, etc)

  * Mensaguens de consulta: fornecer eco, fornecer timestamp, Solicitação e anunciação do roteador

  * não passa pra session nem link: serve só pra network layer

  * as mensaguens do ICMP ficam encapsuladas dentro dentro do datagrama IP, com o campo *protocol=1* e o payload como sendo a mensaguem do ICMP

    ![image-20200313140346452](Images - Redes de Computadores/image-20200313140346452.png)

#### IPv6

-   IP version 6
-   supports more addresses (8 sequences of 4 hexadecimal characters)
-   128bits
-   Codificando ipv6 como ipv6: IPv4 = 1.2.3.4 IPv6= ::0102:0304 (e o resto zero)
-   ==fragmentation was disallowed?==
-   IPv5: priorizava stream de dados, mas era muito lento para packotes normais
-   nem tudo é usado para endereçamento
    -   os bits mais significativos representam informações adicionais (provedor, tipo de assiante, entre outros)
    -   Alguns bits são por default zero 
    -   com mais possibilidades de indentificação dá pra priorizar quem paga mais… igualidade vai pro saco
-   Campos
    -   campos nao usados do IPv4 foram removidos, e  alguns mudaram de nome
    -   Traffic class: semelhante ao ipv4 Type of Service, definindo prioridades
    -   campo adicionado: flow label
        -   Para stream
        -   se é stream ou não, qual resolução, entre outro
-   IPv6 uses the Neighbor Discovery Protocol and its extensions such as Secure Neighbor Discovery, rather than ARP. 
-   **Protocolo auxiliar ICMPv6**
    -   idem ICMPv4
    -   mais tipos 
    -   novas mensaguens: Neighbor Discovery (Substitui o ARP), Neighbor Reply

## Transport layer

-   Logical communication <u>between processes</u>
-   End-to-end
-   Collect and distribute data, in a logical abstraction
-   All necessary to logical transport: control, segment, multiplex, etc
-   **Flow control**
    -   evita inundar <u>receptor</u> mais lento
    -   deal with a sender that systematically wants to transmit frames faster than the receiver can accept
    -   <u>feedback-based flow control</u>
        -   Executado pela entidade receptora
        -   the receiver sends back information to the sender giving it permission to send more data or at least telling the sender how the receiver is doing
        -   Pode ser implementado por *stop and wait* ou sistema de créditos
    -   <u>rate-based flow control</u>
        -   the protocol has a built-in mechanism that limits the rate at which senders may transmit data, without using feedback from the receiver.
        -   Is not used
-   **Congestion control**
    -   Evita inundar <u>rede</u> mais lenta
    -   modulates traffic entry into a telecommunications network
    -   Congestion causes lost of packages and higher delays 
    -   typically accomplished by reducing the rate of packets.
    -   <u>Method 1: no explicit feedback</u>
        
        -   The confestion is infered by the host using observed loss or delay
        -   Used by TCP
        -   limit the differece between lastByteSent and LastByteAcknowledged (LBS-LBA<limit)
        -   The limit is named in TPC *cwnd* (congestion window), informado pelo hot
        -   
        -   Transmit as fast as possible to not go beyond the limit
        -   This limit increases with time, adjusted by bandwidth probing
        -   <u>*bandwidth probing*</u>
            
            -   Increase cwnd in no package loses (probably because of buffer overun), decrease if loses
            
            -   slow start: Increse rate exponentially
            
                -   só de vez em quando o  servidor devolve um ACK. Este ACK basicamente serve  para atualização do campo window. 
            
                    Quando a transferência  se comporta desta maneira - 
            
                -   o cliente envia rajadas direto, seguindo a  equação $10 * 2^n$, sem que espere pelo ACK final de cada rajada
            
                -   Quando houver pacote com  erro ou perdido, o receptor envia dois ACKs seguidos com o mesmo número,  alertando o cliente que aquele pacote foi perdido.
            
            -   congestion avoidance: increase rate linearly (much more conservative)
            
            -   You can begin with slow start and then change to congestion avoidance after the cwnd reach a treshold (named in TCP *ssthresh*)
    -   <u>Method 2: explicit feedback</u>
        
        -   Router tells the hosts when the network is congestioned
        -   used by ATM
-   **Segmentação**
    -   Ou *fragementação*
    -   dividir um pacote em vários 
    -   ethernet: 1500 bytes
-   **Multiplex**
    
    -   How several processes use the same physical interface de rede
-   **Ensuring reliability**
    
    -   ?
-   **Port**
	
	* Logical connection place
	* Identificação de processos
	* Permite que o SO gerencie o acesso de vários processos à mesma interface de rede
	* The way a client specifies a particular server (application) on a network
	* :keyboard:Shell: `netstat –a` (ver portas usadas e processos)
	* There are 65536 (16-bit unsigned integer) ports in TCP, some of then assigned by default
	* <u>Portas reservadas (0~1023)</u>
		* Or *well know ports*
		* Exclusive for specific applications 
		* High level protocols (like http) have preassigned ports, because they wil specify the server using de URL
		* Port 80 → default HTTP
		* Port 21 → default FTP
	* <u>Portas liberadas (1024~49151)</u>
		* Or *registered ports*
		* semi reserved
		* User written programs should not use these ports
	* <u>Client programs (49152~65535)</u>
		* or ephemeral ports
		* free to use these in client programs
		* are dynamically assigned
		* Can be reused once the session is closed
### **Socket**

* communication flow between two programs running over a network

*  can also be used for communication between processes within the same computer

*  Analogy: if the transport layer is the hallway, the socket is just the door

*  1 port = 1 socket

* One process can open multiple sockets

* A single listening port can accept more than one connection simultaneously. The number of concurrent open connections is limited by server ‘s resources allocated for file descriptor.

* The OS manages the socket, the application will control it 

* Definido por: protocolo (TCP ou UDP), end (porta+IP) local e remoto, processo (PID) local e remoto

  ![image-20200513201154473](Images - Redes de Computadores/image-20200513201154473.png)

* I’ll write mainly about TCP sockets, but the main difference is that in UDP sockets does’nt need to connect

* Client socket: who will request

* Server socket: who keeps listening

* I’ll write mainly thinking about python, but is is language agnostic

* **WebSocket**: application level protocol based on sockets (see in *Application Layer*)

* **Socket vs HTTP REST**

  * (+) have less overhead
  * (+) full-duplex
  * (-) harder to use
  * (-) Hard to handle parallel requests
  * when you need something statefull (can stay open)

* **Basic API**

  * Almost any programming function will provide a socket implementation with a similar API

  * `socket()`: cria e retorna file descriptor

  * `bind()`: vincula o processo a uma porta. Obrigatório para o servidor, opcional pro cliente (só se você quiser fixar a porta)

  * `listen()`

  * `accept()` (permite receber várias requisições paralelas; bloqueia até receber; retorna o socket)

    * The accept() system call is used with connection-based socket types  (SOCK_STREAM, SOCK_SEQPACKET). It extracts the first connection  request on the queue of pending connections for the listening socket,  sockfd, creates a new connected socket, and returns a new file  descriptor referring to that socket. The newly created socket is not  in the listening state. The original socket sockfd is unaffected by  this call. (http://linux.die.net/man/2/accept)
    * Usually, this newly created active socket object is then handed off to another process or thread, then `accept()` can be called *again* on the passive socket

  * `connect()` (inicia a conexão, manda o hanshake; retorna o socket)

  * `connect_ex()`

  * `send()`

  * `recv(MaxBufLen)` (blocking by default)

  * `close()`

    <img src="Images - Redes de Computadores/sockets-tcp-flow.1da426797e37.jpg" alt="TCP socket flow" style="zoom:50%;" />

* **Usage**

  * [Very good videoclass](https://www.youtube.com/watch?v=Lbfe3-v7yE0)

  * create a socket object using `socket.socket()`

  * Socket type: `socket.SOCK_STREAM` (TCP) or `socket.SOCK_DGRAM` (UDP)

  * `AF_INET` is the Internet address family for IPv4

  * Quando um recv retornar 0 bytes, significa que o outro lado finalizou a conexão (ou está no processo de fechamento) da conexão

  * Applications are responsible for checking that all data has been sent;  if only some of the data was transmitted, the application needs to  attempt delivery of the remaining data

  * <u>Simple single transfer</u>

    * Use a client socket for one exchange (open->connect->request…receive->destroy)

    * you can just read once, like:

      ```
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      s.connect((socket.gethostname(), 1234))
      msg = s.recv(64)
      ```

    * Or iIf you wanna be sure that you read the entire response AND If you supose that the other side (server) will close the connection, you can do:

      ```python
      full_msg = ''
      while True:
          msg = s.recv(8)
          if len(msg) <= 0: break
          full_msg += msg.decode("utf-8")
      ```
    
  * <u>fixed length</u>
  
    * predefined and accorded
    * https://docs.python.org/3.8/howto/sockets.html
  
  * <u>delimited message</u>
  
    * Or *shrug*
    * Waits for a specific end character
  
  * <u>Fixed-length header</u>
  
    * indicate how long they are
  
    * This is the best option
  
    * you may not get all 5 characters in one `recv`, specially in high network loads
    
    * Adding the header to the message:
    
      ```
       msg = f"{len(msg):<{HEADERSIZE}}"+msg
      ```
      
    * To decode this message:
    
      ```python
      ## Decode header
      headerReceived = 0
      headerData = []
      while headerReceived < HEADERSIZE:
          data = conn.recv(min(HEADERSIZE-headerReceived, 4))
          headerData.append(data)
          headerReceived+=len(data)
          #print('Received in header', len(data), "bytes")
      print("Full header received")
      
      ## Decode payload
      payloadSize = int(b''.join(headerData))
      payloadReceived = 0
      payloadData = []
      while payloadReceived < payloadSize:
          data = conn.recv(min(payloadSize-payloadReceived, 4))
          payloadData.append(data)
          payloadReceived += len(data)
      print("Full payload received: ", b''.join(payloadData))
      ```
  
* **non-blocking sockets**

  * The ‘default’ socket is blocking, that is, your code will be stoped will receive response

  * `socket.setblocking(0)`, after creating the socket

  * multi-threaded server

    * each thread blocks while receiving data from the respective client
    * (-) creating threads have to much overhead

  * Simple non-blocking

    * Just set to non blocking

    * `send` returns imediatly

    * (+) allows to send a big file piecewise

    * (-)does not gurantee full sending, so you’ll have to send several times

      ```
      totalSent = 0
      while (len(data)):
      	sent = sock.send(data)
      	totalSent += sent
      	data = data[sent:]
      	...do something else...	
      ```

  * Using select

    * To manage the the requests, use `select`
    * cooperative squeduling 
    * The `select` call is blocking, but you can give it a timeout.
    * syscall for moniting events on file descriptors
    * monitors a set of sockets and tells us which sockets have data to be read

### Handling binaries

* This could be with sockets themselves, and in some sense there is not much difference between sending text and binary, but I think it’s a confusing subject

* I’ll write mainly about sending images, but it’s basically the same to any file

* The major problem is that not all machines use the same formats for binary data

* if both sides agree on a format or a serializer, OK

* To send python objects (example: dictionary), use a serializer like pickle

* :keyboard: Python example:

  ```
  d = {1:'hey', 2:'world'}
  serialized = pickle.dump(d)
  clientSocket.send(serialized)  
  ```
  
* Or, if the data is simple, you can serialize as string, like ` data=nparray.tolist()` and then `json.dumps(data)`

* Or send and entire file, like in https://stackoverflow.com/questions/27241804/sending-a-file-over-tcp-sockets-in-python

* Or codify as Base64

  * o base64 usa apenas caracteres ASCII para representar o dado binário, então ele é independente de sistema ou arquitetura, nao vai dar problemas manda-lo de um lado pro outro (fonte: Nagel)
  
  * https://stackoverflow.com/questions/6485790/numpy-array-to-base64-and-back-to-numpy-array-python
  
  * (para numpy array) uma pequena desvantagem de usar base64 ao invés de uma imagem (jpg, png): a dimensão e o tipo do array é perdido, então o receptor tem que saber dtype e o shape do array(veja o primeiro comentário do stackoverflow, tem que fazer um reshape). Se a imagem tiver tamanho fixo blz
  
  * ==send image (jpg) vs send array (npy)?==
  
  * :keyboard: Pythn example ([source](https://stackoverflow.com/questions/6485790/numpy-array-to-base64-and-back-to-numpy-array-python)):
  
    ```
    import base64
    import numpy as np
    
    # Send
    t = np.arange(25, dtype=np.float64)
    s = base64.b64encode(t)
    # Receive
    r = base64.decodebytes(s)
    q = np.frombuffer(r, dtype=np.float64)
    q = np.reshape(q,(32,32))
    ```
  
  * :keyboard: JS example, sending canvas image by websocket as uint8 ArrayBuffer:
  
    ```
    var img = canvas_context.getImageData(0, 0, 400, 320);
    var binary = new Uint8Array(img.data.length);
    for (var i = 0; i < img.data.length; i++) {
      binary[i] = img.data[i];
    }
    connection.send(binary.buffer);
    ```
  
  * :keyboard: JS example, sending file by websocket as blob:
  
    ```
    var file = document.querySelector('input[type="file"]').files[0];
    connection.send(file);
    ```
  
    

### Protocols

-   **UDP (User Datagram Protocol)**

    -   Unreliable

    -   Unordered delivery

    -   não orientado à conexão (orientado a datagrama), no handshake

    -   does not guarantee packet delivery

    -   Does not fragment nor do flow control

    -   Only check error over its header

    -   Max datagram size: 2^16 bytes (incluindo o header)

    -   Se passar desse tamanho, trunca

    -   (+) less network overhead, fast

    -   (+) No speed limit (because does not control congestion)

    -   (+) can do multicast

    -   Used in: DNS, SNMP, simple applications

    -   Formato:

        ![image-20200403232540556](Images - Redes de Computadores/image-20200403232540556.png)

    -   pseudo-header: vai junto com o payload, utilizado para
        verificação adicional e confirmação de que o
        datagrama chegou ao destino correto

-   **TCP (Transmission Control Protocol)**

    -   Reliable, Ordered delivery, Controle de erros com retransmissão, Controle de fluxo
    -   orientado à conexão
    -   full-duplex
    -   establish a connection between two machines
    -   reliable, each packet has a sequence number, and an
        acknowledgement is expected
    -   Packet delivery is guaranteed (resend if fails)
    -   Parâmetros
        -   Tamanho =  total do frame TCP
        -    flags (syn, fin, psh, rst, ack, urg)
        -   Urgent Poninter é um ponteiro para dados urgentes, contidos na área de
            dados.
        -   
        ![image-20200412230751630](Images - Redes de Computadores/image-20200412230751630.png)
    -   Numero de sequencia
        -   Cada host define o seu no começo da conexão
        -   Incrementado a cada mensaguem trocada
        -   Cada host reseta (muda) com frequencia determinada na RFC793
    -   Como é uma conexao
        -   Manda sys
        -   recebe sys/ack
        -   vai mandando e recebendo confirmação
        -   cada lado da conexão deve finalizar a conexão de
            forma independente
    -   MSS (Maximum Segment Size): tamanho do maior bloco de dados que poderá ser enviado
        para o destino (determinado pelo host destino)
    -   A necessidade de realizar uma retransmissão é
        detectada pela ausência do ACK
    
-   **Secure Sockets Layer (SSL)**
    
    -   Enhanced version of TCP
    -   allows client and server to agree on the cryptographic
        algorithms at the beginning of the SSL session
    -   ![image-20200515125600821](Images - Redes de Computadores/image-20200515125600821.png)
    
-   **Transport Layer Security (TLS)**

    -   Improvement of SSL (slightly modified version of SSL version 3)
    -   Normatization: RFC 4346

-   **Secure Reliable Transport (SRT)**

    -   For low latency video
    -   competidor of TCP
    -   Based on UDP
    -   AES encription
    -   https://www.srtalliance.org

-   Auxiliary protocol: Datagram Congestion Control Protocol (DCCP)
    
- Auxiliary protocol: Stream Control Transmission Protocol (SCTP)

## Session layer

* end-to-end
* sincronização e recuperação de falhas
* como as aplicações conversam
* handshakes, acknowledges, etc
* SOCKS protocol
  * this is a rare case: a true session layer protocol in real internet, not Application layer
  * foward TCP and (since version 5) UDP packets
  * used for proxying
  * SOCKS operates at a lower level than HTTP proxying: SOCKS uses a handshake protocol to inform the proxy software about the connection that the client is trying to make, and then acts as transparently as possible, whereas a regular proxy may interpret and rewrite headers
  * Version 5: more choices for authentication; UDP

## Presentation layer

* or *syntax layer*
* end-to-end
* data translator for the network
* Funtions data convetions (like character encoding), compression, encryption 


## Camada de aplicação 

* end-to-end

* Alto nível, abstrai quase tudo

* É a forma como tua máquina conversa com o servidor

* processo: programa que roda em um host

* **O que especifica**

  * Tipos mensagens: mensagens de pedido e resposta, etc
  * Sintaxe/formato: campos presetes (sintaxe) e seus significados (semântica)
  * sincronização e sincronização: quando os processos enviam e respondem às mensagens
  
* **Paradigmas**
  
  * The two architectures can be mixed
  * <u>client-server</u>
    * client request, server provide
    * (+) easy for a corporation to manage
  * <u>peer-to-peer (P2P)</u>
    *  compartilhamento de recursos/serviços diretamente entre sistemas
    * higly scalable
    * (-) hard to managle
    * (+) scales better to big number of clients
    * Example: skype (is hybrid, because a server manages the connection)
    * Arquitetura centralizada: um servidor mantêm um índice com quem-tem-o-que (Exemplo: Napster)
    * Arquitetura descentralizada: Cada nó mantém uma lista de vizinhos, que é
      construída mais ou menos de forma randômica (exemplo: eMule)
  
* **URL**

   * **U**niversal **R**esouce **L**ocator

   * Defined in RFC 2396

   * Identifica um local (recurso) na rede WWW

   * globally unique name for each document that can be accessed of the Web

   * Cannot contain special characters

   * Sintaxe:

     > protocol://hostname:port/path-and-file-name

   * Hostname → can be a domain (ex:*www.google.com*) or an IP

   * Path → server document base directory

### **Email protocols**

-   User agent: who acess (usually in browser or outlook-like)

-   mail server: provides the mailbox, message queue, implements SMTP, etc

-   Several web-based browser (agent) use http to access the server, and internally the server not necessarily use SMTP 

-   **POP3**
    
    * To retrieve emails from mail server
    * Download from erver then deletes it from the server
    * you can only access once from the server
    * Commands: list, retrieve, delete, quit
    * stateless
    
-   **IMAP**
    
    * To retrieve emails from mail server
    * Keep the emails in the server
    * statefull
    
-   **SMTP**
    * Simple Mail Transfer Protocol
    
    * to send emails between mail servers
    
    * port 25, on TCP
    
    * ACII based
    
    * handshake -> messages -> closure
    
    * response: status code and phrase
    
    * multiple emails can be sent in the same connection
    
      ![image-20200514134554806](Images - Redes de Computadores/image-20200514134554806.png)

### Filesystem protocols

* **Network File System (NFS)**

  -   Procotol to enable file sharing over a network

  -   permite a montagem de sistemas de arquivos remotos

  -   Os arquivos parecem ser locais

  -   RPC (Remote Procedure Call): componente que possibilita trabalhar com SOs diferentes

  -   XDR (External Data Representation): componente que possibilitar trabalhar com filesystems diferentes

  -   dá pra montar direto no boot

  -   stateless (não sabe quais arquivo estão abertos), com exceção do NFSv4

  -   Runs above TCP

      ![image-20200514204001182](Images - Redes de Computadores/image-20200514204001182.png)

*   **SMB (Server Message Block)**

    -    that is built on top of TCP/IP
    -   Compatible with windows
    -   foi recentemente rebatizado com o nome CIFS (Common Internet File System)
    -   Software that  implements: Samba (open source)

*   **Direct Access Protocol (DAP)**

    -   Communiccation protocol for accessing and maintaining distributed directory information services
    -   LDAP: lighweigh DAP
    -   read, move, modify and etc files
    -   Kerberous: authentication protocol
    -   The directory service works like a network database, storing information as trees of entries
    -   Schemas
        -   How the directory service is organized
        -   Much like databases
    -   object classes, attributes, name bindings and namespaces
    -   Operations
        -   Add
        -   Bind (authenticate): anonymous (no password), simple or SASL (secure)
        -   delete
        -   modify
        -   …
    -   Entries
        -   distinguished name, a collection of attributes, and a collection of object classes

### FTP

* connection oriented
* Default port: 21
* **FTP server**
  * Example of open FTP server: [ftp://speedtest.tele2.net/](ftp://speedtest.tele2.net/) (user *anonymous*, any password)
  * Softwares
    * vsftpd: opensource; for linux; 
* **FTP client**
  * ?
  * Softwares
    * Filezilla
* **FTP proxy**, see in the *Proxy servers* section
* **Other protocol: SFTP**
  * Secure
* **Other protocol: TFTP**
  * simple
  * connectionless, uses UDP
  * unreliable

### Protocolo HTTP

-   **H**yper**T**ext **T**ransfer **P**rotocol

-   maintained by [*W3C*](http://www.w3.org/)

-   There are currently two versions (1.0 and 1.1)

-   Based on the paradigm request-response between client and server

![1546016607110](Redes de Computadores - Images/1546016607110.png)

-   Permits negotiating of data type and representation (json, etc)

-   The current request does not know what has been done in the previous
	requests.

-   Built above TCP (i was changed to UDP in HTTP/3)
	
-   ASCII based (therefore human readable)
	
-   Is stateless: the server does not information about the previous requests
	
-   half-duplex
	
-   **Connection types**
	
	-   <u>Persistent</u>
		   -   Default in HTTP/1.1
		   -   Servidor deixa a conexão TCP aberta após o
		       envio da resposta
		   -   é possível estabelecer uma conexão TCP, enviar uma solicitação e
		       obter uma resposta e, logo em seguida, enviar solicitações adicionais e receber respostas
	          adicionais, amortizando o custo da instalação e liberação do TCP para cada solicitação.
		   -   As requisições podem ser paralelas (só vai pedindo) ou não (espera receber a resposta para enviar a próxima)
		   -   (+) Less overhead
		
	-   <u>Non persistent</u>
	    -   Default in  HTTP/1.0
	    -   Conexão TCP é fechada após o servidor http enviar o
	        objeto
	    -   Utiliza um soquete para uma transferência
	    -   (-) Sobrecarrega servidores que atendem vários clientes ao
	        mesmo tempo
	
-   **Browser**

    -   Interprets the user input, mount the requisition and send

    -   Receive the requisition, interprets and
    	render

    -   Terminal browser softwares: lynx, w3, etc

    -   Terminal simple request: curl

        ![1546016619611](Redes de Computadores - Images/1546016619611.png)

-   **What the server does when a request comes?**

	-   Find the requested files and send, or

	-   Execute the requested program and send his return, or

	-   Returns error message

-   **HTTP server software**

	-   Implements the protocol in a computer
	-   E.g: Apache

-   -   -   

-   **Usefull links**

- * <http://www.ntu.edu.sg/home/ehchua/programming/webprogramming/http_basics.html> 		 		

- **Methods**

  -   Ou *métodos* ou *verbos*
  
  -   Indicate what the server must do with the requisition
  
  -   Case sensitive: must be uppercase
  
  -   *Get*
  
      -   Most simple and used method
      -   Issuing an HTTP URL from the browser always triggers a GET
          request
      -   Is usually used to ask data, but can send some information
          in a *query string* appended after the URI with a ‘?’
      -   There is a server-specific threshold to the amount of
          appended data
      -   The data is organized in pairs of “name=value” and separed
          by ‘&’
      -   *Example*: appending data from a login:
  
          ```
          GET /bin/login?user=Peter+Lee&pw=123456&action=login HTTP/1.1
          ```
  
  -   *Post*
  
      -   Usually used to send data
      -   The query string will be sent in the body of the request message
          
      -   Can be used to send files
      
  -   conditional GET
  
      -   O request pode conter o campo condicional, tipo `If-modified-since:`
      -   Se a página não tiver sido modificada, o servidor só informa isso (status 304) e o browser usa a versão que está no seu cache
      -   se não, a página é retornada
  
  -   Head
  
  -   Put
  
  -   Delete
  
  -   Trace
  
  -   Options
  
  -   Connect

* **AJAX**
  -   Asynchnous JavaScript and XML
  -   Tecnologia que permite fazer requisições assíncronas, usando o
      protocolo http

#### Request

-   From client to server

-   Have the following format:

![1546016690614](Redes de Computadores - Images/1546016690614.png)

* **Request line**

  * Sintax:

  ```
  request-method request-URI HTTP-version
  ```

  * Method

  * Universal Resource Identifier
    -   Resource → what we want in the server

    -   E.g: /index.html

  * HTTP version

    -   E.g: HTTP/1.1
  -   ==Have something builtin to authentication?==

* **Request header**
  * Some adicional information

  * Pairs of things, in that format:

  ```
  Header1: value1, header2: value2, …
  ```

  * Host
  * Connection

    -   What will happen after the response

    -   Close → default in HTTP 1.0

    -   Keep alive → default in HTTP 1.1
  * Accept
  * Accept-language

* **Request message body**

  * whatever you want to send
  * To send image alone, as encoded binary: Just send the binary data as-is in a POST body (using [cv2](https://gist.github.com/kylehounslow/767fb72fde2ebdd010a0bf4242371594) or [ByteIO](https://stackoverflow.com/a/52920154/12555523)), with the appropriate Content-Type header (e.g. image/jpeg)
  * To send alongside text: [encoded as base64](https://stackoverflow.com/a/54661844/12555523), or convert numpy to list (to send as json), or send in two different request, or send as a multipart request

#### Response

-   General formal

![1546016887472](Redes de Computadores - Images/1546016887472.png)

-   Example:

	![1546016896485](Redes de Computadores - Images/1546016896485.png)

-   **Response status code**

	-   200 → success
	-   4xx: error o client-side
	-   405 → error: method requested is not allowed/implemented to you
	-   404 → error: URI does not exists
	-   400 → error: bad request (like HTTP-version missing)
	-   5xx: error or server-side
	-   501 → error: method requested does not exist

### **MQTT**

-    Message Queuing Telemetry Transport
-    publish/subscribe protocol
-    comunicação assíncrona entre as partes
-    desacopla o emissor e o receptor da mensagem tanto no espaço quanto no tempo
-    usually runs over TCP/IP
-    can run over WebSocket or others, but it will have higher overhead
-    bidirectional (==full or half duplex?==)
-    (+) Minimum overhead (much less than HTTP)
-    (+) excelent when dealing with several parties comunicating togheder (instead of a simple point-to-point)
-    (+) good when netowork is not reliable (can keep a sincrnous communication)
-    ideal for IOT applications
-    **MQTT broker**
     -   acts as a post office
     -   recebe e roteia as mensaguens 
     -   Mosquitto broker: written in C
     -   Adafruit IO
-    **MQTT Client**
     -   qualquer coisa que possa interagir com o broker
     -   Each client can both produce and receive data by both publishing and subscribing
-    **Mensaguens**
     -   organizadas por tópicos
     -   Podem ter varios tipos
     -   header?
     -    tipos
          -    CONNECT
               -   Parameters: cleanSession, username, etc
               -   O cliente receberá uma mensagem CONNACK do broker.
          -    SUBSCRIBE
               -   Parameters: qualityOfService (garantia de recebimento ou nao)
          -   UNSUBSCRIBE
          -    PUBLISH
               -   Parameters: topicName, QoS, retainFlag
               -   \+ dados (texto ou binário)
     -   wildcards
         -   “+” matches one hierarchical level
         -   “#” stands for everything deeper in the tree
-    **Variation: AMQP**
     -   Advanced Message Queuing Protocol
-    **libraries?**
     -    Python mosquitto
     -   Paho MQTT
-    **Private brokers - The Gigants**
     -    aws
     -    azure
     -    google cloud
     -    They have their own SDKs, which are not open, and sometimes not even implement the whole MQTT specification
-    **Private brokers - Others**
     -    HiveMQ
          -    includes the load balancers, SSL certificates and a MQTT broker cluster 
          -    US\$ 7.50 hour (2020), with 1 week free trial of \$ 1.50 / Hour
          -    They are a free public broker as [demo](http://www.hivemq.com/demos/websocket-client/)
     -    CloudMQTT
          -    starting at \$5 a month (2020)
          -    billed by the second
          -    infrastructure on AWS
          -    Can also be deployed to Heroku… i dont know how it works
          -    bridge: built-in MQTT client in the broker which can connect to another broker and forward messages between the brokers.
     -    mqtt.flespi.io: acho que é de graça, e permite TLS
     -    Eclipse Mosquito free public demo: test.mosquitto.org:1883
-    **Client tools**
     -    Just to analyse, like WireShark
     -    MQTT X
     -    Mosquito CLI
     -    MQTT.fx
     -    MQTT Explorer

### WebSocket

-   provides full-duplex communication channels over a single TCP connection
-   designed to work over HTTP ports 80 and 443 as well as to support HTTP proxies and intermediaries
-   
-   Indicado pelo schema `ws://`
-   WebSocket Secure: ?
-   As vezes o proxy reverso precisa saber quereceberá websockets ([exemplo nginx](https://www.nginx.com/blog/websocket-nginx/))
-   
-   [RFC 6455](https://tools.ietf.org/html/rfc6455)
-   [Good tutorial](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)
-   handshake
    -   done with HTTP
    -   the same server's 80 port is shared by multiple TCP connections
    -   the client sends a key, the server response with a variation of the key
    -   The HTTP Upgrade header requests that the server switch the application-layer protocol from HTTP to the WebSocket protocol.
    -   After the upgrade, a new socket will be created, each with it’s own thread and ==it’s own port== (I think the server uses always the same port, only the client allocated a new random port)
-   messages
    -   transmissions are described as "messages"
    -   the messages have an specific framing, so you dont have to care about the TCP frames
    -   a message is composed of one or more TCP frames
    -   You can send data as a string, Blob, or ArrayBuffer
-   alternativas
    -   there are other ways to achieve bidirectional communication
    -   XMLHttpRequest
    -   iframe
    -   long polling: open an httprequest that will stay open for a long time 
-   Frameworks
    -   Socket.IO: JavaScript, both server and client
    -   Flask-SocketIO: to python flask, based on Socket.IO
-   Client programming
    -   define several event handlers, in which you must set the callback funtion
    -   `s.onopen`
    -   `s.onerror`
    -   `s.onmessage` event handler called when receive a full message
    -   `s.close()`
    -   `s.send('your message')`
    -   `bufferedAmount `: data still to be sent
-   Server programming
    -   ?

### Other protocols

-   **XMPP**

    -   Extensible Messaging and Presence Protocol
    -   mensagem instantânea (IM) ponto a ponto
    
    -   similar to MQTT
    -   much heavier
    
-   **Protocolo FTP**

    -   **F**ile **T**ransfer **P**rotocol

    -   (RFC 959

    -   Forma de enviar arquivos da máquina local para um servidor

    -   Software: filezila

    -   No encryption

    -   Uses athentication

    -    conexões separadas p/ controle, dados

    -    Servidor FTP mantém o estado: diretório atual, autenticação anterior, etc

    -    Comandos enviados em ASCII

    -   The file can be binary or ascii

    -   SFTP: secure (uses ssh encryption)

    -   TFTP: trivial ftp, very basic and without authenticaton. UDP based

        ![image-20200514202502690](Images - Redes de Computadores/image-20200514202502690.png)

-   **Telnet**

    -   não possui criptografia

    -   acesso remoto

    -   Estabelece um Terminal Virtual de Rede (NVT): shell padronizado para essas conexões

    -   Ambas as extremidades podem negociar as opções (full ou half duplex, formatação de caracters, etc)

    -   :keyboard: Shell: ` telnet nome-da-máquina-remota`

    -   :keyboard:Shell example (open connection and perform GET):

        ```
        telnet www.inf.ufsc.br 80
        GET /~willrich/ HTTP/1.1
        Host: www.inf.ufsc.br
        ```

-   **ssh**

    -    secure shell client
    -   Replaces Telnet
    
-   **whois**

    -   described by RFC 954 and RFC 3912
    -   simple, text-based TCP protocol
    -   port 43
    -   to query domains registered with in the Internet Assigned Numbers Authority (IANA) 
    -   The exact format varies from service provider to service provider
    -   ==RDAP queries?==
    -   https://www.arin.net/resources/registry/whois/rws/api/

-   **Modbus**

    -   used a lot in automotive
    -   master slave
    
-   **RDP**

    -   Remote Desktop Protocol
    -   does not allow the user of the remote computer to see (or control) their own screen during an RDP connection

# Comunications infrastructures

## Telephone System

* In *OSI Model* I wrote a brief comparison bet ATM model (used in the public switched telephone network) and OSI

*  uses twisted pair

* Telephone infrastructure:

  ![image-20200627142940364](Images - Redes de Computadores/image-20200627142940364.png)

  ![image-20200627170651962](Images - Redes de Computadores/image-20200627170651962.png)

* History

  * Alexander Graham Bell patented the telephone in 1876 (just a few hours ahead of his rival, Elisha Gray)
  * By 1890 in US, the three major parts of the telephone system were in place: the switching offices, the wires between the customers and the switching offices, and the long-distance connections between the switching offices
  * Prior to the 1984 breakup of AT&T, the telephone system was organized as a highly-redundant, multilevel hierarchy
  * 1984, AT&T was broken up into AT&T Long Lines, 23 BOCs (Bell Operating Companies), and a few other pieces. ==count decision to increase competition?==

* 

##  Mobile Telephone System

* Cellular telephony

* **History**

  * The first mobile system was devised in the U.S. by AT&T and mandated for the whole country by the FCC

  * AMPS (Advanced Mobile Phone System): US, 1982

  * Digital AMPS: compatible with AMPS

* **Generations**

  * 1G
    * much like radio

    * AMP protocol
* 2G
  * Digital Voice
  * D-AMPS, GSM, and IS-95
  * Better quality, SMS (Short Message Service), simple pictures, etc
* 3G
  * Digital Voice and Data
  * GSM, UMTS, CDMA2000 (sucessor of IS-95)
  * 4G
  * broadband Internet access
    * Allows Voip, gaming services, high-definition mobile TV, etc
* 5G
  
* GPRS

  * General Packet Radio Service
  * ==Maybe I should move radio to another section==
  * overlay packet network on top of D-
    AMPS or GSM
  * The available time slots are divided into several logical channels, used for different purposes. The base station
    determines which logical channels are mapped onto which time slots

* GSM

  * Global System for Mobile Communications

  * Sucessor of D-AMPS	

  * (+) better quality than D-AMPS

  * ==is this link layer in OSI? Or ir covers OSI Layers 1 to 3?==

  * The GSM standard for 2G cellular systems uses combined FDM/TDM (radio) for the air interface

  *  each mobile transmitting on one frequency and receiving on a higher frequency

  * a single frequency pair is split by time-division multiplexing into time slots shared by multiple mobiles

  * GSM architecture, using 2G:

    ![image-20200627140518640](Images - Redes de Computadores/image-20200627140518640.png)

## Television systems

* See in specific file

## Satelite systems

* According to Kepler's law, the orbital period of a satellite varies as the radius of the orbit to the 3/2 power
* broadcast media. It does not cost more to send
  a message to thousands of stations within a transponder's footprint than it does to send to one
* Geostationary Satellites

  -   To prevent total chaos in the sky, orbit slot allocation is done by ITU
  -   period of 24hrs
  -   altitude of about 35,800 km

- Medium-Earth Orbit Satellites

  -   lower altitudes  than Geostatinary

- Low-Earth Orbit Satellites

  - rapid motion

  -  do not need much power

  -  Due to their rapid motion, large numbers of them are needed for a complete system

  - Iridium system

    - planned since 1990

    - launched in 1997

    - bankrut in 1999, restart in 2001

    - positioned at an altitude of 750 km, in circular polar orbits

    - entire earth is covered

    - 66 satelites

    - targeted at telephone users located in odd places (satelite phones)

      ![image-20200220192441891](Images - Redes de Computadores/image-20200220192441891.png)

  -   Globalstar system

  -   Teledesic system

### GNSS

* Global Navegation Satalite System

* Receivers GNSS 	→ usually able to use all these systems 
* SBAS (Satellite-based augmentation systems) are regional systems that expand (with fully compatibility) an existing system, to a specific area
  
* **Historic**
  * GPS	: american system : since 1988
  * Glonass: sovietic system	: since 1988
  * Galileo: European system: since 2010? 
  * Compass: Chinese system	: since 2006
* **NMEA**
  * Protocol to communications of a lot of instruments (including GNSS receivers)
  * By default, the NMEA commands are output at 9,600bps, 8 data bits, 1 start bit, 1 stop bit, and no parity   
  * Example: `$GNGSA,A,3,21,25,31,12,,,,,,,,,1.95,1.73,0.91*19`
  * [C library](https://github.com/kosma/minmea)
* **Linx GNSS**
  * <u>Assembly</u>
    * 3.3V (with ripple<20mV)
    * Bypass capacitors should be placed as close as possible to the module
    * Rx from the FTDI in the TX of the gps 
    * Pin 11 → Backup battery supply voltage (must be powered to enable the module)
  * <u>Pulse per second (PPS)</u>
    * Rising edge
    * +- 11ns accuracy
    * Only with 5 or more satellites  
# The Internet

* An specific internetwork
* a vast collection of different networks that use certain common protocols
  and provide certain common services
* was not planned by anyone, is not controlled, not coordinated
* Its resilience comes from its architectural features
* The glue that holds the Internet together is the TCP/IP reference model and TCP/IP protocol stack
* WWW (World Wide Web)

  * application invented by CERN physicist Tim Berners-Lee
  * ==in what exactly it helps???==
* **History**

  * Grew from Arpanet and NSFnet 
  * grew exponentially after TCP/IP became the only official protocol on 1983

## **Wireless networks**

* To more details about specific protocols, see in the correspondent OSI layer
* Wireless PAN: mainly based on bluetooh or zigbee
* Wireless LAN: mainly based on Wifi
* Wireless WAN: mainly based on Mobile telephone or satelite
* Elements of a wireless network:

![image-20200627133300169](Images - Redes de Computadores/image-20200627133300169.png)

![image-20200704111058418](Images - Redes de Computadores/image-20200704111058418.png)

![image-20201226114831506](Images - Redes de Computadores/image-20201226114831506.png)

## Internet Architecture

![image-20200220185349327](Images - Redes de Computadores/image-20200220185349327.png)

* **ISPs (Internet Service Providers)**

  * Companies that offer individual users at home the ability to call up one of their machines and connect to the Internet

  * ISPs often cooperate between them 

  * Sometimes server farms even *become* the ISP (google sometimes does that), becoming what is called *Content Provider Network*

    ![image-20200513185947223](Images - Redes de Computadores/image-20200513185947223.png)

  * POP (Point of Presence)
    * where signals  are removed from the telephone system and
      injected into the ISP's regional network
    * ISPs typically have multiple PoPs
    
  * From client to ISP
    
    * See also in *Physical Layer - Transmission Midia* 
    * Internet discada: ocupa a linha telefônica (não é possível utilizar os dois ao mesmo tempo)
    * DSL
      * digital suscriber line
      * OSI physical layer standard
      * telehpone infractrusture
      * asymmetric digital subscriber line (ADSL), the most commonly installed DSL technology. a velocidade da conexão é maior em um sentido
      *  filtro de linha é utilizado para separar telefone de internet
      * (+) inherently more secure than cable (because the channel is not shared)
    * TV infrastructure
      * Coaxial Cable
      * faster than ADSL
      * Como o canal é compartilhado entre várias casas (veja em *Television Systems*), o tráfego de um vizinho influencia no outro (horário de pico é foda)
      * velocidades de download e upload são iguais
      * (-) Menos instável (internet cai, velocidade varia)
    * Rádio
      * por antenas
      * Barreiras próximas à antena podem atrapalhar (e muito) a conexão;
      * ==ausência de IP???==
      * (-) fortemente influenciado pelo clima
    * Satélite
      * é necessário ter uma antena (semelhante a uma parabólica), cujo custo é bem elevado
      * (-) mais caro que rádio
    * Fiber
      * Much faster
      * Até  500Mbps

* **Backbone**

  * Normally operated  by companies like AT&T and Sprint.
  * thousands of routers connected by high-bandwidth fiber optics

* **Server farms**

  * Huge server infractrusture
  
  * Ex: AWS, google, etc
  
  * Often connect directly to the backbone
  
  * Sometimes they even *become* the ISP (google sometimes does that), becoming what is called *Content Provider Network*

- -   

## Deep web

* contents are not indexed by standard web search-engines
* The content of the deep web can be located and accessed by a direct [URL](https://en.wikipedia.org/wiki/URL) or [IP address](https://en.wikipedia.org/wiki/IP_address), but may require a password or other security access to get past public-website pages
* Alternative search engines
  * DeepPeep, Intute, Deep Web Technologies, Scirus, and Ahmia.fi
* dark web
  * portion of the deep web that has been intentionally hidden and is inaccessible through standard browsers and methods
  * require specific software, configurations, or authorization to access
  * For example, Tor allows users to access websites using the *.onion* server address anonymously, hiding their IP address.
  * Tor = open-source software that implements tor network
  * Tor network
    * Or *Tor dark web* or *onionland*
    * hard to track because the routing is wierd
    * .onion: its top level domain 
    * onion routing
      * traffic anonymization technique
      * you chose 3 random nodes as your introduzing 
      * this info is spread over a distributed table
      * anyone who wants to talk with you, talk with with those introducing nodes
      * after introduction, a intermidaite point will be chosen at random to exchage messages 
      * to reach that intermediate point, each part have to pass by several hops
      * normal message have 3 hops
      * message is encripted again at every hop
  * I2P software
    * ?

## Servers

* **provêm algum serviço**
  -   File system
  -   web conectivity
  -   database
  -   mail
  -   virtualization
  -   etc
* **Storage methods**
  -   Direct-atached storage (DAS)
      -   Physicaly connected to the server
      -   (-) insecure
  -   Network attached storage (NAS)
      -   connected through switch
      -   Phisicaly connected to the network
      -   Have come kind of simple operating system
      -   (-) May be pretty slow
      -   (-) Cogestionate the network
  -   Storage area network (SAN)
      -   Connected though an firebird swich (own high speed network)
      -   Phsically conneted to the network
* **Dedicated hardware to servers**
  -   What to consider? load on the servers, space and budget constraints, storage capacity, integration, etc
  -   <u>Tower</u>
      -   Almost a tradicional PC
      -   (+) Cheap
      -   (-) Simple
      -   (-) not space eficient
      -   Each one needs an individual KVM (Keyboard, Video, and Mouse) switch in order to be managed
  -   <u>Rack</u>
      -   Small hardware mounted inside a rack
      -   (+) easy to organize, because everything is inside a single rack
      -   (-) Still a lot of cabling
  -   <u>Blade</u>
      -   most advanced type
      -   Like rack, but with every unit as independent as possible
      -   (+) very litle cabling
      -   (+) very easy to organize
      -   (-) complex setup
      -   (-) hardcore heat and ventilation
      -   (-) expensive
      -   (+) power and space eficiency
      -   Most of the blades work hot-plug
* **Types of servers**
  * <u>Printer services</u>
    -   you can rarely can connect the printer directely to the computer
    -   Alternative: connect all printers to a server, and connect the computers to the server
    -   Linux software: CUPS
    -   Cloud option: you send through the browser 
  * <u>Comunications services</u>
    * XMPP: open source protocol to instant communication
  * <u>Email services</u>
    * you can run your on server, setting up an MX DNS (mail exchange record)
    * This is complex, so is easier to pay a provider (like gmail)
    * See more in *Application Layer - Email Protocols*
  * <u>Database server</u>
    * host a database and make it available
    * Softwares: mysql, postgree, etc
  * <u>Directory services</u>: see specific section
  * <u>Web servers</u>: see specific section

## Data centers

* Building dedicated to house computer systems
* **Requirements**
  * Lightning: ?
  * Refrigeration: ?
  * …? 

## Directory services

-   easy share files, control users, etc

-   for locating, managing, administering and organizing everyday items and network resources

-   maps the names of network resources to their respective network addresses

-   Cloud simple file services: google drive, mega, etc ([comparison](https://www.cloudwards.net/comparison/))

-   Organizes users and groups

-   easy to mantein and manage accesss

-   To control…what?
    -   Centralized authentication
    -   What softwars are installed in the computers
    -   keep track of the computers
    -   who can access, and levels of access
    -   Security configurations of the computers
    
-   Replication

    -    automatically copies directory data from one Directory Server to another
    -   (+) improves availability and read performance
    -   (+) fault tolerance

-   **Samba**

    -   Software that implement SMB protocol 
    -   open source

-   **OpenLDAP**
    
    -   software that implements LDPA
    -   good for linux (compatible with windows and macOS)
    -   open source
    -   Natively by command line
    -   php LDAP admin: web graphical interface
    
-   **Active Directory**
    
    -   software that implements LDPA (see in *Application layer*)
    
    -   good for windows (compatible with macOS, but not with linux)
    
    -   ORganizational unit: groups of users/computers
    
    -   Active Directory Administrative Center (ADAC): tool to anagement
    
    -   Objects: Real-world entities such as users and  computers
    
    -   Policies
        -   what users can do, what software they access, etc.
        -   Group policies objects (GPO)
            -   user configuration: applied on log-on
            
            -   computer configuration: applied on boot
            
            -   the computer are ensured to be running these policies
            
            -   Are applied imidiatly after changed
            
            -   Fast log-on optimization: some policies are not applied in the log-on, just after
            
            -   WMI filter: apply policies based on the cofigurations fo the machine
            
            -   : local group policy, site, domain, organizational units
            
                ![Group Policy Order of Precedence FAQ | Me, Myself and IT](Images - Redes de Computadores/gp-order-2.jpg)
        -   group policies preferences
            -   “templates”
            -   someone using the computer can be changed during use
        -   inherence
            -   summing GPOs
            -   each unit can determine the *precedence* of how the policies are applied

## Web Servers

-   Or *host*
-   Store and serve content through the Internet
-   O que coloca seu site/aplicação online e permite acessá-lo a partir de outras
    máquinas
-   O servidor precisa suportar as tecnologias utilizadas (php, sql, mysql, etc)
-   Web server software
    -   Precisa suportar os protocolos da camada de aplicação usados (HTTP, geralmente)
    -   Most of them already perform reverse proxy, load balancing, etc
    -   apache
        -   htaccess: configuration file
    -   PHP has a built-in server, but is usually used with Apache
    -   Nginx: reverse proxy, load balancer, mail proxy and HTTP cache
    -   runserver: used by Django
    -   
    -   Traefik
        -   Open source
        -   (+) good for observability, tracing, etc
        -   (+) good with docker, kubernets, etc
        -   (+) makes deploying microservices
        -   static configuration: run on start
        -   dinamic configuration: you
        -   includes its own monitoring dashboard
        -   entrypoints: what you’ll be listening to 
        -   Router
            -   Analize the request
            -    is in charge of connecting incoming requests to the services that can handle them
            -   Rules: what the request should look like (host, path, etc)
            -   middleware
                -   Attached to the routers
                -   slightly modify the requests
                -   The middlewares will take effect only if the rule matches
        -   API: allow http-based monitoring
-   
-   common gateway interface (CGI)
    -   standard protocol that defines how the Web server communicates with application programs
    -   a programmer can write a CGI application in a number of different languages
    -   não importa a linguagem usada, só a passagem dos parâmetros e a resposta
    -   FastCGI: A cada requisição, o servidor web cria um novo processo
    -   https://klauslaube.com.br/2012/11/02/entendendo-o-cgi-fastcgi-e-wsgi.html
-   Microsoft's [Active Server Page](https://searchwindowsserver.techtarget.com/definition/Active-Server-Page) (ASP)
    -   alternative to the CGI protocol
-   Simple Common Gateway Interface (SCGI)
    -   alternative to the CGI protocol
    -   is a protocol for applications to interface with HTTP servers
*  **WSGI**
   * Web Server Gateway Interface
   * interface simples e universal entre servidores web e frameworks <u>python</u>
   * <u>WSGI servers</u>
     * Servidores *WSGI* conseguem servir as aplicações sem o auxílio de um *Apache* ou *Nginx*, mas uma prática comum usar um Nginx “na frente” de um WSGI server servindo estáticos, fazendo caching e “aguentando porrada”, enquanto que o WSGI server está totalmente focado em servir o conteúdo dinâmico
     * Werkzeug: used in Flask; Not for deploy
     * Gunicorn
       * 'Green Unicorn'
       * trabalha com *pre-fork* de *workers*, onde um processo “master” gerencia um conjunto de processos que são de fato os responsáveis por servir a sua aplicação
     * uWSGI
     * Tornado
   * <u>ASGI servers</u>
     * ASGI (*Asynchronous Server Gateway Interface*) is a spiritual successor to WSGI
     * for both asynchronous and synchronous apps
     * Univcorn
       * ==uses uvloop== (event loops for asynchronous IO)
     * daphne
     * hypercorn
   * Starlette
     * lightweight ASGI framework/toolkit
     * which is ideal for building high performance asyncio services
     * runs above uvicorn
  *  performance that matches Node, and in some cases, even Go!
  
*  Autoscaling: automatically create new instances when the load increases and automatically turn them down when the load  decreases
* Load Balancing: distributing the requests among many servers
* Local servers
  * É possível usar uma ferramenta para gerenciar o servidor localmente, como o Cpanel
  * Gears software
    * browser plug-in that provides a database, a local Web server, and support for parallel execution of JavaScript at the client.
    * Paid plataform <https://www.gearhost.com/>

## Data persistence

### Session

* Creates a temporary file on the server
* name/value pair
* The variables can be used in all the pages of the website
* Limpas quando o navegador é fechado
* I'll write here for PHP, but the same ideia is valid to any backend language
* lasts for as long as the browser is open and survives over page reloads and restores
* ==how can I set a cookie from the front, using JS??==
* Funcionamento
  * O php identifica a seção através de um cookie chamado “PHPSESSID”
  * This session id is passed to the web server every time the browser makes an HTTP request
  * The session id is unique, so those variables can’t be used by another user or even another browser 
  * Os valores ficam armazenados em array super global \$SESSION

### Cookies

*  piece of text (on server) with an associated name (on user’s browser)
*  Each item has a key and a value
*  **How it works**
   * O site seta um cookie (linha `set-cookie` no header http) e recebe um número de identificação
   * número de identificação é armazenado e gerenciado pelo browser
   * o mesmo site pode, posteriormente, acessar esse dado (desde que esteja em posse do número de identificação)
   * o browser só enviará a identificação para o servidor certo
   * The next time browser sends any request to that web server, will send those cookies together to the server
   * A domain (Web site) only receive the cookies that it has set

![image-20200513230359671](Images - Redes de Computadores/image-20200513230359671.png)

### Others

* Local Storage?
  * The `localStorage` API is built into your browser
  * has no expiration time (se manter gravados mesmo com o fechamento do navegador)
* Cache storage?
* IndexedDB?

## Proxy server

* acts as an intermediary for requests
  
* satisfaz pedido do cliente sem envolver o servidor de origem
  
* may reside on the user's local computer, or at any point between the user's computer and destination servers on the Internet

* Gateway (or *tunneling proxy*): passes unmodified requests and responses

* **How it works**

  * primeiro os pedidos http são enviados para o proxy, se o browser estiver configurado assim (e por default geralmente o é)

  * Se o objeto está na cache ele é imediatamente retorna uma resposta http 

  * Senão o objeto é pedido para o servidor de origem, então retorna a resposta para o cliente

    ![image-20200513231023625](Images - Redes de Computadores/image-20200513231023625.png)
    
  * <u>Transparent proxy</u>
  
    * ??
  
* **??**

  * If you are using Linux then you can also add the following lines to your `.bashrc` file allowing command line applications to use the proxy server for outgoing connections.

    ```
    export ftp_proxy=<host>:<port>
    export http_proxy=<host>:<port>
    export https_proxy=<host>:<port>
    ```

* **Types**

  * <u>Filtering proxie</u>

    * regulate access to certain websites
    * Filter what is accessed (from inside) and what is received (from outside)
    
  * <u>Bypass proxy</u>

    * Used to hide the real site you’re trying to access
    * example: to access facebook from inside school
    * Bypass proxie to burlate a filtering proxie or firewall:

    ![img](Images - Redes de Computadores/400px-CPT-Proxy.svg.png)

  * <u>Reverse proxy</u>

    * appears to clients to be an ordinary server

    *  is usually an internal-facing proxy used as a front-end to control and protect access to a server on a private network

    * commonly also do load-balancing, authentication, decryption (SSL) and caching

      ![A proxy server connecting the Internet to an internal network.](Images - Redes de Computadores/280px-Reverse_proxy_h2g2bob.svg.png)
    
  * <u>Cache proxy</u>

    * ou *Web Caches*
    * accelerates service requests by retrieving the content saved from a previous request 

  * <u>FTP proxy</u>: see specific section

* **Open proxies**

  * available on internet, fowarding everything it receives
  * proxify (web based)

* **Software - Squid**

  * Free and opensource
  * primarily used for HTTP and FTP
  * does not support SOCKS protocol

### FTP proxy

* It forwards traffic between a client and a server without looking too much if both hosts do real FTP
* All connections from the client to the server have their source address rewritten so they appear to come from the proxy 
* **Software: linux FTP-Proxy**
  * application level gateway between  FTP  clients  and  servers
  * Main purpose: to secure servers against attacks based on the FTP protocol.
  * allows LDAP
  * configuration file
    * option lines and comments
    * Placed in `/etc/proxy-suite`
    * comment: line starting with a '#'
    * option line format: `[WhiteSpace] Name WhiteSpace Value [WhiteSpace]`
    * not case sensitive
    * DestinationAddress: Both user and global context.  Defines where to redirect incoming FTP traffic

### Load balancing

* ????

## Browser cache

* browser saves several contents and data in temporary storage
* browsers typically cache what are known as "static assets"
* (+) speed things up
* See also *HTTP - Conditional GET*, that is one of the methods to explicitly avoid transmit the same resource twice

## Multimedia services

* :books: Ross chapter 9
* ==I should move this to an specific file?? WTF is all that?==
* **Autoração**
  *  criação, edição, legendagem e dublagem de mídia
  *  geralmente no contexto de filmes DVDs
  *  A saída de um programa de autoração é um arquivo *master* (finished DVD file)
* **Codificação de texto**
  *  ASCII: 7 bits. 128 code points.
  *  ISO-8859-1: 8 bits. 256 code points.
  *  UTF-8: 8-32 bits (1-4 bytes). 1,112,064 code points.
  *  Both ISO-8859-1 and UTF-8 are backwards compatible with ASCII, but UTF-8 is not backwards compatible with ISO-8859-1
* **RSS**
  * ==where to put taht???==
  * utilizado para divulgação de notı́cias na Internet, permitindo aos usuários assinarem os chamados feeds RSS para obterem as notı́cias de um determinado provedor desejado. 
  * a notı́cia vai até o usuário, no lugar de ele ir até a notı́cia.
  * arquivo RSS: contem a noticia; formato XML
  * feed: varios arquivos RSS

### Compressão

* Codec: ?
* **Tipos**
  * compressão sem perdas: eliminamos apenas a redundância de um sinal, não há perda de
    informação
  * Compressão perceptualmente sem perdas: perda não é perceptível pelo ser humano
  * Compressão normal: com perdas
* **Técnicas de melhoramento - Audio**
  * Permite maior qualidade mesmo com menor taxa de transmissão
  * SBR (Spectral Band Replication)
  * PS (Parametric Stereo)
* **Video**
  * MJPEG (Motion JPEG): aplica compressão JPEG para cada quadro; leva em conta apenas a redundância intraquadro
  * MPEG: leva em conta redundancia interquadro
    *  diferentes
       resoluções para as componentes de crominância (subamostragem de
       crominância)
    *  vários perfis e níveis
* **Softwares**
  * FFmpeg: conversões e etc's

### DRM

* ==move to…?==
* Digital Rights Management
* tecnologias para proteger mídias digitais
* Objetivo: evitar a distribuição ilegal de conteúdo protegido
* Como: proibir copias, reprodução, etc

## Content streaming

* Services like netflix, youtube, spotfy
* Se o stream for pregravado, este pode ser enviado sob demanda (o cliente pausa, adianta, etc)
* Se o stream for ao vivo, o cliente assume um papel passivo e não controla quando o stream começa ou termina, nem possui qualquer controle sobre a exibição da mídia
* **Adaptative stream**
  * to handle diffenrent bandwith
  * improve user’s experience
  * ==the only way is to== store data in different formats and resolutions (netflix stores about 1000 copies of each movie… source???)
  * trechos de geralmente uns 10 segundos?
* **how it works**
  * prerecorded videos are placed on servers, and users send
    requests to the servers to view the videos on demand
  * Once the number of bytes in the client’s buffer exceeds a predetermined threshold, the client application begins playback
* **Others**
  * C++ stream server basic example (using winsock): https://www.medialan.de/usecase0001.html	
  * JS RTSP client: https://github.com/Streamedian/html5_rtsp_player(they have an interesting running demo)

## Multi/broadcast stuff

* ==MOVE!==
* 

### Application layer protocols

* **RTP**
  * Real-time Transport Protocol
* **RTCP**
  * Real-time Control Protocol
  * to monitor transmission metrics and quality of service (QoS) while aiding in the synchronization of multiple streams
* **RTSP**
  *  Real Time Streaming Protocol
  *  Statefull (in contrast to, for instance, http)
  *  clients can emmit commands like play, record and pause
  *  Most RTSP servers use the RTP in conjunction with RTCP
  *  não é responsável pela transmissão do conteúdo
     propriamente dito: quem faz isso é o RTP
  *  It is stateful: each connected client receives an ID
  *  ==can be adaptive??==
  *  pode ser enviado over TCP or UDP… ???
  *  can be multicast?
  *  <u>packet types/methods</u>
     * Options
     * Describes
     * Setup
     * Play
     * Pause
     * Record
     * Anounce
     * Teardown
     * Get parameter
     * Set parameter
     * Redirect
* **Stream over HTTP**
  * splitting the source files into multiple segments which are then delivered over the `HTTP` protocol
  * (+) menos chance de ser bloqueaod por firewalls
  * (+) integra-se facilmente com proxies, firewalls e NATs, além de aproveitar caches
  *  não é um streaming de
    dados propriamente dito. Trata-se de uma série de downloads de arquivos que são
    reproduzidos em um momento específico conforme uma determinada sequência
  * Para as CDNs é mais facil fazer cache de HTTP do que de RTP
  * <u>Dynamic Adaptive Streaming over HTTP (DASH)</u>
    * the video is encoded into several different versions, with each version having a
      different bit rate and, correspondingly, a different quality level. The client dynamically requests chunks of video segments of a few seconds in length. When the amount of available bandwidth is high, the client naturally selects chunks from a high-rate version; and when the available bandwidth is low, it naturally selects from a low-rate version. The client selects different chunks one at a time with HTTP GET request messages 
    * The information about the content is found in a manifest file called the Media Presentation Description (MPD), basically a XML file.
    * 
* **HLS**
  * ?

### Gstreamer software

* multi-platform media streaming framework
* pipeline management
* open-source
* roda por baixo da maioria dos softwares (provavelmente até do FFmpeg)
* For C language, with biding for python, C++, and othres
* Have a lot of libraries built upon it
* **usage**
  * Initialize GStreamer: `gst_init (&argc, &argv);`
  * Every GStreamer element has an associated state, which you can more or less think of as the Play/Pause button in your regular DVD player.
  * Pipeline
    * Set of sequencial interconnected elements
    * Media travels from the “source” elements (the producers), down to the “sink” elements (the consumers), passing through a series of intermediate elements performing all kinds of tasks
    * Example: read mp4, processes it, export avi
  * pipeline elements

    * `playbin`:  plays binary; both source and sink

    * `videotestsrc`

    * `autovideosink`
  * `gst_element_factory_make(element, type)`: add element
  * `gst_parse_launch`: takes a textual representation of a pipeline and builds the pipeline
  * `gst_bin_add_many`
  * 
  * `gst_element_set_state (pipeline, GST_STATE_PLAYING);`: execute pipeline
  * `gst_element_set_state (pipeline, GST_STATE_NULL)`: free pipeline resources
* **Others**
  * [Example gstreamer openCV pipeline](https://www.youtube.com/watch?v=fWhPV_IqDy0)

## **Content distribution networks (CDN)**

* replica o conteúdo de
  seus clientes nos servidores

* Quem provê conteúdo (“dono do site”) paga para a CDN

* point of presence: ?

  ![image-20200513231713020](Images - Redes de Computadores/image-20200513231713020.png)

* Exemplo: empresa Akamai. o provedor hospeda parte do seu conteúdo na akamai (uma image, nesse exemplo) e o cliente tem que ir buscar na akamai. 

  ![image-20200513232529125](Images - Redes de Computadores/image-20200513232529125.png)
  
* Main companies: Akamai, Limelight, and Level 3

* <u>Open conect</u>

  * Netflix’s custom CDN
  * Released in 2012 (they used common CDNs before)
  * guardar conteúdo no ISP
  * dont handle the networking itself (like a traditional CDN), that is done by the ISP
  * Faz com que o request não precisa ir para o sevidor principal (ex: aws)
  * (+) there is much less cache miss, because they can predict usage better

## Other web things

* 
* ==NTP==
  * Protocol to sincronize time
  * You can setup your time or connect to an open server
  * The server sincronize by satelite
* intranet

  * computers network only accessible from inside a company, institution , etc
* private network

  * ==same as intranet?==
  * deploy a stand-alone physical network—including routers, links, and a
    DNS ­infrastructure—that is completely separate from the public Internet
* See all devices connected to your network: https://www.howtogeek.com/423709/how-to-see-all-devices-on-your-network-with-nmap-on-linux/
* **Como redirecionar o cliente para um servidor particular?**
  * Quando você faz uma requisição, quem decide o melhor servidor? como distribuir carga?
  * Como parte do roteamento
    * anycast
  * Como parte da aplicação
    * HTTP redireciona
    * O servidor recebe a requisição e, se for o caso, retorna um HTTP rediret para o melhor servidor
  * Como parte da resolução do nome
    * DNS round robin (RFC 1794)
    * Métricas semi estáticas de seleção



## IP cameras

* https://www.ipcamlive.com/
  * cloud based
  * can receive RTSP/H264 video stream from an IP Camera and can broadcast it to the viewers.
  * you can embed their player on a HTML page using an iframe









