# Conceituação

## Orgaos reguladores

* **Anatel**
  * Agencia Nacional de Telecomunicações
  * no brasil
  * limite de transmissão residencial: 1W
  * acima de 5W, além de não ser permitido, você já vai influciar TV e radios em um raio de vários quilometros (lembresse da histório do Golberi, rsrs)
* **ITU**
  * international-telecommunication-union
  * https://www.geneve-int.ch/international-telecommunication-union-itu-0
* **ETSI**
  * europeu
  * especialmente atuante na parte de GSM?
* **FCC**
  * Manages the radio spectrum in US
* **CE**
  * Conformité Européene

## Propagação de ondas

* frente de onda: regiões no espaço com a mesma fase? Ou especificamente as regiões de crista?

* sobre ondas eletromagnéticas: acima de 30GB é ionizante

* **Densidade de potência**

  * inversamente proporcional à distância 

  * Para uma antena isotrópica:
    $$
    P=\frac{P_t}{4\pi r^2}
    $$

    > $P_t$ = potência transmitida
    >
    > $P$ = densidade de potencia

* **Caminhos de propagação**

  * <?: contorna grandes obstáculos como montanhas
  
  * **<3MHz**
  
    * reflexão no solo
    * exclusivamente polarização vertical
    * (-) necessita potência elevada
    * alcanse inversamente proporcional à frequência
    * VLF, LF, MF
  
  * **3~30MHz**
  
    * Refração na ionosfera;
  
    * HF
  
    * alcanse diretamente proporcional à frequência
  
    * Maximum usable frequency (MUF) = $\frac{35MHz}{cos \theta}$ 
  
    * geralemnte se utiliza entre 60% e 85% da MUF
  
    * ionosfera: 50~250Km
  
    * existe algum tipo de transmissão que reflete na troposfera?
  
      ![image-20201211091701160](Images - Antenas e telecomunicações/image-20201211091701160.png)
  
  * **\>30MHz**
  
    * só visada direta
    * VHF, UHF, SHF e EHF
    * 30~100MHz: um pouco de difração
    * alcance dependente da altura da terra
    * Distância máxima entre as antenas (considerando a curvatura da terra): $S = 3.569*(\sqrt{h_1} + \sqrt{h_2})$

## Radioenlaces

* Remembering some fundamentals:

  ![image-20210423144513556](Images - Antenas e telecomunicações/image-20210423144513556.png)	

  ![image-20210423144525452](Images - Antenas e telecomunicações/image-20210423144525452.png)

* formula de friis (relaiona potencia de recepção e emissão)

  ![image-20210423113201055](Images - Antenas e telecomunicações/image-20210423113201055.png)

  ![image-20210423153904843](Images - Antenas e telecomunicações/image-20210423153904843.png)

  > PT = potência transmitida
  > PR = potência recebida
  > GT = ganho da antena transmissora
  > GR = ganho da antena receptora

* A parcela referente à distância pode incluir chuva, difração, desvanecimentos, etc

* Se utilizarmos a formula de Friis em dB, as parcelas se somarão

## Problemas na transmissão

* Ruido aleatório (no tempo e na frequencia)

*  Ruído impulsivo

* Multiplos percursos

  * autointerferencia
  * Em TV analógica, víamos aqueles “fantasmas”

  ![image-20200907164339133](Images - Antenas e telecomunicações/image-20200907164339133.png)

* **Fading**

  * variações aleatórias ao longo do dia
  *  atenuações, reforços e distorções
    no espectro do sinal
  
*  **Não estacionaridade**

  * Coisas de movendo
  * Se for o ambiente, será algo similar ao fading
  * Se for o receptor ou emisor, acontecerá o efeito doppler
  
*  **Other channel distiortions**

  * Linear Distortion 
  * Distortion Caused by Channel Nonlinearities
  * Distortion Caused by Multipath Effects 
  * Decorrente do uso compartilhado de canais

# Antenas

* Um meio para irradiar ou receber ondas de rádio

* a ideia básica é igual ao capacitor, mas com o campo não estando confinado e sim sendo dispersado para o ar

* as pontas da antena não possuem contato elétrico

* transmite e recebe igualmente

* o ar possui ~377 ohm (esse ohm é unidade de… impedância magnética? relação entre o modulo do campo magnpetico e campo elétrico), portanto as antenas possuem a mesma impância para máximo casamento; antenas possuem por padrão 300ohm

* Circuitos equivalentes (sem perdas):

  ![image-20210212094915178](Images - Antenas e telecomunicações/image-20210212094915178.png)

* **Formas de propagação**

  * Omnidirecional: propaga igualmete em total as direções do plano (duas direções)
  * Isotrópica: propaga igualmente em todas as direções do espaço (3 direções)
  * Diretiva: focada para uma direção


* **Campo magnético da antena**

    * Os campos radiados têm comportamentos bastante distintos nas diferentes regiões (próxima ou distante da antena).
    * Geralmente aproximamos por equações diferentes para near-field ($r>\frac{2D^2}{\lambda}$, onde D é a maior dimensão da antena) e far-field; far field geralmente considera os dois campos em fase (se elementos reativos);
    * geralmente consideramos far-field após d>2D^2/lambda 

## Polarização

* Para ver sobre o efeito de polarização, ver em *Eletromagnetismo*

* **Tipos de polarização**

  * linear: o vetor campo elétrico aponta sempre na mesma direção
  * circular: o vetor campo elétrico gira numa circunferência (para a esquerda ou para a direita); (+) maior penetração em edifícios e áreas de recepção difícil; less vulnerable to multipath fading and have less polarization dependency than linearly polarized waves
  * é possível enviar informações separadas em polarizações diferentes, ao mesmo tempo, bem como é possível separar na recepção

* **Polarization Loss Factor (PLF)**

  * Perda por causa da polarização da antena receptora ser diferente da polarização da onda
  * Casamento perfeito = PLF = 1
  * ![image-20210212182343854](Images - Antenas e telecomunicações/image-20210212182343854.png)
  * Geralmente dado em dB: $PLF ( dB) = 10 log PLF$
  * Se a polarização for circular e o receptor for linear, perde-se 3dB

* **Polarização cruzada**

  * não desejável

  * polarização ortogonal à direção principal, que é excitada de forma indesejável devido às deformidades construtivas da antena.

  * Nível de polarização cruzada dado por:

    ![image-20210212183001242](Images - Antenas e telecomunicações/image-20210212183001242.png)

* Co poarização???

## Parâmetros

### Básicos

* **Ganho**
  
  * concentração da densidade de potência em relação a outras antenas
  * dBv: em relação à antena dipolo (omnidirecional)
  * dBi: em relação à antena isotropico (o ganho será maior pois a isotropica dissipa em uma área maior)
  * Se desconsiderarmos as perdas, ganho=diretividade
  
* **Resistência de radiação**

  * Resistência tal que dissipe a mesma potência da antena ($P_T$)

  * quanto maior, mais potência será irradiada para a mesma corrente (o que é desejado)

  * Quanto mais proximo de uma impedância real, melhor 

  * antena ressonante: impendacia puramente resistiva, o que é ótimo

  * A dependencia da frequência é geralmente:

    ![image-20210212172106009](Images - Antenas e telecomunicações/image-20210212172106009.png)

    ![image-20210212172355866](Images - Antenas e telecomunicações/image-20210212172355866.png)

* **Rendimento ($\eta$)**
  
  * rendimento geralmente é muito alto, na ordem de 99%
  * perdas: efeitos de borda, absorção do material, 
  * $\eta=\frac{P_r}{P_{input}}$
  
* **Diagrama de radiação**
  
  * Representação gráfica que mostra as propriedades de radiação de
    uma antena em função de coordenadas espaciais
    
  * potência radiada em função dos ângulos θ e φ
  
  * geralmente normalizados em relação ao seu valor
    máximo
    
  * Exemplo para uma dipolo:
  
    ![image-20210226235630588](Images - Antenas e telecomunicações/image-20210226235630588.png)
  
* **Largura de banda**

  * Faixa de freqüências dentro da qual uma antena opera corretamente
  * Geralmente dado em percentual (ex: 50%), porporção (ex: 1:2) ou absoluta (ex: 10MHz)
  * antena de banda estreita: frequência máxima/mínima<2
  * banda larga: frequência máxima/mínima>2

*  **Intensidade de radiação (U)**

  *  Potência radiada por unidade de ângulo sólido
  * [watts/esferorradiano (W/sr)]
  * Para antena isotrópica: $U=\frac{P}{4\pi}$

### Exclusivos de antenas diretivas

* half power bean width (HPBW); hozontal e vertical; em graus
* SLL, de “Side Lobe Level”
* Beam Width
* between First Nulls
* **Ganho diretivo**
  * Ganho em um certo angulo
  * ![image-20210212173449960](Images - Antenas e telecomunicações/image-20210212173449960.png)
* **Diretividade**
  * ganho diretivo para phi=0 (para frente)
* **abertuda efetiva ($A_e$)**
  * [m²]
  * area que, caso a antena não tivesse perdas, receberia a mesma potência que ela está recebendo
  * $\epsilon_{ab}=A_e/A_{fisica}$
  * Tipicamente, cornetas têm eficiência de abertura entre 30% a 90%
    e antenas refletoras entre 50% a 80%.





## Tipos

* **Antena isotrópica ideal**: pontual; teórica; isotrópica perfeita

* **filamentar**
  * geralmente desconsideramos a grossura
  * campo primário gerado é elétrico
  * what else?
  
* **Antena de quadro**

  * Ou *dipolo magnético*, ou *antena de loop*
  * o campo primário gerado é magnético
  * muitas espiras enroladas num núcleo de ferrite
  * deve ficar de lado, com a irradiação máxima para a lateral?

* **microstrip antennas**

  * impressas, geralmente em PCB
  * um elemento ativo monolítico e um plano terra
  * (-) baixa efficiencia
  * (+) facilidade construtiva
  * não pode ser omnidirecional, no mínimo um dos hemisférios será eliminado pelo plano terr
  * had three definite parts as patch resonator, substrate, and ground (GND) plane
  * é muito comum usar uma antena de quadro  impressa em uma tag flexível (RFID, NFC, etc)
  * impedância pode ser alterada pela trilha de feed
  * quanto mais groça a PCB melhor?

* **Adicição de refletores**

  * o elemento ativo geralmente é um dipolo

  * refletor simples: só uma haste, como na Yagi

  * refletor de canto: nas laterais de uma omnidirecional, para tornar-la mais diretiva

    ![image-20210319091405529](Images - Antenas e telecomunicações/image-20210319091405529.png)

  * <u>refletor parabolico</u>

    * centraliza tudo o que é recebido em um unico ponto

    * alta diretividade 

    * tipo front feed: foco no centro ;(-) se for usada para transmissão, pode causar umaonda estacionária; (-) parte da recepção é bloqueada

    * tipo offset: o foco é deslocado de forma que não fique no meio da antena

      ![image-20210319100819331](Images - Antenas e telecomunicações/image-20210319100819331.png)

    * tipo refletor duplo: o elemento ativo fica dentro

      ![image-20210319100947652](Images - Antenas e telecomunicações/image-20210319100947652.png)

* **Radome**

  * “aba” na lateral
  * para diminuir os efeitos 
  * dentro possui uma espuma que absorve sinais que não vêm da frente?
  * sometimes is just a protection; Outra opção é construir o radome com material tal que NÃO interfira com as ondas eletromagnéticas, utilizando-o apenas para proteger a antena do ambiente. Essa proteção é especialmente importante quando a antena opera em condições adversas, como em aplicações aeronáuticas ou marítimas.

* **widening BW techniques**

  * add more elements of different sizes 
  * use fractal geometries
  * Vivaldi antenna: ?

* **Other annotations**

  * traveling wave type?
  * end-fire radiation pattern?

* **Sobre distribuição de corretne**

  * onsideraremos uma filamentar

  * zero na ponta extrema

  * onda estacionária

  * para uma antena dipolo:
  
  * ![image-20210219091451248](Images - Antenas e telecomunicações/image-20210219091451248.png)
    $$
    \beta=\frac{2\pi}{\lambda}
    $$

### Basic omnidirecionals

* Quanto maior a antena, menor a frequencia

* **dipolo**

  * ?

  * Tipo filamentar

  * potência máxima quando o receptor está alinhado com o equador da antena

    ![image-20201218092649512](Images - Antenas e telecomunicações/image-20201218092649512.png)

    ![image-20210219090627354](Images - Antenas e telecomunicações/image-20210219090627354.png)

  * <u>dipolo infinitesimal</u>

    * comprimento de ponta a ponta é menor do 1 que 1/50 do comprimento de onda
    * consideramos distribuição de corrente uniforme ao longo da haste
    * (-) resistencia muito baixa, o que é gera pouca irradiação
    * resistencia de irradiação: $80\pi^2(L/\lambda)^2$
    * diretividade: 1.5
    * diretividade dbi: 1.76
    * HPBW: 90º

  * <u>short dipole</u>: $L=\frac{\lambda}{10}$

  * <u>meia onda</u>

    * $L=\frac{\lambda}{2}$
    * diretividade: 1.64
    * diretividade dbi: 2.15
    * HPBW: 78º
    * resistencia de irradiação: 73

  * <u>onda completa</u>: $L={\lambda}$

  * se o dipolo não for multplo de 1/2 1/4 etc, não funciona tao bem: frequencias multiplas e harmonicas são maximamente emitidas/recebidas

* **monopolo**
  * pode ser considerado um caso especial da dipolo
  
  * parecido com dipolo, mas o outro plano é o terra
  
  * Tipo filamentar
  
  * usa o chão como elemento refletor
  
  * para essas anteninhas monopolos pequenas o plano terra será a PCI, a carcaça do equipamento, a basezinha da antena, etc
  
    ![image-20210219092824974](Images - Antenas e telecomunicações/image-20210219092824974.png)
  
* **Dipolo em geometrias variádas**

  * <u>dipolo dobrado</u>
    * Variação do dipolo meia onda
    * condutores paralelos ligados na extremidade
    * Impedancia: 300 ohms
    * Perfil de radiação igual do dipolo
    * (+) maior largura de banda
    * (+) resistência pode ser alterada variando o tamanho e espaçamento dos condutores

  ![image-20210227153817324](Images - Antenas e telecomunicações/image-20210227153817324.png)

  * <u>Turnstile</u>
  * Dipolos cruzados
  * <u>Dipolo em V</u>
    * maior direcionalidade, maior ganho, porém maior dificuldade de captar canais paralelos?
  * <u>Dipolo em V com dois elementos</u>

    * Ou *seta/anel*?
    * posicioados ao contrário
    * polarização circular
  
* **Antena de quadro**

  * Or *loop*

  * vários formatos: tringulo, quadrado, etc

  * ==juntar isso com as anotações acima==

  * o campo exatamente normal ao quadro é sempre nulo

  * A melhor radiação é “de lado”

  * Quadro pequeno: modelo simplificado, análogo ao dipolo infinitesimal; a diretividade do quadro pequeno é
    idêntica à do dipolo infinitesimal (D = 1,5 ou 1,76 dBi).

  * tanto a resistência de radiação quanto a diretividade crescem com o
    aumento do tamanho do quadro

    ![image-20210331121246009](Images - Antenas e telecomunicações/image-20210331121246009.png)

    ![image-20210331121528865](Images - Antenas e telecomunicações/image-20210331121528865.png)

* **bicônicas**

  * (+) Banda larga
  * Muito similar ao dipolo
  * Variação discone: um disco e um cone

###  Basic diretives

* Muito usada em enlaces ponto a ponto, satelites, etc

* possui l[obulos laterais de irradiação, sempre indesejados

* Setorizada: mais ampla que a diretiva normal; boa quanto queremos irradiar, por exemplo, para uma sala inteira

* direcionais captam melhor o sinal do que as bidirecionais

* é comum adicionar um refletor atras da antena, cuja distancia para o elemento ativo deve ser calculada tal que a onda refletida esteja em fase com a onda principal

* E sobre adicionar uma bobina?

* **Arranjo Parasita**
  
  * Um elemento ativo, vários auxiliares
  
  * Auxliares melhoram a diretividade
  
  * um refletor pode ser adicionado atras 
  
  * qto maior o numero de diretores, maior o ganho e
    mais estreito o ângulo de abertura.
    
  * Mais diretores -> mais diretividade, mas vai aumentando cada vez menos
    
    ![image-20210227154838331](Images - Antenas e telecomunicações/image-20210227154838331.png)
    
  * <u>Variação Iagi Uda</u>
    * apenas um elemento ativo, um dipolo meia onda dobrado
    
    * end-fire array
    
    * faixa de frequencia estreita
    
    * ganhos de 3 a 15 dB são possíveis com
      abertura de 20 a 40o
      
    * projetadas para maximizar a relação F/B em vez do ganho
      * Espaçamento típico entre refletor e o elemento excitado: 0,15$\lambda$ a 0,25$\lambda$
      
      * Espaçamento típico entre diretores: 0,2$\lambda$ a 0,35$\lambda$
      
        ![image-20210305093207954](Images - Antenas e telecomunicações/image-20210305093207954.png)
  
* * 

* **Helicoidal**

  * hélice condutora e um disco refletor

  * polarização circular

  * dependendo do formato da hélice, pode atuar em dois modos

    ![image-20210331120509284](Images - Antenas e telecomunicações/image-20210331120509284.png)
    
    ![image-20210305094722988](Images - Antenas e telecomunicações/image-20210305094722988.png)
    
  * <u>Normal mode</u>

    * omnidirecional
    * Liner polarization
    * when the circumference of the helix is significantly less than the wavelength and its pitch is significantly less than a quarter wavelength 
    * fininha
    
  * <u>Axial mode</u>

    * diretiva; 
    * end-fire
    * circularly polarized waves
    * when the helix circumference is near the wavelength
    * gordinha
    
    

* **corneta**
  
  * ?
  
  * Corneta com refletor parabólico: é a tradicional “antena parabólica”
  
  * (+) banda larga
  
    ![image-20210312094557368](Images - Antenas e telecomunicações/image-20210312094557368.png)
  
* **log periódica**

  * (+) Banda larga
  * vários elementos ativos, igualmente espaçados
  * uma em frente à outra
  * sem refletor
  * Astes com dimensões diferentes para serem maximamente excitadas em frequências diferentes, em frequências logaritmicamente espaçadas 
  * A alimentação da haste é “cruzada” para minimizar a reatância
  * mais ampla – menos direcional
  
* **espirais**

  * (+) Banda larga

  * relativamente diretiva em ambas as direções, para frente e para trás

  * Polarização circular

  * alimentada pelo centro

  * diferença instintiva entre o quadro pequeno e a espiral (a geometria parecida é parecida mas o diagrama radiação é completamente diferentes): padrao espacial de anulamento de lóbulos, por causa da parte interna da espiral

  * Padrão aarquimediano:

    ![image-20210312095208066](Images - Antenas e telecomunicações/image-20210312095208066.png)

* **Parabólica**

  * Polarização: linear
  * Faixa: UHF / MO

### Arrays

* several antenas

* grupo de elementos radiantes
  dispostos de maneira a produzir características particulares de radiação

* Phased array: constructive interference in desired direction, destructive in undesired

* [slides do golberi](https://prezi.com/cnzolz7gdiiv/arranjo-de-antenas/)

* **arranjo horizontal**

  * faces: quantas antenas

  * disposição mecanica: ajuste na posição delas em relação ao plano que as contem, “afundando” ou “sobressando” mais elas; faz um ligueiro efeito de phases array

  * geralmente dispomos em uma torre com seção quadrada, triangular ou hexagonal

  * permite ajustar o ganho de azimute

    ![image-20210326094252476](Images - Antenas e telecomunicações/image-20210326094252476.png)

* **Arranjo vertical**

  * Ou *Colinear*

  * uma em cima da outra

  * niveis: quantasantenas

  * vários elementos ativos

  * uma do lado da outra

  * pode ser feito “dobrando” a mesma antena várias vezes (mas não é tão bom)

  * permite manipular o ganho de elevação

  * a difetividade vertical fica maior (vira uma rosquinha fina)

  * (+) emite mais com a mesma potencia (pois o campo é proporcional a raiz da potencia)

  * 

    ![image-20210326094307751](Images - Antenas e telecomunicações/image-20210326094307751.png)
    
    ![image-20210227155654963](Images - Antenas e telecomunicações/image-20210227155654963.png)

* **fractais**

  * Para ter um resultado Broadband
  * as várias dimensões são maximamente exidatas por frequencias diferentes
  * comportamente em frequencia bem não linear (pega bem em uma frequencia, não pega na proxima, bem na proxima, ruim da proxima…)
  * Padrões fractais podem ser aplicadas a várias antenas, tanto omnidirecioais quanto diretivas
  * Curiosidade: os celulares da Nokia usavam uma antena fractal
  
* **arranjo endfire**

  * similar oa broadfire
  * um elemento dipolo na frente do outro
  * o sinal nos dipolos estão defasados 90º cada um
  * altamente diretivo
  * podemos variar a defasagem para obter respostas diferentes

### Smart antennas

* antenna arrays with digital signal processing to control them
* adjusted on the fly: moving, weather changing, environment chaging, etc
* minimin interference 
* Adaptive Beamforming: ?
* **Radiation redirectional**
  * direction can be manipulated with liquid cristals 

## Casamento de impedâncias

* Linha e antena deve estar casadas

* Se a impedância não estiver bem casada a transferência de potência da tinha de transmissão para a antena (ou vice versa) não será ótima

* Isso acontece pois parte da energia transmitida é refletida e origina uma onda estacionária

* Esse efeito pode ser medido pelo Standing Wave Ratio (SWR)

* carta de smith: ?
  
* **como exatamente se dá o retorno da onda**
  
  * Se a linha estiver desbalaneadas (impedancias diferentes nas linhas?), parte da corrente que flui pela blindagem (condutor externo) retorna para a Terra através da superfı́cie externa da mesma. 
  * Estas correntes estão separadas fisicamente através do efeito pelicular
  * coaxial cables are naturally unbalanced
  
* **Modificar a geometria da antena**

  * dipolo: modificar o comprimento

  * dipolo dobrado: modificar o espaçamento

  * Arranjo T: variação de um dipolo dobrado, mas sem encostar?

  * Arranjo Gamma: outra variação; fácil de implementar

    ![image-20210409232341514](Images - Antenas e telecomunicações/image-20210409232341514.png)

  * Arranjo Omega:  

    ![image-20210409232406710](Images - Antenas e telecomunicações/image-20210409232406710.png)

* **Modificar a geometria da linha**

  * toco de linha; no começo ou no final desta; 

    ![image-20210409233319250](Images - Antenas e telecomunicações/image-20210409233319250.png)

* **transformador de impendancia**

  * Transformador normal: casa as impedancias

* **balum**

  * Tipo especial de transformador
  
  * casa as impendacias e balanceia a linha
  
  * place high impedance between the antenna and the cable’s outer shield
  
  * [good link](https://www.onetransistor.eu/2018/04/transmission-line-baluns-for-vhf-and-uhf.html)
  
  * [Another good explanation](https://palomar-engineers.com/tech-support/tech-topics/why-a-balun-by-kurt-n-sterba)
  
  * Tipo bazuca: ?
  
  * Tipo trombone: 4:1
  
    ![4:1 balun with half-wavelength cable](Images - Antenas e telecomunicações/4_to_1_balun.png)
  
  *  Baluns com nucleo: ?; exemplo:
  
  ![Fabricando transformadores de impedância | Regional DX](Images - Antenas e telecomunicações/balun.jpg)

## Simulation softwares

* COMSOL Multiphysics



# Equipamentos para RF

* filtros?
* amplificadores?

# Telecom

* Formatação e transmissão de sinais em banda base;

* Transmissão digital em banda passante

* Equalização;
  * Antialiasing Filter vs. Matched Filter?
  * Maximum Likelihood Sequence Estimation (MLSE)?
  
* Sincronismo.

* Visão clássica sobre comunicação:

  ![image-20210518082027813](Images - Antenas e telecomunicações/image-20210518082027813.png)

* Visões mais detalhadas, específicas para transmissão digital:

  ![image-20210612002650679](Images - Antenas e telecomunicações/image-20210612002650679.png)

* mais frequencia -> maior perda

* mais frequencia -> maior capacidade de tansmitir informação, pois podemos aumentar a largura de banda?

* qual é a relação entre taxa de transmissão de informação e a potencia? Por que é necessário uma potencia maior para transmitir mais informação?

* sequnecia de pulsos = banda base

* lembrese que o sinal vai ser transladado na frequencia ao ser multiplicado pela portadora

* **Teorema de shannon**

  * máximo teorico

  * SNR considerença um ruído branco
    $$
    C=B log_2(1+SNR)
    $$

    > C dado em bps
    >
    > B=abndwith
    >
    > SNR = signal to noise ratio

* 

* 

* 

* 

* a digital message constructed with M symbols is called M-ary message

* the bandwidth of a channel is the range of frequencies that can be transmitted with fidelity

* if a channel of bandwidh B can transmit N symbols per second, with the same technology a channel with BK is required to transmitr KN

* with more power, you can use an smaller bandwidth
  $$
  C = B log_2(1+SNR)
  $$

  > C = max bits per second
  >
  > B = bandwidth

## Modulação

- Processo pelo qual alguma característica de uma onda portadora varia de acordo
  com o sinal modulante.
  
- Onda portadora: sinal utilizado para transportar a informação; Representaremos pelo indice $0$

- Sinal modulante: Informação em banda base, aquilo que queremos transmitir; Representaremos pelo indice $m$

- Sinal modulado: Informação em banda passante, aquilo que vamos transmitir; transladado em frequência

- 

- baseband transmission: communications that do not use modulation

- carrier transmission (or passband transmission?): communication that use modulation

- linear modulation: satisfy the condition of linearity (sum of signals sums in the output, same about multiplication)

- **motivação**
  
  - frequencia maior -> antena menor
  - possibilidade de controlar a frequência e banda -> facilidade de trabalhar com canais em paralelo
  
- **Métricas?**
  - <u>largura de banda</u>
    - ?
    - Como geralmente o sinal é ilimitado (não conseguimos um filtro passa faixa ideal), consideramos um certo threshold para a largura de banda
    - LArgura de banda em 3dB: onde metade da potência está confinada
    - Largura de banda ocupada: onde 99% da energia está c
  - <u>Eficiência de potência</u>: preservar a qualidade com baixa potencia
  - <u>Eficiência de largura de banda</u>
    -   Taxa de dados / largura de banda
    -   bps/HZ
    -   Channel Capacity and Data Rate: ?
  - n: ?; parte da potencia que é usada para enviar a informação?
  
- **upconverter**

  - or *frequency mixer*

  - faz a transladação em frequencia

  - usa AM ou FM?

  - não adiciona informação, apenas “modula” para transladar na frequência

    ![image-20210606124424583](Images - Antenas e telecomunicações/image-20210606124424583.png)

- **other anontaitons**
  -   LOCAL CARRIER SYNCHRONIZATION?
  -   Regenerative Repeater?
  -   eye diagram?

### **Portadora analógica**

- Geralmente (==sempre?==) senoidal

#### Informação analógica

-   **Amplitude Modulation (AM)**

    -   (+) Simples

    -   (-) Suceptivel à ruído

    -   linear

    -   $k_a$ é um parâmetro de sensibilidade, dado em $V^{-1}$

    -   Podemos variar a forma de tansmitir… ?

    -   Aplicações: radio, ?

        ![image-20210521182303354](Images - Antenas e telecomunicações/image-20210521182303354.png)
        
    -   há várias formas diferentes de fazer a transmissão

    -   para cada modo, o demodulador deve ser equivalente

    -   <u>Double sided band (DSB)</u>

        -   É a forma tradicional

        -   Simplesmente multplica a modulante pela portadora? Ou há um fator constante para evitar a inversão de fase?

        -   Considerando uma mensaguem senoidal, a frequência do sinal de transmissão terá compontes em $w_0$ (banda central), $w_0-w_m$ (banda lateral inferior) e $w_0+w_m$ (banda lateral superior)

        -   2/3 da energia são para transmitir a portadora

        -   $n = m^2/{2+m}$

        -   (+) simples, 

        -   (-) o dobro da largura de banda em relação à transmissão em banda base?

        -   (-) baixa eficiencia espectral e eficiencia de potencia

        -   não é linear (por causa da portadora)

            ![image-20210522114850837](Images - Antenas e telecomunicações/image-20210522114850837.png)

    -   <u>DSB with suppressed carrier (DSB-SC)</u>

        -   transmissão apenas das bandas laterais
        -   bota um filtro rejeita faixa na frequencia da carrier
        -   formalmente: só multiplica, quando for zero dá zero, mas pr causa disso dá inversão de fase?
        -   (+) Maior eficiência na potencia de transmissão.
        -   é linear

    -   <u>single sideband (SSB)</u>

        -   Primeiro gera um DSB-SC, e depois filtra passa-faixa uma das bandas laterais (superior ou inferior)
        -   Transmissão de apenas uma banda lateral 
        -   (+) Menor largura de banda
        -   (+) Maior eficiência espectral.
        -   (+) dá pra aplicar uma demodulação mais eficiente (chamada *demodulação coerente*, onde tanto a frequência quanto a fase da onda portadora devem ser conhecidos no receptor)
        -   (+) Mesma largura de banda em relação à transmissão em banda base?
        -   (-) Aumento da complexidade do circuito
        -   (-) dependendo da mensagem (grande largura de banda), não dá pra usar
        -   é linear

    -   <u>vestigial sideband (VSB)</u>

        -   Banda lateral + traço/vestígio da outra banda.
        -   Largura de banda maior do que no SSB.
        -   similar ao SSB
        -   é o mais usado na prática
        -   (+) funciona para qualquer mensagem
        -   é linear

    -   <u>métricas - indice de modulaão (m)</u>

        - relação das amplitudes
        - geralmente entre 0.7 e 0.8 
        - quanto menor for m, mais energia precismos usar apenas para transmitir a portadora (o que é indesejado)
        - $m=\frac{E_m}{E_0}$

        - 

-   **Phase Modulation (PM)**

    -   ?

    -   (-) muitas bandas lateras

    -   non linear

        ![image-20210525101322256](Images - Antenas e telecomunicações/image-20210525101322256.png)

        > KF Converte de volts para rads/s

-   **Frequency Modulation (FM)**

    -   (-) muitas bandas laterais
    
    -   non linear
    
    -   the bandwidth of a FM modulated signal is thepretically infinite, we have to crop
    
    -   desvio de frequência: representando a diferença máxima da frequência instantânea da onda FM da frequência fc da portadora.
        ![image-20210525102404779](Images - Antenas e telecomunicações/image-20210525102404779.png)
    
    -   $Bandwidth = 2(\Delta f + f_m)$
    
    -   $\text{potencia do sinal} = A^2/2$
    
    -   <u>índice de modulação ($\beta$)</u>
    
        -   b = máximo desvio de frequencia a partir da portadora / frequencia maxima do sinal modulante
    
        -   unidade: rad
    
        -   quanto maior, mais bandas laterais
    
        -   quanto maior, melhor é minha sensibilidade/resolução
    
        -   Indica a distribuição de potência entre as bandas laterais e a portadora
    
        -   FM de faixa estreita: b<2; 
    
        -   FM de faixa larga: b>2; 
    
            ![image-20210525102649784](Images - Antenas e telecomunicações/image-20210525102649784.png)
        
            ![image-20210525104957912](Images - Antenas e telecomunicações/image-20210525104957912.png)


#### Informação digital

- <u>Ampltude (ASK)</u>

  - Para cada nível do sinal digital modulado temos uma amplitude de transmissão

  - Modulação OOK -> On-off keying (mais imune a ruido, mas com maior largura de banda); like info comming from a telegram

    ![005](Images - Redes de Computadores/image-20200220175859005.png)

  - Modulação B-ASK ->Binary

  - Modulação M-ASK -> Multilevel

- <u>Frequency (FSK)</u>

  -    Para cada nível do sinal digital modulado temos uma frequência de transmissão
  -    boa imunidade a ruídos
  -    precisa de maior largura de faixa

- <u>Phase (PSK)</u>

  - precisa inicializar para determinar a fase inicial 
  - same as “angle modulation”?
  - Quadrature (QAM)

    -   Combinação das técnicas ASK e PSK elaborada de maneira a
        aumentar o número de bits transmitidos

### **Portadora digital**

-   Geralmente (==sempre?==) Trem de Pulsos
-   the square waves used in digital signals have a wide frequency spectrum and thus are subject to strong attenuation and delay distortion
-    
-   Taxa de bits (R_b): bits/tempo; bits por segundo
-   Intervalo de bit (T_b): 1/R_b
-   simbolo (bouds?): conjunto de n bits

#### Informação analógica

-   Pulse <something> Modulation
-   **Amplitude (PAM)**: ?
-   **Largura (PWM)**: ?
-   **Posição (PPM)**: ?
-   **(PCM)**: ?

#### Informação digital

-   se a informação for digital e a portadora digital, não precisa modular nada, mas precisamos determinar como será a transmissão (Código de Linha)
-   determina como a sequência de bits será transmitida, em que sinais elétricos será transformada 
-   ==qual a diferença para transmitir em banda base?==
-   modular = transmitir em banda passante?
-   BER = taxa de erro de bit
-   transparência: a probabilidade de erro é a mesma independente da mensagem
-   o que escolher em um codigo de linha
    -   temporização
    -   componente dc
    -   ==…==
    -   largura de banda ocupada
    -   eficiencia espectral: bits por hertz 
-   Alguns codigos de linha
    -   on-off: on is high voltage, off is low voltage; non transparent
    -   polar: on is high voltage off is negative high voltage
    -   bipolar: ?; non transpa
    -   Nonreturn to zero (NRZ): pequena largura de banda
    -   manchester: tudo por transição. Bom pra nao perder o sincronismo

### Demudulation

* coherent receiver
  * must generate a local carrier that is sincronous with the incomming carrier

## Multiplexing

* Simultaneous Transmission of Multiple Signals
* ==coisas tipo MPEG especificam tanto a multilexaão quanto a source coding… wtf?==

## Coding

* **source coding**
  * compressão de audio, video, etc; ==iso não pode mesmo ser considerado camada física, né??==
  * to generate fewest bits as possible
  * removes redundancy
* **channel coding**
  * same as error correction?
  * adds redundancy
  * paridade, CRC, etc

## Outros

* sataitile vai até uns 14GHz
* 