# Conceptualization

* “the most secure systems are those with nothing to steal”
* **Interesting study cases**
  * [https://en.wikipedia.org/wiki/December_2015_Ukraine_power_grid_cyberattack](https://en.wikipedia.org/wiki/December_2015_Ukraine_power_grid_cyberattack)
  * [https://en.wikipedia.org/wiki/Dual_EC_DRBG](https://en.wikipedia.org/wiki/Dual_EC_DRBG)
* **Catálogos de segurança**
  * OWASP Top 10 (2010)
    * Injection
    * Broken authentication
    * Sensitive data exposure
    * XML external entities (XXE)
    * Broken access control
    * Security misconfigurations
    * Cross site scripting (XSS)
    * Insecure deserialization
    * Using components with known vulnerabilities
    * Insufficient logging and monitoring
  * CWE/SANS Top 25 (2010)
* **Hashing**
  * See in Algorithms

## Goals
* Taken form Ammar Rayes, IoT book
* **Data Confidentiality**: ensures that the exchanged messages can be understood
  only by the intended entities.
* **Data Integrity**: ensures that the exchanged messages were not altered/tampered
  by a third party.
* **Availability**: ensures that the service is not interrupted. Denial of Service attacks
  target this requirement as they cause service disruption.
* **Secure Authentication**: ensures that the entities involved in any operation are
  who they claim to be. A masquerade attack or an impersonation attack usually
  targets this requirement where an entity claims to be another entity.
* **Secure Authorization**: ensures that entities have the required control permissions
  to perform the operation they request to perform.
* **Freshness**: ensures that the data is fresh. Replay attacks target this requirement
  where an old message is replayed in order to return an entity into an old state.
* **Non-repudiation**: ensures that an entity can’t deny an action that it has
  performed.
* **Forward and Backward Secrecy**: Forward secrecy ensures that when an entity
  leaves the network, it will not understand the communications that are exchanged
  after its departure. Backward secrecy ensures that any new entity that joins the
  network will not be able to understand the communications that were exchanged
  prior to joining the network.

## DevSecOps

* [guide DevSecOps](https://devsecops.pagerduty.com/)

## Access control

* Principle of "least privilege": providing that only authorized  individuals, processes, or systems should have access to information on a need-to-know basis.

### Authentication

* Never store user's plaintext passwords. Always store a "secure hash" that you can then verify.
* **Authentication principals**
  * Knowledge factors: something that just the user knows, like a password
  * Possession factors: something that just the user have, like a email account
  * Inheritance factors: some physical caracteristic of the user, like fingerprint; Very often it’s less secury than it seams
* **Cookie based authnetication**
  * the key could be something like 'username' and the value would be the  username.  Each time you make a request to a website, your browser will  include the cookies in the request, and the host server will check the  cookie
  * session must be kept both server and client-side
* **Token-Based Authentication**
  * When we talk about authentication with tokens, we generally talk about authentication with JSON Web Tokens (JWTs)
  * Token = string of character
  * “the bearer of the token can access the API”
  * ==is this the same as OAuth?==
  * ==always randomly generated?==
  * <u>JWT</u>
    * JSON Web Token
    * Contains a set of claims
    * Header: “alg” (algorithm, usually HS256) and “typ” (usually JWT)
    * Payload:
    * Signature: 
* **General good practices**
  * Notify users of suspicious login behavior
  * Limit the number of login attempts
  * password strength checkers

### Passwordless login

* With any type of passwordless login, users still have to prove that they are who they say they are with one or more forms of authentication
* **Motivation**
  * 30% of online customers [abandon their shopping carts](https://swoopnow.com/shopping-cart-abandonment/) if forced to register with a password
  * 75% of users quit after a [password reset](https://securityboulevard.com/2019/03/what-is-self-service-password-reset-sspr/)
* **Other forms of authentication**
  * Email Authentication, with Oauth2. ==Swoop service?==
  * Social Midia Authentication, with Oauth2

### Password managers

* bitwarden
  * ?

## Standards and audits

* **SOC 2**
  * Audit?
  * https://www.imperva.com/learn/data-security/soc-2-compliance/#:~:text=SOC%202%20is%20an%20auditing,when%20considering%20a%20SaaS%20provider
  * specially for SaaS and cloud
* **cissp**
  * Personal certification
  * good path toward CISO

## Others

* HSMs (hardware security modules) can move the overhead of
  key escrow and processing to a hardware device to ensure privacy, security, and
  proper authentication mechanism for nodes
* 
* 
* Security incidents
  * anything that compromise any of the goals
  * Data breach: unauthorized access to data; is a type of security incident
  * Data Disclosure: A breach for which it was confirmed that data was actually disclosed (not just exposed) to an unauthorized party
* Security Control levels
  * Preventive: ?
  * Compensating: secondary system that acts as the main one; ex: "db 1 fail -> db 2 gets activated"
  * Corretive: ?
*  

## Security for backend dev

* ​	Deixar os usuários somente com os privilégios necessários
* ​	Criar um novo usuário com os mesmos privilégios do root. Deletar o root depois.
* ​	Troque a porta 3306 para outra não convencional.
* ​	Criptografe informações confidenciais no servidor com HASH (ou [outro]())
* ​	Pré-processe os dados inseridos no login (usando sua linguagem de programação backend) para evitar SQL injection
* **basic principles that you should always bear in mind:**
  * Minimize Attack Surface Area
  * Principle of Least Privilege
  * Secure Defaults
  * Encrypt Sensitive Data (Don’t leave personally identifiable information, financial data,
    passwords, or other credentials in plain text, whether in a
    database or some other external file)
  * Maintain Security Updates
* **Sec in microservices**
  * authentication: Oauth and OpenID is the way to go; have a specific service to authentication
* **Other tips**
  * Don’t check in secrets, API keys, SSH keys, encryption
    passwords or other credentials alongside your source code in
    version control.
  * never do it yourself: if you can take battle-tested libraries, do it

# Criptografia

* Código: substitui simbolo/palavra por outro (levando em conta uma estrutura linguistica); Pouco usados

* Cifra: transformação simbolo por simbolo

* Modelo de criptografia:

  ![image-20200731193902426](Images - Security/image-20200731193902426.png)

* **O que deve ser garantido**

  * Sigilo
  * Integridade
  * autenticação

* **Principios**

  * Redundancia: eliminar uma parte não destroe o todo
  * Atualidade: garantir que as mensaguens foram geradas recentemente, ao invés de serem antigas repetidas

## Classificações

* **Quanto ao tipo de operações usadas para transformar o texto**
  * Substituição – cada elemento é mapeado em outro elemento
  * Transposição – elementos no texto em claro são rearrumados
* **Quanto ao número de chaves usadas**
  * Simétrica (uma única chave)
  * Assimétrica (duas chaves – cifragem de chave pública)
* **Quanto a forma na qual o texto em claro é processado**
  * Block cipher: seqencias inteiras; mais usado 
  * Stream cipher: 1 byte por vez, no fluxo

## Chave Simétrica

* transmissor e receptor devem possuir a mesma chave

* (-) A chave precisa ser distribuida

* criptografa e descriptografa com a mesma chave

  ![image-20200731214955497](Images - Security/image-20200731214955497.png)

* **Cifra de cesar**
  
  * Desloca no alfabeto
  
* **Cifra de substituição monoalfabética**
  * mapeaia cada letra pra otura
  * chave: string de 10^26 (alfabeto brasileiro)
  
* **Cifra de transposição**
  * reorganiza por colunas (por uma chave)
  * manda coluna por coluna
  
* blowfish (Velho e lento)

* RC6 (da empresa RSA)

* **AES**

  * ex Rijndael
  * AES256 is the most used for data description

* ==hash algorithms (like SHA) are asymetric criptography? Anyway, they are registered in Algorithms==

## Assimétrica

* A chave publica pode ser 100% aberta

* (+) não possui o problema de distribuição

* (-) lento

* Fácil de criptografar (usando a publica), dificil de descriptografar (usando a privada)

* **Workflow**

  * Cada usuário gera um par de chaves para
    criptografar/decriptografar mensagens

  * Cada usuário coloca sua chave pública em algum
    registro público ou local acessível

  * Se Bob deseja enviar uma mensagem
    confidencial para Alice , Bob criptografa a mensagem
    usando a chave pública de Alice

  * Quando Alice recebe a mensagem, ela
    decriptografa usando sua chave privada

    ![image-20200731220423735](Images - Security/image-20200731220423735.png)

* **Autoridade certificadora**

  * Why? Se maliciosamente trocarem as chaves publicas na hora que você requisitar-la, você esta criptografando com a chave errada (uma que talvez possa ser facilmente descriptograda)

* **envelope digital**

  *  para criptografar informação em grande quantidade (velocidade)
  * criptografa uma chave simétrica com uma assimétrica

* Diffie-Hellman

* RSA 

* El Gamal

## Protocolos criptografados

* Camada de enlace: não são usados, pois precisaria decriptar os pacotes nos switches
* **Camada de rede**
  * IPsec
    * IP security
    * provides security at the network layer.
    * Can be used to create a VPN (see below)
* **Camada de transporte**
  * criptografar conexões fim-a-fim, processo-a-processo
  * SSL
  * TLS
* **camada de aplicação**
  * SET
  * HTTPS

### **HTTPS**

-   Or *HTTP over TLS* or *HTTP over SSL*
-   TLS and SSL can be used interchangabely (TLS is better, is its sucessor)
-   used to authenticate the Web site to the user (so the user does
    not connect to a fake site believing it to be the intended site). The HTTPS protocol
    also encrypts data, and prevents man-in-the-middle attacks.
-   Enabeling TLS
    -   you need to get a digital certificate of trust from an entity called
        a certificate authority
    -   You should keep the certitification in your server
    -   the certificates proves that you are you

## virtual private networks (VPNs)

* inter-clients traffic is
  encrypted before it enters the public Internet

  ![img](Images - Redes de Computadores/image-20200515133537586.png)

# Attacks

## SQL Injection

* the attacker manages to get an application to execute an SQL query created by the attacker
* Prevent
	* Dont concatenate the user input directly to the SQL query
	* scape ‘ and “ form the user input
	* Instead of building the query dictly, use a library that already handles SQL injection with parametrized queries

##  cross-site scripting (XSS)

* When the site allow users to write public text (like a forum, or even just a name)
* Attacker enters code written in a client-side scripting language
* prevent
	* HTTP have a of check from what site the request is been originated (referer)
	* instead of authenticate the user by a cookie, authenticate by the IP
	* Never use a GET method to perform any updates
*  cross-site request forgery
	* little variation, when the code does a http request
	* Prevention:  disallow any HTML in the input

## Password Leakage

* When the DB pass is hard coded
* prevent
	* use some function to encrypt the password (but then the key to decript may also be vulnerable)
	* many database systems allow access to the database to be restricted to a given set of Internet addresses

## Security Assertion Markup Language (SQML)

*  provide cross-organization single sign-on
* outra alternativa: OpenID??

## Denial of service attack (DoS)

* do a lot of request
* that it, the server will not be able to serve real users
*  envio excessivo de pacotes SYN para um destino

## Packet sniffing

* see the raw thing, like using wireshark

## IP spoofing

* ?

## DNS redirect

* Or *DNS hijacking*
* atacar o DNS server para induzir um nome a apontar para um endereço diferente do correto
* um atacante consegue modificar servidores DNS para redirecionar usuários para sites de interesse do próprio atacante. Dessa forma, quando um usuário digita no navegador o nome de um domínio, como: exemplo.com, o DNS malicioso responderá essa busca com um site malicioso.
* Como evitar: proteja a rede do roteador
  * roteador configurado de forma segura e atualizado
  * Desabilite no roteador a administração remota do dispositivo
  * Use senha forte

## Cloud specific attacks

* Taken form Ammar Rayes, IoT book

![image-20201226142626808](Images - Security/image-20201226142626808.png)

* Highly distributed clouds (like using thousand of serveless things) are harder to be hacked; see in [this brief explanation](https://www.youtube.com/watch?v=XAjzzhBtMfY&list=PLIBwmCV8YuLOW-5lvtjY-9KPXwjfNyJ2K&index=8)

## Fog specific attacks

* Taken form Ammar Rayes, IoT book

* Unlike cloud data centers
  which are offered by well-known companies, fog devices are expected to be
  owned by multiple and less-known entities

  ![image-20201226142737426](Images - Security/image-20201226142737426.png)

## Physical/RF stuff

* See more in SDR
* unlock cellphone
  * by the USB is one way
  * I think that disable usb debug mode (in andorid) can avoid the attack; now I think it i disbled by d

# Security management

* least access principle: People only have access to what they need to use

* Deploy-production division

* Keep software updated

* Security services

* Have a good role based access control 

## Securing servers

* Sobre acesso ssh
  * o certo é criar o servidor, botar as chaves nos computadores que vão acessar ele (de frma que ele eles vão acessar sem botar senha) e fechar o acesso com senha
  * Software in linux: openssh

# Privacy

## User tracking

* **Fingerprinting**
  * Valid usages
    * newspaper offering x free news (and tracking you)
    * dating site dont allowing you to have 2 accounts 
    * fraud detection
  * what can be tracked
    * fonts (this one is important, because is highly unique and low normal usefulness)
    * user agent (browser) and extensions instaled
    * window size
  * Others
    * options of “do not track” doesnt help much
    * Test your browser to identify how unique you are: https://panopticlick.eff.org