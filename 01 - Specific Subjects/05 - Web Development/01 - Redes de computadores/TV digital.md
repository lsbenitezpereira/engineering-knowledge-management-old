# Conceituação

* TV aberta (ou *terrestre*, TDT), IPTV, WebTV, P2PTV

* Permite programação não linear (trasmtir aplicações e arquivos além do audio/video principal)

* (+) Qualidade de mídia

* (+) Interatividade

* [Good slides about DTV fundametals](https://www.slideserve.com/taya/4745795)

  ![image-20200906145524659](Images - TV digital/image-20200906145524659.png)
  
* Arquitetura geral:

  ![image-20200907165658455](Images - TV digital/image-20200907165658455.png)


* **Contexto**

  * in Brazil 95.1% of households have at least one TV set.[
  
  * foram distribuidos gratuitamente 14 milhões de conversores
  
  * A prefeitura de florianópolis distribuiu 50 mil do Kit Digital (ali por 2017), que continha antena de conversor 
  
  *  o Ministério das Comunicações definiu em 2011 a obrigatoriedade do ginga vir instalado (não sei quando isso foi efetivado)
  
  * [Notícia 2020](https://forumsbtvd.org.br/forum-sbtvd-trabalha-na-etapa-final-da-suite-de-testes-do-perfil-d-do-ginga/): Impulsionada principalmente pelo isolamento social, a audiência da TV  aumentou. Segundo dados da pesquisa da Kantar Ibope Media, realizada  entre 16 a 19 de março deste ano, para 77% da população, a TV aparece  como o meio mais confiável para se obter informações sobre o coronavírus (Covid-19). No período, os segmentos com destaque foram os  jornalísticos (alta de 17%) e os infantis (alta de 14%).
  
  * Até 2023, 90% dos televisores LCD deverão sair de fábrica com o Perfil D do middleware brasileiro ([fonte](https://www.telesintese.com.br/fabricantes-tem-de-colocar-novo-ginga-nas-tvs-a-partir-de-2021/)

## T commerce

* Análogo à E-comerce
* Comércio atrelado à TV
* [My slides: Zads - Considerações sobre T-comerce](https://docs.google.com/presentation/d/1S0qq8DsU6-t78XzxZOGb3Fu2hXiVDKHbQtZnqN4Nuj0/edit#slide=id.g9455d206b8_0_104)

## SBTVD

* Ou *ISDB-T International* (pois é baseado no padrão japonês ISDB)
* Sistema Brasileiro de Televisão Digital
* Middleware: Ginga
* Frame rate: 30 FPS (even in portable devices)
* video compression System: MPEG-4 AVC (H.264)
* Audio Compression System: MPEG-4 AAC
* Multiplexing system: MPEG-2 System
* **Normas ABNT**
  * Codificação de audio/video: 15602
  * Codificação de dados: 15606
  * Multiplexação: 15603
  * Transmissão: 15601
  * Receptores: 15604
  * Canal de retorno: 15607 (cancelada)

### Detalhes da transmissão

* Processes for Error Correction: Time Interleaving and Frequence Interleaving
* Modulation: BST-OFDM (Band Segmented Transmission-Orthogonal Frequency Division Multiplexing).
* Frequency Band: VHF or UHF
* Channel Bandwidth: 6 MHz (==portadoras multiplas? Quantas?==)
* **Faixas de frequência**
  * Canais que estão na faixa de 14 a 69, na faixa de 470 – 806 MHz
  * Canal 37 (frequência de 608 a 614MHz) não é utilizado  para radiodifusão, sendo reservado para pesquisas radioastronômicas. 
  * Canais de 60 a 69 (frequências de 746 a 806MHz) são reservados para as TVs públicas
* **Modos de transmissão**
  * <u>OneSeg</u>
    * recibir y decodificar el segmento central de
      la transmisión, independientemente de los demás 12 segmentos
    * ideada para ser uti-
      lizada en dispositivos móviles, cuya baja capacidad de recepción y procesamiento
      se ve beneficiada por los bajos anchos de banda y bitrates
    * menor ancho de banda
    * considerable disminución en el procesamiento del equipo transmisor
  * <u>Full seg</u>
    * 12 segmentos
* * 

### Canal de Retorno
* Ou *Canal de Interatividade*
* através da qual o receptor pode receber ou enviar dados
* Does not need to use the same medium as the main channel
* Sem o canal de retorno, podemos ter apenas uma interação local
* A ABNT não regulamentou ainda (a [NBR 15607-1:2011](https://www.abntcatalogo.com.br/norma.aspx?ID=87512) foi cancelada)
* **Transport layer possibilities**

  * Geralmente em TCP/IP
* **Physical layer possibilities**

  * analog TV VHF
  * WiMAX
  
  * GSM
  * qualquer meio que suporte internet (fibra, ADSL, etc)

### Outras features
* **Multiprogramação**

  * Vários programas sendo transmitidos juntos
  
  * Devido à limitação de bandwidth, geralmente se têm um prorama HD e vários de baixa qualidade
  
  * podem estar relacionados a um mesmo conteúdo (ex: multicamera) ou não.
  
  * bloqueada pelo Ministério das Comunicações para uso das emissoras de televisão comerciais (por questões de segurança)
  
  ![image-20200908170638956](Images - TV digital/image-20200908170638956.png)
  
  > Fonte: [Darwin Alulema](https://www.researchgate.net/publication/318762024_Desarrollo_de_aplicaciones_para_TDT_con_GINGA-J)


  <img src="Images - TV digital/800px-ISDB-T_CH_Seg_Prog_allocation.svg.png" alt="img" style="zoom:67%;" />

* **closed captioning**
  
  * ?



## TVD em outros paises

![img](Images - TV digital/380px-Digital_terrestrial_television_standards.svg.png)

* **ATSC 3.0**

  * USA
  * Usa DSM-CC

* **DVB-T2**

  * Europe

  * Integrated Services Digital Broadcasting

  * Middleware: MHP

  * ==Usa DSM-CC??==
  
  * DVB-ASI
  
    * transport strem mecanishm protocol
    * usually implemented in hardware

## Hardware and devices

* **MAM**
  * Media Asset Management
  * software that manages the ingestion and delivery of media files
* **Master control**

  * hub of a broadcast operation
  * Functions: monitor quality, ensure regulations meets, prepare programming for playout
* **playout**
  * Alguns playouts comerciais já embarcam outras funções, como modulação
  * [EITV playout](https://www.eitv.com.br/en/produtos/eitv-playout-professional/)
  * [PBS playout](https://www.pebble.tv/our-products/) (software-only runnig on VMs, plus optional hardware)
  * [DekTek StreamXpress](https://www.sencore.com/product/dtc-300-dektec-streamxpress-playout-software/) (playout software)
* **Conversor digital**
  * Digital -> Analógico
  * Muitas TVs  já saem de fábrica com o conversor digital embutido
  * Set-top box: Equipamento separado da TV
* **Meios físicos mais usados**

  * <u>ASI - Asynchronous Serial Interface</u>

    * Usually for compressed and multiplexed MPEG Transport Stream
    * ==? É o nome do conector para o cabo SDI? Ou o protocolo de ransmissão?== 
    * +-270 Mbps

  * <u>SDI - Serial digital interface</u>
    
    * cable and connector
    * Usually for uncompressed and unencrypted digital video+audio signals
    * standardized by SMPTE
    * HD-SDI: High-Definition SDI, 1.485 Gbit/s
    * SD-SDI: Standard-Definition SDI, 270 Mbit/s
    
  * <u>DVI - Digital Visual Interface</u>

    * video only

    * data rate is 3.96 and 7.92 Gbits/s

      <img src="Images - TV digital/DVI.jpg" alt="img" />

  * <u>VGA - Video Graphics Array</u>

    * Analog signal

  * <u>HDMI - high-definition multimedia interface</u>

    * Uncompressed signal

    * 5Gbps

      ​	![HDMI tipo A, B, C ou D? Entenda para que serve cada conector | TVs |  TechTudo](Images - TV digital/hdmi.png)

  * <u>Coaxial cables</u>
    
    * for RF signals
    * most used connectors
      * SMA: up to 26.5GHz (sometimes 40GHz); 2.4-, 2.92-, and 3.5-mm
      * BNC: up to 4GHz (sometimes little more); larger used in osciloscopes
      * Type-N: up to 300 MHz
      * F-type: usually used on house TV
      * Usually a thin-thin-thin gold-plating

## IPTV

* send television over IP network
* Broadband, in constrast to traditional broadcast
* usually transmitted with RTP (with runs over UDP); See more in *Computers Networks - Application layer*
* what is the difference to TSoverIP??
* from the arquitecture point of view, this is basically the same as normal internet

## Video on Demand

* the uer can select a program of his choice and watch

## Over the Top (OTT)

* Content that comes not from broadcast, but from internet?
* Difference to IPTV?

## UI/UX em TV

* **tipografia**
  * Fontes de TV: tiresias, unvier45, antique olive
  * tamanho recomendado: pelo menos 24px
  * fundo escuro, fonte branca
* **Fluxo**
  * Usar o botão vermelho do controle remoto par iniciar uma aplicação

## Landscape

* ITU-T
  * International Telecommunication Union
* radiodifusora
  * provedor de conteúdo
  * Globo, RIC, etc
* Operadoras
  * Net, Sky, etc
  * TV por assinatura, satélite, cabo, parabólica, etc
* GIRED
  * Grupo de Implantação do Processo de Redistribuição e Digitalização de Canais de TV e RTV)
* Tele Midia Labs
  * http://www.telemidia.puc-rio.br/about/
  * PUC-Rio
  * affiliated with its Computer Science Department
  * creator of Ginga
* selo DTVi
  * garante a compatibilidade com a ABNT 15606 (ginga)
* EI TV
  * Focada em hardware
  * Um apontinha em software e na implementação do Ginga
* Digilab
  * Focada em… hardware?
  * https://www.digilab.com.br/
* **AdMongrel**
  * Bussiness view very, very similar to Zads
  * https://blog.admongrel.com/
* **Forum SBTVD**
  * Tem unidade
  * Lista de participantes: https://forumsbtvd.org.br/estrutura/modulo-tecnico/participantes-tecnico/
* **Serviços de estatística e análise de audiencia**
  * People metter
  * Monitor Flex (serviço terceirizado do ibope); só para montorar os comerciais?
  * Pixes
  * Pesquisas pontuais?
* **Fabricantes de TV**
  * Samsumg: Tizen é o OS deles







# Ginga
* Middleware
* royalty-free standard
* For interactive video applications in different TV contexts (Broadcast, Broadband, IPTV and IBB), 
* Capa intermedia de software entre el sistema operativo del Set Top Box y la infraestructura de
  ejecución
* adopted by more than 13 countries
* Desenvolvido no Brasil
* open source, GPL license
* ABNT, na série 15606
* **Utilização**

  * vêm instalado set-top box
  * Permite executar aplicações
  * Consegue acessar conteúdo da Internet (precisa implementar um minibrowser em Lua?)
  * Voltado para o conteúdo enviado pela emissora
* **Possibilidades**
  * Interatividade
  * propaganda direcionada
  * sistemas multimídia
  * áudio imersivo (ex: seleção de faixa, direcionamento espacial, etc)
  * consumidor/telespectador
* **Outras anotações**
  * Rodando no raspberry pi: https://groups.google.com/g/tvinterativa/c/rP4-V40UwgU

## Architetura

* Ginga-NCL: Ginga's subsystem responsible for processing and presenting NCL 

* Ginga-J: Ginga's subsystem in charge of defining all the Java APIs

* subsistema do Ginga responsável por oferecer funcionalidades específicas de TV Digital e comum para os ambientes procedural e declarativo

  ![Ficheiro:Diagram SBTVD in layers.jpg](Images - TV digital/Diagram_SBTVD_in_layers.jpg)

  ![image-20200905011207912](Images - TV digital/image-20200905011207912.png)

  > Architetura da implementação da EiTV (acima)
  
* **Common Core (CC)**

  * Ou *núcleo comum*
* subsistema do Ginga responsável por oferecer funcionalidades específicas de TV Digital e comum para os ambientes procedural e declarativo
  
  * ambiente de execução das aplicações
* principais funções: exibição de mídias, o controle de recursos do sistema (plano gráfico, canal de retorno, dispositivos de armazenamento e etc.) e a sintonização dos diferentes tipos de canais
  
  * formatador NCL: responsável por receber um documento NCL e controlar sua apresentação
* ![image-20200908194301124](Images - TV digital/image-20200908194301124.png)


## Perfils

* características dos receptores para suportarem o middleware Ginga
* max 6MB. ==qual perfil?==
* **Perfil A**
  
  * Toda TV fabricada na ZFM devere incorporar o perfil A do middleware ([fonte, no final do texto](https://teletime.com.br/28/07/2020/minicom-autoriza-ginga-nas-smart-tvs-a-partir-de-2021/))
  * a maior parte das TVs suporta perfil A (tiago ribeiro, EiTV)
  
  * <u>Requisitos mínimos</u>
  * Não reproduz audio nem video
* **Perfil B**

  * Acho que nem chegou a ser usado

  * <u>Requisitos mínimos</u>
    * Video no formato MPEG1
* **Perfil C**

  * Possui canal de retorno
  * É o que foram distribuidos 14 milhões de conversores
  
  * foi desenvolvida especialmente para essa distribuição, mas nunca pegou entre os outros fabricantes
  * 2015
  
  * <u>Requisitos mínimos</u>
    * 512 de memória RAM e 2 GB de memória não volátil (flash)
    * Conexão com internet por meio de um modem externo via USB ou cabo
    * Outros formatos de vídeo?
    * aplicações podem ser instaladas no receptor para execução offline/assincrona
  * <u>Escopo excluido</u>
    * GingaJ passa a ser opcional
* **Perfil D**

  * Estimativa bruno: uns 4 anos para pegar
  * convergir a TV aberta com a banda larga… ==como?==
  
  * comunicação entre aplicativos externos ou locais que se comunicam com a TV por meio da API Restful do GingaCC WebServices
  * Até 2023, 90% dos televisores LCD deverão sair de fábrica com o Perfil D do middleware brasileiro ([fonte notícia](https://www.telesintese.com.br/fabricantes-tem-de-colocar-novo-ginga-nas-tvs-a-partir-de-2021/), [portaria interministerial](https://www.in.gov.br/web/dou/-/portaria-interministerial-n-40-de-24-de-julho-de-2020-268917347?inheritRedirect=true&redirect=%2Fweb%2Fguest%2Fsearch%3Fdata%3D28-07-2020%26secao%3Ddo1%26qSearch%3DPortaria%2520Interministerial%2520n%25C2%25B0%252040))
  
  * começa a permitir integração web com os elementos nativos da TV (smartTV) e até com dispositivos externos (smart homes, IoT, etc)
  * [webinar foda sobre isso](https://www.youtube.com/watch?v=KebwT_M0gqg)
  
  * <u>Requistios mínimos</u>
    * memória maior??
    * Precisa ter conexão direta com a internet?
  * <u>Suporte a</u>
    * OTT
    * criptografia
    * Incorpora HTML5, CSS3 e JS (especificação W3C, nada específico para Ginga)
    * suporte a DRM (Digital Rights Management)????
    * NEtworking melhor: stream adaptativo
    * plicações web no modo servidor
    * suporte à UDP (antes era só TCP)
    * Monitoramento do carrossel (adição de arquivos, modificações, etc)
  * <u>Out of scope</u>
    * suporta à segunda tela não é obrigatório

## Middlewares similares

* To allow  the execution    of MHP (Multimedia Home Platform) [3] applications on  other Digital    TV platforms, the DVB group proposed a unified  specification for Digital TV    System's middlewares, called GEM [10].  GEM includes MHP characteristics    that are not related to specific  characteristics of DVB system. This specification    has been adopted by the Japanese (ISDB ARIB B.23 [20]) and American    (ATSC ACAP [22] and  OCAP [21]) middleware standards. Ginga,    the middleware of the ISDTV-T - in its Java Part called Ginga-J - is compatible    with GEM.
  
  Following the same    trend of compatibility, The International Telecommunication Union (ITU) published    a set of recommendations called ITU-T, which  consists in: J.200, J.201 and J.202    (equivalent to GEM). These  recommendations intend to harmonize the systems of    Digital TV at  different levels
  
* **ARIB**
  
  * também japonês?
  * ==is this the middleware, or the whole system?==
  * a Ginga é formemente baseado nele
  * Usa DSM-CC
  
* **Multimedia Home Platform (MHP)**

  * Used in DVB (europe)

* **OCAP**

* **Jtvos**
  
  * By Cineca group (italia) ([contact info](https://www.cineca.it/en))
  * Open source middleware
  * https://www.yumpu.com/en/document/read/45526946/cinecatv-introduction-to-jtvos-download-java
  
* **DASE-ATSC**
  
  * ? 

## Implementações

* Implementação de referencia
* [Tutorial - Como rodar vários desses players](http://www2.elo.utfsm.cl/~elo323/ginga-ncl.html)
* 
* **PUC**
  * escrito em c++
  * fornecem compilável, pacote deb, ou em VM
  * [repositório](https://github.com/TeleMidia/ginga)
  * [Tutorial - Como executar na VM](https://www.youtube.com/watch?v=y6_cdSWnadM) 
  * [Tutorial - Como instalar a VM](https://www.youtube.com/watch?v=MH4L8zx2evI)
* **OpenGinga**
  * não emulada (o que exatamente isso significa?)
  * opensource
  * http://ginga.lavid.ufpb.br/projects/openginga/
  * [Tutorial - Como instalar](https://b4dtv.blogspot.com/2009/08/tutorial-de-instalacao-da-maquina.html)
  * [Tutorial - Como instalar (Zads)](https://drive.google.com/drive/folders/1qT5C3iVtz8tlS4oHupaJgjE8y5uD66r1?usp=sharing)
  * parece completa, mas o ultimo commit foi em 2011
  * laboratório LAVID
  * união da Implementação de Referência Ginga-J do Lavid da UFPB e a  Implementação de Referência Ginga-NCL do laboratório Telemídia da  PUC-Rio
  * Possibilita instanciar diferentes versões de Ginga
  * Para PC, não para set-top-box
  * Maquina virtual
    * versão 0.4.3 instalada do OpenGinga
    * Ubuntu 10.04
* **Ginga.AR** (Argentina)
  * usado pela Philco nas TVs argentinas (fonte)
  * Parece que no começo estava disponível, mas agora não consegui achar. Acho que fizeram algum acordo com a Philco e tiraram do ar.
* **GingaGO**
  * mobile implementation of Ginga NCL specification developed
    by Digital Stream.
* **Set Top Boxes**
  * diversos set top boxes permitem executar aplicações ginga diretamente a partir do pendrive, sem precisar receber do sinal de broacast
  * <u>bolsa familia</u>
    * ==PEGA EMAIL==
  * <u>VT7200D</u>
    * ==PEGAR ANOTAÇẼOS==
    * [Rodando aplicação Ginga no VT7200D](https://thiagovespa.com.br/blog/2011/12/23/rodando-aplicacao-ginga-no-vt7200d/)
    * [REVIEW VISIONTEC VT7200A](https://www.techtudo.com.br/review/visiontec-vt7200a.html)
    * Não consegui rodar, parece que precisava de uma atualização de software e a Visiontech não faz mais

## Application Layer

### NCL

* Nested Context Language

* linguagem declarativa

* Extension of XML

* Foco: authoring

* define como objetos de mídia são estruturados e relacionados, no tempo e espaço

* treats hypermedia relations as first-class entities

* does not define any media content itself, just how to glue them togueder

* define uma separação bem demarcada entre o conteúdo e a estrutura de uma
  aplicação

* Estas anotações se referem majoriariamente à versão 3.0 perfil Enhanced Digital TV

  ![image-20200907200029591](Images - TV digital/image-20200907200029591.png)


* **Metadados**
  * Declarados no começo do documento
  * First line: version
  * `<?xml>`: ?
  * `<ncl>`: 
    * root element
    * id and namespace (`xmlns`, specifing the ginga profile to be interpreted)
    * Allowed profiles: http://www.ncl.org.br/NCL3.0/EDTVProfile and http://www.ncl.org.br/NCL3.0/BDTVProfile ([source](http://handbook.ncl.org.br/doku.php?id=ncl))
    * the schemaLocation (`.xsd` file) should be detected automatically
* **Versions**

  * 2.0: ?
  * 3.0:  versão inicial do padrão para TV digital
  * 4.0: interfaces multimodais; objetos 3D
  * It also have profiles (Full Profile, Language Profile, Enhanced DTV Profile, etc) which are not related to the Ginga profiles (A,B, C, etc)
  * Those profiles are subset of the full language, specific to some application
* **Useful links**

  * [quick docs/tips](http://www.telemidia.puc-rio.br/~roberto/ncl-tooltips/)
  * [awesome 1](http://www.telemidia.puc-rio.br/~roberto/ncl-resources/doku.php?id=start)
  * [NCL Validation Service](http://validator.ncl.org.br/index.php)
* **Other annotations**

  * Segundo a ANBT não podemos usar CC com aplicação Ginga ativada  (simultaneamente). A implementação de cada receptor deve decidir como  tratar as exceções (ex. pressionar botão cc depois do app ginga já estar aberto, ou abrir app ginga quando o cc já estiver ativado).  (fonte: Bruno)

#### Entities

* head: código das regiões, descritores, regras, conectores, entre outros

* body: código das midias, entre outros

* toda tag possui id único

* a tag pode ser autofechável (`<tag name=value ... />`) ou expadida (`<tag name=value ...> </tag>`)

* **Nodes**
  * Entities

  * o quê

  * podem conter um conjunto de propriedades ([list of properties](http://handbook.ncl.org.br/doku.php?id=property#predefined-properties))

  * especificar as propriedades de um objeto pode ser feito por propriedades no objeto ou por um descritor (preferível)

  * <u>Mídias</u>

    * Ou *objeto de mídia*

    * objetos de mídia estáticos (imagens, etc) não possuem duração implícíta (atributo explicitDur)

    * videos, images, audios, and texts

    * Properties: id, src, descriptor (id to descritor que contém as propriedades aplicadas à essa mídia)

    * Optional properties: type, refer, instance

    * o import de midia não mantem o aspect ratio

    * o src geralmente aponta para um fluxo elementar MPEG System (:book: Soares Capítulo 8), `sbtvd-ts://0`

    * Inline properties:

      	`<media id="photo" src="media/photo.png"/>`
  
    * As propriedades também podem estar definidas de forma separada, como tag filha, o que permite alterar ==de forma dinâmica==:
  
      ![image-20200911165406351](Images - TV digital/image-20200911165406351.png)

    * Area: parte da mídia (ex: trecho do video)

    * Há alguns tipos especiais, como “application/x-ncl-settings” type (ver um arquivo separado)
  
* <u>Composition nodes</u>
  
  * or *contexts*
  
    * Aggregate several nodes
  
    * não é possível acessar diretamente objetos de mídia dentro de um contexto
  
    * o acesso deve ser feito passando pelas portas,
  
* **Descriptor**

  * Como
* agrupar um conjunto de propriedades
  
  * Pode ser compartilhado por mais de uma mídia
* aponta para uma região
  
  * NCL composer criar descritor: arraste a mídia para o frame na visão de layout
* In head inside `<descritorBase>`
  
  * ==o uso do atributo refer indica que o elemento herdará toda a definição do elemento referido==
* <u>Parameters - General</u>
  
  * `focusIndex`
  * `explicitDur` (duração que será exibido)
  * `moveUp`, `moveDown` (inteiros que descrevem o índice navegável)
  * <u>Parameters to borders</u>
  * focusBorderColor (“white” | “black” | "red" ... )
    * selBorderColor (“white” | “black” | "red" ... )
  * focusBorderWidth (integer, borda internal caso negativo)
    * focusBorderTransparency (0 a 1 ou 0% a 100%)
* <u>Parameters to text midias</u>
  
  * `fontColor`: blue, red…
  
    * `fonteSize`: 10, 20, …
  
  * `weight`: bold, normal, …
  
  * `fontFamily`: Tiresias, etc…
  * <u>Parameters to transitions</u>

    * TransitionBase?
  * transIn: ?
    * transOut: ?
* <u>Parameters to sound</u>
  
  * `soundLevel`: 0-1
  
* **Região**

  * Onde

  * agrupa propriedades espaciais de uma mídia

  * In head inside `<regionBase>`

  * pode ser especificado em porcentagem ou pixels (só o valor, mas o NCLcomposer buga)

    ![image-20200911164818267](Images - TV digital/image-20200911164818267.png)

  * Pode especificar em que tipo de dispositivo será exibido, por exemplo: `<regionBase device="systemScreen(1)">`

  * Propriedades: height, width, left, right, top, bottom, zindex

* **Link**

  * Quando
  * defines a relationship among media and composite objects
  
  * event based
  * NCLcomposer add link: shift, clica no primeiro objeto, arrasta para o segundo objeto (aba estrutural)
  
  * <u>Bind</u>
    * modificam/especificam as roles definidas no connector
    * Condições
      * onBegin, onEnd
      * onPause, onResume
      * onAbort
      * onBeginAttribution, onEndAttibution
      * onSelection (recebe um parâmetro como ` name="keyCode" value="RED"`); A quantidade máxima deveria ser 1
    * Ações
  
      * Start, Stop
      * pause, resume
  
      * abort: ?
  * <u>Connector</u>
    * O conector é descrito separadamente, e o link apenas referencia ele
    * O mesmo conector pode ser referenciado por varios links, mas um link pode referenciar apenas um conector
    * Ao referenciar um conector definido externamente ao documento, um
      elemento <link> deve concatenar o valor do atributo alias com o caractere
      especial # seguido do identificador do conector.
    * isso é bizzaro, na minha opinião foi alguma coisa que eles não conseguiram resolver e fizeram gambiarra
* **Connector**

  * In head inside `connectorBase`

  * definem relações n:m entre interfaces de nós

  * roles: interface points

  * The main (only?) type of connector is the CausalConnector

  * São geralmente escritos em um arquivo separado, que descreve todas as possibilidades, nomeadas pela concatenação dos binds

  * ações e condições e parâmetros

  * Example:

    ![image-20200911183302466](Images - TV digital/image-20200911183302466.png)

  * conditions: simple or compound

  * actions: simple or compound

* **Portas**

  * definem quais os objetos daquele contexto serão iniciados quando aquele contexto for iniciado

  * Properties: id, component (to whom it points), ?

* **âncoras**

  *  subconjunto das unidades de informação de um nó

  *  Exemplo: para um nó de vídeo, uma âncora pode ser um trecho do vídeo

* **Switchs**

  * define regras para adaptar o conteúdo a ser exibido

  * Varibles, comparrison operator, value

  * Simple switches: alternativas de conteúdos

  * Descriptor switches: alternativas da forma de exibição


#### **Comandos de Edição**

* Pode vir do usuário (interação), de uma aplicação aninhada, ou da rede (broadcast)
* :book: Soares capitulo 16
* Sem autorização vs sob demanda??
* Principal objetivo: permitir à aplicação manipular mídias em tempo de execução (após a aplicação ser enviada pelo emissora)
* não é possível precisar o momento da chegada de um descritor no receptor, mas é possível espeificar a execução com NPT (Normal Play Time)
* <u>descritores de eventos</u>
  * encapsula comandos
  * Propridades: id, referência de tempo (delay após recebimento do comando), commandTag, campo de dados privados, etc
  * will be sent in the DSM-CC stream-event descriptors; See more about it in DSM-CC
* <u>Tipos de comandos</u>
  * operação da base privada
    * openBase,
      activateBase, deactivateBase, saveBase, and closeBase
  * manipulação de documentos
    * adicionar, remover e salvar um documento em uma base privada aberta e para iniciar, pausar, retomar e parar apresentações de documentos em uma base privada ativa
  * manipular entidades NCL de um documento em uma base privada aberta
    * addNode (baseId, documentId,
      compositeId, {uri, id}+)
    * removeNode(baseId,
      documentId, compositeId,
      nodeId)
    * setPropertyValue(baseId,
      documentId, nodeId,
      propertyId, value)
    * Entre outros
* <u>Base privada</u>
  * Estrutura de dados fornecida pelo Ginga
  * A base privada organiza documentos em uma estrutura de árvore
  * Stay in the receiver memory.
  * Gerenciador de Base Privada (do ginga): responsável por receber comandos de
    edição de documentos NCL (se registra para escutar o ID do *Objetos de eventos de fluxo DSM-CC*) e pela execução desses comandos
  * Cada canal de televisão esta associada a pelo menos uma base privada
  * uma aplicação em uma base privada associada a um canal de TV só pode ser executada quando esse canal estiver sintonizado (ABNT 15606-2:2018, pg  146)

#### Settings

* specifying an object whose properties are global variables defined by the document author
  or are reserved environment variables that may be manipulated by the NCL document
   processing
* propriedades globais definidas pelo programador e variáveis de ambiente reservadas
* um documento NCL pode conter apenas
  um objeto do tipo settings
* Alguma podem só ser lidas, outras lidas e escritas
* :book: Soares, pg 259
* **System**
* **User**
* **Default**: valores padrões para algumas propriedades
* **Service**
  * <u>currentKeyMaster</u>
    * Determina quem controla as teclas
    * O valor da propriedade service.currentKeyMaster deve ser o identificador da mídia associada (atributo id)
    * Se vazio, o controle é do formatadorNCL
    * Nas versões anteriores do Ginga, a mídia recebia o controle da tecla  somente através da atribuição de seu focusIndex ao  service.currentKeyMaster. Depois, mudou-se do focusIndex para o id da  mídia. Por isso, para manter a compatibilidade com aplicações mais  antigas, as mídias cujo descritor possui um focusIndex recebiam o valor  do focusIndex, e não do id.
* **SI**
* **Channel**
* **Shared**

#### Embedding HTML

* NCL does not substitute, but can embed HTML-based documents
* ==HTML vs XHTML???==
* The NCL presentation engine have a XHTML-based user agent (based on the web browser Links), which includes a stylesheet (CSS) and ECMAScript interpreters
* Dicas gerais
  * o documento html deve estar em conformidade com a especificação definida pelo W3C
  * Deve possuir `<head>`
  * Deve possuir `<title>` dentro do `<head>`

#### Embedding stream

* 

#### Others

* **Events and lifecicle**
  
  * Event state machine:
  
    ![img](Images - TV digital/fetch.php)
  
  * types of events
    
    * presentation
    * selection
    * attribution
    * composition
  
* **Recursos de teclado**
  
  * Lista completa em :book: Soares pg273
  
    ![image-20200911182850267](Images - TV digital/image-20200911182850267.png)
  
* **Recursos de foco**

  * para navegação por setas do controle remoto
  * Um elemento em foco, caso seja pressionada a tecla “ENTER”,
    passa a receber (controlar) toda a navegação pelo controle remoto até que a
    tecla “BACK” seja pressionada
  * Os detalhes disso são descritos no <descriptor>
  * atributos
    * focusIndex (int; defines the sequence)
    * moveLeft, moveRight, moveUp, moveDown,
    * focusBorderColor, focusBorderTransparency, focusBorderWidth
    * focusSrc, focusSelSrc e selBorderColor
  * Acho que a unica forma de fazer um foco mais “bonito” (com mais estilização do que apenas a borda) é controlando individualmente a sequencia (ver exemplo *TVDigital_Social*)… acho…

* **==Timing==**

* **Edição de elementos**

  * As propriedades de um objeto de mídia passíveis de
    manipulação através de relacionamentos devem ser explicitadas por meio de
    elementos <property>, mesmo que seus valores iniciais sejam definidos
    usando elementos <descriptor> ou <region>
  * Exemplo de resize: :book: Soares pg 69
  * Parâmetro “fit”: :book: Soarea pg 222

* **Extending with imperative code content**
  
  * Lua code, ECMAScript code, Java code
  * Included as midia node object
  * Ver mais sobre lua em sua seção específica
  
* **Extending with declarative content**
  * Included as midia node object
  * See more in http://handbook.ncl.org.br/doku.php?id=importbase
  *  HTML-based code, SVG code, X3D code, SMIL code, other NCL applications
  *  <importedDocumentBase>? Na minha versão do ginga (0.13.6~202009242107~ubuntu18.04.1) deu problema
  
* **IDEs**
  
  * Eclipse com plugin: voltado para code
  * NCL composer: voltado para edição visual; permite edição por código, mas fica brigando com a edição visual

### Lua

* pequena e leve
* criada em 1993 no Tecgraf, um laboratório de pesquisa e desenvolvimento da PUC-Rio
*  Eclipse is promoting as an ideal IoT programming language
* **Lua virtual machine**
  * interprete from bytecodes
  * a simple while-switch dispatch loop is used for running the program, one instruction at a time
  * It is *register based*, that is different from *stack based VMs* like java’s… ==what is the difference?==
  * totalmente ANSI C
  * the core is less than 100 Kbytes (==this includes the compiler?==) + todas as bibliotecas padrão pesa uns 150k
* **LuaC**
  * Precomiler
  * convert the code bytecode, to be interpreted by the Lua Virtual Machine
  * Quando um codigo é executado, ele é primeiro pré-compilado em instruções para uma máquina virtual e depois
    o código compilado é executado por um interpretador para a máquina virtual.
  * Codigos também podem ser pré-compilados em uma forma binária, usando o compilador luac
  * Programas na forma de código-fonte e na forma de um arquivo-fonte já compilado são intercambiáveis;
    Lua automaticamente determina qual é o tipo do arquivo e age em conformidade com ele.
  * can be executed with `lua_dofile` in C or with `dofile` in Lua
  *  The binaries are portable to all architectures with the same word size
  *  The compilation speed of Lua is much faster than other scripting languages
  * Currently the compiler
    accounts for approximately 30% of the size of the Lua core. For memory-limited
    applications, such as embedded systems, it is possible to embed Lua without the
    compiler. Lua programs are then precompiled off-line and loaded at run time by
    a tiny module (which is also fast because it loads binary files). 
  * <u>precompiling and Ginga</u>
    * o suporte de execução de bytecode foi desabilitado nas TVs (ABNT 15606-2:2018, pg 146)
* **LuaJIT**
  * Just-In-Time Compiler
  *  can run Lua code substantially faster
* **Mihini**: embedded Lua runtime

#### Fundamentals

*  Origem do nome: remete à ideia de uma linguagem satélite

* funcional e imperativa, procedural

* tipada dinamicamente, strong typed

* uma das mais utilizadas no mundo na área de jogos e entretenimento

* (+) núcleo é muito pequeno e totalmente ANSI C

* (+) alto grau de portabilidade

* (+) licença livre

* (+) Low embedding cost

* **data types**

  * number
  * string
  * nil
  * boolean (true or false)
  * <u>Table</u>
    * {}
    * unordered
    * implemented as hash table
    * acess and asignment by dot: `t.key = 666`
    * keys can be string, numbers…anything?
    * the values can be values, expressions, functions, anything
    * if you ommit the key in declaration, the keys will be sequencial (array)
    * ==metatable?==
    * `table.sort(f)`
  * <u>Arrays</u>
    * tables with specific key structure, always in sequence; 
    * index in 1, by convention
    * Have no order
    * Because arrays are represented by tables, they are naturally sparse
    * you only need space for the non-nil elements
  * function (see more in specific section)
  * userdata
  * thread

* **casts**

  * tonumber(<string>)
  * tostring(<anything>)

* **Functions**

  * declared with `function <name>(<parameters)` … `end`
  * all parameters are optional, in a too-much-imo flexible way
  * can return several values
  * anonymous function: `function () return 666 end`
  * varag: `function(foo, ...) end`

* **operators**

  * `^`: exponentiation
  * `%`: modulus 
  *  
  * `==`
  * `~=`
  *  
  * `..`: string concatenation 

* **modules**

  * just load: `loadfile('filename.lua')`
  * load module and run: `dofile('filename.lua')`
  * `asName = require('filename')` (caches the file; searches paths; without file extension)
  * package.preload: when the module is required, this function will be called (you can overwrite it for performance, adding the module in build time)
  * module() is deprecated in Lua 5.2; you should use the new syntax (create a table inside of your program and return it). The format that was  originally use is no longer supported.
  * <u>Execute from string</u>
    * compiled in execution time
    * very similar to `dofile`
    * Both loadstring and loadfile never raise errors. In case of any kind of error, both functions return nil plus an error message
    * Security tips
      * Optimally checksum and sign the code, and verify the signature before doing anything
      * You could also use rings library to isolate the code you're running within the Lua environment itself

* **Errors**

  * Implemented with C longjmp()

  * `error("<message>")`

    * you can use a value of any type as an error message, usually error messages are strings

  * `assert(<value>, <expectation>)`

  * `pcall()`
    
    * If there are no errors, `pcall` returns **true**, plus any values returned by the call. Otherwise, it returns **false**, plus the error message
    * We *throw* an exception with `error` and *catch* it with `pcall`
    
  * `xpcall()`

    * Besides the function to be called, it receives a second argument, an error handler function
    * In case of errors, Lua calls that error handler before the stack unwinds, so that it can use the debug library to gather any extra information it wants about the error.

  * `debug.traceback()`

  * <u>good practices (in genera)</u>

    *  Criteria: If caller usually can't deal with
      the situation locally, it's an error

    * On error, function returns [nil, error
      message] tuple

    * Can use assert() to convert to
      exception

    * Try-except implementation:

      ```lua
      function try(f, catch_f)
          local status, exception = pcall(f)
          if not status then
          	catch_f(exception)
          end
      end
      try(function()
          -- Try block
          --
      end, function(e)
          -- Except block. E.g.:
          -- Use e for conditional catch
          -- Re-raise with error(e)
      end)
      
      ```

*  **flow control**

   *  if then end

   *  if then elseif else end

   * for (both ends are included):

     ```
     for i = <start>, <end>, <optionalStep> do
       lalala
     end
     ```

     ```
     for k,v in pairs({key1=value1, key2=value2}) do
        lalala
     end
     ```

     * 

* **OO**

  * You can use tables as classes
  * Everything will be just convention, Lua is not OO
  
  * allow opeartor overloading, overwriting the functions `__add`, `__sub`, …
  * `object.methdo(object)`
  
  * `object:method()`
  * [Small library to make it easier](https://github.com/jonstoler/class.lua)
  
* **Coroutines**

  *  Similar to threads in multiprogramming, but execute only one at a time
  *  chamadas a corrotinas são síncronas (o código que chama uma corrotina sempre aguarda o seu retorno)
  *  With coroutines, at any given time, only one coroutine runs and this  running coroutine only suspends its execution when it explicitly  requests to be suspended
  *  Podem ser paradas (yield) ou reiniciadas (resume) em diferentes momentos
  *  `coroutine.create`: recieve a function, returns a value of type thread
  *  The coroutine  starts in the suspended state
  *  `coroutine.resume`: chamado fora da função, recebe a corotina e seus parâmetros de entrada
  *  if there is any error inside a coroutine, Lua will not show the error message, but instead will return it to the `resume` call.
  *  `yield`: chamado dentro da função, recebedo os valores de retorno (é um return não definitivo)
  *  `return`: chamado dentro da função, finaliza a corotina
  *  Status: running, ==normal==, suspended or dead
  
*  **Memory management**

   *  Have Garbage collector
   *  ==How it deals with dynamic memory allocation? I think you dont, unless you write in C==
   *  The collection is incremental: interleaved with the execution of the main program
   *  `collectgarbage("collect")` − Runs one complete cycle of garbage collection.
   *  `collectgarbage("count")` − Returns the amount of memory currently used by the program in Kilobytes.
   *  Weak Tables: ?

* **Others**

  * print(“lalalala”)

  * io.open(‘filename’, ‘w’)

  * `math.random(<max>)`

  * `math.sqrt(<number>)`

  * Options to implement an “sleep”: http://lua-users.org/wiki/SleepFunction

  * allows multi assignment: `a,b = 666, 667`

  * `;` is an optionalline separator

  * comments: `--` (singleline) and `--[[ lalala ]]--` (multiline)

  * <u>metatable</u>
    * ?
  
  *  <u>Environment</u>
  
     * Similar to metatables
     * regular tables associated with objects
     * multiple objects can share the same environment. 
  
  *  Command line and processes
  
     * os.execute(<>)
     * io.popen()
     * [interesting library with non-ansi extensions](https://github.com/LuaDist/luaex)
     * [library - LuaFileSystem](https://github.com/keplerproject/luafilesystem)
  
* **Libs for numemecial computing and algebra**

  *  <u>SciLua</u>
     - [https://github.com/stepelu/lua-sci](https://github.com/stepelu/lua-sci)
     - seems great
  - <u>Lunum</u>
      - [https://github.com/jzrake/lunum](https://github.com/jzrake/lunum)
      - deprecated
  - <u>Torch</u>
      - [http://torch.ch/](http://torch.ch/)
      - machine learning (yes, it inspired PyTorch)
      - deprecated

*  **Package managers**

   *   <u>LuaRocks</u>
       *   to install Lua modules as self-contained packages called rocks
       *   packages  written in Lua (.lua files) or binary modules (.so/.dll files)
       *   specification format (.rockspec)
       *   distribution format (.rock)
       *   Run `luarocks init` to start a project; “Everything in the current directory”
       *   [Compile C library as luarocks package](https://stackoverflow.com/a/47350675)

#### C CPI

* You can also do the other way arround, calling lua from C. [Tutorial](http://www.troubleshooters.com/codecorn/lua/lua_c_calls_lua.htm)
*  [tutorial](http://www.troubleshooters.com/codecorn/lua/lua_lua_calls_c.htm)
*  must install liblua5.1-dev (from apt)
*  imported as module from a `.os` file
*  C packages need to be loaded and linked with an application before use
*  Lua uses a virtual stack for passing values between Lua and C
*  Whenever you push an element onto the stack, it is your responsibility to ensure that the stack has space for it; you can call `    int lua_checkstack (lua_State *L, int sz);`
*  [interesting tutorial, doing thing in a slightly ifferent way](https://medium.com/@imwithye/interfacing-lua-with-c-92a1067435ab)
*  [Creating a Luarock package from C](https://stackoverflow.com/a/47350675)
*  <u>C file</u>
   *  Register this file's functions with the luaopen_<libraryname>()
   *  his function should contain lua_register() commands for each function you want available from Lua.
   *  within the fuction, the return value is the number of items it's returning; values return with `lua_push<type>()`; takes in a Lua state as the input; parameters are read with (alreay checking the type, as good practice) `luaL_check<type>(L, 1);`
*  <u>To move: OS file</u>
   *  shared object
   *  dont have a main
   *  compiled set of functions
   *  reusable
   *  compiled to a given architecture, and runs in any computer with that architecture
   *  can be used in #include <>
   *  In Windows, it's called DLL
*  <u>loadlib</u>
   *  loads a so… also?
   *  whats the difference?
   *  is 5.1, package.loadlib()
*  

#### Minificação e obfuscação

*  obfuscator: need to be reversed to execute the code

*  random character substitution may cause conflicts if not verified; longer strings hurt performance but have less conflicts

*  no caso da TV não é recomendado precompilar, por problemas de compatibilidade com o settopbox (na implementação PUC, por exemplo, botar um bytecode dá o erro `version mismatch in precompiled chunk`)

*  **LuaSrcDiet**

   *  size optimizer

   *  https://github.com/jirutka/luasrcdiet

   *  Avoid deleting a block comment with a certain message with *--keep*; this is for copyright or license texts.

   *  for maximum code size reduction and maximum verbosity, use:

      ```
      luasrcdiet application.lua -o min.lua --maximum
      ```

*  **Squish**

   *  size optimizer, amalger (didnt worked for me), uglifier (didnt worked for me)
   *  More aggressive than luasrcdiet
   *  incorporates LuaSrcDiet
   *  http://code.matthewwild.co.uk/squish/file
   *  http://matthewwild.co.uk/projects/squish/readme.html
   *  Comparação de performance: http://matthewwild.co.uk/projects/squish/squish_penlight.png
   *  Meus resultados
      *  não consegui juntar arquivos
      
      *  Para o caso de TVs, cheguei a conclusão que o suporte não é bom; não consegui idnetificar o porquê
      
      *  comparação de compressão:
         
         ```
         original size: 19084
         13122 (minify basic)
         12798 (uglify)
         7571 (full iglify)
         7308 (full uglify, full minify)
         apos uglify, o gingaPUC não consegue mais executar (erro `attempt to call global 'getfenv' (a nil value)`); Those two function were removed in Lua 5.2
         ```
   
*  **Aztup Brew**

   *  https://obfuscator.aztupscripts.xyz/
   *  [Post inicial sobre o app](https://v3rmillion.net/showthread.php?tid=936308)
   *  Obfuscator and uglyfier
   *  (-) aumenta muito o tamanho do arquivo: de 19kB  para 90kB, na mais conversadora das modificações (chega a 530kB em outras)
   *  (-) código fechado
   *  (+) disponível via API
   *  modified version of ironbrew 2 made by Defcon42 (que não parece estar disponível)
   *  [Consumir a API com nodejs](https://github.com/LaziestBoy/Aztups-Obfuscator-NodeJS)
   *  <u>Configuration options</u>
      * NoControlFlow
      * NoBytecodeCompress
      * EncryptStrings
      * EncryptImportantStrings
      * PreserveLineInfo
      * AddMemes
      * Uglify

* **Ironbrew**

  * obfuscator
  * made by Defcon42 
  * written in C#
  * was meant to be paid, I’m not sure now
  * I found [this repo](https://github.com/Trollicus/ironbrew-2), but I’m not sure if it’s the original
  * usa luadiet para minifcar
    a encriptação parece algo bem handmade, não sei qual algoritmo ele usa
    a compressão parece ser só uma mudança de base
    precompila? acho que sim 
  * [another repo, irobrewV1](https://github.com/GustasRBLX/Ironbrew)
  * [another repo](https://github.com/SnowQT/ironbrew-2)

* **WallySecure**

  *  theres also a paid version, you can buy it in the discord server (discord.gg/wallyhub)
  *  I’ve installed the VScode extension, didnt understood how to use
  *  was recomended as the best (https://devforum.roblox.com/t/the-guide-for-obfuscation/614643/36)
  *  I entered their discord group and… wtf was that?? 

*  **lua-minifier**

   *  https://goonlinetools.com/lua-minifier/
   *  online tool
   *  very simple: probably just uses luadiet

*  **XFuscator**: https://github.com/efrederickson/XFuscator

*  LuaGuard: obfscator and IDE?

*  https://luac.mtasa.com/

   *  obfuscator
   *  (-) compila para luac

*  https://github.com/Peepx0/LuaObfuscator/blob/master/Obfuscator.lua

   *  very simple, just random substitution

*  https://mothereff.in/lua-minifier

   *  didnt improved luasrcdiet
   
*  *  

*  Verdict: obfuscator; paid

*  Luraph: obfuscator; paid; got discontinued

*  Syanpse Xen: obfuscator; paid; [Presentation post](https://wearedevs.net/forum/t/8523?__cf_chl_jschl_tk__=51c3a0f96e4e82ea180c4dead2771d84e045a29a-1606241620-0-AT49079OQtrgL7zr1YljgwE4HA0KGtG0lMstdblD-sT8ZHslLC98HRbwU7dEnZKB356wBkyfo5k-NqJc5wQ7pEqI0inIuONxumZLdDqf0FcnEZNO6Fc__LxrlKBP6Spd269nih6tzpb3z06y6eWtMbMQdX4A6RurOXkXluYz8PenTiPJD4MITxz1N9-3LvdtrWTFK7GNZPW8_MLHnixpQOfgygzGFY1Q1iFqTx6wCWn7Ryqt6st2JlLnLPA4K8P3E_eRbl8b7AkfRP5mo2J__uaLzYF_FF_IDi8rpPKs0mpw5pS9oev0yctLLtUojbGW_Q)

*  

*  Others: https://www.xspdf.com/resolution/50867312.html

*  

   

   https://github.com/jkusner/LuaObfuscator

   https://github.com/PY44N/Lua-Obfuscator

   https://github.com/Peepx0/LuaObfuscator

   https://controlc.com/205c74cd

#### Profiling

* https://bitbucket.org/itraykov/profile.lua/src/master/

  

  Export file to visualize with KCachegrind: https://jan.kneschke.de/projects/misc/profiling-lua-with-kcachegrind/

#### TV specifcs

* Pode ser incluida em uma aplicação NCL

* execução orientada à eventos

* Para se adequar ao ambiente de TV Digital e se integrar à NCL, a
  linguagem Lua foi estendida com novas funcionalidades

* O que diferencia um NCLua de um programa Lua puro é o fato de ser
  controlado pelo documento NCL no qual está inserido e utilizar as extensões providas pelo ginga

* não há nenhum suporte a *multi-threading*

* **Canvas**

  * guarda em seu estado atributos sob os quais as primitivas gráficas operam
  * getters e setter acessado como: `canvas:attrColor()` e `canvas:attrColor(value)`
  * As coordenadas passadas para os métodos são sempre relativas ao ponto mais à esquerda e ao topo do canvas
  * [Documentation](http://www.telemidia.puc-rio.br/~francisco/nclua/referencia/canvas.html)
  * [Examples](http://www.telemidia.puc-rio.br/~francisco/nclua/tutorial/index.html)
  * Não permite resize, ainda…
  * 

* **Events**

  * Asynchrounous bidirectional communication between NCL formatter and Lua app

  * representado por uma tabela

  * Example: `{class  = 'ncl', type   = 'presentation', action = 'start'}`

  * Podem ser tanto emitidos quando recebidos pelo Lua

    ![image-20201013202643119](Images - TV digital/image-20201013202643119.png)

  * Classes de evento: ncl (tipos presentation, attribution e selection), key (types press, release), tcp, sms, edit, si, user

  * <u>Eventos de SI</u>

    * acesso a um conjunto de informações multiplexadas em um fluxo de transporte e transmitidas periodicamente por difusão.
    * devem ser requisitadas pela postagem de um evento
    * O evento de resposta é gerado após todas as informações requisitadas serem processadas pelo middleware
    * :book: 15606-2 2011
    * type = services ou mosaic ou epg outime (data e hora corrente no horário oficial do local)

  * <u>Eventos de edição</u>
    * This class reproduces the editing commands for the Private Base manager (see Section 9). However, there is an important difference between editing commands coming from DSM-CC stream events (see Section 9), and the editing commands performed by Lua scripts (NCLua objects). The first ones alter not only the NCL document presentation, but also the NCL document specification. That is, in the end of the process a new NCL document is generated incorporating all editing results. On the other hand, editing commands coming from NCLua media objects only alter the NCL document presentation. The original document is preserved during all editing process.
    * Acho que ainda não foi implementada na implementaao de referencia do Telemidia, pelo menos até a versão 1.3

* **Tratadores de eventos**
  
  * recebem um evento como parâmetro
    
  *  deve ser registrados,exemplo:
  
    ```lua
    ...                        -- código de iniciação
    function tratador (evt)
      ...                      -- código de um tratador
    end
    event.register(tratador)   -- registro de pelo menos um tratador
    
    ```

  * `event.timer (time: number, f: function)`
  
  * `event.uptime ()`
  
    * retorna o tempo passado desde o início da execução do NCLua.
  
  * `event.post(evt)`
    * emite evento
    * a função de envio de eventos nunca aguarda o retorno de um valor.
  
* **Settings module**
  
  * oferece acesso às variáveis definidas no objeto settings do
    documento NCL (objeto do tipo “application/x-ncl-settings”).
  * :book: ABNT 15606-2 2018, pg 174
  
* **Modulo persistent**: salva dados para recuperarlo entre execuções

* **Modulo Ginga**

  *  Se um pacote “x” da API do Ginga-J estiver disponível no ambiente Lua, ginga.x manterá uma referência para uma tabela Lua com todas as definições relacionadas a "x" (classes e subpacotes)

* **Restrições**

  * Nem todas as funções da biblioteca padrão de Lua estão disponiveis nas tvs
  * Algumas são dependentes de plataforma, algumas são bloqueadas por segurança, enfim…
  * No módulo package: a função loadlib.
  * No módulo io: todas as funções.
  * No módulo os: as funções clock, execute, exit, getenv, remove, rename, tmpname
    e setlocale.
  * No módulo debug: todas as funções.

* **Useful links**
  
  * [Docs](http://www.telemidia.puc-rio.br/~francisco/nclua/referencia/)

#### Using sockets

* Só suporta TCP
* Usaremos aqui o módulo `tcp.lua` do Telemidia

* Esse módulo faz o controle das chamadas assíncronas, permitindo que a  comunicação seja programada sequencialmente.
* **How it works**
  * Faz um wrapper em torno dos eventos tcp (`evt.class == 'tcp'`)
  * Mantem uma lista global de conexões TCP ativas
  * Utiliza co-rotinas de lua para simular multi-thread
* **Utilização**
  * `tcp.connect`: recebe o endereço e porta de destinos
  * `tcp.send`: recebe uma string com o valor a ser transmitido
  * `tcp.receive`: retorna a string com o valor a ser recebido
  * `tcp.disconnect`: fecha a conexão

### Java

* 100% java

* utiliza API’s do pacote Java-DTV

* o interpretador de GingaJ é atualmente (2020) opcional

* se basa en tres API ́s llamados Verde, Amarillo y Azul

* ==foi descontinuado?==

* ==voltou?==

  ![image-20201207134844834](Images - TV digital/image-20201207134844834.png)

* 

* 

* Atualmente (2020, qual norma??), as TV rodam JavaME vesão ==4?==

* 

* 

* Todas as aplicações Ginga-J devem conter uma classe implementando a interface
  javax.microedition.xlet.Xlet (ver PBP 1.1 2008), que deve ser instanciada de acordo
  com as definições de sinalização de aplicação (ver NBR 15606-3:2007, 12.16).

* metdos do ciclo de vida: initXlet, startXlet, pauseXlet e destroyXlet

* um ou mais arquivos jar
  15606-4:2016, 7.3.5 prevê baixar aplicações

* **Links uteis**

  * docs: http://gingadf.com.br/blogGinga/javaDtvApi/javaDoc_V1.3/com/sun/dtv/ui/package-use.htm
  * minicurso: https://github.com/taylsonmartinez/MiniCursoGingaJ
  * JavaTV basics: https://siteantigo.portaleducacao.com.br/conteudo/artigos/iniciacao-profissional/o-pacote-javatv/45908
  * 
  * http://gingarn.wikidot.com/construindoaplicacoesgingaj
  * 
  * https://www.slideshare.net/mercuriocfg/tv-digital-introduo-ao-middleware-gingagingaj
  * [https://sites.google.com/site/lmsdiego/tutoriais-e-dicas/desenvolvendo_aplicacoes_java_para_tv_digital?tmpl=%2Fsystem%2Fapp%2Ftemplates%2Fprint%2F&showPrintDialog=1](https://sites.google.com/site/lmsdiego/tutoriais-e-dicas/desenvolvendo_aplicacoes_java_para_tv_digital?tmpl=%2Fsystem%2Fapp%2Ftemplates%2Fprint%2F&showPrintDialog=1&authuser=0)

* **Algumas aplicações de exemplo**

  * https://github.com/albertcito/EncuestaGingaJ
  * https://github.com/ygormutti/jsudoku
  * https://github.com/folksilva/agendaTv

#### Fundamentals

* criada pela Sun Microsystems
* Applet: carregados pela Web e executados dentro de um browser
* Midlet: executados dentro de dispositivos móveis
* Serverlet: ?
* Xlets: programas em Java para TV
* JVM
  * interpreta o bytecode
* 
* **Multithreading**
  * wait
    *  interrompe a thread atual, ou seja, coloca a mesma para “dormir” até que uma outra thread use o método “Objec.notify()” no mesmo objeto para “acordá-la”.
  * notify e notifyAll 
* **Exeptions**
  * If you want, you can add throws clauses to your  methods. Then you don't have to catch checked methods right away. That  way, you can catch the exceptions later (perhaps at the same time as  other exceptions)

#### Softwares and IDEs

* **Xletview**
  * Para MHP (europeu)
  * emula um ambiente estático, pois não havia como simular a transmissão de *streams*; não implementate as APIs do Ginga
  * Source: https://sourceforge.net/projects/xletview/files/XleTView/xletview-0.3.6/
  * Tutorial de instalação: http://b4dtv.blogspot.com/2008/02/tutorial-de-instalao-do-java-tv-e.html
* **Emulador Ginga-J**
  * da lavid, baseada na Xletview
  * segue a especificação da JavaDTV
  * comatível com lwuit e gingaJ
  * slides gingaJ emulator: https://www.slideshare.net/erisvaldojunior/gingaj-emulator-uma-ferramenta-de-execuo-de-aplicaes-imperativas-para-o-middleware-ginga
  * Instruções para executar (app Nemo)
    * abra o  emulador, Manage Applications, New Application, escolha um nome  qualquer
    * no Path selecione a pasta build/ da aplicação do Nemo e em Xlet clique nas reticências onde você verá Xlet1.class . 
    * Salve e pronto, a aplicação já pode ser executada no seu emulador.
* **OpenGinga**
  * baseado no XletView
  * Não possui 100% dos componentes visuais do Ginga-J liberados
  * o bridge funciona parcialmente
* **Emulador da PUC**
  * Até 2012 não rodava Java, acho
  * Acho que até hoje (2020) não roda

#### Graphical libraries

* [Excelentes slides sobre ambas as bibliotecas gráficos, com vários exemplos](https://www.slideshare.net/erisvaldojunior/arquitetura-de-apis-grficas-do-java-dtv-lwuit-e-dtvui)

* [Documentation (from gingadf)](http://gingadf.com.br/blogGinga/javaDtvApi/javaDoc_V1.3/com/sun/dtv/ui/package-use.html)

* **JavaTV**

  * bibioteca gráfica da Sun

  * em 2006 esta na versão 1.1

  * largely superseded the AWT's widgets

  * mixing Swing components and basic AWT widgets often resulted in undesired side effects

  * https://siteantigo.portaleducacao.com.br/conteudo/artigos/iniciacao-profissional/o-pacote-javatv/45908

  * <u>planos</u>

    * ABNT 15606-4 8.4.3

    * video: fluxo elementar

    * ==Hscene que representa o local onde será exibido o vídeo==

      ![image-20201204163814370](Images - TV digital/image-20201204163814370.png)

  * <u>events</u>

    * types: numerical keys, arrow keys, colored keys

  * Layout manager

    * ?

      ![image-20201204164723286](Images - TV digital/image-20201204164723286.png)
    
  * <u>ScarceResource</u>

    * só podem ser ouvidos em um lugar?
    * filtros de seções MPEG-2
      (com.sun.dtv.filtering.DataSectionFilterCollection), eventos de entrada do usuário
      (com.sun.dtv.ui.event.UserInputEvent,
       com.sun.dtv.ui.event.KeyEvent,
      com.sun.dtv.ui.event.MouseEvent,
       com.sun.dtv.ui.event.RemoteControlEven),
      dispositivos de rede (com.sun.dtv.net.NetworkDevice), telas (com.sun.dtv.ui.Screen) e
      sintonizador (com.sun.dtv.tuner.Tuner)
    * need to be reversed before usage

* **LWUIT**

  * Lightweight User Interface Toolkit
  * Integra o JavaTV
  * Desenvovida pela Lavid, mantida pela Sun? 
  * abnt15606:4 2016 8.4.1
  * `com.sun.dtv.lwuit`
  * suporta diferentes temas (UIManager)
  * baseada no Swing
  * <u>Event Dispatch Thread</u>
    * single main thread
    * All events and paint
      calls are dispatched using this thread
    * ==?==
  * <u>Container</u>
    * ?
    * cada container tem o seu proprio layout	
    * can be nested 
  * <u>Form</u>
    * raiz para a interface 
    * composto por: title, contentPlane, menuBAr
  * <u>Dialog</u>
    * ?
  * <u>Label</u>
    * texto, image, etc
  * <u>TextArea</u>
  * <u>TextField</u>: ?
  * <u>Button</u>	
    * RadioButton, CheckBox, 
  * <u>TabbedPane</u>
  * <u>List</u>
  
* **Swing**: para elementos gráficos

* AWT

  * Abstract Window Toolkit
  * preceding Swing
  * plataform dependents (looks kinda native)

* **HAVi**
  * Mais antigo que o LWUIT?
  * Específico para TV? 

### Bridge

* aplicações imperativas utilizem serviços disponíveis nas aplicações
  declarativas, e vice-versa.
* A ponte de mão-dupla entre o Ginga-NCL e o Ginga-J é feita:
  * em um sentido, através dos relacionamentos do NCL, definidos nos elementos <link> que se referem aos elementos <media> que representam os códigos Xlet (tipo application/x-ginga-NCLet) suportados pelo Ginga-J; e através dos scripts Lua (elementos <media> do tipo application/x-ginga-NCLua) que referenciam os métodos do Ginga-J pela módulo Ginga
  * no caminho inverso, através das funções do Ginga-J que podem monitorar qualquer evento NCL e também podem comandar alterações em elementos e propriedades NCL, através de relacionamentos definidos em elementos <link> ou através de comandos de edição do NCL.
* ​	
* **importar uma biblioteca**
  * baixar jar dela, importar lea nas bibliotecas do projecto; a veroa do jar deve rodar an versão da JVM usada

### **Frameworks**

* **LuaOnTV**
  * parece abandonada
  * tens uns bugs no codigo que preisam ser corrigidos
  * tão lenta que é inviável ([source](https://softwarepublico.gov.br/social/ginga/historico-de-foruns/linguagem-lua/entrada-de-dados-via-controle))
* **LuaSmartGUI**
  * Bem parecida com QT
  * [download](https://sites.google.com/site/luasmartgui/)
  * [TCC sobre isso](http://dspace.sti.ufcg.edu.br:8080/jspui/handle/riufcg/111)
  * Layout é responsável por posicionar outros componentes dentro dele
  * Widget é um componente visível para usuário e que possuem ações específicas
  * modelagem hierárquica de componentes, onde todo componente possui um pai e alguns podem ter filhos
  * a posição de cada componente na tela é relativa ao seu pai
  * Cada componente possui o seu próprio canvas, que é posicionado dentro de seu pai.
  * Para gerenciar os eventos de controle remoto, foi utilizado o design pattern Observer.
* **luatpl**
  * inserir Lua direto no NCL ao estilo PHP
  * Pelo que entendi, só variáveis
  * https://github.com/robertogerson/luatpl
* **LUAR**
  * template processor
  * uses LUA
  * precisa compilar
  * http://ginga.lavid.ufpb.br/projects/wiki/wiki/_Instala%C3%A7%C3%A3o_do_Luar





# Considerações de telecom

## TV systems architecture

* The codification of the information being sent can be analog or digital

* The analog is quite simple: signal representation (image and sound are signals)

* The digital offers several possibilities, that are better explained in the begining of the file

* **Cable based**

  * mainly coaxial (to end users)

  * <u>Hybried antenna+cable version</u>

    * conceived in the late 1940s
    * Big antenna on top of a hill to pluck the television signal out of the air, an amplifier, called the head end, to strengthen it, and a coaxial cable to deliver it to people's houses
    * Transmission was one way, from the headend to the users

  * <u>Full cable version</u>

    * from 70s

    * a single cable is shared by many houses

      ![image-20200627164129939](Images - TV digital/image-20200627164129939.png)

* **Broadcast based**

  * big antena go go, small antena come come

* **IP based**

  * See more in *IPTV*

## Multiplexação

*  binds information contained in two or more different flows
* **Tipos**
  * síncronos (fracamente acoplados)
  * sincronizados (fortemente acoplados):  assume que os fluxos de dados são
    sincronizados entre si
  * assíncronos (desacoplados): nenhuma marca de tempo
    (timestamp) é associada aos dados

### MPEG2 system

* multiplexation protocol (or we should call it *digital container format*?)

* Estabelece como um ou mais sinais de áudio/vídeo/arquivos devem ser combinados para transmissão

* 

* Lossy video compression, but the compression is great

* The video compression is similar to jpeg

* motion prediction: forward, backward, or bidirectional

* Applies discrite cosine transform, to keep low frequencies?

* applies quantization?

* Entropy encoding? 

* the image (2d) is reshaped into series (1d) in a way that inimizes the entropy of the signal? Goes on zig zag?

* Better quality than AVI

* 

* multiplexar serviços em um fluxo de transporte significa organizar os pacotes dos vários fluxos elementares, pertencentes aos serviços contemplados, em um único fluxo

* programa (ou *serviço*): grupo composto de um ou mais fluxos elementares
  com uma mesma base temporal

* ISO/IEC 13818
  
* An MPEG multiplexed bit stream is either a transport stream or a program stream
  
* The transport stream is more suitable for use in environments where errors are likely.
  
* [Great video (and a whole series on TV systems)](https://www.youtube.com/watch?v=lAkuix41XJ8)
  
  <img src="Images - TV digital/image-20210519111014816.png" alt="image-20210519111014816" style="zoom:60%;" />
  
* **Packet**

  * basic unit of data
  * 188bytes in total (including header)
  * big-endian; 
  * PID: individual identification of each package; 13-bit

  ![image-20210426182946795](Images - TV digital/image-20210426182946795.png)

* **Transport stream**

  * Ou *fluxo de transporte*
  * Contem vários serviços (programas) simultaneamente
  * o bitrate total deve ser constante; Se os programas variam dinamicamente em bitrate, devemos preencher dinamicamente com nulls
  * Single program vs multi program transport stream?
  
* **Program stream**

  * é formado por um ou mais fluxos elementares

* **Elementary Stream**
  * ou *fluxo elementar*
  
  * fluxo de dados gerado pela codificação do conteúdo de vídeo, áudio ou outros dados
  
  * Todo fluxo elementar deve ser quebrado em pacotes de forma a poderem
    ser multiplexados no fluxo de transporte
    
  * é a mesma coisa que Packetized Elementary Streams (PES)??
    
    ![image-20200907184325677](Images - TV digital/image-20200907184325677.png)
  
* **Buquê**

  * conjunto de serviços que estão agrupados logicamente, sem ter em conta de que fluxo de transporte este conjunto faz parte
  * Existe tanto na ISDBT quanto no DVB
  * para agrupar serviços como canais esportivos, canais de notícias, canais de filmes, etc
  
* **Seções privadas**

  * ?
  
* **eventos de sincronismo**

  * ?
  * serão iterpretados pelo middleware (Ginga)
  * 
  * Objetos de eventos de fluxo: ?; possui um ID
  * Descritores de eventos de fluxo: ?
  * Descritores de eventos são transportados em fluxos listados na tabela
    PMT com o tipo igual a 0x0C
  
* **Conditional Access System**

  * Described in the CAT signalling table
  * The user can only decode the parts of the TS that he is allowed
  * The content is always encripted? 



#### Signalng tables

* ==chamamos tudo de “SI”? Ou apenas algumas dessas tabelas são chamadas de SI?==

* Para transmissão, as tabelas são dividida em um certo número de pacotes chamados secções. 

* Cada uma das tabelas é transmitida repetidamente a uma taxa determinada, a fim de se certi-
  ficar que o receptor recebeu a totalidade da tabela
  
* Agumas tabelas possuem PID prefixados:

  ![image-20210426183444184](Images - TV digital/image-20210426183444184.png)

* **Program-specific information (PSI)**

  * metadata about a program (channel) and part of an MPEG transport stream
  * Sobre los streams elementales que están contenidos dentro de los TSs
  * transported in private structures
  * Referencia: ISO/IEC_13818-1 (2015)
  * <u>PAT (Program Association Table)</u>: contém identificadores dos fluxos elementares contendo as outras tabelas; Para descobrir que programas estão contidos em um determinado fluxo; Cada entrada na PAT dá um PID que contém uma tabela de mapeamento de programas (PMT) (Tabela 3.3) para esse programa
  * <u>PMT (Program Mapping Table)</u>: lista de identificadores dos fluxos elementares que compõem um serviço/programa; there is one PMT per program
  * <u>CAT (Conditional Access Table)</u>: management of the cypher keys used for decryption of restricted streams

* **Extenções de SI do ISDBT**

  * complementar to PSI
  * contains aditional information
  * Quais são as tabelas mínimas obrigatórias para um sinal ISDBT? PAT, PMT, SDT e NIT? Ver ABNT NBR 15603-2 tabela 6.
  * 
  * Referencia: ABNT_NBR-15603-1 (2007) e ETSI_EN-300-468 (2014)
  * <u>NIT (Network Information Table)</u>: dá ao receptor as referências para identificar quais fluxos de transporte fazem parte de uma mesma rede e como ele pode ter acesso a eles; Especificação dos campos original_network_id, network_id e service_id: ver ABNT NBR 15603-2 Anexo H.
  * <u>SDT (service description table)</u>: 
  * <u>BAT</u>: 
  * <u>EIT (event information table)</u>: contains program names, start times, durations, and so on; Carries the EPG (Electronic Program Guide)
  * <u>RST</u>: 
  * TDT (time date table): embeds a UTC time and date stamp in the transport stream
  * <u>TOT</u>: 
  * <u>AIT</u>
    * Configura a aplicação ginga
    * fornece informações completas sobre a transmissão de dados, o estado de ativação necessário de aplicações realizadas por ele.
    * permite a inserção de aplicativos, tanto para o Ginga como para HbbTV
    * prioridade das aplicações: ?; determinada pelo fornecedor de serviços.
    * Campos
      * initial_class
      * class_path_extension 
      * base_directory
      * application_name
      * application_type

#### **Formatos de arquivos**

* Transport streams differ from the similarly-named [MPEG program stream](https://en.wikipedia.org/wiki/MPEG_program_stream) in several important ways: program streams are designed for reasonably reliable media, such as discs (like [DVDs](https://en.wikipedia.org/wiki/DVD)), while transport streams are designed for less [reliable](https://en.wikipedia.org/wiki/Reliability_(computer_networking)) transmission, namely [terrestrial](https://en.wikipedia.org/wiki/Terrestrial_television) or [satellite broadcast](https://en.wikipedia.org/wiki/Satellite_television). Further, a transport stream may carry multiple programs.
* *Transport stream* file formats include [M2TS](https://en.wikipedia.org/wiki/M2TS), which is used on [Blu-ray](https://en.wikipedia.org/wiki/Blu-ray) discs, [AVCHD](https://en.wikipedia.org/wiki/AVCHD) on re-writable DVDs and [HDV](https://en.wikipedia.org/wiki/HDV) on [compact flash](https://en.wikipedia.org/wiki/Compact_flash) cards. *Program stream* files include [VOB](https://en.wikipedia.org/wiki/VOB) on [DVDs](https://en.wikipedia.org/wiki/DVD) and [Enhanced VOB](https://en.wikipedia.org/wiki/Enhanced_VOB) on the short lived [HD DVD](https://en.wikipedia.org/wiki/HD_DVD).
* <u>ES</u> (Elementary Streams): Vídeo, áudio ou closed caption gerado pelo encoder; Formato depende do codec (h.264, etc)
* <u>PES</u> (Packetized Elementary Streams): Especificação para empacotamento de ES; especificados no MPEG-2, Parte 1.
* <u>TS</u>
  * vídeo multiplexado com outros streams
  * um ou mais programas de dados codificados 
  * Transport streams can be read from and written to binary files, called TS files.
  * A standard TS file must contain contiguous 188-byte TS packets without any encapsulation
  * SPTS: single program TS (ffmpeg can do that)
  * MPTS: multi program TS
  * BTS: ?

#### SCTE-35

* event signaling in transport stream
* inserir marcações/mensaguens no video
* in-stream messaging mechanism to signal splicing and insertion opportunities
* frame accurate or non-frame accurate
* 
* Streming manifest: describe the chunks of videos
* [githu awesome](https://github.com/leandromoreira/scte-35-scte-104-scte-67)
* **Main usages**
  * Ad insertion
  * Alternate content replacement
  * EPG (live schedule & metadata) synchronization
  * Content identification
  * <u>Dynamic Ad Insertion (DAI)</u>
    * technology that allows advertisers to serve different ads to each viewer
    * Server-Side Ad-Inserter (SSAI):  take the original streaming manifest and modify it for each viewer
    * ad avail events: provide information about advertisement availability
* **splice events**
  * such as a network Break or return from a network Break
  * pertains to a given program
  * it’s PID is referred to by the PMT (see Signaling Tables)
  * Splice information table: for notifying downstream devices of splice events
* **commands**
  * splice_schedule
  * splice_insert: ?; It is also possible to signal a splice event without specifying the  time. When splice_immediate_flag is 1, a splice event is triggered at  the earliest possible timing. This, however, causes various headaches in practice.
  * splice_info_section
* **SCTE Specific tables**:
  * ==?==
  * 0xCO table id is used by Programme Information Message
  * 0xC1 table id is used by Programme Name Message
* **SCTE ecosystem**
  * [SCTE 30] is used to support splicing of advertising into live QAM MPEG-2 transport streams
  * [SCTE 130-3] is used to support alternate content decisions (advertising, blackouts, stream switching) for live and time
    shifted delivery
  * [SCTE 214-1] defines how SCTE 35 is carried in MPEG-DASH. 
  * [SCTE 224] (ESNI) is used to pass event and policy infor

#### **DSM-CC**

* Digital Storage Media - Command and Control
* to broadcast files over transport streams
* broadcast file system protocol
* Standard for developing control channels associated with MPEG-1 and MPEG-2 streams
* Defined in Part 6 of the [MPEG-2 Standard](https://www.linuxtv.org/wiki/index.php/MPEG-2_Standard)
* 
* carossel é a sua forma mais simples de transmissão
* Comparado com outros protocolos de download, DSM-CC foi projetado para operações rápidas e leves,
* (+) aplicado em redes broadcast sem canais de upstream
* ![Generating a transport stream](Images - TV digital/ts_creation.gif)
* [Interesting tutorial](https://www.tvwithoutborders.com/tutorials/getting-started/mhp-ocap-application-development-getting-started/generating-a-transport-stream/)
* [Other excelent tutorial (long)](https://www.tvwithoutborders.com/tutorials/dtv_intro/how-to-become-an-expert-in-dsm-cc/)
* timecode (called Normal Play Time, or NPT for short): ???
* **event stream**
  * For instance, stream events could be used to indicate the start of a new TV show – using stream events means that the broadcaster doesn’t have   to tell any applications that new shows start at NPT values X, Y and Z – instead, they can broadcast a ‘new show’ stream event and let the  device receiving the stream take care of the rest.

#### Data caroussel

* Or *data/object carousel*

* protocolo de transmissão cíclica de dados

* protocolo de download para transmissões broadcast

* Camada de transporte

* Objetivo: Multiplexação de conteúdo (Program allocation)

* Motivação: Forma de lidar com personalização em transmissão broadcast

* Ideia básica: Arquivos de dados devem ser periodicamente transmitidos pelo provedor

* Baseado no standard DSM-CC (carossel é a sua forma mais simples de transmissão)

* mais de um carrossel pode ser transmitido simultaneamente

* Compartilha: video principal, aplicações, arquivos

  ![image-20200908202518985](Images - TV digital/image-20200908202518985.png)

![image-20200904224839320](Images - TV digital/image-20200904224839320.png)




* DSM-CC supports two kinds of carousel: data and objects

* Data caroussel: simply send data, without information about it’s context

* object carousel: provides functionality close to a standard filesystem

* **Modules**

  * The filesystem will be sent in chuncks (modules), which will be sent over and over again

  * Cada módulo pode, por sua vez, ser quebrado em blocos

  * Each block will have a DDB header (DownloadDataBlock); contém module ID, version and block number

  * DownloadInfoIndication (DII)?

  * You can broadcast some modules more often than others… but that seams hard

  * For the receiver to access a file it must wait until it receives the  module containing that file, at which point it can parse the module and  access the file.

  * Example:

    ![Putting files into modules in a DSM-CC object carousel](Images - TV digital/dsmcc_modules.gif)

* **Considerações sobre multiplexação**

  * Cada seção DSM-CC de um carrossel é transmitida no fluxo de transporte MPEG, como
    um fluxo elementar de dados, uma após a outra.
  * O vídeo e o áudio são entregues aos
    codificadores digitais, responsáveis pela geração dos respectivos fluxos de
    vídeo principal e áudio principal comprimidos. Esses fluxos, mais os fluxos
    dos outros dados, são então multiplexados em um único sinal, denominado
    fluxo de transporte 
  * Carrosséis de dados e de
    objetos são transportados em seções privadas específicas MPEG-2
  * aplicações transferidas
    em arquivos de um carrossel podem fazer referência a recursos presentes no
    mesmo carrossel ou a recursos de outros carrosséis

* **Considerações sobre a taxa de transmissão**

  * Bandwidth é fixo e limitado
  * A quantidade de conteúdo a ser transmitido é variável
  * Quanto mais arquivos estiverem presentes no carrossel, mais demorado é o acesso a cada arquivo

#### Transport Stream Over IP

* Or *TSoIP*
* Usually over UDP
* can only be unicast?
* **hardwares that do that**
  * https://www.enensys.com/products/gigacasterii/; you have to use one both to sen
* **Common problems**
  * Jitter
  * Packet loss
  * https://www.thebroadcastbridge.com/content/entry/12382/the-challenges-of-sending-ip-video-over-a-wan




























































## Modulação

* responsável por receber um fluxo de transporte e posicioná-lo em um canal de frequência
* ==Diferença entre  codificador de canal e modulador???==
* **portadora única**
  *  os símbolos digitais são transmitidos serialmente
* **múltiplas portadoras**
  * dividido em um grande número de canais com baixa taxa de
    bits
  * desempenho muito melhor frente ao ruído impulsivo e múltiplos percursos (por podermos aumentar a base de tempo)
  * variação da multiplexação por divisão de frequência (FDM)
* **Channel coding**
  * or *forward error correction*
  * ==Should this be included under odulation, or have its own section?==
  * happens before the modulation
  * To make the transmission more resilient to noise
  * <u>Channel coding steps</u>
    * External Coder
    * Energy Dispersal
    * Byte Interleaver
    * Inner Coder (convolutional)
    * Bit Interleaver
    * Combiner and Interleaver (Time Interleaver and Frequency Interleaver)
* **Modulation Steps**
  * Mapper and Spectrum Adjust
  * OFDM Frame Adpation
  * IFFT and Cyclic Prefix
* **about OFDM**
  * The TMCC (Transmission Multiplexing Configuration Control) is send in the OFDM frame, with information about the parameters
  * ISDB-T defined the Broadcast Transport Stream (BTS), which is
    created in the TS Remux block by combining up to three
    different TS inputs.; Hierarquical layers?
  * point is that if the BTS changes
    its original packets’ order

## Outras anotações!

* Integrated Broadcast Broadband (IBB), é o padrão internacional da
  ITU, que nasceu da proposta de sistemas de interatividade para TV digital.
* Um TS consta de um ou mais pro-
  gramas, formados a partir de pacotes PES, e estes pacotes transportam os dados
  dos ES, fluxos elementares dos codificadores de áudio e vídeo.
* Os pacotes TS de um valor PID vão transportar dados de um e somente um ES.
* Os Fluxos de Transporte contêm dois tipos diferentes de SI. O primeiro está
  definido pelo padrão MPEG, chamado informação de programa específico, que o
  receptor utiliza para buscar o fluxo a se decodificar. O segundo tipo de informação
  de serviço está definido por um organismo de normalização diferente, que pode ser
  o SBTVD que é chamado de Sistemas de Informação ou do tipo DVB denominado
  como DVB Service Information (DVB SI).



# SDR e rádio amador

* Use of digital signal processing to generate radio signals

* utilizam um sintonizador mais ou menos como o tradicional, que entrega os sinais captados a um ADC de alta velocidade. Mas, o processo de filtragem, equalização, demodulação, decodificação, são predominantemente realizados por processamento digital.

* As the mains tasks are performed by software on a very general hardware platform, the project itself may become almost independent of the hardware part and can be easily modified to fulfil the
  functions of different communication systems.

* Radio is usually processed ith FPGA, ASIC, DSP, etc

* Tutorial focused on mathematical fundamentals: https://www.youtube.com/watch?v=TZmHgIPBLDo&list=PLVQhg1UYyzxUcv-bZnxOOA6pkoAB021d9

* (+) possibilidade de trabalhar com uma ampla
  faixa de radiofrequência e no qual as funções mais
  críticas de processamento são realizadas em software
  ao invés de hardware dedicado.

* Arquitetura genérica de um SDR:

  ![image-20200921175106168](Images - TV digital/image-20200921175106168.png)

* **Software Communications Architecture (SCA)**

  * padrão criado por militares dos EUA
  * Paga e fechada
  * ferramentas baseadas nela permitem a criação de
    projetos como uma ligação entre componentes
  * Common Object Request Broker Architecture (CORBA): protocolo de comunicação interno

* **Open Source SCA Implementation Embedded (OSSIE)**

  * developed by xthe Virginia Tech University
  * Versão OpenSource da SCA

## Multimidia manipulation

* ==keep it here, or what?==
* ==Vocoders, video compression, Linear Prediction Coding Vocoders ???==
* [workshop/book about digital video technologies](https://github.com/leandromoreira/digital_video_introduction)
* Compressão
  * com perdas: quase igual, mas o sinal origina é irrecuperável
  * sem perdas: conseguimos recuperar exatamente o sinal original
* **codecs**
  * geralmente trabalham no domínio da frequência
  * <u>audio</u>
    * geralmente dividem a frequência em bandas, usando ou Fourier ou Cossine
    * efeitos de mascaramento (ver em *sistema auditivo*) são aproveitados para não codificar sons que não seriam percebidos
    * [Comparison of audio coding formats](https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats)
    * codecs preditivos: um algoritmo acordado prevê as amostras futuras, e apenas a diferença (erro) é transmiti
  * <u>audio - específicos para voz</u>
    * geralmente a taxa de quantização de bits não é uniforme com a amplitude, seguindo ou a u-law ou A-law; smaller amplitudes are more frequent than high amplitudes; a forma mais fáci de implementar é primeiro “linearizar” o audio e depois aplicar uma quantização uniforme
    * vocoder: linear prediction voice coder; generates voice?
  * <u>video</u>
    * pixels change very little from frame to frame, and we can transmit only the differen
  * <u>openMax</u>
    * set of opensource codec specifications?
    * [Project - transcoder for the Raspberry Pi](https://github.com/dickontoo/omxtx)
* 
* 
* 
* 
* **VLC**
  * Player
  * can generate TS from MP4
  * can play: TS, udp (not always), etc
* **mplayer**
  * Player
  * can play: TS, udp, etc
* **Opera HbbTV Emulator**
  * emulador que permite visualizar aplicações HbbTV via HTTP ou aplicações armazenadas no disco rígido
* **ProgDVB**
  * Player
  * For watching digital TV and listening to radio channels.
* **Camtasia**
  * Easy to change resolution
* **Gstreamer**
  * it’s last version seams to support DSM-CC
  * See details about this software in another section
  * [gstreamer openCV pipeline example](https://www.youtube.com/watch?v=fWhPV_IqDy0)
* **avconv**
  * Generates a MPEG transport stream in real time, from webcam
  * https://libav.org/avconv.html
* **OBS**
  * can I capture stream? In which formats? 
  * Can I only broadcast via udp? 
  * Can I send direcly to a hard like UT-100 (USB donge modulator)?

## TS manipulation

* A manipulação dos streams geralmente está diretamente relacionada à aplicação (ex: TV digital)

* **Opencaster**
  * MPEG2 transport stream data generator 
  
  *  oferece uma série de ferramentas que são utilizadas
    para a geração, processamento, emissão e difusão de conteúdos transport stream.
    
  * CLI
  
  * Open source
  
  * [main page](http://www.avalpa.com/the-key-values/15-free-software/33-opencaster)
  
  * [User manual](http://www.avalpa.com/assets/freesoft/opencaster/AvalpaBroadcastServerUserManual-v3.0.pdf)
  
  * [deb package](http://ftp.de.debian.org/debian/pool/main/o/opencaster/)
  
  * [video about it](https://rmll.ubicast.tv/videos/open-source-interactive-television-in-italy-and-europe-too_/)
  
  * <u>History</u>
    * Initially developed by Cineca group (italia) ([contact info](https://www.cineca.it/en)), Initally named *JustDVb-It*
    * Now developed by Avalpa
    * na versão 2.4 a LIFIA fez modificaçẽos para adicionar suporte à SBTVD
    * Supports SBTVD since version 2.5 (não sei se fruto das modificações da LIFIA). Earlier versions can be adapted using the moditications from LIFIA lab
    * Eles vendem diversos produtos, incluindo o Avalpa Broadcast Server (um equipamento tipo playout, que usa o opencaster)
    
  * <u>carousel manipulation</u>
    * a directory has to be marshalled into
      a transport stream and the transport stream file need to be recreated every time the
      directory change
    * `oc-update.sh`: ferramenta para gerar carrosséis de dados 
    
  * <u>Signaling tables</u>
  
    * Each one come as a TS
    * the bitrates are specified
  
  * <u>principais comandos</u>
    * `esaudio2pes`, permite encapsular os ES de aúdio em PES,
    * `pesaudio2t`s, permite transformar o pacote PES de áudio em TS,
    * `tscbrmuxer`, encarregado da multiplexação dos arquivos sejam de áudio, vídeo, dados, num só TS,
    * `oc-update.sh`, a ferramenta permite a criação de um TS utilizando um carrossel de dados.
    *  
    * `tscbrmuxer`: junta vários ts em um; já faz em loop?
    * `tsloop`: repete um ts e joga em um ts fifo
    * `sec2ts`: encapsulate sections into ts packets; allows to define the PID
    * 
    * `tsudpsend`: manda um ts fifo por udp
    * `tstcpsend `
    * `tsrfsend`: manda um ts fifo por RF
    * `tsasisend`: manda um ts fifo por ASI
    * 
    * `tstcpreceive 7001 > received.ts`:  waits for an incoming connection on port 7001
    * 
    * `tsstamp`: pega um ts fifo e transforma em outro ts fifo, aplicando algumas correções de tempo; corrige  instabilidades após a multiplexação, evitando algum tipo de atraso
    *  
    *  
    *  
    
  * <u>features</u>
    * PSI generation (PAT, PMT, NIT, SDT, TDT, AIT, TOT, ...)
    - EPG/EIT generation
    - Object and data carousel generation and receivement (MHP, MHEG5, HbbTv, DVB-SSU, DSMCC, ...)
    - third party software AC-3, MPEG2 audio, MPEG2 video and H264 video encoders support
    - Audio Description generation support
    - IP encapsulation (MPE)
    - CBR transport stream muxing
    - PCR stamping
    - Null packet replacement
    - PID filtering
    - PID remapping
    - Null packet insertion
    - Video buffer verifier
    - PCR timing verifier
    - PES headers analyzer
    - MPEG2 video and MPEG2/AC-3 audio ES header analyzer
    - M2TS file generation from TS file
  
* **TMM**
  * Developed by Telemidia
  * Focused on ISDB-T
  * Open source
  * https://github.com/TeleMidia/tmm
  * Segundo o Alan, suporta stream events DSM-CC
  * Dica: use com uma placa IP-to-ASI ([exemple](https://www.dektec.com/products/IP/DTE-3100/)) para fazer transmissões simples com aplicações
  * entradas: arquivo TS
  * saídas: arquivo, udp
  
* **dsmcc-mhp-tools**
  * Presentation page: https://linuxtv.org/dsmcc-mhp-tools.php
  * Files page: https://www.linuxtv.org/cgi-bin/viewvc.cgi/dsmcc-mhp-tools/
  * from LinuxTV project
  * collection of utilities for generating MPEG2 elementary streams that include DSM-CC MHP ObjectCarousels
  * Acho que é compatível com SBTVD ([fonte](https://softwarepublico.gov.br/social/ginga/historico-de-foruns/tv-digital-em-geral/carrossel-de-dados))
  
* **Dtv utils**

  * collection of utilities and scripts for digital television projects using bladeRF and GNU Radio
  * https://github.com/drmpeg/dtv-utils

* **S and T**
  
  * http://www.s-and-t.co.uk/products/index.html
  * open?
  
* **TS duck**

  * General-Purpose Toolbox for Digital TV
  * includes an analyser
  * all TSDuck commands can manipulate tables in binary or XML  format equally
  *  *tsp*, the transport stream processor.  It is a flexible transport stream processing framework.  Running *tsp* is a combination of elementary processing operations using plugins.
  *  *tstabcomp* is a table compiler.

* 

* 

* 

* 

* 

* 

* [DVBlast](https://www.videolan.org/projects/dvblast.html) - A simple and powerful MPEG-2/TS demux and streaming application.

* [dvbshout](https://github.com/njh/dvbshout) - Tool to send DVB audio to a shoutcast server or a RTP stream.

* [Project X](https://sourceforge.net/projects/project-x/) - DVB demux tool.

* [ts2mpa](https://github.com/njh/ts2mpa) - Simple tool to extract MPEG Audio from a MPEG Transport Stream (TS).

### TS analysers

* [DVB Inspector](https://sourceforge.net/projects/dvbinspector/) - An open-source DVB analyzer.
* **MPEG-2 TS packet analyser**
* **TS Reader**
* **Randon github repo**

  * https://github.com/darrenstarr/DVBTools
* **SBTVD parser**
  * roda em linux
  * http://sbtvdparser.sourceforge.net/
* **DVB inspector**
  * Funcioa bem pakas, inclusive no sistema brasileiro
  * https://github.com/EricBerendsen/dvbinspector
* **dvbsnoop**

  * MPEG stream analyzer program
  * Tutorial on how to use it with OpenCaster in page 27 of the OpenCaster manual
  * CLI
  * http://dvbsnoop.sourceforge.net/
  * É um programa analisador de fluxos DVB/MPEG
  * depuração das informações enviadas via satélite, cabo ou terrestre do fluxo em um formato legível.
  * para MPEG, DVB, DSM-CC, MHP, além de analisar fluxos MPEG
  * tipos de fluxos possíveis são os TS, PS, PES ou SECTIONS
  * Opensource
* **MediaInfo**
  * Opensource
  * permite identificar dados correspondentes ao áudio e vídeo composto no TS, os seus tipos de extensão,
    e referências diversas de um modo geral
  * 

## File/signal manipulations

* **GNU radio**
  * open-source
  * for c++ and python
  * <u>GNU radio companion</u>
    * GUI for GNU radio
    * open-source
    * flow-graph based
    * `.grc` extension
    * similar to Simulink or LabVIEW
  * <u>Source</u>
    * in
  * <u>Sink</u>
    * Out
    * file: export a file… in whihc ofrmat?
  * [Blocks for interfacing with various radio hardware](https://github.com/hennichodernich/gr-osmosdr) (including OsmoFL2K, bladeRF and hackRF)
  * PyBOMBS: ease installation of everyting? “package manager” for gnuradio
  * <u>OS image: Pen2Linux</u>
    * linux
    * already have installed GNR radio and several others stuffs like that
    * makes easy to experiment with SRD
  * <u>OS image: pentoo</u>
    * Linux
    * Full support for HackRF and GNU Radio.
    * Download the latest Pentoo .iso image from one of the mirrors listed  at http://pentoo.ch/downloads/
  * <u>filters</u>
    * taps: the weights of each delay
    * `from gnuradio.filter import firdes`: fir design
* **SDR-Radio**
* **QTRadio**
* **SDR#**
  * very easy to use
  * just windows (there is a “server” for linux?)
* **vuniversal radio hacker**
  * excelent tools for demodulation, interpretation, etc for communications
* **inspectrum**
  * https://github.com/miek/inspectrum
  * inspectrum is a tool for analysing captured signals,
* **gr-dvbt**
  * implementação de DVB em GNU radio
  * https://github.com/BogdanDIA/gr-dvbt
  * https://github.com/drmpeg/gr-dvbt2
  * ==supports carousel manipulation? I dont think so==
* **gr-isdbt**
  * implementação de ISDB-T em GNU radio
  * https://github.com/git-artes/gr-isdbt
  * ==supports carousel manipulation? I dont think so==
* **gr-osmo**
  * CLI: `osmocom_fft` open a gnuradio-like interface for the fft scope



## Interface AD

* Vou incluir nessa seção as interfaces analógicas para SRD, as gambiarras, e  os moduladores por hardware (que NÃO são para SDR, mas é conveniente registrar junto)

* [Comparrison between hackRF, bladeRF and USRP](http://www.taylorkillian.com/2013/08/sdr-showdown-hackrf-vs-bladerf-vs-usrp.html)

*  The FPGAs act first as an intermediary between the ADC/DAC and the FX3 controller but also provides DSP and logic resources that can apply digital filters, basebands, etc

* **HackRF**

  * connects via USB, transmit to several frequencies
  * Transceiver 
  * Half duplex
  * [Great course (more about DSP and signal processing than hackRF itself)](https://www.youtube.com/watch?v=0csVDUKod9U&list=PL75kaTo_bJqmw0wJYw3Jw5_4MWBd-32IG&index=10)
  * [To buy](https://greatscottgadgets.com/hackrf/one/)
  * [His initial Kickstarter campaign](http://www.kickstarter.com/projects/mossmann/hackrf-an-open-source-sdr-platform)
  * Produced by Great Scott Gadgets
  * CPLD on the board
  * open source hardware
  * Driver to GNU Radio are provided in [gr-osmosdr](https://github.com/hennichodernich/gr-osmosdr)
  * 8 bit DAC
  * Frequency: 1MHz to 6GHz
  * Bandwidth: 20MHz
  * ~US\$300
  * [Tutorial: Analisador de Espectro com HackRF](https://www.fernandok.com/2019/12/analisador-de-espectro-de-baixo-custo.html)
  * CLI: `hackrf_transfer`: maniplares raw data
  * CLI: `hackrf_info`
  * 2 bytes per sample
  * why bandwidth == samplerate? yes, always; remember nyquist theorem; you can have an smaller bandiwidth by ==filtering out the other frequencies== (hackRF have implemented in hardware? ), that have the advantage of ==reducing the aliasing?==; for the same sample rate, the smaller the bandwidth the better?
  * be careful if connecting two hackrf directly: the transmitting power is higher than the maximum safe receving power; use an attenuator with something like 20db
  * 
  * <u>tunnings</u>
    * 
    * you can set for a active antenna?
    * RF gain:  amplifier gain (pre gain) stage; usually, dont activate
    * IF gain?; when transmitting, that is the actual transmitting gain
    * baseband gain? only for receiving
  * <u>other annotations</u>
    * the spike at the center is because of the DC offset

* **BladeRF**

  * 47mHz to 6GHz
  * Produced by nuand
  * Transceiver 
  * Full duplex
  * onboard programmable FPGA
  * Driver to GNU Radio are provided in [gr-osmosdr](https://github.com/hennichodernich/gr-osmosdr)?
  * [To buy](https://www.nuand.com/)
  * ==openosurce? or just the FPGA core?==
  * connect to computer by USB
  * ~US\$480
  
* **Universal Software Radio Peripheral (USRP)**
  
  * hardware family
  
  * Produced by National Instruments 
  
  * Transceiver 
  
  * Full duplex
  
  * many of the products are open source hardware
  
  * all USRP products are controlled with the open source UHD driver, which is free and open source software
  
  * Receive from computer by USB
  
  *  https://www.ni.com/pt-br/shop/hardware/products/usrp-software-defined-radio-device.html
  
  *  [Slides about using USRP togueder with GNU radio](https://pt.slideshare.net/csete/gnu-radio-and-the-universal-software-radio-peripheral-4889786)
  
  *  ~US\$ 1,285.00 (the cheaper one)
  
  *  offers the possibility of using 3 different
     ways for reference signals that are internal generation, external
     generation, or a GPS module that may be attached to the board.
  
  *  integrates
     digital-to-analog converters (DAC) and analog-to-digital
     converters (ADC), RF front end, and an FPGA (Cypress FX2)
  
  *  programmable gain amplifier
  
  *  Alredy have package on GNU radio
  
     ![Implementations of Coherent Software-Defined Dual-Polarized Radars](Images - TV digital/images)
  
  *  <u>Modelo B100</u>
  
     * Require daughter boards
     * Different daughterboards are used to access different portions of the spectrum
  
  *  <u>Modelo B210</u>
  
     * single board solution, rather than a  mother/daughterboard combination
     * new duaghterboards can be swapped in
     * RF range from 70 MHz to 6 GHz
     * 12 bits ADC and DAC converters
     * RF bandwidth up to 56 MHz,
     * maximum sampling rate of 61.44 MS/s,
     * [Documentation](https://github.com/EttusResearch/uhd/blob/d251a7a55b198aa61ae05809c2e601a04fcb9b97/host/docs/usrp_b200.rst)
  
* **Osmo-FL2K**

  * Transmitter only
  * is a software hack that allows cheap $5-$15 USB 3.0 to VGA adapters to be used as a transmit-only capable SDR. 
  * It is an excellent compliment to the RTL-SDR (receiver?)
  * Amplification: by connecting an antenna-like cable to the red pin (Pin 1 of the three-row 15-pin VGA connector)
  * standalone osftware available at http://git.osmocom.org/osmo-fl2k
  * Driver to GNU Radio are provided in [gr-osmosdr](https://github.com/hennichodernich/gr-osmosdr)
  * ~US$15
  * [Tutorial - SETTING UP AND TESTING OSMO-FL2K](https://www.rtl-sdr.com/setting-up-and-testing-osmo-fl2k/)
  * você provavlemente vai precisar de outros conversores para conectar a antena, por exemplo: VGA-to-composite, VGA-to-BNC + BNC-to-coaxial, etc

* **Adam Pluto**

  * Transceiver 
  * full duplex
  * Frequency: max 3.8GHz

* **UT-100 USB DVB-T Modulator Adaptor**

  * USB stick
  * Have a modulator card
  * Driver instalation is quite straightforward because the driver and SDK are included in the package
  * no host CPU computation required
  * Direct digital conversion to 50~950MHz and 1200~1350 MHz
  * Bandwidth 2~8Mhz
  * Output: RF 75ohm
  * [Usage tutorial](https://oz9aec.net/technology/dvb/using-the-ut-100-dvb-t-modulator-on-linux)
  * [Optional driver, more flexible](https://github.com/linuxstb/it9507)
  * <u>USB OpenCaster bundle</u>
    * OpenCaster itself it’s free software, but they sell this USB
    * contains an UT-100C and a special edition of OpenCaster software
    * Includes the tsrfsend software, with I didnt found online
    * include examples
    * ~US$200
    * [To sell](https://hides.en.taiwantrade.com/product/opencaster-special-edition-tx-only-1562581.html)

* **MyriadRF**

  * Very similar to BladeRF
  * Little bit cheaper
  * does not directly connect to a computer
  * http://myriadrf.org/

*  **PCI based modulators**

  * Are usually for technical people
  * Are usually expensivier than the off the shelf, even without case/connectors/all, because are better
  * [Dektec Dta-111](https://produto.mercadolivre.com.br/MLB-969802019-dektec-dta-111-pci-com-nf-_JM)
  * [DekTec DTA-2111](https://www.dvbgear.com/modulators/pci_based/dta-2111_multi-standard_vhf_uhf_modulator_for_pcie/) (US\$ 1000)
  * [DekTec DTA-115](https://www.dvbgear.com/modulators/pci_based/dta-115-sp_multi_standard_PCI_modulator/) (US\$ 2000)
  * [List of PCI cards for DVB-T](https://www.linuxtv.org/wiki/index.php/DVB-T_PCI_cards)

* **Off-the-shelf modulatores**

  * geralmente entrada HDMI e saída RF
  * As vezes possuem entrada RF para misturar sinais
  * As vezes possuem várias saída RF, para distribuir o sinal (muito usado em hoteis)
  * https://www.orbitadigital.com/pt/video-son/moduladores/digital/2278-johansson-8201-modulador-hdmi-a-dvb-t-y-isdb-t.html
  * https://eletronicos.mercadolivre.com.br/modulador-isdb-t
  * https://pt.aliexpress.com/item/33038228137.html
  * https://pt.aliexpress.com/store/group/SA-ISDB-T/4657011_514484788.html
  * https://www.proatec-ltda.com/shop
  * https://telesystembrasil.com.br/produto/encoder-moduladormodelo-isdb-t/
  * https://www.bhphotovideo.com/c/product/1502659-REG/thor_h_hdmi_rf_petit_petit_hdmi_rf_modulator.html
  * https://www.showcabos.com.br/p-10185149-Conversor-e-Modulador-HDMI_HD-para-RF---ISDB
  * [Loja - dvbgear](https://www.dvbgear.com/)
  
* **ubertooth one**

  * para bluetooh
  
* **RTL-SDR**
  * receiver only
  * SMA to USB
  * [have a fucking interesting image](https://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/)
  * [Review of several dongles](https://hackaday.com/2017/09/05/19-rtl-sdr-dongles-reviewed/)
  * RTL2832
    * ?



## Applications and tutorials

* [playlist with several applications](https://www.youtube.com/playlist?list=PLxyM2a_cfnzgv30vrsxayCxWWoKAFGgEo)

* [presentation at black hat about SDR](https://www.youtube.com/watch?v=N0p3_ES2dBU)

* GPS Spoofing (seding fake signals): https://www.youtube.com/watch?v=3NWn5cQM7q4

* **Replay Attack**

  * receive a signal and then repeat it
  * door bell): https://www.youtube.com/watch?v=uIVBVd6yi_A
  * Car: https://www.youtube.com/watch?v=-uVHLX0RmN8
  * car: https://www.youtube.com/watch?v=5CsD8I396wo
  * Car and doors, I think it’s the author of rolljam: https://www.youtube.com/watch?v=1RipwqJG50c
  * if the code/key is static, just replay
  * if the code/key is rolling, you must record jsut the sender (without the receiver receiivning) and then replay
  * rolljam atack: agais rolling windows; emmit jamming signal while the code is being sent; beucase of the jamming, it wont work; after stop de jaming (or there are ways f doing at the same time), yo
  * <u>general tips</u>
    * the center frequency must be the same as when recorded; idem sample_rate
    * you may need to amplify, maybe just even is software (multiplication)
    * it’s goo to use a bandpass filter when transmitting, just with the signal you intedted
    * to be even better, you can demodulate and rege

* **protocol reverse engineering**

  * Termometer: https://hackaday.io/project/4690-reverse-engineering-the-maverick-et-732
  * Some basic questions we’ll need to answer: Frequency; Channel width; Modulation; Bit rate; Preamble?; Sync word?; CRC?; Whitening?; 
  * Tips: search for the FCC ID; search at https://fccid.io
  * PResentation at black hat of general RF reverse engineering: https://www.youtube.com/watch?v=N0p3_ES2dBU
  * great slides: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwi8t4Xvq6TwAhWBieAKHdT1BkkQFjADegQIFBAD&url=https%3A%2F%2Fowasp.org%2Fwww-pdf-archive%2FAppSecIL2016_HackingTheIoT-PenTestingRFDevices_ErezMetula.pdf&usg=AOvVaw16PAjNk8MllTyjgEBLioFm

* **get satelite data**

  * cupturing signals (images) from one specific satelite: https://www.youtube.com/watch?v=jGWFg7EDnyY
  * https://www.youtube.com/watch?v=gMwciWchH3Q

* **jamming**

  * generate interference at the right frequency

* **Digital TV transmission - Tutorials**

  * avconv (generate stream), gr-dvbt (manipulation), BladeRF (transmitter), RTL-SDR dongle receiver): https://irrational.net/2014/03/02/digital-atv/
  * (swedish guy) gr-dvbt, bladeRF, RTL-SDR: https://www.youtube.com/watch?v=USkaq9R8XsY
  * GSM, gnu radio, bladeRF: https://www.youtube.com/watch?v=kpRnRfnr-Pg
  * Raspberry, UT-100C DVB-T
    * http://blog.everpi.net/2015/04/projeto-raspberry-pi-transmitindo-video-em-hd-hdtv-dvb-t.html
    * https://hackaday.com/2015/03/28/transmitting-hd-video-from-a-raspberry-pi/
    * https://issuu.com/cq-datv/docs/cq-datv14/11
  * ==TODO:==Explore these links: https://github.com/realraum/hackrf-dvb-t/blob/master/notes.txt
  * [my slides: Zads - ISDB-T testbed](https://docs.google.com/presentation/d/1FOgOR4pDglIwGDstzzCGtpQSqgWB4Z1y06WgSbgxe5Y/edit?usp=sharing)
  
  







































