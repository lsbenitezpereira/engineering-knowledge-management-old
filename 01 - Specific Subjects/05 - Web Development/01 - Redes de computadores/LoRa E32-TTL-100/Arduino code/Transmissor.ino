#include <SoftwareSerial.h>

#define BTN1  4
#define BTN2  5

SoftwareSerial loraSerial(2, 3); // TX, RX

String turnOn = "on";
String turnOff = "off";


void setup() {
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  Serial.begin(9600);
  loraSerial.begin(9600);
}
int t, t2 = 0;
void loop() {
  loraSerial.print(turnOn);
  Serial.println(turnOn);
  delay(2000);
  loraSerial.print(turnOff);
  Serial.println(turnOff);
  delay(2000);
  /*
    if (digitalRead(BTN1) == 0) {
    loraSerial.print(turnOn);
    Serial.println(turnOn);
    while (digitalRead(BTN1) == 0);
    delay(50);
    }

    if (digitalRead(BTN2) == 0) {
    loraSerial.print(turnOff);
    Serial.println(turnOff);
    while (digitalRead(BTN2) == 0);
    delay(50);
    }
  */
}
