## 1)

Para Para vairos K: 0.5, 1, 1.5
$$
FTMA(s) = \frac{k}{s(s+1)(s+0.5)}
$$
![img](Images - lista_03/7689b157-f2b4-4e82-9f55-86f9ebc29ed3.png)

![img](Images - lista_03/4bf5dd05-0977-48b8-a941-99aa3b498370.png)

![img](Images - lista_03/3a710304-c763-4e96-ade4-78cc48be3ec0.png)

## Comp. em avanço por LR, ordem>2

pg 12

![image-20210413224425290](Images - exercicios extras/image-20210413224425290.png)

![image-20210413224456877](Images - exercicios extras/image-20210413224456877.png)

![image-20210413224508464](Images - exercicios extras/image-20210413224508464.png)

![image-20210413224519392](Images - exercicios extras/image-20210413224519392.png)

![image-20210413224531075](Images - exercicios extras/image-20210413224531075.png)

> W_g = frequencia onde o ganho é 1, em rads/s

![image-20210413224554174](Images - exercicios extras/image-20210413224554174.png)

![image-20210413224640633](Images - exercicios extras/image-20210413224640633.png)

![image-20210413224718276](Images - exercicios extras/image-20210413224718276.png)

![image-20210413224732302](Images - exercicios extras/image-20210413224732302.png)

![image-20210413224742444](Images - exercicios extras/image-20210413224742444.png)

![image-20210413224752020](Images - exercicios extras/image-20210413224752020.png)



## Comp. em avanço por DB

![image-20210413232939592](Images - exercicios extras/image-20210413232939592.png)

![image-20210413232950599](Images - exercicios extras/image-20210413232950599.png)

![image-20210413233000088](Images - exercicios extras/image-20210413233000088.png)

![image-20210413233010770](Images - exercicios extras/image-20210413233010770.png)

![image-20210413233022852](Images - exercicios extras/image-20210413233022852.png)

![image-20210413233034934](Images - exercicios extras/image-20210413233034934.png)

![image-20210413233058625](Images - exercicios extras/image-20210413233058625.png)

![image-20210413233109490](Images - exercicios extras/image-20210413233109490.png)

## Comp. em avanço por DB

![image-20210412233027735](Images - exercicios extras/image-20210412233027735.png)

do diagrama, temos zeta=0.49

KC = Kv/lim s->0 GH = 20/10 = 2



```
s=%s;
num= 1;
den= s*(s+1);
FTMA= syslin('c', num, den);

[mf ,fg]=p_margin(FTMA)
[mg,ff]=g_margin(FTMA)
printf('Margem de fase: %.2f graus\n', mf)
printf('Frequência onde o ganho é 1: %.2f Hz\n', fg*100)
printf('Margem de ganho: %.2f dB\n', mg)
printf('Frequência onde a fase é 180º: %.2f Hz\n', ff*100)
```



wn = 12.51/0.75 =16.6Hz = 0.29 rad/s

## Comp. em atraso 

![image-20210413233257804](Images - exercicios extras/image-20210413233257804.png)

![image-20210413233336113](Images - exercicios extras/image-20210413233336113.png)

## Comp. em avanço por DB

![image-20210413233427759](Images - exercicios extras/image-20210413233427759.png)

![image-20210413233438014](Images - exercicios extras/image-20210413233438014.png)

![image-20210413233448902](Images - exercicios extras/image-20210413233448902.png)

![image-20210413233501589](Images - exercicios extras/image-20210413233501589.png)

## ZN MF por bode

pg 65

![image-20210415101758590](Images - exercicios extras/image-20210415101758590.png)

