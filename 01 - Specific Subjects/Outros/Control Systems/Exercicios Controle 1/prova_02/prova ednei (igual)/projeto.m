%ednei trabalho controle compensador Lugar das Raizes
clc; clear; close all;

zeta = 0.7 %zeta para sistema requerido
wn = 9 %wn para sistema requerido
kc = 1.9 %kc para sistema requerido
%fun��o original
numgs=[1 10];
dengs=[1 2 1];
gs=tf(numgs,dengs)

% An�lise Sistema Original
figure(1);
subplot(1,3,1);
margin(gs);
subplot(1,3,2);
rlocus(gs);
subplot(1,3,3);
ftmfgs=feedback(gs,1);
step(ftmfgs)
suptitle('Desempenho do Sistema Original')

%mostra as Margens de Ganho e Fase Console
disp("Margens em K");
[Gm,Pm,Wcg,Wcp]=margin(gs)
%'GM(dB); PM(graus); freq. 180 graus(r/s); freq. de 0 dB(r/s)'
disp("Margens em db");
margens=[20*log10(Gm),Pm,Wcg,Wcp]


%calcula s1 gs1
s1=-zeta*wn + wn*i*sqrt(1-zeta^2)
gs1=(s1+10)/(s1^2 +2*s1 +1)

%modulo e angulo de gs1 e s1
modmg=abs(gs1);
angmg=angle(gs1);
modms=abs(s1);
angms=angle(s1);

%calcula tz e tp
tz=(sin(angms)-(kc*modmg*sin(angmg-angms)))/(kc*modmg*modms*sin(angmg))
tp=-(((kc*modmg*sin(angms))+ (sin(angmg+angms)))/(modms*sin(angmg)))
%fun��o gc
numgc=kc*[tz 1];
dengc=[tp 1];
gc=tf(numgc,dengc)

%Diagrama Bode Compensador
figure(2)
margin(gc);
suptitle('Bode Compensador Gc')

%fun��o gc * gs malha aberta
gcgs=gc*gs;

%bode do sistema compensado
figure(3);
subplot(1,3,1);
margin(gcgs);
subplot(1,3,2);
rlocus(gcgs);
subplot(1,3,3);

%gcgs malha fechada
ftmfgcgs=feedback(gcgs,1);
step(ftmfgcgs);
suptitle('Desempenho do Sistema Compensado')

%mostra as Margens de Ganho e Fase Console
disp("Margens em K sistema compensado");
[Gm,Pm,Wcg,Wcp]=margin(gcgs)
%'GM(dB); PM(graus); freq. 180 graus(r/s); freq. de 0 dB(r/s)'
figure(4); hold on;
step(ftmfgs);
ftmfkcgs=feedback(kc*gs,1);
step(ftmfkcgs);
step(ftmfgcgs); hold off;
suptitle('Resposta ao Degrau')
legend('Sistema Original','Sistema Origina * Kc ','Sistema Compensado')
figure(5);
subplot(1,2,1)
nyquist(gs);
grid on;
subplot(1,2,2)
nyquist(gcgs);
grid on;