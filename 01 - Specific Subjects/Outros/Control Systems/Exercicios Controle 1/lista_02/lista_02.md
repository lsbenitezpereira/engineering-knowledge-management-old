## 1)

<img src="Images - lista_02/image-20210215134847047.png" alt="image-20210215134847047" style="zoom:50%;" />

## 2)



<img src="Images - lista_02/image-20210215134904875.png" alt="image-20210215134904875" style="zoom:50%;" />

Resposta ao impulso (impulso acontecendo em $t=1$):

![image-20210213233049230](Images - lista_02/image-20210213233049230.png)



## 3)

<img src="Images - lista_02/image-20210215134936584.png" alt="image-20210215134936584" style="zoom:50%;" />

![image-20210213234350249](Images - lista_02/image-20210213234350249.png)

## 4)

![image-20210215135056043](Images - lista_02/image-20210215135056043.png)

<img src="Images - lista_02/image-20210213235129069.png" alt="image-20210213235129069" />

## 5) 

<img src="Images - lista_02/image-20210223193251346.png" alt="image-20210223193251346" style="zoom:67%;" />

## 6)

<img src="Images - lista_02/image-20210215135129870.png" alt="image-20210215135129870" style="zoom:50%;" />

![image-20210214204246150](Images - lista_02/image-20210214204246150.png)

## 7)

<img src="Images - lista_02/image-20210215135202831.png" alt="image-20210215135202831" style="zoom:40%;" />

Item B, erro ao degrau:

![image-20210215115533026](Images - lista_02/image-20210215115533026.png)

Erro à rampa de1.32% (tá errado, era pra ser 10%!!!):

![image-20210215115613388](Images - lista_02/image-20210215115613388.png)

![image-20210215115755168](Images - lista_02/image-20210215115755168.png)

## 8)

<img src="Images - lista_02/image-20210215135229660.png" alt="image-20210215135229660" style="zoom:50%;" />

## 9)

<img src="Images - lista_02/image-20210223193226207.png" alt="image-20210223193226207" style="zoom:50%;" />

Resolvemos a ultima equação numericamente, usando a ferramenta gráficas [Desmos](https://www.desmos.com/calculator), por onde obteve-se o valor de $k=5.986$. Para esse valor de $k$, temos $y_{ss}=\frac{6}{22}=0.2727$ e, portanto, erro de $1-y_{ss}=0.7272$.

O gráfico interativo está disponível [neste link](https://www.desmos.com/calculator/yxmdom8nha).

![image-20210223193649946](Images - lista_02/image-20210223193649946.png)

## 10)

Simlação (impulso em$ t=1$):

![image-20210214201557787](Images - lista_02/image-20210214201557787.png)

<img src="Images - lista_02/image-20210215135328007.png" alt="image-20210215135328007" style="zoom:50%;" />	
