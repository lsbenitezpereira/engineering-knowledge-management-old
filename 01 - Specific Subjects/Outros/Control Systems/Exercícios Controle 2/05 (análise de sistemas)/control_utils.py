import numpy as np
import control
import matplotlib.pyplot as plt
from control.matlab import lsim

def step_info(FTMF:control.xferfcn.TransferFunction, tfinal:float)->dict:
    '''
    All the times are reported in units of `t`
    '''
    x, y = control.step_response(FTMF, tfinal)
    T = FTMF.dt
    metrics = {}
    
    # Sobressinal percentual
    metrics['Mp'] = (max(y)-y[-1])/(y[-1]-y[0])*100 
    metrics['tp'] = np.argmax(y)*T
    

    # Tempo de acomodação para 5%
    for i in range(len(x)-1, 0, -1):
        delta = abs((y[i]-y[-1])/y[-1])
        if delta >= 0.05:
            break
    metrics['ts5'] = i*T
    
    # Tempo de acomodação para 2%
    for i in range(len(x)-1, 0, -1):
        delta = abs((y[i]-y[-1])/y[-1])
        if delta >= 0.02:
            break
    metrics['ts2'] = i*T
    
    # Rising time
    tr = 0
    for i in range(len(x)-1):
        if y[i]>=y[-1]*0.1 and y[i]<y[-1]*0.9:
            tr += 1
    metrics['tr'] = tr*T
    
    # Delay time
    for i in range(len(x)-1):
        if y[i]>=y[-1]*0.5:
            break
    metrics['td'] = i*T
    
    return metrics


def errors(FTMA:control.xferfcn.TransferFunction, eps:float=1e-3, verbose=True):
    T = FTMA.dt
    metrics = {}
    
    [[n]],[[d]]= control.tfdata(FTMA);
    K = np.polyval(n, 1)/np.polyval(d, 1)   #  lim z->1
    metrics['Kp'] = K
    metrics['ess_degrau'] = np.array(1)/(1+K)
    
    aux = control.minreal(control.tf([1, -1],[1, 0],T)*FTMA);  # onde tf([1 -1],[1 0],T) = (z-1)/z = 1-z^-1
    [[n]],[[d]]= control.tfdata(aux);
    K = (np.polyval(n, 1)/np.polyval(d, 1))/T
    if abs(K)<eps: K=0
    if abs(K)>1/eps: K=np.inf
    metrics['Kv'] = K
    metrics['ess_rampa'] = np.array(1)/K
    
    aux = control.minreal(control.tf([1, -1],[1, 0],T)**2*FTMA);
    [[n]],[[d]]= control.tfdata(aux);
    K = (np.polyval(n, 1)/np.polyval(d, 1))/(T**2)
    if abs(K)<eps: K=0
    if abs(K)>1/eps: K=np.inf
    metrics['Ka'] = K
    metrics['ess_parabola'] = np.array(1)/K
    
    if verbose:
        _type = 'Above 3'
        if metrics['ess_rampa']==np.inf: _type = 0
        elif metrics['ess_parabola']==np.inf: _type = 1
        elif abs(metrics['ess_parabola'])<eps: _type = 2
        print(f'System type: {_type}')

    return metrics

def plot_responses(FTMF:control.xferfcn.TransferFunction, tfinal:float, figsize=(15, 4)):
    T = FTMF.dt
    Kmax = int(tfinal/T+1);
    t = np.linspace(0, tfinal, Kmax)
    plt.subplots(1,3, figsize=figsize)
        
    ##    
    plt.subplot(1,3,1)

    entrada = np.ones(t.shape[0]);

    y, t, x = lsim(FTMF, entrada, t);
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta ao degrau")
    plt.xlabel("k*T")
    plt.grid(True)

    ##
    plt.subplot(1,3,2)
    entrada = t;

    y, t, x = lsim(FTMF, entrada, t);
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta à rampa")
    plt.xlabel("k*T")
    plt.grid(True)
    
    ##
    plt.subplot(1,3,3)
    entrada = t**2;

    y, t, x = lsim(FTMF, entrada, t);
    plt.plot(t, entrada,'o', t, y,'*')
    plt.title("Resposta à parabola")
    plt.xlabel("k*T")
    plt.grid(True)