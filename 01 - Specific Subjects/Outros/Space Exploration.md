# Fundamentals

* ?

# News

* Mining water on moon: https://www.airspacemag.com/airspacemag/moons-gold-180975327/

# Private exploration

## SpaceX

* created in 2002

* **Rockets**
  * Falcon 1
    * ?
  * Falcon9
    * Falcon 9's most important improvement was its reusability. That enables SpaceX to recuperate around 46.5 million of the estimated ​62 million price tag associated with these flights.
    * reduce cost of space exploration
    * SpaceX first tried to save a booster in 2013, but it failed. The first successful save was in 2014
  * Startlink
    * ?
* NASA partnership
  *  in 2010, [the Obama administration directed NASA](https://www.sciencedirect.com/science/article/abs/pii/S0265964610001189?via%3Dihub) to refocus its efforts on deep space missions and rely on private companies to provide access to the ISS and low Earth orbit
  * It signed [contracts with NASA](https://www.spacex.com/press/2012/12/19/spacex-wins-nasa-cots-contract-demonstrate-cargo-delivery-space-station)  to provide cargo services to the ISS and with other companies and the U.S. military to provide general launch services
  * it also provides NASA a means of access to the ISS without relying on the Russian Soyuz.
  * A SpaceX fez história no último final de semana [hoje é 05/06/2020] ao lançar dois astronautas estadunidenses ao espaço rumo à Estação Espacial Internacional (ISS). Essa foi a primeira vez que uma cápsula tripulada e construída por uma empresa privada chegou na órbita da Terra e marca a primeira viagem espacial a partir de solo americano em quase uma década

## Boing

*  Boeing’s spaceship, called Starliner, spent two days in Earth orbit  without a crew in December and encountered several problems, including a crucial timing error in its software. Starliner will do another  uncrewed test flight in the coming months and is not likely to fly  astronauts until next year

## Blue origin

From Jeff Bezos