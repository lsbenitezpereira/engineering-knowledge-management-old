# Conceptualization

* Engineering + medice

* **Commom databases**
  * Used in biological sequence analysis: [Genbank](https://en.wikipedia.org/wiki/Genbank), [UniProt](https://en.wikipedia.org/wiki/UniProt)
  * Used in structure analysis: [Protein Data Bank](https://en.wikipedia.org/wiki/Protein_Data_Bank) (PDB)
  * Used in finding Protein Families and [Motif](https://en.wikipedia.org/wiki/Sequence_motif) Finding: [InterPro](https://en.wikipedia.org/wiki/InterPro), [Pfam](https://en.wikipedia.org/wiki/Pfam)
  * Used for Next Generation Sequencing: [Sequence Read Archive](https://en.wikipedia.org/wiki/Sequence_Read_Archive)
  * Used in Network Analysis: Metabolic Pathway Databases ([KEGG](https://en.wikipedia.org/wiki/KEGG), [BioCyc](https://en.wikipedia.org/wiki/BioCyc_database_collection)), Interaction Analysis Databases, Functional Networks
  * Used in design of synthetic genetic circuits: [GenoCAD](https://en.wikipedia.org/wiki/GenoCAD)
* **Conferences**
  * [Intelligent Systems for Molecular Biology](https://en.wikipedia.org/wiki/Intelligent_Systems_for_Molecular_Biology) (ISMB), [European Conference on Computational Biology](https://en.wikipedia.org/wiki/European_Conference_on_Computational_Biology) (ECCB), and [Research in Computational Molecular Biology](https://en.wikipedia.org/wiki/Research_in_Computational_Molecular_Biology) (RECOMB).
* **Online courses**
  * Bioinformatics Specialization ([UC San Diego](https://en.wikipedia.org/wiki/University_of_California,_San_Diego))
  * Genomic Data Science Specialization ([Johns Hopkins](https://en.wikipedia.org/wiki/Johns_Hopkins_University))
  * [EdX](https://en.wikipedia.org/wiki/EdX)'s Data Analysis for Life Sciences XSeries ([Harvard](https://en.wikipedia.org/wiki/Harvard_University)).

## Main Areas

* Clinical
  * Act in hospicals
  * Waste management
  * Data management (organization, filter, information retrivial, etc)
* Prostetics, bionics and biomecanics
* Pharmaceltics
* Instrumentation
* Materials
* Tissue engineering and genetics
* Bioinformatics and Health IT
  * Genomics (main area!!)
  * protein identification
  * Image processing
  * Wearables + embedded devices + Biomedical IoT
  * Personalized Health Care
  * Ambient Assisted living