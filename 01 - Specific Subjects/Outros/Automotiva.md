# Coisas elétricas

## Conversão mecânica -> elétrica

Para carregar a bateria
Saída precisa ser CC

Geralmente ligado por correia

### Dinâmo

Carros antigos, tipo fusca

### Alternador

Subtitui o dinâmo
A saída é senoidal alternada, precisa de um retificador
nível de tensão de 12,6 a 15 volts. 

## Motor de arranque

Ou *motor de partida*
Fornece energia inicial para o motor principal
motor elétrico com escovas
fica inoperante após realizar a partida
Alimentado com CC


Depois que a vela causa a primeira faísca na injeção de combustível, o motor de arranque para de funcionar e deixa o motor principal continuar trabalhando sozinho.

Curiosidade: Nos primeiros carros inventados, não havia um método de partida prático, sendo necessário dar a partida manualmente com uma manivela.

Automático de partida: controla o campo magético?
Bendix (ou *pinhão*): transmite o torque do motor de arranque ao motor principal?	
impulsor (ou “Eixo-Bendix”): ?
gira a cerca de 200 RPM 


Coisas que geram DANO ao motor de arranque: Insistência em manter o motor de partida funcionando quando o motor não pega; Acionamento do motor de arranque quando o motor já está funcionando.



# Motor principal

Volante do motor:  ?
Virabrequim: ?
Creamalheira:  ?

## Ingestão de combustível

responsável por enviar o combustível, de maneira controlada, ao motor do veículo


bobinas de ignição: ?
velas de ignição: ?

* **filtro de combustível**
  * na saída da bomba
  * dica: caso o filtro estiver com problema ou for novo (seco), antes de ligar girar a chave ate a metade para acionar a bomba de combustíel e enxarcar o filtro





Bomba de combustível - Responsável por fornecer o combustível sob pressão aos injetores.

Eletroventilador de arrefecimento - Posicionado atrás do radiador, ele é acionado quando o motor encontra-se em uma temperatura alta, gerando passagem de ar pelo radiador mesmo quando o automóvel estiver parado



* Problemas
  * Resíduos nos injetores, velas ou cabo de vela também são causas frequentes de falhas no sistema de injeção. Impurezas nos bicos injetores podem fazer com que o motor trabalhe de maneira irregular, desencadeando problemas mais graves.
  * Uma vez que exista suspeita de problemas no sistema de injeção, é importante utilizar um scanner automotivo para identificar exatamente em que local houve a pane

### Carburador

componente mecânico
criação da mistura ar/combustível
carburador eletrônico: comandado por dispositivos eletrônicos
==ainda bastante usado em motos?==


A borboleta (instalada na base do carburador) que é ligada diretamente ao pedal do acelerador dosa de acordo com sua abertura a quantidade de mistura que o motor precisa aspirar



(+) bataro
(-) gasta mais combustível

### Injeção eletrônica

Substituiu o carburador
(+) corta a ingestão de combustível quando em desaceleração (cutoff) (ou seja, ponto morto não economiza)
(+) menos poluentes, pois a queima é mais eficiente

permite um controle mais eficaz da mistura


* **Sensores**

  * sonda lambda: sensor de oxigênio na mistura
* **Atuadores**

  * Injetores
  * Bobinas
  * Motor corretor marcha lenta ou motor de passo
  * Válvula purga canister - Permite a circulação dos gases gerados no reservatório de combustível para o motor.
  * 
# Gasolina
Muitas vezes confundida com a gasolina premium (que possui octanagem maior), a aditivada é um combustível que recebe aditivos para reduzir e dissolver os resíduos sólidos depositados no sistema de ali... 

Leia mais em: https://quatrorodas.abril.com.br/auto-servico/gasolina-aditivada-e-benefica-para-motores-com-injecao-direta/



# Outros

Filtro de ar (do motor)? Filtro do ar condicionado?