# VFD

* Variable freqene driver
* rectifier -> inverter

# Partida indireta

* ?

* the bigger the motor, the more likely it is to require
  a starter

* Mechanical soft starters include clutches and several types of couplings using a fluid, magnetic forces, or steel shot to transmit torque, similar to other forms of torque limiter

* Partida estrela/triângulo,

* compensação por autotransformador

* slip-controlled wound-rotor induction motor (WRIM) drive controls speed by varying motor slip via rotor slip rings?

* Static VAR control

  * turn the voltage on and off

    ![image-20210422164659853](Images - Acionamento de motores elétricos/image-20210422164659853.png)

* Soft starter: ver em seção separada

## Soft starter

* used with AC electrical motors
* Diminui a corrente de pico na partida 
* Controla a tensão média, pelo chaveamento on/off da tensão
* The most widely used.
* (+) smoth
* (+) ajustable
* (-) corrente de partida não senoidal
* (-) not good for steady state speed control
* **como funciona**
  * ponte tiristorizada
  * Each thyristor is fired once per half-cycle, the firing being synchronized with
    the utility supply and the firing angle being variable so that each pair conducts
    for a varying proportion of a cycle.
  * dá pra usar só 2 ou 3 tiristores, mas o desbalanceamento provavelmente causará pulsação no torque
* **control strategies**
  * cheapest: open loop; alter the firing angle linearly with time; problematic with  high static friction loads (will have a dead time)
* **economia de energia**
  * pelo fato de a maioria desses equipamentos  possuírem um circuito que diminui a tensão para aqueles processos onde  se utiliza apenas 50% da potência do motor
* **firulas adicionais**
  * O processo de desligamento também pode ser feito de forma decrescente.
  * Detecção de falta de fase do motor, e funcionamento com apenas duas fases.
  * Conjugado de partida;
  * Parada por corrente contínua;
  * Proteção contra sobrecarga;
  * Contenção do nível de corrente.
  * Economia de energia.
  * Proteção contra sobreaquecimento;
* **Principais fabricantes**
  * Siemens, Danfoss, WEG, Schneider

# Torque control

* Interesting book: Advances in Motor Torque Control, IntechOpen, edited by mukhtar ahmad
*  The torque produced by an induction motor is proportional to the
  square of the applied voltage