%%% Parâmetros de entrada
clear
Zl  = 8.271+j*5.342 % Load impedance
V2 = 800            % Load nominal voltage
a=1/3
Z1=0.02 + j*0.04
Z2 = 0.170 + j*0.340
Zm = j*60
Zf = 80

%%% Cálculos
I2 = V2/Zl
E2 = V2 + I2*Z2
I1linha = I2/a
E1 = E2*a
Zmf = (Zf*Zm)/(Zf+Zm)
I0 = E1/Zmf
I1 = I0 + I1linha
V1 = E1 + I1*Z1

P1 = V1*I1'
P2 = V2*I2'
FP1 = cos(arg(P1))
FP2 = cos(arg(P2))

V2avazio = V1/a
Rpercentual = (abs(V2avazio) - abs(V2))/abs(V2)*100

%%% Resultados
printf("a) A tensão que deve ser aplicada ao primário do transformador para que a carga seja alimentada com tensão nominal: %s\n", num2str(V1))
printf("b) Potência na entrada do transformador.%s\n", num2str(P1))
printf("b) Potência na saída do transformador.%s\n", num2str(P2))
printf("c) Fator de potência da carga: %.4f\n", FP2)
printf("c) Fator de potência da fonte: %.4f\n", FP1)
printf("d) Rendimento do transformador: %.4f\n", real(P2)/real(P1))
printf("e) Regulação de tensão do transformador: %.1f\n", Rpercentual)%%%%%???
printf("f) Perdas no ferro: %.2f\n", real(abs(E1)^2/Zf'))
printf("f) Perdas no cobre: %.2f\n", real(abs(I1)^2*Z1) + real(abs(I2)^2*Z2))
