



Stepper
=======

A stepper motor may have any number of coils. But these are connected in
groups called \"phases\". All the coils in a phase are energized
together.

Stepper control with digital counter:
<http://electronics-diy.com/electronic_schematic.php?id=516>

[https://circuitdigest.com/electronic-circuits/stepper-motor-driver]()

Standalone board control: <https://www.pololu.com/docs/0J71/all>

**Unipolar** drivers, always energize the phases in the same way. One
lead, the \"common\" lead, will always be negative. The other lead will
always be positive. Unipolar drivers can be implemented with simple
transistor circuitry. The disadvantage is that there is less available
torque because only half of the coils can be energized at a time.\
\
**Bipolar** drivers use H-bridge circuitry to actually reverse the
current flow through the phases. By energizing the phases with
alternating the polarity, all the coils can be put to work turning the
motor.\
A two phase bipolar motor has 2 groups of coils. A 4 phase unipolar
motor has 4. A 2-phase bipolar motor will have 4 wires - 2 for each
phase. Some motors come with flexible wiring that allows you to run the
motor as either bipolar or unipolar.

![](./Images/Motores e Conversores eletromecanicos/media/image1.png){width="7.2652777777777775in"
height="4.876388888888889in"}

Motores eletricos
=================

-   Motor elétrico foi feito para trabalhar no talo (se não operar no
    seu 100% não é legal)

-   Rendimento:

$n\text{\%} = \frac{P_{\text{mecanica}}}{P_{\text{eletrica}\left( \text{ativa} \right)}} \ast 100$

* **Perdas**
  * Perdas de exitação (na propria bobina)
  * perdas no induzido da bobina
  * perdas no ferro
  * perdas mecânicas

## DC

* muito usados quando queremos controlar a velocidade
* Motores baseados em campo elétrico?
  * Todos os DC?
  * campos eletricos possuem baixa densidade de corrente
  * Então esse tipo de motor é usado apenas em aplicações com baixo torque 

### Brushless DC

* (-) exige mais manutensão que motorde indução
* (+) fácil de controlar a velocidade
* existe uma forte tendencia de, ao invés de usar motor CC, usar um inversor com motor de indução
* brush? brushless??
* 

### motor DC

* Armadura: dentro

* estator: fora

* escova: geralmente de grafica (que é condutora e lubrificante sólida)

* mancal = rolamento?

* o espaço de entreferro, entre rotor e estator, costuma ser o menor possível

* imã pode ser natural ou eletroima (geralmente só quando precisa ser muito grande, que não se consiga de forma natural)

  ![image-20201216082013773](Images - Motores e Conversores eletromecanicos/image-20201216082013773.png)

* **modelling**

  * J representa a inércia, do rotor

  ![image-20201216083110501](Images - Motores e Conversores eletromecanicos/image-20201216083110501.png)

* **controle**
  * pela corrente de armadura ou do estator (caso for con eletroimã)
  * geralmente, pela de armadura 
  * o termo mecânico é mais lento que o termo elétrico, e é ele que dita o comportamento dinâmico
  * geralmente desprezamos o termo elétrico
* **outros**
  * motores parar gerar energia, que devem girar continuamente na mesma velocidade, possuem J alto (para a mesma massa, são mais gordinhos)
  * motores para mover coisas, que geralmente vão e param, possuem J baixo (para a mesma passa, são mais alongadinhos)

### ServoDC

torque maior que o servo normal?

## Motor de indução trifásico

* os mais usados a industria
* se quisermos controlar a velocidade, a forma mais viável é usar um inversor com frequencia controlável
* corrente de partida é 6~8 vezes maior do que a nominal
* Softstarter: geralmente implementado com um gradador CA, começando com uma tensão bem baixa (portanto torque baixo, impedindo o motor de acelerar muito) até a tensão nominal (onda a velocidade do motor será dada pela frrequência da senoide)
* partida estrela-triangulo: primeiro lliga em estrela, depois chaveia para triangulo; tem dois “trancos” de arranque, apesar de eles serem menores do que a partida unica; 
* 95% dos motores industriais

![image-20210415213324726](Images - Motores e Conversores eletromecanicos/image-20210415213324726.png)

se formos diminuir a frequencia, é comum diminuirmos também um pouco da tensão para não causar saturação magnética no motor

* Limitesde operação
  * geralmente aguentam trabalhar com tensão até 10% acima da nominal
  * mais do que isso, só por pouco tempo (tipo na partida)

Motores mecânicos automotivos
=============================

-   Motores com carburador: economizam gasolina se ficar em ponto motor.
    Motores com injeção eletrônica (a maioria dos modernos): economiza
    gasolina se ficar engranado na descida (pois a injeção irá cortar
    automaticamente o combustivel, o que não acontence no ponto morto)

Outras coisas mecânicas
=======================

-   Degree of freedom (DOF)

    -   We count one degree of freedom for each independent direction in
        which a robot can move

    -   For nonrigid bodies, there are additional degrees of freedom
        within the robot itself

```{=html}
<!-- -->
```
-   kinematic state (or *pose*): space position (x, y, z) + angular
    orientation(yaw, roll, and pitch)

-   dynamic state: rate of change of each kinematic dimension

# Geradores

puxar mais corrente de um gerador mecânico faz ele diminuir sua velocidade de rotação