### Ciência, tecnologia e Covid-19

> “Vivemos numa sociedade intensamente dependente da ciência e da tecnologia, em que quase ninguém sabe algo sobre ciência e tecnologia.”

> \- Carl Sagan

As ciências biológicas possuem uma importância indubitável no combate ao Covid-19, mas praticamente todas as áreas científicas e tecnológicas possuem contribuições a dar. Na minha área de pesquisa, Inteligência Artificial, diversas soluções vêm sendo desenvolvidas, por exemplo: 

* Acelerar o diagnóstico da doença [1];
* Descobrir novos medicamentos [2];
* Minerar e sumarizar a grande quantidade de informação (artigos, pesquisas, estatísticas, etc) sobre o vírus [3];
* Identificar pessoas com princípio de febre, por exemplo em aeroportos e rodoviárias [4].

Essas soluções ajudarão? Provavelmente, mas as medidas que vêm se mostrando mais efetivas são as **Intervenções Não Farmacológicas (INF)**, como fechar escolas e diminuir a mobilidade internacional. Essas medidas serão efetivas para conter o avanço da doença? É difícil elaborar um modelo matemático preciso para uma situação tão complexa e sem precedentes recentes, mas Neil M Ferguson et al. (2020) elaborou previsões para três cenários diferentes de INFs implementadas por 5 meses (retângulo azul) na Grã Bretanha: 

![image-20200330195941771](Images - Ciência, tecnologia e Covid-19/image-20200330195941771.png)

O modelo prevê que após o alívio das medidas haverá um segundo pico de casos, que pode ser tão grave quando o cenário inicial. Dessa forma, o estudo recomenda que as medidas sejam aplicadas de forma intermitente ao longo de vários meses (os retângulos azuis representam a aplicação de severas medidas de intervenção):

![image-20200330200440135](Images - Ciência, tecnologia e Covid-19/image-20200330200440135.png)

Podemos ver que essas intervenções são eficientes para distribuir casos ao longo do tempo e evitar a sobrecarga do sistema de saúde, mas a covid-19 ainda assim causará um grande número de infecções ao longo dos próximos anos. Diversos estudos apresentam cenários análises similares, por exemplo Kiesha Prem et al. ao analisar diferentes durações para as INFs atualmente implementadas na China (onde as linhas sólidas representam a mediana da incidência acumulada e a área sombreada o intervalo inter-quartil): 

![image-20200330202240577](Images - Ciência, tecnologia e Covid-19/image-20200330202240577.png)

Percebe-se que, mesmo em diferentes cenários, a doença provavelmente atingirá em torno de 60% da população. A assintota da curva, também chamada de *capacidade de suporte*, pode ser atingida em pouco ou muito tempo, o que faz toda a diferença na sobrecarga do sistema de saúde. 

Dada a realidade política e econômica do Brasil, eu considero improvável que ao longo dos próximos anos venhamos a fazer uma vacinação em larga escala da população ou que um novo remédio seja acessível para a população de baixa renda. Portanto, considero válido prever que se a covid-19 acometer 60% da população brasileira nos próximos anos e a taxa de mortalidade for de 0,5% (o que já será um avanço comparada a atual taxa de mortalidade de 3,2% [7]), teremos mais de 600 mil mortos. 

Estamos falando de um dos eventos geopolíticos mais importantes dos últimos séculos, lado a lado com as grandes guerras do século 20 e com outras grandes epidemias. Os efeitos - das mortes, das hospitalizações, dos traumas do isolamento, da economia paralisada, entre outros -  serão sentidos em todas as dimensões do mundo moderno, em todas as regiões do planeta. “Estamos em guerra”, disse o presidente francês Emmanuel Macron. **Quais países sairão vencedores dessa guerra?** Certamente não aqueles que lutarem como quem luta contra uma *gripezinha*.



# Referências

[1] - [https://thenextweb.com/neural/2020/03/02/alibabas-new-ai-system-can-detect-coronavirus-in-seconds-with-96-accuracy/](https://l.facebook.com/l.php?u=https%3A%2F%2Fthenextweb.com%2Fneural%2F2020%2F03%2F02%2Falibabas-new-ai-system-can-detect-coronavirus-in-seconds-with-96-accuracy%2F%3Ffbclid%3DIwAR0YQVlgk0SQ1fYL6Yoqgu_xmvPFhh1MjtEBK11YBAGX-v5hMTsS5-SjZu4&h=AT0qeShXnduWzZ6EZJkIFus77l2M4T2l1z1oFIfzh9rPZE9GWfRObchzg3kW5kRmSSf_lYYrpAzsAZi83kvA_qEknqTlZeGqfrm60dZ_vZVQ_UBgLx0IJcDi_Elx&__tn__=-UK-R&c[0]=AT3iQfTJhPciT0kaNj53Fyo-MJzc4Vnbgqfc2jbcLtpT9f7rcd3iqUhnWfoRhQsTUA_Zkt6QxE_wKhoGi3zoTX-w_hBx94SuSYsKuzAxth0pa02n1NQKn-VT7X173ZRO7FwydLlPY7uY0v0wDnop)

[2] - [http://news.mit.edu/2020/artificial-intelligence-identifies-new-antibiotic-0220](https://l.facebook.com/l.php?u=http%3A%2F%2Fnews.mit.edu%2F2020%2Fartificial-intelligence-identifies-new-antibiotic-0220%3Ffbclid%3DIwAR0biOa3sWVWSA5Y3zHyZ8IzBQbXU_IxC_XkF9m2TCbXFhiRg5hxZMPcG0w&h=AT3ejbiD5ndhufceuZDdiMqKjYdgu3lIj5HNBqGK7GE9VOErrloly79ugyy-mbe9O1ES2_pWvtwEzDct8dDfhg4LmEH0LAkZcuuM-CBpHmSJp3z11Xg0KB6IAGmm&__tn__=-UK-R&c[0]=AT3iQfTJhPciT0kaNj53Fyo-MJzc4Vnbgqfc2jbcLtpT9f7rcd3iqUhnWfoRhQsTUA_Zkt6QxE_wKhoGi3zoTX-w_hBx94SuSYsKuzAxth0pa02n1NQKn-VT7X173ZRO7FwydLlPY7uY0v0wDnop)

[3] - [https://www.whitehouse.gov/briefings-statements/call-action-tech-community-new-machine-readable-covid-19-dataset/](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.whitehouse.gov%2Fbriefings-statements%2Fcall-action-tech-community-new-machine-readable-covid-19-dataset%2F%3Ffbclid%3DIwAR27sqg-KPkulJgo6isJHIGoFda45pZ0qK7RQQbZnlQ7X-owtKF07VI4ITo&h=AT1zPd1niIOjqyOBMpZIdGrop2xkxroQn3YH0ED4vEWtjO_9C9IE7WS1v8wHFdUB2VvCv-fdSbeVQatk0k9pEDxTVjDPs7XpDA2C9L-4rB1x-Lg7Hjs-OgZSo1A0&__tn__=-UK-R&c[0]=AT3iQfTJhPciT0kaNj53Fyo-MJzc4Vnbgqfc2jbcLtpT9f7rcd3iqUhnWfoRhQsTUA_Zkt6QxE_wKhoGi3zoTX-w_hBx94SuSYsKuzAxth0pa02n1NQKn-VT7X173ZRO7FwydLlPY7uY0v0wDnop)

[4] - [https://www.photonics.com/Articles/Demand_for_FLIR_Temperature_Screening_Devices/a65632](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.photonics.com%2FArticles%2FDemand_for_FLIR_Temperature_Screening_Devices%2Fa65632%3Ffbclid%3DIwAR1jV6aVGt8fXksdgXGC_2o7QV0ZuOMGdfrB34HGCFirdcVgq2gYKtUTaw0&h=AT3D1UE25fdgrD-l_p2q6aZgoenSIf82iQlK7cLye44E22zgp6nSupzLPHuYIcUcLxU4_cCAZfryFDkRZqiriEQZKu9a0iMa5owxQDlKdw3FlIcwjIL2ILtgW-PL&__tn__=-UK-R&c[0]=AT3iQfTJhPciT0kaNj53Fyo-MJzc4Vnbgqfc2jbcLtpT9f7rcd3iqUhnWfoRhQsTUA_Zkt6QxE_wKhoGi3zoTX-w_hBx94SuSYsKuzAxth0pa02n1NQKn-VT7X173ZRO7FwydLlPY7uY0v0wDnop)

[5] - Ferguson, N., Laydon, D., Nedjati Gilani, G., Imai, N., Ainslie, K.,  Baguelin, M., ... & Dighe, A. (2020). Report 9: Impact of  non-pharmaceutical interventions (NPIs) to reduce COVID19 mortality and  healthcare demand.

[6] - Prem, K., Liu, Y., Russell, T. W., Kucharski, A. J., Eggo, R. M., Davies, N., ... & Abbott, S. (2020). The effect of control strategies to reduce social mixing on outcomes of the COVID-19 epidemic in Wuhan, China: a modelling study. The Lancet Public Health.

[7] - https://noticias.r7.com/saude/brasil-tem-136-mortes-por-covid-19-numero-de-casos-chega-a-4256-29032020



# Preview

A atual epidemia é um dos eventos geopolíticos mais importantes dos últimos séculos. Quais países sairão vencedores dessa guerra? O que podemos fazer para minimizar os impactos humanos, sociais e econômicos? 

