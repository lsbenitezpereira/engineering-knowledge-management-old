## Des-progresso ordenado: como uma cultura destrói a sua ciência

Este é um breve ensaio sobre a história do retrocesso humano: como grandes civilizações deixaram de ser grandes, como sociedades tecnologicamente avançadas se tornaram atrasadas, como algumas culturas que antes incentivavam o desenvolvimento científico passaram a inibir-lo.

Ou seja, como podemos retroceder, andar para trás (não para ganhar impulso), sabotarmos o nosso próprio desenvolvimento. Já alerto que esse texto é altamente baseado no livro *Armas, Germes e Aço* de Jared Diamond, o qual faz uma análise muito mais rigorosa do que eu sobre os destinos das sociedades humanas. Não existe uma equação matemática para modelar o progresso humano, então irei tomar alguns exemplos para discutirmos.

A **China**, uma das mais antigas civilizações do mundo, foi berço de uma longa lista de invenções: pólvora, papel, bússola, técnicas de fundição de ferro, entre muitas outras. Até meados de 1400 ela era muito mais avançada do que a Europa, mas algo se perdeu nos séculos seguintes. Sim, hoje a China é um dos países mais ricos do mundo, mas no começo do século 20 era uma nação pobre, em declínio e fortemente dominada por países estrangeiros [2,3].

Ou seja, a China foi uma das civilizações mais avançadas do mundo mas “não foi capaz de realizar o que é conhecido como uma revolução científica“ [4, tradução própria] e perdeu a liderança. O que causou isso? Jared Diamond dá algumas dicas ao apontar que **uma sequência de governantes autoritários e conservadores se opuseram ao desenvolvimento científico**, e entre seus exemplos estão frotas de navios que foram desmanteladas por causa de disputas políticas (parecidas ao moderno “não daremos continuidade à nada feito no governo anterior”) e projetos de máquinas de fiar movidas à agua que pararam de ser desenvolvidos por decisões de governantes. Outros autores apontam outras razões, como restrições de comércio e produção industrial impostas por algumas dinastias objetivando estimular a agricultura e a produção de alimentos [5].

Outro exemplo vêm do **Oriente Médio**, que durante a idade média foi tecnologicamente muito avançado, aberto à inovação, e com índices de alfabetismo e riqueza muito superiores à Europa no mesmo período (faço muitas comparações com a Europa, mas poderia perfeitamente fazer com outras regiões do mundo). Assim como no caso chinês podemos atribuir ao oriente médio incontáveis desenvolvimentos, da trigonometria à química, mas isso mudou com a ascensão de uma escola filosófica/religiosa profundamente oposta à ciência e à racionalidade [6], com um pensamento que prefere explicar a chuva como sendo “[…] criação divina ao invés de evaporação causada pelo sol que se condensa” [7]

Inúmeros outros exemplos existem:

* O Japão que, após serem o maior produtor e detentor de armas de fogo do mundo em 1600, abandonou quase completamente a pólvora por causa de uma classe dominante samurai que via nela uma ameaça à sua cultura de armas de corte [1, pg. 258];
* Aborígenes da Tasmânia, que abdicaram da pesca e ferramentas de osso (importantes tecnologias para produção de alimento) para voltarem a serem coletores e caçadores; [1]

* Aborígenes da Austrália que, apesar de terem no passado dominado o uso de arco e flecha, também optaram por abandonar essa tecnologia. [1]

Veja bem, o problema não é de uma religião ou outra, esse partido político ou aquele. **O retrocesso de uma sociedade acontece pelo repúdio aos métodos da inovação e do progresso científico, por uma visão de mundo incompatível com o progresso.** 

Por fim, deixo algumas provocações: o que mais retrocede? A qualidade da nossa democracia está ficando pior? Será que a qualidade da política de países com uma história democrática muito maior do que a nossa - como por exemplo os Estados Unidos (nós somos uma democracia desde 1989, eles desde 1776) - também está piorando? Será que a capacidade intelectual de um cidadão mediano, nossa “inteligência”, também está diminuindo? Em caso afirmativo, como podemos reverter essas tendências? 

<img src="Images - 03 - Des-progresso ordenado/DSCN3730.JPG" alt="img" style="zoom:10%;" />

> Foto de Helsinki, capital da Finlândia. Esse já foi um país muito pobre, um dos mais pobres da Europa, mas reverteu sua tendência por meio de investimento em educação e ciência. Autoria Própria.

## Referências

[1] - Jared Diamond. Guns, Germs, and Steel. W. W. Norton & Company, 1997.

[2] - History Learning Site, 2020. Disponível em: https://www.historylearningsite.co.uk/modern-world-history-1918-to-1980/china-1900-to-1976/china-in-1900/

[3] - Wikipedia. Economic history of China (1912–49), 2020. Disponível em: https://en.wikipedia.org/wiki/Economic_history_of_China_(1912%E2%80%9349)

[4] - Economic History Association, 2020. Disponível em: https://eh.net/encyclopedia/economic-history-of-premodern-china-from-221-bc-to-c-1800-ad/

[6] Fu Zhufu. The Economic History of China: Some Special Problems. Sage Publications. Modern China, Vol 7 No 1, 1981. Disponível em: https://journals.sagepub.com/doi/abs/10.1177/009770048100700101?journalCode=mcxa

[6] - Hillel Ofek. Why the arabic world turned away from science. The New Atlantis. 

Hillel Ofek. Why the Arabic World Turned Away from Science. The New Atlantis, Number 30, Winter 2011, pp. 3-23. Disponível em: https://www.thenewatlantis.com/publications/why-the-arabic-world-turned-away-from-science

[7] - Joe Boyle. Nigeria's 'Taliban' enigma. BBC, 2009. Disponível em: http://news.bbc.co.uk/2/hi/africa/8172270.stm