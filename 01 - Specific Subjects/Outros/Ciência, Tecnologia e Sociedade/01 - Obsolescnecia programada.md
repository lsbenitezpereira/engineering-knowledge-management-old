* * exportação de lixo como "produto des egunda mão"
    ambiental damagae as "externalities"





# Texto 1

*dá pra aproveitar isso em outra resenha*

As engenharia comumente vêem impactos ambientais como externalidade, fatores que não são inerentes ao desenvolvimento do projeto per si.

carros elétricos são bons para o meio ambiente… ignornado o descarte das baterias

hidroelétricas são uma fonte limpa… ignorando a área alagada e os impactos sociais de populações indigenas deslocadas

o mercado de smartphones é muito lucrativo… ignorando a destinação correta do lixo eletrônico



o mesmo não acontece com outras área, que são considerados desde o começo do ciclo de vida do produto: a logistica de escoamento dos produtos é fundamental na decisão de onde construir uma nova fábrica; as estratégias de marketing e divulgação são centrais no modelo de negócio de uma empresa de tecnologia, tão importantes quanto o produto em si; entre outros.

Por que a logística reversa [] também não é incluída no planejamento dos custos desde o começo? 

# Texto 2





# Ciência, Tecnologia e Lixo

O grande acúmulo de resíduos sólidos no mercado de eletroeletrônicos tornou-se uma preocupação global. Em menos de dez anos o planeta Terra sofreu um aumento de 49% na quantidade desse tipo de lixo, e apenas em 2016 foram produzidos mais de 46,1 toneladas de lixo eletrônico [1].

A partir destes dados surgiram grandes preocupações acerca da sustentabilidade, planos de reciclagens, estudos sobre o impacto do e-lixo no nosso ecossistema, projetos para redução do lixo e leis criadas através de diretrizes, como é o caso da RoHS (Restriction of Certain Hazardous Substances) e da Waste Electrical and Electronic Equipment (WEEE). No Brasil criou-se a Política Nacional de Resíduos Sólidos (PNRS) [2], que em um de seus pontos trata sobre a questão de resíduos eletroeletrônicos. 

Mas… serão apenas leis o suficiente? Não, e as “grandes mudanças” (*alerta de ironia*) vistas na cadeia logística das empresas atuando no Brasil suportam essa afirmação [3]. Mudanças efetivas nessa realidade apenas serão alcaçadas quando nós, sociedade brasileira, tivermos discussões nacionais sobre qual é o posicionamento do Brasil sobre o gerenciamento de lixo. Devemos exigir das companhias das quais consumimos que tenham para com a logística reversa dos resíduos (Figura 1) a mesma seriedade que exigimos para com a produção e entrega dos produtos (logística direta), atendimento ao cliente, suporte, etc. 

![Image result for logistica reversa](Images - 01 - Obsolescnecia programada/logistica-reversa-o-que-e-como-funciona-e-como-aplicar.png)
> Figura 1 - Logística reversa [4]

“Utopia”. “Não no Brasil”. Bom, cada um escolhe em quais utopias acreditar, quais ídolos/exemplos seguir, e eu tenho claro os meus: o Japão foi o primeiro país a fazer um sistema de coleta de e-lixo, chamado EPR (Extended Producer Responsibility). Também possui diversas leis para produtos eletrônicos diferentes, de forma que a maior parte do lixo eletrônico é coletado e tratado.

# Referências

[1] BALDÉ, C. P. et al. (2015), The global e-waste monitor – 2014, United Nations University,
IAS – SCYCLE, Bonn, Germany. Disponível em:
https://i.unu.edu/media/unu.edu/news/52624/UNU-1stGlobal-E-Waste-Monitor-2014-small.pdf.

[2] BRASIL. Política Nacional de Resíduos Sólidos, 2010. Disponíve em: http://www.planalto.gov.br/ccivil_03/_ato2007-2010/2010/lei/l12305.htm

[3] FLORESTI, F (2018). Quase todo lixo eletrônico do Brasil é descartado de maneira errada. Revista Galileo. Disponível em: https://revistagalileu.globo.com/Ciencia/Meio-Ambiente/noticia/2018/05/quase-todo-lixo-eletronico-do-brasil-e-descartado-de-maneira-errada.html

[4] STABELINI, D. Logística reversa: o que é, como funciona e como aplicar. Texaco. Disponível em:  https://blog.texaco.com.br/ursa/logistica-reversa-o-que-e-como-funciona/