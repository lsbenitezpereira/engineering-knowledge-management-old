# Plantação subterrânea

* https://www.bbc.com/future/bespoke/follow-the-food/the-massive-farms-emerging-beneath-our-cities.html
* Or, in a more general name, controlled environment agriculture
* with a long tunnel, one of the major concerns is good airflow, otherwise pathogens build up
* Conditions
  * pink light (both blue and red wavelengths are used, optimal for growing, but the lights look pink)
  * ~14C
  * 