# Conceituação

-   *Ambiente Eletromagnético* →Fenômenos eletromagnéticos de um local.

-   Ruído Eletromagnético → Variável no tempo, pode se sobrepor ou se combinar com outro sinal.

-   Sinal Interferente → Prejudica a recepção de um sinal

-   Sinal não desejado → Pode ou não prejudicar a recepção de um sinal.

-   Perturbação Eletromag. →fenômeno eletromagnético perturbador

-   ==Emissão X Radiação Eletromagnética → Transmissão da energia eletromagnética por uma fonte X Transmissão das ondas/energia eletromag por uma fonte.==

-   **Interferência Eletromagnética(EMI)**

    -   Prejuízo no desempenho do sistema causando por uma perturbação eletromag.
        
    -   De 9kHz a 1Ghz EMI é onde costuma dar mais problema forte

    -   Interferência inter-sistemas → Perturbação que um sistema prova em outro.
        
    -   Interferência intra-sistemas → Perturbação produzida dentro do próprio sistema.

-   **Compatibilidade Eletromagnética(EMC)**
    -   Capacidade de um sistema de funcionar bem no ambiente eletromagnético sem ser uma fonte de poluição nele
    -   Fazer com que um equipamento funcione bem em um ambiente.
    -   Estudo necessário para que os equipamentos estejam dentro das normas e tenham êxito de desempenho.
    -   Principais normas: FCC e IEC61000-x-x (==Ver Slide?==)
    -   <u>Requisitos gerais para um sistema ser compatível</u>
    -   Não causar interferência a si mesmo.
        
    -   Não é suscetível a emissões.
        
    -   Não causa interferência em outros sistemas.
    
-   **Componentes básicos necessários**

    -   Transmissor de EMI, dispositivo de acoplamento e um receptor.
    
-   **Projetos na área de EMC**

    -   quase todo produto eletrônico precisa de preucupar com EMC

    -   quanto mais cedo, melhor

        ![image-20210524184433533](Images - 1.0 - Compatibilidade eletromagnética/image-20210524184433533.png)

## Ruido conduzido

* Caminhos condutores intensionais ou não

* geralmente de 150KHz à 30 MHz

* Se o equipamento for à bateria não compartilhada, não teremos problema com ruido conduzido

* Correntes de modo comum: vai por dois caminhos (ex: fase e neutro) e volta pelo terra; correntes possuem o mesmo sentido
  Correntes de modo diferencial: vai por um (ex: fase), volta por outro (ex: neutro); correntes possuem sentidos opostos
  
* procedimento de medição

  * ?

  * o terra da medição é diferente do terra da rede

  * equipamento sobre uma mesa, sem nenhuma parte metalica

  * LISN: estabilizador de impedância; isola a rede do equipamento;

    ![image-20210531190522715](Images - 1.0 - Compatibilidade eletromagnética/image-20210531190522715.png)

## Ruído irradiado

* ?

* geralmente de 30MHz à 1GHz

* procedimento de medição

  * em campo aberto ou câmara enecoida/semianecoica (mas nesse caso devemos fazer a correlação com um ambiente aberto)

    ![image-20210531194318601](Images - 1.0 - Compatibilidade eletromagnética/image-20210531194318601.png)

## Electrostatic Discharge (ESD)

* Procedimento de medição
  * simulando o dedo de alguem enconstando no equipamento

## Outros problemas

*  Transientes elétricos rápidos
  * 
  * procedimento de medição: injeta picos rápidos seguidos na rede, em pulsos
* surtos
  * raios, descarga, etc
* interrupções rápidas e variações de tensão

## Testes de suceptibilidade

* Enviar um ataque e ver o caso

* * 

## integridade de sinal

* se dois sinais precisam percorrer um mesmo percurso ao mesmo tempo (exemplos: barramenteos de mcu), as trilhas devem ter o mesmo tamanho

## Causas de ruído

* Comutação

  * quanto menor a razão cíclica, melhor

  * quanto maior o tempo de comutação, melhor

  * Se a comutação tiver overshoot e undershoot, haverá mais energia no espectro naquela frequenia de os

  * Efeito da Largura do Pulso 	dos Tempos de Subida e Descida:

    

## Softwares

* EMCoS

# Normas

* Alguns elementos comuns à quase todas as normas serão registrado nessa seção principal
* a maioria das normas se baseia na norma CISPR
* Classe A: uso industrial ou comercial
* Classe B: uso residencial; costumam ter limites mais severos
* normatizam tanto a emissão quanto a suceptibilidade
* feito com o equipamento completo (tudo o que é vendido) e operando normalmente
* ruído: sinal que causa interferência no equipamento
*  
* é bom projetar o equipamento para ficar uns 3dB abaixo dos limites

## Certificações

* O equipamento não pode passar por modificações *significativas* após o teste
* no testes eles tiram foto do equipamento, parte interna e externa, etc
* alguns tipos de equipamentos precisam ser retestados regularmente
* geralmente o equipamento é enviado para o laboratório certificador. Em alguns casos e quando o equipamento já está sendo comercializado, o laboratório compra o equipamento do mercado
* **custos**
  * geralmente em torno de uns 10k, para um equipamento simples?

# Prevenção da interferência

-   Geralmente o equipamento é mais suscetível às mesmas frequências que ele emite
-   
-   Deixar o caminho de propagação da onda ineficiente.
-   Receptor menos suscetível a interferência
-   Suprir as necessidades da fonte
-   
-   blindagem as vezes ajuda, mas pode piorar para algumas frequencias 
-    
-    
-    
-   
- “Snubbers”;
- Dissipadores;
- Elementos Magnéticos;
- Isolamento Galvânico;
- Escolha de Componentes Passivos;
 Layout do equipamento;
- Blindagens e Filtros;(força bruta)

## Cabos e coisas

* **Par trançado**
  * passo de trançamento: relacionado à frequencia?
* **flat cable**
  * ruim para emi?
* **fibra otica**
  * não sofre problemas de compatibiidade eletromagnetica
* **choke de EMI**
  * aquele negocinho dos cabos
* **filtro snubber**
  * ou *filtro localizado*
  * ?
* **espuma condutora**
  * gazget?

# Condições de segurança

-   Aterramento, filtro de EMI,indutor, plano terra e blindagem.

-   Equipamentos de medição(Ver Slide)
