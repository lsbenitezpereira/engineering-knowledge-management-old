# Introduction

### Conceptualization

- Three terminals: collector, emitter and base

- Two types: pnp and npn

- Both electrons and holes participate of the currnet conduction (the
  name bipolar comes from that)

- **applications**
  -   still the preferred device in some very demanding analog and
      digital integrated-circuit
  -   especially true in very-high-frequency circuits
  -   high-current-driving capability
  -   Symbology:

  ![alt-text](Images - BJT/screenshot011.png)

### Physical structure

-   NPN: consists of three semiconductor regions: the emitter region (n
    type), the base region ( p type), and the collector region (n type)
-   PNP: p-type emitter, an n-type base, and a p-type collector
-   Both have two pn juctions
-   the device is not symmetrical, and thus the emitter and collector
    cannot be interchanged
-   Simplified vision:

    ![alt-text](Images - BJT/screenshot001.png)

* Cross section:

![alt-text](Images - BJT/screenshot008.png)

* Real model: o substrato geralmente é P, para podemos fazer vários
TBJs mantendo o canal isolado.

Operation
=========
-   $i_E = I_C + I_B $
-   Low $V_{CE}$: saturation (switch application)
-   Hight $V_{CE}$: active direct (amplifier application)
-   Negative $V_{CE}$: active reverse

![alt-text](Images - BJT/screenshot002.png)
*  The $V_{BE}$ decreases about 2 mV for every 1°C rise
* **PNP considerations**
	* npn: corrente entra na base
	* for a pnp transistor operating in the active mode $v_{EB}$ is
	  negative, while in an npn transistor $v_{BE}$ is positive.
	* Current is mainly conducted by holes injected from the emitter
	* Pnp: corrente sai da base
	* All equations are equal, but we’ll replace $V_{BE}$ by $V_{EB}$
- **common emitter current gain ($\beta$)**

  - or $H_{FE}$

  - $ I_C=\beta I_B$

  - Proportional to the width of the base region (W) and to the
    relative dopings of the base region and the emitter region
    (Na/Nd)

  - Usually from 50 to 200 (to direct region). Varia muito para cada transistor

  - Better when bigger (to most applications)

  - depends on the dc current [at which the
    transistor is operating

  - Increase with temperature 

    ![1541879175105](Images - BJT/1541879175105.png)

  - <u>alpha</u>

    -   Manipulação matemática de facilita os calculos
    -   $\alpha$: common-base current gain (value close to unity)
    -   $i_C = \alpha I_E$
    -   $I_E=(\beta+1)I_B$
    -   $ \alpha = \frac {\beta}{\beta+1} $
    -   ==$ \beta = \frac{\alpha}{1-\alpha}$==

  - <u>ac beta ($h_{FE}$)</u>

    -   or *incremental beta*
    -   There is a little difference (that will not be taken in count here) between the dc and ac beta.
    -   $\beta_{ac}=\frac{\Delta i_C}{\Delta i_B}$
    -   Remember, $\beta_{DC}=\frac{I_C}{I_B}$
    -   is lower in saturation than in the active 

- **Scale current ($I_S$)**

  -   or *saturation current*
  -   approximately doubling for every 5°C rise in temperature
  -   inversely proportional to the base width W
  -   Directly proportional to the area of the EBJ
- **Collector-base reverse current**($ I_{CBO}$)
  - Small (few nano amperes), and usually be ignored
  - thermally generated minority carriers
  - is the reverse current flowing from collector to base with the emitter open-circuited (hence
    the subscript O)
  - depends strongly on temperature
* **Temperature efects**

  * $V_{BE}$ decreases by about 2 mV for each rise of 1°C in temperature

    ![1541867034642](Images - BJT/1541867034642.png)
    
### Saturation mode

  - For switching applications
  - The CBJ is forward biased by more than 0.4 V
  - Saturation means something completely different in a BJT and in
	a MOSFET
  - Como o calculo para a região de saturação é bem dificil,
    geralmente apenas forçamos uma beta. Esse beta deve ser menor do
    que o beta normal
  - We can adjust the ratio $ \frac{i_C}{i_B}$ to a valua lower than
    beta, and called *forced beta*:

![alt-text](Images - BJT/screenshot010.png)

![1541870546655](Images - BJT/1541870546655.png)

  - Esse beta pode ser meio chutado no feeling, foda-se
  - Fator de força: $ \frac{\beta}{\beta_{forçado}}$ (geralmente
    forçamos entre 2 e 1)
  - $V_ce$ fica entre 0.1V e 0.3V
  - ??fator de força?
* **Saturation resistance**
	* Usually a few tens of ohms 

![1541870804527](Images - BJT/1541870804527.png)

### Active mode

  -   When Vce &gt; 0.3	
  -   For amplifier applications
  -   **Physical Currents analysis**
	* electrons injected from the emitter into the base, and holes
		injected from the base into the emitter.
	* electrons will be minority carriers in the p-type base region.
	* Some electrons will recombine in the base (when going in the
		direction of the colector), but that “lost” will be small
	* The Ib needs to be supplied by the external circuit in order to
		replace the holes lost from the base through the recombination
		process
	* a recombinação (quando circula corrente) dissipa energia
	* se ligarmos ao contrario, vamos aumentar a recombinação: ele vai esquentar mais e provavelmente queimar.

![alt-text](Images - BJT/screenshot003.png)

![alt-text](Images - BJT/screenshot004.png)

- **Hardcore Equations**

  -   $ I_C = I_S \left(e^{\frac{V_BE}{V_T}} -1 \right)$
  -   $V_T=\frac{kT}{q}$ (thermal voltage) (25mV at room temperature)
  -   We can neglect that “-1”, because the exponential will be usually largely bigger than one 
  -   doping concentration in the emitter = $N_D$
  -   doping concentration in the base = $N_A$
  -   $Nd >> Na$
  -   $ n_p (0)$ = concentration of electrons injected from emitter to
  	base
  -   $p_n(0)$ = concentration of holes injected from the base to the
  	emitter

  ![alt-text](Images - BJT/screenshot009.png)
* **Finite output resistance**
  * Or *Early Voltage* (Early is just the name of someone)

  * Caused by base-width modulation effect

  * Non-perfect dependence of $ I_C $ with $V_{BE} $

  * Physical explanation:  increases the width of the depletion region of collector–base junction, decreasing the base width and increasing $I_C$ proportionally 
    ![1541867435356](Images - BJT/1541867435356.png)

  * $V_A$ is a parameter of the transistor (usually from 10 to 100V)

  * $r_o=\frac{V_A+V_{CE}}{I_C}=\frac{V_A}{I_C^{'}}$, where IC is the value of the collector current with the Early effect neglected

  * Tacking that in count, the collector current will be:
$$
\begin{aligned}
I_C = ??????
\end{aligned}
$$
### **Breakdown region**

* Due to avalanche efetc in the junctions 

* Usually greater than 50V

* The measure is done in open circuit (that is th best case, not the worst)

* $BV_{CBO}$: Breakdown of collector-base juntion 

* $BV_{CEO}$: Breakdown of the collector-emmiter (also called *sustaining voltage*) 

	![1541945704916](Images - BJT/1541945704916.png)

* $BV_{CEO}$ is about the half of $BV_{CBO}$. ==WHY?????==

* Breakdown of the CBJ is not destructive

* Breakdown of the EBJ (that happens and 6~8V) will reduce beta permanently). EBJ can be used a zenner diode, if dont care for the beta. 

# Circuits

### Large signal model

* Podemos interpretar como dois diodos (modelo de #Hebert S’mals#), mas o equacionamento acaba sendo bem fodido.
* apply for any positive value of v BE
* nao use esse modelo para resolver o circuito na mão
* Basic equivalent circuit
  * We can model in terms of the physical values (like in the image), or in term of $H_{FE}$ (the controled souce will be $H_{FE} I_b$)

![alt-text](Images - BJT/screenshot006.png)

* **Improved equivalent**

  * Move the diode to base (dividing by beta)
  * We can include the finite output resistance:

  ![1541867802716](Images - BJT/1541867802716.png)

* **Saturated model**

  * neglected the saturation resistance

### Current driver

* We can cascate several driver to increse gradually
* **Load in the colector**
  *  Normal
  *  Easier to analise
  *  If the load had a inductive characteristic (like a motor), we should put a supression diode (called in portuguese *diodo de roda livre*)

![Image result for bjt driver](Images - BJT/_scaled_driverbjt-1541876752235.jpg)

  * **Load in the emmiter**
    * better utilize the current
    * Usually used just to drive leds in embedded systems
    * If $ led voltage + 0.7 = ucontroler voltage $, we may not need the base resistor 
    * if $ led voltage + 0.7 > ucontroler voltage $, that circuit CANT be used 

![Image result for bjt driver emitter](Images - BJT/OnVwa.png)

* **Ponte H**

  * To drive bidirectional motors

  	![Image result for ponte H](Images - BJT/HBridge.png)
* **Darlinton topology**

  * $\beta=\beta_1\beta_2$
  * Dissipa mais potência no transistor, pois haverá no mínimo 0.8V Vce

### Current Sources

* **Topology 1**

  * Use a zenner diode to keep the current constant

  * Highly deppends on temperature 

  	![Image result for bjt current source zener](Images - BJT/transistor-constant-current-circuit-02.gif)

* **Topology 2**

  * Loman passou isso em sala e eu nao encontrei na internet

  * Desehei no falsted, mas mas valores estão errados

  * O resistor da esquerda deve ter valor elevado (ele serve para diminuir a corrente de base)

  * Ambos funcionam na regiao ativa-direta, ou só o da esquerda?

  * $I_L=\frac{0.7}{R_1}$

  	![1541947260687](Images - BJT/1541947260687.png)

* **Topology 3**

  * Se $I_C >> I_B$, então $R\approx \frac{V_{cc}-0.7}{I_C} $

  * $R_{L max}=\frac{V_{CC}-0.3}{I_C}$

    ![img](Images - BJT/chptr11-f4.png) 
    
### Linear regulators
* Vin mínimo deve ser maior do que Vout

* Topology 1
  * Bem sensível à regulação de linha (pois altera a corrente sobre o dido)
  * $V_{out}=V_Z-V_{BE}$
  * Calcule o resistor para que a corrente sobre o zener (podemos desconsiderar a corrente de polarização transistor) seja nominal/adequada.

* Topology 2
  * Podemos escolher o valor do zenner com maior flexibilidade
  * Podemos colocar um trimpot para permitir um ajuste fino
  * $V_z=\frac{V_{out}R_2}{R_1+R_2}-0.7$

* Topology 3
  * Nos tornamos independentes da variação de $V_{BE}$
  * Trimpot de ajuste deve ser muito grande
  * Podemos usar uma topologia darlongtown para diminuir a corrente de saída do ampop 

* Bonus: proteção conta sobre corrente

### Other usual circuits

* Polarização de base com divisor resistivo: faça equivalente de thevenin

* **Logic inverter**

  * blabla

    ![Image result for BJT inverter](Images - BJT/img.png)

* Espelho de currente

  * Possui um resistor no emissor 