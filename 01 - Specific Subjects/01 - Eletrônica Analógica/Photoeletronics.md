# Photonics

* Ciência de trabalhar com a luz
* Como “escravizar” a luz para nossos propósitos
* **Light sources**
  * light-emitting diodes (LEDs)
  * superluminescent diodes
  * lasers
  * fluorescent lamps
  * cathode ray tubes (CRTs)
  * plasma screens.
* **Transmission media**
  * Glass fiber
  * plastic optical fiber
  * photonic crystals
* **Detection**
  * photodiodes 
  * charge coupled devices (CCDs)
    * Used mainly in digital cameras
  * Solar cells

# LED

​	
are useful in special electronic circuit applications as well as in applications that involve light,

such as light-emitting diodes (LEDs).

# Photodiodes

* convert light signals into electrical signals.

* A tensão de junção V0 aumente com o fornecimento de energia luminosa  

* The more light striking the junction, the larger the reverse current


* **Chemical vision**

  * Photons impacting the junction cause covalent bonds to break

  * The liberated electrons goes to the *n* 	side and the holes to the *p* 	side


* **Anotations**

  * PIN diode → photovoltaic cell

  * OPT 101 → integrado que implementa o seguinte 	circuito (medidor luminoso com fotodiodo):

![image-20200402162532424](Images - Photoeletronics/image-20200402162532424.png)

# Phototransistor

* Will be register here just to keep the context of optoelectronic

* more sensitivity to light than a photodiode

* cannot turn on and off as fast (microseconds, while photodiodes are in nanoseconds)

* Can conduct more currente (miliamperes, while photodiodes can microamperes)

# Optocoupler

* See in Isolation Structures