https://e2e.ti.com/blogs_/b/powerhouse/archive/2015/10/01/power-tips-how-to-design-a-robust-series-linear-regulator-with-discrete-components 
https://www.eetimes.com/document.asp?doc_id=1272218

https://www.youtube.com/watch?v=7JoN3sOyZ8c&list=PLZvLSclgk4yLq0MbCSVqa8ECBgpRtiMFo&index=1


[15:02, 11/15/2018] Eng Felipe Broering: Fazer a regulação de tensão com mosfet é complicado, mas não lembro ao certo o problema
[15:03, 11/15/2018] Eng Felipe Broering: Lembro que um dos problemas, é que os mosfets que nós temos disponíveis no almox, são mosfets de potência, com uma região linear bem pequena, oque dificulta a regulação
[15:05, 11/15/2018] Eng Felipe Broering: Outra coisa, era a polarização do mosfet, como a tensão aplicada ao gate é relacionada ao source, se a tensão vgs que você precisa é 10V e a tensão de source é 10V, vc precisa de uma tensão de 20V para aplicar no gate
[15:05, 11/15/2018] Eng Felipe Broering: Aí trás complicações, que resolvemos usando um dobrador de tensão
[15:06, 11/15/2018] Eng Felipe Broering: Em eletrônica de potência, se usa drivers para fazer o acionamento desses mosfets, basicamente o driver tem uma tensão isolada para fazer esse acionamento, que consegue então ter como referência a tensão de source

dissipador: menor do que 4ºC/W

lm358
