# Geral amplifiers

-   Ideally linear (with exceptions in some applications)

-   So, an amplification by factor A can be described as:

$$
{v} _ {0  (t  )} = A {v} _ {i  (t )}
$$

-   The gain can be described in decibels

$$
Gain _{dB}= 20 log  | A  |
$$

-   Schematic symbol:

![img](Images - Estruturas Amplificadoras/1557690229608.png)

-   Efficiency of the amplifier:

![img](Images - Estruturas Amplificadoras/1557690253409.png)

-   Signal flow is unidirectional, from input to output.

-   **Saturation**

    -   the amplifier transfer characteristic remains linear over only a
        limited range of input and output voltages.

    -   Beyond some limit, the output will be clipped off

-   **Cascated amplifiers**

  * One amplifier after other

  * The stages are usually not identical; rather, each is designed to serve a specific purpose
  $$
  A_{final}=A_1*A_2*…*A_n
  $$

-   **Types of amplifiers**

    -   ?

![img](Images - Estruturas Amplificadoras/1557690332250.png)

## Frequency response

-   Whenever a sine-wave signal is applied to a linear circuit, the
    resulting output is sinusoidal with the same frequency as the input.
    Can be be shifted in phase.

-   But the gain can variate with frequency

- The frequency response can be expressed by the Transfer function:

  $$
  T(w)=\frac{V_o(w)}{V_i(w)}
  $$
  
- Magnitude response → how the gain variates (usually in dB)

- Phase response → how the shift variates

- Bandwidth → range of working (constant and high good gain)

- Typical magnitude response of an amplifier

  - an tipical amplifier have decouple and bypass capacitors (*external capacitaces*) and parasidic capacitaces in the transistor (*internal capacitances*)
  - The bypass capacitor cause the gain reduction in low frequency (cause he is in series with the signal)
  - The transistor capacitantes cause the gain reduction in high frequencies (cause they are in parallel with the signal)

  ![img](Images - Estruturas Amplificadoras/1557690420799.png)

  * a frequencia de corte inferior é calculada pela freuquencia em que o capacitor de decouple possui a mesma impedância de entrada do circuito, ou seja:
    $$
    w_1=\left.w\right|_{X_c=Z_{in}+R_s}
    $$
  
    > Zin = amplifier imput resistance
    >
    > Rs = voltage source impedance
* Botar uma carga resistiva não muda a resposta e frequência

## Amplificadores transistorisados


* Biasing: process of establishing DC operating point in the device
* Escolhemos o ponto Q que permita a maior excursão do sinal AC 
* Quanto menor a impedância de saída, menor o ganho 
* Dividimos em duas etapas: análise CC e análise CA 
* **Couple capacitor**
  * Allow us to couple an ac signal into an amplifier without disturbingits Q point.
  * We put coupling capacitors to separe the DC bias from the possible AC signal that we want to process. 
  *  its reactance must be much smaller (10x less) than the resistance at the lowest frequency of the ac source.
* **Bypass capacitor**

  * used to create an ac ground
  * Reactance: same rule of above (10x less)
  * Putted in the source/emitter, to increase the ac current and the voltage output range
  * Podemos separar resistencia source/emitter em duas, e dar o by pass só no de baixo (para estabilizar o ganho ac com a temperatura). Regra empírica: $R_{inferior}=4R_{superior}$
* **Análise CA**
  * Todo os capacitores são curto-circuitos 
  * Ganho (G, ou $A_V$): $\frac{V{out}}{V_{in}}$ 
  * Impedância de entrada () 	→ vista pela fonte de sinal
  * Impedância de saída ()	→ vista pela carga 

## Multistage amplifiers

* **Pre amp**
  * amplificado de tensão (pouca corrente, muito ganho)
  * Alta qualidade, baixa potência 

## Dicas de projeto

* Coloque o ponto quiecete no meio da reta de carga
  * ou seja, $V_{CE}=V_{CC}/2$
  * Permitimos assim as maior excursão simétrica do sinal
  * A tensão absoluta de $V_C$ deverá estar um pouco acima de $Vcc/2$ (pois a operação não é linear om $Vce$ proximo de zero). Recomenda-se deslocar esse ponto pra cima aproximadamente $Vcc/10$, ou alguns volts caso a alimentação for baixa  
  
* **Medindo Zout e Zin na prática**

  * Calcularemos como um divisor de tensão, a partir do modelo CA de amplificador (quadripolo)

  * Precisamos saber a impedancia da fonte de sinal (e.g, gerador de função) e da carga (e.g, speaker)
    $$
    Z_{in}=r_{int}\frac{v_{in}}{v_{open}-v_{in}}
    $$

    > rint = resistencia inerna da fonte
    >
    > vin = tensão com o sistema conectado
    >
    > vopen = tensão no gerador não conectado
    
    $$
    Z_{out}=Z_{load}\left(\frac{V_{open}}{V_{out}}-1\right)
    $$

> vout = tensão com o sistema conectado
>
> vopen = tensão no amplificador não conectado

## Amplifiers for RF

* Para trabalhar em MHz, GHz
* o que tem dentro das implementações comerciais: componentes melhores, mas os princípios são os mesmos
* nessas condiçẽs o ganho é muito relevante: com um ganho baixo dá pra trabalhar em frequencias mais altas; geralmente se trabalha com coisas integradas; a maior dificuldade de bandwidth são as não liearidades dos componentes discretos, o ampop consuma trabalhar bem em frequências abaixo da máxima (FT é um passa baixa)
* geralmente é tudo já integrado
* Se faz também muitas contruçao em termos de *metal*, e não resistor/capacitor indutor; por exemplo, filtro: cavidade ressoante; caixinha metalica em que s'algumas frequencias conseguem se estabelecer
* amplificadores sao do tipo *amplificadores sintonizados;* geralmemte sao circuitos LC

## Technological comparison

* **BJT**
  * (+) Tensão de offset baixa e estável
  * (+) Baixo ruído (devido à impedância de entrada ser baixa)
  * (+) Altos ganhos
  * (-) Altas correntes de polarização
  * (-) Baixa resistência de entrada
* **JFET**
  * ?
* **BiFET**
  * transistores de entrada do tipo JFET (canal P), mas amplificação com BJT
  * (+) Altas impedâncias de entrada

  * (+) Baixas correntes de polarização

  * (+) Altos Slew-rates

  * (-) Tensão de offset alta e instável

  * (-) Especificações ruins de CMRR e Ganho de MA

  * (-) Altas tensões de ruído.
* **CMOS**
  * (+) Operação com fontes simples;
  * (+) Aplicação com alimentação baixa (micropower);
  * (+) Alta impedância e baixas correntes de polarização;
  * (+) Saídas rail-to-rail.
  * (-) Tensões de offset mais alta (porém mais estável);
  * (-) Especificações ruins de CMRR e Ganho de MA;
  * (-) Altas tensões de ruído.

![image-20200402160120198](Images - Estruturas Amplificadoras/image-20200402160120198.png)


# BJT ampliers

* Determinamos o ponto Q de operação (usando as condições CC, Ic e Vce)


* **Load line**

  * extreme points: saturation (Vce=0) and cutoff (Ic=0)

  * will define the possible operations regions 

    ![1554064873124](Images - Estruturas Amplificadoras/1554064873124.png)

  * Operating point

    * or *quiecent point*
    * Quiescent means quiet, still, resting
    * Specific point in the load line, that will be the one in witch the transistor will operate in DC (and the point at which the AC signal will oscillate )

* **ac resistance of the emitter diode ($r_e$)**

  * $r_e=\frac{v_{be}}{i_e}$

  * Aproximation using solid state physics: $r_e=\frac{25mV}{I_E}$

  * Esse 25mV vêm do valor de $V_T$ à temperatura ambiente

  * Ganho de tensão no ponto de operação ($g_m$)

    * Parâmetro análogo, porem matematicamente mais conveniente

    * $g_m=1/r_e$

    * Inclinação da reta $I_C \times V_{BE}$, ou seja:

      ![1554073609946](Images - Estruturas Amplificadoras/1554073609946.png)

* **Voltage gain ($A_V$)**

  * or $G$
  * $A_V=Vout/Vin$
  * Should be deduced to each topology 

* **Current gain ($A_i$)**

  * $A_i=Iout/Iin$

* :red_circle:Relationships Between R and H Parameters (malvino pg303)

  * The h parameters model the transistor as a quadripole, a mathmatical systemic external description (different than our internal description using the internal resistences)

* :red_circle: The Loading Effect  of Input Impedance (pg 308)

## Smal signals model

* **Modelo pi**

  * Fonte controlada por tensão

  * explicita a resistênciade entrada

  * $z_{in}=r_\pi=\beta r_e=\beta/g_m$ 

    ![Related image](Images - Estruturas Amplificadoras/220px-H_pi_model.svg.png)

* **Modelo T**

  * or *Ebers-Moll model*
  * Fonte controlada por corrente
  * $z_{in}={v_{be}}/{i_b}=\beta r_e$
    ![1553558426438](Images - Estruturas Amplificadoras/1553558426438.png)

## Circuits

* O Boylestad pg 394 trás o equacionamento de várias topologias

* **3 resistor bias**

  * source no gnd
  * $G=-g_m(R_C || r_o || R_L )$

* **voltage-divider-biased (VDB)**

  *  We also need to use a bypass capacitor between the emitter and ground. Without this
    capacitor, the ac base current would be much smaller. But with the bypass capac-
    itor, we get a much larger voltage gain

  * The capacitor should be small enough to mantain the source voltage pure DC (all of the ac voltage will appears across the base-emitter diode)

    ![1557078123980](Images - Estruturas Amplificadoras/1557078123980.png)

* **4 resistors polarization**

  * Common-emitter (CE)

  * Class A (Conducts: 360°)

  * O resistor sem bypass no emissor serve para estebailizar o sinal AC em relação a temperatura (negative feedback)

  * Also increases the input impedance

  * Also reduces the distortion of large signals (because will be a fixed value comapared to $r_0$)

  * Diminui o ganho (o que geralmente é desejado)

  * Maximum efficiency: 25%

  * Max output range < Vcc

  * $A_V=\frac{R_C}{r_e+r_0}$

    ![1553356920946](Images - Estruturas Amplificadoras/1553356920946.png)

* **Push-pull**

  * Common-emitter (CE)

  * Current amplifier

  * Class B

  * Max output range = Vcc

  * Maximum efficiency 78.5%

  * the collector current flows for only 180° of the ac cycle

  * On the positive half-cycle of input voltage, the upper transistor conductics, acting as immiter follower (output voltage = input voltage)

  * Same to lower transistor in the negative cicle

  * Podemos usar transistores darlington pra dar ganhos ainda maiores

  * The most difficult thing about designing a Class-B amplifier is setting up a stable Q point at cutoff, because there is not series resistence (dc load line is vertical). we can put small resistor (so small that we genarally do with wire, with resistance between 0.3 and 1 ohm) between the two emmiters, deixando os transistores levemente polarizados.

    ![img](Images - Estruturas Amplificadoras/1553362402733.png)

  * Crossover distortion: no current flows the transistor when the signal is less than 0.7 V, so it will be clipped:

    ![img](Images - Estruturas Amplificadoras/1553361675813.png)

  * <u>Crossover correction</u>

    * Also called class AB
    * We need to apply a slight forward bias to each emitter diode (locating the Q point slightly above cutoff). 
    * Essa pequena polarização é na odem de 500 a 600mV
    * With that correction, we can say the amplifier is class AB
    * ==e com o resistor em serie? já não resolve?==

  * <u>Diodes variation</u>

    * The polarization with diodes are good because compensate the thermal variability
    * Better if the Vbe  curve is matched with the transistors
    * Temperatura sobre -> transistor conduz mais mas diodo diminui a tensão, de forma que diminui a condução do transistor
    * *Tip*: put the diodes in the same heasink of the transistors (with thermal paste)

  * <u>Driver variation</u> 

    * Puts a driver in the input, because just the push-pull dont have gain

      ![img](Images - Estruturas Amplificadoras/1553365351022.png)

    * By adjusting R2, we can control the dc emitter  current through R4. This means that Q1 sources the biasing current through the
      compensating diodes

    * voltage gain of the drive (aproximation):
      $$
      A_V = \frac{R_3}{R_4}
      $$

  * <u>Feedback variation</u>

    * :red_circle:

* **Class C** 

  * Common-emitter (CE)
  * Conducts , 180°
  * Distortion: Large
  * Maximum efficiency < 100%
  * Relies on tuned tank circuit
  * MPP 5 2 (VCC)  



# JFET Amplifier

* **Transconductance ($g_m$)**

  * ou $y_{fs}$

  * how effective the gate-source voltage
    is in controlling the drain current

  * Unit: mho or siemen (ampere/volt)
    $$
    g_m = \frac{i_d}{v_{gs}}
    $$

  * Transcondutance curve: graph of ID versus VGS

  $$
  I_D=I_{DSS}\left(1-\frac{V_{GS}}{V_{GS(off)}}\right)^2
  $$

  

  ![1554061711287](Images - Estruturas Amplificadoras/1554061711287.png)

  * Determinando o ponto de gm
    * Escolhems no gráfico Id x Vgs
    * preferencialente “no meio”
    * Se for muito proximo do eixo y (Vgs=0) a transdutância aumenta muito, além de não permitir uma excursão de sinal muito grande
    * Se for muito longe (Vgs negativamente grande), a operação fica muito não-linear

* $r_d$
  * característica ohmica do dreno.
  * Usado no modelo de pequenos sinais
  * Obtido no Datasheet

## Circuitos

* O Boylestad pg 448 trás o equacionamento de todas as topologias

* Geralmente podemos aproximar $I_D \approx V_G/R_S$
* When more accuracy is needed in determining the Q point for a voltage-divider bias circuit, a graphical method can be used.

# Amplificadores de instrumentação

* Ganho controlado por um único resistor

  ![image-20200402161130221](Images - Estruturas Amplificadoras/image-20200402161130221.png)
  $$
  A_V=\left(1+\frac{2R_1}{R_G}\right)*\left(\frac{R_3}{R_2}\right)
  $$
  
  
  
* Características

  * elevado CMR;
  * elevada impedância das entradas (entradas separadas)
* baixo offset; e
  
  * baixa corrente de polarização.


* Circuitos e sugestões

  * Opções integradas são as mais variáveis, pois garatimos que as resistências são simétricas e diminuimos o ruído

  * SSM2141

  * INA133

  * LMH6550, Diferencial bipolar:

    ![image-20200402155155293](Images - Estruturas Amplificadoras/image-20200402155155293.png)




* Escolha de instrumentação
  * Banda passante
  * Valor de SNR
    * O quanto odispositivo consegue amplificar de sinal em relação á quanto ele amplifica da ruido
    * abaixo de 80db: ruim pra instrumentação
    * 110db: bom pra audio 
  * Tensão de offset
  * Slew-rate
  * Valor de CMRR
  * Impedância de entrada

# Amplificadores logaritmicos

* Utilização: onde exista a necessidade
  de se comprimir a faixa dinâ1nica de uma informação ou medição a ser processada, por exem-
  plo: medidores de VU (unidade de volu1ne de áudio), instrumentação nuclear, equipan1entos
  de radar, etc.

* **CIrcuito básico**

  * (-) extremamente dependente da temperatura.

  * :book: antonio junior 124

    ![image-20210222094406097](Images - Estruturas Amplificadoras/image-20210222094406097.png)

    ![image-20210222094413888](Images - Estruturas Amplificadoras/image-20210222094413888.png)

* Integrados

  * (+) fucking precisos
  * correção de temperatura e etc
  * ICL8048

# Outras anotações

* ganho DC: Vdrain/Vbase = ganho potencial máximo

* **Class D**
  * Works switching
  * Excelent performance
  * low spatial volume
  * usando em, por exemplo, celulares