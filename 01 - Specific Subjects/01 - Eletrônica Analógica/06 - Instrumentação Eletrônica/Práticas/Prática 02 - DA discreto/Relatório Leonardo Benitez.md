**Aluno**: Leonardo Santiago Benitez Pereira 

# Introdução
Trata-se de um circuito R2R com ampop governado pelas seguintes equações:

$$
A = Vcc*(\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}+)=4.68V
$$

$$
H=\frac{A}{2^{4}}=0.3125
$$

$$
L = \frac{1}{f}=1ms
$$

$$
T = 2^4 * L
$$



# Simulação
<img src="Captura1.png" alt="img" style="zoom:67%;" />

<img src="Captura2.png" alt="img" style="zoom:67%;" />

<img src="Captura3.png" alt="img" style="zoom:67%;" />