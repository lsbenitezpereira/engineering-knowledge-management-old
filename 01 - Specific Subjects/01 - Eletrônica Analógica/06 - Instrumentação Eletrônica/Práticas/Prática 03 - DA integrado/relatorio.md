# Simulação 1

Espera-se a corrente de referência em torno de $I_{ref}=\frac{5 V}{2k2\Omega}=2.27mA$ (condizente com a faixa operacional de $0mA$ à $4mA$).

Para a tensão de saída desejada, devemos ter $R_1=R_2=2k2\Omega$, obtendo o resultado abaixo:

<img src="Images - relatorio/image-20200911232054191.png" alt="image-20200911232054191" style="zoom:80%;" />

As correntes de saída vão de zero a $2.27mA$, conforme o gráfico:

<img src="Images - relatorio/image-20200911233718352.png" alt="image-20200911233718352" style="zoom: 50%;" />

Removendo a conexão entre a entrada não-inversora e $\overline {IOUT}$:

<img src="Images - relatorio/image-20200911232402491.png" alt="image-20200911232402491" style="zoom:80%;" />

# Simulação 2

Para a corrente de referência de $1.5mA$, devemos ter $R_2=R_4=\frac{15V}{1.5mA}=10k\Omega$

A Amplitude de saída $A$ será dada por $A=\frac{15V}{2^8}*2^4*G$

Para $A=10V$, precisamos de $G=10.66$ e, portanto, $R_1=R_3=106.66k\Omega$

Verificou-se que $A=9.39V$ e $T=30ms$, conforme segue:

<img src="Images - relatorio/image-20200912001534998.png" alt="image-20200912001534998" style="zoom:80%;" />

Corrente de saída:

<img src="Images - relatorio/image-20200912001227058.png" alt="image-20200912001227058" style="zoom:50%;" />

Se os 4 bits mais significativos forem usados, a conversão será de $2^4$ até $2^8$ (ao invés de $0$ a $2^4$). Portanto, A será $\frac{2^8}{2^4}=16$ vezes maior e $T$ será o mesmo. Para manter a tensão de saída de 0 a 10V, devemos ajustar $R_1=R_2$ para um valor 16 vezes menor, obtendo assim a seguindo tensão de saída:

<img src="Images - relatorio/image-20200912003150416.png" alt="image-20200912003150416" style="zoom:80%;" />