Based on [NI - Data Acquisition Using LabVIEW NXG](https://mythinkscape.com/courses/5574/labs)

Fiz só o começo do curso, pqp, muito chato

# Conceptualization

* aplicações de teste, medição e controle
* rápido acesso ao hardware
* abordagem de programação gráfica (G)

# Data aquisition

![image-20200320122235031](Images - LabView/image-20200320122235031.png)

* Hardware pinout

  * Al sense: common reference (not ground!)

* Grounding terminal configurations

  * never ground both

  * non referenced single ended

  * referenced single ended

  * pseudodiffenretial

    ![image-20200320125056584](Images - LabView/image-20200320125056584.png)

  

* Chooisng hardware

  * N of channels
  * speed of aquisiion
  * code width
    * smallest detectable change in signal
    * defined by resolution (in bits) divided by the range (in volts or other physical unit)
    * code width = inputRange/2^ADCresolution
  * max and min
  * accuracy
    * uncertainty in a measurement
    * ADC resolution = log2(inputRange/accuracy)
  
  
  

## Comom measurements

* Voltage
* current
  * Shunt ammeter (corrente passa pelo sensor). Resistor shunt deve ficar o mais proximo do grund possivel (para que a tensao absoluta seja baixa)
  * CUrrent transformer
  * rogowski coild (just rate of change)

## ADCs

* Data transfer


* DMA
* Interrupts
* rprogrammed IO
* USb bulk

# Interface

* Measurement panels