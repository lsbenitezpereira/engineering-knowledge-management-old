# Imaging

* **Computational photography**
  * digital image capture and processing technique
  * how to work with images
* **sensor model**
  * object model: describes the objects that inhabit the visual world
  * Rendering model: describes the physical/statistical processes that produce the stimulus

## Camera models

* **Basic pinhole camera**

  * hole = apperture

  * ligh sensibilizes a film

  * distance between hole and film determines the focus

  * if hole is too big, image gets blurred

  * if hole is too small, you’ll have problems with difraction

  * image plane = projective plane

  * $-x=f\frac{x}{z}$

    ![image-20200606133536566](Images - Imaging/image-20200606133536566.png) 

* **Basic lens camera**

  * use a lens instead of hole

  * you can have an apperture after the lens to improve the iamge

  * changing position of lens can change the focus point

  * Thin lens equation: $\frac{1}{p}+\frac{1}{q}=\frac{1}{f}$, where $p$ is the distance between object and lens, $q$ is the distance between image and lens, and $f$ is the focus length of lens 

  * Problems with lens

    * different colors are reflected different by lens  

    * vignetting: corners tend to be darker

    * tangential distortion:  lens not being exactly parallel to the imaging plane

    * Radial distortion:

      ![image-20200607155832398](Images - Imaging/image-20200607155832398.png)

* **Model 2**

  *  we swap the pinhole and the image
     plane

  * easy to do math

    ![image-20200606144607090](Images - Imaging/image-20200606144607090.png)

## Camera properties

* **Depth of field**
  * how much the focus change when the object is slightly off the focal distance
  * “spead” of the focus
  * controled by the apperture (in the lens camera)
  * less depth = narrow focus = wide apperture
* **Field of view (FOV)**
  * like zooming
  * deppends of the focal length
  * Effetcs of perspective: http://www.stepheneastwood.com/tutorials/Tutorials_Lens_Perspective.htm
  * Dolly zoom
    * invented by hichcock
    * move closer, enlarge FOV

## Techinologies

* HDR technology: ?
* Charge-coupled device (CCD)
* NMOS active-pixel sensor (APS): 
* CMOS active-pixel sensor (CMOS sensor)

## Image properties

* **Edge**	

  * Curves in the image plane across which there is a “significant” change in image

  * Primal skecth: representatin of the edges of the objetc

  * Different kinds of edges: (1) depth discontinuities; (2) surface orientation
    discontinuities; (3) reflectance discontinuities; (4) illumination discontinuities (shadows).

    ![1568048813235](Images - Imaging/1568048813235.png)

* **Texture**

  * Visual “feel” of a surface
  * largely invariant to changes in illumination
  * Edge detection is harder in textured objects

* **Color**

  * About physical properties of color, see is *Physics - Optics*
  * About the computational representation of colors, see in specific section
  * We’ll write here about the computational aspects of light
  * ==move from Web Desing==

* **Shape**

  * pq space? Define a orientation of the surface?
  * Shape from shading
    * how to map from image light intensity to the 3d object surface
  * Shape from photometric stereo
    * take several pictures with diferent lighting
    * same camera, same object, same pose
    * The direction of lighting should (always?) be known
    * need at least 3 photos 

## Camera stabilization

* mechanical or optical (like [this one](https://i.imgur.com/legsOG4.gifv) wiht two mirror)

## Others

* high dynamic range (HDR)
  * take several photos wit different brightness, blend them

# Color

## Properties

* Saturação → escurecimento/luminosidade da cor
-   Luminance?

## Representation

* Or *color space*

* We humans discriminate red and green better than blue, so some representation have more bits to store them

* Same about intensity

*   **Principle of trichromacy**: any spectral energy density (no matter how complicated) can be represented as another spectral energy density consisting of a mixture of just three colors, such that a human can't tell the difference between the two

*   **RGB**: aditivo (branco são todas as cores, preto nenhuma)

    * How to convert RGB to ogther representation, like YUV?

*   **CMYK**: subtrativo (preto são todas as cores, branco nenhua). Uso as cores ciano, magenta e amarelo. Usada em impressoras

*   **HSV**:

    -   Hue value saturation

        ![image-20201018160929798](Images - Imaging/image-20201018160929798.png)

*   **HSL**

    * similar, but different

*   **YUV**

    * specially useful to separate colors
    * eaasier to cluster colors
    * the luminance is separate (Y variable), which specially good to represent things for humans (we can discriminate intensity much better than color)

*   **Gammut**

    * Subset of the whole color space that a device/software can represetn
    * 

























































