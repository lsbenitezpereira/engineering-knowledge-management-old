# Conceituação

* Aqui estamos interessados em processamento *eletrônico* de energia, mas muitas dessas conversões tambem podem ser feitas de forma eletromecânica





![image-20201110184809191](Images - Conversores/image-20201110184809191.png)



<img src="./Images/Conversores/media/image1.jpeg" alt="Resultado de
imagem" style="zoom:33%;" />

* ==O que significa ser um conversor **estáticco**?==
  
* **Sftwares**
  
  * Psim é bom para eletronica de potenca
  * power stage designer, da TI: bom pra caralho, já calcula várias coisas sozinho; basicamente, já tem várias planilhas precalculadas e esquemáticos desenhados
  
* **bibliografia**
  
  * eletronica de potencia, analise e projeto de circuitos, daniel w. hart, primeira versão em espanhol
  
* **Nomemclatura**

  * f = frequencia de chaveamento
  
  * switching period ($T$): 

  * Duty cicle ($D$):
  
    ![image-20201121225513246](Images - Conversores/image-20201121225513246.png)

## Circuitos de controle

* muito usados para controle dos CC-CC

* geralmente usamos margem de fase em torno de 45º

* Se o controle for bom, corrige até a oscilações da entrada devido à uma retificação meia-boca

* o crcuito de PWM geralmente é analógico

  * Um dente de serra é zerado
  * valor comparado com uma tensão analógica de referência
  * onda quadrado (saída do comparador) na saída

## Circuitos de qualidade

* **Correção de fator de potência (PFC)**

  -   Bota alguma coisa antes do do filtro, para mudar o formato da corrente

  -   Boost PFC:

      ![image-20210204205946470](Images - Conversores/image-20210204205946470.png)

  -   Pode estar em condução contínua ou não

* **filtro de linha**

  * ?

## Semicondutores de potência

* A empresa semikron vende vários drivers para acionar 
* **SCR**
  * ?
  * o acionamento geralmente precisa de uma corrente na ordem de 5% da corrente principal
* **Triac**
  * controlado
  * MOC2030: CI já com opto na entrada
* DIAC
  * segura até uma certa tensão, depois libera
  * geralmente vem em um encapsulamento de diodo

# CC-CC

* Mais componentes -> masis dificil de controlar

* tente casar a caracteristica da carga (tensão estável ou corrente estável) com a saída do conversor

* qual é o melhor depende de cada caso

  ![image-20201201192028545](Images - Conversores/image-20201201192028545.png)

* **Condução descontínua**

  * Corrente nos indutores chega a zero
  * Geralmente desejaremos trabalhar em condução contínua
  * quanto mais descontínuo, mais a tensão de saída aumenta
  * Conclulsão: sempre precisa de carga, senão tudo queima pro sobretensão 
  * (-) perioso, a tensão facilemente fica zuada
  * (+) mais facil de controlar

* **Resistencia serie do capacitor**

  * aumenta o ripple na saida, especialmente para conversores com saída em tensão
  * uma boa solução é botar capacitores em paralelo

* **Interleaved converters**

  * basicamente, dois conversores em um, operando juntos
  * duas chaves, dois indutores, dois diodos (ou tudo isso 2x para os mais complexos)
  * (+) menos ondulação de correntes
  * (+) cada chave fica responsável por metade da corrente
  * (+) melhor compatibilidade eletromagnética?

* **Multiple outputs**

  * Geralmente podemos aproveitar parte do circuito para duas fontes, como a parte pré trafo
  * só dá pra fazer isso com conversores isolados? 
  * tensões controladas pela razão cíclica

  

## Buck

* abaixador

* switch + lowpass filter

* frequencia de corte do filtro passa baixa geralmente é na ordem de 10x menor o que a frequencia de chavamento, mas esse raciocinio não costuma ser usado durante o projeto

* filtro de segunda ordem

* Indutor em série com a saída

  ![image-20201201203414781](Images - Conversores/image-20201201203414781.png)

* modo operação contínua: sempre conduzindo, corrente nunca chega a zero; é o que consideraremos aqui

* valor necessário do indutor para o ripple de corrente desejado ($\Delta i_L$):

  ![image-20201119210143618](Images - Conversores/image-20201119210143618.png)

* cuidado: foi assumido a tensão praticamente constante

* O ripple de tensão pode ser obtido por:

  ![image-20201119210635720](Images - Conversores/image-20201119210635720.png)

* **state 1**

  * switch closed, diode off
  * corrente no indutor cresce linearmente 
  
* **state 2**
  
  * switch open, diode on
  * se o ripple for pequeno, podemos considerar o capacitor como uma fonte CC para fins de análise
  * For the preceding analysis to be valid, continuous current in the inductor must
    be verified
  
* **Design considerations**
  
  * geralmente escolhese um ripple de tensão menor que 2%
  * geralmente escolhesse um ripple de corrente em torno de 70% (fodase, só influencia o capacitor, além de que valores altos precisam de indutores caros)
  * Capacitor grande -> transitório muito longo; controle em macha fechada difícil
  * Indutor grade -> caro
  * high switching frequencies are desirable to reduce the size of both the inductor and the capacitor, but have increased power loss in the switches
  * 500 kHz is a usual value
  * we can also use a mosfet instead of a diode, to have lower losses
  
* **Non idealities**

  * A resistência série do capacitor e indutor não costuma influenciar muito no Buck

## Boost

* Elevadores

* consegue elevar até umas 5x a tensão (D=0.8), acima disso não funciona bem 

* o rendimento é geralmetne acima de 90%

* ![image-20201124184811454](Images - Conversores/image-20201124184811454.png)

  ![image-20201124185739422](Images - Conversores/image-20201124185739422.png)

  ![image-20201124190718179](Images - Conversores/image-20201124190718179.png)

  ![image-20201124190730717](Images - Conversores/image-20201124190730717.png)

* **State 1**

  * Switch closed, diode off
  * a carga é sustentada pelo capacitor
  * indutor acumula energia, corrente aumentando

* **State 2**

  * Switch open, diode on

* **Non idealities**

  * a resistencia do capacitor influencia bastante, deve ser pequena

## Buck Boost

* Elevador e abaixador

* inverte a tensão de saída

* (-) costuma precisar de um indutor maior

* (-) costuma ter um rendimento menor que buck ou boost

  ![image-20201126205003250](Images - Conversores/image-20201126205003250.png)

  > Cuidado: o capacitor nesse esquemático está invertido

![image-20201126205605915](Images - Conversores/image-20201126205605915.png)

![image-20201126205718511](Images - Conversores/image-20201126205718511.png)

![image-20201126210503840](Images - Conversores/image-20201126210503840.png)

## Isolados

* usam transformador

* transformador tenta manter o fluxo, de forma análoga ao indutor sozinho

* quando para de 

* simbologia para indutores acoplados (aka transformador): redondinho é primário, quadradinho é secundário

* carrega pelo primário, descarrega pelo secundário

* (+) isolado eletricamente

* (+) podemos controlar a relação de transformação diretamente pelo relação do transformador, mesmo quando as tensões forem muito diferentes

* (+) podemos trabalhar com tensões e correntes altas com mais facilidade (ex: tensão alta na entrada, abaixa, e então os compoenentes do secundário não precisam de uma isolação tão alta)

* (-) a realimentação precisa ser isolada (para controlar a chave)

* (-) um pouco mais de perdas, pelo trafo

* (-) o trafo de alta frequencia é geralmente dificil de encontrar comercialmente

* razão cíclica de 40% a 50% costumam ser mais eficientes, e controlamos o resto pelo trafo

* razão cíclica não pode ser maior que 50%

* em um trafo real a corrente costuma ser um pouco maior que o ideal, por causa da corrente de magnetização (que costuma ser da ordem de 10%)

* [Excelent “tutorial” about transformers with square waves](https://electronics.stackexchange.com/a/307081)

* **Flyback**

  * similar ao buck-boost

  * é o mais usado para fontezinha de celular, e até fontes chaveadas em geral

  * costuma ser mais usado no modo discontínuo do que no contínuo

  * (+) simples

  * (+) opera tanto com saída negativas quanto positivas

  * (+) é fácil botar várias saídas
  
  * (-) rendimento não muito bom

  * energy was
  stored in Lm when the switch was closed and transferred to the load when the
    switch was open
  
  * Equaçẽos para o modo contínuo:
  
    ![image-20201210211634607](Images - Conversores/image-20201210211634607.png)
  
    ![image-20201210211831191](Images - Conversores/image-20201210211831191.png)
  
* **Forward**

  * Similar ao buck

  * transformador na entrada

  * coloca um circuito de desmagnetização (descarrega a corrente), pois a onda de entrada possui um valor médio

  * bota mais um diodo na saída para garantir que o fluxo de energia é unidirecional (entrada -> saída)

  * não costuma ser utilizado para tensões elevadas

  * esforço na chave: o dobro da tensão de entrada

    ![image-20201219140101980](Images - Conversores/image-20201219140101980.png)

    ![image-20201219142422656](Images - Conversores/image-20201219142422656.png)

    ![image-20210206223606385](Images - Conversores/image-20210206223606385.png)

    ![image-20201219142532301](Images - Conversores/image-20201219142532301.png)

    ![image-20201219142727631](Images - Conversores/image-20201219142727631.png)
    
  * And the magnetizing current in the primary (loss) will be:

    ![image-20210206231757403](Images - Conversores/image-20210206231757403.png)

  * Average current in Lx is the same as the current in the load

  * Current in the primary winding of the transformer is the sum of the reflected current
    from the secondary and the magnetizing currents.

  * 

* **Push pull**

  * (-) transformador mais completo: 2 enrolamentos no primeario, 2 no secundario
  * (-) duas chaves na entrada
  * uma chave opera de cada vez, ambas pelo mesmo tempo
  * são usadas para gerar uma onda alternada
  * saída com retificador de onda completa
  * coloca um circuito de desmagnetização
  * esforço na chave: o dobro da tensão de entrada
  * é interessante para uns 200~500W

* **ponte completa**

  * (-) quatro chaves na entrada (mas em compensação os esforços são menores, igual à tensão de entrada, e portanto menos perdas)
  * (+) trafo menos completo que o pushpull: 1 enrolamento no primeario, 2 no secundario
  * (+) melhor eficiência de *todos*
  * saída com retificador de onda completa
  * acima de uns 500W ele é a melhor opção

* **meia ponte**

  * pulsa de forma similar ao ponte completa, mas a tensão que aparece no transformador é metade de tensão de entrada (o que é ruim)
  * só é vantagem se, por causa do circuito que vêm antes do conversor em si, os dois capacitores já existirem por algum motivo
  
* **Foward current**

  * Entrada em corrente, saída em tensão
  * razão cíclica precisa de maior que 0.5

## Others

* **Linear**

  * abaixador; LM780x

  * Geralmente implementados com algum circuito tipo esse (:book: antonio junio pg 137):

    ![image-20210222094901315](Images - Conversores/image-20210222094901315.png)

* **Cúk**

  * /chuck/

* **SEPIC**
  
  * se nunca fechar a chave, não tem tensao na saída
  
  * portanto, permite proteção, o que o buck não permitia
  
  * os dois indutores podem ser enrolados no mesmo nucleo, pois as correntes crescem e decrescem juntas
  
  * Dimensionamento dos componentes:
  
    ![img](Images - Conversores/image-20201204001056005.png)
  
    ![img](Images - Conversores/image-20201204000007339.png) 
  
    ![img](Images - Conversores/image-20201204001842315.png)
  
    ![img](Images - Conversores/image-20201204001852518.png)
    
  * Tensões e correntes:
  
    ![image-20201204090918120](Images - Conversores/image-20201204090918120.png)
  
* **Switched capacitor**

  * elevadores ou abaixadores
  * (-) dificeis de controlar
  * (-) correntes geralmente sao altas
  
* **other annotations**

  * ==como fazer um conversor cc-cc bidirecional?==

# CA-CC

* rectifier
* Ripple → time dependent/ocscilant component.
* [Video aula sobre manutensão em fontes chaveadas](https://www.youtube.com/watch?v=2yy3xG3v_fk)
* **Fontes Iineres**
  * rendimento cerca de 60%, enquanto chaveadas costumas ser acima de 85%
  * o trafo de 60Hz é caro, grande e pesado
  * O processo de análise costuma ser exatamente igual à chaveada, só muda se tem um trafo na entrada (linear) ou se bota um CC-CC na saída (chaveada)
* **Projeto com tensão de entrada variável**
  * deve-se considerar as piores situações
  * para determinar o capacitor e as
    correntes dos elementos, deve-se considerar a
    menor tensão,
  * para a escolha da tensão nominal do
    capacitor e da tensão reversa dos diodos, deve-se
    considerar a maior tensão e no seu valor de pico
  * Uma outra opção é botar um trafo com tap central na entrada, de forma que podemos reoganizar as conexões para diferentes tensões de entrada; na alta tensão (ex: 220V) botamos as bobinas do primário em série, na baixa tensão (ex: 110V) botamos elas em paralelo

## Monofásico

### Sem filtro capacitivo

-   **meia onda**

    -   valor médio na saída do retificador: 0.45*Vrms
    -   Diodos conduzem apenas no pico da onda (péssimos harmonicos)
    -   Simple (1 diode)
    -   Just one diode in the current path

    ![](Images - Conversores/image1-1603758504883.png)

    $$
    \theta = \text{arcsen}\left( \frac{V_{D}}{V_{S}} \right)
    $$

    ![](Images - Conversores/image2-1603758504883.png)

    -   <u>carga RL</u>
        
        -   atrasa a corrente
        -   conduz por mais tempo
        -   (-) no final do periodo de condução, a carga vai ver uma tensão negativa
        
    -   <u>Carga RL com diodo de roda livre</u>

        -   dá pra botar um diodo em parelelo com a carga, para fins de roda livre; (+) carga não recebe mais tensão negativa; se for indutivo o suficiente, pode até se manter em condução contínua

        -   both diodes cannot be
            forward-biased at the same time

        -   Caso estiver em condução contínua, a tensão de saída pode ser calculada por séries e fourier:

            ![image-20210214224343231](Images - Conversores/image-20210214224343231.png)

        

- **Full-wave rectifier**

  * More complex (but not too much)

  * Current through the load always flows in the same direction

  * (+) less ripple than the half-wave rectifier.

  * The average load current is determined from the dc term in the Fourier series: $V_{med}=\frac{2V_p}{\pi}$

  *  Amplitudes of the ac voltages/current can be comuted by the first term of Fourier

     ![](Images - Conversores/image2.png)

  * <u>Configuration with central tap</u>

    * (+) dois diodos a menos (e portanto menos perdas)

    * (-) trafo mais complexo

    * (-) Trafo é mal aproveitado, cada enrolamento trabalha só meio ciclo

    * (-) tensão reversa o dobro da saida;

      

  $$
  \text{PIV} = 2V_{S} - V_{D}
  $$

  $$
  \theta = 2\text{arcsen}\left( \frac{V_{D}}{V_{S}} \right)
  $$

  ![](Images - Conversores/image3-1603758504884.png)

  ![](Images - Conversores/image4-1603758504884.png)

  * <u>Bridge configuration</u>

    * (-) Two diodes in the current path
    * (+) tensão reversa igual a tensão de saída
    * The source and the load don't have the same reference (bad, very bad, because we cant measure a load with a instrument with reference in the AC gnd, so we usually use a transformer between the AC and the bridge. Isolar também é útil caso a instalação estiver mal feita e o neutro e terra estiverem invertidos: nesse caso usar um trafo isolador pode salvar vidas) \#\#esse comentário sobre isolação deveria estar em outro lugar, mas por enquanto fica aqui

  $$
  \text{PIV} = V_{S} - V_{D}
  $$

  $$
  \theta = 2\text{arcsen}\left( \frac{V_{D}}{V_{S}} \right)
  $$

  ![](Images - Conversores/image5-1603758504884.png)

  * <u>voltage doubler in the output</u>

    * An easy way to have a dual voltage converter is to put a voltage doubler in the output
    * or we can put circuit to detect the voltage, and do the switch with a triac

    ![image-20210223225835767](Images - Conversores/image-20210223225835767.png)

  * <u>Carga RL-fonte</u>

    * a fonte causa um efeito similar ao controle de um tiristor: só conduz após certo angulo

      ![image-20210314005651060](Images - Conversores/image-20210314005651060.png)

### Com filtro capacitivo

* **Meia onda**

  * corrente é zuada
  * os primeiros picos (no transitório) vão ser ainda maiores
  * Important parameters: max current, Peak Inverse Voltage (PIV) though diodes and conducting angle $\theta$
  * The time constant *CR* must be much greater than the discharge interval.
  * Vr → voltage ripple

  ![image-20210304210528348](Images - Conversores/image-20210304210528348.png)

  $$
  \theta = -\tan^{-1}(\omega RC) + \pi
  $$

  * <u>Approximating the capacitor discharge</u>

    * Capacitor with linear discharge (we consider the time-constant \>\> than the period)
    * Discharge time = period T
    * tempo de condução (carga do capacitor) = t_c

    $$
    V_{\text{Oavg}} = V_{P} - \frac{1}{2}V_{r}
    $$

    $$
    I_{L} = \frac{V_{P}}{R}
    $$

    $$
     \text{PIV} = 2V_{S} - V_{D}
    $$

  ![image-20210304231909660](Images - Conversores/image-20210304231909660.png)

  ![](Images - Conversores/image6-1603758504884.png)

  ![](Images - Conversores/image7-1603758504885.png)

  ![](Images - Conversores/image8-1603758504885.png)

  ![](Images - Conversores/image9-1603758504885.png)
  
  > Minimum capacitor for a given ripple:

  ![image-20210304230624562](Images - Conversores/image-20210304230624562.png)

  > *Remembering*: $\omega = 2\text{πf}$

  * <u> Diode current </u>
    * All the charge that is provided to the circuit by the capacitor is provided to the capacitor (through the diode) in a short period of time $\mathrm{\Delta}t$
    * So, that current in the diode in that interval will be high
    * Correntes nos diodos dada por:

    ![](Images - Conversores/image10-1603758504885.png)

    ![](Images - Conversores/image11-1603758504885.png)
    $$
    i_{Dmax}=2C\Delta V_o/t_c
    $$

    $$
    i_D = i_C + i_o
    $$

    ![image-20210304231713230](Images - Conversores/image-20210304231713230.png)


-   **Full-wave peak rectifier**


    -   Capacitor será dado por:
    
        ![image-20210304233208175](Images - Conversores/image-20210304233208175.png)
    
    -   Esforços no diodo e ==quem é Vr?==:

![](Images - Conversores/image12-1603758504886.png){width="2.4270833333333335in"
height="1.7291666666666667in"}

* Esforços no diodo em função de $t_c$:

![image-20210304233559886](Images - Conversores/image-20210304233559886.png)

![image-20210304233703431](Images - Conversores/image-20210304233703431.png)

![image-20210304233302367](Images - Conversores/image-20210304233302367.png)

* Esforços no capacitor:

  ![image-20210304233847490](Images - Conversores/image-20210304233847490.png)

### Controlados

* Usado para manter a tensão média de saída constante mesmo quando a entrada variar, ou variar a saída independentemente da entrada

* não existe retificador controlado com filtro capacitivo, pois o  capacitor só se carrega com o pico (então o controle é irrelevante)
  Se tiver filtro capacitivo, quem deve fazer o controle é o próximo estágio
  
* use alpha in radians

* **meia onda Controlado por SCR**

  -   SCR é acionado depois de certo angulo

  -   É possível variar a tensão média

      ![image-20210214225145352](Images - Conversores/image-20210214225145352.png)

  -   Tensão média na carga dado por:

      ![image-20210214225359202](Images - Conversores/image-20210214225359202.png)
  
* **Onda completa**

  * something_0 = average

  * é usual fazer essa ponte mista com dois diodos e dois tiristores, visto que dois já é o suficiente para fazer o controle;

  * usar 4 tiristores faz sentido caso quisermos que a tensão de saída fique negativa, permitindo recuperação de energia;

  * Output voltage:

    ![image-20210313232937103](Images - Conversores/image-20210313232937103.png)

  * Output current:

    ![image-20210313232909642](Images - Conversores/image-20210313232909642.png)

    ![image-20210313233604726](Images - Conversores/image-20210313233604726.png)
  
  * V_m = tensão de pico da entrada
  
    

### Others

-   **Precision rectifier**
    -   Or *superdiode*

    -   To very small signals

    -   Use diodes and opamps

    -   precise and predictable transfer characteristics.

    -   Extra advantage: the op amp supplies the current, and the source
        is buffered

    -   If Vin is positive, the circuit is a voltage follower

    -   If Vin if negative, the diode blocks the current and the output
        goes to zero (Va goes to negative saturation)

![](Images - Conversores/image13-1603758504886.png){width="4.520833333333333in"
height="1.7652777777777777in"}

-   **Effect of nonidealities**
    -   Threshold voltage → diode drop divided by the op amp's open-loop
        gain.

## Trifásico

* um retificador em cada fase, com as saídas conectadas juntas em uma unica saída

* os diodos só vão conduzir um de cada vez, quando estes receberam a mairor tensão

* commonly used in industry to produce a dc voltage and
  current for large loads

* Geralmene não ligamos o neutro: fica mais estável, mais equilibrado, entre outros
  
* **meia onda**

  * 3 diodos
  * um diodo em cada fase
  * cada diodo conduz por 180º
  * a saída já é quase uma CC, com 50% de ripple (de 311 a +- 150, em 220V?)
  * precisa ter neutro
  * tensão na saída vai ser a de fase ($V_{vf}\sqrt 2$ )

* **Onda completa**

  * 6 diodos

  * Os diodos ficam meio que me antiparalelo

  * cada diodo conduz por 120º

  * (+) melhora a corrente (fica mais equilibrada)

  * (+) melhora a tensão (ripple pequeno, +- 14%?, de 537 à ?)

  * (+) bom fator de potencia (~0.95) e distorção harmônica

  * chamamos os diodos de “grupo positivo” e “grupo negativo”

  * tensão na saída vai ser a de linha ($V_{vf}\sqrt 3$ )

    ![image-20210309201222002](Images - Conversores/image-20210309201222002.png)

    ![image-20210309202809420](Images - Conversores/image-20210309202809420.png)

  * The maximum reverse voltage across a diode is the peak line-to-line voltage.

### Controlados

* **Onda completa**

  * Ou *conversor de 6 pulsos*
  * cuidado ao calcular o angulo de atraso: se for medido em relação à fase A, então o atraso deve ser somado em 30º (por que a saída é em relação às tensões de linha, que são 30º atrasadas)

  ![image-20210312151737229](Images - Conversores/image-20210312151737229.png)

  ![image-20210312151754122](Images - Conversores/image-20210312151754122.png)

# CA-CA

* Geralmente nos referimos apenas a controle da tensão média
* Podemos também controlar a frequência (ciclo-conversores, mas não pouco usados, só pra potências muito altas); geralmente convertemos primeiro para CC e depois de volta para CA

## Monofásico

* **Gradador CA**

  * (-) Alta distorção harmônica

  * Podemos usar dois tiristores em anti paralelo, ou um triac

    ![image-20210325211505679](Images - Conversores/image-20210325211505679.png)

    ![image-20210325211621385](Images - Conversores/image-20210325211621385.png)

    ![image-20210325211712663](Images - Conversores/image-20210325211712663.png)

  * <u>Carga RL</u>

    * mantem conduzindo por mais tempo

      ![image-20210325214535425](Images - Conversores/image-20210325214535425.png)

  * <u>Dimmer</u>

    * O gradador CA geralmente é chamado de *dimmer*, apesar do nome *dimmer* também ser utilizado para outros dispositivos similares

    * O comando de gatilho pode ser gerado automaticamente usando um DIAC:

      ![Dimmer com TRIAC (ART294)](Images - Conversores/art0294_02.png)

* **Controle por ciclos inteiros**

  * corta vários ciclos inteiros, deixa passar vários
  * funciona quando a carga tem uma inércia grande
  * melhora THD e fator de potencia
  * se a carga for grande, pode causar uma perturbação do resto da rede

## Trifásicos

* corrente vai por um conjunto, volta por outro

# CC-CA

* Inversores

## Monofásicos

### Saída quadrada

* Basicamente uma ponte H

* Não conseguimos ajustar a tensão RMS: será sempre igual a tensão CC da entrada

* a maioria das cargas pode ser ligada direto nesse conversor (considerando que elas deveriam receber senoidal), talvez prejudicando sua vida útil e/ou desempenho 

* é comum colocar um capacitor em série para garantir que não haverá uma componente CC na saída

* **Carga RL**

  * Corretne fica uma “ondinha”

  * não esqueça dos diodos de roda livre!!

  * Meia onda:

    ![image-20210406200946801](Images - Conversores/image-20210406200946801.png)

  * Onda completa:

    ![image-20210406201033728](Images - Conversores/image-20210406201033728.png)

* **Variando os angulos**

  * angulos de acionamento
  
  * se dermos um tempo morto no cruzamento por um angulo $a$ (mantendo a simetria), as harmônicas serão dadas por:
  
    ![image-20210408210311510](Images - Conversores/image-20210408210311510.png)
  
    ![image-20210408210231182](Images - Conversores/image-20210408210231182.png)
  
  * conseguindo eliminar algumas harmônicas; para eliminar a harmônica n, fazermos $a=90º/n$
  
  * Conseguimos variar a RMS da saída
  
  * Conseguimos deixar com valor médio, mas é raro
  
  * ou ajusta o valor eficaz ou ajusta as harmônicas, não dá pra ajustar os dois juntos (só temos um grau de liberdade, alfa)
  
  * acionamentos mais rebuscados permitem controles mais rebuscados nas harmônicas
  
* **multinível**

  * duas fontes CC (ou vários, se quisermos)
  * bota dois inversores juntos
  * (+) a potencia total fica distribuida entre os dois conjuntos
  * (-) usa o dobro de componentes

### Saída PWM

* 

* as harmônicas são de bem alta ordem e são fáceis de serem filtradas

* valor médio similar à uma senoide

* frequência do PWM é bem maior do que a frequẽncia da senoide desejada, geralmente umas duas décadas acima

* para uma carga indutiva, como um motor, funciona que é uma beleza

* senão botaoms um filtro (indutor em série e capacitor em paralelo)

* quanto melhor o filtro, mais próximo de uma senoide será a tensão de saída

* aparece só as harmônicas multiplas da frequência de chaveamento

* **saída à dois níves**: fica chaveando entre +vcc e -vcc

* **saída à tres níveis**

  * +vcc, zero e -vcc; (+) o drobro da frequencia de chaveamento (portanto mais fácil de filtrar); pode ser fácilmente implementado comandando cada braço da ponte com a comparação com uma senoide diferente:

  * ?

    ![image-20210413192418569](Images - Conversores/image-20210413192418569.png)

* **controle de tensão**

  * indice de modulação de amplitude: relação entre triangular e senoide de controle; tensoes pico-a-pico; quanto mais se abaixa a tensão, menor a eficiencia do inversor

    indice de modulação de frequencia: geralmente maior do que 100; aumenta a tensão da senoide, diminui a rms de saida

    ![img](Images - Conversores/Untitled.png)

    > Vl = tensão de pico da senoide?

  * As harmônicas também podem ser ==ajustadas com (!!!???)==:
  
    ![image-20210413201700250](Images - Conversores/image-20210413201700250.png)

## Trifásicos

* Iram 600V: CI que já implementa todas as chaves; bastante usado para acionamento de motores

* **Saída quadrada**

  * Topologia com dois capacitores se quisermos o ponto de neutro, senão podemos ligar apenas um capacitor:
  
  ![image-20210406201407944](Images - Conversores/image-20210406201407944.png)
  
  * <u>carga em estrela</u>
  
    * dá pra comutar as fases pra conseguir 2/3 da tensão em um certo resistor
    * as harmonicas ficam melhores
  
    ![image-20210415210242894](Images - Conversores/image-20210415210242894.png)

# Aplicações

* **Turbinas eolicas**
  * pás -> retificardor -> inversor -> rede elétrica
  * a tensão e a frequencia da pá varia (CA)
  
* **Paineis fotovoltaicos**
  * painel -> inversor -> refe elétrica
  * a tensão do painel varia (CC)
  
* **controle de torque**
  * dá pra estimar o torque a partir da tensão e corrente
  * é comum injetar uma tensão para fazer essa medição “sensorless”
  
* **carregador para carros elétricos**
  * NBR IEC 6185-1:2013, entre outras
  * <u>classifiacção pela velcidade</u>
    * dado pelo tempo até 100% da carga
    * lenta: 4 a 8h
    * semirápida: 2h
    * rapida: 40 minutos
    * ultrarapida: 15 minutos
  
* **driver para lampadas leds**
  * led funciona em CC
  * precisa de uma corrente alta
  * driver de corrente constance: ?
  * driver de tensão constante: ?
  * Podem ser dimerizáveis ou não
  
* **no brake**
  * or *UPS*
  
  * CA-CC, bateria, CC-CA
  
  * os baratos geralmente usam um inversor por onda quadrada, ao invéz de senoidal
  
  * online: constantemente corrige (THD, FP, etc), mesmo quando há energia chegando
  
  * offline: só entra quando precis 
  
    ![image-20210429212948016](Images - Conversores/image-20210429212948016.png)
  
* **transmissão de energia sem fio**

  * exemplos de baixissima potencia: applicações de rfid passivo, implantes biomédicos, etc
  
  * <u>técnologias indutivas</u>
    * fortemente acoplada: ferrite ou algo assim
    * fracamente acoplada: só ar
    * bobinas precisam estar em ressonancia? capacitor em serie?
    * a potencia que chega no receptor deve ser controlado?
    * transmitido em alta frequencia
    * eficiencia costuma ser em torno de 75%
    * se as bobinas não estiverem alinhadas, a eficiencia cai muito
    
  * <u>outras tecnologias (não indutivas)</u>
    * optico: laser de potencia -> placa fotovoltaica?
    
    * uktrassonica: usually with acoustic peizoelectric
    
    * capacitivia: ?
    
      ![image-20210430213039021](Images - Conversores/image-20210430213039021.png)
    
  * <u>padrão QI</u>
    * class 0: 5~30W
    
  * <u>orgãos</u>
    * Wireless power consorcium
    
  * <u>empresas</u>
    *  Energous 
    * Powercast
    
  * <u>outras anotações</u>
  
    * bobinas espirais possuem um fator de acoplamento melhor
