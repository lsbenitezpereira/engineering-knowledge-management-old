## Exercício 5.8

![image-20210404193035751](Images - Relatório/image-20210404193035751.png)

Para realizar os cálculos utilizou-se a linguagem Python e a biblioteca SymPy, implementando o seguinte código:

```python
for P in [25, 75]:
  vin = 120

  R = 120**2/100
  vout = sp.sqrt(P*R)
  print('Para %d W na saída é preciso ter %.2f Vrms'%(P, vout))
  print('Corrente RMS na carga = %.2f\n'%(vout/R))

  print('Para encontrar o angulo de defasagem (a), encontrar a raiz de:')
  a = sp.symbols('a')
  Vin = sp.symbols('V_in')
  Vout = sp.symbols('V_{out}')
  expr = Vin*sp.sqrt(1-a/sp.pi+sp.sin(2*a)/(2*sp.pi)) - Vout
  display(expr)
  expr = expr.subs(Vin, vin).subs(Vout, vout)
  angle = sp.nsolve(expr, 1)
  print('a = %.2f graus\n'%(angle/sp.pi*180))

  if angle<=sp.pi/2:
    vmax = vin
  else:
    vmax = vin*sp.sin(angle)
  print('Tensão máxima na carga = %.2f V'%vmax)
  print('Tensão reversa máxima no diodo = %.2f V'%vmax)
  print('\n--------\n')
```

Uma versão interativa do código acima está disponível em https://colab.research.google.com/drive/1d07pOZWzICfuzHDzaQR9_C7q4XY0U-vD?usp=sharing.

Utilizou-se o software PSIM para simular o circuito:

![image-20210404233724699](Images - Relatório/image-20210404233724699.png)

### A)

![image-20210404233126671](Images - Relatório/image-20210404233126671.png)

![image-20210404233631292](Images - Relatório/image-20210404233631292.png)

### B)

![image-20210404233136356](Images - Relatório/image-20210404233136356.png)

![image-20210404233409846](Images - Relatório/image-20210404233409846.png)

