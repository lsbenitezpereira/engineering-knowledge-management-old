## 8.11

![image-20210413114221167](Images - Relatório/image-20210413114221167.png)

![image-20210413114419811](Images - Relatório/image-20210413114419811.png)

Podemos eliminar a harmônica $n$ por meio da expressão:

![image-20210408210311510](../../Images - Conversores/image-20210408210311510.png)

Para eliminar a 7º harmônica, temos que fazer $\alpha = \frac{\arccos(0)}{7} = 12.85^\circ$

Nessa condição, a tensão RMS de saída é dada por:

![image-20210413114633686](Images - Relatório/image-20210413114633686.png)

Simulação:

![image-20210413154958185](Images - Relatório/image-20210413154958185.png)

![image-20210413155248110](Images - Relatório/image-20210413155248110.png)

![image-20210413155550909](Images - Relatório/image-20210413155550909.png)

## 	8.19

![image-20210413114326818](Images - Relatório/image-20210413114326818.png)



Para $m_a=\frac{250}{160\sqrt 2}=0.9$ e utilizando $V_{sin}=5$, temos que $V_{tri}=4.5$.
Para a $m_f$ dada, temos que $f_{tri} = 1860$.

Obtivemos a THD via simulação ($4.4\%$), bem como as correntes em cada uma das chaves:

![image-20210415182424732](Images - Relatório/image-20210415182424732.png)





![image-20210415182739728](Images - Relatório/image-20210415182739728.png)



