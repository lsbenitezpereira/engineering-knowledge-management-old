Conceptualziation
=================

-   or *regex*
-   string defined Search pattern
-   Varrem uma string procurando um padrão de texto
-   regex processor
    -   translates a regular expression in the above syntax into an internal representation that can be executed and matched against a string representing the text being searched in
    -   Thompson's construction algorithm: computes an equivalent nondeterministic finite automaton

## How it works

* Each character is either a metacharacter, having a special meaning, or a regular character that has a literal meaning
* []
  * [a-z] (match all lower case letters from 'a' to 'z') 
* |
  * or
  * `gray|grey` can match "gray" or "grey".
* ()
  * grouping
  * define precedences
* \\
  * scape
  * the next metacharacter or character will be considered a character
* ^
  * starts with
  * 
* \$
  * Ends with
* quatifier
  * how often that a preceding element is allowed to occur
  * `?`:  zero or one
  * `*`: zero or more
  * `+`: one or more
  * `{n}`
  * `{min,}`
  * `{min,max}`
* wildcards
  * .
    * matches every character except a newline
    *  `a.*b` matches any string that contains an "a", and then the character "b" at some later point.

## Tools

* [Site para testar expressões regulares](http://regexpal.com/)

## python

* [python tutorial and console](https://www.w3schools.com/python/python_regex.asp#findall)

* Snippet:

  ```python
  pattern = f'\&{parameter}=(.*?)\&'
  result = re.findall(pattern, url)
  print(int(result[0]))
  ```

  

