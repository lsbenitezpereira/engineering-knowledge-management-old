#include "queueP.h"

struct Queue{
    lista_enc_t* list;
};
struct Member{
    void* data;
    int priority;
};

/**********************************************************************/
queue_t* newQueue(void){
    //member_t* member =
    queue_t* queue = malloc (sizeof(queue_t));
    queue->list = criar_lista_enc();
    #ifdef DEBUG_QP
    printf("Queue created\n");
    #endif // DEBUG_QP
    return queue;
}

/**********************************************************************/
void enqueue(queue_t* queue, void* data, int priority){
    member_t* member = malloc (sizeof(member_t));
    member->data = data;
    member->priority = priority;
    no_t* element = criar_no(member);
    no_t* tempElement = obter_cabeca(queue->list);

    if (obter_tamanho(queue->list)==0 || ((member_t*)obter_dado(tempElement))->priority>priority){
        //Adiciona na cabe�a: lista vazia OU maior prioridade que a cabe�a
        add_cabeca(queue->list, element);
    }else{
        no_t* tempElement2 = obter_proximo(tempElement);
        while (tempElement2!=NULL && ((member_t*)obter_dado(tempElement2))->priority<=priority){
            tempElement = tempElement2;
            tempElement2 = obter_proximo(tempElement);
        }
        add_after(queue->list, tempElement, element);
    }
    #ifdef DEBUG_QP
    printf("Enqueue element, now size is: %d\n", queueSize(queue));
    #endif // DEBUG_QP
}

/**********************************************************************/
void* dequeue(queue_t* queue){
    member_t* member = remove_cabeca(queue->list);
    #ifdef DEBUG_QP
    //printf("member dequeued %p: piority %d\n", member->data, member->priority);
    #endif // DEBUG_QP
    return member->data;
}

/**********************************************************************/
void* queueHead(queue_t* queue){
    no_t* element = obter_cabeca(queue->list);
    #ifdef DEBUG_QP
    printf("Queue head accessed (not dequeue)\n");
    #endif // DEBUG_QP
    return obter_dado(element);
}
/**********************************************************************/
int queueSize(queue_t* queue){
    return obter_tamanho(queue->list);
}

/**********************************************************************/
int isQueueEmpty(queue_t* queue){
    return (obter_tamanho(queue->list))?0:1;
}

/**********************************************************************/
void freeQueue(queue_t* queue){
    if (queueSize(queue) == 0){
        libera_lista(queue->list);
        free (queue);

    } else {
        printf("Erro ao liberar queue: ainda existem dados");
        exit (-1);
    }
}
