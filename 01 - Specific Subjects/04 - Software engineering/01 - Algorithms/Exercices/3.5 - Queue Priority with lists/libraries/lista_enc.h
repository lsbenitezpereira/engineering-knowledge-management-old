#ifndef LISTA_ENC_H_INCLUDED
#define LISTA_ENC_H_INCLUDED

/**********************************************************************
 Title			Singly-linked list
 Language 		C
 Author			Renan Stark and Leonardo Benitez
 Date  			03/09/2018
 Description
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "no.h"

//#define DEBUG_LL

typedef struct listas_enc lista_enc_t;

/**
  * @brief  Cria uma nova lista encadeada vazia.
  * @param	Nenhum
  *
  * @retval lista_enc_t *: ponteiro (referência) da nova lista encadeada.
  */
lista_enc_t *criar_lista_enc(void);


/**
  * @brief  Adiciona um elemento à cabeça da lista
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval void
  */
void add_cabeca(lista_enc_t* lista, no_t* elemento);


/**
  * @brief  Adiciona um nó de lista no final.
  * @param	lista: lista encadeada que se deseja adicionar.
  *         elemento: nó que será adicionado na cauda.
  *
  * @retval Nenhum
  */
void add_cauda(lista_enc_t *lista, no_t* elemento);


/**
  * @brief
  * @param
  *
  * @retval void
  */
void add_after (lista_enc_t* list, no_t* elementN, no_t* element);


/**
  * @brief  Obtém a referência do início (cabeça) da lista encadeada.
  * @param	lista: lista que se deseja obter o início.
  *
  * @retval no_t *: nó inicial (cabeça) da lista.
  */
no_t *obter_cabeca(lista_enc_t *lista);


/**
  * @brief  Retorna o número de nós da lista
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval tamanho
  */
int obter_tamanho(lista_enc_t *lista);


/**
  * @brief  verifica se a lista está vazia
  * @param	lista: lista que se deseja verificar
  *
  * @retval Retorna 1 se a lista está fazia
  */
int is_list_empty(lista_enc_t* lista);


/**
  * @brief
  * @param
  *
  * @retval pointer to the data
  */
void* remove_cabeca(lista_enc_t* lista);


/**
  * @brief  Free dinamic allocated memory. The list MUST be empty.
  * @param
  *
  * @retval void
  */
void libera_lista(lista_enc_t* list);
#endif // LISTA_ENC_H_INCLUDED
