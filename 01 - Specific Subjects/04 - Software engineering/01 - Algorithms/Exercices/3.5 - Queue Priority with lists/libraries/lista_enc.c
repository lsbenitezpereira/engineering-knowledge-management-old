#include "lista_enc.h"

struct listas_enc {
    no_t *cabeca;   /*!< Referência da cabeça da lista encadeada: primeiro elemento. */
    no_t *cauda;    /*!< Referência da cauda da lista encadeada: último elemento. */
    int tamanho;    /*!< Tamanho atual da lista. */
};

/**********************************************************************/
lista_enc_t *criar_lista_enc (void) {
    lista_enc_t *p = malloc(sizeof(lista_enc_t));

    if (p == NULL){
        perror("cria_lista_enc:");
        exit(EXIT_FAILURE);
    }

    p->cabeca = NULL;
    p->cauda = NULL;
    p->tamanho = 0;

    return p;
}

/**********************************************************************/
void add_cabeca(lista_enc_t* lista, no_t* elemento){
    if (lista == NULL || elemento == NULL){
        fprintf(stderr,"add_cabeca: ponteiros invalidos");
        exit(EXIT_FAILURE);
    }

   #ifdef DEBUG_LL
   printf("Adicionando %p --- tamanho: %d\n", elemento, lista->tamanho);
   #endif // DEBUG_LL

   //lista vazia
   if (lista->tamanho == 0)
   {
        #ifdef DEBUG_LL
        printf("add_cabeca: add primeiro elemento: %p\n", elemento);
        #endif // DEBUG_LL

        lista->cauda = elemento;
        lista->cabeca = elemento;
        lista->tamanho++;

        desligar_no(elemento);
   }
   else {
        //liga a cebeça nele
        ligar_nos(elemento, lista->cabeca);

        lista->cabeca = elemento;
        lista->tamanho++;
   }
}

/**********************************************************************/
void add_cauda(lista_enc_t *lista, no_t* elemento)
{
    if (lista == NULL || elemento == NULL){
        fprintf(stderr,"add_cauda: ponteiros invalidos");
        exit(EXIT_FAILURE);
    }

   #ifdef DEBUG_LL
   printf("Adicionando %p --- tamanho: %d\n", elemento, lista->tamanho);
   #endif // DEBUG_LL

   //lista vazia
   if (lista->tamanho == 0)
   {
        #ifdef DEBUG_LL
        printf("add_cauda: add primeiro elemento: %p\n", elemento);
        #endif // DEBUG_LL

        lista->cauda = elemento;
        lista->cabeca = elemento;
        lista->tamanho++;

        desligar_no(elemento);
   }
   else {
        // Remove qualquer ligacao antiga
        desligar_no(elemento);
        // Liga cauda da lista com novo elemento
        ligar_nos(lista->cauda, elemento);

        lista->cauda = elemento;
        lista->tamanho++;
   }
}

/**********************************************************************/
void add_after (lista_enc_t* list, no_t* elementN, no_t* element){
    if (list == NULL || element == NULL){
        fprintf(stderr,"add_after: ponteiros invalidos");
        exit(EXIT_FAILURE);
    } else if (elementN == NULL){ //list empty: add to head
        list->cauda = element;
        list->cabeca = element;
        list->tamanho++;
        desligar_no(element);
    } else if (obter_proximo(elementN) == NULL){     //elementN is the last element: add to tail
        ligar_nos(list->cauda, element);  // Liga cauda da lista com novo elemento
        list->cauda = element;
        list->tamanho++;
        desligar_no(element);              // Remove qualquer ligacao antiga
    } else {    //normal case
        ligar_nos(element, obter_proximo(elementN));
        ligar_nos(elementN, element);
        list->tamanho++;
    }
   #ifdef DEBUG_LL
   printf("Adicionando %p --- tamanho: %d\n", elemento, lista->tamanho);
   #endif // DEBUG_LL
}

/**********************************************************************/
no_t *obter_cabeca(lista_enc_t *lista){
    return lista->cabeca;
}

/**********************************************************************/
int obter_tamanho(lista_enc_t *lista){
    return lista->tamanho;
  /*  no_t* temp = obter_cabeca (lista);
    int n;
    while (temp){
        temp = obter_proximo(temp);
        n++;
    }
    return n;*/
}

/**********************************************************************/
int is_list_empty(lista_enc_t* lista){
    return (lista->cabeca)?(0):(1);
}

/**********************************************************************/
void* remove_cabeca(lista_enc_t* lista){
    no_t* temp = obter_proximo(lista->cabeca);
    void* dado_cabeca = obter_dado(lista->cabeca);
    libera_no(lista->cabeca);
    lista->cabeca = temp;
    lista->tamanho--;
    #ifdef DEBUG_LL
    printf("removendo cabeca, tamanho atual: %d\n", obter_tamanho(lista));
    #endif // DEBUG_LL
    return dado_cabeca;
}

/**********************************************************************/
void libera_lista(lista_enc_t* list){
    //printf("tamanho da lista: %d\n", obter_tamanho(list));
    if (list->tamanho == 0){
        free (list);
    } else {
        printf("Erro ao liberar lista: ainda existem dados");
        exit (-1);
    }
}
