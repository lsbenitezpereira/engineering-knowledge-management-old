#include <stdio.h>
#include <stdlib.h>
#include "libraries/lista_enc.h"

struct Portion {
    int number;
    int carry;
};
typedef struct Portion portion_t;

int main(void){
    no_t* node;
    portion_t* portion;
    lista_enc_t* list1,* list2;
    //int i;
    long long int n1 = 123456789123456789;
    long long int n2 = 654321987654321;

    list1 = criar_lista_enc();
    list2 = criar_lista_enc();
    while (n1){
        printf("%d\n", n1%100000);
        portion = malloc (sizeof(portion_t));
        portion->number = n1%100000;
        portion->carry = 0;
        node = criar_no(portion);
        add_cauda(list1, node);
        n1/=100000;
    }
    printf("---\n");
    while (n2){
        printf("%d\n", n2%100000);
        portion = malloc (sizeof(portion_t));
        portion->number = n2%100000;
        portion->carry = 0;
        node = criar_no(portion);
        add_cauda(list2, node);
        n2/=100000;
    }
    //while (obter_tamanho())
    return 0;
}
