/**********************************************************************
 Title			App.c
 Language 		C
 Author			Leonardo Benitez
 Date  			03/09/2018
 Description	Stock control appication, using Queue
**********************************************************************/

#include "app.h"

#define TAM_BUFFER 30
#define DEBUG

struct StockElement {
    char name[16];
    int amount;
};

struct Stock{
    queue_t* queue;
};

/**********************************************************************/
stock_t* newStock (void){
    stock_t* stock = malloc (sizeof(stock_t));
    stock->queue = newQueue();
    #ifdef DEBUG
    printf("Stock created\n");
    #endif // DEBUG
    return stock;
}

/**********************************************************************/
void readStock(stock_t* stock, char* file){
    FILE* fp;
    char buffer [TAM_BUFFER];
    stock_element_t* tempElement;

    ///Open file
    fp = fopen (file, "r");
    if (fp==NULL){
        perror ("Error reading file");
        exit (-1);
    }

    ///Read file
    while (fgets(buffer, TAM_BUFFER, fp)!=NULL){
        tempElement = malloc (sizeof(stock_element_t));
        if (sscanf(buffer, "%16[^,],%d", tempElement->name, &tempElement->amount) != 2){
            printf("Unstructured file, aborting...");
            exit (-1);
        }
        enqueue(stock->queue, tempElement);
    }

    if (fclose(fp)!=0){
        perror ("Error closing file");
        exit(-1);
    }

    #ifdef DEBUG
    printf("Data in file %s added to stock\n", file);
    #endif // DEBUG
}
/**********************************************************************/
void freeStockElement (stock_t* stock){
    stock_element_t* tempElement;
    tempElement = (stock_element_t*) dequeue(stock->queue);
    printf("Name: %s \t\t\t Amount: %d\n", tempElement->name, tempElement->amount);
    free (tempElement);
}
/**********************************************************************/
void freeStock(stock_t* stock){
    stock_element_t* tempElement;
    printf("\n------\n");
    printf("STOCK LIST\n");
    while(!isQueueEmpty(stock->queue)){
        tempElement = (stock_element_t*) dequeue(stock->queue);
        printf("Name: %s \t\t\t Amount: %d\n", tempElement->name, tempElement->amount);
        free (tempElement);
    }
    printf("\n------\n");
    //printf("queue size: %d", queueSize(stock->queue));
    freeQueue(stock->queue);
    printf("queue liberada\n");
    free (stock);
}
/**********************************************************************/
