/**********************************************************************
 Title			Queue.c
 Language 		C
 Author			Leonardo Benitez
 Date  			03/09/2018
 Description	Implementation of a queue using singly linked list
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "lista_enc.h"
#include "no.h"
#include "queue.h"

//#define DEBUG

struct Queue{
    lista_enc_t* list;
};

/**********************************************************************/
queue_t* newQueue(void){
    queue_t* queue = malloc (sizeof(queue_t));
    queue->list = criar_lista_enc();
    #ifdef DEBUG
    printf("Queue created\n");
    #endif // DEBUG
    return queue;
}

/**********************************************************************/
void enqueue(queue_t* queue, void* data){
    no_t* element = criar_no(data);
    add_cauda(queue->list, element);
    #ifdef DEBUG
    printf("Enqueue element, now size is: %d\n", queueSize(queue));
    #endif // DEBUG
}

/**********************************************************************/
void* dequeue(queue_t* queue){
    void* element = remove_cabeca(queue->list);
    #ifdef DEBUG
    printf("Dequeue element, now size is: %d\n", queueSize(queue));
    #endif // DEBUG
    return element;
}

/**********************************************************************/
void* queueHead(queue_t* queue){
    no_t* element = obter_cabeca(queue->list);
    #ifdef DEBUG
    printf("Queue head accessed (not dequeue)\n");
    #endif // DEBUG
    return obter_dado(element);
}
/**********************************************************************/
int queueSize(queue_t* queue){
    return obter_tamanho(queue->list);
}

/**********************************************************************/
int isQueueEmpty(queue_t* queue){
    return (obter_tamanho(queue->list))?0:1;
}

/**********************************************************************/
void freeQueue(queue_t* queue){
    if (queueSize(queue) == 0){
        libera_lista(queue->list);
        free (queue);

    } else {
        printf("Erro ao liberar queue: ainda existem dados");
        exit (-1);
    }
}
