#include <stdio.h>
#include <stdlib.h>

#include "lista_enc.h"
#include "no.h"

//#define DEBUG

struct listas_enc {
    no_t *cabeca;   /*!< Referência da cabeça da lista encadeada: primeiro elemento. */
    no_t *cauda;    /*!< Referência da cauda da lista encadeada: último elemento. */
    int tamanho;    /*!< Tamanho atual da lista. */
};

/**
  * @brief  Cria uma nova lista encadeada vazia.
  * @param	Nenhum
  *
  * @retval lista_enc_t *: ponteiro (referência) da nova lista encadeada.
  */
lista_enc_t *criar_lista_enc (void) {
    lista_enc_t *p = malloc(sizeof(lista_enc_t));

    if (p == NULL){
        perror("cria_lista_enc:");
        exit(EXIT_FAILURE);
    }

    p->cabeca = NULL;
    p->cauda = NULL;
    p->tamanho = 0;

    return p;
}


/**
  * @brief  Obtém a referência do início (cabeça) da lista encadeada.
  * @param	lista: lista que se deseja obter o início.
  *
  * @retval no_t *: nó inicial (cabeça) da lista.
  */
no_t *obter_cabeca(lista_enc_t *lista){
    return lista->cabeca;
}


/**
  * @brief  Obtém a referência do fim (cauda) da lista encadeada.
  * @param	lista: lista que se deseja obter o fim.
  *
  * @retval no_t *: nó final (cauda) da lista.
  */
no_t *obter_cauda(lista_enc_t *lista){
    return lista->cauda;
}
//TODO: biblioteca completa? do renam?


/**
  * @brief  Adiciona um nó de lista no final.
  * @param	lista: lista encadeada que se deseja adicionar.
  *         elemento: nó que será adicionado na cauda.
  *
  * @retval Nenhum
  */
void add_cauda(lista_enc_t *lista, no_t* elemento)
{
    if (lista == NULL || elemento == NULL){
        fprintf(stderr,"add_cauda: ponteiros invalidos");
        exit(EXIT_FAILURE);
    }

   #ifdef DEBUG
   printf("Adicionando %p --- tamanho: %d\n", elemento, lista->tamanho);
   #endif // DEBUG

   //lista vazia
   if (lista->tamanho == 0)
   {
        #ifdef DEBUG
        printf("add_cauda: add primeiro elemento: %p\n", elemento);
        #endif // DEBUG

        lista->cauda = elemento;
        lista->cabeca = elemento;
        lista->tamanho++;

        desligar_no(elemento);
   }
   else {
        // Remove qualquer ligacao antiga
        desligar_no(elemento);
        // Liga cauda da lista com novo elemento
        ligar_nos(lista->cauda, elemento);

        lista->cauda = elemento;
        lista->tamanho++;
   }
}


/**
  * @brief  Adiciona um elemento à cabeça da lista (procedimento não recomendado)
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval void
  */
void add_cabeca(lista_enc_t* lista, no_t* elemento){
    if (lista == NULL || elemento == NULL){
        fprintf(stderr,"add_cabeca: ponteiros invalidos");
        exit(EXIT_FAILURE);
    }

   #ifdef DEBUG
   printf("Adicionando %p --- tamanho: %d\n", elemento, lista->tamanho);
   #endif // DEBUG

   //lista vazia
   if (lista->tamanho == 0)
   {
        #ifdef DEBUG
        printf("add_cabeca: add primeiro elemento: %p\n", elemento);
        #endif // DEBUG

        lista->cauda = elemento;
        lista->cabeca = elemento;
        lista->tamanho++;

        desligar_no(elemento);
   }
   else {
        //liga a cebeça nele
        ligar_nos(elemento, lista->cabeca);

        lista->cabeca = elemento;
        lista->tamanho++;
   }
}


/**
  * @brief  Retorna o número de nós da lista
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval tamanho
  */
int obter_tamanho(lista_enc_t *lista){
    return lista->tamanho;
  /*  no_t* temp = obter_cabeca (lista);
    int n;
    while (temp){
        temp = obter_proximo(temp);
        n++;
    }
    return n;*/
}

/**
  * @brief
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval 1 if empty, 0 if non-empty
  */
int is_list_empty(lista_enc_t* lista){
    return (lista->cabeca)?(0):(1);
}

/**
  * @brief
  * @param	lista: lista que se deseja obter o tamanho
  *
  * @retval
  */
void* remove_cabeca(lista_enc_t* lista){
    no_t* temp = obter_proximo(lista->cabeca);
    void* dado_cabeca = obter_dado(lista->cabeca);
    libera_no(lista->cabeca);
    limpa_anterior(temp);
    lista->cabeca = temp;
    lista->tamanho--;
    #ifdef DEBUG
    printf("removendo cabeca, tamanho atual: %d\n", obter_tamanho(lista));
    #endif // DEBUG
    return dado_cabeca;
}


/**
  * @brief
  * @param
  *
  * @retval
  */
void* remove_cauda(lista_enc_t* lista){
    no_t* temp = obter_anterior(lista->cauda);
    void* dado_cauda = obter_dado(lista->cauda);
    libera_no(lista->cauda);
     limpa_proximo(temp);
     lista->cauda = temp;
    lista->tamanho--;
    #ifdef DEBUG
    printf("removendo cabeca, tamanho atual: %d\n", obter_tamanho(lista));
    #endif // DEBUG
    return dado_cauda;
}


/**
  * @brief
  * @param
  *
  * @retval void
  */
void libera_lista(lista_enc_t* list){
    printf("tamanho da lista: %d\n", obter_tamanho(list));
    if (list->tamanho == 0){
        free (list);
    } else {
        printf("Erro ao liberar lista: ainda existem dados");
        exit (-1);
    }
}
