#include "stackAD.h"

struct pilhas {
    int topo;       /*!< �ndice do vetor que representa o topo da fila */
    int max;
    float* data;      /*!< Vetor que manter� os dados. */
};

/**********************************************************************/
pilha_t* cria_pilha (int size){
    pilha_t *pilha;

    pilha = (pilha_t*)malloc(sizeof(pilha_t));
    pilha->data = malloc(sizeof(float)*size);

    pilha->topo = 0;
    pilha->max = size;

    #ifdef DEBUG
    printf("Pilha criada, tamanho: %d\n", pilha->max);
    #endif // DEBUG

    return pilha;
}


/**********************************************************************/
void push(pilha_t *pilha, float dado){
    int topo = pilha->topo;

    if (topo == (pilha->max - 1)) {
        pilha->max *= 2;
        pilha->data = realloc(pilha->data, sizeof(int)*pilha->max);
        #ifdef DEBUG
        printf("Dobrando o tamanho dinamico da pilha. Tamanho atual: %d\n", pilha->max);
        #endif // DEBUG
    }
    pilha->data[topo] = dado;
    pilha->topo++;
    #ifdef DEBUG
    printf("Elemento adicionado a pilha.\n");
    #endif // DEBUG
}

/**********************************************************************/
float pop(pilha_t *pilha){
    int topo = pilha->topo;

    if (topo < 0 || topo > pilha->max){
        fprintf(stderr, "Pilha corrompida!\n");
        exit(EXIT_FAILURE);
    } else if (topo == 0) {
        fprintf(stderr, "pop() em pilha vazia!\n");
        return 0;
    } else if (topo < (pilha->max)/2){
        pilha->max /= 2;
        pilha->data = realloc(pilha->data, sizeof(int)*pilha->max);
        #ifdef DEBUG
        printf("Diminuindo o tamanho dinamico da pilha. Tanho atual: %d\n", pilha->max);
        #endif // DEBUG
    }

    pilha->topo--;
    return pilha->data[topo - 1];
}

/**********************************************************************/
int stackSize (pilha_t* stack){
    return stack->topo;
}

/**********************************************************************/
//TODO: free queue function
