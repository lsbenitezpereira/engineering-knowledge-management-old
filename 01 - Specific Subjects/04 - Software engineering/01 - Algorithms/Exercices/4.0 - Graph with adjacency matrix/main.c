/*
 ============================================================================
 Name        : grafos-adj-matrix.c
 Author      : Renan Augusto Starke
 Version     :
 Copyright   : Renan, todo os direitos reservados
 Description : grafos com matriz de adjacencia, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "libraries/grafo.h"

#define TAM 10

int main(void) {
	int i,j;
	grafo_t *g;

	g = cria_grafo(TAM);

	cria_adjacencia(g, 1, 1);
	cria_adjacencia(g, 4, 1);
	cria_adjacencia(g, 3, 2);
	cria_adjacencia(g, 1, 2);
	cria_adjacencia(g, 1, 6);
	cria_adjacencia(g, 6, 8);

	/* Imprimi matriz
	for (i=0; i < TAM; i++){
		for (j=0; j < TAM; j++)
			printf("[%d] [%d] : %d\n", i,j, adjacente(g,i,j));
	}*/

	exportaDot (g);
	buscaLargura (g, 2);
	buscaProfundidade(g, 2);
	printNodes(g);



	libera_grafo(g);

	return EXIT_SUCCESS;
}
