#ifndef WINTER_GAMES_INCLUDED
#define WINTER_GAMES_INCLUDED

#include "lista_enc.h"
#include "no.h"

typedef struct Nations nations_t;

lista_enc_t* readFile(char* file);
void closeFile(lista_enc_t* list);  //TODO: list � palavra reservada????
void printNations (lista_enc_t* list);

#endif	//WINTER_GAMES_INCLUDED
