#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libraries/heap.h"
#define SIZE 20

int main(){
    int i;
    //heap_t* myHeap = heap_new(666, 15);
    srand(time(NULL));
    //int array[] = {1,11,3,4,5,6,7,8,9,10};
    int array[SIZE];
    for(i = 0; i < SIZE; i++) {
        array[i] = rand() % 50;
    }
/*    //heap_from_vector (myHeap, &array[0], 10);

    heap_export_dot("heap.txt", myHeap);
    heap_set_vertex(myHeap, 10, 4);
    for (i=heap_get_father(myHeap, 10); i>=0; i=heap_get_father(myHeap, i))
        heap_max_heapify(myHeap, i);
*/
    heapSort (&array[0], SIZE);
    for (i=0; i<SIZE; i++){
        printf("%d \n", array[i]);
    }
    return 0;
}
