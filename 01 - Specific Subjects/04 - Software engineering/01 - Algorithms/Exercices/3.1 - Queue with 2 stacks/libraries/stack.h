#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

/**********************************************************************
 Title			Stack implementation using singly-linked list
 Language 		C
 Author			Leonardo Benitez
 Date  			15/09/2018
 Description
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "lista_enc.h"

typedef struct Stack stack_t;

stack_t* newStack(void);
void stackPush(stack_t* stack, void* data);
void* stackPop(stack_t* stack);
void* stackReturnTop(stack_t* stack);
int stackSize(stack_t* stack);
int isStackEmpty(stack_t* stack);
void freeStack(stack_t* stack);
#endif // STACK_H_INCLUDED
