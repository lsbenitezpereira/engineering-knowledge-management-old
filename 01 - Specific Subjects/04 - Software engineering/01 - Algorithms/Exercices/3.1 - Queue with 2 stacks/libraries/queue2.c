#include "queue2.h"

struct Queue {
    stack_t* stack1;
    stack_t* stack2;
};

/**********************************************************************/
queue_t* newQueue (){
    queue_t* queue = malloc (sizeof(queue_t));
    queue->stack1 = newStack();
    queue->stack2 = newStack();
    #ifdef DEBUG
    printf("Queue created\n");
    #endif // DEBUG
    return queue;
}

/**********************************************************************/
void enqueue (queue_t* queue, void* data){
    stackPush(queue->stack1, data);
    #ifdef DEBUG
    printf("Data enqueued\n");
    #endif // DEBUG
}

/**********************************************************************/
void* dequeue (queue_t* queue){
    while (!isStackEmpty(queue->stack1)){
        stackPush (queue->stack2, stackPop(queue->stack1));
    }
    void* dequeuedData = stackPop(queue->stack2);
    while (!isStackEmpty(queue->stack2)){
        stackPush (queue->stack1, stackPop(queue->stack2));
    }
    #ifdef DEBUG
    printf("data dequeued\n");
    #endif // DEBUG
    return dequeuedData;
}

/**********************************************************************/
int queueSize (queue_t* queue){
   return stackSize(queue->stack1);
}

/**********************************************************************/
void exportQueue (queue_t* queue, char* fileName){
    FILE *fp = fopen (fileName, "wb");
    if (fp==NULL){
        perror ("Error in Main");
        exit (EXIT_FAILURE);
    }
    printf("export: %s\n", fwrite(queue, sizeof(queue_t), 1, fp)?"sucess":"fail");
    if (fclose(fp)!=0){
        perror ("Error in Main");
        exit (EXIT_FAILURE);
    }
}

/**********************************************************************/
void freeQueue (queue_t* queue){
    freeStack(queue->stack1);
    freeStack(queue->stack2);
    free (queue);
    //TODO: free all things. Verify if the queue is empty
}
