#include <stdio.h>
#include <stdlib.h>
#include "libraries/queue2.h"

int main(){
    int a=10;
    int b=11;
    int c=12;

    queue_t* queue = newQueue ();
    enqueue(queue, &a);
    printf("enqueued. size: %d\n", queueSize(queue));
    enqueue(queue, &b);
    printf("enqueued. size: %d\n", queueSize(queue));
    enqueue(queue, &c);
    printf("enqueued. size: %d\n", queueSize(queue));

    exportQueue(queue, "batata.bin");

    while(queueSize(queue)!=0){
        printf("data dequeued: %d\n", *(int*)dequeue(queue));
    }

    if(queueSize(queue)==0)
        freeQueue(queue);
    else
        printf("LIST ISN'T EMPTY, YOU DUMB ASS");
    return 0;
}
