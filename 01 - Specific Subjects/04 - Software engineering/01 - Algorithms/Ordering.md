# Conceituação

* Seja S uma sequência arbitrária
* Rearanjar os elementos de S para produzir uma nova sequência, S’, onde os elementos aparecem em alguma ordem  
* Essa ordenação é dada por um operador de ‘>’, chamado de ordem total. Ele deve ser capaz de comparar todos os elementos do conjunto. A relação deve atender a propriedade transitiva.  
* Sequência estável: quando possui valores iguais, a sequência é dita estável se mantêm a ordem anterior  
* https://9gag.com/gag/awAQoQW 
* **Dicas de implementação**
  * Se for preciso ordenar (por exemplo) uma lista simplesmente encadeada (ou qualquer outra coisa que não dê para trocar os elementos com facilidade), passe para um vetor, ordene e passe para lista novamente 
  * Para ordenar mais do que números (estruturas, por exemplo), utilize dados abstratos 
* **Dicas de Software**
  * YEd (para desenhar)
  * WhyWorks



# Buble sort

* Vai jogando os maiores pro final  

* A cada iteração, o numero de passos vai diminuindo  

* Cada elemento precisa de $n-1$ operações para ir para a posição correta  

* Após k passos pelos dados, os últimos k elementos já estão ordenados  

* **Complexidade**

  * O (n^2)
  * Se e somente se o swap for O(1), o que não é verdade para uma implementação com lista simplesmente encadeada 
  * justificativa: 

    ![1542714897216](Images - Ordering/1542714897216.png) 

* **Algoritmo**

  *  Laço externo		→ executado n-1 (todo o array)
  *  Laço interno		→ executado i-1 (da sua posição até o final)

  ![1542714914824](Images - Ordering/1542714914824.png)

 

# Quick sort

*  Fast as fuck

*  In C, is implemented in stdlib

*  Divide the problem in little problems (“divide and conquer”)  

*  **Algoritmo**

	  *  Selecionamos um element aleatorio, chamado de pivô  
	*  Remove-se p de S e particiona-se o elementos restantes de S in duas sequencias distintas:
	*  { L: todos os elementos de L s~ao iguais ou menores do que p
	*  { G: todos os elementos de G s~ao iguais ou maiores do que p
	*  { Em geral, L e G n~ao est~ao ordenados.  
	*  O pivô já estará na posição correta  

*  **Complexity**

	  *  Worst case	→ O(n^2) → equal to bubble  
	*  normal	→ O(nlogn)

*  **Escolha do pivo**

	  *  O melhor pivô seria a mediana da amostra
	*  Como as aostras geralmente vêm semi-ordenadas, pegar sempre o mesmo pivô é receita para o fracasso  

  ![1542715020499](Images - Ordering/1542715020499.png)

  ![1542715028265](Images - Ordering/1542715028265.png)

​    

# Insertion

* Dada sequência não ordenada, computa várias sequências ordenadas
* Vai inserindo uma outra 
* Pode ser implementada com lista encadeada
* Não é recursivo (excelente para sistemas embarcados)
* **Complexidade**
  * Se o vetor já estiver orderanado, terminamos em $O(n)$
* **Implementação direta** 
  * Faz a busca pelo local de inserção linearmente
  * A busca (para descobrir o local de inserção) possui complexidade $O(n)$
  * Complexidade total: $O(n^2)$
* **Implementação binária** 
  * A busca possui complexidade $O\left(log(n)\right)$
  * Complexidade total: $O(nlogn)$



# Heap sort
* Organiza o vetor em um heap
* Tira o elemento de cima (que será o maior)
* Refaz o heap
* tira de novo e repita o processo
* **Algoritmo**
	* Para “tirarmos” o elemento apenas precisamos movêlo para o final e diminuir o tamanho oficial do heap, de forma que conseguimos fazer tudo no mesmo vetor
	* Em um heap máximo, o maior valor (primeiro a sair) fica no final (sequência crescente)

![1542715122239](Images - Ordering/1542715122239.png)

# Merge sort
* Intercalação 
* Problema: gastamos o dobro da memória, pois a fusão requer um array temporário com o mesmo tamanho de vetor original 
* **Algoritmo**

  * Seja S uma sequênia com mais de um elemento
  * dividimos em duas sb sequências (foda-se o pivo)
  * Vamos dividindo, sem ordenação
  * Ao unir os elementos unimos vamos colocando na ordem (não é apenas uma junção, temos que ir intervalando na ordem)
* **Complexidade**

  * $O(nlogn)$
  * Consideramos a fusão O(n)
* **Dicas de implementação**

  * Para não ter que alocar o vetor dentro (pois o algoritmo é recursivo, então teriamos que criar uma camada de interface), já passe um array temporário como parâmetro:

       ```c
       MergeSort (int* array, int* arrayTemp, int meio, int dir)
       ```

# Selection

* Coocamos o elemento sempre no final (difernete do insertion, que já coloca no lugar certo)

* **Using straigh selection sort** 

  * O(n²)

  * faz uma busca linear para achar o maior elemento 

  * algoritmo: 

  	![1542975819130](Images - Ordering/1542975819130.png)

* **using heap**

  * Construimos um heap (comlexidade n)
  * Tiramos o maior (complexidade logn)
  * inseremos no final da nova sequência 
  * $O(nlog_2n)$

# non-comparative algorithms

* Order the values directly, without comparing them
* https://cs.nyu.edu/courses/fall17/CSCI-UA.0102-001/Notes/LinearSort.html



