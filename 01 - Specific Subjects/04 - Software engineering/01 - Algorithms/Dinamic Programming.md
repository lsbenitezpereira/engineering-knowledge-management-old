# Conceituação

* optimization method 
* solves problems by
  combining the solutions to subproblems
* problem exhibits overlapping subproblems (share subsubproblems)
* Although DP ideas can be applied to problems with continuous state and action spaces, exact solutions are possible only in special cases.
* There is an time-memory trade-off
* Planned to reusing previously computed values, so it needs a memory
* the name “dynamic” is because the solution is step-by-step, by sequencial actions 
* Optimal substructure
  * Optimal solutions to a problem incorporate optimal solutions to related subproblems, which we may solve independently
  * if you cannot find a optimal substructure to your problem, then you cannot solve your problem with DP
  * overlapping subproblems: subproblems may occur many times; how to computationally solve them only once? 
* Others
  * Bellman equation: between the value of the larger problem and the values of the sub-problems

## Relation to optimization

* how to optimize stuff with DP?

## Relation to markov processes

* DP can be used to solve markov decision processes (MDP) (see definition in *Statistics*)

* that is, to find optimal policies given a perfect model of the environment

* therefore, can be used to optimal planning

* **Formulation as a classical DP problem**

  *  each successive approximation is obtained by using the Bellman equation for $v_\pi$ as an update rule: 

  ![image-20210101153800692](Images - Dinamic Programming/image-20210101153800692.png)

  * This will be a greedy improvement of the policy
  * is guaranteed to find an optimal policy (really? with a greedy appraoch? waht are the assumptions about the MDP?)
  * Temporal complexity to find an optimal policy: polynomial in the number of states and actions
  * (-) If the state set is very large, then even a single iteration can be prohibitively expensive.
  * ==policy iteration vs value iteration?==
  * [interesting demo](https://cs.stanford.edu/people/karpathy/reinforcejs/gridworld_dp.html)

* **Variation - asynchronous DP**

  * improves effiency for large spaces
  * is more fleible about the sequence in which the states are evaluated

## Relatino to **algorithms and programming**

* Divide and conquer strategies, usually recursive
* sorting: merge sort and quick sort
* string: alignment algorithms, etc
* graph: shortest path, etc
* **Top-down aproach**
  * check if it was previsly solved
  * if yes, search in array/hash table
  * if no, compute and save
* **Bottom-up aproach**
  * solve the "smaller” problems first
  * When solving a particular subproblem, we have already solved all of the smaller subproblems its
    solution depends upon, and we have saved their solutions.
  * Often runs faster

## Relation to other fields

* graphical applications (ex: viterbi algorithm)
* bioinformatics applications (ex: lattice models)
* games applications: Hanoi puzzle, etc
* mathematical applications: matrix multiplication, fibonacci series calculation, etc

# Case study - Bunnies

## Introduction

Let $B$ be the total number of Bunnies and $N$ be the number of required bunnies. Given a set of TK keys (acronym from *TotalKeys*), each key distributed to BPK bunnies (acronym from *buniesPerKey*), we’ll have that:
$$
TK = {B\choose BPK} = \frac{B!}{BPK!*(B-BPK)!}
$$
where ${B\choose BPK}$ denotes the combination of B elements taken BPK at the time \cite{1}.

To find the value of BPK, we can use the Pigeon Hole Principle \cite{2}: for $k,n \in \N$,  if $n$ objects are distributed among $m$ sets, then at least one of the sets will contain at least $k+1$ objects, being $k=\frac{n-1}{m}$.

To our specific problem, $k=TK={B\choose BPK}$, $m=BPK$, and $n={B \choose N} N +1$, leading to:
$$
{B\choose BPK} = \frac{{B\choose N}N}{BPK}
$$

$$
(B-BPK)! (BPK-1)! = (B-N)! (N-1)!, \forall BPK \neq N
$$

$$
\therefore BPK = B-N+1
$$

Therefore, to solve this problem optimally we can assign each key to BPK bunnies, from a total of B bunnies, that is:
$$
combinations(range(B),BPK)
$$

## Related works

Alternatively, it is possible to choose ${B-1 \choose B-N}=\frac{B-1!}{(B-N)!*(N-1)!}$ keys to each bunny subject to the constraint that each key is assigned to $BPK$ bunnies, but that requires much more computation. 

Another option is to calculate the total number of keys using the python `itertools.combinations()` tool, instead of using the ${B\choose BPK}$ equation, as was implemented by \cite{3}. This implementation was compared to mine, running the algorithm 10.000 times to the input (5,3) in a computer with Intel Core i5 7200U 2.5GHz processor and 8GB DDR4 of memory.  The results are compiled in the table below. 

| Algorithm            | Runtime |
| -------------------- | ------- |
| Solution unoptimized | 72.2 ms |
| Solution optimized   | 58.9 ms |

## References

[1] - Combination. Wikipedia. Available in: https://en.wikipedia.org/wiki/Combination

[2] - Pigeon Hole Principle. Wikipedia. Available in: https://en.wikipedia.org/wiki/Pigeonhole_principle

[3] - Vasani, Hiren. Github repository. Available in: https://github.com/hirenvasani/foobar/blob/master/free_the_bunny_prisoners.py

