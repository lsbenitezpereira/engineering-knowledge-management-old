# String matching

* Let *m* be the length of the pattern, *n* be the length of the searchable text
* Basic aproach
  * Or "Brute-force" or "Naive" algorithm
  * element by element, compare
  * if equal, compare the next
  * problem: you have to check twice if the begining of the pattern is represent but not complete
  * $O(mn)$ (worst case)
* Knuth-Morris-Pratt KMP
  * O(m+n)
  * intuition
    * does the begining (prefix) of the pattern repeat itself across the pattern?
    * Can be do backtrack do avoid repeting to check the same part of the pattern?
  * algorithm
    * j = infex in pattern
    * i = index in string
    * if not match, i++
    * if match, j++ and i++
    * in case of mismach
      * j back to the last repetion (and here is the difference, because we dont move i back)
      * if j==0, i++
      * perceba que podemos aproveitar uma possível repetição no padrão para não ter que voltar i várias casas

## regular expression searching

* Pattern is contituted by symbols

# Fuzzy matching

Paper sobre fuzzy match em geral: [https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjM79vx7tDuAhWESzABHeZKBywQFjABegQIBRAC&url=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fuploads%2F2003%2F01%2Fbm_sigmod03.pdf&usg=AOvVaw0e09VdUNNOz8DRjyFNRTre](https://www.google.com/url?sa=t&rct=j&q&esrc=s&source=web&cd&cad=rja&uact=8&ved=2ahUKEwjM79vx7tDuAhWESzABHeZKBywQFjABegQIBRAC&url=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fuploads%2F2003%2F01%2Fbm_sigmod03.pdf&usg=AOvVaw0e09VdUNNOz8DRjyFNRTre&authuser=0)

Post sobre fuzzy match no contexto de custumer identification: https://www.datarobot.com/blog/using-fuzzy-matching-plus-artificial-intelligence-to-identify-duplicate-customers/



Fuzzy matching com locallity sensitive hashing; Algumas implementações:
https://github.com/embr/lsh
https://github.com/ekzhu/datasketch



* Common problems
  * Mispelling
  * different languages
  * abbreviations
  * Pseudonyms and Alternative naming
  * repetitons: two different entities may have equal values, without nothing being wrong



## Type of inputs

* **Phonetic**
  * records based on vocal input
  * Matching algorthm must handle phonetically similar cases
  * pytohn library: [phonetics](https://pypi.org/project/phonetics/); converts to a canonical representation
* **Pure string**
  * [tutorial](https://www.datacamp.com/community/tutorials/fuzzy-string-python)
* **Structured data**
  * several parameters representing the entity 
  * basic approach: Neirest Neighbors
  * Negative Matching approach: provides additional exclusion rules, usually handcrafted
  * <u>Type of field</u>
    * muttable (with different frequencies) (time dependent or not)
    * Immutable
    * multi valued; voting strategies may be specially useful for them 
  * <u>Implementations</u>
    * https://github.com/dedupeio/dedupe
      https://github.com/facebookresearch/faiss (only for int features)
      https://pixelogik.github.io/NearPy/

## Consolidation strategy

* Hard merge
  * consolidate together multiple records into one single record
  * (-) information loss
* Soft merge: creates an association between the different records that, when read, can be grouped.





# Check for balanced parentheses

https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-python/































