# Conceptualization

* collection of independent computers appears to its users as a single coherent system.
* a software system built on top of a computer network

## Consistency

* eventual vs strong consistency?
* Two-phase commit
  * pattern
  * enfornce storng consistency
* Saga
  * pattern
  * sequence of local transactions, reverted/compensated in case of failure
  * the revertion is not a rollback: the initial actions were actually done
  * enforce eventual consistency


# Grades curriculares

* **CS ufsc**

  * 6.1)6.2)6.3)1.Fundamentos de Computação Distribuída [16 horas-aula]

    - 1.1 Arquitetura de Sistemas Distribuídos
    - 1.2 Paradigmas de Computação Distribuída
    - 1.3 Suporte Computacional
    - 1.4 Comunicação entre Processos
    - 1.5 Sistemas de Arquivos Distribuídos
      2.Tecnologias para Computação Distribuída [28 horas-aula]
    - 2.1 Objetos Distribuídos
    - 2.2 Web Services
    - 2.3 Redes Peer-to-Peer
    - 2.4 Middleware Orientado a Mensagens
    - 2.5 Memória Compartilhada Distribuída
    - 2.6 Computação em Grid e em Nuvem

    3. Algoritmos para Computação Distribuída [28 horas-aula]

    - 3.1 Segurança de funcionamento
    - 3.2 Comunicação em grupo
    - 3.3 Sincronização de Relógios
    - 3.4 Algoritmos de Eleição e Exclusão Mútua
    - 3.5 Detecção de Deadlocks
    - 3.6 Algoritmos de Acordo
    - 3.7 Transações Distribuídas

* **Eng computação ufsc**

  * Habilitar o aluno a projetar e desenvolver sistemas computacionais de natureza distribuída, bem como reconhecer as principais características e algoritmos em um sistema distribuído.
  * UNIDADE 1: Introdução
     Conceitos de sistemas distribuídos
     Comunicação em redes de computadores
     Computação cliente-servidor
     Definição de sistemas distribuídos
     Tipos de sistemas distribuídos
     Exemplos de sistemas distribuídos
  * UNIDADE 2: Processos em Sistemas Distribuídos
     Processos e threads
     Processos cliente-servidor
     Virtualização
     Migração de código
  * UNIDADE 3: Comunicação entre processos distribuídos
     Protocolos de rede em camadas
     Comunicação cliente-servidor
     Sockets
     Chamada remota de procedimento
     Invocação remota de método
     Comunicação em grupo
     Comunicação par a par
  * UNIDADE 4: Concorrência e sincronização
     Sincronização de relógios
     Algoritmos para exclusão mútua
     Algoritmos de eleição
     Algoritmos de acordo
     Transações distribuídas
  * UNIDADE 5: Tolerância a Faltas
     Definição
     Segurança de Funcionamento
     Classificação e Semântica de Faltas
     Fases da Tolerância a Faltas
     Técnicas de Replicação
  * UNIDADE 6: Segurança em Sistemas Distribuídos
     Conceitos de Segurança em Sistemas Distribuídos
     Criptografia Simétrica
     Criptografia Assimétrica
     Certificados Digitais
     Assinatura Digital
  * UNIDADE 7: Estudos de caso de sistemas distribuídos
     Computação em Grid/Cluster
     Web Services/DPWS
     Computação em nuvem
     Internet of Things
     Deep Web
     Docker/Kubernetes
     Bockchain

## Dealing with time

* **Unix timestamph**

* * or Unix time,epoch time, OSIX time
  * Seconds since January 1, 1970 
  * not counting leap seconds
  * System that store the timestamp in 32 bits signed int will have problems in 2038

