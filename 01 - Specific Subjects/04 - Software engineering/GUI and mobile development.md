# Conceptualization

* Downlaod via store (google play, etc)
* (+) Better performance
* (-) Need aprovation (~72hrs) from the store
* (-) Dont work on desktop
* (-) Need to be updated

* **Stores**
  * App store: 99USD by year

##  Hybrid App approach

* using a WebView 
* the web content loaded inside the WebView forms the entire app user experience
* **Apache corova**
  * Allows to develop apps for android using web languages 
* **==Ionic?==**



## PWA

* aplicação híbrida entre web e mobile
* See in *Web Development*

## SDK

* software development kit
* Compiler, debugger, etc
* For Android
  * ==Android software development? That’==
  * Used to develop android apps, writing in Java, C++ or others 







# C\# and .NET framework

* **Mono**
  * Enables the execution of .NET apps in Linux and mac
  * Open source
  * give an  impression in such a way that as if we are running Linux package rather  than executing .exe file

# C++

## Qt

* /cute/

* GUI framework

* cross platform, aimed for mobile and embedded 

* open source

* Written in C++

* have binding for Python

* You build a CLI and use Qt just for interoperatibility

* Qt Widgets

  * to desktop-like applications
  * (-) you really have to work to get your interface to not look like it was designed in the 90's

* Qt quick

  * for building modern, fluid, animated UIs
  * for writing QML applications
  * QML: Qt Modeling Language;  interface markup language; looks like kivy Design Language
  * 

* Atualmente é uma empresa independente
  
* Licença: se você for usar a versão opensource, sua aplicação deve ser opensource. Alguns dos módulos do Qt são proprietários.
  
* ==QT MOC?==
  
* [excelent videoclasses](https://www.youtube.com/watch?v=Y1c-ieVO-UY)

* **Modules**

  * Qt Core: Principais componentes não gráficos do QT.
  * Qt GUI: Componentes gráficos do QT
  * Qt Multimedia: Componentes Multimídia (audio, vídeo, rádio, etc)
  * Qt QML: Componentes do QML e JavaScript
  * Qt Quick: Framework para UI gráfica declarativa
  * Qt SQL: Componentes para acesso a banco de dados
  * Qt Test: Componentes para testes unitários
  * Qt Webkit: Componentes para renderização WWW (Webkit2)
  * Qt Webkit Widgets: Webkit 1 (compatibilidade versões antigas)
  * Qt Widgets: Extensão do Qt GUI para C++ Widgets
  * Qt Network
    * Componentes para programação da rede
    * provê tipos padrões para datas, etc
    * socket (udp/tcp) ou http

* **Elements**

  * <u>QMessageBox</u>: to open a dialog box (pop up)

  * <u>Resource Collection Files</u> (.qrc): standard way of dealing with static files

  * <u>Layouts</u>
    * All resizing is automatically
    * BoxLayout: div
      * QHBox
      * QVBox
      * QGrid
    
  * <u>Widgets</u>
    * QPushButton
    * QSpinBox
    * QSlider
    * QLabel
    
  * <u>windows</u>

    * ?

    * has its own layout to which you can add QToolBars, QDockWidgets, a QMenuBar, and a QStatusBar

      ![img](Images - GUI and mobile development/mainwindowlayout.png)

  * <u>Dialogs</u>

    * like a window
    * For short-term tasks
    
  * <u>Others</u>

    * spacers and strechers

* **Signals e Slots**

  * signal: IO event
  * Slot: action; função que será invocada no evento
  * Slot executada automaticamente toda vez que o signal é emitido
  * Um sinal pode ser conectado a outro sinal
  * Ambos devem possuir a mesma assinatura de parâmetros (tipo e ordem)
  * Excepcionalmente, se um sinal possuir um número maior de parâmetros, os parâmetros adicionais são ignorados
  * um signal pode ser ativado com `emit`, exemplo: `emit foo(10,20);`
  * When using qtDesigner, the signal are automatically connect by name
  * Ex: object `button1` is connected to `on_button1_clicked(){}`
  * This "magic" is provived by the macro Q_OBJECT

* **third party libraries**

  * Qt custom plot: to graphs

* **IDEs**

  * Qt creator
    * Gera documentação em html
    * gera makefile
    * QtDesigner: para desenhar a interface, opcional
    * ==roda bem em linux? Acho que rodou, mas o QtDesigner não==

* **C++ Qt vs C# .NET**

  * C++ using Qt is similar to C# using .NET
  * C# locks you into windows infrastructure, while Qt is portable
  * C# have better support to web, C++ have better support to image processing

* **To run code** (The first creates the makefile, the latter creates the executable):

  ```shell
  qmake
  make 
  ```

## Alegro

aimed at video game and multimedia programming
is not a game engine
cross-platform and open source
initially developed by atari
C API (therefore c++ compatible)
biding for python, lua, and others

# Python

* **tkinter**
  * Very simple
  * comes with python by default
  * desktop focused
* **WxPython**
* **PyQt**
  * :) 
* **BeeWare**
  * open-source
  * native-looking
  * mobile focused
  * (-) less mature than Kivy

## **Kivy**

* open-source
* cross platform (iOS, Android, Linux, Windows, macOS, raspberry pi)
* bem documentado
* roda em cima de OpenGL
* non native-looking
* `source ~/kivy_venv/bin/activate`
* [Youtube course (sentdex)](https://www.youtube.com/watch?v=FjwD0SOGQ1k&list=PLQVvvaa0QuDfwnDTZWw8H3hN_VRQfq8rF)
* **Create a package for Android**
  * Your application needs some complementary files to be able to run on  Android. See Create a package for Android for further reference
  * https://kivy.org/doc/stable/guide/packaging-android.html
  * buildozer
    * tool for creating application packages easily
    * compile to apk
    * https://www.youtube.com/watch?v=g-V-iH4MQI8
    * ==just to kivy?==
* **positioning layouts**

  * much like css’s position (relative, absolute, etc) mixed with bootstrap grid system
  * BoxLayout
    * ?
  * gridLaout
    * arranges children in a matrix
    * you set the columns, the rows are completed automatically
  * FloatLayout: relative position
* **kv Design Language**
* like HTML and CSS
  * hierarquy determined by indentantion, not brackets
  * Assignment with `:` instead of `=` (just like css)
  * properties
    * declare in kv and py files with the same name, and they’ll be binded

# Doing ML

* OpenCV
  * https://opencv.org/android/