* progeny Elixir
* Processes
  * Implement the Actors pattern
  * Erlang runtime implements a supervision
    system, which manages the lifetimes of processes, potentially
    restarting a process or set of processes in case of failure. And
    Erlang also offers hot-code loading: you can replace code in a
    running system without stopping that system