Conceituação
============

-   Sistema de controle de versão (VCS)

-   Todas as informações do git ficam armazenadas na pasta ".git"

-   **Sistema distribuído**

    -   Cada usuário possui uma cópia de todo o histórico).

    -   Vantagens da distribuição: seguro, rápido e mais colaborativo

    -   Comparação com um sistema não-distribuído (nesse caso, subversion):

![](./Images -/media/image1.png){width="4.177083333333333in"
height="4.351388888888889in"}

-   **Funciona por referência**

    -   A evolução do código é representada por um grafo de commits
    
        Ideia simple (ver a ideia correta mais à frente): Entre um nó e outro, o git armazena só um diff
    
    -   diff = deltas in packfiles
    -   Vantagem: o tamanho do repositório fica muito menor
    
    -   Arquivos binários (não-texto, não-código) o git não consegue fazer o diff, então ele armazena o arquivo inteiro a cada commit (mesmo que não tenha sido modificado)
        
    -   Cada commit armazena seus "arquivos" em uma tree, que identifica os blobs que fazem parte desse commit e os seus nomes reais no repositório. O blob é o objeto que contém o conteúdo do
        arquivo (e as diff para os arquivos anteriores)
    -   Ideia correta: snapshots
        -   Representation of the commit’s state
        -   “full copies of the entire file tree”
    
-   **Estados de operação **
    -   Working directory → pasta local
    -   Staging área (ou *index*) → arquivos prontos para serem adicionados (*trackeados*)
    -   Repository → já controlado pelo git

![](./Images -/media/image2.png){width="4.208333333333333in"
height="2.5659722222222223in"}

-   **Ramificações**

    -   Branch → nova linha de desenvolvimento

    -   Master → branch default básica e, usualmente, principal

    -   Head → onde você está (ponteiro para uma posição no grafo de commits)
        
    -   Cada branch é só uma referência para um commit
    
    -   Trocando de branch, os arquivos são substituídos

-   **Outras anotações**

    -   A integridade (não perda de informações) é garantida por um checksum com hash SHA-1 (40 caracteres hexadecimais)

-   **Links uteis**

    -   [[https://git-scm.com/doc]{.underline}](https://git-scm.com/doc)
    -   [[Video-aulas Rodrigo Branas]{.underline}](https://www.youtube.com/watch?v=C18qzn7j4SM&index=1&list=PLQCmSnNFVYnRdgxOC_ufH58NxlmM6VYd1)
    -   [[anti-patterns-you-should-be-aware-of]{.underline}](https://speakerdeck.com/lemiorhan/10-git-anti-patterns-you-should-be-aware-of?slide=25)
    -   [[Controle de release]{.underline}](https://semver.org/lang/pt-BR/)

## Submodules

* dont update
* dont propagate from one branch to another
* there is quite different set of commands (to upgrade, for instance)
* **Adding**
  * clone the repo\
    save the url from where to find the repo\
    the clone is to an specific commit, and stays like that: do not
    automaticaly update
  * `git submodule add https://github.com/chaconinc/DbConnector`
  * to clone a new repo and include all its submodules: `git clone --recurse-submodules --remote-submodules <repo>`
* **Deleting**
  * https://gist.github.com/myusuf3/7f645819ded92bda6677

## Repository management

-   Colaborator \>\> contributor

-   git fork?

-   private repositories and organization repositóries

-   **Permition levels **

    -   There is only **one owner** of a repository owned by a user account; this permission cannot be shared with another user account
-   In a private repository, repository owners can only grant write access to collaborators. Collaborators can\'t have read-only access to repositories owned by a user account.

## Others softwares

* **Subvertion**
  * ?
* **OSTree**
  * version control
  * similar to git, but is designed to track binary files and other large data

Configuração e iniciação
========================

-   **Git config **

    -   Ajusta configurações gerais

    -   \--global → mudanças serão válidas para todo os repositórios
    > (não só o atual)
    
    -   \-- list → lista todos os comandos
    
    -   Core.editor → editor padrão
    
    -   Merge.tool → editor para resolver conflitos em merges
    
    -   user.name "" → nome do usuário que aparecerá no commit
    
    -   user.email "" → nome do email (não é verificado)
    
-   **Git init \<nome\>**

    -   Cria um repositório local

Comandos básicos de funcionamento
=================================

-   **Git add \<file\>**

    -   Adiciona o arquivo à staging

    -   -A → adiciona todos os arquivos

-   **Git status**

    -   Identifica o que está na staging

-   **Git diff**

    -   Mudanças feitas em arquivos monitorados (que estão na stagging)

    -   \--staged → diferença entre o arquivo e sua última versão
    > comitada
    
    -   \<b1\>\...\<b2\> → diferença entre uma branch e outra
    
-   **Git commit**

    -   Joga da staging para o repositório

    -   -m "\<mensagem\>" → já adiciona o título

    -   `git commit --amend`: change commit message

-   **Git show \<commit\>**

    -   Mudanças que foram feitas nesse commit

-   **Git log**

    -   Mostra a lista de commits já realizados

    -   \--al → mostra todas as branchs

    -   \--oneline → mostra de forma organizada (em uma só linha)

    -   \--oneline \--graph → mostra de forma "gráfica"

    -   \--decorate → indica a head?

    -   \--stat → indica algumas estatísticas uteis de cada commit

    -   -p → mostra as modificações

    -   \--follow \<file\> → lista todas as mudanças nesse arquivo

    -   q to quit

-   **Git reflog**

    -   Mostra os identificadores do commit

-   **Git reset \<file\>**

    -   Tira da stagging (não altera seu conteúdo)

## Trabalhando com branches

-   **Git branch**

    -   Identifica a branch atual
    -   Git branch \<nome\> → cria uma nova branch (mas não vai pra ela)
    -   -d \<nome\> → exclui a branch
    -   git branch -d \<branch\>: delete locally (to delete in the remote repository, see in the specific section)
    -   -m \<newName\>: rename the actual branch
-   **Git checkout \<nome\>**

    -   Muda para a branch "\<nome\>"

    -   -b \<nome\> → cria nova branch e já muda pra ela
-   **Git diff \<branch1\> \<branch2\>**

    -   Faz só o diff
-   **Git merge**
    -   Combina o histórico do branch específico com o branch atual
    -   git conflicts: HEAD is ours changes
    -   Non destructive operation (it keeps all the commits)
    -   Tip: do the merge from the feature branch (without deleting the
            master, rsrs):
-   ![](./Images -/media/image3.png){width="3.9805555555555556in"
height="2.66875in"}

## Pull requests

* [Simple tutorial](https://opensource.com/article/19/7/create-pull-request-github)

## Revertendo commits

-   **Git reset \<indentificador\_commit\>**
    -   Volta para o commit

    -   \--soft → aquivos ficam na staging area

    -   \--mixed → arquivos saem da staging área (modo default)

    -   \--hard → arquivos vao embora (deleta até do working directory)

    -   Podemos inclusive ir pra um commit que já foi desfeito anteriormente

* **Se já tiver feito push**

  * `git revert HEAD~1`
* o melhor é \"reverter\" o ultimo commit, em vez de o desfazer. 
  
* "Reverter\", neste contexto, significa criar um commit novo que apague as linhas introduzidas/introduza as linhas apagadas no último commit.
  
* **Checkout and detached mode**

  * ?
  
  

## Rebase

-   similar to merge (integrate changes from other brach to the
    actual branch)
-   alternative to merging
-   Destructive operation (*re-writes* the project history)
-   Git rebase \<branchToIncorporateHere\>
-   moves the entire actual branch to begin on the tip of the master
    branch
-   interactive mode
    -   `-i` option
    -   show you the commits that are going to be rebased
    -   to modify the commits
-   Compation to merge

    -   Losses in safety and traceability

    -   Gain in readablity of the history (because all the commits
        of the feature will be togueder), and you can modify your
        commits to organize them
-   Tips

    -   Never do it in a branch there other people work (because
        change history is bad)

## Filter branch

* `filter-branch` does *not* work with diffs. You’re working with the “snapshot” model of commits here, where each commit is a snapshot of the tree, and rewriting these commits.
* The basic syntax is `git filter-branch  branch_name`. You can use `HEAD` or `@` to refer to the current branch instead of explicitly typing `branch_name`.
* The `--prune-empty` argument is useful here, as it removes commits which are now empty due to the rewrite.
* Rewrites history, with no traces
* **Remove file wrongly committed**
  * https://thomas-cokelaer.info/blog/2018/02/git-how-to-remove-a-big-file-wrongly-committed/
  * `git filter-branch --tree-filter 'rm -rf path/to/your/file' HEAD`

## Stash

-   Área onde é possível armazenar código sem dar commit

-   Ou seja, é uma "zona obscura" em que o arquivo não vai pro
> repositório mas também não se perde se mudarmos de branch

-   O arquivo na stash permanece associado àquela branch

-   O arquivo que vai pra stash tem que estar na staging

-   Cada arquivo na stash recebe um identificado, algo como stash@{0}

-   **Git stash **

    -   save "nome" → Salva o arquivo na stash

    -   list → mostra os arquivos em stash

    -   apply \<identificador\> → trás o arquivo de volta (não tira ele da stash)

-   drop \<identificador\> → exclui da stash
    
    -   pop → tira o arquivo stash do topo e já o dropa

Repositório remoto
==================

-   Qualquer ação no repositório remoto precisa de internet
-   **Git remote**

    -   Add \<alias\> \<url\> → adiciona o repositório remoto (alias geralmente é "origin")
        
    -   Show \<url\> → informações sobre esse remoto
    
    -   Rename "\<no\>" "\<nn\>" → renomeia o repositório (só o alias né?)
    
-   **Git clone \<url\> **

    -   Baixa um repositório remoto

    -   Vem todas as branches e todo o histórico
-   **Git fetch**

    -   Parecido com pull, porém sem o merge (só traz o origin master)

    -   Baixe todo o histórico de um marcador de repositório
-   **Push**

    -   Atualiza o repositório remoto, enviando os arquivos

    -   git push \--delete origin \<branch\>: delete remote
-   **Pull**
-   Atualiza o repositório local, baixando os arquivos (merge automático)
* **trocando o repositorio remoto**
  * `git remote rm origin`
  * `git remote add origin <newURL>`
  * `git branch --set-upstream-to=origin/master master`?

## Github

-   Plataforma de hospedagem para repositórios remotos

-   Além da hospedagem em si, permite realizar operações básicas (criar

    > repositórios, branches, etc) de forma fácil e instintiva,
    > fornecendo também diversos recursos adicionais para criar wikis,
    > issues, entre outros

-   **Projects**

    -   Tarefas grandes a serem realizadas

    -   Permite organizações por cards, seguindo o padrão kanban

-   **Issues**

    -   Problemas
    -   Referência de issue no commit → \#numero\_da\_issue na mensagem
    
-   **PAT authentication**

    -   https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token
    -   "Once you have a token, you can enter it instead of your password when performing Git operations over HTTPS."
    -   to save the next password, git config --global credential.helper cache
    -   to rmeove the cached password, git config --global --unset credential.helper


## Bitbucket

* supports Git and Mercurial VCS (but not SVN).
* (+) better pricing 
* better CI/CD 
* part of the Atlassian family