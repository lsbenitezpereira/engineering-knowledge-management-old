# Conceptualization

-   Information held on a blockchain exists as a shared --- and
    continually reconciled --- database
-   the network reconciles every transaction that happens in ten-minute
    intervals
-   A blockchain is very
    similar to a linked list—each block contains a pointer to the previous block. A key
    difference in blockchain is that each block contains a hash pointer to the previous
    block. A hash pointer contains two things: a pointer, or reference to the location of
    the previous block, and the cryptographic hash of that block. Storing the crypto-
    graphic hash of the previous block allows us to verify that the block we are pointing
    to has not been tampered with
-   Block

    -   group of transations

    -   Each block has a cryptographic hash, a list of validated
        transactions, and a reference to the previous block's hash.

    -   
-   
-   sharding: distribute to burden to ==verify (or store? Or both?)== transations
-   
-   Every node is an "administrator" of the blockchain, and joins the
    network voluntarily (in this sense, the network is decentralized).
    However, each one has an incentive for participating in the network:
    the chance of winning Bitcoins.
-   Through each node gossiping to one
    another, they learn of new transactions

![](./Images/Fundamentos de Blockchain/media/image1.png){width="7.2555555555555555in"
height="4.279166666666667in"}

* 
* **Permissioned, Permissionless, and Consortium**
  * Permissionless blockchains, such as Bitcoin, are designed so that anyone can join
    and participate in the network without having to establish their identity.
  * permissioned ledgers are
    primarily used in private applications where strong indicators of identity are required
    to join the network
  * Consortium blockchains: similar to permissioned blockchains, but new participants are authenticated by a predetermined group of private entities.
* **Smart contracts**
  * A major innovation in blockchain, considered Blockchain 2.0, is a technology known as contracts
  * Ethereum is the most popular blockchain
    system with embodied smart contracts

## Concensus mecanism

* **Proof of work**
  * ?
  * <u>Mining</u>
    * validation of transactions. For this effort, successful miners obtain new cryptocurrency as a reward; cria novos ativos/moedas; Mining has one and only one purpose: providing a decentralized, permissionless mechanism for determining which version of history to accept, in case there are multiple valid competing versions;
    * (at least in bitcoin?) The mining power is set so that the miners need 10 minutes in average to mine a block. If 50% of the miners would disappear because it's not profitable any more, the difficulty would decrease so that it's profitable again.
* **Proof of stake**
  * Validators are responsible for the same thing as miners in proof-of-work
  * [etherum docs of PoS](https://ethereum.org/en/developers/docs/consensus-mechanisms/pos/#:~:text=Ethereum%20is%20moving%20to%20a,of%2Dwork%20(PoW).)
  * <u>staking</u>
    * ?
    * how it works: you lock an amount of coins; … ; gain coins passively?

## Security

* ==Does Bitcoin suffer from the probability of hash collisions?==
* **Sybil attack**
  * 51% take-over of a network
  * is one of the attacks that is looked at when it comes to consensus and node propagation

# Modalidades
## Criptocurrencies

* **Utility coin**
  * exemple - Chiliz: eared towards mainstream consumers

### Metrics to anayse

* **MVRV z score**
  
  * quanto está alto (+- acima de 8), então a moeda esta no seu pico (e está na hora de vender)
  * quando está baixo (+- abaixo de 2), então a moeda está no seu vale (e está na hora de comprar)
  
* **Cruzamentos entre médias móveis**

  * A ocorrência dos cruzamentos envolvem duas médias móveis importantes: a de 50 dias e a de 200 dias. Existem dois padrões importantes de cruzamento, cada um com seu significado.

    No primeiro caso, a média de 50 dias pode cruzar com a média de 200 dias em tendência de alta, formando a chamada Cruz Dourada. Este padrão é indicativo de forte tendência de alta no preço do Bitcoin.

    A Cruz Dourada precedeu todos os fortes movimentos de alta do BTC desde 2013, todos terminados em máximas históricas.

    Por outro lado, a Cruz da Morte traça o caminho oposto. O evento ocorre quando a média de 50 dias cruza com a de 200 dias em tendência de baixa. A Cruz da Morte acontece após uma forte valorização do BTC, marcando o fim do movimento.

### Bitcoin

* Bitcoin was presented as the peer-to-peer electronic payment system, and
  blockchain was the proposed mechanism that allowed it to work
* In Bitcoin, hashing is performed by miners, and the hash produced must be lower
  than the target hash set by the network. To find a hash meeting this criteria, miners
  try different nonce values and check if the output hash is lower than the target,

### Ether

* Ethereum is a decentralized, open-source blockchain featuring smart contract functionality
* Ether (ETH) is its cryptocurrency
* O mundo está sendo tokenizado e ETH é a principal cripto utilizada para infraestrutura nesse novo mundo de tokens
* **2.0**
  * changes to proof of stake; increase transaction throughput; people have to chose to change?; also will implement sharding, but the migration to it will be serate than the migration to proof of stake; 
  * they started the transition at december 2020; after the transition is completed (~2022), etherium 1.0 and 2.0 will merge
* **staking at etherium**
  * probabilidade de receber reward é inversamente proporcional à quantidade de coins stakeados?
  * Nem toda moeda POS pode ser feito o stake de forma simples, algumas são POS delegado (apenas algumas pessoas selecionadas podem fazer stake),

## DeFi

* Decentralized finances
* são uma generalização de smart contracts?
* **exchange descentralizada**
  * ?
  * <u>algumas</u>
    * uniswap
    * pancake swap
    * sushi swap
    * 1inch
    * Curve
* **Stable coin**
  * type of criptocurrency
  * criptocoin backed by real money, with 1-to-1 conversion
  * issued by a smart contract
  * <u>algumas</u>
    * USDC
    * DAI
    * TUSD
* **PLataformas de emprestimos**
  * ?
  * <u>algumas</u>
    * aae
    * compound
* **apostas**
  * <u>algumas</u>
    * 

## Smart Contracts

* **Oracle services**
  * provide a way that information from the outside world can be inputted into smart contracts on the blockchain
  * <u>Chainlink</u>
    * It’s an Oracle service
    * currently operating on the Ethereum blockchain but is designed to be agnostic
    * Chainlink also has a token, LINK, that is used to cover the fees for using the service

# Security, stability

*  para uma rede ser verdadeiramente segura é preciso maximizar o número de usuários que podem executar um nó da blockchain.

Possibilities
=============

Currently, however, users who want to hail a ride-sharing service have
to rely on an intermediary like Uber. By enabling peer-to-peer payments,
the blockchain opens the door to direct interaction between parties ---
a truly decentralized sharing economy results.

using blockchain to make it easier to pay for charging EVs

# Other

* ERC 20: padrão de programação usado pelo etherium

## Consensus algorithms

* solve a set of problems about xxx
* origininated with the “The Byzantine Generals Problem”, proposed in 1982: The generals must have
  an algorithm to guarantee that (A) all loyal generals decide upon the same plan of action
  ... (B) A small number of traitors cannot cause the loyal generals to adopt a bad plan.
* A Byzantine Fault Tolerant system is one that can tolerate the
  Byzantine Generals Problem
* **Proof-of-Work (PoW)**
  * Computation is needed to solve cryptographic puzzle to
    ensure consensus
  * (-) computationally heavy
  * nodes trust the longest chain—the one with the most blocks added to it by
    other miners. Thus PoW is safe as long as 51% of the compute power is owned by
    honest miners.
* **Proof-of-Stake (PoS)**
  * (+) computationally light
  * Ability to mint a new block is proportional to the stake in
    the blockchain network
* **Proof-of-Activity (PoA)**
  * (-) computationally heavy
  * Computation is needed to solve cryptographic puzzle to
    only known validators who are active
* **Proof-of-Activity (PoA)**
  * (+) computationally light
  * Use of random time intervals that determine which node is the current miner

















