## Página 17

### A

--> A=[0.8 -0.4 0; -0.4 0.8 -0.4; 0 -0.4 0.8]
--> b = [41; 25; 105]
--> es= 0.00001
--> gauss_seidel(A,b, 1, es)
 ans  =
   173.74999
   244.99999
   253.74999
Numero de iterações: 25.

### B

--> gauss_seidel(A,b, 1.2, es)
Converge com apenas 12 iterações

--> gauss_seidel(A,b, 2, es)
Não converge

## Página 18

### A

A = [3 -3 -3; 4 7 -4; 4 -4 10]

b = [7; -20; 70]

es= 0.00001

gauss_seidel(A,b, 1, es)

 ans  =

   3.9999999
  -2.6666667
   4.3333334

Numero de iterações: 34

### B

gauss_seidel(A,b, 0.93, es)

Numero de iterações: 20