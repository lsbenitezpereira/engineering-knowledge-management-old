# Tarefa I - Terceiro Lote

Encontrar a raiz de:
$$
f(x) = \cos(x) - x*e^x
$$
através dos métodos da bissecção e da falsa posição, no intervalo $x \in [-2,0]$ com critério de parada $e_s < 10%$

Adotaremos sempre 4 algarismos significativos de precisão, com arredondamento da NBR

## Bissecção

| **$y_{l}$**           | **$y_{m}$**                     | **$y_{r}$**      | **$e$** |
| ------------------------------- | ------------------------------- | ---------------- | ------- |
| $f(-2)=-0,1455$ | $f(-1)=0.9082$       | $f(0)=1,00$       | $100\%$   |
| $f(-2)=-0,1455$ | $f(-1,5)=0,4054$    | $f(-1)=0.9082$    | $33,33\%$ |
| $f(-2)=-0,1455$ | $f(-1,75)=0,1258$    | $f(-1,5)=0,4054$  | $14,28\%$ |
| $f(-2)=-0,1455$   | $f(-1,875)=-0,01199$ | $f(-1,75)=0,1258$ | $6,666\%$ |

Foram preciso quatro iterações para que $e<10\%$, e a taxa de convergência foi $R=\frac{33,33}{14,28} \approx \frac{14,28}{6,666} \approx 2,334$

Interpretação gráfica do método:

<img src="Images - Tarefa I - Terceiro Lote/image-20200315130624379.png" alt="image-20200315130624379" style="zoom:67%;" />

## Falsa posição

| **$y_{l}$**                     | **$y_{m}$**                     | **$y_{r}$**      | **$e$** |
| ------------------------------- | ------------------------------- | ---------------- | ------- |
| $f(-2)=-0,1455$                 | $f(-1,746)=0,1303$              | $f(0)=1,00$      | $100\%$ |
| $f(-2)=-0,1455$                 | $f(-1,866)=-2,187 \times 10^{-3}$ | $f(-1,746)=0,1303$ | $6,431\%$ |
| $f(-1,866)=-2,187 \times 10^{-3}$ | $f(-1,864)=-5,246\times 10^{-6}$  | $f(-1,746)=0,1303$ | $0,1073\%$ |

Foram preciso tres iterações para que $e<10\%$, e a taxa de convergência foi $R=\frac{6,431}{0,1073}=59,93$

<img src="Images - Tarefa I - Terceiro Lote/image-20200315130421787.png" alt="image-20200315130421787" style="zoom:67%;" />