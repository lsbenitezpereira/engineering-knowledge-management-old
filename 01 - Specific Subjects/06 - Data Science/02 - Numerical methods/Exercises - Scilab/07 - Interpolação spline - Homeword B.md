# Spline cúbicas 

Resolva os exercícios propostos na vídeo aula

## 1)

`[y, b, c, d, ye] = splines([8, 11, 15, 18, 22],[5, 9, 10, 8, 7],12.7, 'n')`

`ye=10.118896`

## 2)

 Ajuste splines cúbicos não é nó aos mesmos dados usados no exemplo 3. Apresente a estimativa do valor de y em x = 5.

`[y, b, c, d, ye] = splines([3, 4.5, 7, 9],[2.5, 1, 2.5, 0.5],5, 'k')`

`ỳe=1.1518519`

## 3)

 Um braço de um robô deve passar nos instantes t0, t1, t2, t3, t4 e t5 por posições pré-definidas θ(t0), θ(t1), θ(t2), θ(t3), θ(t4) e  θ(t5), onde θ(t) é o ângulo (em radianos) que o braço do robô faz com o  plano XOY.

Com base nos dados da tabela, aproxime a trajetória do robô por uma spline cúbica amarrada. Indique também uma aproximação da posição do  robô no instante 𝑡 = 1.5 𝑠.

 Calcule uma aproximação à velocidade do robô no instante 𝑡 = 1.5 s.

`[y, b, c, d, ye] = splines([1, 3, 4, 6],[1, 1.75, 1.25, 3.15],1.5, 'c', [0.25 0.15])`

`ye=1.2230957`

`y1_prime = (ye-1)/(1.5-1)=0.4461914`

