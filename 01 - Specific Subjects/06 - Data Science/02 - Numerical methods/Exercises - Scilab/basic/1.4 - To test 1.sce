/**********************************************
 Name        : Age classifier 
 Author      : Leonardo Benitez
 Date        : 03/04
 Description : Classifies the age
 **********************************************/
 
 while (1) then
    age = input("Whats your age? ");
    if age < 5 then
    	printf("baby");
    elseif age <= 7 then
    	printf("intanfile A");
    elseif age <= 10 then
    	printf("infatile B");
    elseif age <= 13 then
    	printf("Juvenile A");
    elseif age <= 17 then
    	printf("Juvenile B");
    else
    	printf("Adult");
    end 
end
