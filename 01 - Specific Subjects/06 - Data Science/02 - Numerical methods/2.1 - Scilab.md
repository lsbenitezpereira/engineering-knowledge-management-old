Scilab
=================

-   Scietific language/software, based on Matlab

-   Interpreted language

-   Commentary with // and /\*

-   Clear screen comand → clc

-   Usual extension of scilab codes: ".sce"

-   Assign variable without ";" will show the result in the screen

## Operators

-   **Logical**

    -   &, \|, \~

-   **Bitwise**

    -   Bitand ()

    -   Bitor ()

    -   Isequalbitwise ()

    -   Bitcmp() → complement

    -   Bitget (x, pos) → return bit at the specific position

    -   Bitset (x, pos) → set bit

    -   Bitxor

-   **Relational**

    -   Different → \<\> or \~=

-   **Arithmetical**

    -   Modulo (a,b): rest of integer division

    -   Don't exist increment operator (x++) and even compound
        assignment

## Flow control

-   Don't use () and {}

-   **Selection**

    -   If, then, elseif, else, end

    -   Select-case

![](./Images/2.2 - Scilab - Others/media/image1.png){width="4.333333333333333in"
height="2.1666666666666665in"}

-   **Iteration**

    -   while (condition) then *command* end

    -   for *variable* = *begin : increment : end \<command\> end* (if
        increment is not declared, default is 1)

    -   Continue and break can be used

## Functions

-   generic sintax:

```
function \<ret1, ret2, ..., retn\> = nome(arg1,arg2, ... ,arg n)
                                                                
\<comandos executáveis\>                                        
                                                                
end function                                                    
```

-   **function in a file**

    -   Usually, the scilab function are put inside a separate file

    -   Including the function → exec ("path\\nome\_arq\_função" ,-1) →
        the path can be omitted, if the file is in the same directory


## Basic I/O

-   **Screen output **
-   disp (variables) → printed in inverse sequence
    
-   printf → equal to C
    
-   **Input**
-   Number → x = input ("say a number")
    
-   String → Name = input ("give-me your name: ", "s")

# Data types

-   It's not necessary to declare variables (they are double by default)

-   Case sensitive

-   Allow the use of \'\#\', \'!\', \'\$' and \'?\'

-   **Constants**

    -   Scilab already have a lot of constants natively

    -   Represented in type real, with 64 bits

    -   \%pi, %e

## Numeric types

-   **Integer**

    -   Int8, int16 and int32

    -   To specify, you can do a cast:

y = int8(16);

-   **Boolean**

    -   Range → %t or %f

    -   Operators → and (&), or (\|), not(\~)

-   **Complex**

    -   The imaginary constant is represented by %i

    -   Abs (variable) → absolute value

    -   Real (variable) → get the real part

    -   Imag (variable) → get the imaginary part

    -   Atan (imag, real) → argument

    -   Conj (variable) → conjugate

## Strings

-   Does not differentiate between a character and a string

-   Concatenation → just use +

-   Quotes in string → just use two consecutive. *Ex:* a= "he said:
    ""fuck you!"" ";

## Matrix

-   In fact, all the variables are unidimensional matrix

-   Can have any dimension

-   The first element had index 1

-   The elements need to be coherent with the dimension (each line with
    same number of elements)

-   Columns are separated by spaces, lines by ';'

    It's possible to used a *for* in the declaration

-   *Example*: declaring

![](./Images/2.1 - Scilab - Data types/media/image1.png){width="2.279166666666667in"
height="1.1791666666666667in"}

-   *Example:* obtaining element from line 2 column 1:

![](./Images/2.1 - Scilab - Data types/media/image2.png){width="1.3805555555555555in"
height="0.8111111111111111in"}

-   **Matrix manipulation**

    -   Is possible to select only a part (rectangular and regular) of
        the matrix

    -   *Example:*

![](./Images/2.1 - Scilab - Data types/media/image3.png){width="4.61875in"
height="1.0520833333333333in"}

-   **General functions**

    -   X = length (A) → return number of elements

    -   \[m,n\] = size (A) → return number of lines and columns

-   **Function to Initialization **

    -   A = Zeros (I,j) → initialize with zeros

    -   A = Ones (i.j) → initialize with ones

    -   A = eye (i,j) → create an identity matrix

-   **Other functions:**

![](./Images/2.1 - Scilab - Data types/media/image4.png){width="3.7083333333333335in"
height="1.9375in"}

![](./Images/2.1 - Scilab - Data types/media/image5.png){width="3.8020833333333335in"
height="1.9375in"}

-   **Operations**

    -   Just as simple as normal numbers

    -   Adding a dot in the operator indicates a element by element
        operation

    -   *Example:* multiplication EbE → A .\* B

## Struct

-   Structs are weird in scilab

-   You need to use a function to declare one

-   *Example:*

Struct\_var=struct (field1, value1, field2, value2);

Struct\_var.field1 = 0;

# System manipulation

* **Polinomial type**

  * You can just type with variable `%s`, or using `poli()`

  * Ex: `den=poly([1 7],'s','coeff')`

  * partial fraction expansion in domain $s$:

    ```
    s = %s
    num = s+3
    den = (s+1)*(s+1)
    F = pfss(num/den)
    ```
  
    
  
* **Syslin**

  * linear system definition

  * `'c'` :continous

  * sistemas podem ser manipulados

  * time response to step input given the Transfer Function of a system:
  
    ```
     s= %s;
     K= 1; T= 1;
     num= K; den= T*s+1;
     tf= num/den;     // ainda não é a FT
     FTMF= syslin('c', num/den);
     t= linspace(0,10,501);  // t=0:0.02:10 (501 pontos)
     step= csim('step',t,FTMF);
     plot2d(t,step);
     xgrid();
     xtitle('Resposta ao degrau','tempo','Magnitude');
    
    ```
    
  
* **csim**

  * simulation of the controlled linear system	

* * 

* **Laplace**

  * transform and inverse transform?

* **Others**

  * :keyboard: matlab: `<a>/.<b>`: FTMF de a realimentado com b