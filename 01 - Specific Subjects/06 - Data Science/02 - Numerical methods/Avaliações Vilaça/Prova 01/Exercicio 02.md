## A

Montei o sistema:

![image-20200923115951471](Images - Exercicio 02/image-20200923115951471.png)

Mas verifiquei com o scilab que este não iria convergir com o método de Gauss Seidel:

```matlab
A=[-5/6 1/3 1/2; 1/3 5/12 0; 1/2 -1 -2/3]
b=[-4;0;0]
gauss_seidel(A,b,1.1)

Processo não convergiu
```

Utilizando o método gaussp, obtive:

```
gaussp(A, b)

 ans  =
   32.
  -25.6
   62.4
```

## B

![image-20200923115918442](Images - Exercicio 02/image-20200923115918442.png)

## C

![image-20200923115857555](Images - Exercicio 02/image-20200923115857555.png)