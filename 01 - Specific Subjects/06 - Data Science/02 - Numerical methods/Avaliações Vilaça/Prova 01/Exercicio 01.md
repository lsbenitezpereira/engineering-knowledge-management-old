

## A

![image-20200923131307374](Images - Exercicio 01/image-20200923131307374.png)

## B

Segundo o teorema exposto em sala de aula:

> “Se o erro relativo de qualquer número não é maior que $1/(2*10^n)$, o
> número está com certeza correto para n algarismos significativos”

Dessa forma, a resposta está correta para $n=log(\frac{1}{2*0.02})=1.397$, ou seja, 1 algarismo significativo.

## C

Utilizando o método gráfico e a ferramenta [Desmos](https://www.desmos.com/calculator), obtemos:

![image-20200923121352761](Images - Exercicio 01/image-20200923121352761.png)
$$
V_D \approx 0.695808
$$

## D

Tentei utilizar a função do scilab, mas o programa fecha sem indicar o motivo do erro. Comandos utilizados:

```matlab
bissecao('10-x-10^(-11)*(%e^(x/0.025248)-1)', 0.6, 0.8)
```

A mesma função trava mesmo quando utilizado uma função mais simples, como:

```
bissecao('x', -1, 1)
```

Dessa forma, utilizei a ferramenta online [PlanetCalc](https://planetcalc.com/3718/):

![image-20200923124154766](Images - Exercicio 01/image-20200923124154766.png)

![image-20200923124208602](Images - Exercicio 01/image-20200923124208602.png)

