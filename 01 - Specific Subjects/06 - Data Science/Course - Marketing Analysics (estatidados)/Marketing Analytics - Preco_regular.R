#Bloco geral 

#Marketing Analytics - Prof´s Thiago Marques e Marcos Severo


#Importando bibliotecas

vetor_pacotes=c("readr","ggplot2","plotly","e1071",
                "dplyr","Hmisc","DescTools","esquisse",
                "gridExtra","readxl","tidyr","kableExtra","stringr")

#install.packages(vetor_pacotes)
lapply(vetor_pacotes, require, character.only = TRUE)

library(readr)
library(ggplot2)
library(plotly)
library(e1071)
require(dplyr)
require(Hmisc)
require(esquisse)
library(DescTools)
require(gridExtra)
library(readxl)
library(tidyr)

# BLOCO CARREGANDO O BANCO 

#Direcionando a pasta no diretório que contém o arquivo
setwd("C:\\Users\\teste\\Desktop\\Marketing Analytics - Thiago Marques e Professor Marcos Severo")

#Conferindo
getwd()

#Carregando o banco de dados

#base do R (Mais flexível e menor performance de velocidade)
dados_choc <- read.csv("BD_marcas_de_chocolate.csv",
                       header = TRUE, 
                       sep=";", 
                       dec = ","
)

dados_choc_excel <- read_excel("BD_marcas_de_chocolate.xlsx",sheet="Plan1"
                               
)


#Verificando o nome das variáveis do banco
names(dados_choc)

#renomeando a primeira variável
names(dados_choc)[1]="semana"

#BLOCO DA ANÁLISE DESCRITIVA DAS VARI?VEIS

#Resumindo os dados
s=summary(dados_choc)  
d=describe(dados_choc)

#boxplot
boxplot(dados_choc$Vendas_Budget)

#Selecionando só os resumos de interesse
d$Vendas_Budget
d$Vendas_Budget$values
s[1:6,1:2]

#Verificando NA´S
resumo_nas=dados_choc %>%
  select(everything()) %>%  
  summarise_all(list(~sum(is.na(.))))


#--------------- -------------------
#Preco_regular 

#Geral

#selecionando a variável semana
dados_choc_Preco_regular_full = dados_choc %>%  select(semana)

#Selecionando só as séries de preço atual de ambos os anos
dados_choc_Preco_regular_full = dados_choc %>%  select(starts_with("Preco_regular"))

#Utilizando a base do R para timeseries
dados_choc_Preco_regular_full_ts=ts(dados_choc_Preco_regular_full)
plot(dados_choc_Preco_regular_full_ts)

library(lattice)

xyplot(dados_choc_Preco_regular_full_ts, superpose = F) 



cv=function(x){
  sd(x)/mean(x)
}

# Ano 1

#Calculando estatísticas resumo para o ano 1
dados_choc_Preco_regular_ano1_full = dados_choc %>%
  filter(semana %in% c(1:52)) %>%  
  select(semana|starts_with("Preco_regular"))

dados_choc_Preco_regular_ano1 = dados_choc %>% filter(semana %in% c(1:52)) %>%  
  summarise(across(starts_with("Preco_regular"),
                   list(media=mean,
                        mediana=median,
                        desvio=sd,
                        cvar=cv
                   )))

resumo_budget_ano1=dados_choc_Preco_regular_ano1 %>% select(contains("Budget"))
resumo_Whittake_ano1=dados_choc_Preco_regular_ano1 %>% select(contains("Whittake"))
resumo_Cadbury_ano1=dados_choc_Preco_regular_ano1 %>% select(contains("Cadbury"))
resumo_Donovan_ano1=dados_choc_Preco_regular_ano1 %>% select(contains("Donovan"))
resumo_Pams_ano1=dados_choc_Preco_regular_ano1 %>% select(contains("Pams"))

dados_consolidados_Preco_regular_ano1=t(data.frame(
  resumo_budget_ano1=t(resumo_budget_ano1),
  resumo_Whittake_ano1=t(resumo_Whittake_ano1),
  resumo_Cadbury_ano1=t(resumo_Cadbury_ano1),
  resumo_Donovan_ano1=t(resumo_Donovan_ano1),
  resumo_Pams_ano1=t(resumo_Pams_ano1)
))

colnames(dados_consolidados_Preco_regular_ano1)=c("Media","Mediana","Desvio","CV")

dados_consolidados_Preco_regular_ano1 %>% kbl %>% kable_material_dark( 
  full_width = F, 
  font_size = 15)

# Ano 2

#Calculando estatísticas resumo para o ano 2
dados_choc_Preco_regular_ano2_full = dados_choc %>%
  filter(semana %in% c(53:104)) %>%  
  select(semana|starts_with("Preco_regular"))

dados_choc_Preco_regular_ano2 = dados_choc %>% filter(semana %in% c(53:104)) %>%  
  summarise(across(starts_with("Preco_regular"),
                   list(media=mean,
                        mediana=median,
                        desvio=sd,
                        cvar=cv
                   )))

resumo_budget_ano2=dados_choc_Preco_regular_ano2 %>% select(contains("Budget"))
resumo_Whittake_ano2=dados_choc_Preco_regular_ano2 %>% select(contains("Whittake"))
resumo_Cadbury_ano2=dados_choc_Preco_regular_ano2 %>% select(contains("Cadbury"))
resumo_Donovan_ano2=dados_choc_Preco_regular_ano2 %>% select(contains("Donovan"))
resumo_Pams_ano2=dados_choc_Preco_regular_ano2 %>% select(contains("Pams"))

dados_consolidados_Preco_regular_ano2=t(data.frame(resumo_budget_ano2=t(resumo_budget_ano1),
                                                 resumo_Whittake_ano2=t(resumo_Whittake_ano1),
                                                 resumo_Cadbury_ano2=t(resumo_Cadbury_ano1),
                                                 resumo_Donovan_ano2=t(resumo_Donovan_ano1),
                                                 resumo_Pams_ano2=t(resumo_Pams_ano1)
))

colnames(dados_consolidados_Preco_regular_ano2)=c("Media","Mediana","Desvio","CV")

dados_consolidados_Preco_regular_ano2 %>% kbl %>% kable_material_dark(full_width = F,
                                                                    font_size=15)

# Ano 1 Base long                                                                                                font_size=15)

#transformando o formato wide em long 

#Colocando todas as séries de preços de chocolates juntas
dados_choc_Preco_regular_ano1_full_long = dados_choc_Preco_regular_ano1_full %>%
  select(semana, 
         contains("Preco_regular")
  ) %>%
  gather(key = "marcas", value = "valor_Preco_regular", -semana)

library(stringr)
#Criando a coluna das marcas extraindo padrões dos textos para criar uma coluna de marcas
dados_choc_Preco_regular_ano1_full_long$marcas_novo=str_extract(dados_choc_Preco_regular_ano1_full_long$marcas,'[:alpha:]+$')

#Gráfico de linha utilizando a base long
grafico_linhas=ggplot(dados_choc_Preco_regular_ano1_full_long, 
                      aes(x=semana, y=valor_Preco_regular)) +
  geom_line(aes(color = marcas_novo), size = 1) + 
  scale_x_continuous(breaks = seq(1,52,1)) +
  geom_hline(yintercept = c(dados_consolidados_Preco_regular_ano1[,1][1],dados_consolidados_Preco_regular_ano1[,1][2]))+
  xlab("Semana") + 
  ylab("Ganho em Vendas") +
  labs(color = "Marcas")
ggtitle("Evolução do preço atual de chocolate por semana")


grafico_linhas_Preco_regular=grafico_linhas               
ggplotly(grafico_linhas_Preco_regular)

#Gráfico de linha utilizando a base long
grafico_linhas=ggplot(dados_choc_Preco_regular_ano1_full_long, 
                      aes(x=semana, y=valor_Preco_regular)) +
  geom_area(aes(color = marcas, fill = marcas_novo), 
            alpha = 0.5, position = position_dodge(0.8))+
  #geom_hline(yintercept = c(dados_consolidados_ano1[,1][1],dados_consolidados_ano1[,1][2]))+
  scale_x_continuous(breaks = seq(1,52,2)) +
  xlab("Semana") + 
  ylab("Ganho em Vendas") + 
  ggtitle("Evolução do preço atual de chocolate por semana") 

grafico_area_Preco_regular=grafico_linhas               
ggplotly(grafico_area_Preco_regular)

#Segunda forma de obter as estatísticas resumo e geral, utilizando formato long

dados_choc_Preco_regular_ano1_full_long_segunda_forma =
  dados_choc_Preco_regular_ano1_full_long %>% 
  group_by(marcas_novo) %>% 
  summarise( 
    media=mean(valor_Preco_regular),
    mediana=median(valor_Preco_regular),
    desvio=sd(valor_Preco_regular),
    cvar=cv(valor_Preco_regular)
  )

#Deixar a tabela bonitinha preta com o kable
dados_choc_Preco_regular_ano1_full_long_segunda_forma %>% 
  kbl %>% 
  kable_material_dark(full_width = F)

#Exportar o arquivo em csv
write.csv(dados_choc_Preco_regular_ano1_full_long_segunda_forma,"resumoano1precoatual.csv", row.names = F)

# Ano 2 Base long                                                                                                font_size=15)

#transformando o formato wide em long 

#Colocando todas as séries de preços de chocolates juntas
dados_choc_Preco_regular_ano2_full_long = dados_choc_Preco_regular_ano2_full %>%
  select(semana, 
         contains("Preco_regular")
  ) %>%
  gather(key = "marcas", value = "valor_Preco_regular", -semana)

library(stringr)
#Criando a coluna das marcas extraindo padrões dos textos para criar uma coluna de marcas
dados_choc_Preco_regular_ano2_full_long$marcas_novo=str_extract(dados_choc_Preco_regular_ano2_full_long$marcas,'[:alpha:]+$')

#Gráfico de linha utilizando a base long
grafico_linhas=ggplot(dados_choc_Preco_regular_ano2_full_long, 
                      aes(x=semana, y=valor_Preco_regular)) +
  geom_line(aes(color = marcas_novo), size = 1) + 
  scale_x_continuous(breaks = seq(53,104,1)) +
  geom_hline(yintercept = c(dados_consolidados_Preco_regular_ano2[,1][1],dados_consolidados_Preco_regular_ano2[,1][2]))+
  xlab("Semana") + 
  ylab("Ganho em Vendas") +
  labs(color = "Marcas")
ggtitle("Evolução do preço atual de chocolate por semana")


grafico_linhas_Preco_regular=grafico_linhas               
ggplotly(grafico_linhas_Preco_regular)

#Gráfico de linha utilizando a base long
grafico_linhas=ggplot(dados_choc_Preco_regular_ano2_full_long, 
                      aes(x=semana, y=valor_Preco_regular)) +
  geom_area(aes(color = marcas, fill = marcas_novo), 
            alpha = 0.5, position = position_dodge(0.8))+
  #geom_hline(yintercept = c(dados_consolidados_ano1[,1][1],dados_consolidados_ano1[,1][2]))+
  scale_x_continuous(breaks = seq(53,104,2)) +
  xlab("Semana") + 
  ylab("Ganho em Vendas") + 
  ggtitle("Evolução do preço atual de chocolate por semana") 

grafico_area_Preco_regular=grafico_linhas               
ggplotly(grafico_area_Preco_regular)

#Segunda forma de obter as estatísticas resumo e geral, utilizando formato long

dados_choc_Preco_regular_ano2_full_long_segunda_forma =
  dados_choc_Preco_regular_ano2_full_long %>% 
  group_by(marcas_novo) %>% 
  summarise( 
    media=mean(valor_Preco_regular),
    mediana=median(valor_Preco_regular),
    desvio=sd(valor_Preco_regular),
    cvar=cv(valor_Preco_regular)
  )

#Deixar a tabela bonitinha preta com o kable
dados_choc_Preco_regular_ano2_full_long_segunda_forma %>% 
  kbl %>% 
  kable_material_dark(full_width = F)

#Exportar o arquivo em csv
write.csv(dados_choc_Preco_regular_ano2_full_long_segunda_forma,"resumoano2precoatual.csv", row.names = F)

# Geral Transformação de Variáveis (Feature Engineering)

#Voltando na base original e Criando variáveis transformadas 
#(Feature Engineering) 

dados_choc_novas_var = dados_choc %>% mutate ( mercado = Vendas_Budget + Vendas_Pams + Vendas_Cadbury + Vendas_Whittaker + Vendas_Donovan,
                                               participacao_budget=Vendas_Budget/mercado,
                                               participacao_Pams=Vendas_Pams/mercado,
                                               participacao_Cadbury=Vendas_Cadbury/mercado,
                                               participacao_Whittaker=Vendas_Whittaker/mercado,
                                               participacao_Donovan=Vendas_Donovan/mercado
)
names(dados_choc_novas_var)                          

dados_choc_full_long = dados_choc_novas_var %>%
  select(semana, 
         contains("Preco_atual"),
         contains("Preco_regular"),
         contains("Anuncio"),
         contains("Display"),
         contains("AD"),
         contains("Vendas"),
         contains("participacao"),
         contains("mercado")
         
  ) %>%
  gather(key = "marcas", value = "valor", -semana)

#Criando a coluna das marcas extraindo padrões dos textos para criar uma coluna de marcas
dados_choc_full_long$marcas_novo=str_extract(dados_choc_full_long$marcas,'[:alpha:]+$')


