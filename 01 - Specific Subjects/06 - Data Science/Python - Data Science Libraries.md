# Numpy

* numeric programming
* to linear algebra
* implement arrays and matrix
* To work with big data and statistics. 
* Similar to matlab
* [Cheat sheet](https://www.dataquest.io/blog/numpy-cheat-sheet/)
* **Arrays**
  * Are not implemented in the core of python, just in Numpy
  * Elements must be of the same data type
  * Matrix are just like arrays, with little differences in sintax (like reciving an tuple insted of a number in the creation methods). 
* **Elements selection**
  * Are threated as python sequences, so can be accessed by index using <array>[<index>] and accept all methods/operatins (like slicing)
  * Access an matrix element with `<array>[<line>][<column>]` or `<array>[<line>,<column>]`
  * If you pass an boolean array as parameter to the index, it will return only the posions marked with true
* **Slicing**
  * select a range of elements
  * <array>[a:b] select the element with index i so that $a\leq i < b$
  * you must specify the selection for all the axis
* **Comparisons**
  * ex: <array> > 5: return an boolean array, with the comparison element-by-element if they are bigger than 5
  * You can pass a comparison as a parameter to a selection to access just the element that satisfy the condition
* **Memory management**
  * numpy does a big *gotcha* here
  * When you select (ex: slice) an array, it does not return a *copy*, but a *view* (like a pointer)
  * modify the copy will modify the original array
  * numpy does that to avoid excessive memory usage
  * to copy an array, you must explicity use `<array>.copy()`
* **Universal functions**
  * Arithmatic and logical operation in the elements array
  * receive a fixed number of scalar inputs (array), and return a fixed number of scalar outputs (operated array)
  * https://docs.scipy.org/doc/numpy/reference/ufuncs.html
* **others**
  * memmap: Create a memory-map to an array stored in a *binary* file on disk.?

## Creation methods

* np.array(<list>)
  * return an array, from a list
* np.arange(<begin>, <end>, <stepSize>)`
  * create an array
  * Does not include the <end>
* np.zeros(<size>)
  * Create a array with zeros
  * in <size> is a tuple, will create an matrix
* np.ones(<size>)
* np.linspace(<begin>, <end>, <numberOfElements>)
  * create a array with variable number of elements
* np.eye(<size>)
  * Create an square eye matrix with <size> dimention
* np.random.<method>(<size>)
  * generate an array/matrix of random number, using the random function (the are a lot of buit in fuctions)
  * Functions:
  * rand (full random, from 0 to 1)
  * randn (normal distribution from -1 to 1, centered in 0)
  * randint

## Operation methods

* <array>.<reshape> (<nlines>, <ncolums>)
  * Transform an array into an matrix
  * The number of elements in the array must be compatible with the number of elements in the matrix

* <array>.shape

  * return size (how many dimensions the tensor has along each axis)

* <array>.ndim

  * return dimention

* <array>.dtype

  * return datatype

* <array>.max

  * return the max element

* <array>.mean

* <array>.argmax

  * return the index of the max element

## Export

* .npy

  * It is a standard binary file format for persisting a single arbitrary NumPy array on disk

  * https://numpy.org/devdocs/reference/generated/numpy.lib.format.html

  * `.save()` não funciona com BytesIO, só com tempfile ou com um arquivo em disco:

    ```
    # Arquivo em disco
    fileName = 'assets/last_image.npy'
    np.save(fileName, payload_camera)
    file = open(fileName, 'rb')
    content = file.read()
    file.close()
    ```

    ```
    # Tempfile
    file = TemporaryFile()
    np.save(file, payload_camera)
    file.seek(0)
    content = file.read()
    file.close()
    ```

* **Python list**

  * That’s an easy but inneficient way
  * you can do it to send over network as plain text, but it’s a looooot of overhead
  * `image.tolist()`



# Pandas

* To data analysis, cleaning and preparations

* Built on top of numpy

* Series: unidimensional dataframe (like a array, but with named index)

* [Cheat sheet](https://www.dataquest.io/blog/pandas-cheat-sheet/)

* **Dataframe**

  * Data type that works like a table
  * Series (the columns) that share the same index 
  * Immutable objects

* **Elements selection**

  * The reference to the index is done in quoutes if the index is a string, or without quotes if the index are integers
  * Even with string indexes you can access by its numeric pos
  * <series>[‘<index>’]: return the value associated to that index
  * <df>[‘<columnName>’]: return a serie
  * <df>[[‘<columnName1>’, … , ‘<columnNameN>’]]To select several rows (receive a list)
  * <df>.<columnName>: also work (not recomend because is confuse)
  * You can add new column dinamicly, just atribution values
  * <df>.loc[‘<indexName>’]
    * return a row (threated as a serie)
    * *loc* stands for *location*
    * Insted of a single index, you can pass a list of indexes (ex: `loc[['i1', 'i2']]`)
    * you can access a field using `.loc['<indexName>', <columnName>]`
  * <df>.iloc[<indexName>]
    * reference a string index by its numeric position
  * comparing a dataframe in a expression (ex: $df[C1]>0$) return a boolean serie/dataframe. Puttin that into another selection return just row you want (ex: $df[df[C1]>0]$)
  * Operators $and$ and $or$, when used to selection, must be used in the element-by-element sintax $\&$ and $|$ 
  * To composed operation in the selection, thing become a little tricky. Ex (select the columns Y and X from the subset of the dataframe where values of column W>0 and Y>): 
  ```python
  df[(df['W']>0) & (df['Y'] > 1)][['Y','X']]
  ```

* **operations**
  
  * ex: sum is made element-by-element in the matching indexes. 
  * if some index of one array have no match in the other, the result for that index will be null
  * Arithmetic operations return float
  * logical operations must be done with the sintax $\&$ and $|$
  * A cópia de um dataframe precisa ser feita de forma explícita (usando a funão copy), pois o draframe é underlying numpy

* **multilevel hierarquy**

  * select the high level index return a sub dataframe, so you can used common  element selection on the return 	
  * the levels can have names
  * sub level from different parrent levels are ==totally?== independent 
  * cross section:  allow select rows from different parent levels`<df>.xs(<index>, level='<indexName>')`
  
* **Package - pandasSQL**

  * interface similar to pandas, but allow queries in SQL
  * `pandassql(“<SQL statements>”, globals())`

## Basic methods

* parameter `inplace=true`: mutate the original object (accepted in most methods)
* parameter `axis=<0|1>`: if you want to drop a row (axis=0) or a column (axis=1)
* pd.Series(<valuesList>, <indexList>)
  * receive two lists (or numpy arrays)
  * If dont receive the indexes, they will be sequecially numered
* pd.DataFrame(<valuesList>, <indexList>, <columnNamesList>)
  * receive two lists (or numpy arrays)
  * can also receive one dictonary 
  * If dont receive the indexes, they will be sequecially numered
* <df>.drop(<name>, <axis>)
  * name: index or column name
* <df>.index()
  * return informations about the index
* <df>.reset_index()
* <df>.set_index(<column>)
* <df>.sort_values(<column>)
  * ordenate the df by that column (dont change the index) 
* ==pivot_table?==

## Data IO methods

* Some libraries used here (need to be installed with `conda install`): sqlalchemy, lxml, html5lib, BeautifulSoup4
* pd.read_csv(‘<FileName>’)
  * parameter header=<number>: line in witch is the header
  * parameter index_col=<number>: column of the index
* df.to_csv(‘<fileName>’)
  * parameter index=<bool> = export or not the index
* pd.read_excel(‘<FileName>’)
  * Dont import macros, formulas, styles, etc (can even cause error)
  * each tab will be a different dataframe (return a ==list?== of dataframes)
* pd.read_html(‘<link>’)
  * return just the tables 
  * for more than one table in the page, return a list of dataframes
* There are specific libraries to work with each database (you can even created a in memory sqlite database)

## Operation methods

* Quando o método atuar sobre uma série, entenda que pode ser a seleção de uma coluna (ex: df[‘c1’] retorna uma série)
* <df>.head()

  * print header and first rows
* <df>.info()

  * Number and type of rows in each column
* <df>.rename

## Stats

* <df>.describe()
* Statistical informations
* <series>.unique()
* <series>.nunique()
* number of uniques
* <series>.value_counts()
* how many times each value appear
* <df>.corr()

  * return a correlation table (df) between all variables

## Time manipulation

* When a time datatype is set to index, it can be easyly manipulated

* **Time datatypes** 

  * Timestamp: specific point in time
  * Timedelta:
  * Period: 

* **Frequency alias**

  * can be combined with numbers to specify other frequencies

    ![1554138595337](Images - Python/1554138595337.png)

* **Selection methods**

  * <df>['2014-07-04':'2015-07-04']: select interval 
  * <df>.['2015']: select whole year
  * .between_time('<start>', '<end>') 

* **Creation methods**

  * pd.Timestamp('2019-10-26')
  * pd.to_datetime(<series>)

    * receive a series (selection of a coumn) and transform it to Pandas timestamp
    * To receive from epoch timestamp: `pd.to_datetime(<epoch>, unit='s')   `
  * date_range('<begin>', periods=<p>, freq='<format>')

    * Create a df of time series
    * Period: number of rows
    * freq: time between row (ex: '1D20min')

* resample(‘<binsize>’).<agregFunctionc>()
  * Axis must be DatetimeIndex, TimedeltaIndex or PeriodIndex
  * bin size: W (per week), H (by houer), etc
  * Agregation function: sum, count, deviation, etc 
  
* forward or backfilling

  * ?
  
* Others

  * If index is time type, `df.plot()`

## Aggegating

* <df>.groupby(‘<columnName>’)
  * group records into “buckets” by categorical values
  * will return an “grouped object”, and on it yo can apply agregate methods
  * The grouping can be hierarquical
  * `.describe()`: return a lot a things
  * `.size()`: number of rows in each group
  * composed of counts, sums, or other aggregations is usually called *pivot table*
  * Geral idea: split, apply function, combine
* pd.concact(<listOfDf>)
  * glue several dataframes (they must be compatible)
  * can concatenate by rows or columsn 
  * https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.replace.htmljust put together, dont verify if they are compatible
* pd.merge (<df1>, <df2>, on=‘<key>’)
  * glu by the macthing values on the key column
  * parameter $how$: inner, outer, etc
* <leftDF>.join(<rightDF>)
  *  combine the columns by the index

## Transforming

* <series>.apply(<funcName>)
  * Execute the function to all cells
  * works with lambda functions
  * Used to Dataframes (row or colum)
* <df>.applymap(<funcName>)
  * element-wise on a DataFrame
* <series>.map
  * element-wise on a Series
* <df>.transform
  * ?
* 
* <pd>.get_dummies (<df>[<column>])
  * convert nominal variables to booleans
  * return one column for each class
* <df>.dropna()
  * drop the rows with   values
  * can drop a column, changing   parameter
  * the receive a threhold, with the minimum values of NaN to dro therow 
* <df>.fillna(value=<value>)
  * complete the NaNwith a value 
* <df>.isnull()
  * return boolean df
* 
* * 
* * 
* * 
* Manual normalization 
  * `normalized=(resampled-resampled.min())/(resampled.max()-resampled.min())`

## Hierarquical indexing

*  `.unstack()` method—use it to convert the results into a more readable format and store that as a new variable

## Visualization

* Based on plt
* https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html

* * 

# Scikit 

* mineração e análise de dados
* Código aberto, comercialmente utilizável
* Uses NumPy as a core component
* relies on duck typing (not inheritance), so the classes just need to have the same interface
* **API design**
  * <u>Estimators</u>
    * train on a dataset, then give a values
    * `.fit()`
  * <u>Transformers</u>
    * train on a dataset, then give another dataset
    * `.fit_transform()`
  * <u>Predictors</u>
    * estimators that can make predictions given a dataset
    * <estimator>.fit()
    * <estimator>.predict(<newXvalues>)
    * `<estimator>.score()`: simply analise the goodness of fit

## Hyperparameter tunning

* HalvingGridSearchCV; starts on a few samples with a large set of parameters. In the next iteration, the number of samples increase (by a factor n), but the number of parameters is halved/reduced
* HalvingRandomSearchCV

## Transformers

* SelectFromModel: feature selction by importance?
* Sequential Feature Selector (SFS): (+) good to high dimention; does not required to have the `feature_importances_` or `coef_` attribute
* One-Hot Encoder
* OrdinalEncoder

## learnRequest library

* to APIs

* Dictionary_data = json.loads (json_data)

## ML Algoritms

* ==semi supervised algorithms?== [tutorials](https://scikit-learn.org/stable/auto_examples/semi_supervised/plot_semi_supervised_versus_svm_iris.html#sphx-glr-auto-examples-semi-supervised-plot-semi-supervised-versus-svm-iris-py)
* Dummy
  * `from sklearn.dummy import DummyClassifier`
  * predicts the majority/m?
* LinearRegression
  * .fit recebe dois dataframes, e não vetores (ver [stackoverflow](https://datascience.stackexchange.com/questions/25962/error-expected-2d-array-got-1d-array-instead))
* Suport vector machine
  * `from sklearn.svm import SVC` : classifier
  * .fit (<dfX>, <dfY>)
  * Parameters fit
    * C: cost of missclassification. Large C = low bias and high variance
    * gamma: variance of the gaussian fit. Large gamma = high bias and low variance
* GridSearchCV
  * creating a 'grid' of parameters and just trying out all the possible combinations
  * `.best_params_`
* KNN
  *  KNeighborsClassifier (<nNeighbors>)
     *  return an knn object
* K means
  *  `from sklearn.cluster import KMeans`
  *  KMeans(n_clusters=<k>)
  *  <predictor>.inertia_: Sum of Squared Error

## Evaluation

* plot_confusion_matrix(estimator, X, y, colorbar=False)
* **Metrics**
  * MAPE: is implemented since 0.24

## Pipelines

* ==how is the dataset split?==

* Pipeline class; receive a list of name/estimator pairs; All but the last estimator must be transformers 

  ```python
  from sklearn.pipeline import Pipeline
  from sklearn.preprocessing import StandardScaler
  num_pipeline = Pipeline([
      ('imputer', SimpleImputer(strategy="median")),
      ('attribs_adder', CombinedAttributesAdder()),
      ('std_scaler', StandardScaler()),
  ])
  housing_num_tr = num_pipeline.fit_transform(housing_num)
  
  ```

* you can quite easelly do HPO with that, for instance:

  ```python
  from sklearn.model_selection import GridSearchCV
  from sklearn.linear_model import LogisticRegression
  from sklearn.pipeline import Pipeline
  
  clf = Pipeline([
      ("kpca", KernelPCA(n_components=2)),
      ("log_reg", LogisticRegression())
  ])
  param_grid = [{
      "kpca__gamma": np.linspace(0.03, 0.05, 10),
      "kpca__kernel": ["rbf", "sigmoid"]
  }]
  grid_search = GridSearchCV(clf, param_grid, cv=3)
  grid_search.fit(X, y)
  
  ```

  

* ColumnTransformer

  * will apply some transformers to just some columns
  * Instead of a transformer, you can specify the string "drop" if you
    want the columns to be dropped. Or you can specify "pass
    through" if you want the columns to be left untouched

## Other methods

* Train split
  * `from sklearn.model_selection import train_test_split  `
  * train_test_split (<x==vector??==,…)
  * parameter random state?
  * return two lists (to y) and two matrix (x)
* Normalization
  * `from sklearn.preprocessing import StandardScaler`
  * StandardScaler(): return an scaler object
  * you can remove a column (ex:the class) from the normalization 

# Scipy

* Scientific programming tools

* Scipy.stats_ttest_ind (<list1>, <list2>, <equal_var>)
  * Declared in scipy.stats library 
  * Equal var	→ ask if the variance value are the same (false, in welchs test)
  * Return (T, p)
* Scipy.stats.shapiro (<data>)
  * Implement Shapiro-wild test
  * Return <W,P>
  * W → test result 





# Visualization

## Matplotlib

* `import matplotlib.pyplot as plt
  %matplotlib inline`
* Biblioteca de visuaização de dados
* sintaxe baseada no matlab
* **Approaches**

  * There are basicly two ways of work with MatPlotLib: object orientated or function orientated
  * Orientated version: we use method of graph instances (==or *axes*?==)
  * Functional: always work with one “instance”, and switch with graph the actual instance represents
  * Generally, we’ll use here the OO aproach
* default instance: plot
* <fig>, <instances> = plt.subplots (<rows>, <columns>)

  * Create a grid of instances
* **Instance methods**

  * Applied as methods of the instance (`instance.method()`)
  * The methods to functional aproach are almost equal: just don’t have the “set_” 
  * .set_title (‘<string>’)
  * .set_xlabel (‘<string>’)
  * .set_ylabel (‘<string>’)
  * .set_yscale (‘<string>’)
    * log
  * .legend( ? )
  * .set_xlim([<min>, <max>])
  * .set_ylim([<min>, <max>])
  * .grid(<boolean>)
  * ax2 = ax1.twinx()
    * To dual y
    * Create a new Axes instance with an invisible x-axis and an independent y-axis
* **Formaters**

  * Passed as parameters to the plotting methods
  * figsize=(<horzontal>, <vertical>)
  * label=‘<string>’
  * color=‘<name | rgb | hex>’
  * linewidth=<number>
  * alpha=<number>
  * linestyle=‘<string>’
    * -
    * –
    * steps
    * :
  * marker=‘<string>’
    * o
    * +
    * **
  * markersize=<number>
* **Exporting**

  * <fig>.savefig (‘<name>’)
* **Plot types**
  * applied as methods of the instance (`instance.method()`)
  * <instance>.plot(<x>, <y>)
    * Basic line visualization
    * Receive two arrays and plot an scatter
  * scatter(x,y)
  * hist(data)
  * boxplot(data,vert=True,patch_artist=True)

## Seaborn

* Open source
* Beatefull plots
* Recebe um framework, e voce separa os eixos passando o nome das colunas de interesse
* Build on top of matplotlib, so there are similarities (specially in styling)
* sns.<type>
* two plot in the same jupyter cell: superposition
* **Geral options**
  * pallete=‘<string>’
    * fast style
    * From matplotlib colormap
    * coolwarm
    * seismic
  * hue = <colName>
    * separe the categories
  * ==standad_scale?==
* **style**
  * passed as parameters to the function
  * linecolor = ‘<string>’
  * aspect=<inteter> (acpect ratio)
  * size=<integer>
* **Others**
  * sns.set_styles(‘<string>’)
  * some global styling

### Types

* **Countinous data**
  * distplot (x=<series>)
    * Receive a series and compute the distribution
    * kde = <boolean>
    * bins = <integer> (number of bins)
  * jointplot (x=‘<colName>’, y=‘<colName>’, data=<dataframe>)
    * Scatter with two distributions
    * kind = ‘<string>’
      * typo of gaph that stay in the middle ()
      * ‘hex’
      * ‘reg’ (regression)
      * ‘kde’ (density of match between the values)
* **Categorial data**
  * barplot (x=‘<colName>’, y=‘<colName>’, data=<dataframe>)
    * estimator = ‘<string>’: what to calculate (agregation function) inside each mean (np.mean, np.std, etc)
  * countplot (x=‘<colName>’, data=<dataframe>)
  * boxplot (x=‘<colName>’, y=‘<colName>’, data=<dataframe>)
  * violinplot (x=‘<colName>’, y=‘<colName>’, data=<dataframe>)
    * boxplot with distribution
    * if you use hue, you can configure `split=true` to show distribution side-by-side
  * stripplot
    * x is categorical
    * y is continous
    * `jitter=true` adds randon noise (desalinha os pontos)
* **To matrix**
  * heatmap (<dataframe>)
    * annot=<boolean> (show the values in cell)
  * clustermap (<dataframe>)
    * uses hierarchal clustering to produce a clustered version of the heatmap
* **Grid plots**
  * pairplot(<dataframe>)
    * everyone vs everyone (of the numerical columns)
    * Simplifier version of plairGrid
  * PairGrid(<dataframe>)
    * .map_diag()
    * .map_upper
    * .map_lower
  * FacetGrid (<dataframe>, col=‘<colName>’, row=‘<colName>’)
    * Generates a grid with every combination of row and col (discrete varibles)
    * .map(<type>, <colName>)
* **Others**
  * lmplot (x=‘<colName>’, y=‘<colName>’, data=<dataframe>)
    * do a linear regression
    * hue=<colName> (to multiple regressions)
    * col=<colName> (like hue, but in separate graphs)

## ggplot

* aes(xvar, yvar)
  * create the plot, the canvas

## Plotly

## Streamlit

* build interactive visual dashboard
* easy to share and show results
* good for very quick prototyping
* have some options for deploy (like to heroku)
* (+) live auto-reloading
* Visualization compatibility: plt, sns, plotly, Altair, etc
* [Deploy tutorial with docker and Traefik](https://www.kdnuggets.com/2020/10/deploying-secure-scalable-streamlit-apps-aws-docker-swarm-traefik-keycloak.html)

## Dash

* Visualization
* by Plotly built on top of Javascript, React, Flask
* older than streamlit
* deploy ready
* Visualization compatibility: mainly plotly (but not only, see [here](https://github.com/plotly/dash-alternative-viz-demo))
* The code is very html-driven
* styles applied with css (they provide several themes)
* dash_bootstrap_components: ?

## Others

* Altair
* Bokeh

# Others

* Excelent compilation: https://amitness.com/toolbox/
* Compilation (not much organized): https://www.reddit.com/r/MachineLearning/comments/hoqnrm/d_machine_learning_toolbox/fxk63t3?utm_source=share&utm_medium=web2x



Multiclass balancing: https://pypi.org/project/multi-imbalance/

## Pandas profiling

* https://pandas-profiling.github.io/pandas-profiling/docs/master/rtd/
  * `df.profile_report()`

## Ruffus

* To organize pipelines
* open-source
* 





































