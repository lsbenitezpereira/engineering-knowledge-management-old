# Conceitos probabilísticos

- Probability = deal with uncertanty
- resultado: output resultado de uma única tentativa
- relative risk: ratio of two probabilities.
- **Espaço amostral (E)**
  - Or *sample space ($\Omega$)* 
  - Conjunto de todos os resultados possíveis de um experimento
    aleatório
  - Ex: espaço amostral do lançamento de um dado cúbico: 1, 2, 3, 4,
    5 e 6.
- **Evento**

  -   Subconjunto do espaço amostral
  -   evento simples: subconjunto com apenas 1 resultado
  -   Ex: resultados pares de um dado: 2, 4 e 6.
- **Tipos de evento**

  * Evento certo

    * Probabilidade 1
    * n(a)=n(E)
  * Evento impossível

    * Probabilidade 0
    * n(a) = zero
  * Complemento do evento

    * Chance do evento não acontecer
    * $p(a)+p(a')=1$
  * Eventos independentes

    * a ocorrencia de um nao afeta a acorrencia do outro 
    * ou seja, se $P(B|A)=P(B)$
    * Ou ainda P ( A ∩ B ) = P(A)P(B), que é uma manipulação da equação acima
    * Independence does not mean disjoint (withou intersection)
  * Mutuamente exclusivos
    * Se acontece um, não pode acontecer o outro
    * A ∩ B = ∅
- **Descrição gráfica de eventos**

  - Just the graphic diagram is not enough to fully describe the behavior

![Resultado de imagem para probabilidade diagrama de
  venn](2.0 - Probabilidade - Images/image1.jpeg){width="5.688888888888889in"
  height="3.0in"}

* **Tipos de experimento**

  * Experimento determinístico
    * Mesmas condições, mesmos resultados
    * Ex: corrente em um resistor (se é aplicada a mesma tensão, vai passar a mesma corrente)
  * Experimento aleatório
    * Mesmas condições, resultados diferentes
    * Ex: lançamento de um dado

## **Ramos da probabilidade**

-   **Probabilidade clássica**
    -   ou *teórica*
    -   Relação percentual entre o nº de elementos do evento analisado e o
        espaço amostral

$$
P \left (E \right ) = \frac{n º \left (E \right )} {n º \left (A \right )}
$$
> E = eveto
> A = espaço amostral
- **Probabilidade Frequentista**
  - Ou *Probabilidade empírica*, *probabilidade estatística* ou *aletatória*
  - formada a partir de observações passadas de um experimento
  - A probabilidade empírica será a frequencia relativa do evento (frequencia do evento / frequencia total)
  - only applies to a series of identical trials
  - If there is no set of identical trials, there is no how to calculate probability
  - A menos quando explicitado o contrário, trataremos aqui de estatística frequentista
  - Lei dos grandes numeros: a probabilidade empírica se aproxima da clássica com o aumento do número de repitições no experimento 
- **Probabilidade bayesiana**
   - ou *probabilidade epistemológica*
   - defines probability as a degree of belief that an event will occur (it’s about modealing the future, and not just observing the past)
   - depends on a person’s state of knowledge (and that is fucking subjective)

## Calculo de probabilidades

- **Probabilidade condicional**

  * probabilidade de um evento A sabendo que ocorreu um outro evento B

  * After the condicional, the sample space will change and probabilities will be calculated relative to the new scenario

  * As propoções entre os eventos se mantem

  * A condicional B (Manipulating the product of probabilities):
    $$
    P(A\mid B)={\frac {P(B \cap A)}{P(B)}}
    $$

- **Produto de probabilidades**

  -   Probabilidade dois eventos ocorrerem simultaneamente
  -   Princípio “e” 
  -   Representado por $\cap$ (intersecção)
  -   $P \left (A ∩ B \right ) = P \left (A \right ) ∗ P \left (B|A \right )$

- **Soma de probabilidades**

  - Probabilidade de dois eventos ocorrerem um-ou-outro

  - Princípio “ou”

  - Representado por U (união)

  - Calculado pela soma das probabilidades individuais menos a probabilidade destas ocorrerem simultaneamente

  - Se os eventos forem mutuamente exclusivo (conjuntos disjuntivos), esse ultimo termo será zero 
    $$
    P \left (A ∪ B \right ) = P \left (A \right ) + P \left (B \right ) − P \left (A ∩ B \right )
    $$
  
- **Teorema da probabilidade total**

  - Soma todas as parcelas de acontecimento de um evento F

    ![1557679027220](Images - 2.0 - Probabilidade/1557679027220.png)

## Others

* Tabela de Conjunção de probabilidades: representa a probabilidade de cada configuração
  
* MAP: take the ost probable as certain (P=1). Computationally faster

* Chain rule
  
* or *general product rule*
  
* permits the calculation of any member of the joint distribution of a set of random variables using only conditional probabilities
  
  * sequence of probabilities is a roduct of conditionals
    $$
    {\displaystyle \mathrm {P} (A_{n}\cap \ldots \cap A_{1})=\prod _{k=1}^{n}\mathrm {P} \left(A_{k}\,{\Bigg |}\,\bigcap _{j=1}^{k-1}A_{j}\right)}
    $$


# Distribuição probabilística

* Essa seção complementa a seção sobre Distribuição Estatística, mas aqui teremos analisaremos as características teóricas do fenômeno que gerará o conjunto de dados

* Conditional distribution: distribution of a subset of the data which is selected according to a condition

* **Variável aleatoria**

  * representa um valor numerico associado a cada resultado de um experimento probabilistico

  * Asign a number to each outcome

  * A function that maps from the sample space to a real number

  * Can be described by an PMF

  * Random variables are usually written with a capital letter

  * Undertanding the name

    * Random because can select a random value from a distribution

    * Variable because will be manipulated as an independent variable

    * Tip: dont really think as a variables o as random, but as a funtion


## Describring distributions

* modelamos a probabilidade por uma função, que pode ser discreta ou contínua

* Essa função pode representar um valora acumulado de probabilidade (do começo até alí) ou instantâneo

  ![1554661035188](Images - 1.0 - Estatística/1554661035188.png)

* **Probability mass function (PMF)**

  * Discrete, non cumulative

  * Análogo ao histograma, para distribuições estatísticas

  * How likely is to obtain the outcome x given the random variable X

  * $PMF(x)=Prob(X=x)$

  * Function that maps from values to probabilities (float point)

  * We can calculate statistics by the PMF ($x_i$ are the input objects and $p_i=PMF(x_i)$)
    $$
    \bar{x}=\int x_i p_i dx
    $$

    $$
    \sigma^2={\int p_i(x_i - \bar{x})^2}
    $$

* **Cumulate Mass Function (CMF)**

  * Discrete, culumative
  * Análogo à ogiva, para distribuições estatísticas
  * Integral of the PMF

* **Probability density function (PDF)**

  * Continous, non cumulative
  * derivative of a CDF
  * The result is not a probability; it is a probability density
  * Integrate in some range will result the probability of have an outcome in that range
  * A integral converge: $\lim_{x \rightarrow \infty} PDF(x)=0$
  * A probabilidade total é 1: $\lim_{x \rightarrow \infty} \int_{-\infty}^{x} PDF(x)=1$
  
* **Cumulative distribution function (CDF)**

  * Continuous, cumulative
  * function that maps values to their percentile rank in a distribution
  * Integral of the PDF
  * The CDF of the random variable $X$ in the point $x$ is $CDF(x)$, or $F(x)$, and the result is a probability
  
  * <u>Properties</u>
    * $0 \leq CDF(x) \leq 1$
    * $\lim_{x \rightarrow 0} CDF(x) = 0$
    * $\lim_{x \rightarrow \infty} CDF(x) = 1$
    * The CDF is monotonicly crescent
  
  

## Distribuições discretas

* uma distribuição de probabilidade discreta é uma lista de probabilidades de cada valor da variável aleatória
* a probabilidade de cada valor da variável está entre zero e 1 ($0<=P(X)<=1$)
* a soma de todas as probabilidade dos valores da variável aleatória é 1 ($\sum P(x)=1$)
* **Valor esperado**
  * ou *esperança*, ou *média*
  * Faz uma média ponderada das saídas, pesando pela probabilidade
  * $\mu=\sum xP(X=x)$
  * Properties
    * $E(c) = c$
    * $E(X + c) = E(X) + c$ (linearidade da constante)
    * $\mu(aX+b)=X\mu(X)+b$ (bi linearidade)
* **Variância**
  * V, ou $\sigma^2$
  * $\sigma^2=\sum (x_i-\mu)^2P(X=x_i)$
  * mede a incerteza,  aleatoriedade (pois está relacionada ao espalhamento)
  * Desvio Padrão = DP (X) = $\sqrt{V(X)}$
  * Properties
    * $V(c) = 0$
    * $V(X + c) = V(X)$ (cons)
    * $V(aX+b)=a^2V(X)$ (scale enlarge the variance, but shift don’t)
    * $DP(cX) = |c|DP(X)$
    * $\sigma^2(X)=\mu(X^2)-\mu(X)^2$

### Distribuição binomial

* Usado para Confidence Interval in problemas de classificação binários (duas classes)

* um experimento de probabilidade é chamado de binomial se

  * Existe um numero finito de tentativas
  * há apenas dois resultados possíveis para cada tentativa (sucesso ou fracasso)
  * cada tentativa é independente das outras (mesma probabilidade de sucesso em cada tentativa)
  * a variável aleatória contabiliza o numero de tentativas com sucesso

* n = numero de tentativas

* p = P(s) = probabilidade de sucesso

* q = P(f) = 1-p = possibilidade de fracasso

* x = tentativas que resultaram em sucesso

* probabilidade de $x$ sucessos em $n$ tentativas será:
  $$
  P(x)=\frac{n!}{x!(n-x)!}p^xq^{n-x}
  $$

* Média: $np$

* Variância: $np(1-p)$

* **Dedução instintiva**

  * é a combinação (permutação sem ordenação) de uma sequência de interesse específica
  * Ou seja, um numero fixo de sucessos.
  * A probabilidade será as possibilidades de combinação multiplicada pela possibilidade dessa sequencia.

* **De onde aparece esse binômio de newton**

  * o bionomio de newton transforma um produto em uma soma

  * Ambos serão iguais, mas um formato pode ser mais util que o outro

  * Ao fazer a análise probabilistica de uma sequência de eventos independentes, a possibildade de uma sequencia será o produto das probabilidades parciais:
    $$
    (q+p)^n
    $$

  * sendo n o numero de eventos sequenciais, q a possibilidade de sucesso e P de fracasso

  * só que enxergar isso como um produto não é muito util, só temos informaçẽos sobre o "ponto final' e não sobre os pontos intermediários

  * além disso essa fórmula algébrica sempre soma 1, pela definição de probabilidade, então realmetne não agrega nada samer o resultado final

  * queremos transformar (mantendo a igualdade) isso em um somatorio, que traz informações sobre os estados parciais e, assim, nos permite analisar o evento de forma muito mais completa

  * Ou seja, queremos uma equação que me descreva a probabilidade na forma:
    $$
    (q+p)\bigg |_\text{parcial 1}+(q+p)\bigg|_\text{parcial 2}+...+(q+p)\bigg|_\text{parcial n}
    $$

### Distribuição geométrica

* Probabilidade de termos que repetir a tentativa até conseguir sucesso

* x = numero de tentativas até obter o sucesso 

* p = probabilidade de sucesso em cada tentativa (as tentativas são independentes)

* A probabilidade de que o primeiro sucesso ocorra na tentativa x é:
  $$
  P(x)=p(1-p)^{x-1}
  $$

### Distribuição hipergeométrica

* Semelhante à distribuição binomial, porém ao invés de contabilizar o numero de tentativas com sucesso, considera o numero de “seleções” com sucesso

* Ou seja, amostragem sem reposição

* Probabilidade de ==$x$ sucessos em $n$ remoções, de um universo N?==:

  ![1557680255096](Images - 2.0 - Probabilidade/1557680255096.png)

* Média: $np$

* Variância: $np(1-p)\frac{N-n}{N-1}$

### Distribuição de Poisson
* Probabilidade de algo acontecer um numero x de vezes dentro de um intervalo (de tempo, de espaço, etc)

* o número de observações de uma variável em um intervalo contínuo (tempo ou espaço)

* inter-arrival durations are exponentially distributed

* $\mu$ = média do numeros de ocorência nesse intervalo (constante em todo o intervalo de observação)
  $$
  P(x)=\frac{\mu^x e^{-\mu}}{x!}
  $$
  
* **Dedução**

  * a partir a distribuição binomial, quando o numero de tentaivas vai para infinito
  * $\mu=np$
  * Consideraremos a média constante (a probabilidade diminui quando o numero de tentativas aumenta)
## Continuous distribuitions

* characterized by CDF

* real world phenomena can be approximated by continuous distributions

* **Valor esperado**

  * Dada por (caso a integral convirja):
    $$
    E(x)=\int_{-\infty}^{+\infty}xPDF(x)dx
    $$

* **Variancia**

  * $\sigma^2=E({[x-E(x)]}^2)$
  * $\sigma^2=E(x^2) - E(x)^2$
### Distribuição uniforme

* The PDF will be given by:

  ![1558051399879](Images - 2.0 - Probabilidade/1558051399879.png)
  $$
  k=\frac{1}{b-a}
  $$

* E(x)=$\frac{b+a}{2}$

* V(x) = $\frac{(b-a)^2}{12}$

### **Exponential distributions**

* come up when we look at a series of events and measure the times between events, which are called interarrival times. If the events are equally likely to occur at any time, the distri-
  bution of interarrival times tends to look like an exponential distribution

* Described by:
  $$
  CDF( x) = 1 − e^{−\lambda x}
  $$
  ![1554659446879](Images - 1.0 - Estatística/1554659446879.png)

  ![1552610001844](1.0 - Estatística - Images/1552610001844.png)

* $\lambda$: determines the shape 

  If we plot $1 − CDF(x)$ on a log-y scale the result is a straight line withslope − λ

* Mean: $1/\lambda$

* median: $ln(2)/λ$


### **Pareto distribution**

* Commom in social sciences

* described by:

  ![1552687102618](1.0 - Estatística - Images/1552687102618.png)

  ![1552687198406](1.0 - Estatística - Images/1552687198406.png)

* $x_m$: determine the location (minumum possible value)

* $\alpha$:  shape of the distribution

* Median: $x_m 2^{1/\alpha}$

* log-log scale, looks like a straight line (slope −α and intercept α log x_m)

### Gauss distribution

* or *normal distribution*

* média e mediana 

* É simétrica, em forma de sino 

* Possui dois pontos de inflexão, em $x=\mu \pm \sigma$

* Descrita por:
  $$
  PDF(x)=\frac{1}{\sigma \sqrt{2\pi}}e^{-(x-\mu)^2/2\sigma^2}
  $$
  
* Uma gaussiana fica completamente determinada pela média e desvio padrão

* Versão resumida (centrada em zero, variância 1 e não normalizada): $e^{-x^2}$

* **Cálculo de probabilidade**

  * A PDF(x) fornece uma densidade de probabilidade

  * Para calcular a probabilidade de um evento acontecer dentro de um range, devemos integrar a PDF(x) dentro desses limites

  * Integrar esse função é foda, mas podemos fazer por série de mclauring (existe também uma substituição marota, mas não lembro):
    ![img](1.0 - Estatística - Images/image1.png)
    
  * De forma geral, utilizaremos valores tabelados

  * Definiremos uma curva gaussiana padrão, com $\mu=0$ e $\sigma=1$

  * Para transformar uma distribuição qualquer na curva padrão, utilizaremos a seguinte mudança de variável (para z):
    $$
    z=\frac{x-\mu}{\sigma}
    $$
    Percebe que a nova variável é o z-score

  ![1551402074478](2.0 - Probabilidade - Images/1551402074478.png)

![Related image](2.0 - Probabilidade - Images/1_NRlqiZGQdsIyAu0KzP7LaQ.png)

*  **Operations**
  * Closed under linear transformation and convolution
  * Sum and multiply a gaussian by a real number will return a gaussian
  * If $X_1∼N(\mu, \sigma^2) $ is a random variables distributed as a gaussian with some mean and variance, so $X_2=aX_1+b$ will be $X_2∼N (aμ + b, a^2 σ^2 )$
  * Sum two gaussians return a gaussian
  * If $Z = X + Y$, so $Z ∼ N (μ_X + μ_Y , σ_X^2 + σ_Y^2 )$
  
*  **Multivariate gaussian**

  *  If we consider the variables as independent, then we can just calculate gaussian several times

  *  Probailbity of seen a sample: $\prod_{j=1}^n p(xi; \mu_i, \sigma_i)$ (produt probability of seen each variable given its mean and std, ignoring covariance)

  *  if we consider dependencies, then we have to know the covariance matrix (see details in *Statistics*)

    ![image-20200430183620814](Images - 2.0 - Probabilidade/image-20200430183620814.png)

### Triangular

* Mínimo, moda, máximo
* has been called a "lack of knowledge" distribution, hahah

## Other annotations

* **Sum of distribuitions**

  * the distribution of the sum is the convolution of the distributions

  * If $Z = X+Y$ (sum of aleatory variables), then
    $$
    PDF_Z=PDF_Y * PDF_X
    $$
    ![1554659643824](Images - 1.0 - Estatística/1554659643824.png)

* **Media amostral**

  * Dada uma população de média $\mu$ e desvio padrão $\sigma$, podemos escolher infinitas amostras aleatórias com reposição
  * cada amostra terá uma média
  * a media da distribuição das médias de cada amostra será igual à da população, $\mu_{\bar{x}}=\mu$
  * O desvio padrão da distribuição de médias será $\sigma_{\bar{x}}=\frac{\sigma}{\sqrt n}$, onde n é o numero de elementos da amostra
  * Se $T_0$ for a soma de todos os valores de uma amostra, $\mu_{T_0}=n\mu$ e $\sigma_{T_0}=\sigma\sqrt n$
  * Se os valores da população estiverem normalmente distribuidos, então a distribuição das médias das amostras (cada uma com n elementos) será uma curva normal, dada por $\mu_{\bar{x}}$ e $\sigma_{\bar{x}}$
  * A distribuição de $T_0$ também será normal 

* **Central limit theorem**

  *  if we add up a large number of values from almost any distribution, the distribution of the sum converges to normal
  *  We mut consider that the values have to be drawn independently
  *  The distribuitions must have finite mean and variance

### Power distribution

* “scientists typically assume that most natural
  phenomena are distributed according to the bell curve or normal distribution.
  However, power laws are being discovered in such a great number and variety
  of phenomena that some scientists are calling them “more normal than ‘nor-
  mal.’ ”  Mitchell
* Practical examples
  * given a large text, the frequency of a word is approximately proportional to the inverse of its rank (i.e., 1/rank).
  * body mass vs metabolic rate: po

# Inferência estatística

* A partir de uma amostra, estimar um parâmetro da população
* Amostra tendenciosa: subestima ou superestima o parâmetro populacional
* Essa extrapolação será válida com uma certa margem de erro
* Estimativa pontual: prever um único valor (sem margem)
* Estimativa intervalar: prevemos uma amplitude de valores

## Intervalo de confiança

* Ou *confidence interval*

* way of quantifying the uncertainty of an estimate

* provides bounds on a population parameter, such as a mean, standard deviation, or similar.

* Iremos supor primeiro uma distribuição normal

* Veja no final dessa seção sobre outras distribuições

* **Nível de confiança (c)**

  * or *confidence level*

  * Propabilidade do parâmetro populacional estar dentro do nosso intervalo estimado

  * Geralmente éna ordem de 90%, 95% (most common one), 99.7%

  * Valores criíticos: extremos na curva normal padrão
  
    ![1560456921455](Images - 2.0 - Probabilidade/1560456921455.png)
  
  * Valores usuais de z: 
  
    ![1560458080954](Images - 2.0 - Probabilidade/1560458080954.png)


* **Margem de erro (E)**

  * o desvio padrão da amostra ($s$) pode ser usado no lugar no populacional ($\sigma$)

    ![1560457054016](Images - 2.0 - Probabilidade/1560457054016.png)
  
  * intervalo de confiança: $\bar x \pm E$

* **Estimativa da média**

  * Procedimento: 

  ![1560458037405](Images - 2.0 - Probabilidade/1560458037405.png)


  * Definindo o tamanho da amostra

    * Geralmente queremos um certo erro e escolhemos o tamanho da amostra a partir daí

    * Nesse caso:

  ![1560458222072](Images - 2.0 - Probabilidade/1560458222072.png)

  > $\sigma \approx s$, se n>30

* **Distribuição t**

  * ou *t de student*

  * Usada quando n<30

  * graus de liberdade = numero de amostras

  * Dada uma variável aleatória x aproximadamente normal, teremos:

    ![1560458565141](Images - 2.0 - Probabilidade/1560458565141.png)

  * A margem de erro será dada por:

    ![1560458700063](Images - 2.0 - Probabilidade/1560458700063.png)

  * Se formos usar $s$ ao invés de $\sigma$, recomenda-se seguir a tabela com $n-1$ graus de liberdade

  * Procedimento: 

    ![1560458586230](Images - 2.0 - Probabilidade/1560458586230.png)

* **Intervalo de confiaça para proporções populacionais** 

  * A proporção de sucessos é definida como:

    $p=x/n$

    > x = número de sucessos
    >
    > n = numero de tentativas
    
  * Margem de erro:

    ![1560459252486](Images - 2.0 - Probabilidade/1560459252486.png)

  * Se np>5 e nq>5, podemos aproximar por uma distribução normal e, então:

    ![1560459283000](Images - 2.0 - Probabilidade/1560459283000.png)

    ![1560459288984](Images - 2.0 - Probabilidade/1560459288984.png)

  * Procedimento: 

    ![1560459330500](Images - 2.0 - Probabilidade/1560459330500.png)

  * Nuemro de amostras:

    ![1560459361642](Images - 2.0 - Probabilidade/1560459361642.png)

  * Se voce não tiver uma estimativa preliminar para p, use $p=q=0.5$

* **Distribuição qui^2**

    * para estimar desvio padrão e variancia

    * Dada uma variável aleatória x normal, a distrbuição qui^2 será: 

    ![1560459486012](Images - 2.0 - Probabilidade/1560459486012.png)

    * use n-1 graus de liberdade
    * área do  qui² = confiança
    * Precisamos de uma fronteira a direita e outra a equeda
    * alpha = erro = 1 - c
    * dado o alpha a esquerda e á direita, calculamos os intervalos

* Intervalo da Variância  ($\sigma^2$):


  ![1560459868751](Images - 2.0 - Probabilidade/1560459868751.png)

  * Intervalo do Desvio padrão ($\sigma$):

  ![1560459879993](Images - 2.0 - Probabilidade/1560459879993.png)

  ![1560459899978](Images - 2.0 - Probabilidade/1560459899978.png)

* **Intervalo de confiança em distribuições não-normais**
  * Podemos refazer a dedução para uma outra distribuição específica (ex: bernoulli)
  * Podemos utilizar um método estatístico mais robusto (ex: bootstrapping)
  * Podemos utilizar métodos não paramétrico (ex: wilcox)
  * https://stats.stackexchange.com/questions/112829/how-do-i-calculate-confidence-intervals-for-a-non-normal-distribution
  * https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=2ahUKEwjz4_-qi5DoAhXtIbkGHdFbCR4QFjABegQIBhAB&url=https%3A%2F%2Focw.mit.edu%2Fcourses%2Fmathematics%2F18-05-introduction-to-probability-and-statistics-spring-2014%2Freadings%2FMIT18_05S14_Reading23b.pdf&usg=AOvVaw2tjdbzEjL4ZpvwLaRVchEn
* **To binary classification**
  * To less than 30 examples, we should take $z$ using binomial distribution. To more than 30 we can aproximate by gaussian (we’ll consider this case here)
    ```python
    interval = 1.96 * sqrt( (accuracy * (1 - accuracy)) / n)
    ```
    > n = sample size
    > 1.96 came from 95% of the gaussian (confidence)
  * The same equation can be used for error instead of accuracy. ==For other metrics too?==

## Hypothesis testing

* Estudaremos aqui principalmente afirmações sobre a média de uma distribuição gaussiana, mas o racicionio é facimente expansível 

* Used when we see apparent effects ( like a difference between two groups), and want to evaluate whether the effect is real, or whether it might have happened by chance.

* “Statistical significance was never meant to imply scientific importance”

* the larger the sample size, the higher the chance to detect a difference

* [Statistical Hypothesis Tests in Python (Cheat Sheet)](https://machinelearningmastery.com/statistical-hypothesis-tests-in-python-cheat-sheet/)

  ![1563416243657](Images - 2.0 - Probabilidade/1563416243657.png)

* **Definição**

  * hipótese estatística: alegação sobre o valor de um parâmetro (ex: a média de idade de uma população é maior do que 30)
  * Uma hipótese nula $h_0$ é uma hipotese estatística que contém uma afirmação com igualdade ($\leq, = ou \geq$). É  que inicialemtne consideramos verdadeira. 
  * Generally is a statement that we are trying to reject
  * uma hipótese alternativa $h_a$ é o complemento da hipótese nula; geralmente essa é a nossa hipótese de pesquisa, aquilo que queremos afirmar

* **Erros**

  * tipo 1: hipotese nula rejeitada, apesar de verdadeira (false positive)
  * tipo 2: hipotese nula aceita, apesar de falsa (false negative)
  * Geralmente, os testes minimizam os erros do tipo 1

* **Nível de significância ($\alpha$)**

  * em um teste de hipótese, o nível de significancia é a probabilidade máxima admissível para um erro do tipo 1 
  * Valor usual: 0.05 (5%)

* **P value**

  * ou *valor de probabilidade*, or *significance level*
  * se a hipótese nula for verdadeira, um valor P de um teste de hipótese é a probabilidade de se obter um valor tão extremo ou mais extremo do que aquela determinada a partir da amostra
  * P é calculado a partir de dados da amostra
  * 
  * It can be intepreted as a measure of how likely the data sample would be observed if the null hypothesis were true, that is, $Pr(data | hypothesis)$
  * <u>confidence level</u>
    * $c$ ou $a$
    * confidence level = 1 - significance level
    * $c=1-p$
    * probabilidade de cometermos um erro do tipo 1
  * <u>Regra de decisão</u>
    * Se $p \leq \alpha$, rejeite $h_0$
    * Se $p > \alpha$, falheaos em rejeitar $h_0$ (nada podemos afirmar)
    * Geralmente a Hipotese Nula é formulada de forma a indicar nenhum efeito significativo. Portanto, se p<0.05 nosso resultado é significativo

* **Types of test**

  * Parametric: assume that the data follows some known distribution  (like gaussian)
  * Non parametric: does not assume any distribuition
  * Testes não-parâmetricos geralmente são mais dificeis de conseguirmos provar a significância
  * Quando utilizar testes não parametricos
    * Os dados sob análise têm um nível de mensuração qualitativo: ordinal ou nominal.
    * Os dados sob análise têm nível de mensuração quantitativo, mas há indícios de que a distribuição
      populacional não é normal.
    * Há interesse em realizar inferência sobre outras características da população, além dos parâmetros da sua distribuição, como a própria forma da distribuição.

* Escolhendo o teste: https://help.xlstat.com/s/article/which-statistical-test-should-you-use?language=en_US

### Distribution Type

* **Shapiro-Wilk Test**	
  * Non parametric
* **D’Agostino’s K^2 Test**
* **Anderson-Darling Test**
* **D’Agostino and Pearson’s**
* Parametric
  * null hypothesis: x comes from a normal distribution
  * Python: `scipy.stats.normaltest`

### Relationship

* correlation or another similar metric
* goodness of fit?
* **Chi-Squared Test**
  * ?
* **Kolmogorov–Smirnov**
  * ?
* **basic hypothesis tests on the linear regression coefficients**
* H0 (null hypothesis): There is no relationship between X and Y (β1 = 0)
  * Ha (alternative hypothesis) : There is some relationship between X and Y (β1 $\neq$ 0)
  * We reject the null hypothesis—that is, we declare a relationship
    to exist between X and Y —if the p-value is small enough
  * Typical p-value cutoffs for rejecting the null hypothesis are 5 or 1 t%.

### Mean

* **Z test**

  * Parametric

  * ==Compara um conjunto de dados e uma média pré-estabelecida==

  * Pode ser adaptado para outras estimativas além da média

  * O nome vem por causa da utilização de z-scores

  * assume uma distribuição gaussiana

  * Null hypothesis: Sample mean is same as the population mean (pre determined)

  * Requires to know the population std: we can even estimate from the sample, but that is not good

  * Z-test is used when sample size is large (n>50), or the population variance is known. t-test is used when sample size is small (n<50) and population variance is unknown

  * <u>Procedimento</u>

    * Identifique as duas hipóteses

    * Especifique o nível de significância

    * calcule z (que usaremos para achar p):
      $$
      z=\frac{\bar x - \mu}{\sigma / \sqrt n}
      $$

      > $\mu$ = média da população (que a $h_0$ estimou)
      > $\sigma$ = desvio padrão da população (ou da amostra, se não soube)
      >
      > $\bar x$ = média da amostra
      >
      > $n$ = tamanho da amostra

    * Se a condição for do tipo $h_a$: $\mu<k$, P será a área da cauda direita==, $p=\int_{-\infty}^k pdf(x)dx$?==

    * Se a condição for do tipo $h_a$: $ \mu >k$, P será a área da cauda esquerda

    * Se a condição for do tipo $h_a$: $ \mu \neq k$, P será a soma da cauda direita e da esquerda

    * Uma vez tendo P, podemos decidir se rejeitamos ou não a hipótese 

* **T Test**

  * Parametric
  * Compara dois conjuntos de dados
  * Aceita ou rejeita uma hipótese nula that 2 independent samples have identical average
  * Assume que a variância das amostras são iguais
  * Welch’s T test: não assume que a variancia é igual
  * <u>How it works</u>
    * Reduz seu conjunto de dados a um valore real *mi,* utilizando o valor médio e o desvio padrão (supõe uma distribuição gaussiana)
    * Comparamos um dois *mi* e obtemos parâmetros comparativos V (relacionado ao grau de liberdade) e T
    * Calculamos a probabilidade de nossa hipótese nula (uma ou hipótese mais extrema que ela) ser verdadeira, baseado nos T e V obtidos
* **Analysis of Variance Test (ANOVA)**
  
  * Parametric
  * Used to compare multiple (three or more) samples with a single test
* **Repeated Measures ANOVA Test**
  
  * Parametric	
* **Mann-Whitney U Test**
  
  * Or *Wilcoxon-Mann-Whitney test*
  * Non parametric
  * Test null hipotesis in two data populations: two populations have the same mean
* **Wilcoxon Signed-Rank Test**
  
  * Non parametric
* **Kruskal-Wallis H Test**
  
  * Non parametric
  * To several populations
  * scipy implements to median
  * Assumptions: all of your samples come from the same "type" of distribution with only a difference in their central location
  * does not identify *which* populations are different, just that at least one ahve a different mean
* **Friedman Test**
  
  * Non parametric

## Other estimation methods

* Estimation statistics is quite a new area, so there is a lot of methods being developed
* Some people refer to it as *new statistics*, and does not include hypothesis testing on it


* **Effect Size**
  * Methods for quantifying the size of an effect given a treatment or intervention.
* **Interval Estimation**
  * Methods for quantifying the amount of uncertainty in a value
  * See datails in its own section above
* **Meta-Analysis**
  * Methods for quantifying the findings across multiple similar studies.

