% For Your Eyes Only
pkg load image;

img = imread('bird_small.png');
img = img(:,:,1)
imshow(img);
edges = edge(img, 'canny')
imshow(edges)