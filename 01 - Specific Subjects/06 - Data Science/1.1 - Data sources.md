# Data sources

## Public souces of data

* http://wolframalpha.com
* Kaggle: https://www.kaggle.com/datasets
* Portal Brasileiro de Dados Abertos: http://dados.gov.br
* UCI Machine Learning Repository: https://archive.ics.uci.edu
* Google Dataset Search: https://datasetsearch.research.google...
* Dados Abertos do Governo Americano: https://www.data.gov
* Amazon’s AWS datasets
* sites that [aggregate data](http://www.readwriteweb.com/archives/where_to_find_open_data_on_the.php) such as [InfoChimps](http://infochimps.org/), [Freebase](http://www.freebase.com/), and [Information Aesthetics](http://infosthetics.com/). , http://dataportals.org/, http://opendatamonitor.eu/, http://quandl.com/
* **Receita Federal - CNPJ**
  * http://receita.economia.gov.br/orientacao/tributaria/cadastros/cadastro-nacional-de-pessoas-juridicas-cnpj/dados-publicos-cnpj
  * Python utility: https://github.com/fabioserpa/CNPJ-full
* **Anatel**
  * [canais de radiodifusão](http://sistemas.anatel.gov.br/se/public/view/b/srd.php)
* https://datasets.bifrost.ai/?ref=producthunt (images/videos)
* https://www.kdnuggets.com/datasets/index.html
* [papers with code](https://paperswithcode.com/)

## Private companies that sell data

* https://lionbridge.ai
* FigureEight: data collection services; also existing datasets, private and opensources; acquired by Appen

# By application

## Demografic

* **IBGE**
  * Segmentação por municípios, mesoregiões, etc
* Data Rio

## Climate databases

* **Climate Forecast System (CFSR)**
  * Base de dados da NASA, atualizada 4 vezes de por dia
  * Confiável pra caralho
  * Fornecem diversos parametros climáticos (além da radiação)
  * <https://rda.ucar.edu/datasets/ds094.0/#!access>
  * Registro
    * Depois de registrar, clicar em data acess e depois get a subset.
    * Depois escolhe a data e o parâmetro e avança, daí seleciona as configurações e vai em submit request.
    * Vai abrir uma página da solicitação.
    * Tem que ficar atualizando pra ver quando vai liberar
    * Se eles não liberarem rápido NÃO tenta de novo, fica esperando, senão eles bloqueiam teu cadastro
    * Talvez  ele pergunte antes de mandar pra página de solitação se comprimir ou  não. Melhor comprimir pra RAR porque senão ele manda todos os arquivos  separados por dia ou mês
* **Era-Interim**
  * Base de daddos europeia
  * Atualizada mensalmente
  * <https://apps.ecmwf.int/datasets/data/interim-full-daily/levtype=sfc/>
* **labrem**
  * ifsc participou
  * http://labren.ccst.inpe.br/atlas_2017_SC.html
  * http://labren.ccst.inpe.br/atlas2_tables/SC_glo.html
  * 2957 (Número de florianópolis na shapefile deles
* **SONDA**
  * http://sonda.ccst.inpe.br/index.html
  * japa não conseguiu
* **Windguru**
  * mostra os dados como velocidade e direção de ventos, tamanho de ondas, temperatura, pluviosidade
  * https://www.windguru.cz/53
  * https://stations.windguru.cz/json_api_stations.html
  * https://stackoverflow.com/questions/34321679/how-to-extract-wind-speed-and-wind-direction-from-windguru-spot-service
* **SWERA**
  * https://openei.org/doe-opendata/dataset/swer
* **Climate analyser**
  * <https://climatereanalyzer.org/wx/DailySummary/#t2>
* **Dicas**
  * recomendação de software para visualizar dados em netCDF: panoply

## Thermal image dataset

http://vcipl-okstate.org/pbvs/bench/ (open. Roof mounted)
http://www.qirt.org/liens/FMTV.htm (faces)

https://www.researchgate.net/post/I_want_a_database_of_thermal_images_for_faces_can_anyone_suggest_from_where_i_can_get_the_database (some baby photos)S



## Interesting ones

* **Slave voyages**
  * Several datasets
  * Seems to be very well organized
  * https://www.slavevoyages.org/





# CV

* MS COCO
  * 80 classes
  * detection and segmentation
* Open Images
  * 600 classes

# Annotation formats

* ==Should I move this??==

## Image

* **PASCAL VOC**

  * used by ImageNet

  * Difficult tag: According to your deep neural network implementation, you can include or exclude difficult objects during training.

  * Truncated tag: ?

  * XML file for each image, each one something like:

    ```XML
    <annotation>
    	<folder>images</folder>
    	<filename>images(1980).jpg</filename>
    	<path>/home/benitez/Desktop/pizza/images_subset/train/images/images(1980).jpg</path>
    	<source>
    		<database>Unknown</database>
    	</source>
    	<size>
    		<width>275</width>
    		<height>183</height>
    		<depth>3</depth>
    	</size>
    	<segmented>0</segmented>
    	<object>
    		<name>pizza</name>
    		<pose>Unspecified</pose>
    		<truncated>0</truncated>
    		<difficult>0</difficult>
    		<bndbox>
    			<xmin>95</xmin>
    			<ymin>65</ymin>
    			<xmax>207</xmax>
    			<ymax>143</ymax>
    		</bndbox>
    	</object>
    	<object>
    		<name>pizza</name>
    		<pose>Unspecified</pose>
    		<truncated>1</truncated>
    		<difficult>0</difficult>
    		<bndbox>
    			<xmin>182</xmin>
    			<ymin>16</ymin>
    			<xmax>275</xmax>
    			<ymax>85</ymax>
    		</bndbox>
    	</object>
    </annotation>
    ```

* **Yolo format**

  * classes must be declared in "classes.txt”	

  * one txt file, each one something like (two objects, classes 0 and 1):

    ```
    0 0.252727 0.519126 0.432727 0.251366
    1 0.710909 0.674863 0.338182 0.377049
    ```

    

* RecordIO??

# Others

great dataset for big data: NYC taxi and limusine (1.32Bilions rorws_)