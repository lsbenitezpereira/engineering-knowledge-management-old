# Conceptualization

* Some tasks (like file processing or grafical interfaces) are immediately dispatched to compiled C code inside the Python interpreter

* The standard implementation of Python is written in portable ANSI C,

* Python runs from left to right

* Do comments with # (single line) or ''' (multline)

* **Usage**

  * focus on readability, coherence, and software quality

  * Program portability

  * optimized for speed of development

  * Very good libraries

  * Good integration with others languages (allowing customization and extension)

  * Python vs matlab

    > […] they're for different purposes.
    >
    > If you just need a framework to play with some arrays and run basic  industry standard algorithms then Numpy and its various extensions  certainly have you covered. If you need to do serious engineering  design, modeling, and simulation then Matlab has incredibly specialized  libraries for practically every discipline that implement state of the  art algorithms that you simply can't find in the open source world. Not  to mention Matlab's IDE is way better than anything Python can offer for numeric analysis. Also worth mentioning that Matlab has the best  documentation of any language I've worked with. I haven't seen a any  Python library come close to the quality of Matlab's documentation and  most of them just rely on Stack Overflow.
    >
    > Python on the other hand is far more extensible, modular, and deployable. Of  course that's not the purpose of Matlab. No one puts Matlab code into  production. Matlab is for design and testing before you write your real  code in something like C or Fortran or before you drop a few hundred  million dollars on manufacturing custom hardware for real world testing.
    >
    > Not saying one is better than the other. They're just for very different purposes.
    >
    > [Source](https://www.reddit.com/r/MachineLearning/comments/fkwueg/d_can_scipy_numpy_replace_matlab_in_an_industry/)

* **bytecode**
  * Python translate source code statements to an intermediate format known as byte code, and then interpret the byte code in a virtual machine
  * the byte code translation is performed to speed execution
  * Byte code is stored in files in the same directory as the corresponding source files,
    normally with the filename extension .pyc (bytecode source)
  * when runing the program a seconds time, Python will load the .pyc files and skip the compilation step
  * byte code is saved in files only for files that are imported, not for the top-level files of a program that are only run as scripts (strictly speaking, it’s an import optimization)
  * that convertion to bytecode happens already in runtime 
  
   ![img](Images - Python/1550709576910.png)
  
* **Frozen binaries**

  * standalone binary executables, together with the PMV e everything you need
  * executable

* **Python implementation**

  * The original, and standard, implementation of Python is usually called CPython
  * PyPy: implementatin of python that is compilated, to just fucking faster
  * MicroPython: to microcontrollers
  
* **C integration**

  * This type of things is exactly what Python is awesome for: you can write your speed critical components in C directly, and it will interact  (almost) seamlessly with your python code.
  * ==cython?==

* ==what is a code PEP8 compliant?==

## Operators

* **Arithmetic**
  * *, +, -, ** (exponentiation)
  * `math` import
  * math.pi
  * math.sqrt(<number>)
* **sequence operations**
  * sequence = strings, lists and tuples
  * negative indexes count back from the right (*ex:* S[-1] is the last item)
  * careful: they do not change the original string
  * +: concatenation;  expects the same sequence type on both sides
  * *: repetition
  * len(<sequence>)
    * return the lengh of the sequence
  * <sequence>[<index>]
    * acess element by index
  * <Sequence>[<a>:<b>]
    * slice from a to b-1 (subset $a\leqslant i<b$). 
    * If a or b is missing, it slice from first or until last
    * Slices create copies, stead of references (careful: numpy is the oposite)
* **Set operations**
  * just to `set` data type
  * & Intersection
  * | union
  * \- difference
  * \> superset (return boolean)
* **Bitwise**
  * ~: not
* **Logical**
  * `not`
  * `and`
  * `or`
* **Equality**
  * `==` compared value
  * `is` compared if they are the same object, that is, is both *names* referencee the same *object*

## Flow control

* For
  * user-defined loop variable is used to reference the current item each time through.
* while <condition>

## Functions

* **Traditional function**

  * It’s registered in this section only by convenience, but we dont need to be programing thinking in function orientation to used simple functions
  * def <name> (<parameter1>,<parameter2>, …): <body> return <expression>
  * No backets: the function is defined by identation 
  * `def func(*args): myVariable = list(args)`

* **lambda expression**

  * anonimous (inline) functions

  * Do not need *def* or *return*

  * `lambda <parameter1>,<parameter2>: <return expression>`

  * Using if else in lambda function is little tricky, the syntax is as follows:

    `lambda <parameter> : <Return1> if <condition> else <Return2>`
    
  * To do an assignment, use `:=` surrounded by `()` (see [pep 0572](https://www.python.org/dev/peps/pep-0572/))

* **Generators**

  * Or *semi-coroutines*
  * function that acts as an iterator
  * Is similar to a coroutine (because it also is a function that maintains some state), but it can only suspend its execution under specific situations (therefore less flexible than a coroutine)
  * you can use with the `for ... in` operator
  * built using the `yield` operator
  * The performance improvement from the use of generators is the result of  the lazy (on demand) generation of values, which translates to lower  memory usage
  
* **Variable arguments**

  * `**kwargs`
    * keyword arguments (key-value pairs)
  * `*arg`
  
* **Reference**

  * Python is “pass-by-object-reference”

  * Gotcha code:

    ```python
    def reassign(list):
      list = [0, 1]
    
    def append(list):
      list.append(1)
    
    list = [0]
    reassign(list)
    append(list)
    ```

  * 


## Functional Programming

* Will focus here in *functional orientation* (that is oposite to *object orientation*)

* **map**
  
  * To list transformation
  * Apply a function (or lambda expression) to all the elements of a sequence
  * returns a new sequence with the elements changed by that function
  * `map(<function>, <sequence>)`
  * Can receive more than one sequence, and the functions must be compatible (e.g. receive two list and the function receive two parameters)
  
* **Filter**
  
  * To list selection
  * return the elements of a list to which the function returns True. 
  * The return of the function will be interpreted as boolean, an will only be used to select or not the element
  * filter(<function>, <sequence>)
  
* **Reduce**

  * To list aggregation
  * continually applies the function func() to the sequence seq. It returns a
    single value. 
  * The function must receive two parameters
  * The function is applied two by two (the next with the previous result)
  * `reduce (<function(x1, x2)>, <sequence>)`
  * Ex: reduce(lambda x,y: x+y, [47,11,42,13])

  ![Veranschulichung von Reduce](Images - Python/reduce_diagram.png)

## **Object persistence system**

* based on files or access-by-key databases

* translates objects to and from serial byte streams
  automatically
  
* **pickle** 
  
  * Do not export the Class, just a reference to it
  * internally Python reimports the class from its module, creates an instance with its
    stored attributes, and sets the instance’s class link to point to its original class.
    This way, loaded instances automatically obtain all their original methodslin, even if we have not imported the instance’s
    class into our scope.
  * So, to open in another file, you need to import the class (to more explanations, see https://rebeccabilbro.github.io/module-main-has-no-attribute/)
  
* **dbm**

  * access-by-key filesystem for storing strings

* **Shelve**
  
  * Build on top of pickle and dbm
  
  * associate each pickle object to a key (in a dictionary-like object)
  
  *  the key can be any string and should be unique
  
  * Basic template:
  
    ```python
    import shelve					
    db = shelve.open('persondb')	# Filename where objects are stored
    db[obj.name] = obj				# Store object on shelve by key
    db.close()						# Close after making changes
    ```
  
  * `db.keys()`: return all keys
  
  * `db[‘<key>’]`: fetch that object
  
* **Joblib**
  
  * optimized to export numpy arrays and scikitlean modules
  * Build on top of Pickle
  * Will have the same problem with Classes, expect to scikit classes (that he will look for automticaly)
  
* 

# Data types

* Dynamic typing (no esplicit declaration)

* strongly typed (cant change the type)

* Types are just classes (after python 3)

* once you create an object, you bind its operation set for all time

* index begin in zero

* generic operations that span multiple types show up as built-in functions or expressions (e.g.,
  len(X), X[0]), but type-specific operations are method calls (e.g., aString.upper()
  
  ![image-20191223210315218](Images - Python/image-20191223210315218.png)
  
* **Immutability**
  * cant change after created (you need to create a new one). 
  * Numbers, strings, and tuples are immutable; lists, dictionaries, and sets aren’t
  
* **garbage collection**
  
  * Memory is cleaner (free) when we lose the last reference to the object
  * In standard Python, the space is reclaimed immediately, as soon as the last reference to an object is removed
  * Each object keeps track of how many *names* (variables) make references to him
  * There is as special garbage collector for ciclic references, which must be deleted anyway
  * small integers and small strings may be cached for reuse (thus not collected imediatelly)
  
* **Global statement**

  * Global names must be declared only if they are assigned within a function.
  * nonlocal statement: applies to names in the enclosing def’s local scope, rather than names in the enclosing module
  
* **“static” typing with mypy**

  * sincce python 3.6, you can declare types with sintax `variable: type`
  * `def foo(n: int, s: str='Tom') -> str:`
  * Python itself won’t do anything with these types at runtime
  * you cna check it in build time or unit tests
  * do not enforce the type in run time (thus have no execution overhead)

* Useful links

  * Data structures: https://dbader.org/blog/fundamental-data-structures-in-python

## **Numbers**

*  immutable
*  integers
*  floating-point
*  Complex numbers
*  **Decimals**
   *  with fixed precision
   *  need `decimal` import
   *  need explicit declaration with factory function (ex: `<varName> = decimal.Decimal('3.141')`)
*  **rationals**
   *  with numerator and denominator
   *  ex: `<varName> = Fraction(<numerador>, <denominator>)`
*  **Boolean**
*  **==full-featured sets??==**

## String

* wrapped by ‘aspas simples’ ou “duplas” (‘’‘triple’‘’ allow line break)

* positionally ordered collection of characters

* by default if unicode (in python 3)

* ==add a \0, a binary zero byte, does not terminate string==

* import `re` module to patter maching

* sequence, immutable

* **.find (‘<sub>’)**
  
  * search operation
  * it returns the offset of the passed-in substring, or −1 if it is not present
  
* **.replace (‘<sub>’, ‘<rep>’)**
  
  * find and replace
  * return new string
  
* .split

  * Split on a delimiter into a list of substrings
  * by default, slip by space delimiter
  
* **.upper()**

* **.isalpha()**

  * return boolean
  
* **.format()**

  * like printf without print (just return the string)
  * `‘blabla {var1} blabla {var2}’.format(<var1>, <var2>)`
    
  * The “link” can be done in sequence or you can attribute a name to the variables
  
* **String formatting**

  * `f'string'`
    * Or *f-string sintax*
    * commands must be inside `{}`
    * less verbose than `.format()`
  * `“string”.format()`
    * ?
  
  

## **List**

* positionally ordered collections of other objects
* have no fixed type constraint (unlike arrays in C)
* have no fixed size
* wrapped in square brackets []
* sequence, mutable
* **matrix**
*  list of lists
  *  `M [line][column]`
* **list comprehension expression**
  * allow to run an expression inside the list (returns a list from that)
  * ==work on any type that is a sequence?==
  * Generally will run faster than a explicit loop expression 
  * ex (select column 2): ` col2 = [row[1] for row in M]`
* **.append**
* add object at end of list
* **.pop (<n>)**
* delete index n
  * return the deleted item
* **.extend**
* add multiple items at the end
* **.sort()**
* **.reverse()**
* **.insert**
*  insert an item at an arbitrary position
* **.count(<element>)**
  *  return the number of occurencies of <element> in the list
* **remove**

  * remove a given item by value
* `<element> in <sequente>`
  * return (boolen) if that element (can be a literal object) is present in the list

## **Dictionary**

*  store objects by key

*  They are not sequences (like lists or strings), but mappings (collections of other objects, but they store objects by key instead of by relative position)

*  Wrapped in curly braces {}

*  Mutable object

*  assignments to new dictionary keys create those keys (dictionaries dont have explicit bound limitation)

*  *key: value* pairs

*  fetch and change the keys’ associated values: <DicName>[‘<key>’]

*  ==Zipping wih Dict(<keyList>, <valueList>)?==

*  keys can be of any hashable type

*  try to access non existing keys return error. We can test the existence with `if (‘<key>’ in <DicName>)`

*  dictionary comprehensions
   
   *  Ex : `squares = {x: x * x for x in range(10)}`
   
*  Python built-in distionary performance
   *  Implemented with hashtable
   *  O(1) time complexity for lookup, insert, update, and delete operations in the average case
   
*  Other dictionary-based data types
   *  OrderedDict – Remember the insertion order of keys
   *  defaultdict – Return default values for missing keys
   *  ChainMap – Search multiple dictionaries as a single mapping
   *  MappingProxyType – A wrapper for making read-only dictionaries
   *  
   
*  .keys()
   
   *  return a list of the keys (unordered) 
   
*  Variation: default dictionary

   *  automatically create an entry when it is acessed and give it a default value, such as zero or the empty list

   *  Example:

      ```
      from collections import defaultdict
      frequency = defaultdict(int)
      frequency['ideas'] #return 0
      ```

      

## Tuples

* /tupou/
* Sequence, immutable 
* represent fixed collections of items
* wrapped in parenthesis (), but can be declared with no wrapper at all
* support mixed types and nesting
* the tuple can be “unpacked”, breaking the individual items
  * ex: `for (x,y) in ListOfTuples: print (x)`
* .index (<elementValue>)
* .count (<elementValue>)

## Files

* sequence of lines 
* Python comes with additional file-like tools: pipes, FIFOs, sockets, keyed-access files, persistent object shelves, descriptor-based files, relational and object-oriented database interfaces, and more
*  Open (‘<fileName>’, ‘<mode>’)
  * return an file object (that is the only way to create an file object)
  * mode: r, w, rb, wb
* .close()
  * flush output buffers to disk
* **Text files**
  * The data are sotered as strings
  * Encoding
    * Enconded by default in Unicode (after python 3)
    * to other encoding types (like utf8), we must pass as a parameter when opening (will encode on writes and decode on reads)
    * ex:  `file = open('unidata.txt', 'w', encoding='utf-8')`
    * The enconding of an object (string of byte structure) can be explicity changed with .encode(‘<type>’) and .dencode(‘<type>’) methods
  * .write(‘<string>’)
    * Write strings of characters
    *  Return number of items written
  * .readLine()
  * .seek
  * encoding type
* **Bynary files**
  * ==need struct module?==
  * use read and write methods, but we have to pass an struct (packed binary data) as parameter

## Sets

* unordered collections of unique and immutable objects (the are neither mappings nor sequences)
* Allow set compehention expresions (loop inside the set, like with lists)
* support `in` membership tests

# Object Orientation

* Polymorphism, operator overloading, and multiple inheritance
  
* Python supports both procedural and object-oriented programming modes
  
* atributes can be added dinamicaly
  
* **Classes**
  
  * All variables defined on the class level in Python are considered static
  * Python doesn't have any mechanism that effectively restricts access to any instance variable or method
  * Python’s *self* is the same as the *this* pointer
  * An instance object’s namespace records data that can vary from instance to instance,
    and self is a hook into that namespace
  * in memory critical application, you can limit the propeties using `__slots__`
  
* **Operator overloading**
  
  * Methods named with double underscores (`__X__`) are special hooks (or *dunder methods*, or *magic methods*), called automatically in some situations
  
  * Constructor method: ` __init__`
  
  * ` __and__`
  
  * `__str__` is run when an object is printed (technically, when it’s converted to its
    print string by the str built-in function or its Python internals equivalent)
  
  * `__repr__`: when representing/displaying the object to debug or similar 
    
  * `__len__`: to function len()
  
  ![image-20200417223043628](Images - Python/image-20200417223043628.png)
  
* **callig methods**

  * methods can be called through either an instance—`instance.methodName()`—or a class—`Class.methodName(instance)`
  * By the second way you can break the multiple inheritance precedence
  * super: allows calling back to a superclass’s methods more generically
  
* **Hierarchy**

  * multiple inheritance: left-to-right order gives the order in which those superclasses will be searched for attributes by inheritance
  * `super()`: call the super class

  ```
  class C2: ...			# Make class objects
  class C3: ...
  class C1(C2, C3): ...	# Linked to superclasses C2 and C3 (in this priority sequence)
  ```

* **introspection tools**
  
  * special attributes and functions that give us access to some of the internals of objects’ implementations
  * `instance.__class__` attribute provides a link from an instance to the
    class from which it was created
  * `instace.__bases__` attribute that is a tuple containing links to higher superclasses
  * `instance.__dict__` attribute provides a dictionary with one key/value
    pair for every attribute attached to a namespace object (including modules, classes,
    and instances)
  * The `dir(instance)` method tries to return a list of valid attributes of the object. does much more than look up object’s `__dict__`, creating a complete list of available attributes on the instance because it will use the object's heritage
  * `inspect.signature(<class>)` 
  
* `isinstance`
  
* `issubclass`

* super

  * built-in function that can be used to invoke superclass methods generically
  * usage: accesing a same-named method
  * `super().methodName()`
  * a more pythonic alternative is to call is doing `Father.methodName(self)` (supose a superclass called Father)

# Decorators

* a way to specify management or augmentation code for functions and
  classes
* You put it on top of it
* takes the function below and does something with it
* multiple decorators can be used, but not all combinations
* `@classmethod`
  * above a method
  * the method will receive the class as first parameter, not the instance
  * allow to modify all the atributes of the class
* `@staticmethod`
  * does not receive the instance as parameter
  * ==difference to classmethod?==
* `@property`: access a method as it was an atribute (like to do an getter method)
* `@methodName.setter`: similar to above, but to setter

# Modules

* easy way to organize components into a system by serving
  as self-contained packages of variables known as namespaces
* give access to names in a module’s global
  scope
* Path and extension details are omitted and autotically searched. 
* `import`: Lets a client (importer) fetch a module as a whole
  * Does not allow relative import: the search always begin by callee home directory
* `from`
  * Allows clients to fetch particular names from a module
  * You can explicitly determine the search path as relative to you when working from inside the module, doing `from . import P` or `from .. import M`
* `imp.reload`: Function to reload a module’s code without stopping Python
* Search sequence
  1. The home directory of the program (or immediate subdirectory of the one we’re working in)
  2. PYTHONPATH directories (can be set)
  3. Standard library directories
  4. The contents of any .pth files (if present)
  5. The site-packages home of third-party extensions
* `if __name__ == '__main__':`
  * to be executed only in the main software (not included)
*  `__init__.py` Files
  * run automatically the first time a Python pro-
    gram imports a directory
  * Are required in every directory import in python$\leq$3.2
  * can be empty
* Is possible to link compiled extension modules written in another language

# Exceptions

*  events that can modify the flow
   of control through a program

*  high-level control flow tool

*  jump to an exception handler in a
   single step, abandoning all function calls, like a “super go to”

*  default exception-handling behavior: stops the program and prints an error message

*  stack trace (or *Traceback*): list of all the lines and functions that were active when the exception occurred

*  **Usages**

   *  Error handling: shits happen, detect and deal with it
   *  Event notification: an valid event happen, with detect in a elegant manner
   *  Special-case handling: check weird/edge cases
   *  Unusual control flows: high-level and structured “go to”
   *  Termination actions: guarantee execution of destructors


* **`try/except/finally`**

  * Catch and recover from exceptions raised by Python, or by you

* compound, multipart statement

  * After Python 2.5 we can mix except and finally clauses in the same try statement

* Can be nested

  * `try`: main code to be executed

* `except`

  * The defined handler will be used instead of the default

* your program will resume normal execution after it

* You can use several excepts in sequence

  * an except can handle a set of exceptions, like in `except (name2, name3):`
  * If the exception raised doesn’t match any except clause, the default handler is executed
  * Empty except (without exception name):catch all exceptions not previously listed
  * Exception named Exception has almost the same effect as an empty except, but ignores exceptions related to system exits
  * `as`
    *  gain access to the exception object itself
    *  assigning the raised exception object to the variable named
  * `else`

    * Executed if no exception happen
    * You can code else only if there is at least one except
    * there can be only one 
  * `finally`
    * finally will always be executed after the try, exception happening or not
    * there can be only one 

* **`raise`**

  * Trigger an exception manually in your code
  * are caught the same
    way as those Python raises
  * `raise Exception("message") `

* **`assert`**

  * Conditionally trigger an exception in your code.

* **`with/as`**

  * Implement context managers in Python 2.6, 3.0, and later (optional in 2.5)
  * guarantee that termination actions,even if excetion occur
  * may also run startup actions too
  * it’s applicable only when processing
    certain object types
  * Usage
    * Ensure that the file’s output buffer’s content has been flushed from memory to disk
    * Guarantee that server connections are closed

* **Built-in exceptions**

  * IndexError
  * AttributeError
  * TypeError
  * SyntaxError

* **User defined exceptions**

  * coded with classes

  * Example: 

    ```
    class MyEx(Exception): 
      def __str__(self): return 'My Exception Sucks'
    
    def grail(): raise MyEx()
    ```

    

    `class AlreadyGotOne(Exception): pass; `

# Files, IO, interfaces

## Basic File handling

* open(<filename>, <mode>)
  * mode: read (r), append (a), write (w), create (x, error if the file exists), more text (t, default) or binary (b)	
  * Return file pointer
* <fp>.read()
* <fp>.readline()
  * Also `for x in <fp>:` read line by line in x
* <fp>.close() 
  *  file objects are automatically closed on garbage collection
* <fp>.write(<str-content>)
* delete: see in OS module

## In memory buffers

* **ByteIO**

  * creates a binary memory buffer

* **TemporaryFile**

  * https://docs.python.org/3/library/tempfile.html

  

## Encoding and formatting

* **Pillow (PIL)**
  * free library for the Python programming language that adds support for opening, manipulating, and saving many different image file formats
* `bytearray`
  * Usage example: `mqtt.send(bytearray(file.read()))`
  * ==fica codificado como o quê esse negócio??????==
* `base64.b64encode`
  * ?

## OS and directories

* :book: *Chollet (deep learning) page 145*

* **os**

  * **subprocess**

  * 

* **OS module**
  * os.path.join(<dir>, <name>)
  * os.listdir(<dir>)
  * os.remove("demofile.txt") 
  * 
  * * exec command : `os.system('ls')` (cant see return)
  * 

* **sys module**

  * ?
  * 

* **Subprocess module**
  * spawn new processes/==threads==
  * preferable over `os`
  * to pass a single string as command, use the argument `shell=True`
  * `run()`
    *  Wait for command to complete
    *  Example: `subprocess.run(["ls", "-l", "/dev/null"], capture_output=True)`
    *  `timeout` argument: stop an external program if it is taking too long to execute
    *  `input` argument: pass data to the `stdin` of the subprocess
    *  you can get the output with `.read()` ???
  * `call()`
    * Similar to the `Popen` class
    * Wait for command to complete
    * gives you the return code
    * Example: `return_code = subprocess.call(['echo', 'Even more output'])`
  * `check_output()`
    * just an utility function
    * execute, block until complete, and return output
    * Example: `output = subprocess.check_output("cat /etc/services", stderr=subprocess.STDOUT, shell=True)`

## Command line arguments

* **Module sys**: provides access to any command-line arguments via the sys.argv;
* **module getopt**: ease parsing th argumetns, similar to C
* **module argparse**
  * ease parsing
  * dest: name of the attribute returned by parse_args()
  * <u>action</u>
    * what to do with the argument
    * store
    * store_const
    * store_true

## Env variables

* **OS module**
  * `os.getenv(key, <default_value>)`
* **python-dotenv**
  * Already load from a .env file in the same directory

# Libraries

* I’ll write here about some general pourpose libraries, mainly from the Standard Library

* **Datetime**
  * Follows ISO-8601?
  * Creation methods
    * `datetime.strptime('2019-10-26', '%Y-%m-%d')`
    * `datetime.fromtimestamp(1569103140, tz=timezone.utc)`
    * `datetime.strptime('2019-10-29 19:00:00', '%Y-%m-%d %H:%M:%S')`
    * https://www.journaldev.com/23365/python-string-to-datetime-strptime
* range(<n>)
  * generates successive integers
* **`Random` import**
  * random.random(): random number
  * random.choice(<list>): chose one item of the list

## Measuring time

* **time**

  * Example:

    ```
    import time
    StartTime = time.time()
    #Do something
    StopTime = time.time()
    ElapseTime = StopTime - StartTime
    ```

    

* **timeit**

  * Just to small codes
  * Evaluate a string
  * repeat method
    * returns a list giving the total time taken
      to run a test a number of times, for each of repeat runs

## Testing

* In its most simple form, can be something like `assert sum([1, 2, 3]) == 6, "Should be 6"`

* However you’ll probably want some test runners to ease your work

* **unittest**

  * Unit testing framework

  * built into the Python standard library

  * Basic example:

    ```python
    import unittest
    
    class TestSum(unittest.TestCase):
        '''
        Utils: 
            assertEqual
            assertNotEqual
            assertTrue
            assertInt
        
        '''
        def test_sum(self):
            self.assertEqual(sum([1, 2, 3]), 6, "Should be 6")
    
        def test_sum_tuple(self):
            self.assertEqual(sum((1, 2, 2)), 6, "Should be 6")
    
    if __name__ == '__main__':
        unittest.main()
        # or if you are using notebook:
        # unittest.main(argv=[''], verbosity=2, exit=False)
    ```
    
    

* **pytest**

  * https://docs.pytest.org/en/latest/getting-started.html
  * Extension of unittest, allowing to run multiple files from command line
  * run from command line all tests in folder: `pytest -v`
  * test files must have name `test_*.py`
  * test functions must begin with `test_`
  * <u>Differences from unittest</u>
    * Support for the built-in `assert` statement instead of using special `self.assert*()` methods
    * Support for filtering for test cases
    * Ability to rerun from the last failing test
    * An ecosystem of hundreds of plugins to extend the functionality
  * <u>coverage test</u>
    * have as pluging (or build in?)
    * `--cov-report=html`

## Strong typing

* Marshmallow?
* serberous?

### Pydantic

* parsing library (does conversion), not a validation library

* if it cannot parse, raise error

*  (+) fast

* can import from pikcle, sqlalchemy and other wierd objects

* **models**

  * classes which inherit from `BaseModel`
  * is mutable
  * <u>Exporting models</u>
    * .dict()
    * .json()
    * .schema()
  * <u>configurations</u>
    * Set in the nested class `Config`
    * orm_mode: allow to create a pydantic object directly from a sqlalchemy object; recursive parsing; 
    * allow_mutation: attempting to change the values of instance attributes will raise errors; not recursive
    * arbitrary_types_allowed: accept hierarquical definitions with any class, not only pydantic’s

* **types**

  * Most of them are native or are in the module `typing`

  * int, float, str, boo, bytes…

  * datetime, uuid, IPv4Address, PaymentCardNumber, PositiveInt

  * List<type>, Tuple, Dict[<typeKey>, <typeValue>], Union[<type1>, <type2>, …], Iterable, Enum

  * FilePath, EmailStr, Color, url (you can set individually each portion), SecretStr (gets masked)

  * Any

  * `Type[T]` to specify that a field may only accept classes (not instances) that are subclasses of `T`.

  * You can have runtime-defined types, inheriting from Generic[<type>]

  * Strict Types (StrictStr, StrictInt, etc): prevent coerce

  * Hierarquicals:

    ```python
    class Bar(BaseModel):
        apple: str = 'x'
    
    class Spam(BaseModel):
        bars: List[Bar]
            
    m = Spam(bars=[{'apple': 'x1'}, {'apple': 'x2'}])
    ```

  * PrivateAttr(): not show nowhere

  * <u>optionalness</u>

    * if have type only, is required
    * if have default value, is optional
    * `Optional[<type>]` is a shorthand notation for `Union[<type>, None]`
    * If it have the value `...`, is required

  * <u>Field customisation</u>

    * [docs](https://pydantic-docs.helpmanual.io/usage/schema/#field-customisation)

    * allow to set more stuff

    * Example: `c: int = Field(...)`

    * Runtime default value, example: `created_at: datetime = Field(default_factory=datetime.utcnow)`

    * The `description` for models is taken from either the docstring of the class or the argument `description` to the `Field` class.

    * Example:

      ```
      snap: int = Field(
          42,
          title='The Snap',
          description='this is the value of snap',
          gt=30,
          lt=50,
      )
      ```

      

* **Validators**

  * within the class, use the decorator `@validator`
  * To lists and etc, the validation can be performed in `each_item`
  * by default validators are not called for fields when a value is not supplied
  * The validation can happen in the entire class using `@root_validator`
  * the keyword argument `pre` will cause the validator to be called prior to other validation

* * <u>custom error messages</u>
    * create your own exception from PydanticValueError

* **Settings management**

  * https://pydantic-docs.helpmanual.io/usage/settings/
  * If you create a model that inherits from `BaseSettings`, the model initialiser will attempt to determine the values of any fields not passed as keyword arguments by reading from the environment.

* **External plugins**

  * generate pydantic model from example json: ?

## Networking

* DNS lookup: `import socket; socket.gethostbyname('google.com')`

## Others

* 
* **Cookiecutter**
  * package to create project templates
  * open source
* * 

## Softwares and IDEs

* **Jupyter notebook**
  
  * Aplicação web de código aberto
  
  * incorpora equações, textos, imagens, tudo em um unico arquivo
  
  * It runs on the browser. To run the localhost (port 8888 by default), run `jupyter notebook` on terminal (in linux)
  
  * Export .py from notebook:
  
    ```
    get_ipython().system("jupyter nbconvert --output 'outputName' --to script 'inputName'")
    ```
  
  * <u>Useful extensions</u>
  
    * See memory usage: https://github.com/yuvipanda/nbresuse
    
  * <u>Using midias</u>
  
    * from IPython.display import Image
    
    * img1 = Image(filename='lalala.png', width=500)
    
    *  audio:
    
      ```python
      from IPython.display import Audio
      
      def synth(freq, duration, fs):
          t = np.linspace(0., duration, int(fs * duration))
          x = np.sin(freq * 2. * np.pi * t)
          display(Audio(x, rate=fs, autoplay=True)) # para mostrar o widget
          return x
      s=synth(330, .2, 8000)
      ```
    
      
    
    * display(Audio(x, rate=fs, autoplay=True))
  
* **Colaboratory**

  * It's a Jupyter notebook environment that requires no setup to use and runs entirely in the cloud
  * By google
  * free-of-charge access to a robust GPU
  
* **jupytext**. It allows you to open .py scripts as notebooks (but they are  saved back to .py format). This way you can do "git diff", pull  requests, etc

* **Binder**:  similar to colab

* **deepnote**: similar to colab, with better colaboration

* **Apache zepelin**

  * ?

## Package manager

* **Virtual environment**
  * is a semi-isolated Python environment 
* **venv**
  * Isolated python envioroment
  * no need for root permision
  * work well with pip
  * `virtualenv ENV`, where ENV can be a name or a directory
  * `pip freeze --local`
  * `source /bin/activate`
  * `deactivate`
* **conda**
  * Isolated python envioroment
  * Conda replaces virtualenv
  * has a built-in virtual enviroment
  * doenst need root?
  * Anaconda: package of libraries and softwares to data science
  * Works with pip… but not very well, because one can modify the packages of the other and break dependencies 
  * create: `conda create --name environment_name python=3.6`
  * Install: `conda install <package>`
  * Export: `conda list -e > requirements.txt`
  * Clone: `conda create --name myclone --clone myenv`
  * to disable the automatic base activation: `conda config --set auto_activate_base false`
* **pip**
  * package manager
* **Poetry**
  * dependency management, packaging and publishing
  * poetry uses pyproject.toml to replace setup.py, requirements.txt, setup.cfg, MANIFEST.in and Pipfile
* **PDM**
  * similar no NPM
  * no venv envolved
  * created a manifest per projectd





# Concurency

* asynchronous code: the CPU will do something else while waiting for some slow IO, pooling frequency to check if that IO is done
* concurrency (asunchronous) vs paralelism: [funny example](https://fastapi.tiangolo.com/async/#concurrency-and-burgers)
* Overview of the python options: https://devarea.com/python-multitasking-multithreading-multiprocessing/#.XpEY0Pl7k8o
* threads can't return a value, so there are two ways to return a value in threads, one: you can write the output into a file, and then read the file a try\except block, or you can change a global  value to what you want to return. If you still want to use  multiprocessing, you can find some help here: [how to get the return value from a thread in python?](https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python)
* `multitasking` library
  * https://pypi.org/project/multitasking/
* Python std concurrent
  *  higher level API to `threading` library
* mecanismos de sincronização
  *  semáforos
  *  monitores

## **Coroutines**

* term for the thing returned by an `async def` function
* will execute sequentially (one at the time), but the function can be paused and resumed
* abstraction built on top of generator functions that yield futures/promises
* `async/await` : is just a special  syntax to work with promises in a comfortable fashion. A promise is  nothing more than an object representing the eventual completion or  failure of an asynchronous operation.
* With `async def`, Python knows that, inside that function, it has to be aware of `await` expressions, and that it can "pause" the execution of that function and go do something else before coming back
* functions defined with `async def` have to be "awaited". So, functions with `async def` can only be called inside of functions defined with `async def` too.

## Software level paralelism

* About hardware level paralelism (multicicle, pipeline, multicore, etc), see in *Computers Architecture*
* O programa já é pensado para rodar em um processador paralelo

* **Multitasking**
  * OS level
  * Single core, doing one thing at the time
  * each task have a time window
  * just aparent paralelism
  * More demanding programs normally receive more time
  * Cooperative multitasking: ==?==
* **Multithreading**
  * Program level
  * same program, eah part doing multitasking? 
  * still just aparent paralelism
  * for good performance, we can only have a limited number of concurrent  tasks. The recommendation is to have the number of threads equal to  total number of cores in the CPU of the system
  * thread vs process. 
    * process is a container. It has one or more threads. All these threads in a process share some common resources, like the virtual memory space, list of open descriptors, signal masks etc. A thread is an entity that can run code on CPU
* **Multiprocessing**
  * Real paralelism
  * need several cores