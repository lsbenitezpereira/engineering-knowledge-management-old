# Conceptualization

* Active community

* not so good for a final product ==?==

## Data structures

* **Dataframe**
  * Conjunto de vetores/fatores/lista com o mesmo tamanho
  * OU seja, uma tabela
  * DFname$column name //retorna todos os valores dessa coluna  

* **vetor**
  * name <- c (v1, v2, …, vn)
  * c é de concatenação
  * name[n] //retorna valor
  * name[n1:n2] //retorna range 
  * name[-n] //retorna todos os elementos menos o n

* **Fatores**
  * Vetor com variáveis categóricas (discretas)
  * Fname <- factor (c(n1, n2)) //cria um fator 
  * Pode ser explicitamente ordenados 
  * Fname > “n2” //return the values bigger than the n2 catagory (only for ordenated)
  * acessar [n]: retorna uma sublista
  * acessar com [[n]]: retorna só o valor

* **Lista**
  * Conjunto de elementos de tipos distindos? 
  * Lname$field // retorna o field 

## Rstudio

* Free software
* Similar to matlab 

# R language

* <dfName\$colName>: select
* formato white vs formato long???

## Import

* `read.csv(“”)`: return dataframe
* timeseries
  * 

## Analize

* `.summary`
* `.describe`
* is.na(<series>)

## Visualizatios

* view(dataframe)
* head(dataframe)
* `names(df)`: retorna o nome das colunas
* `boxplot(<series>)`
* `dim(<df>)`: numero de linhas e colunas
* ggplotly: graficos dinâmicos

# Deploy

## Shiny
O Shiny é um sistema para desenvolvimento de aplicações web usando o R, um pacote do R (shiny) e um servidor web (shiny server)
