% Chapter 02: Example 02.02: Signal Synthesis

%

close all

n = -2:10; x = [1:7,6:-1:1];

figure;stem(n,x);xlabel('n'); title('x(n)'); axis([-2,10,0,8])

% a) x1(n) = 2*x(n-5) - 3*x(n+4)

[x11,n11] = sigshift(x,n,5); [x12,n12] = sigshift(x,n,-4);

figure;stem(n11,x11);xlabel('n'); title('x(n-5)'); axis([-6,15,0,8])

figure;stem(n12,x12);xlabel('n'); title('x(n+4)'); axis([-6,15,0,8])

[x1,n1] = sigadd(2*x11,n11,-3*x12,n12);

figure; stem(n1,x1); title('Sequence in Example 2.2a')

xlabel('n'); ylabel('x1(n)'); axis([min(n1)-1,max(n1)+1,min(x1)-1,max(x1)+1])

set(gca,'XTickMode','manual','XTick',[min(n1),0,max(n1)])

%

% b) x2(n) = x(3-n) + x(n)*x(n-2)
close all
[x21,n21] = sigfold(x,n); 

figure;stem(n21,x21);xlabel('n'); title('x(-n)'); axis([-7,12,0,8])

[x21,n21] = sigshift(x21,n21,3);

figure;stem(n21,x21);xlabel('n'); title('x(-(n-3))'); axis([-7,12,0,8])

figure;stem(n,x);xlabel('n'); title('x(n)'); axis([-7,12,0,8])

[x22,n22] = sigshift(x,n,2);

figure;stem(n22,x22);xlabel('n'); title('x(n-2)'); axis([-7,12,0,8])

[x22,n22] = sigmult(x,n,x22,n22);

figure;stem(n22,x22);xlabel('n'); title('x(n)*x(n-2)'); axis([-7,12,0,40])

[x2,n2] = sigadd(x21,n21,x22,n22);

figure; stem(n2,x2); title('Sequence in Example 2.2b')

xlabel('n'); ylabel('x2(n)'); axis([min(n2)-1,max(n2)+1,0,40])

set(gca,'XTickMode','manual','XTick',[min(n2),0,max(n2)])