% Chapter 02: Example 02.01: Signal Synthesis

%

clf; close all

% a) x(n) = 2*delta(n+2) - delta(n-4), -5<=n<=5

n = [-5:5];

x1 = 2*impseq(-2,-5,5); x2 = impseq(4,-5,5);

x=x1-x2;

figure;stem(n,x1);xlabel('n'); title('2*delta(n+2)'); axis([-5,5,-2,3])

figure;stem(n,x2);xlabel('n'); title('delta(n-4)'); axis([-5,5,-2,3])

figure; stem(n,x); title('Sequence in Example 2.1a')

xlabel('n'); ylabel('x(n)'); axis([-5,5,-2,3])

%

% b) x(n) = n[u(n)-u(n-10)]+10*exp(-0.3(n-10))(u(n-10)-u(n-20)); 0<=n<=20

n = [0:20];

x11 = n; x12 = stepseq(0,0,20)-stepseq(10,0,20);x1=x11.*x12;

x21 = 10*exp(-0.3*(n-10));x22 = stepseq(10,0,20)-stepseq(20,0,20);x2=x21.*x22;

x = x1+x2;

figure;stem(n,x11);xlabel('n'); title('n'); axis([0,20,0,20])

figure;stem(n,x12);xlabel('n'); title('u(n)-u(n-10)'); axis([0,20,0,2])

figure;stem(n,x1);xlabel('n'); title('n[u(n)-u(n-10)]'); axis([0,20,0,12])

figure;stem(n,x21);xlabel('n'); title('10*exp(-0.3(n-10))'); axis([0,20,0,210])

figure;stem(n,x22);xlabel('n'); title('u(n-10)-u(n-20)'); axis([0,20,0,2])

figure;stem(n,x2);xlabel('n'); title('10*exp(-0.3(n-10))(u(n-10)-u(n-20))'); axis([0,20,0,12])

figure;stem(n,x);

title('Sequence in Example 2.1b')

xlabel('n');ylabel('x(n)');axis([0,20,-1,11])

%

% c) x(n) = cos(0.04*pi*n) + 0.2*w(n); 0<=n<=50, w(n): Gaussian (0,1)

n = [0:50];

x1 = cos(0.04*pi*n); x2 = 0.2*randn(size(n)); x=x1+x2;

figure;stem(n,x1);xlabel('n'); title('cos(0.04*pi*n)'); axis([0,50,-1.4,1.4])

figure;stem(n,x2);xlabel('n'); title('0.2*w(n)'); axis([0,50,-1.4,1.4])

figure;stem(n,x);title('Sequence in Example 2.1c')

xlabel('n');ylabel('x(n)');axis([0,50,-1.4,1.4])

%

% d) x(n) = {...,5,4,3,2,1,5,4,3,2,1,...}; -10<=n<=9

n=[-10:9];

x=[5,4,3,2,1];

xtilde=x' * ones(1,4);

xtilde=(xtilde(:))';

figure;stem(n,xtilde);title('Sequence in Example 2.1d')

xlabel('n');ylabel('xtilde(n)');axis([-10,9,-1,6])