function y = Sm2TensC(x,n)
%
% y = Sm2TensC(x,n)
% --------------
% Decimal equivalent of 
%  Sign-Magnitude format integer to n-digit 10's-Complement format conversion
%
%    x: integer between -10^n/2 <= x <  10^n/2  (sign-magnitude)
%    y: integer between       0 <= y <= 10^n-1  (10's-complement)

N = 10^n;
if any((x < -N/2) | (x >= N/2)) 
    error('Numbers must satisfy -10^n/2 <= x <  10^n/2')
end

s = sign(x);  % sign of x (-1 if x<0, 0 if x=0, 1 if x>0)
sb = (s < 0); % sign-bit  (0 if x>=0, 1 if x<0));
y = (1-sb).*x + sb.*(N+x);