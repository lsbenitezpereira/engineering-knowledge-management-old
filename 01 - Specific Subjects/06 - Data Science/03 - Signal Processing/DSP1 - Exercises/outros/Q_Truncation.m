function xq = Q_Truncation(x,B)
%
% xq = Q_Truncation(x,B)
% ----------------------
%   Binary equivalent xq of truncation of x to B fraction bits
%
xq = fix(x*2^B)/2^B;