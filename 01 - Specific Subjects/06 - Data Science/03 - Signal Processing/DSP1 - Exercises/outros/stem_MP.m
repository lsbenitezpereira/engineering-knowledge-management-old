function [] = stem_MP(X)
%
[M,N] = size(X);

if N == 1
    % One complex array
    figure('units','normalized','position',[0.1-1,0.1,0.8,0.8]);
    X1 = cell2mat(X); N1 = length(X1); k1 = 0:N1-1;
    subplot(2,N,1); Hs1 = stem(k1,abs(X1),'filled'); set(Hs1,'markersize',3);
    xlim([-1,N1]); ylabel('Magnitude'); title('Magnitude Plot','fontsize',12);
    xlabel('Frequency Sample k'); set(gca,'xtick',[0,N1-1]);
    subplot(2,1,2);
    Hs2 = stem(k1,angle(X1)/pi,'filled'); set(Hs2,'markersize',3);
    xlabel('Frequency Sample k'); title('Angle Plot','fontsize',12);
    ylabel('Radians (\times \pi)'); axis([-1,N1,-1.1,1.1]);
    set(gca,'xtick',[0,N1-1]);
elseif N == 2
    % Two complex arraysarray
    figure('units','normalized','position',[0.1-1,0.1,0.8,0.8]);
    % Array-1
    X1 = cell2mat(X(1));  N1 = length(X1); k1 = 0:N1-1;
    subplot(2,N,1); Hs1 = stem(k1,abs(X1),'filled'); set(Hs1,'markersize',3);
    xlim([-1,N1]); ylabel('Magnitude'); title('Magnitude Plot of X1','fontsize',12);
    xlabel('Frequency Sample k'); set(gca,'xtick',[0,N1-1]);
    subplot(2,N,2); Hs2 = stem(k1,angle(X1)/pi,'filled'); set(Hs2,'markersize',3);
    xlabel('Frequency Sample k'); title('Angle Plot of X1','fontsize',12);
    ylabel('Radians (\times \pi)'); axis([-1,N1,-1.1,1.1]);
    set(gca,'xtick',[0,N1-1]);
    % Array-2
    X2 = cell2mat(X(2));  N2 = length(X2); k2 = 0:N2-1;
    subplot(2,N,3); Hs3 = stem(k2,abs(X2),'filled'); set(Hs3,'markersize',3);
    xlim([-1,N2]); ylabel('Magnitude'); title('Magnitude Plot of X2','fontsize',12);
    xlabel('Frequency Sample k'); set(gca,'xtick',[0,N2-1]);
    subplot(2,N,4); Hs4 = stem(k2,angle(X2)/pi,'filled'); set(Hs4,'markersize',3);
    xlabel('Frequency Sample k'); title('Angle Plot of X2','fontsize',12);
    ylabel('Radians (\times \pi)'); axis([-1,N2,-1.1,1.1]);
    set(gca,'xtick',[0,N2-1]);
else
    error('*** Only two complex arrays can be used ***');
end
