x = [2, 4, -3, 1, -5, 4, 7]
n = [-3, -2, -1, 0, 1, 2, 3]

# Function definition
x4a = 2*exp(0.5*n);
[x4b, n4b] = sigmult(x4a, n, x, n);

x4c = cos(0.1*pi*n);
[x4d, n4d] = sigshift(x,n,-2);
[x4e, n4e] = sigmult(x4c, n, x4d, n4d);

x4f = zeros(1, 21);
n4f = [-10:10];

[x4, n4] = sigadd(x4b, n4b, x4e, n4e)
[x4, n4] = sigadd(x4, n4, x4f, n4f)

stem(n4, x4)