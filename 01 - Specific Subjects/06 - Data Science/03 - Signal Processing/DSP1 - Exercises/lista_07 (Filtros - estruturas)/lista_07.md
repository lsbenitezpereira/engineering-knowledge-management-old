Aluno: Leonardo Benitez

# P6.1

![image-20210328185340841](Images - lista_07/image-20210328185340841.png)

# P6.6
Em todos os items utilizaremos:
```matlab
n = 0:50;
x = 1 + 2*(-1).^n;
```

E após a aplicar o filtro, teremos:

![image-20210328165153573](Images - lista_07/image-20210328165153573.png)

P6.6 – faltaram desenhos do diagrama de fluxo!!

## 1)

```matlab
b = cos(0.1*pi*[0:4]);
a = [1,((0.8).^[1:5]).*sin(0.1*pi*[1:5])];
y = filter(b,a,x);
```
## 3)
```matlab
[b0,B,A] = dir2cas(b,a)
y = casfiltr(b0,B,A,x)
```
## 5)
```matlab
[K,C] = dir2ladr(b,a)
[y] = ladrfilt(K,C,x)
```

# P6.17
Os coeficientes são `e.^(-0.9.*abs([0:6]-3))`, ou seja, `0.067206   0.165299   0.406570   1.000000   0.406570   0.165299   0.067206`
## 1)

![image-20210328171902364](Images - lista_07/image-20210328171902364.png)

## 2)

![image-20210328171825530](Images - lista_07/image-20210328171825530.png)

## 3)

`[b0,B,A] = dir2cas(b,a)`

![image-20210328172148030](Images - lista_07/image-20210328172148030.png)

# P6.22
## 1a)

![image-20210328185512138](Images - lista_07/image-20210328185512138.png)

P6.22.a – faltou desenho do diagrama de fluxo da forma transposta!!!

## 1b)

Calculando as raízes de b, temos:

```matlab
>> roots([1,-4,6.4,-5.12,2.048,-0.32768])
ans =

   0.80072 + 0.00053i
   0.80072 - 0.00053i
   0.79972 + 0.00085i
   0.79972 - 0.00085i
   0.79911 + 0.00000i
```

![image-20210328185536136](Images - lista_07/image-20210328185536136.png)

## 1c)

é a forma já calculada pela função `dir2cas`, portanto temos apenas que fazer:

```matlab
>> [b0,B,A] = dir2cas([1,-4,6.4,-5.12,2.048,-0.32768], 1)
b0 =  1
B =
   1.00000  -1.59945   0.63956
   1.00000  -1.60145   0.64116
   1.00000  -0.79911   0.00000

A =
   1   0   0
   1   0   0
   1   0   0
```

![image-20210328185548307](Images - lista_07/image-20210328185548307.png)

## 2)

Como vimos, em todas as implementações são feitas 5 multiplicações e 5 somas.