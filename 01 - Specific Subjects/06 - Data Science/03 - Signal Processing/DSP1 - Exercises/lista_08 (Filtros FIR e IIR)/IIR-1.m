clear;
fs = 8000
ws = 2*pi*1000/fs;
wp = 2*pi*2000/fs; 
Rp = 2; % Passband ripple in dB
As = 50; % Stopband attenuation in dB

% Calculations of Chebyshev-I Filter
[N,wn] = cheb1ord(wp/pi,ws/pi,Rp,As);
[b,a] = cheby1(N,Rp,wn,'high');
[b0,B,A] = dir2cas(b,a);

[db,mag,pha,grd,w] = freqz_m(b,a);
delta_w = 2*pi/1000;

printf('Actual Passband Ripple (Rp): %f\n', -(min(db(wp/delta_w+1:1:501))))
printf('Min Stopband attenuation (As): %f\n', -round(max(db(1:1:ws/delta_w+1))))

subplot(2,2,1); plot(w/pi,db);title('Magnitude Response in dB');
xlabel('frequency in pi units'); ylabel('Decibels');ylim([-100, 0]);
subplot(2,2,2); plot(w/pi,pha); title('Phase response');
xlabel('frequency in pi units'); ylabel('pi units');
subplot(2,2,3); zplane(b,a);title('Z-plane');
subplot(2,2,4); plot(w/pi,grd);title('Group delay');
xlabel('frequency in pi units'); ylabel('Samples')
