# Introdução
Este relatório descreve o projeto de quadro filtros digitais passa-altas, elaborados como parte da disciplina de Processamento Digital de Sinais I do curso de Engenharia Eletrônica do IFSC. A frequência de amostragem do sistema é sempre de $f_s = 8 kHz$, e os filtros devem atender os seguintes requisitos:

* Banda de rejeição: 0 a 1 kHz
* Banda passante: 2 a 4 kHz
* Ripple na banda passante de 2 dB
* Atenuação mínima na banda de rejeição de 40 dB

Serão implementados dois filtros FIR e dois IIR, seguindo as metodologias apresentadas em sala de aulas e no livro texto Digital Signal Processing Using MATLAB (Proakis e Ingle, 2010). Como margem de segurança, decidiu-se adicionar previamente $10dB$ de atenuação à todos os filtros.

Os códigos Matlab necessários para reproduzir os resultados foram distribuídos junto com o presente documento. O restante deste documento está estruturado conforme segue:
[TOC]

# Projeto dos filtros FIR

## Janela Kaiser


![image-20210424234005326](Images - relatorio/image-20210424234005326.png)
M =  27
beta =  4.5513
Actual Passband Ripple (Rp): 0.043447
Min Stopband attenuation (As): 53.000000

### Implementação em estrutura direta

```matlab
h =

 Columns 1 through 7:

  -5.1234e-04  -2.7932e-03  -1.8823e-03   5.5778e-03   1.1008e-02  -2.3278e-17  -2.2529e-02

 Columns 8 through 14:

  -2.3946e-02   1.7925e-02   6.5546e-02   3.6443e-02  -1.0729e-01  -2.9060e-01   6.2500e-01

 Columns 15 through 21:

  -2.9060e-01  -1.0729e-01   3.6443e-02   6.5546e-02   1.7925e-02  -2.3946e-02  -2.2529e-02

 Columns 22 through 27:

  -2.3278e-17   1.1008e-02   5.5778e-03  -1.8823e-03  -2.7932e-03  -5.1234e-04
```
![image-20210425132939394](Images - relatorio/image-20210425132939394.png)

Número de adições necessárias: 27
Número de multiplicações necessárias: 27
Total de operações: 54

### Efeitos da quantização dos coeficientes

Actual Passband Ripple (Rp): 0.043209
Min Stopband attenuation (As): 53.000000

![image-20210424234827645](Images - relatorio/image-20210424234827645.png)

## Janela backman

![image-20210424234349833](Images - relatorio/image-20210424234349833.png)

M=45
Actual Passband Ripple (Rp): 0.003319
Min Stopband attenuation (As): 74.000000

### Implementação em estrutura direta

```matlab
h =

 Columns 1 through 7:

  -1.4198e-19   1.0724e-05   1.2022e-04   1.1266e-04  -4.0772e-04  -9.2567e-04  -1.9779e-18

 Columns 8 through 14:

   2.2830e-03   2.5708e-03  -1.9849e-03  -7.2172e-03  -3.7651e-03   9.3272e-03   1.6128e-02

 Columns 15 through 21:

  -3.0837e-17  -2.7605e-02  -2.7611e-02   1.9716e-02   6.9540e-02   3.7648e-02  -1.0883e-01

 Columns 22 through 28:

  -2.9163e-01   6.2500e-01  -2.9163e-01  -1.0883e-01   3.7648e-02   6.9540e-02   1.9716e-02

 Columns 29 through 35:

  -2.7611e-02  -2.7605e-02  -3.0837e-17   1.6128e-02   9.3272e-03  -3.7651e-03  -7.2172e-03

 Columns 36 through 42:

  -1.9849e-03   2.5708e-03   2.2830e-03  -1.9779e-18  -9.2567e-04  -4.0772e-04   1.1266e-04

 Columns 43 through 45:

   1.2022e-04   1.0724e-05  -1.4198e-19
```
![image-20210425132932437](Images - relatorio/image-20210425132932437.png)

Número de adições necessárias: 45
Número de multiplicações necessárias: 45
Total de operações: 90

### Efeitos da quantização dos coeficientes

![image-20210424235038579](Images - relatorio/image-20210424235038579.png)

Actual Passband Ripple (Rp): 0.002790
Min Stopband attenuation (As): 72.000000



# Projeto dos filtros IIR

## Chebyschev I

![image-20210424235729399](Images - relatorio/image-20210424235729399.png)

N =  5
Actual Passband Ripple (Rp): 2.000000
Min Stopband attenuation (As): 58.000000

### Implementação em estrutura cascata

```matlab
b0 =  0.018404
B =

   1.00000  -1.99941   0.99941
   1.00000  -2.00154   1.00154
   1.00000  -0.99905   0.00000

A =

   1.00000   0.69498   0.59547
   1.00000   0.04584   0.87071
   1.00000   0.64162   0.00000
```

![image-20210425122621333](Images - relatorio/image-20210425122621333.png)

Número de adições necessárias: 12
Número de multiplicações necessárias: 12
Total de operações: 25

### Efeitos da quantização dos coeficientes

![image-20210425001225822](Images - relatorio/image-20210425001225822.png)

Actual Passband Ripple (Rp): 2.001704
Min Stopband attenuation (As): 58.000000

## Elíptico

![image-20210425003841653](Images - relatorio/image-20210425003841653.png)

N =  4
Actual Passband Ripple (Rp): 1.999953
Min Stopband attenuation (As): 50.000000

### Implementação em estrutura cascata

```matlab
b0 =  0.070355
B =

   1.00000  -1.11189   1.00000
   1.00000  -1.78758   1.00000

A =

   1.000000   0.819370   0.405287
   1.000000   0.052474   0.834226
```

![image-20210425122531873](Images - relatorio/image-20210425122531873.png)



Número de adições necessárias: 8
Número de multiplicações necessárias: 8
Total de operações: 17

### Efeitos da quantização dos coeficientes

![image-20210425003946651](Images - relatorio/image-20210425003946651.png)

Actual Passband Ripple (Rp): 2.000358
Min Stopband attenuation (As): 50.000000

# Conclusões

A margem de segurança não foi necessária, pois todos os filtros atenderam adequadamente os requisitos.

A quantização prejudicou apenas ligeiramente a performance dos filtros, sendo que o único requisito que deixou de ser cumprido foi o bandpass ripple dos filtros IIR (2.001704 ao invés de 2, no pior caso). A quantização também perturbou a posição dos polos e zeros do filtro, porém nenhum filtro tornou-se instável por causa disso.

Em termos de complexidade computacional da implementação, os filtros IIR levaram à implementações com ordens significativamente menores, em média 8 vezes menor. O número de multiplicações e adições, consequentemente, nos filtros IIR também foram mais eficientes.

