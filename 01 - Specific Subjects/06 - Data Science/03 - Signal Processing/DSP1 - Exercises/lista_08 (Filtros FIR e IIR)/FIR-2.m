clear;
fs = 8000
ws = 2*pi*1000/fs;
wp = 2*pi*2000/fs; 
As = 50;

% calculo
tr_width = wp - ws
wc = (wp + ws)/2
M = ceil(11*pi/tr_width) + 1 
hd = ideal_lp(pi,M) - ideal_lp(wc,M)
n=[0:1:M-1];
w_win = (blackman(M))';
h = hd .* w_win;
[db,mag,pha,grd,w] = freqz_m(h,[1]);
delta_w = 2*pi/1000;

printf('Actual Passband Ripple (Rp): %f\n', -(min(db(wp/delta_w+1:1:501))))
printf('Min Stopband attenuation (As): %f\n', -round(max(db(1:1:ws/delta_w+1))))

subplot(2,2,1); plot(w/pi,db);title('Magnitude Response in dB');
xlabel('frequency in pi units'); ylabel('Decibels');
subplot(2,2,2); plot(w/pi,pha); title('Phase response');
xlabel('frequency in pi units'); ylabel('pi units');
subplot(2,2,3); zplane(h,[1]);title('Z-plane');
subplot(2,2,4); stem(w/pi, grd);title('Group delay');
xlabel('frequency in pi units'); ylabel('Samples')