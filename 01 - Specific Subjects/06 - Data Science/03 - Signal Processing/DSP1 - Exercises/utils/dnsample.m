function [y,m] = dnsample(x,nx,M)
% decimation y(n)=x(n*M)
% length(y)=length(x)/M
% 

m=1:1:floor(length(x)/M);
y=zeros(1,length(m));
y(1)=x(1);
Lx=length(m);
for q=1:1:Lx,
  y(q)=x(q*M); 
end