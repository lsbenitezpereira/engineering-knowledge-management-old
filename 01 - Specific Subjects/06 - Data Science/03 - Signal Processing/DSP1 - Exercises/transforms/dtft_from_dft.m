function [Xk] = dtft_from_dft(xn)
    N = length(xn);
    paddings = 20;

    xn = [xn,zeros(1,paddings*N)];
    N = (paddings+1)*N;

    n = [0:1:N-1]; % row vector for n
    k = [0:1:N-1]; % row vecor for k
    WN = exp(-j*2*pi/N);  % Wn factor
    nk = n'*k; % creates a N by N matrix of nk values
    WNnk = WN .^ nk; % DFS matrix
    Xk = xn * WNnk; % row vector for DFS coefficients

    subplot(2,1,1); 
    plot(n*2/N, abs(Xk));
    grid;
    title('Magnitude Part');
    xlabel('frequency in pi units'); 
    
    subplot(2,1,2);
    plot(n*2/N, angle(Xk));
    grid;
    title('Angle Part');
    xlabel('frequency in pi units'); 
endfunction